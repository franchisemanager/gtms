Public Class frmExpenseEndAnnualAmount
    Inherits DevExpress.XtraEditors.XtraForm

    Private ExpenseAllowanceDataRow As DataRow
    Public BRID As Int32
    Public EXID As Int32

    Private hTransaction As SqlClient.SqlTransaction
    Private Property Transaction() As SqlClient.SqlTransaction
        Get
            Return hTransaction
        End Get
        Set(ByVal Value As SqlClient.SqlTransaction)
            hTransaction = Value
            Power.Library.Library.ApplyTransactionToAllDataAdapters(Value, Me)
        End Set
    End Property

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal BRID As Integer, ByVal EXID As Integer, ByVal Transaction As System.Data.SqlClient.SqlTransaction)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.BRID = BRID
        Me.EXID = EXID
        Me.Transaction = Transaction
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DsWizards As WindowsApplication.dsWizards
    Friend WithEvents txtALFromDate As DevExpress.XtraEditors.DateEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmExpenseEndAnnualAmount))
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton
        Me.DsWizards = New WindowsApplication.dsWizards
        Me.txtALFromDate = New DevExpress.XtraEditors.DateEdit
        CType(Me.DsWizards, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtALFromDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.Location = New System.Drawing.Point(8, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(328, 56)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "You have either discontinued an expense or changed the payment method for this ex" & _
        "pense.  This expense was previously on a specified annual amount.  This annual a" & _
        "mount will now be set to zero."
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.Location = New System.Drawing.Point(8, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(328, 48)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "You must specify a date that the annual amount should end.  This is the date the " & _
        "expense is no longer paid as an annual amount."
        '
        'Label27
        '
        Me.Label27.Location = New System.Drawing.Point(16, 128)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(136, 21)
        Me.Label27.TabIndex = 2
        Me.Label27.Text = "Annual amount end date:"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(176, 168)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 5
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(256, 168)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 6
        Me.btnCancel.Text = "Cancel"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton1.Image = CType(resources.GetObject("SimpleButton1.Image"), System.Drawing.Image)
        Me.SimpleButton1.Location = New System.Drawing.Point(8, 168)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(72, 23)
        Me.SimpleButton1.TabIndex = 4
        Me.SimpleButton1.Text = "Help"
        '
        'DsWizards
        '
        Me.DsWizards.DataSetName = "dsWizards"
        Me.DsWizards.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'txtALFromDate
        '
        Me.txtALFromDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsWizards, "ExpenseAllowances.ALFromDate"))
        Me.txtALFromDate.EditValue = Nothing
        Me.txtALFromDate.Location = New System.Drawing.Point(152, 128)
        Me.txtALFromDate.Name = "txtALFromDate"
        '
        'txtALFromDate.Properties
        '
        Me.txtALFromDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtALFromDate.Size = New System.Drawing.Size(176, 20)
        Me.txtALFromDate.TabIndex = 3
        '
        'frmExpenseEndAnnualAmount
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(338, 200)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtALFromDate)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExpenseEndAnnualAmount"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Cancel Annual Amount"
        CType(Me.DsWizards, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtALFromDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmExpenseEndAnnualAmount_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ExpenseAllowanceDataRow = DsWizards.ExpenseAllowances.NewExpenseAllowancesRow
        ExpenseAllowanceDataRow("BRID") = BRID
        ExpenseAllowanceDataRow("EXID") = EXID
        DsWizards.ExpenseAllowances.AddExpenseAllowancesRow(ExpenseAllowanceDataRow)
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If ValidateForm() Then
            sp_InsertExpenseAllowance(BRID, EXID, ExpenseAllowanceDataRow("ALFromDate"), 0, 0, Transaction)

            OK = True
            Me.Close()
        End If
    End Sub

    Private Function ValidateForm() As Boolean
        If ExpenseAllowanceDataRow("ALFromDate") Is DBNull.Value Then
            DevExpress.XtraEditors.XtraMessageBox.Show("You must enter a date.", "Franchise Manager")
            Return False
        End If
        Return True
    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        End If
    End Sub

End Class

