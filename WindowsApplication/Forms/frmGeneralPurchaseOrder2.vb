Public Class frmGeneralPurchaseOrder2
    Inherits DevExpress.XtraEditors.XtraForm

    Private Const TableName As String = "VOrders"
    Private OriginalDataSet As DataSet
    Dim orderBRIDid As Int32


    'Private DataRow As DataRow
    Private Property DataRow() As DataRow
        Set(ByVal Value As DataRow)
            Dim i = 0
            BindingContext(dvOrders).Position = 0
            Do Until IsSameRowByPrimaryKey(DataRow, Value)
                If BindingContext(dvOrders).Position = BindingContext(dvOrders).Count - 1 Then
                    Throw New Exception("The DataRow could not be found in the DataTable " & TableName & ".")
                End If
                BindingContext(dvOrders).Position = BindingContext(dvOrders).Position + 1
            Loop
        End Set
        Get
            Return CType(BindingContext(dvOrders).Current(), DataRowView).Row
        End Get
    End Property

    Public Shared Sub Add(ByVal dataTable As DataTable, ByVal BRID As Integer, Optional ByVal HideAccountingInformation As Boolean = False)
        Dim gui As New frmGeneralPurchaseOrder2
        If HideAccountingInformation Then
            gui.gvOrders_Materials.Columns.Remove(gui.colOMInvoicedCost)
        End If

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            .OriginalDataSet = dataTable.DataSet
            CopyData(.OriginalDataSet, .dataSet, False)
            .dataSet.AcceptChanges()

            .CreateNewRow(BRID)

            gui.ShowDialog()

            UpdateOriginalDataSet(.dataSet, .OriginalDataSet)

            'If gui.ShowDialog() = .DialogResult.OK Then
            '    UpdateOriginalDataSet(.dataSet, .OriginalDataSet)
            'End If
            .SqlConnection.Close()
        End With
    End Sub

    Public Shared Sub Edit(ByVal row As DataRow, Optional ByVal HideAccountingInformation As Boolean = False)
        Dim gui As New frmGeneralPurchaseOrder2
        If HideAccountingInformation Then
            gui.gvOrders_Materials.Columns.Remove(gui.colOMInvoicedCost)
        End If

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            .OriginalDataSet = row.Table.DataSet
            CopyData(.OriginalDataSet, .dataSet, False)
            .dataSet.AcceptChanges()

            .DataRow = row

            .FillData()

            If gui.ShowDialog = .DialogResult.OK Then
                UpdateOriginalDataSet(.dataSet, .OriginalDataSet)
            End If
            .SqlConnection.Close()
        End With
    End Sub

    Private Sub CreateNewRow(ByVal BRID As Integer)
        'SqlConnection.Open()
        Dim OrderNumber As Long = sp_GetNextOrderNumber(BRID, SqlConnection)
        'SqlConnection.Close()

        Dim newRow As DataRow = dataSet.Tables(TableName).NewRow()
        newRow("BRID") = BRID
        newRow("OROrderNumber") = OrderNumber
        newRow("ORDate") = Today.Date
        newRow("ORType") = "GPO"
        newRow("ORShipToJobAddress") = False
        newRow("ORReceived") = False
        'newRow("VOrdersVOrders_Materials") = ""

        orderBRIDid = BRID

        DataRow = dataSet.Tables(TableName).Rows.Add(newRow.ItemArray)
    End Sub

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents dataSet As WindowsApplication.dsOrders
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtNIName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents dtNIDeliveryDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtLDUser As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents dgOrder_Materials As DevExpress.XtraGrid.GridControl
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents RepositoryItemLookUpEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents gvOrders_Materials As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colMTID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOMBrand As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOMModelNumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOMDescription As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gvOrders_Materials_SubItems As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents BandedGridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BandedGridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BandedGridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BandedGridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents editButtonPopup As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents txtAmount As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents dvStockedItems As System.Data.DataView
    Friend WithEvents dvStaff As System.Data.DataView
    Friend WithEvents btnRemoveMaterial As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dvOrders As System.Data.DataView
    Friend WithEvents pceSupplier As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents pccSupplier As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents pceShipTo As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents pccShipTo As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents colOMQuotedCost As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOMInvoicedCost As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtCurrency As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents TextEdit4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtCurrency2 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnPrint As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label9 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmGeneralPurchaseOrder2))
        Me.gvOrders_Materials_SubItems = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.BandedGridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.BandedGridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.BandedGridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.BandedGridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.editButtonPopup = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
        Me.dgOrder_Materials = New DevExpress.XtraGrid.GridControl
        Me.dvOrders = New System.Data.DataView
        Me.dataSet = New WindowsApplication.dsOrders
        Me.gvOrders_Materials = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colMTID = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemLookUpEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.colOMBrand = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colOMModelNumber = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colOMDescription = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colOMQuotedCost = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtCurrency2 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colOMInvoicedCost = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtCurrency = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.txtAmount = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit
        Me.TextEdit4 = New DevExpress.XtraEditors.TextEdit
        Me.pceShipTo = New DevExpress.XtraEditors.PopupContainerEdit
        Me.pccShipTo = New DevExpress.XtraEditors.PopupContainerControl
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit
        Me.Label8 = New System.Windows.Forms.Label
        Me.pceSupplier = New DevExpress.XtraEditors.PopupContainerEdit
        Me.pccSupplier = New DevExpress.XtraEditors.PopupContainerControl
        Me.Label6 = New System.Windows.Forms.Label
        Me.btnRemoveMaterial = New DevExpress.XtraEditors.SimpleButton
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtLDUser = New DevExpress.XtraEditors.LookUpEdit
        Me.dvStaff = New System.Data.DataView
        Me.Label4 = New System.Windows.Forms.Label
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.dtNIDeliveryDate = New DevExpress.XtraEditors.DateEdit
        Me.txtNIName = New DevExpress.XtraEditors.TextEdit
        Me.Label7 = New System.Windows.Forms.Label
        Me.dvStockedItems = New System.Data.DataView
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.btnPrint = New DevExpress.XtraEditors.SimpleButton
        CType(Me.gvOrders_Materials_SubItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.editButtonPopup, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgOrder_Materials, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvOrders_Materials, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemLookUpEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCurrency2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCurrency, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pceShipTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pccShipTo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pceSupplier.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pccSupplier, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDUser.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvStaff, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtNIDeliveryDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNIName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvStockedItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gvOrders_Materials_SubItems
        '
        Me.gvOrders_Materials_SubItems.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvOrders_Materials_SubItems.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvOrders_Materials_SubItems.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvOrders_Materials_SubItems.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvOrders_Materials_SubItems.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvOrders_Materials_SubItems.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvOrders_Materials_SubItems.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvOrders_Materials_SubItems.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvOrders_Materials_SubItems.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvOrders_Materials_SubItems.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvOrders_Materials_SubItems.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvOrders_Materials_SubItems.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvOrders_Materials_SubItems.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvOrders_Materials_SubItems.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvOrders_Materials_SubItems.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvOrders_Materials_SubItems.Appearance.VertLine.Options.UseBackColor = True
        Me.gvOrders_Materials_SubItems.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.BandedGridColumn1, Me.BandedGridColumn2, Me.BandedGridColumn3, Me.BandedGridColumn4})
        Me.gvOrders_Materials_SubItems.GridControl = Me.dgOrder_Materials
        Me.gvOrders_Materials_SubItems.Name = "gvOrders_Materials_SubItems"
        Me.gvOrders_Materials_SubItems.OptionsCustomization.AllowFilter = False
        Me.gvOrders_Materials_SubItems.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvOrders_Materials_SubItems.OptionsView.ShowGroupPanel = False
        Me.gvOrders_Materials_SubItems.ViewCaption = "Sub Items"
        '
        'BandedGridColumn1
        '
        Me.BandedGridColumn1.Caption = "Brand"
        Me.BandedGridColumn1.FieldName = "OMSBrand"
        Me.BandedGridColumn1.Name = "BandedGridColumn1"
        Me.BandedGridColumn1.Visible = True
        Me.BandedGridColumn1.VisibleIndex = 1
        Me.BandedGridColumn1.Width = 126
        '
        'BandedGridColumn2
        '
        Me.BandedGridColumn2.Caption = "Model #"
        Me.BandedGridColumn2.FieldName = "OMSModelNumber"
        Me.BandedGridColumn2.Name = "BandedGridColumn2"
        Me.BandedGridColumn2.Visible = True
        Me.BandedGridColumn2.VisibleIndex = 2
        Me.BandedGridColumn2.Width = 124
        '
        'BandedGridColumn3
        '
        Me.BandedGridColumn3.Caption = "Qty"
        Me.BandedGridColumn3.FieldName = "OMSQty"
        Me.BandedGridColumn3.Name = "BandedGridColumn3"
        Me.BandedGridColumn3.Visible = True
        Me.BandedGridColumn3.VisibleIndex = 3
        Me.BandedGridColumn3.Width = 88
        '
        'BandedGridColumn4
        '
        Me.BandedGridColumn4.Caption = "Description"
        Me.BandedGridColumn4.ColumnEdit = Me.editButtonPopup
        Me.BandedGridColumn4.FieldName = "OMSDescription"
        Me.BandedGridColumn4.Name = "BandedGridColumn4"
        Me.BandedGridColumn4.Visible = True
        Me.BandedGridColumn4.VisibleIndex = 0
        Me.BandedGridColumn4.Width = 353
        '
        'editButtonPopup
        '
        Me.editButtonPopup.AutoHeight = False
        Me.editButtonPopup.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.editButtonPopup.Name = "editButtonPopup"
        '
        'dgOrder_Materials
        '
        'Me.dgOrder_Materials.DataBindings.Add(New System.Windows.Forms.Binding("DataSource", Me.dvOrders, "VOrdersVOrders_Materials"))
        Me.dgOrder_Materials.DataSource = Me.dataSet.VOrders_Materials
        '
        'dgOrder_Materials.EmbeddedNavigator
        '
        Me.dgOrder_Materials.EmbeddedNavigator.Name = ""
        GridLevelNode1.LevelTemplate = Me.gvOrders_Materials_SubItems
        GridLevelNode1.RelationName = "VOrders_MaterialsOrders_Materials_SubItems"
        Me.dgOrder_Materials.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.dgOrder_Materials.Location = New System.Drawing.Point(8, 264)
        Me.dgOrder_Materials.MainView = Me.gvOrders_Materials
        Me.dgOrder_Materials.Name = "dgOrder_Materials"
        Me.dgOrder_Materials.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemLookUpEdit1, Me.editButtonPopup, Me.txtAmount, Me.txtCurrency, Me.txtCurrency2})
        Me.dgOrder_Materials.ShowOnlyPredefinedDetails = True
        Me.dgOrder_Materials.Size = New System.Drawing.Size(712, 152)
        Me.dgOrder_Materials.TabIndex = 18
        Me.dgOrder_Materials.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvOrders_Materials, Me.gvOrders_Materials_SubItems})
        '
        'dvOrders
        '
        Me.dvOrders.RowFilter = "ORType = 'GPO'"
        Me.dvOrders.Table = Me.dataSet.VOrders
        '
        'dataSet
        '
        Me.dataSet.DataSetName = "dsOrders"
        Me.dataSet.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'gvOrders_Materials
        '
        Me.gvOrders_Materials.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvOrders_Materials.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvOrders_Materials.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvOrders_Materials.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvOrders_Materials.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvOrders_Materials.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvOrders_Materials.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvOrders_Materials.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvOrders_Materials.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvOrders_Materials.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvOrders_Materials.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvOrders_Materials.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvOrders_Materials.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvOrders_Materials.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvOrders_Materials.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvOrders_Materials.Appearance.VertLine.Options.UseBackColor = True
        Me.gvOrders_Materials.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colMTID, Me.colOMBrand, Me.colOMModelNumber, Me.colOMDescription, Me.colOMQuotedCost, Me.colOMInvoicedCost})
        Me.gvOrders_Materials.GridControl = Me.dgOrder_Materials
        Me.gvOrders_Materials.Name = "gvOrders_Materials"
        Me.gvOrders_Materials.OptionsCustomization.AllowFilter = False
        Me.gvOrders_Materials.OptionsDetail.AllowExpandEmptyDetails = True
        Me.gvOrders_Materials.OptionsDetail.EnableDetailToolTip = True
        Me.gvOrders_Materials.OptionsDetail.ShowDetailTabs = False
        Me.gvOrders_Materials.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvOrders_Materials.OptionsView.ShowGroupPanel = False
        '
        'colMTID
        '
        Me.colMTID.Caption = "Item"
        Me.colMTID.ColumnEdit = Me.RepositoryItemLookUpEdit1
        Me.colMTID.FieldName = "MTID"
        Me.colMTID.Name = "colMTID"
        Me.colMTID.Visible = True
        Me.colMTID.VisibleIndex = 0
        Me.colMTID.Width = 250
        '
        'RepositoryItemLookUpEdit1
        '
        Me.RepositoryItemLookUpEdit1.AutoHeight = False
        Me.RepositoryItemLookUpEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemLookUpEdit1.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("MTName")})
        Me.RepositoryItemLookUpEdit1.DataSource = Me.dataSet.VMaterials
        Me.RepositoryItemLookUpEdit1.DisplayMember = "MTName"
        Me.RepositoryItemLookUpEdit1.Name = "RepositoryItemLookUpEdit1"
        Me.RepositoryItemLookUpEdit1.NullText = "<Incomplete>"
        Me.RepositoryItemLookUpEdit1.ShowFooter = False
        Me.RepositoryItemLookUpEdit1.ShowHeader = False
        Me.RepositoryItemLookUpEdit1.ValueMember = "MTID"
        '
        'colOMBrand
        '
        Me.colOMBrand.Caption = "Brand"
        Me.colOMBrand.FieldName = "OMBrand"
        Me.colOMBrand.Name = "colOMBrand"
        Me.colOMBrand.Visible = True
        Me.colOMBrand.VisibleIndex = 2
        Me.colOMBrand.Width = 131
        '
        'colOMModelNumber
        '
        Me.colOMModelNumber.Caption = "Model #"
        Me.colOMModelNumber.FieldName = "OMModelNumber"
        Me.colOMModelNumber.Name = "colOMModelNumber"
        Me.colOMModelNumber.Visible = True
        Me.colOMModelNumber.VisibleIndex = 3
        Me.colOMModelNumber.Width = 128
        '
        'colOMDescription
        '
        Me.colOMDescription.Caption = "Description"
        Me.colOMDescription.ColumnEdit = Me.editButtonPopup
        Me.colOMDescription.FieldName = "OMDescription"
        Me.colOMDescription.Name = "colOMDescription"
        Me.colOMDescription.Visible = True
        Me.colOMDescription.VisibleIndex = 1
        Me.colOMDescription.Width = 267
        '
        'colOMQuotedCost
        '
        Me.colOMQuotedCost.Caption = "Quoted Cost"
        Me.colOMQuotedCost.ColumnEdit = Me.txtCurrency2
        Me.colOMQuotedCost.FieldName = "OMQuotedCost"
        Me.colOMQuotedCost.Name = "colOMQuotedCost"
        Me.colOMQuotedCost.Visible = True
        Me.colOMQuotedCost.VisibleIndex = 4
        Me.colOMQuotedCost.Width = 147
        '
        'txtCurrency2
        '
        Me.txtCurrency2.AutoHeight = False
        Me.txtCurrency2.DisplayFormat.FormatString = "c"
        Me.txtCurrency2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCurrency2.EditFormat.FormatString = "c"
        Me.txtCurrency2.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCurrency2.Name = "txtCurrency2"
        '
        'colOMInvoicedCost
        '
        Me.colOMInvoicedCost.Caption = "Invoiced Cost"
        Me.colOMInvoicedCost.ColumnEdit = Me.txtCurrency
        Me.colOMInvoicedCost.FieldName = "OMInvoicedCost"
        Me.colOMInvoicedCost.Name = "colOMInvoicedCost"
        Me.colOMInvoicedCost.Visible = True
        Me.colOMInvoicedCost.VisibleIndex = 5
        Me.colOMInvoicedCost.Width = 157
        '
        'txtCurrency
        '
        Me.txtCurrency.AutoHeight = False
        Me.txtCurrency.DisplayFormat.FormatString = "c"
        Me.txtCurrency.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCurrency.EditFormat.FormatString = "c"
        Me.txtCurrency.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCurrency.Name = "txtCurrency"
        Me.txtCurrency.NullText = "<Incomplete>"
        '
        'txtAmount
        '
        Me.txtAmount.AutoHeight = False
        Me.txtAmount.DisplayFormat.FormatString = "#.000"
        Me.txtAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtAmount.EditFormat.FormatString = "#.000"
        Me.txtAmount.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtAmount.Name = "txtAmount"
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.XtraTabControl1.Location = New System.Drawing.Point(8, 8)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(744, 480)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1})
        Me.XtraTabControl1.Text = "XtraTabControl1"
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.Label9)
        Me.XtraTabPage1.Controls.Add(Me.Label10)
        Me.XtraTabPage1.Controls.Add(Me.CheckEdit1)
        Me.XtraTabPage1.Controls.Add(Me.TextEdit4)
        Me.XtraTabPage1.Controls.Add(Me.pceShipTo)
        Me.XtraTabPage1.Controls.Add(Me.TextEdit3)
        Me.XtraTabPage1.Controls.Add(Me.Label8)
        Me.XtraTabPage1.Controls.Add(Me.pceSupplier)
        Me.XtraTabPage1.Controls.Add(Me.Label6)
        Me.XtraTabPage1.Controls.Add(Me.btnRemoveMaterial)
        Me.XtraTabPage1.Controls.Add(Me.Label5)
        Me.XtraTabPage1.Controls.Add(Me.dgOrder_Materials)
        Me.XtraTabPage1.Controls.Add(Me.txtLDUser)
        Me.XtraTabPage1.Controls.Add(Me.Label4)
        Me.XtraTabPage1.Controls.Add(Me.TextEdit2)
        Me.XtraTabPage1.Controls.Add(Me.Label3)
        Me.XtraTabPage1.Controls.Add(Me.Label2)
        Me.XtraTabPage1.Controls.Add(Me.dtNIDeliveryDate)
        Me.XtraTabPage1.Controls.Add(Me.txtNIName)
        Me.XtraTabPage1.Controls.Add(Me.Label7)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(735, 450)
        Me.XtraTabPage1.Text = "Order Details"
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(8, 112)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(168, 21)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Notes (appears on order form):"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(8, 208)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(112, 21)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "Ship To:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CheckEdit1
        '
        Me.CheckEdit1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dvOrders, "ORReceived"))
        Me.CheckEdit1.Location = New System.Drawing.Point(616, 176)
        Me.CheckEdit1.Name = "CheckEdit1"
        '
        'CheckEdit1.Properties
        '
        Me.CheckEdit1.Properties.Caption = "Order Received"
        Me.CheckEdit1.Size = New System.Drawing.Size(104, 19)
        Me.CheckEdit1.TabIndex = 14
        '
        'TextEdit4
        '
        Me.TextEdit4.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dvOrders, "ORNotes"))
        Me.TextEdit4.EditValue = ""
        Me.TextEdit4.Location = New System.Drawing.Point(176, 112)
        Me.TextEdit4.Name = "TextEdit4"
        Me.TextEdit4.Size = New System.Drawing.Size(544, 20)
        Me.TextEdit4.TabIndex = 9
        '
        'pceShipTo
        '
        Me.pceShipTo.Location = New System.Drawing.Point(120, 208)
        Me.pceShipTo.Name = "pceShipTo"
        '
        'pceShipTo.Properties
        '
        Me.pceShipTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.pceShipTo.Properties.CloseOnLostFocus = False
        Me.pceShipTo.Properties.NullText = "<No contact is selected>"
        Me.pceShipTo.Properties.PopupControl = Me.pccShipTo
        Me.pceShipTo.Properties.ShowPopupShadow = False
        Me.pceShipTo.Size = New System.Drawing.Size(600, 20)
        Me.pceShipTo.TabIndex = 16
        '
        'pccShipTo
        '
        Me.pccShipTo.Location = New System.Drawing.Point(88, 168)
        Me.pccShipTo.Name = "pccShipTo"
        Me.pccShipTo.Size = New System.Drawing.Size(640, 336)
        Me.pccShipTo.TabIndex = 10
        Me.pccShipTo.Text = "PopupContainerControl1"
        '
        'TextEdit3
        '
        Me.TextEdit3.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dvOrders, "ORSupplierInvoiceNumber"))
        Me.TextEdit3.EditValue = ""
        Me.TextEdit3.Location = New System.Drawing.Point(120, 176)
        Me.TextEdit3.Name = "TextEdit3"
        Me.TextEdit3.Size = New System.Drawing.Size(488, 20)
        Me.TextEdit3.TabIndex = 13
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(8, 176)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(112, 21)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "Supplier Invoice #:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pceSupplier
        '
        Me.pceSupplier.Location = New System.Drawing.Point(120, 144)
        Me.pceSupplier.Name = "pceSupplier"
        '
        'pceSupplier.Properties
        '
        Me.pceSupplier.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.pceSupplier.Properties.CloseOnLostFocus = False
        Me.pceSupplier.Properties.NullText = "<No contact is selected>"
        Me.pceSupplier.Properties.PopupControl = Me.pccSupplier
        Me.pceSupplier.Properties.ShowPopupShadow = False
        Me.pceSupplier.Size = New System.Drawing.Size(600, 20)
        Me.pceSupplier.TabIndex = 11
        '
        'pccSupplier
        '
        Me.pccSupplier.Location = New System.Drawing.Point(48, 160)
        Me.pccSupplier.Name = "pccSupplier"
        Me.pccSupplier.Size = New System.Drawing.Size(640, 336)
        Me.pccSupplier.TabIndex = 9
        Me.pccSupplier.Text = "PopupContainerControl1"
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(8, 144)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(72, 21)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Supplier:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnRemoveMaterial
        '
        Me.btnRemoveMaterial.Image = CType(resources.GetObject("btnRemoveMaterial.Image"), System.Drawing.Image)
        Me.btnRemoveMaterial.Location = New System.Drawing.Point(8, 424)
        Me.btnRemoveMaterial.Name = "btnRemoveMaterial"
        Me.btnRemoveMaterial.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveMaterial.TabIndex = 19
        Me.btnRemoveMaterial.Text = "Remove"
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(8, 240)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(104, 21)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "Items in Order:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'txtLDUser
        '
        Me.txtLDUser.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dvOrders, "EXIDOrderedBy"))
        Me.txtLDUser.Location = New System.Drawing.Point(120, 48)
        Me.txtLDUser.Name = "txtLDUser"
        '
        'txtLDUser.Properties
        '
        Me.txtLDUser.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtLDUser.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXName", "", 45, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.txtLDUser.Properties.DataSource = Me.dvStaff
        Me.txtLDUser.Properties.DisplayMember = "EXName"
        Me.txtLDUser.Properties.NullText = ""
        Me.txtLDUser.Properties.ShowFooter = False
        Me.txtLDUser.Properties.ShowHeader = False
        Me.txtLDUser.Properties.ShowLines = False
        Me.txtLDUser.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.txtLDUser.Properties.ValueMember = "EXID"
        Me.txtLDUser.Size = New System.Drawing.Size(176, 20)
        Me.txtLDUser.TabIndex = 3
        '
        'dvStaff
        '
        Me.dvStaff.RowFilter = "EGType <> 'OE'"
        Me.dvStaff.Table = Me.dataSet.VExpenses
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(8, 48)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 21)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Raised by:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextEdit2
        '
        Me.TextEdit2.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dvOrders, "ORDescription"))
        Me.TextEdit2.EditValue = ""
        Me.TextEdit2.Location = New System.Drawing.Point(120, 80)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Size = New System.Drawing.Size(600, 20)
        Me.TextEdit2.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(8, 80)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 21)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Description:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(360, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 21)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Date raised:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtNIDeliveryDate
        '
        Me.dtNIDeliveryDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dvOrders, "ORDate"))
        Me.dtNIDeliveryDate.EditValue = New Date(2005, 7, 20, 0, 0, 0, 0)
        Me.dtNIDeliveryDate.Location = New System.Drawing.Point(440, 48)
        Me.dtNIDeliveryDate.Name = "dtNIDeliveryDate"
        '
        'dtNIDeliveryDate.Properties
        '
        Me.dtNIDeliveryDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False
        Me.dtNIDeliveryDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtNIDeliveryDate.Size = New System.Drawing.Size(168, 20)
        Me.dtNIDeliveryDate.TabIndex = 5
        '
        'txtNIName
        '
        Me.txtNIName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dvOrders, "OROrderNumber"))
        Me.txtNIName.EditValue = ""
        Me.txtNIName.Enabled = False
        Me.txtNIName.Location = New System.Drawing.Point(120, 16)
        Me.txtNIName.Name = "txtNIName"
        Me.txtNIName.Size = New System.Drawing.Size(72, 20)
        Me.txtNIName.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(8, 16)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(72, 21)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Order #:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dvStockedItems
        '
        Me.dvStockedItems.Table = Me.dataSet.VStockedItems
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton1.Image = CType(resources.GetObject("SimpleButton1.Image"), System.Drawing.Image)
        Me.SimpleButton1.Location = New System.Drawing.Point(8, 496)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(72, 23)
        Me.SimpleButton1.TabIndex = 1
        Me.SimpleButton1.Text = "Help"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(680, 496)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(600, 496)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 3
        Me.btnOK.Text = "OK"
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;user id=sa;data source=""SERVER\DEV"";persist " & _
        "security info=False;initial catalog=GTMS_DEV"
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.Location = New System.Drawing.Point(520, 496)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(72, 23)
        Me.btnPrint.TabIndex = 2
        Me.btnPrint.Text = "Print..."
        '
        'frmGeneralPurchaseOrder2
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(762, 528)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Controls.Add(Me.pccSupplier)
        Me.Controls.Add(Me.pccShipTo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGeneralPurchaseOrder2"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "General Purchase Order"
        CType(Me.gvOrders_Materials_SubItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.editButtonPopup, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgOrder_Materials, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvOrders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvOrders_Materials, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemLookUpEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCurrency2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCurrency, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pceShipTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pccShipTo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pceSupplier.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pccSupplier, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDUser.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvStaff, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtNIDeliveryDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNIName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvStockedItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub FillData()
        ' --- CONTACT ---
        DisplaySupplierName()
        DisplayShipToName()
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        DataRow.EndEdit()
        CalculateRowFields()
        OK = True
        Me.Close()
    End Sub

    Private Sub CalculateRowFields()
        DataRow.EndEdit()
        DataRow.BeginEdit()
        DataRow("ORIsComplete") = True
        Dim ORInvoicedCostTotal As Double = 0
        Dim FoundORInvoicedCost As Boolean = False
        Dim FoundNullORInvoicedCost As Boolean = False
        For Each row As DataRow In DataRow.GetChildRows("VOrdersVOrders_Materials")
            If row("OMInvoicedCost") Is DBNull.Value And row("OMUseInvoicedCost") = True Then
                FoundNullORInvoicedCost = True
                DataRow("ORIsComplete") = False
            ElseIf Not row("OMInvoicedCost") Is DBNull.Value And row("OMUseInvoicedCost") = True Then
                FoundORInvoicedCost = True
                ORInvoicedCostTotal = ORInvoicedCostTotal + row("OMInvoicedCost")
            End If
            If row("OMPrimaryAmount") Is DBNull.Value And row("OMUseInvoicedCost") = False Then
                DataRow("ORIsComplete") = False
            End If
            If row("MTID") Is DBNull.Value Then
                DataRow("ORIsComplete") = False
            End If
        Next

        For Each row As DataRow In DataRow.GetChildRows("VOrdersVOrders_StockedItems")
            If row("SIID") Is DBNull.Value Then
                DataRow("ORIsComplete") = False
            End If
            If row("OSInventoryAmount") Is DBNull.Value Then
                DataRow("ORIsComplete") = False
            End If
            If row("OSInventoryDate") Is DBNull.Value Then
                DataRow("ORIsComplete") = False
            End If
        Next

        If FoundORInvoicedCost And Not FoundNullORInvoicedCost Then
            DataRow("ORInvoicedCostTotal") = ORInvoicedCostTotal
        Else
            DataRow("ORInvoicedCostTotal") = DBNull.Value
        End If

        If Not DataRow("ORIsComplete") Then
            DataRow("ORStatus") = "<Incomplete>"
        Else
            DataRow("ORStatus") = ""
        End If

        DataRow.EndEdit()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            DataRow.EndEdit()
            If dataSet.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If Not response = MsgBoxResult.Yes Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub editButtonPopup_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) _
    Handles editButtonPopup.ButtonClick
        Dim focusedView As DevExpress.XtraGrid.Views.Grid.GridView = dgOrder_Materials.FocusedView
        If focusedView.GetDataRow(focusedView.GetSelectedRows(0)) Is Nothing Then
            focusedView.AddNewRow()
        End If
        focusedView.SetFocusedValue(frmMemo.Edit(IsNull(focusedView.GetFocusedValue, ""), focusedView.FocusedColumn.Caption))
    End Sub

    Private Sub GridView_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gvOrders_Materials.KeyDown, gvOrders_Materials_SubItems.KeyDown
        If e.KeyCode = Keys.Delete Then
            If CType(sender, DevExpress.XtraGrid.Views.Grid.GridView).SelectedRowsCount > 0 Then
                CType(sender, DevExpress.XtraGrid.Views.Grid.GridView).GetDataRow(CType(sender, DevExpress.XtraGrid.Views.Grid.GridView).GetSelectedRows(0)).Delete()
            End If
        End If
    End Sub

    Private Sub Decimal_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) Handles txtAmount.ParseEditValue, txtCurrency.ParseEditValue, txtCurrency2.ParseEditValue
        Format.Decimal_ParseEditValue(sender, e)
    End Sub

#Region " Orders_Materials "

    Private Sub gvOrders_Materials_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gvOrders_Materials.InitNewRow
        gvOrders_Materials.GetDataRow(e.RowHandle)("OMUseInvoicedCost") = True
    End Sub

    Private Sub gvOrders_Materials_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gvOrders_Materials.ValidateRow
        UpdateMTPrimaryUOM(e.RowHandle)
    End Sub

    Private Sub UpdateMTPrimaryUOM(ByVal rowHandle As Integer)
        If Not (gvOrders_Materials.GetDataRow(rowHandle)("BRID") Is DBNull.Value Or gvOrders_Materials.GetDataRow(rowHandle)("MTID") Is DBNull.Value) Then
            gvOrders_Materials.GetDataRow(rowHandle)("MTPrimaryUOMShortName") = dataSet.VMaterials.FindByBRIDMTID(gvOrders_Materials.GetDataRow(rowHandle)("BRID"), gvOrders_Materials.GetDataRow(rowHandle)("MTID")).MTPrimaryUOMShortName()
        Else
            gvOrders_Materials.GetDataRow(rowHandle)("MTPrimaryUOMShortName") = ""
            gvOrders_Materials.GetDataRow(rowHandle)("BRID") = orderBRIDid
        End If
    End Sub

    Private Sub btnRemoveMaterial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveMaterial.Click
        If Not CType(dgOrder_Materials.FocusedView, DevExpress.XtraGrid.Views.Grid.GridView).GetSelectedRows Is Nothing Then
            If Not CType(dgOrder_Materials.FocusedView, DevExpress.XtraGrid.Views.Grid.GridView).GetDataRow(CType(dgOrder_Materials.FocusedView, DevExpress.XtraGrid.Views.Grid.GridView).GetSelectedRows(0)) Is Nothing Then
                CType(dgOrder_Materials.FocusedView, DevExpress.XtraGrid.Views.Grid.GridView).GetDataRow(CType(dgOrder_Materials.FocusedView, DevExpress.XtraGrid.Views.Grid.GridView).GetSelectedRows(0)).Delete()
            End If
        End If
    End Sub

#End Region

#Region " Supplier "

    Private SupplierContactsLoaded As Boolean = False
    Private lvSupplier As lvContacts2
    Private SupplierDropDownOpen As Boolean = False
    Private Sub pceSupplier_Popup(ByVal sender As Object, ByVal e As System.EventArgs) Handles pceSupplier.Popup
        If SupplierContactsLoaded Then
            If Not ReopeningSupplierPopup Then
                If Not DataRow("CTIDSupplier") Is DBNull.Value Then
                    lvSupplier.SelectRow(DataRow("BRID"), DataRow("CTIDSupplier"))
                End If
            End If
        Else
            Dim c As Cursor = Me.Cursor
            Me.Cursor = Cursors.WaitCursor
            'Me.SqlConnection.Open()
            lvSupplier = New lvContacts2(Me.SqlConnection, DataRow("BRID"))
            'Me.SqlConnection.Close()
            AddHandler lvSupplier.ContactSelected, AddressOf Me.SupplierSelected
            AddHandler lvSupplier.PopupClosed, AddressOf Me.SupplierPopupClosed
            lvSupplier.DoubleClickAction = lvContacts2.ContactAction.SelectContact
            lvSupplier.Dock = DockStyle.Fill
            lvSupplier.Visible = False
            pccSupplier.Controls.Add(lvSupplier)
            lvSupplier.Visible = True
            If Not ReopeningSupplierPopup Then
                If Not DataRow("CTIDSupplier") Is DBNull.Value Then
                    lvSupplier.SelectRow(DataRow("BRID"), DataRow("CTIDSupplier"))
                End If
            End If
            lvSupplier.Select()
            SupplierContactsLoaded = True
            Me.Cursor = c
        End If
        SupplierDropDownOpen = True
    End Sub

    Private ReopeningSupplierPopup As Boolean = False
    Private Sub SupplierPopupClosed()
        ReopeningSupplierPopup = True
        Me.pceSupplier.ShowPopup()
        ReopeningSupplierPopup = False
    End Sub

    Private CTIDSupplier As Long
    Private Sub SupplierSelected(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal BRID As Integer, ByVal CTID As Long)
        DataRow("CTIDSupplier") = CTID
        DisplaySupplierName()
        SupplierDropDownOpen = False
        pceSupplier.ClosePopup()
    End Sub

    Private Sub DisplaySupplierName()
        Dim CTName, CTAddress As String
        If DataRow("CTIDSupplier") Is DBNull.Value Then
            pceSupplier.EditValue = DBNull.Value
            Exit Sub
        End If
        'SqlConnection.Open()
        CTName = IsNull(DataAccess.ExecuteScalar("SELECT CTFirstName FROM Contacts WHERE BRID = @BRID AND CTID = @CTID", CommandType.Text,
            New SqlClient.SqlParameter() {
            New SqlClient.SqlParameter("@BRID", DataRow("BRID")),
            New SqlClient.SqlParameter("@CTID", DataRow("CTIDSupplier"))},
            Me.SqlConnection), "")
        CTAddress = IsNull(DataAccess.ExecuteScalar("SELECT CTStreetAddress01 FROM Contacts WHERE BRID = @BRID AND CTID = @CTID", CommandType.Text,
            New SqlClient.SqlParameter() {
            New SqlClient.SqlParameter("@BRID", DataRow("BRID")),
            New SqlClient.SqlParameter("@CTID", DataRow("CTIDSupplier"))},
            Me.SqlConnection), "")
        'SqlConnection.Close()
        pceSupplier.Text = CTName & " (" & CTAddress & ")"
    End Sub

#End Region

#Region " Ship To "

    Private ShipToContactsLoaded As Boolean = False
    Private lvShipTo As lvContacts2
    Private ShipToDropDownOpen As Boolean = False
    Private Sub pceShipTo_Popup(ByVal sender As Object, ByVal e As System.EventArgs) Handles pceShipTo.Popup
        If ShipToContactsLoaded Then
            If Not ReopeningShipToPopup Then
                If Not DataRow("CTIDShipTo") Is DBNull.Value Then
                    lvShipTo.SelectRow(DataRow("BRID"), DataRow("CTIDShipTo"))
                End If
            End If
        Else
            Dim c As Cursor = Me.Cursor
            Me.Cursor = Cursors.WaitCursor
            'Me.SqlConnection.Open()
            lvShipTo = New lvContacts2(Me.SqlConnection, DataRow("BRID"))
            'Me.SqlConnection.Close()
            AddHandler lvShipTo.ContactSelected, AddressOf Me.ShipToSelected
            AddHandler lvShipTo.PopupClosed, AddressOf Me.ShipToPopupClosed
            lvShipTo.DoubleClickAction = lvContacts2.ContactAction.SelectContact
            lvShipTo.Dock = DockStyle.Fill
            lvShipTo.Visible = False
            pccShipTo.Controls.Add(lvShipTo)
            lvShipTo.Visible = True
            If Not ReopeningShipToPopup Then
                If Not DataRow("CTIDShipTo") Is DBNull.Value Then
                    lvShipTo.SelectRow(DataRow("BRID"), DataRow("CTIDShipTo"))
                End If
            End If
            lvShipTo.Select()
            ShipToContactsLoaded = True
            Me.Cursor = c
        End If
        ShipToDropDownOpen = True
    End Sub

    Private ReopeningShipToPopup As Boolean = False
    Private Sub ShipToPopupClosed()
        ReopeningShipToPopup = True
        Me.pceShipTo.ShowPopup()
        ReopeningShipToPopup = False
    End Sub

    Private CTIDShipTo As Long
    Private Sub ShipToSelected(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal BRID As Integer, ByVal CTID As Long)
        DataRow("CTIDShipTo") = CTID
        DisplayShipToName()
        ShipToDropDownOpen = False
        pceShipTo.ClosePopup()
    End Sub

    Private Sub DisplayShipToName()
        Dim CTName, CTAddress As String
        If DataRow("CTIDShipTo") Is DBNull.Value Then
            pceShipTo.EditValue = DBNull.Value
            Exit Sub
        End If
        'SqlConnection.Open()
        CTName = IsNull(DataAccess.ExecuteScalar("SELECT CTFirstName FROM Contacts WHERE BRID = @BRID AND CTID = @CTID", CommandType.Text,
            New SqlClient.SqlParameter() {
            New SqlClient.SqlParameter("@BRID", DataRow("BRID")),
            New SqlClient.SqlParameter("@CTID", DataRow("CTIDShipTo"))},
            Me.SqlConnection), "")
        CTAddress = IsNull(DataAccess.ExecuteScalar("SELECT CTStreetAddress01 FROM Contacts WHERE BRID = @BRID AND CTID = @CTID", CommandType.Text,
            New SqlClient.SqlParameter() {
            New SqlClient.SqlParameter("@BRID", DataRow("BRID")),
            New SqlClient.SqlParameter("@CTID", DataRow("CTIDShipTo"))},
            Me.SqlConnection), "")
        'SqlConnection.Close()
        pceShipTo.Text = CTName & " (" & CTAddress & ")"
    End Sub

#End Region

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        Dim gui As New frmReport("")
        Dim rep As New repPurchaseOrder
        rep.Load()
        Dim ds As dsOrders = Me.dataSet

        Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.VBranches WHERE BRID = @BRID", SqlConnection)
        cmd.Parameters.Add("@BRID", DataRow("BRID"))
        Dim da As New SqlClient.SqlDataAdapter(cmd)
        ReadUncommittedFill(da, ds.VBranches)

        cmd = New SqlClient.SqlCommand("SELECT * FROM dbo.VContacts WHERE BRID = @BRID AND CTID = @CTID", SqlConnection)
        cmd.Parameters.Add("@BRID", DataRow("BRID"))
        cmd.Parameters.Add("@CTID", DataRow("CTIDSupplier"))
        da = New SqlClient.SqlDataAdapter(cmd)
        ReadUncommittedFill(da, ds.VContactsSupplier)

        cmd = New SqlClient.SqlCommand("SELECT * FROM dbo.VContacts WHERE BRID = @BRID AND CTID = @CTID", SqlConnection)
        cmd.Parameters.Add("@BRID", DataRow("BRID"))
        cmd.Parameters.Add("@CTID", DataRow("CTIDShipTo"))
        da = New SqlClient.SqlDataAdapter(cmd)
        ReadUncommittedFill(da, ds.VContactsShipTo)

        cmd = New SqlClient.SqlCommand("SELECT * FROM dbo.VJobs WHERE BRID = @BRID AND JBID = @JBID", SqlConnection)
        cmd.Parameters.Add("@BRID", DataRow("BRID"))
        cmd.Parameters.Add("@JBID", DataRow("JBID"))
        da = New SqlClient.SqlDataAdapter(cmd)
        ReadUncommittedFill(da, ds.VJobs)

        rep.SetDataSource(ds)
        rep.SetParameterValue("@BRID", DataRow("BRID"))
        rep.SetParameterValue("@ORID", DataRow("ORID"))
        gui.Display(rep)
        Me.Cursor = Windows.Forms.Cursors.Default
        gui.Text = "Purchase Order"
        gui.Show()
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        ShowHelpTopic(Me, "UsingGeneralPurchaseOrders.htm")
    End Sub
End Class

