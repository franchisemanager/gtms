Public Class frmCalendar2
    Inherits DevExpress.XtraEditors.XtraForm

    Private BRID As Int32
    Private APType As String
    Private APTypeID As Long
    Private SqlConnection As SqlClient.SqlConnection
    Private parentRowForm As Form

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal SqlConnection As SqlClient.SqlConnection, ByVal dataSet As DataSet, ByVal BRID As Int32, Optional ByVal APType As String = Nothing, Optional ByVal APTypeID As Long = Nothing, Optional ByVal parentRowForm As Form = Nothing)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.SqlConnection = SqlConnection
        Me.BRID = BRID
        Me.APType = APType
        Me.APTypeID = APTypeID
        Me.parentRowForm = parentRowForm
        If TypeOf parentRowForm Is frmBooking2 Then
            beiFromDate.EditValue = CType(parentRowForm, frmBooking2).CalendarFromDate
            beiToDate.EditValue = CType(parentRowForm, frmBooking2).CalendarToDate
        ElseIf TypeOf parentRowForm Is frmLead2 Then
            beiFromDate.EditValue = CType(parentRowForm, frmLead2).CalendarFromDate
            beiToDate.EditValue = CType(parentRowForm, frmLead2).CalendarToDate
        End If




        Calendar.DataSet = dataSet
        Calendar.Calendar.Start = Today.Date
        Select Case APType
            Case "LD"
                Calendar.ApplyEGTypeFilter("EXAppearInCalendarRC")
            Case "JB"
                Calendar.ApplyEGTypeFilter("EXAppearInCalendarDL")
        End Select
        Calendar.SetHighlight(BRID, APType, APTypeID)
        DateFilterIsListening = True
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnAddAppointment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEditAppointment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveAppointment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Calendar As WindowsApplication.CalendarControl
    Friend WithEvents ImageListToolBar As System.Windows.Forms.ImageList
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents btnNew As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnEdit As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnDelete As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subReportsOnSelectedItem As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnQuickPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPrintPreview As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnHelp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bAdvancedFilter As DevExpress.XtraBars.Bar
    Friend WithEvents beiFromDate As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents beiToDate As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents btnShowAllDates As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarAndDockingController1 As DevExpress.XtraBars.BarAndDockingController
    Friend WithEvents btnPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subReportsOnSelectedItem2 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCalendar2))
        Me.btnAddAppointment = New DevExpress.XtraEditors.SimpleButton
        Me.btnEditAppointment = New DevExpress.XtraEditors.SimpleButton
        Me.btnRemoveAppointment = New DevExpress.XtraEditors.SimpleButton
        Me.btnClose = New DevExpress.XtraEditors.SimpleButton
        Me.Calendar = New WindowsApplication.CalendarControl
        Me.ImageListToolBar = New System.Windows.Forms.ImageList(Me.components)
        Me.BarManager1 = New DevExpress.XtraBars.BarManager
        Me.bAdvancedFilter = New DevExpress.XtraBars.Bar
        Me.beiFromDate = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.beiToDate = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.btnShowAllDates = New DevExpress.XtraBars.BarButtonItem
        Me.BarAndDockingController1 = New DevExpress.XtraBars.BarAndDockingController(Me.components)
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
        Me.btnNew = New DevExpress.XtraBars.BarButtonItem
        Me.btnEdit = New DevExpress.XtraBars.BarButtonItem
        Me.btnDelete = New DevExpress.XtraBars.BarButtonItem
        Me.subReportsOnSelectedItem = New DevExpress.XtraBars.BarSubItem
        Me.btnHelp = New DevExpress.XtraBars.BarButtonItem
        Me.btnPrintPreview = New DevExpress.XtraBars.BarButtonItem
        Me.btnQuickPrint = New DevExpress.XtraBars.BarButtonItem
        Me.btnPrint = New DevExpress.XtraBars.BarButtonItem
        Me.subReportsOnSelectedItem2 = New DevExpress.XtraBars.BarSubItem
        Me.Bar1 = New DevExpress.XtraBars.Bar
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnAddAppointment
        '
        Me.btnAddAppointment.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddAppointment.Image = CType(resources.GetObject("btnAddAppointment.Image"), System.Drawing.Image)
        Me.btnAddAppointment.Location = New System.Drawing.Point(8, 536)
        Me.btnAddAppointment.Name = "btnAddAppointment"
        Me.btnAddAppointment.Size = New System.Drawing.Size(72, 23)
        Me.btnAddAppointment.TabIndex = 1
        Me.btnAddAppointment.Text = "Add..."
        '
        'btnEditAppointment
        '
        Me.btnEditAppointment.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEditAppointment.Image = CType(resources.GetObject("btnEditAppointment.Image"), System.Drawing.Image)
        Me.btnEditAppointment.Location = New System.Drawing.Point(88, 536)
        Me.btnEditAppointment.Name = "btnEditAppointment"
        Me.btnEditAppointment.Size = New System.Drawing.Size(72, 23)
        Me.btnEditAppointment.TabIndex = 2
        Me.btnEditAppointment.Text = "Edit..."
        '
        'btnRemoveAppointment
        '
        Me.btnRemoveAppointment.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveAppointment.Image = CType(resources.GetObject("btnRemoveAppointment.Image"), System.Drawing.Image)
        Me.btnRemoveAppointment.Location = New System.Drawing.Point(168, 536)
        Me.btnRemoveAppointment.Name = "btnRemoveAppointment"
        Me.btnRemoveAppointment.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveAppointment.TabIndex = 3
        Me.btnRemoveAppointment.Text = "Remove"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(712, 536)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(72, 23)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "Close"
        '
        'Calendar
        '
        Me.Calendar.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Calendar.Location = New System.Drawing.Point(8, 32)
        Me.Calendar.Name = "Calendar"
        Me.Calendar.Size = New System.Drawing.Size(776, 496)
        Me.Calendar.TabIndex = 0
        '
        'ImageListToolBar
        '
        Me.ImageListToolBar.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImageListToolBar.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageListToolBar.ImageStream = CType(resources.GetObject("ImageListToolBar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListToolBar.TransparentColor = System.Drawing.Color.Transparent
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.bAdvancedFilter})
        Me.BarManager1.Categories.AddRange(New DevExpress.XtraBars.BarManagerCategory() {New DevExpress.XtraBars.BarManagerCategory("Built-in Menus", New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd"), False), New DevExpress.XtraBars.BarManagerCategory("File", New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")), New DevExpress.XtraBars.BarManagerCategory("Reports", New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")), New DevExpress.XtraBars.BarManagerCategory("Forms", New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")), New DevExpress.XtraBars.BarManagerCategory("Help", New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")), New DevExpress.XtraBars.BarManagerCategory("Filters", New System.Guid("e6c9ece2-ecb3-403a-9dff-0660dbc3a261"))})
        Me.BarManager1.Controller = Me.BarAndDockingController1
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Images = Me.ImageListToolBar
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnNew, Me.btnEdit, Me.btnDelete, Me.subReportsOnSelectedItem, Me.btnHelp, Me.btnPrintPreview, Me.btnQuickPrint, Me.btnPrint, Me.subReportsOnSelectedItem2, Me.beiFromDate, Me.beiToDate, Me.btnShowAllDates})
        Me.BarManager1.MainMenu = Me.bAdvancedFilter
        Me.BarManager1.MaxItemId = 96
        Me.BarManager1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1, Me.RepositoryItemDateEdit2})
        '
        'bAdvancedFilter
        '
        Me.bAdvancedFilter.BarName = "Date Filter"
        Me.bAdvancedFilter.DockCol = 0
        Me.bAdvancedFilter.DockRow = 0
        Me.bAdvancedFilter.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bAdvancedFilter.FloatLocation = New System.Drawing.Point(109, 160)
        Me.bAdvancedFilter.FloatSize = New System.Drawing.Size(638, 24)
        Me.bAdvancedFilter.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.beiFromDate, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.beiToDate, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnShowAllDates, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.bAdvancedFilter.OptionsBar.AllowQuickCustomization = False
        Me.bAdvancedFilter.OptionsBar.DisableClose = True
        Me.bAdvancedFilter.OptionsBar.DrawDragBorder = False
        Me.bAdvancedFilter.OptionsBar.MultiLine = True
        Me.bAdvancedFilter.OptionsBar.UseWholeRow = True
        Me.bAdvancedFilter.Text = "Date Filter"
        '
        'beiFromDate
        '
        Me.beiFromDate.Caption = "Appointment Dates from:"
        Me.beiFromDate.CategoryGuid = New System.Guid("e6c9ece2-ecb3-403a-9dff-0660dbc3a261")
        Me.beiFromDate.Edit = Me.RepositoryItemDateEdit1
        Me.beiFromDate.Id = 93
        Me.beiFromDate.Name = "beiFromDate"
        Me.beiFromDate.Width = 120
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'beiToDate
        '
        Me.beiToDate.Caption = "Appointment Dates to:"
        Me.beiToDate.CategoryGuid = New System.Guid("e6c9ece2-ecb3-403a-9dff-0660dbc3a261")
        Me.beiToDate.Edit = Me.RepositoryItemDateEdit2
        Me.beiToDate.Id = 94
        Me.beiToDate.Name = "beiToDate"
        Me.beiToDate.Width = 120
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'btnShowAllDates
        '
        Me.btnShowAllDates.Caption = "Show A&ll"
        Me.btnShowAllDates.CategoryGuid = New System.Guid("e6c9ece2-ecb3-403a-9dff-0660dbc3a261")
        Me.btnShowAllDates.Id = 95
        Me.btnShowAllDates.ImageIndex = 14
        Me.btnShowAllDates.ImageIndexDisabled = 14
        Me.btnShowAllDates.Name = "btnShowAllDates"
        '
        'BarAndDockingController1
        '
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseForeColor = True
        Me.BarAndDockingController1.PaintStyleName = "OfficeXP"
        '
        'btnNew
        '
        Me.btnNew.Caption = "&New..."
        Me.btnNew.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnNew.Id = 19
        Me.btnNew.ImageIndex = 0
        Me.btnNew.ImageIndexDisabled = 0
        Me.btnNew.Name = "btnNew"
        '
        'btnEdit
        '
        Me.btnEdit.Caption = "&Edit..."
        Me.btnEdit.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnEdit.Id = 20
        Me.btnEdit.ImageIndex = 1
        Me.btnEdit.ImageIndexDisabled = 1
        Me.btnEdit.Name = "btnEdit"
        '
        'btnDelete
        '
        Me.btnDelete.Caption = "&Delete"
        Me.btnDelete.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnDelete.Id = 23
        Me.btnDelete.ImageIndex = 13
        Me.btnDelete.ImageIndexDisabled = 13
        Me.btnDelete.Name = "btnDelete"
        '
        'subReportsOnSelectedItem
        '
        Me.subReportsOnSelectedItem.Caption = "Reports on Selected Item"
        Me.subReportsOnSelectedItem.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.subReportsOnSelectedItem.Id = 78
        Me.subReportsOnSelectedItem.Name = "subReportsOnSelectedItem"
        Me.subReportsOnSelectedItem.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInCustomizing
        '
        'btnHelp
        '
        Me.btnHelp.Caption = "&Help"
        Me.btnHelp.CategoryGuid = New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")
        Me.btnHelp.Id = 68
        Me.btnHelp.ImageIndex = 4
        Me.btnHelp.ImageIndexDisabled = 4
        Me.btnHelp.Name = "btnHelp"
        '
        'btnPrintPreview
        '
        Me.btnPrintPreview.Caption = "Print Pre&view"
        Me.btnPrintPreview.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrintPreview.Id = 44
        Me.btnPrintPreview.ImageIndex = 10
        Me.btnPrintPreview.ImageIndexDisabled = 10
        Me.btnPrintPreview.Name = "btnPrintPreview"
        '
        'btnQuickPrint
        '
        Me.btnQuickPrint.Caption = "Print"
        Me.btnQuickPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnQuickPrint.Id = 46
        Me.btnQuickPrint.ImageIndex = 11
        Me.btnQuickPrint.ImageIndexDisabled = 11
        Me.btnQuickPrint.Name = "btnQuickPrint"
        '
        'btnPrint
        '
        Me.btnPrint.Caption = "&Print..."
        Me.btnPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrint.Id = 43
        Me.btnPrint.ImageIndex = 11
        Me.btnPrint.ImageIndexDisabled = 11
        Me.btnPrint.Name = "btnPrint"
        '
        'subReportsOnSelectedItem2
        '
        Me.subReportsOnSelectedItem2.Caption = "&Reports on Selected Item"
        Me.subReportsOnSelectedItem2.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subReportsOnSelectedItem2.Id = 87
        Me.subReportsOnSelectedItem2.Name = "subReportsOnSelectedItem2"
        '
        'Bar1
        '
        Me.Bar1.BarName = "Date Filter"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 1
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.FloatLocation = New System.Drawing.Point(-2, 188)
        Me.Bar1.FloatSize = New System.Drawing.Size(638, 24)
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.beiFromDate, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.beiToDate, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnShowAllDates, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.Bar1.OptionsBar.AllowQuickCustomization = False
        Me.Bar1.OptionsBar.DisableClose = True
        Me.Bar1.OptionsBar.MultiLine = True
        Me.Bar1.Text = "Date Filter"
        '
        'frmCalendar2
        '
        Me.AcceptButton = Me.btnClose
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(792, 566)
        Me.Controls.Add(Me.Calendar)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnAddAppointment)
        Me.Controls.Add(Me.btnEditAppointment)
        Me.Controls.Add(Me.btnRemoveAppointment)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmCalendar2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Calendar"
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnAddAppointment.Enabled = Not HasRole("branch_read_only")

        'btnAddAppointment.Enabled = True

        btnRemoveAppointment.Enabled = Not HasRole("branch_read_only")
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#Region " Appointments "

    Private Sub Calendar_NewAllDayEvent(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar.NewAllDayEvent
        If Not Calendar.SelectedResource Is Nothing Then
            frmAppointment2.Add(SqlConnection, Calendar.Storage, Calendar.dvAppointments.Table, BRID, APType, APTypeID, True, 1, Calendar.SelectedResource("EXID"), Calendar.Calendar.SelectedInterval.Start, Calendar.Calendar.SelectedInterval.End, parentRowForm)
            Calendar.SetHighlight(BRID, APType, APTypeID)
        Else
            Message.ShowMessage("You must select a staff member on the right to add this appointment to.", MessageBoxIcon.Exclamation)
        End If
    End Sub

    Private Sub Calendar_NewAppointment(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar.NewAppointment
        If Not Calendar.SelectedResource Is Nothing Then
            frmAppointment2.Add(SqlConnection, Calendar.Storage, Calendar.dvAppointments.Table, BRID, APType, APTypeID, False, 1, Calendar.SelectedResource("EXID"), Calendar.Calendar.SelectedInterval.Start, Calendar.Calendar.SelectedInterval.End, parentRowForm)
            Calendar.SetHighlight(BRID, APType, APTypeID)
        Else
            Message.ShowMessage("You must select a staff member on the right to add this appointment to.", MessageBoxIcon.Exclamation)
        End If
    End Sub

    Private Sub btnAddAppointment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAppointment.Click
        If Calendar.Calendar.SelectedInterval.Start.Hour = 0 And Calendar.Calendar.SelectedInterval.Start.Minute = 0 And _
                Calendar.Calendar.SelectedInterval.End.Hour = 0 And Calendar.Calendar.SelectedInterval.End.Minute = 0 And _
                Calendar.Calendar.SelectedInterval.Start <> Calendar.Calendar.SelectedInterval.End Then
            Calendar_NewAllDayEvent(sender, e)
        Else
            Calendar_NewAppointment(sender, e)
        End If
    End Sub

    Private Sub Calendar_EditAppointment(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar.EditAppointment
        If Not Calendar.SelectedAppointment Is Nothing Then
            'If Not (Calendar.SelectedAppointment.Item("BRID") = Me.BRID And Calendar.SelectedAppointment.Item("APType") = APType And Calendar.SelectedAppointment.Item("APTypeID") = APTypeID) Then
            '    If Message.AppointmentNotCurrentObject("job", Message.ObjectAction.Edit) = MsgBoxResult.No Then
            '        Exit Sub
            '    End If
            'End If
            If Calendar.SelectedAppointment.RowState = DataRowState.Added Then
                frmAppointment2.Edit(SqlConnection, Calendar.Storage, Calendar.SelectedAppointment, 1)
            Else
                If AddAppointmentLock(Calendar.SelectedAppointment.Item("APID")) Then
                    frmAppointment2.Edit(SqlConnection, Calendar.Storage, Calendar.SelectedAppointment, 1)
                    Calendar.SetHighlight(BRID, APType, APTypeID)
                Else
                    Message.CurrentlyAccessed("appointment")
                End If
            End If
        End If
    End Sub

    Private Sub btnEditAppointment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditAppointment.Click
        Calendar_EditAppointment(sender, e)
    End Sub

    Private Sub btnRemoveAppointment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveAppointment.Click
        If Not Calendar.SelectedAppointment Is Nothing Then
            'If Not (Calendar.SelectedAppointment.Item("BRID") = Me.BRID And Calendar.SelectedAppointment.Item("APType") = APType And Calendar.SelectedAppointment.Item("APTypeID") = APTypeID) Then
            '    If Message.AppointmentNotCurrentObject("job", Message.ObjectAction.Delete) = MsgBoxResult.No Then
            '        Exit Sub
            '    End If
            'End If
            If Calendar.SelectedAppointment.RowState = DataRowState.Added Then
                Calendar.SelectedAppointment.Delete()
            Else
                If AddAppointmentLock(Calendar.SelectedAppointment.Item("APID")) Then
                    Calendar.SelectedAppointment.Delete()
                Else
                    Message.CurrentlyAccessed("appointment")
                End If
            End If
        End If
    End Sub

    Private Sub Calendar_AppointmentsChanged(ByVal sender As Object, ByVal e As DevExpress.XtraScheduler.PersistentObjectsEventArgs) Handles Calendar.AppointmentsChanged
        If HasRole("branch_read_only") Then Exit Sub
        For Each obj As Object In e.Objects
            'If Not (obj.GetRow(Calendar.Storage)("BRID") = BRID And obj.GetRow(Calendar.Storage)("APType") = APType And obj.GetRow(Calendar.Storage)("APTypeID") = APTypeID) Then
            '   If Message.AppointmentNotCurrentObject ("appointment") = MsgBoxResult.No Then
            '       Calendar.DsCalendar.RejectChanges()
            '    End If
            'End If
            If CType(obj.GetRow(Calendar.Storage), DataRowView).Row.RowState = DataRowState.Added Then
                CType(obj.GetRow(Calendar.Storage), DataRowView)("TAName") = Calendar.DataSet.Tasks.FindByTAID(CType(obj.GetRow(Calendar.Storage), DataRowView)("APTask")).TAName
                CType(obj.GetRow(Calendar.Storage), DataRowView)("EXName") = Calendar.DataSet.VExpenses.FindByBRIDEXID(CType(obj.GetRow(Calendar.Storage), DataRowView)("BRID"), CType(obj.GetRow(Calendar.Storage), DataRowView)("EXID")).EXName
            Else
                If AddAppointmentLock(obj.GetRow(Calendar.Storage)("APID")) Then
                    CType(obj.GetRow(Calendar.Storage), DataRowView)("TAName") = Calendar.DataSet.Tasks.FindByTAID(CType(obj.GetRow(Calendar.Storage), DataRowView)("APTask")).TAName
                    CType(obj.GetRow(Calendar.Storage), DataRowView)("EXName") = Calendar.DataSet.VExpenses.FindByBRIDEXID(CType(obj.GetRow(Calendar.Storage), DataRowView)("BRID"), CType(obj.GetRow(Calendar.Storage), DataRowView)("EXID")).EXName
                Else
                    Message.CurrentlyAccessed("appointment")
                    CType(obj.GetRow(Calendar.Storage), DataRow).RejectChanges()
                End If
            End If
        Next
    End Sub

    Private Sub Calendar_AppointmentsDeleted(ByVal sender As Object, ByVal e As DevExpress.XtraScheduler.PersistentObjectsEventArgs) Handles Calendar.AppointmentsDeleted
        If HasRole("branch_read_only") Then Exit Sub
        Dim DeletedRows As New ArrayList
        For Each obj As System.Data.DataRow In Calendar.DsCalendar.VAppointments.Rows
            If obj.RowState = DataRowState.Deleted Then
                DeletedRows.Add(obj)
            End If
        Next
        For Each obj As System.Data.DataRow In DeletedRows
            obj.RejectChanges()
            Dim APID As Long = obj("APID")
            'If Not (obj("BRID") = BRID And obj("APType") = "NO" And obj("APTypeID") = 0) Then
            '   If Message.AppointmentNotCurrentObject("quote request") = MsgBoxResult.No Then
            '        'obj.RejectChanges()
            '    End If
            'End If
            If obj.RowState = DataRowState.Added Then
                obj.Delete()
            Else
                If AddAppointmentLock(APID) Then
                    obj.Delete()
                Else
                    Message.CurrentlyAccessed("appointment")
                End If
            End If
        Next
    End Sub

    Private hEditedAppointmentList As New ArrayList
    Public Property EditedAppointmentList() As ArrayList
        Get
            Return hEditedAppointmentList
        End Get
        Set(ByVal Value As ArrayList)
            hEditedAppointmentList = Value
        End Set
    End Property

    Private Function AddAppointmentLock(ByVal APID As Long) As Boolean
        If Not IsInEditList(APID) Then
            If DataAccess.spExecLockRequest("sp_GetAppointmentLock", BRID, APID, SqlConnection) Then
                EditedAppointmentList.Add(APID)
                Return True
            Else
                Return False
            End If
        Else
            Return True
        End If
    End Function

    Private Function IsInEditList(ByVal APID As Long) As Boolean
        For Each lng As Long In EditedAppointmentList
            If lng = APID Then
                Return True
            End If
        Next
        Return False
    End Function

    Private Sub ReleaseAppointmentLocks()
        For Each lng As Long In EditedAppointmentList
            DataAccess.spExecLockRequest("sp_ReleaseAppointmentLock", BRID, lng, SqlConnection)
        Next
    End Sub

#End Region

    Private Sub DateFilter_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles beiFromDate.EditValueChanged, beiToDate.EditValueChanged
        Try
            UpdateDateFilter()
        Catch ex As CouldNotUpdateDateFilterException
            DateFilterIsListening = False
            beiFromDate.EditValue = ex.OriginalFromDate
            beiToDate.EditValue = ex.OriginalToDate
            DateFilterIsListening = True
        End Try
    End Sub

    Private DateFilterIsListening As Boolean = False ' This is so this doesn't trigger during load (this will happen in code)
    Private Sub UpdateDateFilter()
        If DateFilterIsListening Then
            If TypeOf parentRowForm Is frmBooking2 Then
                CType(parentRowForm, frmBooking2).UpdateDateFilter(IsNull(beiFromDate.EditValue, DBNull.Value), IsNull(beiToDate.EditValue, DBNull.Value))
            ElseIf TypeOf parentRowForm Is frmLead2 Then
                CType(parentRowForm, frmLead2).UpdateDateFilter(IsNull(beiFromDate.EditValue, DBNull.Value), IsNull(beiToDate.EditValue, DBNull.Value))
            End If
        End If
    End Sub

    Private Sub btnShowAllDates_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnShowAllDates.ItemClick
        If (Not beiFromDate.EditValue Is Nothing) Or (Not beiToDate.EditValue Is Nothing) Then
            DateFilterIsListening = False
            beiFromDate.EditValue = Nothing
            beiToDate.EditValue = Nothing
            UpdateDateFilter()
            DateFilterIsListening = True
        End If
    End Sub

End Class
