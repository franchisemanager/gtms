Public Class frmBookingAppliance
    Inherits DevExpress.XtraEditors.XtraForm

    Private DataRow As DataRow
    Private IsNew As Boolean = False
    Public BRID As Int32
    Public JBID As Int64
    Public NIID As Int64

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return SqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            SqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private hTransaction As SqlClient.SqlTransaction
    Private Property Transaction() As SqlClient.SqlTransaction
        Get
            Return hTransaction
        End Get
        Set(ByVal Value As SqlClient.SqlTransaction)
            hTransaction = Value
            Power.Library.Library.ApplyTransactionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Public Shared Function Add(ByVal BRID As Integer, ByVal JBID As Int64, ByVal Transaction As SqlClient.SqlTransaction) As frmBookingAppliance
        Dim gui As New frmBookingAppliance

        With gui
            .Transaction = Transaction
            .IsNew = True

            .BRID = BRID
            .NIID = spNew_NonCoreSalesItem(BRID, JBID, "AP", Transaction)
            .FillPreliminaryData()

            .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = .BRID
            .SqlDataAdapter.SelectCommand.Parameters("@NIID").Value = .NIID
            .SqlDataAdapter.Fill(.DsGTMS)
            .DataRow = .DsGTMS.JobsNonCoreSalesItems(0)
            .JBID = .DataRow("JBID")

            .FillData()
        End With

        Return gui
    End Function

    Public Shared Function Edit(ByVal BRID As Integer, ByVal NIID As Int64, ByVal Transaction As SqlClient.SqlTransaction) As frmBookingAppliance
        Dim gui As New frmBookingAppliance

        With gui
            .Transaction = Transaction

            .BRID = BRID
            .NIID = NIID
            .FillPreliminaryData()

            .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = BRID
            .SqlDataAdapter.SelectCommand.Parameters("@NIID").Value = NIID
            .SqlDataAdapter.Fill(.DsGTMS)
            .DataRow = .DsGTMS.JobsNonCoreSalesItems(0)
            .JBID = .DataRow("JBID")

            .FillData()
        End With

        Return gui
    End Function

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()
        ReadMRU(txtNISupplier, UserAppDataPath)
        ReadMRU(txtNIUserType, UserAppDataPath)
        ReadMRU(txtNIBrand, UserAppDataPath)


        'Add any initialization after the InitializeComponent() call
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DsGTMS As WindowsApplication.dsGTMS
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents dtNIDeliveryDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents rdNIDeliveredToClient As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents rdNIPaidByClient As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents rdNIOrderedByClient As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents txtNIName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNIDesc As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents XtraTabPage4 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtNIPrice As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNISupplier As DevExpress.XtraEditors.MRUEdit
    Friend WithEvents txtNIBrand As DevExpress.XtraEditors.MRUEdit
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents txtNIUserType As DevExpress.XtraEditors.MRUEdit
    Friend WithEvents btnHelp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmHistory As System.Windows.Forms.ContextMenu
    Friend WithEvents miClearHistory As System.Windows.Forms.MenuItem
    Friend WithEvents Label12 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmBookingAppliance))
        Me.DsGTMS = New WindowsApplication.dsGTMS
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage
        Me.txtNIDesc = New DevExpress.XtraEditors.MemoEdit
        Me.txtNIName = New DevExpress.XtraEditors.TextEdit
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtNISupplier = New DevExpress.XtraEditors.MRUEdit
        Me.cmHistory = New System.Windows.Forms.ContextMenu
        Me.miClearHistory = New System.Windows.Forms.MenuItem
        Me.txtNIUserType = New DevExpress.XtraEditors.MRUEdit
        Me.txtNIBrand = New DevExpress.XtraEditors.MRUEdit
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage
        Me.rdNIDeliveredToClient = New DevExpress.XtraEditors.RadioGroup
        Me.dtNIDeliveryDate = New DevExpress.XtraEditors.DateEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage
        Me.rdNIOrderedByClient = New DevExpress.XtraEditors.RadioGroup
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.rdNIPaidByClient = New DevExpress.XtraEditors.RadioGroup
        Me.XtraTabPage4 = New DevExpress.XtraTab.XtraTabPage
        Me.Label12 = New System.Windows.Forms.Label
        Me.txtNIPrice = New DevExpress.XtraEditors.TextEdit
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.btnHelp = New DevExpress.XtraEditors.SimpleButton
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.txtNIDesc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNIName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNISupplier.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNIUserType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNIBrand.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.rdNIDeliveredToClient.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtNIDeliveryDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage3.SuspendLayout()
        CType(Me.rdNIOrderedByClient.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rdNIPaidByClient.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage4.SuspendLayout()
        CType(Me.txtNIPrice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DsGTMS
        '
        Me.DsGTMS.DataSetName = "dsGTMS"
        Me.DsGTMS.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "JobsNonCoreSalesItems", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("JBID", "JBID"), New System.Data.Common.DataColumnMapping("NIID", "NIID"), New System.Data.Common.DataColumnMapping("NIName", "NIName"), New System.Data.Common.DataColumnMapping("NIDesc", "NIDesc"), New System.Data.Common.DataColumnMapping("NICost", "NICost"), New System.Data.Common.DataColumnMapping("NIPrice", "NIPrice"), New System.Data.Common.DataColumnMapping("EXIDRep", "EXIDRep"), New System.Data.Common.DataColumnMapping("NICommissionAsPercent", "NICommissionAsPercent"), New System.Data.Common.DataColumnMapping("NICommissionAmount", "NICommissionAmount"), New System.Data.Common.DataColumnMapping("NICommissionPercent", "NICommissionPercent"), New System.Data.Common.DataColumnMapping("NIType", "NIType"), New System.Data.Common.DataColumnMapping("NIUserType", "NIUserType"), New System.Data.Common.DataColumnMapping("NISupplier", "NISupplier"), New System.Data.Common.DataColumnMapping("NIBrand", "NIBrand"), New System.Data.Common.DataColumnMapping("NIPaidByClient", "NIPaidByClient"), New System.Data.Common.DataColumnMapping("NIOrderedByClient", "NIOrderedByClient"), New System.Data.Common.DataColumnMapping("NIDeliveredToClient", "NIDeliveredToClient"), New System.Data.Common.DataColumnMapping("NIDeliveryDate", "NIDeliveryDate")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM JobsNonCoreSalesItems WHERE (BRID = @Original_BRID) AND (NIID = @Orig" & _
        "inal_NIID) AND (EXIDRep = @Original_EXIDRep OR @Original_EXIDRep IS NULL AND EXI" & _
        "DRep IS NULL) AND (JBID = @Original_JBID) AND (NIBrand = @Original_NIBrand OR @O" & _
        "riginal_NIBrand IS NULL AND NIBrand IS NULL) AND (NICommissionAmount = @Original" & _
        "_NICommissionAmount OR @Original_NICommissionAmount IS NULL AND NICommissionAmou" & _
        "nt IS NULL) AND (NICommissionAsPercent = @Original_NICommissionAsPercent OR @Ori" & _
        "ginal_NICommissionAsPercent IS NULL AND NICommissionAsPercent IS NULL) AND (NICo" & _
        "mmissionPercent = @Original_NICommissionPercent OR @Original_NICommissionPercent" & _
        " IS NULL AND NICommissionPercent IS NULL) AND (NICost = @Original_NICost OR @Ori" & _
        "ginal_NICost IS NULL AND NICost IS NULL) AND (NIDeliveredToClient = @Original_NI" & _
        "DeliveredToClient OR @Original_NIDeliveredToClient IS NULL AND NIDeliveredToClie" & _
        "nt IS NULL) AND (NIDeliveryDate = @Original_NIDeliveryDate OR @Original_NIDelive" & _
        "ryDate IS NULL AND NIDeliveryDate IS NULL) AND (NIDesc = @Original_NIDesc OR @Or" & _
        "iginal_NIDesc IS NULL AND NIDesc IS NULL) AND (NIName = @Original_NIName OR @Ori" & _
        "ginal_NIName IS NULL AND NIName IS NULL) AND (NIOrderedByClient = @Original_NIOr" & _
        "deredByClient OR @Original_NIOrderedByClient IS NULL AND NIOrderedByClient IS NU" & _
        "LL) AND (NIPaidByClient = @Original_NIPaidByClient OR @Original_NIPaidByClient I" & _
        "S NULL AND NIPaidByClient IS NULL) AND (NIPrice = @Original_NIPrice OR @Original" & _
        "_NIPrice IS NULL AND NIPrice IS NULL) AND (NISupplier = @Original_NISupplier OR " & _
        "@Original_NISupplier IS NULL AND NISupplier IS NULL) AND (NIType = @Original_NIT" & _
        "ype) AND (NIUserType = @Original_NIUserType OR @Original_NIUserType IS NULL AND " & _
        "NIUserType IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDRep", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDRep", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIBrand", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIBrand", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICommissionAmount", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NICommissionAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICommissionAsPercent", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NICommissionAsPercent", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICommissionPercent", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "NICommissionPercent", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NICost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIDeliveredToClient", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIDeliveredToClient", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIDeliveryDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIDeliveryDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIDesc", System.Data.SqlDbType.VarChar, 2000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIDesc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIOrderedByClient", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIOrderedByClient", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIPaidByClient", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIPaidByClient", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIPrice", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIPrice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NISupplier", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NISupplier", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIUserType", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIUserType", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=WORKSTATION3;packet size=4096;integrated security=SSPI;data source" & _
        "=""server\dev"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO JobsNonCoreSalesItems(BRID, JBID, NIName, NIDesc, NICost, NIPrice, EX" & _
        "IDRep, NICommissionAsPercent, NICommissionAmount, NICommissionPercent, NIType, N" & _
        "IUserType, NISupplier, NIBrand, NIPaidByClient, NIOrderedByClient, NIDeliveredTo" & _
        "Client, NIDeliveryDate) VALUES (@BRID, @JBID, @NIName, @NIDesc, @NICost, @NIPric" & _
        "e, @EXIDRep, @NICommissionAsPercent, @NICommissionAmount, @NICommissionPercent, " & _
        "@NIType, @NIUserType, @NISupplier, @NIBrand, @NIPaidByClient, @NIOrderedByClient" & _
        ", @NIDeliveredToClient, @NIDeliveryDate); SELECT BRID, JBID, NIID, NIName, NIDes" & _
        "c, NICost, NIPrice, EXIDRep, NICommissionAsPercent, NICommissionAmount, NICommis" & _
        "sionPercent, NIType, NIUserType, NISupplier, NIBrand, NIPaidByClient, NIOrderedB" & _
        "yClient, NIDeliveredToClient, NIDeliveryDate FROM JobsNonCoreSalesItems WHERE (B" & _
        "RID = @BRID) AND (NIID = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIName", System.Data.SqlDbType.VarChar, 50, "NIName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIDesc", System.Data.SqlDbType.VarChar, 2000, "NIDesc"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICost", System.Data.SqlDbType.Money, 8, "NICost"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIPrice", System.Data.SqlDbType.Money, 8, "NIPrice"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDRep", System.Data.SqlDbType.Int, 4, "EXIDRep"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICommissionAsPercent", System.Data.SqlDbType.Bit, 1, "NICommissionAsPercent"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICommissionAmount", System.Data.SqlDbType.Money, 8, "NICommissionAmount"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICommissionPercent", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "NICommissionPercent", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIType", System.Data.SqlDbType.VarChar, 2, "NIType"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIUserType", System.Data.SqlDbType.VarChar, 50, "NIUserType"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NISupplier", System.Data.SqlDbType.VarChar, 50, "NISupplier"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIBrand", System.Data.SqlDbType.VarChar, 50, "NIBrand"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIPaidByClient", System.Data.SqlDbType.Bit, 1, "NIPaidByClient"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIOrderedByClient", System.Data.SqlDbType.Bit, 1, "NIOrderedByClient"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIDeliveredToClient", System.Data.SqlDbType.Bit, 1, "NIDeliveredToClient"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIDeliveryDate", System.Data.SqlDbType.DateTime, 8, "NIDeliveryDate"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, JBID, NIID, NIName, NIDesc, NICost, NIPrice, EXIDRep, NICommissionAs" & _
        "Percent, NICommissionAmount, NICommissionPercent, NIType, NIUserType, NISupplier" & _
        ", NIBrand, NIPaidByClient, NIOrderedByClient, NIDeliveredToClient, NIDeliveryDat" & _
        "e FROM JobsNonCoreSalesItems WHERE (BRID = @BRID) AND (NIID = @NIID)"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIID", System.Data.SqlDbType.BigInt, 8, "NIID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE JobsNonCoreSalesItems SET BRID = @BRID, JBID = @JBID, NIName = @NIName, NI" & _
        "Desc = @NIDesc, NICost = @NICost, NIPrice = @NIPrice, EXIDRep = @EXIDRep, NIComm" & _
        "issionAsPercent = @NICommissionAsPercent, NICommissionAmount = @NICommissionAmou" & _
        "nt, NICommissionPercent = @NICommissionPercent, NIType = @NIType, NIUserType = @" & _
        "NIUserType, NISupplier = @NISupplier, NIBrand = @NIBrand, NIPaidByClient = @NIPa" & _
        "idByClient, NIOrderedByClient = @NIOrderedByClient, NIDeliveredToClient = @NIDel" & _
        "iveredToClient, NIDeliveryDate = @NIDeliveryDate WHERE (BRID = @Original_BRID) A" & _
        "ND (NIID = @Original_NIID) AND (EXIDRep = @Original_EXIDRep OR @Original_EXIDRep" & _
        " IS NULL AND EXIDRep IS NULL) AND (JBID = @Original_JBID) AND (NIBrand = @Origin" & _
        "al_NIBrand OR @Original_NIBrand IS NULL AND NIBrand IS NULL) AND (NICommissionAm" & _
        "ount = @Original_NICommissionAmount OR @Original_NICommissionAmount IS NULL AND " & _
        "NICommissionAmount IS NULL) AND (NICommissionAsPercent = @Original_NICommissionA" & _
        "sPercent OR @Original_NICommissionAsPercent IS NULL AND NICommissionAsPercent IS" & _
        " NULL) AND (NICommissionPercent = @Original_NICommissionPercent OR @Original_NIC" & _
        "ommissionPercent IS NULL AND NICommissionPercent IS NULL) AND (NICost = @Origina" & _
        "l_NICost OR @Original_NICost IS NULL AND NICost IS NULL) AND (NIDeliveredToClien" & _
        "t = @Original_NIDeliveredToClient OR @Original_NIDeliveredToClient IS NULL AND N" & _
        "IDeliveredToClient IS NULL) AND (NIDeliveryDate = @Original_NIDeliveryDate OR @O" & _
        "riginal_NIDeliveryDate IS NULL AND NIDeliveryDate IS NULL) AND (NIDesc = @Origin" & _
        "al_NIDesc OR @Original_NIDesc IS NULL AND NIDesc IS NULL) AND (NIName = @Origina" & _
        "l_NIName OR @Original_NIName IS NULL AND NIName IS NULL) AND (NIOrderedByClient " & _
        "= @Original_NIOrderedByClient OR @Original_NIOrderedByClient IS NULL AND NIOrder" & _
        "edByClient IS NULL) AND (NIPaidByClient = @Original_NIPaidByClient OR @Original_" & _
        "NIPaidByClient IS NULL AND NIPaidByClient IS NULL) AND (NIPrice = @Original_NIPr" & _
        "ice OR @Original_NIPrice IS NULL AND NIPrice IS NULL) AND (NISupplier = @Origina" & _
        "l_NISupplier OR @Original_NISupplier IS NULL AND NISupplier IS NULL) AND (NIType" & _
        " = @Original_NIType) AND (NIUserType = @Original_NIUserType OR @Original_NIUserT" & _
        "ype IS NULL AND NIUserType IS NULL); SELECT BRID, JBID, NIID, NIName, NIDesc, NI" & _
        "Cost, NIPrice, EXIDRep, NICommissionAsPercent, NICommissionAmount, NICommissionP" & _
        "ercent, NIType, NIUserType, NISupplier, NIBrand, NIPaidByClient, NIOrderedByClie" & _
        "nt, NIDeliveredToClient, NIDeliveryDate FROM JobsNonCoreSalesItems WHERE (BRID =" & _
        " @BRID) AND (NIID = @NIID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIName", System.Data.SqlDbType.VarChar, 50, "NIName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIDesc", System.Data.SqlDbType.VarChar, 2000, "NIDesc"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICost", System.Data.SqlDbType.Money, 8, "NICost"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIPrice", System.Data.SqlDbType.Money, 8, "NIPrice"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDRep", System.Data.SqlDbType.Int, 4, "EXIDRep"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICommissionAsPercent", System.Data.SqlDbType.Bit, 1, "NICommissionAsPercent"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICommissionAmount", System.Data.SqlDbType.Money, 8, "NICommissionAmount"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICommissionPercent", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "NICommissionPercent", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIType", System.Data.SqlDbType.VarChar, 2, "NIType"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIUserType", System.Data.SqlDbType.VarChar, 50, "NIUserType"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NISupplier", System.Data.SqlDbType.VarChar, 50, "NISupplier"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIBrand", System.Data.SqlDbType.VarChar, 50, "NIBrand"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIPaidByClient", System.Data.SqlDbType.Bit, 1, "NIPaidByClient"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIOrderedByClient", System.Data.SqlDbType.Bit, 1, "NIOrderedByClient"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIDeliveredToClient", System.Data.SqlDbType.Bit, 1, "NIDeliveredToClient"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIDeliveryDate", System.Data.SqlDbType.DateTime, 8, "NIDeliveryDate"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDRep", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDRep", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIBrand", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIBrand", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICommissionAmount", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NICommissionAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICommissionAsPercent", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NICommissionAsPercent", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICommissionPercent", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "NICommissionPercent", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NICost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIDeliveredToClient", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIDeliveredToClient", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIDeliveryDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIDeliveryDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIDesc", System.Data.SqlDbType.VarChar, 2000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIDesc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIOrderedByClient", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIOrderedByClient", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIPaidByClient", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIPaidByClient", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIPrice", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIPrice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NISupplier", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NISupplier", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIUserType", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIUserType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIID", System.Data.SqlDbType.BigInt, 8, "NIID"))
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;integrated security=SSPI;data source=SERVER;" & _
        "persist security info=False;initial catalog=GTMS_DEV"
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.XtraTabControl1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XtraTabControl1.Location = New System.Drawing.Point(8, 8)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(456, 272)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.XtraTabPage3, Me.XtraTabPage4})
        Me.XtraTabControl1.Text = "Appliance Details"
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.txtNIDesc)
        Me.XtraTabPage1.Controls.Add(Me.txtNIName)
        Me.XtraTabPage1.Controls.Add(Me.Label1)
        Me.XtraTabPage1.Controls.Add(Me.Label2)
        Me.XtraTabPage1.Controls.Add(Me.Label5)
        Me.XtraTabPage1.Controls.Add(Me.Label6)
        Me.XtraTabPage1.Controls.Add(Me.Label7)
        Me.XtraTabPage1.Controls.Add(Me.txtNISupplier)
        Me.XtraTabPage1.Controls.Add(Me.txtNIUserType)
        Me.XtraTabPage1.Controls.Add(Me.txtNIBrand)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(450, 246)
        Me.XtraTabPage1.Text = "Appliance Details"
        '
        'txtNIDesc
        '
        Me.txtNIDesc.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "JobsNonCoreSalesItems.NIDesc"))
        Me.txtNIDesc.EditValue = "MemoEdit1"
        Me.txtNIDesc.Location = New System.Drawing.Point(112, 144)
        Me.txtNIDesc.Name = "txtNIDesc"
        Me.txtNIDesc.Size = New System.Drawing.Size(296, 88)
        Me.txtNIDesc.TabIndex = 9
        '
        'txtNIName
        '
        Me.txtNIName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "JobsNonCoreSalesItems.NIName"))
        Me.txtNIName.EditValue = "TextEdit1"
        Me.txtNIName.Location = New System.Drawing.Point(112, 112)
        Me.txtNIName.Name = "txtNIName"
        Me.txtNIName.Size = New System.Drawing.Size(296, 20)
        Me.txtNIName.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(24, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 21)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Supplier:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(24, 144)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 21)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Description:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(24, 48)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 21)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Type:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(24, 80)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(40, 21)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Brand:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(24, 112)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(88, 21)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Model Number:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNISupplier
        '
        Me.txtNISupplier.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "JobsNonCoreSalesItems.NISupplier"))
        Me.txtNISupplier.EditValue = ""
        Me.txtNISupplier.Location = New System.Drawing.Point(112, 16)
        Me.txtNISupplier.Name = "txtNISupplier"
        '
        'txtNISupplier.Properties
        '
        Me.txtNISupplier.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtNISupplier.Properties.ContextMenu = Me.cmHistory
        Me.txtNISupplier.Properties.MaxItemCount = 15
        Me.txtNISupplier.Size = New System.Drawing.Size(144, 20)
        Me.txtNISupplier.TabIndex = 1
        '
        'cmHistory
        '
        Me.cmHistory.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.miClearHistory})
        '
        'miClearHistory
        '
        Me.miClearHistory.Index = 0
        Me.miClearHistory.Text = "Clear History"
        '
        'txtNIUserType
        '
        Me.txtNIUserType.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "JobsNonCoreSalesItems.NIUserType"))
        Me.txtNIUserType.EditValue = ""
        Me.txtNIUserType.Location = New System.Drawing.Point(112, 48)
        Me.txtNIUserType.Name = "txtNIUserType"
        '
        'txtNIUserType.Properties
        '
        Me.txtNIUserType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtNIUserType.Properties.ContextMenu = Me.cmHistory
        Me.txtNIUserType.Size = New System.Drawing.Size(144, 20)
        Me.txtNIUserType.TabIndex = 3
        '
        'txtNIBrand
        '
        Me.txtNIBrand.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "JobsNonCoreSalesItems.NIBrand"))
        Me.txtNIBrand.EditValue = ""
        Me.txtNIBrand.Location = New System.Drawing.Point(112, 80)
        Me.txtNIBrand.Name = "txtNIBrand"
        '
        'txtNIBrand.Properties
        '
        Me.txtNIBrand.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtNIBrand.Properties.ContextMenu = Me.cmHistory
        Me.txtNIBrand.Size = New System.Drawing.Size(144, 20)
        Me.txtNIBrand.TabIndex = 5
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.rdNIDeliveredToClient)
        Me.XtraTabPage2.Controls.Add(Me.dtNIDeliveryDate)
        Me.XtraTabPage2.Controls.Add(Me.Label3)
        Me.XtraTabPage2.Controls.Add(Me.Label4)
        Me.XtraTabPage2.Controls.Add(Me.Label10)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(450, 246)
        Me.XtraTabPage2.Text = "Delivery"
        '
        'rdNIDeliveredToClient
        '
        Me.rdNIDeliveredToClient.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "JobsNonCoreSalesItems.NIDeliveredToClient"))
        Me.rdNIDeliveredToClient.Location = New System.Drawing.Point(104, 24)
        Me.rdNIDeliveredToClient.Name = "rdNIDeliveredToClient"
        '
        'rdNIDeliveredToClient.Properties
        '
        Me.rdNIDeliveredToClient.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.rdNIDeliveredToClient.Properties.Appearance.Options.UseBackColor = True
        Me.rdNIDeliveredToClient.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.rdNIDeliveredToClient.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "Granite Transformations"), New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "Customer (Job Site)")})
        Me.rdNIDeliveredToClient.Size = New System.Drawing.Size(152, 48)
        Me.rdNIDeliveredToClient.TabIndex = 1
        '
        'dtNIDeliveryDate
        '
        Me.dtNIDeliveryDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "JobsNonCoreSalesItems.NIDeliveryDate"))
        Me.dtNIDeliveryDate.EditValue = New Date(2005, 7, 20, 0, 0, 0, 0)
        Me.dtNIDeliveryDate.Location = New System.Drawing.Point(96, 120)
        Me.dtNIDeliveryDate.Name = "dtNIDeliveryDate"
        '
        'dtNIDeliveryDate.Properties
        '
        Me.dtNIDeliveryDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtNIDeliveryDate.Size = New System.Drawing.Size(168, 20)
        Me.dtNIDeliveryDate.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(16, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 21)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Deliver to:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(16, 120)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(88, 21)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Delivered date:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(16, 96)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(256, 21)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "Select the date that delivery is requried by"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.rdNIOrderedByClient)
        Me.XtraTabPage3.Controls.Add(Me.Label8)
        Me.XtraTabPage3.Controls.Add(Me.Label9)
        Me.XtraTabPage3.Controls.Add(Me.rdNIPaidByClient)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(450, 246)
        Me.XtraTabPage3.Text = "Ordering and Payment"
        '
        'rdNIOrderedByClient
        '
        Me.rdNIOrderedByClient.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "JobsNonCoreSalesItems.NIOrderedByClient"))
        Me.rdNIOrderedByClient.Location = New System.Drawing.Point(64, 48)
        Me.rdNIOrderedByClient.Name = "rdNIOrderedByClient"
        '
        'rdNIOrderedByClient.Properties
        '
        Me.rdNIOrderedByClient.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.rdNIOrderedByClient.Properties.Appearance.Options.UseBackColor = True
        Me.rdNIOrderedByClient.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.rdNIOrderedByClient.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "Granite Transformations"), New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "Customer")})
        Me.rdNIOrderedByClient.Size = New System.Drawing.Size(144, 48)
        Me.rdNIOrderedByClient.TabIndex = 1
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(16, 24)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(368, 21)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "The responsibility for ordering this appliance will be undertaken by:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(16, 112)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(368, 21)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "This appliance will be invoiced to and paid for by:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rdNIPaidByClient
        '
        Me.rdNIPaidByClient.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "JobsNonCoreSalesItems.NIPaidByClient"))
        Me.rdNIPaidByClient.Location = New System.Drawing.Point(64, 136)
        Me.rdNIPaidByClient.Name = "rdNIPaidByClient"
        '
        'rdNIPaidByClient.Properties
        '
        Me.rdNIPaidByClient.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.rdNIPaidByClient.Properties.Appearance.Options.UseBackColor = True
        Me.rdNIPaidByClient.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.rdNIPaidByClient.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "Granite Transformations"), New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "Customer")})
        Me.rdNIPaidByClient.Size = New System.Drawing.Size(144, 48)
        Me.rdNIPaidByClient.TabIndex = 3
        '
        'XtraTabPage4
        '
        Me.XtraTabPage4.Controls.Add(Me.Label12)
        Me.XtraTabPage4.Controls.Add(Me.txtNIPrice)
        Me.XtraTabPage4.Controls.Add(Me.Label11)
        Me.XtraTabPage4.Controls.Add(Me.Label13)
        Me.XtraTabPage4.Name = "XtraTabPage4"
        Me.XtraTabPage4.Size = New System.Drawing.Size(450, 246)
        Me.XtraTabPage4.Text = "Pricing and Costing"
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(48, 136)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(360, 80)
        Me.Label12.TabIndex = 3
        Me.Label12.Text = "NOTE: The Sale Price entered here will be linked to this part of the Job Financia" & _
        "ls section and will therefore be included in the calculation that determines wha" & _
        "t the customer owes Granite Transformations. If there was a need to change this fi" & _
        "gure once information had progressed to the Accounting stage then this could be " & _
        "done in the Accounting section."
        '
        'txtNIPrice
        '
        Me.txtNIPrice.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "JobsNonCoreSalesItems.NIPrice"))
        Me.txtNIPrice.EditValue = ""
        Me.txtNIPrice.Location = New System.Drawing.Point(112, 96)
        Me.txtNIPrice.Name = "txtNIPrice"
        '
        'txtNIPrice.Properties
        '
        Me.txtNIPrice.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNIPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtNIPrice.Properties.DisplayFormat.FormatString = "c"
        Me.txtNIPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtNIPrice.Properties.EditFormat.FormatString = "c"
        Me.txtNIPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtNIPrice.Size = New System.Drawing.Size(184, 20)
        Me.txtNIPrice.TabIndex = 2
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(48, 96)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(64, 16)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "Sale Price:"
        '
        'Label13
        '
        Me.Label13.Location = New System.Drawing.Point(48, 16)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(360, 72)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "This amount may be entered in the Administration (Job Bookings) section if this f" & _
        "igure is known at this stage. Alternatively it can be entered in the Accounting " & _
        "(Job Financials) section. There will be no need to enter any amount if the Appli" & _
        "ance will invoiced to paid for by the customer."
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(392, 288)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(312, 288)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "OK"
        '
        'btnHelp
        '
        Me.btnHelp.Image = CType(resources.GetObject("btnHelp.Image"), System.Drawing.Image)
        Me.btnHelp.Location = New System.Drawing.Point(8, 288)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(72, 24)
        Me.btnHelp.TabIndex = 1
        Me.btnHelp.Text = "Help"
        '
        'frmBookingAppliance
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(474, 320)
        Me.Controls.Add(Me.btnHelp)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBookingAppliance"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Appliance"
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.txtNIDesc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNIName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNISupplier.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNIUserType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNIBrand.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.rdNIDeliveredToClient.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtNIDeliveryDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage3.ResumeLayout(False)
        CType(Me.rdNIOrderedByClient.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rdNIPaidByClient.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage4.ResumeLayout(False)
        CType(Me.txtNIPrice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmJobAppliance_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
    End Sub

    Private Sub FillPreliminaryData()
    End Sub

    Private Sub FillData()
    End Sub

    Private Sub EnableDisable()
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        DataRow.EndEdit()
        SqlDataAdapter.Update(DsGTMS)
        OK = True
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        WriteMRU(txtNISupplier, UserAppDataPath)
        WriteMRU(txtNIUserType, UserAppDataPath)
        WriteMRU(txtNIBrand, UserAppDataPath)

        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            DataRow.EndEdit()
            If DsGTMS.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If response = MsgBoxResult.Yes Then
                If IsNew Then
                    DataRow.Delete()
                    SqlDataAdapter.Update(DsGTMS)
                End If
            Else
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub miClearHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miClearHistory.Click
        ClearHistory(sender, e)
    End Sub

    Private Sub txtNIPrice_ParseEditValue(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) Handles txtNIPrice.ParseEditValue
        Format.Decimal_ParseEditValue(sender, e)
    End Sub

    Private Sub Date_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) _
    Handles dtNIDeliveryDate.Validating
        Library.DateEdit_Validating(sender, e)
    End Sub

End Class
