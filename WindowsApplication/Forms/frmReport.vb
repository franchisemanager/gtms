Imports CrystalDecisions.CrystalReports.Engine

Public Class frmReport
    Inherits DevExpress.XtraEditors.XtraForm

    Public oRpt As ReportClass = New ReportClass
    Private HelpTopic As String = Nothing

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal HelpTopic As String)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        'If MyApp.IS_TRIAL Then
        'MyApp.MsgBoxTrial(modMain.ETrailFunction.VIEW_REPORT)
        'End If

        Me.HelpTopic = HelpTopic
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents CRV As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents btnFirst As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPrevious As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnNext As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnLast As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnSave As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarAndDockingController1 As DevExpress.XtraBars.BarAndDockingController
    Friend WithEvents btnHelp As DevExpress.XtraBars.BarButtonItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmReport))
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.CRV = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.BarManager1 = New DevExpress.XtraBars.BarManager
        Me.Bar1 = New DevExpress.XtraBars.Bar
        Me.btnFirst = New DevExpress.XtraBars.BarButtonItem
        Me.btnPrevious = New DevExpress.XtraBars.BarButtonItem
        Me.btnNext = New DevExpress.XtraBars.BarButtonItem
        Me.btnLast = New DevExpress.XtraBars.BarButtonItem
        Me.btnSave = New DevExpress.XtraBars.BarButtonItem
        Me.btnPrint = New DevExpress.XtraBars.BarButtonItem
        Me.btnHelp = New DevExpress.XtraBars.BarButtonItem
        Me.BarAndDockingController1 = New DevExpress.XtraBars.BarAndDockingController(Me.components)
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
        Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ImageList1
        '
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'CRV
        '
        Me.CRV.ActiveViewIndex = -1
        Me.CRV.DisplayGroupTree = False
        Me.CRV.DisplayToolbar = False
        Me.CRV.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CRV.Location = New System.Drawing.Point(0, 24)
        Me.CRV.Name = "CRV"
        Me.CRV.ReportSource = Nothing
        Me.CRV.Size = New System.Drawing.Size(776, 518)
        Me.CRV.TabIndex = 2
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager1.Controller = Me.BarAndDockingController1
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Images = Me.ImageList1
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnFirst, Me.btnPrevious, Me.btnNext, Me.btnLast, Me.btnSave, Me.btnPrint, Me.BarSubItem1, Me.btnHelp})
        Me.BarManager1.MaxItemId = 8
        '
        'Bar1
        '
        Me.Bar1.BarName = "Standard"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.FloatLocation = New System.Drawing.Point(38, 163)
        Me.Bar1.FloatSize = New System.Drawing.Size(455, 24)
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnFirst, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnPrevious, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnNext, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnLast, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnSave, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnPrint, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnHelp, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.Bar1.OptionsBar.AllowQuickCustomization = False
        Me.Bar1.OptionsBar.DisableClose = True
        Me.Bar1.Text = "Standard"
        '
        'btnFirst
        '
        Me.btnFirst.Caption = "&First"
        Me.btnFirst.Id = 0
        Me.btnFirst.ImageIndex = 0
        Me.btnFirst.ImageIndexDisabled = 0
        Me.btnFirst.Name = "btnFirst"
        '
        'btnPrevious
        '
        Me.btnPrevious.Caption = "Pr&evious"
        Me.btnPrevious.Id = 1
        Me.btnPrevious.ImageIndex = 1
        Me.btnPrevious.ImageIndexDisabled = 1
        Me.btnPrevious.Name = "btnPrevious"
        '
        'btnNext
        '
        Me.btnNext.Caption = "&Next"
        Me.btnNext.Id = 2
        Me.btnNext.ImageIndex = 2
        Me.btnNext.ImageIndexDisabled = 2
        Me.btnNext.Name = "btnNext"
        '
        'btnLast
        '
        Me.btnLast.Caption = "&Last"
        Me.btnLast.Id = 3
        Me.btnLast.ImageIndex = 5
        Me.btnLast.ImageIndexDisabled = 5
        Me.btnLast.Name = "btnLast"
        '
        'btnSave
        '
        Me.btnSave.Caption = "&Save"
        Me.btnSave.Id = 4
        Me.btnSave.ImageIndex = 4
        Me.btnSave.ImageIndexDisabled = 4
        Me.btnSave.Name = "btnSave"
        '
        'btnPrint
        '
        Me.btnPrint.Caption = "&Print"
        Me.btnPrint.Id = 5
        Me.btnPrint.ImageIndex = 3
        Me.btnPrint.ImageIndexDisabled = 3
        Me.btnPrint.Name = "btnPrint"
        '
        'btnHelp
        '
        Me.btnHelp.Caption = "&Help"
        Me.btnHelp.Id = 7
        Me.btnHelp.ImageIndex = 6
        Me.btnHelp.ImageIndexDisabled = 6
        Me.btnHelp.Name = "btnHelp"
        '
        'BarAndDockingController1
        '
        Me.BarAndDockingController1.PaintStyleName = "OfficeXP"
        '
        'BarSubItem1
        '
        Me.BarSubItem1.Caption = "&File"
        Me.BarSubItem1.Id = 6
        Me.BarSubItem1.Name = "BarSubItem1"
        '
        'frmReport
        '
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(776, 542)
        Me.Controls.Add(Me.CRV)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "View Report"
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public Sub Display(ByVal Report As ReportClass, Optional ByVal HideHeadOfficeOnlySections As Boolean = False)
        CRV.ReportSource = Report
        If HideHeadOfficeOnlySections Then
            HideHeadOfficeOnly(Report.ReportDefinition)
        End If
    End Sub

    Private Sub HideHeadOfficeOnly(ByVal repDef As CrystalDecisions.CrystalReports.Engine.ReportDefinition)
        For i As Int16 = 0 To repDef.Sections.Count - 1
            If repDef.Sections(i).Name Like "HOOnly*" Then
                repDef.Sections(i).SectionFormat.EnableSuppress = True
            End If
        Next
        For i As Int16 = 0 To repDef.ReportObjects.Count - 1
            Dim repObj As ReportObject = repDef.ReportObjects(i)
            Dim subRepObj As SubreportObject
            If TypeOf repObj Is SubreportObject Then
                subRepObj = CType(repObj, SubreportObject)
                HideHeadOfficeOnly(subRepObj.OpenSubreport(subRepObj.SubreportName).ReportDefinition)
            End If
        Next
    End Sub

    Private Sub btnFirst_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnFirst.ItemClick
        CRV.ShowFirstPage()
    End Sub

    Private Sub btnPrevious_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrevious.ItemClick
        CRV.ShowPreviousPage()
    End Sub

    Private Sub btnNext_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnNext.ItemClick
        CRV.ShowNextPage()
    End Sub

    Private Sub btnLast_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnLast.ItemClick
        CRV.ShowLastPage()
    End Sub

    Private Sub btnSave_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnSave.ItemClick
        CRV.ExportReport()
    End Sub

    Private Sub btnPrint_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrint.ItemClick
        CRV.PrintReport()
    End Sub

    Private Sub btnHelp_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHelp.ItemClick
        If HelpTopic Is Nothing Then
            ShowHelpTopic(Me, "Reports.html")
        Else
            ShowHelpTopic(Me, HelpTopic)
        End If
    End Sub
End Class
