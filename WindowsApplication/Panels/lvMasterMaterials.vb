Imports Power.Forms

Public Class lvMasterMaterials
    Inherits System.Windows.Forms.UserControl
    Implements IListControl

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Connection As SqlClient.SqlConnection, ByVal MGID As Integer, ByVal ExplorerForm As frmExplorerHeadOffice)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.Connection = Connection
        Me.MGID = MGID
        hExplorerForm = ExplorerForm
        FillDataSet()
    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents hSqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents GridControl As DevExpress.XtraGrid.GridControl
    Friend WithEvents colMTName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gvMaterials As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gvStockedItems As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents DsMaterialsGrid As WindowsApplication.dsMaterialsGrid
    Friend WithEvents colSIName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSICurrentInventory As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMTStocked As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSICurrentValue As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSICurrentPurchaseCost As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSICurrentInventoryInPrimaryUOM As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.gvStockedItems = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colSIName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSICurrentPurchaseCost = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSICurrentInventory = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSICurrentValue = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSICurrentInventoryInPrimaryUOM = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridControl = New DevExpress.XtraGrid.GridControl
        Me.DsMaterialsGrid = New WindowsApplication.dsMaterialsGrid
        Me.gvMaterials = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colMTName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colMTStocked = New DevExpress.XtraGrid.Columns.GridColumn
        Me.hSqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        CType(Me.gvStockedItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsMaterialsGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvMaterials, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gvStockedItems
        '
        Me.gvStockedItems.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.gvStockedItems.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvStockedItems.Appearance.FocusedCell.Options.UseBackColor = True
        Me.gvStockedItems.Appearance.FocusedCell.Options.UseForeColor = True
        Me.gvStockedItems.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.gvStockedItems.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gvStockedItems.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvStockedItems.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvStockedItems.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSIName, Me.colSICurrentPurchaseCost, Me.colSICurrentInventory, Me.colSICurrentValue, Me.colSICurrentInventoryInPrimaryUOM})
        Me.gvStockedItems.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.gvStockedItems.GridControl = Me.GridControl
        Me.gvStockedItems.Name = "gvStockedItems"
        Me.gvStockedItems.OptionsBehavior.Editable = False
        Me.gvStockedItems.OptionsCustomization.AllowFilter = False
        Me.gvStockedItems.OptionsNavigation.AutoFocusNewRow = True
        Me.gvStockedItems.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.gvStockedItems.OptionsView.ShowFooter = True
        Me.gvStockedItems.OptionsView.ShowGroupPanel = False
        Me.gvStockedItems.OptionsView.ShowHorzLines = False
        Me.gvStockedItems.OptionsView.ShowIndicator = False
        Me.gvStockedItems.OptionsView.ShowVertLines = False
        Me.gvStockedItems.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colSIName, DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.gvStockedItems.ViewCaption = "Caption"
        '
        'colSIName
        '
        Me.colSIName.Caption = "Stocked Item"
        Me.colSIName.FieldName = "SIName"
        Me.colSIName.Name = "colSIName"
        Me.colSIName.Visible = True
        Me.colSIName.VisibleIndex = 0
        Me.colSIName.Width = 359
        '
        'colSICurrentPurchaseCost
        '
        Me.colSICurrentPurchaseCost.Caption = "Cost (per Inventory Unit of Measure)"
        Me.colSICurrentPurchaseCost.DisplayFormat.FormatString = "c"
        Me.colSICurrentPurchaseCost.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colSICurrentPurchaseCost.FieldName = "SICurrentPurchaseCost"
        Me.colSICurrentPurchaseCost.Name = "colSICurrentPurchaseCost"
        Me.colSICurrentPurchaseCost.Visible = True
        Me.colSICurrentPurchaseCost.VisibleIndex = 1
        Me.colSICurrentPurchaseCost.Width = 115
        '
        'colSICurrentInventory
        '
        Me.colSICurrentInventory.Caption = "Inventory (Inventory Unit of Measure)"
        Me.colSICurrentInventory.DisplayFormat.FormatString = "#0 Sheet(s)"
        Me.colSICurrentInventory.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colSICurrentInventory.FieldName = "SICurrentInventory"
        Me.colSICurrentInventory.Name = "colSICurrentInventory"
        Me.colSICurrentInventory.SummaryItem.DisplayFormat = "Total: {0} Sheet(s)"
        Me.colSICurrentInventory.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.colSICurrentInventory.Visible = True
        Me.colSICurrentInventory.VisibleIndex = 2
        Me.colSICurrentInventory.Width = 120
        '
        'colSICurrentValue
        '
        Me.colSICurrentValue.Caption = "Current Value"
        Me.colSICurrentValue.DisplayFormat.FormatString = "c"
        Me.colSICurrentValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colSICurrentValue.FieldName = "SICurrentValue"
        Me.colSICurrentValue.Name = "colSICurrentValue"
        Me.colSICurrentValue.SummaryItem.DisplayFormat = "Total: {0:c}"
        Me.colSICurrentValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.colSICurrentValue.Visible = True
        Me.colSICurrentValue.VisibleIndex = 4
        Me.colSICurrentValue.Width = 139
        '
        'colSICurrentInventoryInPrimaryUOM
        '
        Me.colSICurrentInventoryInPrimaryUOM.Caption = "Inventory (Primary Unit of Measure)"
        Me.colSICurrentInventoryInPrimaryUOM.DisplayFormat.FormatString = "#0.000"
        Me.colSICurrentInventoryInPrimaryUOM.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colSICurrentInventoryInPrimaryUOM.FieldName = "SICurrentInventoryInPrimaryUOM"
        Me.colSICurrentInventoryInPrimaryUOM.Name = "colSICurrentInventoryInPrimaryUOM"
        Me.colSICurrentInventoryInPrimaryUOM.SummaryItem.DisplayFormat = "Total: {0}"
        Me.colSICurrentInventoryInPrimaryUOM.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.colSICurrentInventoryInPrimaryUOM.Visible = True
        Me.colSICurrentInventoryInPrimaryUOM.VisibleIndex = 3
        Me.colSICurrentInventoryInPrimaryUOM.Width = 118
        '
        'GridControl
        '
        Me.GridControl.DataMember = "VMaster_Materials_Display"
        Me.GridControl.DataSource = Me.DsMaterialsGrid
        Me.GridControl.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'GridControl.EmbeddedNavigator
        '
        Me.GridControl.EmbeddedNavigator.Name = ""
        Me.GridControl.Location = New System.Drawing.Point(0, 0)
        Me.GridControl.MainView = Me.gvMaterials
        Me.GridControl.Name = "GridControl"
        Me.GridControl.Size = New System.Drawing.Size(632, 392)
        Me.GridControl.TabIndex = 0
        Me.GridControl.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvMaterials, Me.gvStockedItems})
        '
        'DsMaterialsGrid
        '
        Me.DsMaterialsGrid.DataSetName = "dsMaterialsGrid"
        Me.DsMaterialsGrid.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'gvMaterials
        '
        Me.gvMaterials.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.gvMaterials.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvMaterials.Appearance.FocusedCell.Options.UseBackColor = True
        Me.gvMaterials.Appearance.FocusedCell.Options.UseForeColor = True
        Me.gvMaterials.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.gvMaterials.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gvMaterials.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvMaterials.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvMaterials.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colMTName, Me.colMTStocked})
        Me.gvMaterials.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.gvMaterials.GridControl = Me.GridControl
        Me.gvMaterials.Name = "gvMaterials"
        Me.gvMaterials.OptionsBehavior.Editable = False
        Me.gvMaterials.OptionsCustomization.AllowFilter = False
        Me.gvMaterials.OptionsNavigation.AutoFocusNewRow = True
        Me.gvMaterials.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.gvMaterials.OptionsView.ShowGroupPanel = False
        Me.gvMaterials.OptionsView.ShowHorzLines = False
        Me.gvMaterials.OptionsView.ShowIndicator = False
        Me.gvMaterials.OptionsView.ShowVertLines = False
        Me.gvMaterials.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colMTName, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colMTName
        '
        Me.colMTName.Caption = "Name"
        Me.colMTName.FieldName = "MMTName"
        Me.colMTName.Name = "colMTName"
        Me.colMTName.Visible = True
        Me.colMTName.VisibleIndex = 0
        Me.colMTName.Width = 233
        '
        'colMTStocked
        '
        Me.colMTStocked.Caption = "Stocked"
        Me.colMTStocked.FieldName = "MMTStocked_Display"
        Me.colMTStocked.Name = "colMTStocked"
        Me.colMTStocked.Width = 107
        '
        'hSqlConnection
        '
        Me.hSqlConnection.ConnectionString = "packet size=4096;integrated security=SSPI;data source=SERVER;persist security inf" & _
        "o=False;initial catalog=GTMS_DEV"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VMaster_Materials_Display", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("MMTID", "MMTID"), New System.Data.Common.DataColumnMapping("MGID", "MGID"), New System.Data.Common.DataColumnMapping("MMTName", "MMTName")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Master_Materials WHERE (MMTID = @Original_MMTID) AND (MGID = @Origina" & _
        "l_MGID OR @Original_MGID IS NULL AND MGID IS NULL) AND (MMTName = @Original_MMTN" & _
        "ame OR @Original_MMTName IS NULL AND MMTName IS NULL)"
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTName", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Master_Materials (MGID, MMTName) VALUES (@MGID, @MMTName); SELECT MMT" & _
        "ID, MGID, MMTName, MMTStocked_Display FROM VMaster_Materials_Display WHERE (MMTI" & _
        "D = @@IDENTITY) ORDER BY MMTID"
        Me.SqlInsertCommand1.Connection = Me.hSqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTName", System.Data.SqlDbType.VarChar, 50, "MMTName"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT MMTID, MGID, MMTName, MMTStocked_Display FROM VMaster_Materials_Display WH" & _
        "ERE (MGID = @MGID) ORDER BY MMTID"
        Me.SqlSelectCommand1.Connection = Me.hSqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Master_Materials SET MGID = @MGID, MMTName = @MMTName WHERE (MMTID = @Orig" & _
        "inal_MMTID) AND (MGID = @Original_MGID OR @Original_MGID IS NULL AND MGID IS NUL" & _
        "L) AND (MMTName = @Original_MMTName OR @Original_MMTName IS NULL AND MMTName IS " & _
        "NULL); SELECT MMTID, MGID, MMTName, MMTStocked_Display FROM VMaster_Materials_Di" & _
        "splay WHERE (MMTID = @MMTID) ORDER BY MMTID"
        Me.SqlUpdateCommand1.Connection = Me.hSqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTName", System.Data.SqlDbType.VarChar, 50, "MMTName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTID", System.Data.SqlDbType.Int, 4, "MMTID"))
        '
        'lvMasterMaterials
        '
        Me.Controls.Add(Me.GridControl)
        Me.Name = "lvMasterMaterials"
        Me.Size = New System.Drawing.Size(632, 392)
        CType(Me.gvStockedItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsMaterialsGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvMaterials, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return hSqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            hSqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Public Function FillDataSet() As Integer Implements IListControl.FillDataSet
        ' Open connection
        Dim trans As SqlClient.SqlTransaction = Connection.BeginTransaction(IsolationLevel.ReadUncommitted)
        Power.Library.Library.ApplyTransactionToAllDataAdapters(trans, Me)
        'Me.GridControl1.DataSource.Clear()
        FillDataSet = SqlDataAdapter.Fill(DsMaterialsGrid)
        'daStockedItems.Fill(DsMaterialsGrid)
        'Close connecton
        trans.Commit()
    End Function

    Private Sub GridView1_RowCountChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvMaterials.RowCountChanged
        ExplorerForm.UpdateNumItems(NumItems)
        ExplorerForm.UpdateVisibleItems(VisibleItems)
    End Sub

    Public ReadOnly Property VisibleItems() As Integer Implements Power.Library.IListControl.VisibleItems
        Get
            Return gvMaterials.RowCount
        End Get
    End Property

    Public ReadOnly Property NumItems() As Integer Implements Power.Library.IListControl.NumItems
        Get
            Return DsMaterialsGrid.Tables(SqlDataAdapter.TableMappings(0).DataSetTable).Rows.Count
        End Get
    End Property

    Private hMGID As Integer
    Private Property MGID() As Integer
        Get
            Return hMGID
        End Get
        Set(ByVal Value As Integer)
            hMGID = Value
            SqlDataAdapter.SelectCommand.Parameters("@MGID").Value = Value
        End Set
    End Property

    Public ReadOnly Property SelectedRow() As DataRow
        Get
            If Not gvMaterials.GetSelectedRows Is Nothing Then
                Return gvMaterials.GetDataRow(gvMaterials.GetSelectedRows(0))
            End If
        End Get
    End Property

    Public ReadOnly Property SelectedRowField(ByVal Field As String) As Object
        Get
            If Not SelectedRow Is Nothing Then
                Return SelectedRow.Item(Field)
            Else
                Return DBNull.Value
            End If
        End Get
    End Property

    Public Function SelectRow(ByVal MGID As Int32) As Boolean
        Dim i As Integer = 0
        Do Until False
            Try
                If gvMaterials.GetDataRow(gvMaterials.GetVisibleRowHandle(i))("MGID") = MGID Then
                    gvMaterials.ClearSelection()
                    gvMaterials.SelectRow(gvMaterials.GetVisibleRowHandle(i))
                    Return True
                End If
                i += 1
            Catch ex As NullReferenceException
                Return False
            End Try
        Loop
        Return False
    End Function

#Region " New, Open and Delete "

    Private Sub GridControl1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridControl.DoubleClick
        List_Edit()
    End Sub

    Public Sub List_New() Implements IListControl.List_New
        Dim c As Cursor = ParentForm.Cursor
        ParentForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            Dim gui As frmMasterMaterialWizard = frmMasterMaterialWizard.Add(MGID)
            gui.ShowDialog(Me)
            FillDataSet()
            SelectRow(MGID)
        Catch ex As ObjectLockedException
            DevExpress.XtraEditors.XtraMessageBox.Show("This material is currently being accessed by another user." & _
                vbNewLine & vbNewLine & "Please ensure all users close this material from all branches, and try again.", _
                "Franchise Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        ParentForm.Cursor = c
    End Sub

    Public Sub List_Edit() Implements IListControl.List_Edit
        If Not SelectedRow Is Nothing Then
            Dim c As Cursor = ParentForm.Cursor
            ParentForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If MaterialExists(SelectedRowField("MMTID")) Then
                Try
                    Dim gui As frmMasterMaterial = frmMasterMaterial.Edit(SelectedRowField("MMTID"))
                    gui.ShowDialog(Me)
                    FillDataSet()
                Catch ex As ObjectLockedException
                    DevExpress.XtraEditors.XtraMessageBox.Show("This material is currently being accessed by another user." & _
                        vbNewLine & vbNewLine & "Please ensure all users close this material from all branches, and try again.", _
                        "Franchise Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Try
            Else
                Message.AlreadyDeleted("material", Message.ObjectAction.Edit)
                Me.GridControl.DataSource.Clear()
                FillDataSet()
            End If
            ParentForm.Cursor = c
        End If
    End Sub

    Public Sub List_Delete() Implements IListControl.List_Delete
        If Not SelectedRow Is Nothing Then
            Dim MMTID As Long = SelectedRowField("MMTID")
            If Message.AskDeleteObject("material", Message.ObjectAction.Discontinue) = MsgBoxResult.Yes Then
                If MaterialExists(SelectedRowField("MMTID")) Then
                    If DataAccess.spExecLockRequest("sp_GetMasterMaterialLock", MMTID, Connection) Then
                        SelectedRow.Item("MMTDiscontinued") = True
                        SqlDataAdapter.Update(DsMaterialsGrid)
                        DataAccess.spExecLockRequest("sp_ReleaseMasterMaterialLock", MMTID, Connection)
                    Else
                        DevExpress.XtraEditors.XtraMessageBox.Show("This material is currently being accessed by another user." & _
                            vbNewLine & vbNewLine & "Please ensure all users close this material from all branches, and try again.", _
                            "Franchise Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                Else
                    Message.AlreadyDeleted("material", Message.ObjectAction.Discontinue)
                    Me.GridControl.DataSource.Clear()
                    FillDataSet()
                End If
            End If
        End If
    End Sub

    Private Function MaterialExists(ByVal MMTID As Int64) As Boolean
        Dim cnn As SqlClient.SqlConnection = Connection
        Dim cmd As New SqlClient.SqlCommand("SELECT Count(MMTID) FROM Master_Materials WHERE MMTID = @MMTID", cnn)
        cmd.CommandType = CommandType.Text
        cmd.Parameters.Add("@MMTID", MMTID)
        Return cmd.ExecuteScalar > 0
    End Function

#End Region

    Private hExplorerForm As Form
    Public ReadOnly Property ExplorerForm() As frmExplorerHeadOffice
        Get
            Return hExplorerForm
        End Get
    End Property

    Public ReadOnly Property AllowDelete() As Boolean Implements Power.Library.IListControl.AllowDelete
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property AllowEdit() As Boolean Implements Power.Library.IListControl.AllowEdit
        Get
            Return True
        End Get
    End Property

    Public ReadOnly Property AllowNew() As Boolean Implements Power.Library.IListControl.AllowNew
        Get
            Return True
        End Get
    End Property

    Public ReadOnly Property AllowUndelete() As Boolean Implements Power.Library.IListControl.AllowUndelete
        Get
            Return False
        End Get
    End Property

    Public Sub List_Undelete() Implements Power.Library.IListControl.List_Undelete

    End Sub

    Public ReadOnly Property DeleteCaption() As String Implements Power.Library.IListControl.DeleteCaption
        Get
            Return Nothing
        End Get
    End Property

    Public ReadOnly Property UndeleteCaption() As String Implements Power.Library.IListControl.UndeleteCaption
        Get
            Return Nothing
        End Get
    End Property

End Class
