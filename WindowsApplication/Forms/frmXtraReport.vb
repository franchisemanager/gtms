Public Class frmXtraReport
    Inherits DevExpress.XtraEditors.XtraForm

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents PrintControl1 As DevExpress.XtraPrinting.Control.PrintControl
    Friend WithEvents BarAndDockingController1 As DevExpress.XtraBars.BarAndDockingController
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents btnFirst As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPrevious As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnNext As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnLast As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnSave As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnHelp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmXtraReport))
        Me.PrintControl1 = New DevExpress.XtraPrinting.Control.PrintControl
        Me.BarAndDockingController1 = New DevExpress.XtraBars.BarAndDockingController(Me.components)
        Me.BarManager1 = New DevExpress.XtraBars.BarManager
        Me.Bar1 = New DevExpress.XtraBars.Bar
        Me.btnFirst = New DevExpress.XtraBars.BarButtonItem
        Me.btnPrevious = New DevExpress.XtraBars.BarButtonItem
        Me.btnNext = New DevExpress.XtraBars.BarButtonItem
        Me.btnLast = New DevExpress.XtraBars.BarButtonItem
        Me.btnSave = New DevExpress.XtraBars.BarButtonItem
        Me.btnPrint = New DevExpress.XtraBars.BarButtonItem
        Me.btnHelp = New DevExpress.XtraBars.BarButtonItem
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
        Me.Bar2 = New DevExpress.XtraBars.Bar
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PrintControl1
        '
        Me.PrintControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PrintControl1.IsMetric = True
        Me.PrintControl1.Location = New System.Drawing.Point(0, 24)
        Me.PrintControl1.Name = "PrintControl1"
        Me.PrintControl1.Size = New System.Drawing.Size(776, 498)
        Me.PrintControl1.TabIndex = 0
        '
        'BarAndDockingController1
        '
        Me.BarAndDockingController1.PaintStyleName = "OfficeXP"
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1, Me.Bar2})
        Me.BarManager1.Controller = Me.BarAndDockingController1
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Images = Me.ImageList1
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnFirst, Me.btnPrevious, Me.btnNext, Me.btnLast, Me.btnSave, Me.btnPrint, Me.BarSubItem1, Me.btnHelp})
        Me.BarManager1.MaxItemId = 8
        Me.BarManager1.StatusBar = Me.Bar2
        '
        'Bar1
        '
        Me.Bar1.BarName = "Standard"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.FloatLocation = New System.Drawing.Point(38, 163)
        Me.Bar1.FloatSize = New System.Drawing.Size(455, 24)
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnFirst, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnPrevious, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnNext, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnLast, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnSave, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnPrint, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnHelp, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.Bar1.OptionsBar.AllowQuickCustomization = False
        Me.Bar1.OptionsBar.DisableClose = True
        Me.Bar1.Text = "Standard"
        '
        'btnFirst
        '
        Me.btnFirst.Caption = "&First"
        Me.btnFirst.Id = 0
        Me.btnFirst.ImageIndex = 0
        Me.btnFirst.ImageIndexDisabled = 0
        Me.btnFirst.Name = "btnFirst"
        '
        'btnPrevious
        '
        Me.btnPrevious.Caption = "Pr&evious"
        Me.btnPrevious.Id = 1
        Me.btnPrevious.ImageIndex = 1
        Me.btnPrevious.ImageIndexDisabled = 1
        Me.btnPrevious.Name = "btnPrevious"
        '
        'btnNext
        '
        Me.btnNext.Caption = "&Next"
        Me.btnNext.Id = 2
        Me.btnNext.ImageIndex = 2
        Me.btnNext.ImageIndexDisabled = 2
        Me.btnNext.Name = "btnNext"
        '
        'btnLast
        '
        Me.btnLast.Caption = "&Last"
        Me.btnLast.Id = 3
        Me.btnLast.ImageIndex = 5
        Me.btnLast.ImageIndexDisabled = 5
        Me.btnLast.Name = "btnLast"
        '
        'btnSave
        '
        Me.btnSave.Caption = "&Save"
        Me.btnSave.Id = 4
        Me.btnSave.ImageIndex = 4
        Me.btnSave.ImageIndexDisabled = 4
        Me.btnSave.Name = "btnSave"
        '
        'btnPrint
        '
        Me.btnPrint.Caption = "&Print"
        Me.btnPrint.Id = 5
        Me.btnPrint.ImageIndex = 3
        Me.btnPrint.ImageIndexDisabled = 3
        Me.btnPrint.Name = "btnPrint"
        '
        'btnHelp
        '
        Me.btnHelp.Caption = "&Help"
        Me.btnHelp.Id = 7
        Me.btnHelp.ImageIndex = 6
        Me.btnHelp.ImageIndexDisabled = 6
        Me.btnHelp.Name = "btnHelp"
        '
        'ImageList1
        '
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'BarSubItem1
        '
        Me.BarSubItem1.Caption = "&File"
        Me.BarSubItem1.Id = 6
        Me.BarSubItem1.Name = "BarSubItem1"
        '
        'Bar2
        '
        Me.Bar2.BarName = "Custom 2"
        Me.Bar2.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
        Me.Bar2.DockCol = 0
        Me.Bar2.DockRow = 0
        Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar2.OptionsBar.AllowQuickCustomization = False
        Me.Bar2.OptionsBar.DrawDragBorder = False
        Me.Bar2.OptionsBar.UseWholeRow = True
        Me.Bar2.Text = "Custom 2"
        '
        'frmXtraReport
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(776, 542)
        Me.Controls.Add(Me.PrintControl1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmXtraReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "View Report"
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private LoadedReport As DevExpress.XtraReports.UI.XtraReport
    Public Sub LoadReport(ByVal report As DevExpress.XtraReports.UI.XtraReport)
        Me.LoadedReport = report
        PrintControl1.PrintingSystem = report.PrintingSystem
        'report.CreateDocument()
    End Sub

End Class

