Public Class frmAppointment
    Inherits DevExpress.XtraEditors.XtraForm

    Private DataRow As DataRow
    Private IsNew As Boolean = False
    Public BRID As Int32
    Public APID As Int64

    Private MinHours As Integer

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return SqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            SqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private hTransaction As SqlClient.SqlTransaction
    Private Property Transaction() As SqlClient.SqlTransaction
        Get
            Return hTransaction
        End Get
        Set(ByVal Value As SqlClient.SqlTransaction)
            hTransaction = Value
            Power.Library.Library.ApplyTransactionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Public Shared Function Add(ByVal Storage As DevExpress.XtraScheduler.SchedulerStorage, ByVal BRID As Integer, ByVal APType As String, ByVal APTypeID As Int64, ByVal Transaction As SqlClient.SqlTransaction, Optional ByVal MinHours As Integer = 0) As frmAppointment
        Dim gui As New frmAppointment(Storage)

        With gui
            .MinHours = MinHours

            .Transaction = Transaction
            .IsNew = True

            .BRID = BRID
            .APID = spNew_Appointment(BRID, APType, APTypeID, .Transaction)
            .FillPreliminaryData()

            .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = .BRID
            .SqlDataAdapter.SelectCommand.Parameters("@APID").Value = .APID
            .SqlDataAdapter.Fill(.DsCalendar)
            .DataRow = .DsCalendar.VAppointments(0)

            .FillData()
        End With

        Return gui
    End Function

    Public Shared Function Edit(ByVal Storage As DevExpress.XtraScheduler.SchedulerStorage, ByVal BRID As Integer, ByVal APID As Int64, ByVal Transaction As SqlClient.SqlTransaction, Optional ByVal MinHours As Integer = 0) As frmAppointment
        Dim gui As New frmAppointment(Storage)

        With gui
            .MinHours = MinHours

            .Transaction = Transaction

            .BRID = BRID
            .APID = APID
            .FillPreliminaryData()

            .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = .BRID
            .SqlDataAdapter.SelectCommand.Parameters("@APID").Value = .APID
            .SqlDataAdapter.Fill(.DsCalendar)
            .DataRow = .DsCalendar.VAppointments(0)

            .FillData()
        End With

        Return gui
    End Function

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Storage As DevExpress.XtraScheduler.SchedulerStorage)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        txtEXID.Storage = Storage
        txtAPTask.Storage = Storage
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtAPNotes As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dpAPBegin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dpAPEnd As DevExpress.XtraEditors.DateEdit
    Friend WithEvents DsCalendar As WindowsApplication.dsCalendar
    Friend WithEvents txtAPDescription As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents btnHelp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblStaffMember As System.Windows.Forms.Label
    Friend WithEvents lblStaffMemberDesc As System.Windows.Forms.Label
    Friend WithEvents lblNotes As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents chkAPAllDay As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents txtEXID As DevExpress.XtraScheduler.UI.AppointmentResourceEdit
    Friend WithEvents txtAPTask As DevExpress.XtraScheduler.UI.AppointmentLabelEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim configurationAppSettings As System.Configuration.AppSettingsReader = New System.Configuration.AppSettingsReader
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmAppointment))
        Me.lblStaffMemberDesc = New System.Windows.Forms.Label
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage
        Me.txtAPTask = New DevExpress.XtraScheduler.UI.AppointmentLabelEdit
        Me.DsCalendar = New WindowsApplication.dsCalendar
        Me.txtEXID = New DevExpress.XtraScheduler.UI.AppointmentResourceEdit
        Me.Label1 = New System.Windows.Forms.Label
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit
        Me.txtAPDescription = New DevExpress.XtraEditors.TextEdit
        Me.lblStaffMember = New System.Windows.Forms.Label
        Me.txtAPNotes = New DevExpress.XtraEditors.MemoEdit
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl
        Me.chkAPAllDay = New DevExpress.XtraEditors.CheckEdit
        Me.dpAPBegin = New DevExpress.XtraEditors.DateEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.dpAPEnd = New DevExpress.XtraEditors.DateEdit
        Me.lblNotes = New System.Windows.Forms.Label
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.btnHelp = New DevExpress.XtraEditors.SimpleButton
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.txtAPTask.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsCalendar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEXID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAPDescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAPNotes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.chkAPAllDay.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dpAPBegin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dpAPEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblStaffMemberDesc
        '
        Me.lblStaffMemberDesc.Location = New System.Drawing.Point(16, 96)
        Me.lblStaffMemberDesc.Name = "lblStaffMemberDesc"
        Me.lblStaffMemberDesc.Size = New System.Drawing.Size(240, 16)
        Me.lblStaffMemberDesc.TabIndex = 2
        Me.lblStaffMemberDesc.Text = "Please select a staff member."
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.XtraTabControl1.Location = New System.Drawing.Point(8, 8)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(352, 480)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1})
        Me.XtraTabControl1.Text = "XtraTabControl1"
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.txtAPTask)
        Me.XtraTabPage1.Controls.Add(Me.txtEXID)
        Me.XtraTabPage1.Controls.Add(Me.Label1)
        Me.XtraTabPage1.Controls.Add(Me.MemoEdit1)
        Me.XtraTabPage1.Controls.Add(Me.txtAPDescription)
        Me.XtraTabPage1.Controls.Add(Me.lblStaffMember)
        Me.XtraTabPage1.Controls.Add(Me.txtAPNotes)
        Me.XtraTabPage1.Controls.Add(Me.GroupControl1)
        Me.XtraTabPage1.Controls.Add(Me.lblStaffMemberDesc)
        Me.XtraTabPage1.Controls.Add(Me.lblNotes)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(346, 454)
        Me.XtraTabPage1.Text = "Appointment Information"
        '
        'txtAPTask
        '
        Me.txtAPTask.DataBindings.Add(New System.Windows.Forms.Binding("SelectedIndex", Me.DsCalendar, "VAppointments.APTask"))
        Me.txtAPTask.Location = New System.Drawing.Point(144, 296)
        Me.txtAPTask.Name = "txtAPTask"
        '
        'txtAPTask.Properties
        '
        Me.txtAPTask.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtAPTask.Size = New System.Drawing.Size(176, 20)
        Me.txtAPTask.TabIndex = 7
        '
        'DsCalendar
        '
        Me.DsCalendar.DataSetName = "dsCalendar"
        Me.DsCalendar.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'txtEXID
        '
        Me.txtEXID.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsCalendar, "VAppointments.EXID"))
        Me.txtEXID.Location = New System.Drawing.Point(104, 120)
        Me.txtEXID.Name = "txtEXID"
        '
        'txtEXID.Properties
        '
        Me.txtEXID.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtEXID.Size = New System.Drawing.Size(216, 20)
        Me.txtEXID.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(16, 296)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(128, 23)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Task:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'MemoEdit1
        '
        Me.MemoEdit1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsCalendar, "VAppointments.APAddressMultiline"))
        Me.MemoEdit1.EditValue = "Address"
        Me.MemoEdit1.Location = New System.Drawing.Point(16, 40)
        Me.MemoEdit1.Name = "MemoEdit1"
        '
        'MemoEdit1.Properties
        '
        Me.MemoEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MemoEdit1.Size = New System.Drawing.Size(320, 48)
        Me.MemoEdit1.TabIndex = 1
        '
        'txtAPDescription
        '
        Me.txtAPDescription.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAPDescription.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsCalendar, "VAppointments.APDescription"))
        Me.txtAPDescription.EditValue = "Quote Request"
        Me.txtAPDescription.Location = New System.Drawing.Point(16, 16)
        Me.txtAPDescription.Name = "txtAPDescription"
        '
        'txtAPDescription.Properties
        '
        Me.txtAPDescription.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.txtAPDescription.Properties.ReadOnly = True
        Me.txtAPDescription.Size = New System.Drawing.Size(318, 18)
        Me.txtAPDescription.TabIndex = 0
        '
        'lblStaffMember
        '
        Me.lblStaffMember.Location = New System.Drawing.Point(16, 120)
        Me.lblStaffMember.Name = "lblStaffMember"
        Me.lblStaffMember.Size = New System.Drawing.Size(80, 23)
        Me.lblStaffMember.TabIndex = 3
        Me.lblStaffMember.Text = "Staff member:"
        Me.lblStaffMember.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAPNotes
        '
        Me.txtAPNotes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAPNotes.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsCalendar, "VAppointments.APNotes"))
        Me.txtAPNotes.EditValue = ""
        Me.txtAPNotes.Location = New System.Drawing.Point(16, 344)
        Me.txtAPNotes.Name = "txtAPNotes"
        '
        'txtAPNotes.Properties
        '
        Me.txtAPNotes.Properties.MaxLength = 2000
        Me.txtAPNotes.Size = New System.Drawing.Size(318, 96)
        Me.txtAPNotes.TabIndex = 9
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.GroupControl1.Appearance.Options.UseBackColor = True
        Me.GroupControl1.Controls.Add(Me.chkAPAllDay)
        Me.GroupControl1.Controls.Add(Me.dpAPBegin)
        Me.GroupControl1.Controls.Add(Me.Label2)
        Me.GroupControl1.Controls.Add(Me.Label4)
        Me.GroupControl1.Controls.Add(Me.dpAPEnd)
        Me.GroupControl1.Location = New System.Drawing.Point(16, 152)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(320, 128)
        Me.GroupControl1.TabIndex = 5
        Me.GroupControl1.Text = "Time Slot Scheduling Details"
        '
        'chkAPAllDay
        '
        Me.chkAPAllDay.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsCalendar, "VAppointments.APAllDay"))
        Me.chkAPAllDay.Location = New System.Drawing.Point(16, 32)
        Me.chkAPAllDay.Name = "chkAPAllDay"
        '
        'chkAPAllDay.Properties
        '
        Me.chkAPAllDay.Properties.Caption = "All day"
        Me.chkAPAllDay.Size = New System.Drawing.Size(88, 18)
        Me.chkAPAllDay.TabIndex = 0
        '
        'dpAPBegin
        '
        Me.dpAPBegin.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsCalendar, "VAppointments.APBegin"))
        Me.dpAPBegin.EditValue = New Date(2005, 7, 21, 0, 0, 0, 0)
        Me.dpAPBegin.Location = New System.Drawing.Point(128, 64)
        Me.dpAPBegin.Name = "dpAPBegin"
        '
        'dpAPBegin.Properties
        '
        Me.dpAPBegin.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False
        Me.dpAPBegin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpAPBegin.Properties.DisplayFormat.FormatString = "g"
        Me.dpAPBegin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dpAPBegin.Properties.EditFormat.FormatString = "g"
        Me.dpAPBegin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dpAPBegin.Properties.Mask.EditMask = "g"
        Me.dpAPBegin.Size = New System.Drawing.Size(176, 20)
        Me.dpAPBegin.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(16, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 23)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Start Date:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(16, 96)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 23)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "End Date:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dpAPEnd
        '
        Me.dpAPEnd.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsCalendar, "VAppointments.APEnd"))
        Me.dpAPEnd.EditValue = New Date(2005, 7, 21, 0, 0, 0, 0)
        Me.dpAPEnd.Location = New System.Drawing.Point(128, 96)
        Me.dpAPEnd.Name = "dpAPEnd"
        '
        'dpAPEnd.Properties
        '
        Me.dpAPEnd.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False
        Me.dpAPEnd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpAPEnd.Properties.DisplayFormat.FormatString = "g"
        Me.dpAPEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dpAPEnd.Properties.EditFormat.FormatString = "g"
        Me.dpAPEnd.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dpAPEnd.Properties.Mask.EditMask = "g"
        Me.dpAPEnd.Size = New System.Drawing.Size(176, 20)
        Me.dpAPEnd.TabIndex = 4
        '
        'lblNotes
        '
        Me.lblNotes.Location = New System.Drawing.Point(16, 328)
        Me.lblNotes.Name = "lblNotes"
        Me.lblNotes.Size = New System.Drawing.Size(96, 16)
        Me.lblNotes.TabIndex = 8
        Me.lblNotes.Text = "Notes:"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(210, 496)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(290, 496)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = CType(configurationAppSettings.GetValue("SqlConnection.ConnectionString", GetType(System.String)), String)
        '
        'btnHelp
        '
        Me.btnHelp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnHelp.Image = CType(resources.GetObject("btnHelp.Image"), System.Drawing.Image)
        Me.btnHelp.Location = New System.Drawing.Point(8, 496)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(72, 23)
        Me.btnHelp.TabIndex = 1
        Me.btnHelp.Text = "Help"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VAppointments", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("APID", "APID"), New System.Data.Common.DataColumnMapping("APBegin", "APBegin"), New System.Data.Common.DataColumnMapping("APEnd", "APEnd"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("APType", "APType"), New System.Data.Common.DataColumnMapping("APNotes", "APNotes"), New System.Data.Common.DataColumnMapping("APTypeID", "APTypeID"), New System.Data.Common.DataColumnMapping("APStatus", "APStatus"), New System.Data.Common.DataColumnMapping("APTask", "APTask"), New System.Data.Common.DataColumnMapping("APAllDay", "APAllDay")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Appointments WHERE (APID = @Original_APID) AND (BRID = @Original_BRID" & _
        ") AND (APAllDay = @Original_APAllDay OR @Original_APAllDay IS NULL AND APAllDay " & _
        "IS NULL) AND (APBegin = @Original_APBegin OR @Original_APBegin IS NULL AND APBeg" & _
        "in IS NULL) AND (APEnd = @Original_APEnd OR @Original_APEnd IS NULL AND APEnd IS" & _
        " NULL) AND (APNotes = @Original_APNotes OR @Original_APNotes IS NULL AND APNotes" & _
        " IS NULL) AND (APStatus = @Original_APStatus OR @Original_APStatus IS NULL AND A" & _
        "PStatus IS NULL) AND (APTask = @Original_APTask OR @Original_APTask IS NULL AND " & _
        "APTask IS NULL) AND (APType = @Original_APType OR @Original_APType IS NULL AND A" & _
        "PType IS NULL) AND (APTypeID = @Original_APTypeID OR @Original_APTypeID IS NULL " & _
        "AND APTypeID IS NULL) AND (EXID = @Original_EXID OR @Original_EXID IS NULL AND E" & _
        "XID IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APAllDay", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APAllDay", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APBegin", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APBegin", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APEnd", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APEnd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APNotes", System.Data.SqlDbType.VarChar, 2000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APStatus", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APStatus", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APTask", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APTask", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APTypeID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APTypeID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=DEV1;packet size=4096;integrated security=SSPI;data source=""SERVER" & _
        "\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Appointments (BRID, APBegin, APEnd, EXID, APType, APNotes, APTypeID, " & _
        "APStatus, APTask, APAllDay) VALUES (@BRID, @APBegin, @APEnd, @EXID, @APType, @AP" & _
        "Notes, @APTypeID, @APStatus, @APTask, @APAllDay); SELECT BRID, APID, APBegin, AP" & _
        "End, EXID, APType, APNotes, APTypeID, APStatus, APTask, APAllDay, APAddressMulti" & _
        "line, APDescription FROM VAppointments WHERE (APID = @@IDENTITY) AND (BRID = @BR" & _
        "ID)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APBegin", System.Data.SqlDbType.DateTime, 8, "APBegin"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APEnd", System.Data.SqlDbType.DateTime, 8, "APEnd"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APType", System.Data.SqlDbType.VarChar, 2, "APType"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APNotes", System.Data.SqlDbType.VarChar, 2000, "APNotes"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTypeID", System.Data.SqlDbType.BigInt, 8, "APTypeID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APStatus", System.Data.SqlDbType.Int, 4, "APStatus"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTask", System.Data.SqlDbType.Int, 4, "APTask"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APAllDay", System.Data.SqlDbType.Bit, 1, "APAllDay"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, APID, APBegin, APEnd, EXID, APType, APNotes, APTypeID, APStatus, APT" & _
        "ask, APAllDay, APAddressMultiline, APDescription FROM VAppointments WHERE (BRID " & _
        "= @BRID) AND (APID = @APID)"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APID", System.Data.SqlDbType.BigInt, 8, "APID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Appointments SET BRID = @BRID, APBegin = @APBegin, APEnd = @APEnd, EXID = " & _
        "@EXID, APType = @APType, APNotes = @APNotes, APTypeID = @APTypeID, APStatus = @A" & _
        "PStatus, APTask = @APTask, APAllDay = @APAllDay WHERE (APID = @Original_APID) AN" & _
        "D (BRID = @Original_BRID) AND (APAllDay = @Original_APAllDay OR @Original_APAllD" & _
        "ay IS NULL AND APAllDay IS NULL) AND (APBegin = @Original_APBegin OR @Original_A" & _
        "PBegin IS NULL AND APBegin IS NULL) AND (APEnd = @Original_APEnd OR @Original_AP" & _
        "End IS NULL AND APEnd IS NULL) AND (APNotes = @Original_APNotes OR @Original_APN" & _
        "otes IS NULL AND APNotes IS NULL) AND (APStatus = @Original_APStatus OR @Origina" & _
        "l_APStatus IS NULL AND APStatus IS NULL) AND (APTask = @Original_APTask OR @Orig" & _
        "inal_APTask IS NULL AND APTask IS NULL) AND (APType = @Original_APType OR @Origi" & _
        "nal_APType IS NULL AND APType IS NULL) AND (APTypeID = @Original_APTypeID OR @Or" & _
        "iginal_APTypeID IS NULL AND APTypeID IS NULL) AND (EXID = @Original_EXID OR @Ori" & _
        "ginal_EXID IS NULL AND EXID IS NULL); SELECT BRID, APID, APBegin, APEnd, EXID, A" & _
        "PType, APNotes, APTypeID, APStatus, APTask, APAllDay, APAddressMultiline, APDesc" & _
        "ription FROM VAppointments WHERE (APID = @APID) AND (BRID = @BRID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APBegin", System.Data.SqlDbType.DateTime, 8, "APBegin"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APEnd", System.Data.SqlDbType.DateTime, 8, "APEnd"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APType", System.Data.SqlDbType.VarChar, 2, "APType"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APNotes", System.Data.SqlDbType.VarChar, 2000, "APNotes"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTypeID", System.Data.SqlDbType.BigInt, 8, "APTypeID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APStatus", System.Data.SqlDbType.Int, 4, "APStatus"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTask", System.Data.SqlDbType.Int, 4, "APTask"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APAllDay", System.Data.SqlDbType.Bit, 1, "APAllDay"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APAllDay", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APAllDay", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APBegin", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APBegin", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APEnd", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APEnd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APNotes", System.Data.SqlDbType.VarChar, 2000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APStatus", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APStatus", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APTask", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APTask", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APTypeID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APTypeID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APID", System.Data.SqlDbType.BigInt, 8, "APID"))
        '
        'frmAppointment
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(370, 528)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnHelp)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAppointment"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Appointment"
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.txtAPTask.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsCalendar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEXID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAPDescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAPNotes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.chkAPAllDay.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dpAPBegin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dpAPEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnOK.Enabled = Not HasRole("branch_read_only")
        chkAPAllDay_CheckedChanged(sender, e)
    End Sub

    Private Sub FillPreliminaryData()
    End Sub

    Private Sub FillData()
        CustomizeScreen()
    End Sub

    Private Sub EnableDisable()
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        DataRow.EndEdit()
        If ValidateForm() Then
            SqlDataAdapter.Update(DsCalendar)
            OK = True
            Me.Close()
        End If
    End Sub

    Private Function ValidateForm() As Boolean
        If txtEXID.EditValue Is DBNull.Value Then
            Select Case DataRow("APType")
                Case "LD"
                    DevExpress.XtraEditors.XtraMessageBox.Show("You must select a salesperson for this appointment.", "Franchise Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Case "JB"
                    DevExpress.XtraEditors.XtraMessageBox.Show("You must select direct labor for this appointment.", "Franchise Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Case Else
                    DevExpress.XtraEditors.XtraMessageBox.Show("You must select person for this appointment.", "Franchise Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Select
            txtEXID.Focus()
            Return False
        End If
        If dpAPBegin.EditValue Is DBNull.Value Then
            DevExpress.XtraEditors.XtraMessageBox.Show("You must select a start date and time for this appointment.", "Franchise Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            dpAPBegin.Focus()
            Return False
        ElseIf Not IsValidDate(CDate(dpAPBegin.EditValue)) Then
            DevExpress.XtraEditors.XtraMessageBox.Show("You must select a start date between the years 1753 and 9999.", "Franchise Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            dpAPBegin.Focus()
            Return False
        End If
        If dpAPEnd.EditValue Is DBNull.Value Then
            DevExpress.XtraEditors.XtraMessageBox.Show("You must select a end date and time for this appointment.", "Franchise Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            dpAPEnd.Focus()
            Return False
        ElseIf Not IsValidDate(CDate(dpAPEnd.EditValue)) Then
            DevExpress.XtraEditors.XtraMessageBox.Show("You must select a end date between the years 1753 and 9999.", "Franchise Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            dpAPBegin.Focus()
            Return False
        End If
        If DateDiff(DateInterval.Hour, dpAPBegin.EditValue, dpAPEnd.EditValue) < MinHours Then
            DevExpress.XtraEditors.XtraMessageBox.Show("You cannot enter an appointment less than " & MinHours & " hour(s).", "Franchise Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End If
        If ExecuteScalar("SELECT dbo.IsAppointmentClashing(@BRID, @APID, @APBegin, @APEnd, @EXID)", CommandType.Text, New SqlClient.SqlParameter() { _
                New SqlClient.SqlParameter("@BRID", DataRow("BRID")), _
                New SqlClient.SqlParameter("@APID", DataRow("APID")), _
                New SqlClient.SqlParameter("@APBegin", DataRow("APBegin")), _
                New SqlClient.SqlParameter("@APEnd", DataRow("APEnd")), _
                New SqlClient.SqlParameter("@EXID", DataRow("EXID"))}, Transaction) Then
            Message.ShowMessage("This appointment clashes with another appointment.  Please reschedule this appointment.", MessageBoxIcon.Exclamation)
            Return False
        End If
        Return True
    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            DataRow.EndEdit()
            If DsCalendar.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If response = MsgBoxResult.Yes Then
                If IsNew Then
                    DataRow.Delete()
                    SqlDataAdapter.Update(DsCalendar)
                End If
            Else
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub CustomizeScreen()
        Select Case DataRow("APType")
            Case "LD"
                Me.lblStaffMember.Text = "Salesperson:"
                Me.lblStaffMemberDesc.Text = "Please select a salesperson."
            Case "JB"
                Me.lblStaffMember.Text = "Direct Labor:"
                Me.lblStaffMemberDesc.Text = "Please select a direct labor person."
            Case "NO"
                Me.lblStaffMember.Text = "Staff Member:"
                Me.lblStaffMemberDesc.Text = "Please select a staff member."
        End Select
    End Sub

    Public Property APAllDay() As Boolean
        Get
            Return DataRow("APAllDay")
        End Get
        Set(ByVal Value As Boolean)
            DataRow("APAllDay") = Value
        End Set
    End Property

    Public Property APBegin() As Date
        Get
            Return DataRow("APBegin")
        End Get
        Set(ByVal Value As Date)
            DataRow("APBegin") = Value
        End Set
    End Property

    Public Property APEnd() As Date
        Get
            Return DataRow("APEnd")
        End Get
        Set(ByVal Value As Date)
            DataRow("APEnd") = Value
        End Set
    End Property

    Public Property EXID() As Int32
        Get
            Return DataRow("EXID")
        End Get
        Set(ByVal Value As Int32)
            DataRow("EXID") = Value
        End Set
    End Property

    Public Property APTask() As Int32
        Get
            Return DataRow("APTask")
        End Get
        Set(ByVal Value As Int32)
            DataRow("APTask") = Value
        End Set
    End Property

    Private hAPBegin As DateTime
    Private hAPEnd As DateTime
    Private hHasPopulatedHiddenTimes As Boolean = False
    Private Sub chkAPAllDay_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAPAllDay.CheckedChanged
        If chkAPAllDay.Checked Then
            If DataRow("APBegin") Is DBNull.Value Or DataRow("APEnd") Is DBNull.Value Then
                Exit Sub
            End If
            hAPBegin = DataRow("APBegin")
            hAPEnd = DataRow("APEnd")
            dpAPBegin.EditValue = CType(dpAPBegin.EditValue, DateTime).Date
            If CType(DataRow("APEnd"), DateTime).TimeOfDay.CompareTo(New TimeSpan(0, 0, 0)) = 0 Then ' if time is at 12am?
                dpAPEnd.EditValue = CType(DataRow("APEnd"), DateTime).Date
            Else
                dpAPEnd.EditValue = CType(DataRow("APEnd"), DateTime).Date.AddDays(1)
            End If
            hHasPopulatedHiddenTimes = True
        Else
            If hHasPopulatedHiddenTimes Then
                dpAPBegin.EditValue = hAPBegin
                dpAPEnd.EditValue = hAPEnd
            End If
        End If
        dpAPBegin.DoValidate()
        dpAPEnd.DoValidate()
    End Sub

    Private Sub btnHelp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHelp.Click
    End Sub
End Class

