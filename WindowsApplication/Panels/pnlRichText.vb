Public Class pnlRichText
    Inherits DevExpress.XtraEditors.XtraUserControl

    Private RichTextStreamPath As String
    Private Screen As String ' WHAT SCREEN ARE WE ON? (from the xml tree)

    Private hExplorerForm As Form
    Public ReadOnly Property ExplorerForm_Branch() As frmExplorer
        Get
            Return CType(hExplorerForm, frmExplorer)
        End Get
    End Property

    Public ReadOnly Property ExplorerForm_HeadOffice() As frmExplorerHeadOffice
        Get
            Return CType(hExplorerForm, frmExplorerHeadOffice)
        End Get
    End Property

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal ExplorerForm As Form, ByVal RichTextStreamPath As String, ByVal screen As String)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.RichTextStreamPath = RichTextStreamPath
        Me.Screen = screen
        hExplorerForm = ExplorerForm
    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents RichTextBox1 As Power.Forms.RichTextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl
        Me.RichTextBox1 = New Power.Forms.RichTextBox
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.PanelControl1.Appearance.Options.UseBackColor = True
        Me.PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.PanelControl1.Controls.Add(Me.RichTextBox1)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl1.DockPadding.All = 8
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(752, 512)
        Me.PanelControl1.TabIndex = 0
        Me.PanelControl1.Text = "PanelControl1"
        '
        'RichTextBox1
        '
        Me.RichTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RichTextBox1.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.RichTextBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RichTextBox1.Location = New System.Drawing.Point(10, 10)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.ReadOnly = True
        Me.RichTextBox1.Size = New System.Drawing.Size(732, 492)
        Me.RichTextBox1.TabIndex = 0
        Me.RichTextBox1.Text = "RichTextBox1"
        '
        'pnlRichText
        '
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "pnlRichText"
        Me.Size = New System.Drawing.Size(752, 512)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub pnlRichText_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.RichTextBox1.LoadFile(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream(RichTextStreamPath), RichTextBoxStreamType.RichText)

        RichTextBox1.Visible = False ' Hide while loading
        Dim cursor As Integer = 1
        Do Until InStr(cursor, RichTextBox1.Text, "<a") = 0
            ConvertToLink(RichTextBox1, InStr(cursor, RichTextBox1.Text, "<a"))
            cursor += 1
        Loop
        RichTextBox1.Visible = True ' Show
    End Sub

    Public Shared Sub ConvertToLink(ByVal rtb As Power.Forms.RichTextBox, ByVal startPosition As Integer)
        Dim address, text As String

        Dim startOpenTag As Integer = startPosition
        Dim endOpenTag As Integer = InStr(startPosition, rtb.Text, ">")
        If endOpenTag = 0 Then Exit Sub ' Could not find ending '>'

        Dim nextOpenTag As Integer = InStr(startOpenTag + 1, rtb.Text, "<")
        If 0 <> nextOpenTag And nextOpenTag < endOpenTag Then Exit Sub ' Found extra '<' within tag

        Dim startAddress As Integer = InStr(startOpenTag + 1, rtb.Text, "{")
        Dim endAddress As Integer = InStr(startAddress + 1, rtb.Text, "}")
        If startAddress = 0 Or startAddress > endOpenTag Or endAddress = 0 Or endAddress > endOpenTag Then Exit Sub ' Could not find two '"'

        address = Mid(rtb.Text, startAddress + 1, endAddress - startAddress - 1)

        Dim startCloseTag As Integer = InStr(endOpenTag, rtb.Text, "</a>")
        If startCloseTag = 0 Then Exit Sub ' could not find closing tag

        text = Mid(rtb.Text, endOpenTag + 1, startCloseTag - endOpenTag - 1)

        rtb.Select(startOpenTag - 1, (startCloseTag + 3) - startOpenTag + 1) ' Don't know why we need the '- 1' in both cases, but whatever works :)

        rtb.InsertLink(text, address)
    End Sub

    Private Sub RichTextBox1_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkClickedEventArgs) Handles RichTextBox1.LinkClicked
        DoHelpAction(Mid(e.LinkText, InStr(e.LinkText, "#") + 1))
    End Sub

    Private Sub DoHelpAction(ByVal helpAction As String)
        helpAction = Trim(helpAction)
        If InStr(helpAction, "/") > 0 Then
            ShowHelpTopic(Me, Mid(helpAction, InStr(helpAction, "/") + 1))
        Else
            Select Case Screen
                Case "ADMINISTRATION"
                    Select Case helpAction
                        ' List help actions
                    End Select
                Case "BRANCH_SETTINGS"
                    Select Case helpAction
                        Case "BranchProperties"
                            ExplorerForm_Branch.LaunchBranchProperties()
                        Case "BranchUsers"
                            ExplorerForm_Branch.LaunchBranchUsers()
                    End Select
                Case "LEADS"
                    Select Case helpAction
                        ' List help actions
                    End Select
                Case "BOOKINGS"
                    Select Case helpAction
                        ' List help actions
                    End Select
            End Select
        End If
    End Sub

End Class