Imports Power.Forms

Public Class lvLeads
    Inherits System.Windows.Forms.UserControl
    Implements IListControl, IPrintableControl

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Connection As SqlClient.SqlConnection, ByVal BRID As Integer, ByVal ExplorerForm As frmExplorer)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.Connection = Connection
        Me.BRID = BRID
        hExplorerForm = ExplorerForm
        FillDataSet()
    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DsGTMS As WindowsApplication.dsGTMS
    Friend WithEvents hSqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLDClientName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLDDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLDUser As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLDJobAddress As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents colLDStatus As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents cmListView As System.Windows.Forms.ContextMenu
    Friend WithEvents miEdit As System.Windows.Forms.MenuItem
    Friend WithEvents miDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents miUncancelJob As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim StyleFormatCondition1 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition
        Me.colLDStatus = New DevExpress.XtraGrid.Columns.GridColumn
        Me.hSqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.DsGTMS = New WindowsApplication.dsGTMS
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.cmListView = New System.Windows.Forms.ContextMenu
        Me.miEdit = New System.Windows.Forms.MenuItem
        Me.miDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.miUncancelJob = New System.Windows.Forms.MenuItem
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colID = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colLDClientName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colLDJobAddress = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colLDDate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colLDUser = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'colLDStatus
        '
        Me.colLDStatus.Caption = "Status"
        Me.colLDStatus.FieldName = "LDStatus"
        Me.colLDStatus.Name = "colLDStatus"
        Me.colLDStatus.Visible = True
        Me.colLDStatus.VisibleIndex = 5
        Me.colLDStatus.Width = 108
        '
        'hSqlConnection
        '
        Me.hSqlConnection.ConnectionString = "packet size=4096;integrated security=SSPI;data source=SERVER;persist security inf" & _
        "o=False;initial catalog=GTMS_DEV"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VLeads", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("LDID", "LDID"), New System.Data.Common.DataColumnMapping("LDIsCancelled", "LDIsCancelled")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Leads WHERE (BRID = @Original_BRID) AND (LDID = @Original_LDID) AND (" & _
        "LDIsCancelled = @Original_LDIsCancelled OR @Original_LDIsCancelled IS NULL AND L" & _
        "DIsCancelled IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDIsCancelled", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDIsCancelled", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=DEV1;packet size=4096;integrated security=SSPI;data source=""SERVER" & _
        "\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Leads (BRID, LDIsCancelled) VALUES (@BRID, @LDIsCancelled); SELECT BR" & _
        "ID, LDID, ID, LDDate, LDClientName, LDJobAddress, LDUser, LDStatus, LDIsCancelle" & _
        "d FROM VLeads WHERE (BRID = @BRID) AND (LDID = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDIsCancelled", System.Data.SqlDbType.Bit, 1, "LDIsCancelled"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, LDID, ID, LDDate, LDClientName, LDJobAddress, LDUser, LDStatus, LDIs" & _
        "Cancelled FROM VLeads WHERE (BRID = @BRID)"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Leads SET BRID = @BRID, LDIsCancelled = @LDIsCancelled WHERE (BRID = @Orig" & _
        "inal_BRID) AND (LDID = @Original_LDID) AND (LDIsCancelled = @Original_LDIsCancel" & _
        "led OR @Original_LDIsCancelled IS NULL AND LDIsCancelled IS NULL); SELECT BRID, " & _
        "LDID, ID, LDDate, LDClientName, LDJobAddress, LDUser, LDStatus, LDIsCancelled FR" & _
        "OM VLeads WHERE (BRID = @BRID) AND (LDID = @LDID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDIsCancelled", System.Data.SqlDbType.Bit, 1, "LDIsCancelled"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDIsCancelled", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDIsCancelled", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDID", System.Data.SqlDbType.BigInt, 8, "LDID"))
        '
        'DsGTMS
        '
        Me.DsGTMS.DataSetName = "dsGTMS"
        Me.DsGTMS.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'GridControl1
        '
        Me.GridControl1.ContextMenu = Me.cmListView
        Me.GridControl1.DataSource = Me.DsGTMS.VLeads
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'GridControl1.EmbeddedNavigator
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemComboBox1})
        Me.GridControl1.Size = New System.Drawing.Size(632, 392)
        Me.GridControl1.Styles.AddReplace("CardBorder", New DevExpress.Utils.ViewStyleEx("CardBorder", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.InactiveBorder, System.Drawing.SystemColors.WindowFrame, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("BandPanelBackground", New DevExpress.Utils.ViewStyleEx("BandPanelBackground", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ControlDark, System.Drawing.Color.DarkSalmon, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("EmptySpace", New DevExpress.Utils.ViewStyleEx("EmptySpace", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("FieldValue", New DevExpress.Utils.ViewStyleEx("FieldValue", "CardView", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("BandPanel", New DevExpress.Utils.ViewStyleEx("BandPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("CardButton", New DevExpress.Utils.ViewStyleEx("CardButton", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("FocusedCardCaption", New DevExpress.Utils.ViewStyleEx("FocusedCardCaption", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ActiveCaption, System.Drawing.SystemColors.ActiveCaptionText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("CardCaption", New DevExpress.Utils.ViewStyleEx("CardCaption", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.InactiveCaption, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("HeaderPanelBackground", New DevExpress.Utils.ViewStyleEx("HeaderPanelBackground", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ControlDark, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("SeparatorLine", New DevExpress.Utils.ViewStyleEx("SeparatorLine", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ActiveBorder, System.Drawing.SystemColors.ActiveBorder, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("FieldCaption", New DevExpress.Utils.ViewStyleEx("FieldCaption", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Top, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'cmListView
        '
        Me.cmListView.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.miEdit, Me.miDelete, Me.MenuItem3, Me.miUncancelJob})
        '
        'miEdit
        '
        Me.miEdit.Index = 0
        Me.miEdit.Text = "Edit Quote Request"
        '
        'miDelete
        '
        Me.miDelete.Index = 1
        Me.miDelete.Text = "Cancel Quote Request"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "-"
        '
        'miUncancelJob
        '
        Me.miUncancelJob.Index = 3
        Me.miUncancelJob.Text = "Uncancel Quote Request"
        '
        'GridView1
        '
        Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
        Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
        Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID, Me.colLDClientName, Me.colLDJobAddress, Me.colLDDate, Me.colLDUser, Me.colLDStatus})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        StyleFormatCondition1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Strikeout, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        StyleFormatCondition1.Appearance.ForeColor = System.Drawing.SystemColors.GrayText
        StyleFormatCondition1.Appearance.Options.UseFont = True
        StyleFormatCondition1.Appearance.Options.UseForeColor = True
        StyleFormatCondition1.ApplyToRow = True
        StyleFormatCondition1.Column = Me.colLDStatus
        StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal
        StyleFormatCondition1.Value1 = "Cancelled"
        Me.GridView1.FormatConditions.AddRange(New DevExpress.XtraGrid.StyleFormatCondition() {StyleFormatCondition1})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsNavigation.AutoFocusNewRow = True
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowGroupedColumns = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.OptionsView.ShowHorzLines = False
        Me.GridView1.OptionsView.ShowIndicator = False
        Me.GridView1.OptionsView.ShowVertLines = False
        Me.GridView1.RowHeight = 8
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colID, DevExpress.Data.ColumnSortOrder.Descending)})
        '
        'colID
        '
        Me.colID.Caption = "ID"
        Me.colID.FieldName = "ID"
        Me.colID.Name = "colID"
        Me.colID.Visible = True
        Me.colID.VisibleIndex = 0
        Me.colID.Width = 29
        '
        'colLDClientName
        '
        Me.colLDClientName.Caption = "Customer Name"
        Me.colLDClientName.FieldName = "LDClientName"
        Me.colLDClientName.Name = "colLDClientName"
        Me.colLDClientName.Visible = True
        Me.colLDClientName.VisibleIndex = 1
        Me.colLDClientName.Width = 123
        '
        'colLDJobAddress
        '
        Me.colLDJobAddress.Caption = "Job Address"
        Me.colLDJobAddress.FieldName = "LDJobAddress"
        Me.colLDJobAddress.Name = "colLDJobAddress"
        Me.colLDJobAddress.Visible = True
        Me.colLDJobAddress.VisibleIndex = 2
        Me.colLDJobAddress.Width = 171
        '
        'colLDDate
        '
        Me.colLDDate.Caption = "Enquiry Date"
        Me.colLDDate.FieldName = "LDDate"
        Me.colLDDate.Name = "colLDDate"
        Me.colLDDate.Visible = True
        Me.colLDDate.VisibleIndex = 4
        Me.colLDDate.Width = 91
        '
        'colLDUser
        '
        Me.colLDUser.Caption = "Recorded By"
        Me.colLDUser.FieldName = "LDUser"
        Me.colLDUser.Name = "colLDUser"
        Me.colLDUser.Visible = True
        Me.colLDUser.VisibleIndex = 3
        Me.colLDUser.Width = 108
        '
        'RepositoryItemComboBox1
        '
        Me.RepositoryItemComboBox1.AutoHeight = False
        Me.RepositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox1.Name = "RepositoryItemComboBox1"
        '
        'lvLeads
        '
        Me.Controls.Add(Me.GridControl1)
        Me.Name = "lvLeads"
        Me.Size = New System.Drawing.Size(632, 392)
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return hSqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            hSqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Public Function FillDataSet() As Integer Implements IListControl.FillDataSet
        ' Open connection
        Dim trans As SqlClient.SqlTransaction = Connection.BeginTransaction(IsolationLevel.ReadUncommitted)
        Power.Library.Library.ApplyTransactionToAllDataAdapters(trans, Me)
        'Me.GridControl1.DataSource.Clear()
        FillDataSet = SqlDataAdapter.Fill(DsGTMS)
        'Close connecton
        trans.Commit()
    End Function

    Private Sub GridView1_RowCountChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.RowCountChanged
        ExplorerForm.UpdateNumItems(NumItems)
        ExplorerForm.UpdateVisibleItems(VisibleItems)
    End Sub

    Public ReadOnly Property VisibleItems() As Integer Implements Power.Library.IListControl.VisibleItems
        Get
            Return GridView1.RowCount
        End Get
    End Property

    Public ReadOnly Property NumItems() As Integer Implements Power.Library.IListControl.NumItems
        Get
            Return DsGTMS.Tables(SqlDataAdapter.TableMappings(0).DataSetTable).Rows.Count
        End Get
    End Property

    Private hBRID As Integer
    Private Property BRID() As Integer
        Get
            Return hBRID
        End Get
        Set(ByVal Value As Integer)
            hBRID = Value
            SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = Value
        End Set
    End Property

    Public ReadOnly Property SelectedRow() As DataRow
        Get
            If Not GridView1.GetSelectedRows Is Nothing Then
                Return GridView1.GetDataRow(GridView1.GetSelectedRows(0))
            End If
        End Get
    End Property

    Public ReadOnly Property SelectedRowField(ByVal Field As String) As Object
        Get
            If Not SelectedRow Is Nothing Then
                Return SelectedRow.Item(Field)
            Else
                Return DBNull.Value
            End If
        End Get
    End Property

    Public Function SelectRow(ByVal BRID As Int32, ByVal LDID As Int64) As Boolean
        Dim i As Integer = 0
        Do Until False
            Try
                If GridView1.GetDataRow(GridView1.GetVisibleRowHandle(i))("BRID") = BRID And _
                        GridView1.GetDataRow(GridView1.GetVisibleRowHandle(i))("LDID") = LDID Then
                    GridView1.ClearSelection()
                    GridView1.SelectRow(GridView1.GetVisibleRowHandle(i))
                    Return True
                End If
                i += 1
            Catch ex As NullReferenceException
                Return False
            End Try
        Loop
        Return False
    End Function

#Region " New, Open and Delete "

    Private Sub GridControl1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridControl1.DoubleClick
        List_Edit()
    End Sub

    Public Sub List_New() Implements IListControl.List_New
        Dim c As Cursor = ParentForm.Cursor
        ParentForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            Dim gui As frmLead = frmLead.Add(BRID)
            gui.ShowDialog(Me)
            FillDataSet()
            SelectRow(BRID, gui.LDID)
        Catch ex As ObjectLockedException
            Message.CurrentlyAccessed("quote request")
        End Try
        ParentForm.Cursor = c
    End Sub

    Public Sub List_Edit() Implements IListControl.List_Edit
        If Not SelectedRow Is Nothing Then
            Dim c As Cursor = ParentForm.Cursor
            ParentForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If AppointmentExists(SelectedRowField("BRID"), SelectedRowField("LDID")) Then
                Try
                    Dim gui As frmLead = frmLead.Edit(SelectedRowField("BRID"), SelectedRowField("LDID"))
                    gui.ShowDialog(Me)
                    FillDataSet()
                Catch ex As ObjectLockedException
                    Message.CurrentlyAccessed("quote request")
                End Try
            Else
                Message.AlreadyDeleted("quote request", Message.ObjectAction.Edit)
                Me.GridControl1.DataSource.Clear()
                FillDataSet()
            End If
            ParentForm.Cursor = c
        End If
    End Sub

    Public Sub List_Delete() Implements IListControl.List_Delete
        If Not SelectedRow Is Nothing Then
            If Message.AskDeleteObject("quote request", Message.ObjectAction.Cancel) = MsgBoxResult.Yes Then
                If AppointmentExists(SelectedRowField("BRID"), SelectedRowField("LDID")) Then
                    Dim LDID As Long = SelectedRowField("LDID")
                    If DataAccess.spExecLockRequest("sp_GetLeadLock", BRID, LDID, Connection) Then
                        SelectedRow.Item("LDIsCancelled") = True
                        SqlDataAdapter.Update(DsGTMS)
                        DataAccess.spExecLockRequest("sp_ReleaseLeadLock", BRID, LDID, Connection)
                    Else
                        Message.CurrentlyAccessed("quote request")
                    End If
                Else
                    Message.AlreadyDeleted("quote request", Message.ObjectAction.Cancel)
                    Me.GridControl1.DataSource.Clear()
                    FillDataSet()
                End If
            End If
        End If
    End Sub

    Public Sub List_Undelete() Implements Power.Library.IListControl.List_Undelete
        If Not SelectedRow Is Nothing Then
            Dim LDID As Long = SelectedRowField("LDID")
            If DataAccess.spExecLockRequest("sp_GetLeadLock", BRID, LDID, Connection) Then
                SelectedRow.Item("LDIsCancelled") = False
                SqlDataAdapter.Update(DsGTMS)
                DataAccess.spExecLockRequest("sp_ReleaseLeadLock", BRID, LDID, Connection)
            Else
                Message.CurrentlyAccessed("quote request")
            End If
        End If
    End Sub

    Private Function AppointmentExists(ByVal BRID As Int32, ByVal LDID As Int64) As Boolean
        Dim cnn As SqlClient.SqlConnection = Connection
        Dim cmd As New SqlClient.SqlCommand("SELECT Count(ID) FROM Leads WHERE BRID = @BRID AND LDID = @LDID", cnn)
        cmd.CommandType = CommandType.Text
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@LDID", LDID)
        Return cmd.ExecuteScalar > 0
    End Function

#End Region

    Public Function CopyToJob() As Boolean
        If Not SelectedRow Is Nothing Then
            Dim c As Cursor = ParentForm.Cursor
            ParentForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If DataAccess.spExecLockRequest("sp_GetLeadLock", SelectedRowField("BRID"), SelectedRowField("LDID"), Connection) Then
                ' Nobody has this locked... release the lock now so that the copy can lock the lead and job
                DataAccess.spExecLockRequest("sp_ReleaseLeadLock", SelectedRowField("BRID"), SelectedRowField("LDID"), Connection)
                Dim gui As frmBooking = frmBooking.CopyFromLead(SelectedRowField("BRID"), SelectedRowField("LDID"))
                CopyToJob = gui.ShowDialog(Me) = DialogResult.OK
            Else
                Message.CurrentlyAccessed("quote request")
            End If
            ParentForm.Cursor = c
        End If
    End Function

    Private hExplorerForm As Form
    Public ReadOnly Property ExplorerForm() As frmExplorer
        Get
            Return hExplorerForm
        End Get
    End Property

#Region " Printing "

    Public Sub Print(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.Print
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.PrintDlg()
    End Sub

    Public Sub PrintPreview(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.PrintPreview
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.ShowPreview()
    End Sub

    Public Sub QuickPrint(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.QuickPrint
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.Print(PrintingSystem.PageSettings.PrinterName)
    End Sub

#End Region

    Private Sub cmListView_Popup(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmListView.Popup
        Me.miUncancelJob.Enabled = SelectedRow("LDIsCancelled")
    End Sub

    Private Sub miEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miEdit.Click
        List_Edit()
    End Sub

    Private Sub miUncancelJob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miUncancelJob.Click
        List_Undelete()
    End Sub

    Private Sub miDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miDelete.Click
        List_Delete()
    End Sub

    Public ReadOnly Property AllowDelete() As Boolean Implements Power.Library.IListControl.AllowDelete
        Get
            Return Not HasRole("branch_read_only")
        End Get
    End Property

    Public ReadOnly Property AllowEdit() As Boolean Implements Power.Library.IListControl.AllowEdit
        Get
            Return True
        End Get
    End Property

    Public ReadOnly Property AllowNew() As Boolean Implements Power.Library.IListControl.AllowNew
        Get
            Return Not HasRole("branch_read_only")
        End Get
    End Property

    Public ReadOnly Property AllowUndelete() As Boolean Implements Power.Library.IListControl.AllowUndelete
        Get
            Return Not HasRole("branch_read_only")
        End Get
    End Property

    Public ReadOnly Property DeleteCaption() As String Implements Power.Library.IListControl.DeleteCaption
        Get
            Return "&Cancel"
        End Get
    End Property

    Public ReadOnly Property UndeleteCaption() As String Implements Power.Library.IListControl.UndeleteCaption
        Get
            Return "&Uncancel"
        End Get
    End Property

End Class
