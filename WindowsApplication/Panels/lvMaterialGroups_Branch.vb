Imports Power.Forms

Public Class lvMaterialGroups_Branch
    Inherits System.Windows.Forms.UserControl
    Implements IListControl

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Connection As SqlClient.SqlConnection, ByVal BRID As Integer, ByVal ExplorerForm As frmExplorer)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.Connection = Connection
        Me.BRID = BRID
        hExplorerForm = ExplorerForm
        FillDataSet()
    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DsGTMS As WindowsApplication.dsGTMS
    Friend WithEvents hSqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colMGName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMGBStocked As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMGBCurrentPurchaseCost As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMGBCurrentWasteFactorAllowance As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents DataListView As DevExpress.XtraGrid.GridControl
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.hSqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.DsGTMS = New WindowsApplication.dsGTMS
        Me.DataListView = New DevExpress.XtraGrid.GridControl
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colMGName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colMGBStocked = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colMGBCurrentPurchaseCost = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colMGBCurrentWasteFactorAllowance = New DevExpress.XtraGrid.Columns.GridColumn
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataListView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'hSqlConnection
        '
        Me.hSqlConnection.ConnectionString = "packet size=4096;integrated security=SSPI;data source=SERVER;persist security inf" & _
        "o=False;initial catalog=GTMS_DEV"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VMaterialGroups_Branch_Display", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("MGID", "MGID")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM MaterialGroups_Branch WHERE (BRID = @Original_BRID) AND (MGID = @Orig" & _
        "inal_MGID)"
        Me.SqlDeleteCommand1.Connection = Me.hSqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO MaterialGroups_Branch(BRID, MGID) VALUES (@BRID, @MGID); SELECT BRID," & _
        " MGID FROM MaterialGroups_Branch WHERE (BRID = @BRID) AND (MGID = @MGID)"
        Me.SqlInsertCommand1.Connection = Me.hSqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, MGID, MGName, MGBStocked, MGBDiscontinued, MGBCurrentPurchaseCost, M" & _
        "GBCurrentWasteFactorAllowance FROM VMaterialGroups_Branch_Display WHERE (BRID = " & _
        "@BRID)"
        Me.SqlSelectCommand1.Connection = Me.hSqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Current, "0"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE MaterialGroups_Branch SET BRID = @BRID, MGID = @MGID WHERE (BRID = @Origin" & _
        "al_BRID) AND (MGID = @Original_MGID); SELECT BRID, MGID FROM MaterialGroups_Bran" & _
        "ch WHERE (BRID = @BRID) AND (MGID = @MGID)"
        Me.SqlUpdateCommand1.Connection = Me.hSqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGID", System.Data.DataRowVersion.Original, Nothing))
        '
        'DsGTMS
        '
        Me.DsGTMS.DataSetName = "dsGTMS"
        Me.DsGTMS.EnforceConstraints = False
        Me.DsGTMS.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'DataListView
        '
        Me.DataListView.DataMember = "VMaterialGroups_Branch_Display"
        Me.DataListView.DataSource = Me.DsGTMS
        Me.DataListView.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'DataListView.EmbeddedNavigator
        '
        Me.DataListView.EmbeddedNavigator.Name = ""
        Me.DataListView.Location = New System.Drawing.Point(0, 0)
        Me.DataListView.MainView = Me.GridView1
        Me.DataListView.Name = "DataListView"
        Me.DataListView.Size = New System.Drawing.Size(632, 392)
        Me.DataListView.TabIndex = 1
        Me.DataListView.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
        Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
        Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colMGName, Me.colMGBStocked, Me.colMGBCurrentPurchaseCost, Me.colMGBCurrentWasteFactorAllowance})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.GridView1.GridControl = Me.DataListView
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsNavigation.AutoFocusNewRow = True
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.OptionsView.ShowHorzLines = False
        Me.GridView1.OptionsView.ShowIndicator = False
        Me.GridView1.OptionsView.ShowVertLines = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colMGName, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colMGName
        '
        Me.colMGName.Caption = "Name"
        Me.colMGName.FieldName = "MGName"
        Me.colMGName.Name = "colMGName"
        Me.colMGName.Visible = True
        Me.colMGName.VisibleIndex = 0
        '
        'colMGBStocked
        '
        Me.colMGBStocked.Caption = "Stocked"
        Me.colMGBStocked.FieldName = "MGBStocked"
        Me.colMGBStocked.Name = "colMGBStocked"
        Me.colMGBStocked.OptionsColumn.ReadOnly = True
        Me.colMGBStocked.Visible = True
        Me.colMGBStocked.VisibleIndex = 1
        '
        'colMGBCurrentPurchaseCost
        '
        Me.colMGBCurrentPurchaseCost.Caption = "Purchase Cost"
        Me.colMGBCurrentPurchaseCost.FieldName = "MGBCurrentPurchaseCost"
        Me.colMGBCurrentPurchaseCost.Name = "colMGBCurrentPurchaseCost"
        Me.colMGBCurrentPurchaseCost.OptionsColumn.ReadOnly = True
        Me.colMGBCurrentPurchaseCost.Visible = True
        Me.colMGBCurrentPurchaseCost.VisibleIndex = 2
        '
        'colMGBCurrentWasteFactorAllowance
        '
        Me.colMGBCurrentWasteFactorAllowance.Caption = "Waste Factor Allowance"
        Me.colMGBCurrentWasteFactorAllowance.FieldName = "MGBCurrentWasteFactorAllowance"
        Me.colMGBCurrentWasteFactorAllowance.Name = "colMGBCurrentWasteFactorAllowance"
        Me.colMGBCurrentWasteFactorAllowance.OptionsColumn.ReadOnly = True
        Me.colMGBCurrentWasteFactorAllowance.Visible = True
        Me.colMGBCurrentWasteFactorAllowance.VisibleIndex = 3
        '
        'lvMaterialGroups_Branch
        '
        Me.Controls.Add(Me.DataListView)
        Me.Name = "lvMaterialGroups_Branch"
        Me.Size = New System.Drawing.Size(632, 392)
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataListView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return hSqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            hSqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Public Function FillDataSet() As Integer Implements IListControl.FillDataSet
        ' Open connection
        Dim trans As SqlClient.SqlTransaction = Connection.BeginTransaction(IsolationLevel.ReadUncommitted)
        Power.Library.Library.ApplyTransactionToAllDataAdapters(trans, Me)
        'Me.GridControl1.DataSource.Clear()
        FillDataSet = SqlDataAdapter.Fill(DsGTMS)
        'Close connecton
        trans.Commit()
    End Function

    Private Sub GridView1_RowCountChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.RowCountChanged
        ExplorerForm.UpdateNumItems(NumItems)
        ExplorerForm.UpdateVisibleItems(VisibleItems)
    End Sub

    Public ReadOnly Property VisibleItems() As Integer Implements Power.Library.IListControl.VisibleItems
        Get
            Return GridView1.RowCount
        End Get
    End Property

    Public ReadOnly Property NumItems() As Integer Implements Power.Library.IListControl.NumItems
        Get
            Return DsGTMS.Tables(SqlDataAdapter.TableMappings(0).DataSetTable).Rows.Count
        End Get
    End Property

    Private hBRID As Integer
    Private Property BRID() As Integer
        Get
            Return hBRID
        End Get
        Set(ByVal Value As Integer)
            hBRID = Value
            SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = Value
        End Set
    End Property

    Public ReadOnly Property SelectedRow() As DataRow
        Get
            If Not GridView1.GetSelectedRows Is Nothing Then
                Return GridView1.GetDataRow(GridView1.GetSelectedRows(0))
            End If
        End Get
    End Property

    Public ReadOnly Property SelectedRowField(ByVal Field As String) As Object
        Get
            If Not SelectedRow Is Nothing Then
                Return SelectedRow.Item(Field)
            Else
                Return DBNull.Value
            End If
        End Get
    End Property

    Public Function SelectRow(ByVal BRID As Int32, ByVal EXID As Int64) As Boolean
        Dim i As Integer = 0
        Do Until False
            Try
                If GridView1.GetDataRow(GridView1.GetVisibleRowHandle(i))("BRID") = BRID And _
                        GridView1.GetDataRow(GridView1.GetVisibleRowHandle(i))("EXID") = EXID Then
                    GridView1.ClearSelection()
                    GridView1.SelectRow(GridView1.GetVisibleRowHandle(i))
                    Return True
                End If
                i += 1
            Catch ex As NullReferenceException
                Return False
            End Try
        Loop
        Return False
    End Function

#Region " New, Open and Delete "
    Private Sub DataListView_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataListView.DoubleClick

        List_Edit()
    End Sub

    Public Sub List_New() Implements IListControl.List_New

    End Sub

    Public Sub List_Edit() Implements IListControl.List_Edit

    End Sub

    Public Sub List_Delete() Implements IListControl.List_Delete

    End Sub

#End Region

    Private hExplorerForm As Form
    Public ReadOnly Property ExplorerForm() As frmExplorer
        Get
            Return hExplorerForm
        End Get
    End Property

    Public ReadOnly Property AllowDelete() As Boolean Implements Power.Library.IListControl.AllowDelete
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property AllowEdit() As Boolean Implements Power.Library.IListControl.AllowEdit
        Get
            Return True
        End Get
    End Property

    Public ReadOnly Property AllowNew() As Boolean Implements Power.Library.IListControl.AllowNew
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property AllowUndelete() As Boolean Implements Power.Library.IListControl.AllowUndelete
        Get
            Return False
        End Get
    End Property

    Public Sub List_Undelete() Implements Power.Library.IListControl.List_Undelete

    End Sub

    Public ReadOnly Property DeleteCaption() As String Implements Power.Library.IListControl.DeleteCaption
        Get
            Return Nothing
        End Get
    End Property

    Public ReadOnly Property UndeleteCaption() As String Implements Power.Library.IListControl.UndeleteCaption
        Get
            Return Nothing
        End Get
    End Property

End Class
