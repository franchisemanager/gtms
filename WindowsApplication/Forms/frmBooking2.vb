Public Class frmBooking2
    Inherits DevExpress.XtraEditors.XtraForm

    Private dataRow As DataRow
    'Public scheduledStartDate As DateTime
    'Public scheduledEndDate As DateTime
    'Public dateJBID As Int32

    Public Shared Sub Add(ByVal BRID As Int32)
        Dim gui As New frmBooking2

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            .LoadTreeViews(BRID)
            .FillPreliminaryData(BRID)

            .dataRow = .dataSet.Jobs.NewRow()
            .dataRow("BRID") = BRID
            .dataRow("ID") = sp_GetNextID(BRID, .SqlConnection)
            .dataRow("JBBookingDate") = Today.Date
            .dataSet.Jobs.Rows.Add(.dataRow)

            .FillInitData()

            gui.ShowDialog()

            gui.ReleaseAppointmentLocks()
            gui.ReleaseOrderLocks()

            .SqlConnection.Close()
        End With
    End Sub

    Public Shared Function Edit(ByVal BRID As Int32, ByRef JBID As Int64) As frmJob2
        Dim gui As New frmBooking2

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            If DataAccess.spExecLockRequest("sp_GetJobLock", BRID, JBID, .SqlConnection) Then

                .LoadTreeViews(BRID)
                .FillPreliminaryData(BRID)

                .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = BRID
                .SqlDataAdapter.SelectCommand.Parameters("@JBID").Value = JBID
                .SqlDataAdapter.Fill(.dataSet)
                .dataRow = .dataSet.Jobs(0)

                .FillData()

                gui.ShowDialog()

                gui.ReleaseAppointmentLocks()
                gui.ReleaseOrderLocks()
                DataAccess.spExecLockRequest("sp_ReleaseJobLock", BRID, JBID, .SqlConnection)
            Else
                Message.CurrentlyAccessed("job")
            End If

            .SqlConnection.Close()
        End With
    End Function

    Public Shared Function CopyFromLead(ByVal BRID As Integer, ByVal LDID As Int64) As DialogResult
        Dim gui As New frmBooking2

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            If DataAccess.spExecLockRequest("sp_GetLeadLock", BRID, LDID, .SqlConnection) Then

                .LoadTreeViews(BRID)
                .FillPreliminaryData(BRID)

                If ExecuteScalar("SELECT Count(JBID) FROM Jobs J INNER JOIN Leads L ON J.BRID = L.BRID AND J.ID = L.ID WHERE L.BRID = @BRID AND L.LDID = @LDID", _
                                CommandType.Text, New SqlClient.SqlParameter() {New SqlClient.SqlParameter("@BRID", BRID), New SqlClient.SqlParameter("@LDID", LDID)}, .SqlConnection) = 0 Then
                    .daLeads.SelectCommand.Parameters("@BRID").Value = BRID
                    .daLeads.SelectCommand.Parameters("@LDID").Value = LDID
                    .daLeads.Fill(.dataSet)
                    Dim lead As DataRow = .dataSet.Leads(0)

                    .dataRow = .dataSet.Jobs.NewRow()
                    .dataRow("BRID") = lead("BRID")
                    .dataRow("ID") = lead("ID")
                    .dataRow("JBClientFirstName") = lead("LDClientFirstName")
                    .dataRow("JBClientSurname") = lead("LDClientSurname")
                    .dataRow("JBClientStreetAddress01") = lead("LDClientStreetAddress01")
                    .dataRow("JBClientStreetAddress02") = lead("LDClientStreetAddress02")
                    .dataRow("JBClientSuburb") = lead("LDClientSuburb")
                    .dataRow("JBClientState") = lead("LDClientState")
                    .dataRow("JBClientPostCode") = lead("LDClientPostCode")
                    .dataRow("JBClientHomePhoneNumber") = lead("LDClientHomePhoneNumber")
                    .dataRow("JBClientWorkPhoneNumber") = lead("LDClientWorkPhoneNumber")
                    .dataRow("JBClientFaxNumber") = lead("LDClientFaxNumber")
                    .dataRow("JBClientMobileNumber") = lead("LDClientMobileNumber")
                    .dataRow("JBJobAddressAsAbove") = lead("LDJobAddressAsAbove")
                    .dataRow("JBJobStreetAddress01") = lead("LDJobStreetAddress01")
                    .dataRow("JBJobStreetAddress02") = lead("LDJobStreetAddress02")
                    .dataRow("JBJobSuburb") = lead("LDJobSuburb")
                    .dataRow("JBJobState") = lead("LDJobState")
                    .dataRow("JBJobPostCode") = lead("LDJobPostCode")
                    .dataRow("JBReferenceName") = lead("LDReferenceName")
                    .dataRow("JBJobDescription") = lead("LDJobDescription")
                    .dataRow("JBClientEmail") = lead("LDClientEmail")
                    .dataRow("CTID") = lead("CTID")
                    .dataRow("JBUseContact") = lead("LDUseContact")
                    .dataRow("EXIDRep") = lead("EXIDRep")
                    .dataRow("JBBookingDate") = Today.Date


                    .dataSet.Jobs.Rows.Add(.dataRow)

                    .FillCopiedData()
                Else
                    Message.ShowMessage("This quote request has already been converted to a job.", MessageBoxIcon.Exclamation)

                    Dim JBID As Int64 = ExecuteScalar("SELECT JBID FROM Jobs J INNER JOIN Leads L ON J.BRID = L.BRID AND J.ID = L.ID WHERE L.BRID = @BRID AND L.LDID = @LDID", _
                                CommandType.Text, New SqlClient.SqlParameter() {New SqlClient.SqlParameter("@BRID", BRID), New SqlClient.SqlParameter("@LDID", LDID)}, .SqlConnection)
                    .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = BRID
                    .SqlDataAdapter.SelectCommand.Parameters("@JBID").Value = JBID
                    .SqlDataAdapter.Fill(.dataSet)
                    .dataRow = .dataSet.Jobs(0)

                    .FillData()
                End If

                CopyFromLead = gui.ShowDialog()

                gui.ReleaseAppointmentLocks()

                DataAccess.spExecLockRequest("sp_ReleaseLeadLock", BRID, LDID, .SqlConnection)
            Else
                Message.CurrentlyAccessed("quote request")
            End If

            .SqlConnection.Close()
        End With
    End Function

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        ReadMRU(txtJBJobDescription, UserAppDataPath)
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupLine4 As Power.Forms.GroupLine
    Friend WithEvents GroupLine3 As Power.Forms.GroupLine
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupLine1 As Power.Forms.GroupLine
    Friend WithEvents GroupLine2 As Power.Forms.GroupLine
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents colNIName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIUserType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents GroupLine18 As Power.Forms.GroupLine
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents GroupLine20 As Power.Forms.GroupLine
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents GroupLine21 As Power.Forms.GroupLine
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents ListBox As Power.Forms.ListBox
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents txtEXIDBookingMeasurer As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents rgJBJobAddressAsAbove As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents txtJBJobStreetAddress02 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBJobSuburb As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBJobState As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBJobPostCode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBJobStreetAddress01 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientSurname As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientFirstName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientStreetAddress02 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientStreetAddress01 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientSuburb As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientState As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientPostCode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents colNIPaidByClient1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIOrderedByClient1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIDesc1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIName1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents RepositoryItemLookUpEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents dvAppliances As System.Data.DataView
    Friend WithEvents dgAppliances As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvAppliances As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colNIName2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIUserType1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIOrderedBy As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIPaidBy As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnAddAppliance As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEditAppliance As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveAppliance As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colNIBrand As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnAddOtherTrade As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gvOtherTrades As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btnEditOtherTrade As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveOtherTrade As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dvOtherTrades As System.Data.DataView
    Friend WithEvents dgOtherTrades As DevExpress.XtraGrid.GridControl
    Friend WithEvents colNIOrderedBy1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIPaidBy1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnAddAppointment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEditAppointment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveAppointment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblJobState As System.Windows.Forms.Label
    Friend WithEvents lblJobStreetAddress As System.Windows.Forms.Label
    Friend WithEvents lblJobSuburb As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents RadioGroup1 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents chkJBIsAllOrderingComplete As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents PnlJobInformation As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlJobAddress As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlScheduling As DevExpress.XtraEditors.GroupControl
    Friend WithEvents PnlOtherTrades As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlAppliances As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlClientInformation As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtJBJobDescription As DevExpress.XtraEditors.MRUEdit
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtJBClientFaxNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientMobileNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtJBClientEmail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents colAPBegin As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEXName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dgAppointments As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvAppointments As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btnHelp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents pnlMain As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnViewCalendar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupLine9 As Power.Forms.GroupLine
    Friend WithEvents GroupLine8 As Power.Forms.GroupLine
    Friend WithEvents txtJBPriceQuoted As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupLine5 As Power.Forms.GroupLine
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cmHistory As System.Windows.Forms.ContextMenu
    Friend WithEvents miClearHistory As System.Windows.Forms.MenuItem
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents dvAppointments As System.Data.DataView
    Friend WithEvents DsCalendar As WindowsApplication.dsCalendar
    Friend WithEvents daExpenses As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents daAppointments As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Storage As DevExpress.XtraScheduler.SchedulerStorage
    Friend WithEvents daTasks As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents colTAName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dvResources As System.Data.DataView
    Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents dvSalesReps As System.Data.DataView
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents dvExpenses As System.Data.DataView
    Friend WithEvents pnlNotes As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblJobPostCode As System.Windows.Forms.Label
    Friend WithEvents txtLDUser As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents dpLDDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dataSet As WindowsApplication.dsJobs
    Friend WithEvents daLeads As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DsNotes As WindowsApplication.dsNotes
    Friend WithEvents dvExpenses2 As System.Data.DataView
    Friend WithEvents daINFollowUpTypes As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daIDNotes As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents dgNotes As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvNotes As DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
    Friend WithEvents GridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents colINUser As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents txtEXID As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents colINDate As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents txtINDate As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents colINNotes As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents txtINNotes As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents colINFollowUpText As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents txtINFollowUpText As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents dvMeasurers As System.Data.DataView
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents rgLDUseContactFalse As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents pceContact As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents rgLDUseContactTrue As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents daContacts As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents pccContacts As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents txtJBReferenceName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientHomePhoneNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientWorkPhoneNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents pnlJobPurchaseOrders As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnRemoveJobPurchaseOrder As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEditJobPurchaseOrder As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAddJobPurchaseOrder As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dvInternalOrders As System.Data.DataView
    Friend WithEvents dvJobPurchaseOrders As System.Data.DataView
    Friend WithEvents dgJobPurchaseOrders As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvJobPurchaseOrders As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents pnlInternalOrders As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnRemoveInternalOrder As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEditInternalOrder As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAddInternalOrder As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dgInternalOrders As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvInternalOrders As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colOROrderNumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colORDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colORDescription As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents daOrders_Materials_SubItems As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daOrders As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daOrders_Materials As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand12 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daOrders_StockedItems As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daMaterials As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommand13 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand14 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daStockedItems As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommand15 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand16 As System.Data.SqlClient.SqlCommand
    Friend WithEvents DsOrders As WindowsApplication.dsOrders
    Friend WithEvents daJobNonCoreSalesItems As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand9 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmBooking2))
        Me.RadioGroup1 = New DevExpress.XtraEditors.RadioGroup
        Me.dataSet = New WindowsApplication.dsJobs
        Me.Label5 = New System.Windows.Forms.Label
        Me.chkJBIsAllOrderingComplete = New DevExpress.XtraEditors.CheckEdit
        Me.GroupLine1 = New Power.Forms.GroupLine
        Me.txtEXIDBookingMeasurer = New DevExpress.XtraEditors.LookUpEdit
        Me.dvMeasurers = New System.Data.DataView
        Me.DsCalendar = New WindowsApplication.dsCalendar
        Me.dvResources = New System.Data.DataView
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.GroupLine2 = New Power.Forms.GroupLine
        Me.Label15 = New System.Windows.Forms.Label
        Me.GroupLine3 = New Power.Forms.GroupLine
        Me.GroupLine4 = New Power.Forms.GroupLine
        Me.btnAddOtherTrade = New DevExpress.XtraEditors.SimpleButton
        Me.dgOtherTrades = New DevExpress.XtraGrid.GridControl
        Me.dvOtherTrades = New System.Data.DataView
        Me.gvOtherTrades = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colNIName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIUserType = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIOrderedBy1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIPaidBy1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.btnEditOtherTrade = New DevExpress.XtraEditors.SimpleButton
        Me.btnRemoveOtherTrade = New DevExpress.XtraEditors.SimpleButton
        Me.dvAppliances = New System.Data.DataView
        Me.btnAddAppliance = New DevExpress.XtraEditors.SimpleButton
        Me.dgAppliances = New DevExpress.XtraGrid.GridControl
        Me.gvAppliances = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colNIUserType1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIBrand = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIName2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIOrderedBy = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIPaidBy = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.btnEditAppliance = New DevExpress.XtraEditors.SimpleButton
        Me.btnRemoveAppliance = New DevExpress.XtraEditors.SimpleButton
        Me.btnAddAppointment = New DevExpress.XtraEditors.SimpleButton
        Me.btnEditAppointment = New DevExpress.XtraEditors.SimpleButton
        Me.btnRemoveAppointment = New DevExpress.XtraEditors.SimpleButton
        Me.txtJBClientSurname = New DevExpress.XtraEditors.TextEdit
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.GroupLine18 = New Power.Forms.GroupLine
        Me.Label36 = New System.Windows.Forms.Label
        Me.GroupLine20 = New Power.Forms.GroupLine
        Me.Label37 = New System.Windows.Forms.Label
        Me.Label38 = New System.Windows.Forms.Label
        Me.Label39 = New System.Windows.Forms.Label
        Me.GroupLine21 = New Power.Forms.GroupLine
        Me.Label40 = New System.Windows.Forms.Label
        Me.Label41 = New System.Windows.Forms.Label
        Me.txtJBClientFirstName = New DevExpress.XtraEditors.TextEdit
        Me.txtJBReferenceName = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientStreetAddress02 = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientStreetAddress01 = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientSuburb = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientHomePhoneNumber = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientFaxNumber = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientMobileNumber = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientState = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientPostCode = New DevExpress.XtraEditors.TextEdit
        Me.pnlScheduling = New DevExpress.XtraEditors.GroupControl
        Me.btnViewCalendar = New DevExpress.XtraEditors.SimpleButton
        Me.GroupLine9 = New Power.Forms.GroupLine
        Me.GroupLine8 = New Power.Forms.GroupLine
        Me.Label7 = New System.Windows.Forms.Label
        Me.dgAppointments = New DevExpress.XtraGrid.GridControl
        Me.dvAppointments = New System.Data.DataView
        Me.gvAppointments = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colAPBegin = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colEXName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colTAName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.PnlOtherTrades = New DevExpress.XtraEditors.GroupControl
        Me.pnlJobPurchaseOrders = New DevExpress.XtraEditors.GroupControl
        Me.dgJobPurchaseOrders = New DevExpress.XtraGrid.GridControl
        Me.dvJobPurchaseOrders = New System.Data.DataView
        Me.DsOrders = New WindowsApplication.dsOrders
        Me.gvJobPurchaseOrders = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.btnRemoveJobPurchaseOrder = New DevExpress.XtraEditors.SimpleButton
        Me.btnEditJobPurchaseOrder = New DevExpress.XtraEditors.SimpleButton
        Me.btnAddJobPurchaseOrder = New DevExpress.XtraEditors.SimpleButton
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.pnlAppliances = New DevExpress.XtraEditors.GroupControl
        Me.PnlJobInformation = New DevExpress.XtraEditors.GroupControl
        Me.dpLDDate = New DevExpress.XtraEditors.DateEdit
        Me.txtLDUser = New DevExpress.XtraEditors.LookUpEdit
        Me.dvExpenses = New System.Data.DataView
        Me.Label22 = New System.Windows.Forms.Label
        Me.LookUpEdit1 = New DevExpress.XtraEditors.LookUpEdit
        Me.dvSalesReps = New System.Data.DataView
        Me.Label21 = New System.Windows.Forms.Label
        Me.txtJBPriceQuoted = New DevExpress.XtraEditors.TextEdit
        Me.Label8 = New System.Windows.Forms.Label
        Me.GroupLine5 = New Power.Forms.GroupLine
        Me.txtJBJobDescription = New DevExpress.XtraEditors.MRUEdit
        Me.cmHistory = New System.Windows.Forms.ContextMenu
        Me.miClearHistory = New System.Windows.Forms.MenuItem
        Me.Label19 = New System.Windows.Forms.Label
        Me.pnlJobAddress = New DevExpress.XtraEditors.GroupControl
        Me.lblJobPostCode = New System.Windows.Forms.Label
        Me.txtJBJobStreetAddress01 = New DevExpress.XtraEditors.TextEdit
        Me.rgJBJobAddressAsAbove = New DevExpress.XtraEditors.RadioGroup
        Me.Label17 = New System.Windows.Forms.Label
        Me.lblJobState = New System.Windows.Forms.Label
        Me.lblJobStreetAddress = New System.Windows.Forms.Label
        Me.lblJobSuburb = New System.Windows.Forms.Label
        Me.txtJBJobStreetAddress02 = New DevExpress.XtraEditors.TextEdit
        Me.txtJBJobSuburb = New DevExpress.XtraEditors.TextEdit
        Me.txtJBJobState = New DevExpress.XtraEditors.TextEdit
        Me.txtJBJobPostCode = New DevExpress.XtraEditors.TextEdit
        Me.pnlClientInformation = New DevExpress.XtraEditors.GroupControl
        Me.rgLDUseContactFalse = New DevExpress.XtraEditors.RadioGroup
        Me.Label3 = New System.Windows.Forms.Label
        Me.pceContact = New DevExpress.XtraEditors.PopupContainerEdit
        Me.pccContacts = New DevExpress.XtraEditors.PopupContainerControl
        Me.rgLDUseContactTrue = New DevExpress.XtraEditors.RadioGroup
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtJBClientWorkPhoneNumber = New DevExpress.XtraEditors.TextEdit
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtJBClientEmail = New DevExpress.XtraEditors.TextEdit
        Me.Label20 = New System.Windows.Forms.Label
        Me.ListBox = New Power.Forms.ListBox
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.colNIPaidByClient1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIOrderedByClient1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIDesc1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIName1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemLookUpEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.btnHelp = New DevExpress.XtraEditors.SimpleButton
        Me.pnlMain = New DevExpress.XtraEditors.PanelControl
        Me.pnlNotes = New DevExpress.XtraEditors.GroupControl
        Me.dgNotes = New DevExpress.XtraGrid.GridControl
        Me.DsNotes = New WindowsApplication.dsNotes
        Me.gvNotes = New DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
        Me.GridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.colINUser = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.txtEXID = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.dvExpenses2 = New System.Data.DataView
        Me.colINDate = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.txtINDate = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.colINNotes = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.txtINNotes = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
        Me.colINFollowUpText = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.txtINFollowUpText = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
        Me.pnlInternalOrders = New DevExpress.XtraEditors.GroupControl
        Me.btnRemoveInternalOrder = New DevExpress.XtraEditors.SimpleButton
        Me.btnEditInternalOrder = New DevExpress.XtraEditors.SimpleButton
        Me.btnAddInternalOrder = New DevExpress.XtraEditors.SimpleButton
        Me.dgInternalOrders = New DevExpress.XtraGrid.GridControl
        Me.dvInternalOrders = New System.Data.DataView
        Me.gvInternalOrders = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colOROrderNumber = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colORDate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colORDescription = New DevExpress.XtraGrid.Columns.GridColumn
        Me.daExpenses = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.daAppointments = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.Storage = New DevExpress.XtraScheduler.SchedulerStorage(Me.components)
        Me.daTasks = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand2 = New System.Data.SqlClient.SqlCommand
        Me.daLeads = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand
        Me.daINFollowUpTypes = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand6 = New System.Data.SqlClient.SqlCommand
        Me.daIDNotes = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand6 = New System.Data.SqlClient.SqlCommand
        Me.daContacts = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.daOrders_Materials_SubItems = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand10 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand10 = New System.Data.SqlClient.SqlCommand
        Me.daOrders = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand9 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand10 = New System.Data.SqlClient.SqlCommand
        Me.daOrders_Materials = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand11 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand12 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand8 = New System.Data.SqlClient.SqlCommand
        Me.daOrders_StockedItems = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand11 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand10 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand10 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand11 = New System.Data.SqlClient.SqlCommand
        Me.daMaterials = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlCommand13 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand14 = New System.Data.SqlClient.SqlCommand
        Me.daStockedItems = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlCommand15 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand16 = New System.Data.SqlClient.SqlCommand
        Me.daJobNonCoreSalesItems = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand9 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand11 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand11 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand9 = New System.Data.SqlClient.SqlCommand
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkJBIsAllOrderingComplete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEXIDBookingMeasurer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvMeasurers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsCalendar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvResources, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgOtherTrades, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvOtherTrades, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvOtherTrades, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvAppliances, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgAppliances, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvAppliances, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientFirstName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBReferenceName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientSuburb.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientHomePhoneNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientFaxNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientMobileNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientState.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientPostCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlScheduling, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlScheduling.SuspendLayout()
        CType(Me.dgAppointments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvAppointments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvAppointments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PnlOtherTrades, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PnlOtherTrades.SuspendLayout()
        CType(Me.pnlJobPurchaseOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlJobPurchaseOrders.SuspendLayout()
        CType(Me.dgJobPurchaseOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvJobPurchaseOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvJobPurchaseOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlAppliances, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlAppliances.SuspendLayout()
        CType(Me.PnlJobInformation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PnlJobInformation.SuspendLayout()
        CType(Me.dpLDDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDUser.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvExpenses, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvSalesReps, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBPriceQuoted.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBJobDescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlJobAddress, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlJobAddress.SuspendLayout()
        CType(Me.txtJBJobStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgJBJobAddressAsAbove.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBJobStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBJobSuburb.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBJobState.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBJobPostCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlClientInformation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlClientInformation.SuspendLayout()
        CType(Me.rgLDUseContactFalse.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pceContact.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pccContacts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgLDUseContactTrue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientWorkPhoneNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemLookUpEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMain.SuspendLayout()
        CType(Me.pnlNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlNotes.SuspendLayout()
        CType(Me.dgNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEXID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvExpenses2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtINDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtINNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtINFollowUpText, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlInternalOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlInternalOrders.SuspendLayout()
        CType(Me.dgInternalOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvInternalOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvInternalOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Storage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RadioGroup1
        '
        Me.RadioGroup1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBIsMeasureOnSite"))
        Me.RadioGroup1.Location = New System.Drawing.Point(352, 176)
        Me.RadioGroup1.Name = "RadioGroup1"
        '
        'RadioGroup1.Properties
        '
        Me.RadioGroup1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.RadioGroup1.Properties.Appearance.Options.UseBackColor = True
        Me.RadioGroup1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.RadioGroup1.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "On site"), New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "Office")})
        Me.RadioGroup1.Size = New System.Drawing.Size(64, 40)
        Me.RadioGroup1.TabIndex = 3
        '
        'dataSet
        '
        Me.dataSet.DataSetName = "dsJobs"
        Me.dataSet.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(48, 328)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(464, 16)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Tick ""All ordering complete"" when all orders (both internal and external) have be" & _
        "en placed."
        '
        'chkJBIsAllOrderingComplete
        '
        Me.chkJBIsAllOrderingComplete.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBIsAllOrderingComplete"))
        Me.chkJBIsAllOrderingComplete.Location = New System.Drawing.Point(136, 352)
        Me.chkJBIsAllOrderingComplete.Name = "chkJBIsAllOrderingComplete"
        '
        'chkJBIsAllOrderingComplete.Properties
        '
        Me.chkJBIsAllOrderingComplete.Properties.Caption = "All ordering complete"
        Me.chkJBIsAllOrderingComplete.Size = New System.Drawing.Size(176, 19)
        Me.chkJBIsAllOrderingComplete.TabIndex = 5
        '
        'GroupLine1
        '
        Me.GroupLine1.Location = New System.Drawing.Point(24, 384)
        Me.GroupLine1.Name = "GroupLine1"
        Me.GroupLine1.Size = New System.Drawing.Size(480, 16)
        Me.GroupLine1.TabIndex = 4
        Me.GroupLine1.TextString = "Recorded By / Date"
        Me.GroupLine1.TextWidth = 110
        '
        'txtEXIDBookingMeasurer
        '
        Me.txtEXIDBookingMeasurer.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.EXIDBookingMeasurer"))
        Me.txtEXIDBookingMeasurer.Location = New System.Drawing.Point(200, 152)
        Me.txtEXIDBookingMeasurer.Name = "txtEXIDBookingMeasurer"
        '
        'txtEXIDBookingMeasurer.Properties
        '
        Me.txtEXIDBookingMeasurer.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True
        Me.txtEXIDBookingMeasurer.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtEXIDBookingMeasurer.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXName", "Name", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.txtEXIDBookingMeasurer.Properties.DataSource = Me.dvMeasurers
        Me.txtEXIDBookingMeasurer.Properties.DisplayMember = "EXName"
        Me.txtEXIDBookingMeasurer.Properties.NullText = ""
        Me.txtEXIDBookingMeasurer.Properties.ShowFooter = False
        Me.txtEXIDBookingMeasurer.Properties.ShowHeader = False
        Me.txtEXIDBookingMeasurer.Properties.ShowLines = False
        Me.txtEXIDBookingMeasurer.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.txtEXIDBookingMeasurer.Properties.ValueMember = "EXID"
        Me.txtEXIDBookingMeasurer.Size = New System.Drawing.Size(176, 20)
        Me.txtEXIDBookingMeasurer.TabIndex = 2
        '
        'dvMeasurers
        '
        Me.dvMeasurers.RowFilter = "EXAppearInMeasurer = 1"
        Me.dvMeasurers.Sort = "EXName"
        Me.dvMeasurers.Table = Me.dataSet.VExpenses
        '
        'DsCalendar
        '
        Me.DsCalendar.DataSetName = "dsCalendar"
        Me.DsCalendar.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'dvResources
        '
        Me.dvResources.Sort = "EGName, EXName"
        Me.dvResources.Table = Me.DsCalendar.VExpenses
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(48, 152)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(136, 21)
        Me.Label12.TabIndex = 2
        Me.Label12.Text = "Templated/measured by:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(48, 72)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(96, 21)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Job description:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(312, 408)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 21)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Date:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(48, 184)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(304, 21)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Will the template/measure be done on site or in the office?"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine2
        '
        Me.GroupLine2.Location = New System.Drawing.Point(24, 24)
        Me.GroupLine2.Name = "GroupLine2"
        Me.GroupLine2.Size = New System.Drawing.Size(480, 16)
        Me.GroupLine2.TabIndex = 4
        Me.GroupLine2.TextString = "Job Description"
        Me.GroupLine2.TextWidth = 82
        '
        'Label15
        '
        Me.Label15.Location = New System.Drawing.Point(48, 48)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(432, 16)
        Me.Label15.TabIndex = 7
        Me.Label15.Text = "This is entered at the appointment stage, and may be changed in the booking stage" & _
        "."
        '
        'GroupLine3
        '
        Me.GroupLine3.Location = New System.Drawing.Point(24, 104)
        Me.GroupLine3.Name = "GroupLine3"
        Me.GroupLine3.Size = New System.Drawing.Size(480, 16)
        Me.GroupLine3.TabIndex = 4
        Me.GroupLine3.TextString = "Templater/Measure and Principal Salesperson Information"
        Me.GroupLine3.TextWidth = 300
        '
        'GroupLine4
        '
        Me.GroupLine4.Location = New System.Drawing.Point(24, 304)
        Me.GroupLine4.Name = "GroupLine4"
        Me.GroupLine4.Size = New System.Drawing.Size(480, 16)
        Me.GroupLine4.TabIndex = 4
        Me.GroupLine4.TextString = "Ordering"
        Me.GroupLine4.TextWidth = 50
        '
        'btnAddOtherTrade
        '
        Me.btnAddOtherTrade.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddOtherTrade.Image = CType(resources.GetObject("btnAddOtherTrade.Image"), System.Drawing.Image)
        Me.btnAddOtherTrade.Location = New System.Drawing.Point(16, 352)
        Me.btnAddOtherTrade.Name = "btnAddOtherTrade"
        Me.btnAddOtherTrade.Size = New System.Drawing.Size(72, 23)
        Me.btnAddOtherTrade.TabIndex = 1
        Me.btnAddOtherTrade.Text = "Add..."
        '
        'dgOtherTrades
        '
        Me.dgOtherTrades.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgOtherTrades.DataSource = Me.dvOtherTrades
        '
        'dgOtherTrades.EmbeddedNavigator
        '
        Me.dgOtherTrades.EmbeddedNavigator.Name = ""
        Me.dgOtherTrades.Location = New System.Drawing.Point(16, 32)
        Me.dgOtherTrades.MainView = Me.gvOtherTrades
        Me.dgOtherTrades.Name = "dgOtherTrades"
        Me.dgOtherTrades.Size = New System.Drawing.Size(440, 312)
        Me.dgOtherTrades.TabIndex = 0
        Me.dgOtherTrades.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvOtherTrades})
        '
        'dvOtherTrades
        '
        Me.dvOtherTrades.RowFilter = "NIType = 'OT'"
        Me.dvOtherTrades.Table = Me.dataSet.VJobsNonCoreSalesItems
        '
        'gvOtherTrades
        '
        Me.gvOtherTrades.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.gvOtherTrades.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvOtherTrades.Appearance.FocusedCell.Options.UseBackColor = True
        Me.gvOtherTrades.Appearance.FocusedCell.Options.UseForeColor = True
        Me.gvOtherTrades.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.gvOtherTrades.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gvOtherTrades.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvOtherTrades.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvOtherTrades.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colNIName, Me.colNIUserType, Me.colNIOrderedBy1, Me.colNIPaidBy1})
        Me.gvOtherTrades.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.gvOtherTrades.GridControl = Me.dgOtherTrades
        Me.gvOtherTrades.Name = "gvOtherTrades"
        Me.gvOtherTrades.OptionsBehavior.Editable = False
        Me.gvOtherTrades.OptionsCustomization.AllowFilter = False
        Me.gvOtherTrades.OptionsNavigation.AutoFocusNewRow = True
        Me.gvOtherTrades.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.gvOtherTrades.OptionsView.ShowGroupPanel = False
        Me.gvOtherTrades.OptionsView.ShowHorzLines = False
        Me.gvOtherTrades.OptionsView.ShowIndicator = False
        Me.gvOtherTrades.OptionsView.ShowVertLines = False
        Me.gvOtherTrades.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colNIName, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colNIName
        '
        Me.colNIName.Caption = "Name"
        Me.colNIName.FieldName = "NIName"
        Me.colNIName.Name = "colNIName"
        Me.colNIName.Visible = True
        Me.colNIName.VisibleIndex = 0
        '
        'colNIUserType
        '
        Me.colNIUserType.Caption = "Trade Type"
        Me.colNIUserType.FieldName = "NIUserType"
        Me.colNIUserType.Name = "colNIUserType"
        Me.colNIUserType.Visible = True
        Me.colNIUserType.VisibleIndex = 1
        '
        'colNIOrderedBy1
        '
        Me.colNIOrderedBy1.Caption = "Ordered By"
        Me.colNIOrderedBy1.FieldName = "NIOrderedBy"
        Me.colNIOrderedBy1.Name = "colNIOrderedBy1"
        Me.colNIOrderedBy1.Visible = True
        Me.colNIOrderedBy1.VisibleIndex = 2
        '
        'colNIPaidBy1
        '
        Me.colNIPaidBy1.Caption = "Paid By"
        Me.colNIPaidBy1.FieldName = "NIPaidBy"
        Me.colNIPaidBy1.Name = "colNIPaidBy1"
        Me.colNIPaidBy1.Visible = True
        Me.colNIPaidBy1.VisibleIndex = 3
        '
        'btnEditOtherTrade
        '
        Me.btnEditOtherTrade.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEditOtherTrade.Image = CType(resources.GetObject("btnEditOtherTrade.Image"), System.Drawing.Image)
        Me.btnEditOtherTrade.Location = New System.Drawing.Point(96, 352)
        Me.btnEditOtherTrade.Name = "btnEditOtherTrade"
        Me.btnEditOtherTrade.Size = New System.Drawing.Size(72, 23)
        Me.btnEditOtherTrade.TabIndex = 2
        Me.btnEditOtherTrade.Text = "Edit..."
        '
        'btnRemoveOtherTrade
        '
        Me.btnRemoveOtherTrade.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveOtherTrade.Image = CType(resources.GetObject("btnRemoveOtherTrade.Image"), System.Drawing.Image)
        Me.btnRemoveOtherTrade.Location = New System.Drawing.Point(176, 352)
        Me.btnRemoveOtherTrade.Name = "btnRemoveOtherTrade"
        Me.btnRemoveOtherTrade.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveOtherTrade.TabIndex = 3
        Me.btnRemoveOtherTrade.Text = "Remove"
        '
        'dvAppliances
        '
        Me.dvAppliances.RowFilter = "NIType = 'AP'"
        Me.dvAppliances.Table = Me.dataSet.VJobsNonCoreSalesItems
        '
        'btnAddAppliance
        '
        Me.btnAddAppliance.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddAppliance.Image = CType(resources.GetObject("btnAddAppliance.Image"), System.Drawing.Image)
        Me.btnAddAppliance.Location = New System.Drawing.Point(16, 320)
        Me.btnAddAppliance.Name = "btnAddAppliance"
        Me.btnAddAppliance.Size = New System.Drawing.Size(72, 23)
        Me.btnAddAppliance.TabIndex = 1
        Me.btnAddAppliance.Text = "Add..."
        '
        'dgAppliances
        '
        Me.dgAppliances.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgAppliances.DataSource = Me.dvAppliances
        '
        'dgAppliances.EmbeddedNavigator
        '
        Me.dgAppliances.EmbeddedNavigator.Name = ""
        Me.dgAppliances.Location = New System.Drawing.Point(16, 32)
        Me.dgAppliances.MainView = Me.gvAppliances
        Me.dgAppliances.Name = "dgAppliances"
        Me.dgAppliances.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemComboBox1})
        Me.dgAppliances.Size = New System.Drawing.Size(464, 280)
        Me.dgAppliances.TabIndex = 0
        Me.dgAppliances.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvAppliances, Me.GridView2})
        '
        'gvAppliances
        '
        Me.gvAppliances.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.gvAppliances.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvAppliances.Appearance.FocusedCell.Options.UseBackColor = True
        Me.gvAppliances.Appearance.FocusedCell.Options.UseForeColor = True
        Me.gvAppliances.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.gvAppliances.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gvAppliances.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvAppliances.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvAppliances.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colNIUserType1, Me.colNIBrand, Me.colNIName2, Me.colNIOrderedBy, Me.colNIPaidBy})
        Me.gvAppliances.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.gvAppliances.GridControl = Me.dgAppliances
        Me.gvAppliances.Name = "gvAppliances"
        Me.gvAppliances.OptionsBehavior.Editable = False
        Me.gvAppliances.OptionsCustomization.AllowFilter = False
        Me.gvAppliances.OptionsNavigation.AutoFocusNewRow = True
        Me.gvAppliances.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.gvAppliances.OptionsView.ShowGroupPanel = False
        Me.gvAppliances.OptionsView.ShowHorzLines = False
        Me.gvAppliances.OptionsView.ShowIndicator = False
        Me.gvAppliances.OptionsView.ShowVertLines = False
        Me.gvAppliances.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colNIUserType1, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colNIUserType1
        '
        Me.colNIUserType1.Caption = "Appliance Type"
        Me.colNIUserType1.FieldName = "NIUserType"
        Me.colNIUserType1.Name = "colNIUserType1"
        Me.colNIUserType1.Visible = True
        Me.colNIUserType1.VisibleIndex = 0
        Me.colNIUserType1.Width = 176
        '
        'colNIBrand
        '
        Me.colNIBrand.Caption = "Brand"
        Me.colNIBrand.FieldName = "NIBrand"
        Me.colNIBrand.Name = "colNIBrand"
        Me.colNIBrand.Visible = True
        Me.colNIBrand.VisibleIndex = 1
        Me.colNIBrand.Width = 166
        '
        'colNIName2
        '
        Me.colNIName2.Caption = "Model Number"
        Me.colNIName2.FieldName = "NIName"
        Me.colNIName2.Name = "colNIName2"
        Me.colNIName2.Visible = True
        Me.colNIName2.VisibleIndex = 2
        Me.colNIName2.Width = 162
        '
        'colNIOrderedBy
        '
        Me.colNIOrderedBy.Caption = "Ordered By"
        Me.colNIOrderedBy.FieldName = "NIOrderedBy"
        Me.colNIOrderedBy.Name = "colNIOrderedBy"
        Me.colNIOrderedBy.Visible = True
        Me.colNIOrderedBy.VisibleIndex = 3
        Me.colNIOrderedBy.Width = 168
        '
        'colNIPaidBy
        '
        Me.colNIPaidBy.Caption = "Paid By"
        Me.colNIPaidBy.FieldName = "NIPaidBy"
        Me.colNIPaidBy.Name = "colNIPaidBy"
        Me.colNIPaidBy.Visible = True
        Me.colNIPaidBy.VisibleIndex = 4
        Me.colNIPaidBy.Width = 171
        '
        'RepositoryItemComboBox1
        '
        Me.RepositoryItemComboBox1.AutoHeight = False
        Me.RepositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox1.Items.AddRange(New Object() {"Align Kitchens", "Customer"})
        Me.RepositoryItemComboBox1.Name = "RepositoryItemComboBox1"
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.dgAppliances
        Me.GridView2.Name = "GridView2"
        '
        'btnEditAppliance
        '
        Me.btnEditAppliance.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEditAppliance.Image = CType(resources.GetObject("btnEditAppliance.Image"), System.Drawing.Image)
        Me.btnEditAppliance.Location = New System.Drawing.Point(96, 320)
        Me.btnEditAppliance.Name = "btnEditAppliance"
        Me.btnEditAppliance.Size = New System.Drawing.Size(72, 23)
        Me.btnEditAppliance.TabIndex = 2
        Me.btnEditAppliance.Text = "Edit..."
        '
        'btnRemoveAppliance
        '
        Me.btnRemoveAppliance.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveAppliance.Image = CType(resources.GetObject("btnRemoveAppliance.Image"), System.Drawing.Image)
        Me.btnRemoveAppliance.Location = New System.Drawing.Point(176, 320)
        Me.btnRemoveAppliance.Name = "btnRemoveAppliance"
        Me.btnRemoveAppliance.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveAppliance.TabIndex = 3
        Me.btnRemoveAppliance.Text = "Remove"
        '
        'btnAddAppointment
        '
        Me.btnAddAppointment.Image = CType(resources.GetObject("btnAddAppointment.Image"), System.Drawing.Image)
        Me.btnAddAppointment.Location = New System.Drawing.Point(48, 408)
        Me.btnAddAppointment.Name = "btnAddAppointment"
        Me.btnAddAppointment.Size = New System.Drawing.Size(72, 23)
        Me.btnAddAppointment.TabIndex = 2
        Me.btnAddAppointment.Text = "Add..."
        '
        'btnEditAppointment
        '
        Me.btnEditAppointment.Image = CType(resources.GetObject("btnEditAppointment.Image"), System.Drawing.Image)
        Me.btnEditAppointment.Location = New System.Drawing.Point(128, 408)
        Me.btnEditAppointment.Name = "btnEditAppointment"
        Me.btnEditAppointment.Size = New System.Drawing.Size(72, 23)
        Me.btnEditAppointment.TabIndex = 3
        Me.btnEditAppointment.Text = "Edit..."
        '
        'btnRemoveAppointment
        '
        Me.btnRemoveAppointment.Image = CType(resources.GetObject("btnRemoveAppointment.Image"), System.Drawing.Image)
        Me.btnRemoveAppointment.Location = New System.Drawing.Point(208, 408)
        Me.btnRemoveAppointment.Name = "btnRemoveAppointment"
        Me.btnRemoveAppointment.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveAppointment.TabIndex = 4
        Me.btnRemoveAppointment.Text = "Remove"
        '
        'txtJBClientSurname
        '
        Me.txtJBClientSurname.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientSurname"))
        Me.txtJBClientSurname.EditValue = ""
        Me.txtJBClientSurname.Location = New System.Drawing.Point(216, 160)
        Me.txtJBClientSurname.Name = "txtJBClientSurname"
        Me.txtJBClientSurname.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientSurname.TabIndex = 5
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(72, 392)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(80, 21)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Home phone:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(72, 440)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(80, 21)
        Me.Label11.TabIndex = 19
        Me.Label11.Text = "Fax:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label13
        '
        Me.Label13.Location = New System.Drawing.Point(72, 464)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(80, 21)
        Me.Label13.TabIndex = 20
        Me.Label13.Text = "Mobile:"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine18
        '
        Me.GroupLine18.Location = New System.Drawing.Point(48, 368)
        Me.GroupLine18.Name = "GroupLine18"
        Me.GroupLine18.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine18.TabIndex = 26
        Me.GroupLine18.TextString = "Contact Details"
        Me.GroupLine18.TextWidth = 79
        '
        'Label36
        '
        Me.Label36.Location = New System.Drawing.Point(72, 208)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(96, 21)
        Me.Label36.TabIndex = 23
        Me.Label36.Text = "Reference name:"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine20
        '
        Me.GroupLine20.Location = New System.Drawing.Point(48, 136)
        Me.GroupLine20.Name = "GroupLine20"
        Me.GroupLine20.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine20.TabIndex = 4
        Me.GroupLine20.TextString = "Name"
        Me.GroupLine20.TextWidth = 35
        '
        'Label37
        '
        Me.Label37.Location = New System.Drawing.Point(72, 336)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(80, 21)
        Me.Label37.TabIndex = 21
        Me.Label37.Text = "State/region:"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label38
        '
        Me.Label38.Location = New System.Drawing.Point(72, 184)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(72, 21)
        Me.Label38.TabIndex = 15
        Me.Label38.Text = "First name:"
        Me.Label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label39
        '
        Me.Label39.Location = New System.Drawing.Point(72, 160)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(144, 21)
        Me.Label39.TabIndex = 17
        Me.Label39.Text = "Surname / business name:"
        Me.Label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine21
        '
        Me.GroupLine21.Location = New System.Drawing.Point(48, 240)
        Me.GroupLine21.Name = "GroupLine21"
        Me.GroupLine21.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine21.TabIndex = 27
        Me.GroupLine21.TextString = "Address"
        Me.GroupLine21.TextWidth = 50
        '
        'Label40
        '
        Me.Label40.Location = New System.Drawing.Point(72, 264)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(80, 21)
        Me.Label40.TabIndex = 16
        Me.Label40.Text = "Street address:"
        Me.Label40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label41
        '
        Me.Label41.Location = New System.Drawing.Point(72, 312)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(80, 21)
        Me.Label41.TabIndex = 22
        Me.Label41.Text = "Suburb/town:"
        Me.Label41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJBClientFirstName
        '
        Me.txtJBClientFirstName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientFirstName"))
        Me.txtJBClientFirstName.EditValue = ""
        Me.txtJBClientFirstName.Location = New System.Drawing.Point(216, 184)
        Me.txtJBClientFirstName.Name = "txtJBClientFirstName"
        Me.txtJBClientFirstName.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientFirstName.TabIndex = 6
        '
        'txtJBReferenceName
        '
        Me.txtJBReferenceName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBReferenceName"))
        Me.txtJBReferenceName.EditValue = ""
        Me.txtJBReferenceName.Location = New System.Drawing.Point(216, 208)
        Me.txtJBReferenceName.Name = "txtJBReferenceName"
        Me.txtJBReferenceName.Size = New System.Drawing.Size(296, 20)
        Me.txtJBReferenceName.TabIndex = 7
        '
        'txtJBClientStreetAddress02
        '
        Me.txtJBClientStreetAddress02.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientStreetAddress02"))
        Me.txtJBClientStreetAddress02.EditValue = ""
        Me.txtJBClientStreetAddress02.Location = New System.Drawing.Point(216, 288)
        Me.txtJBClientStreetAddress02.Name = "txtJBClientStreetAddress02"
        Me.txtJBClientStreetAddress02.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientStreetAddress02.TabIndex = 9
        '
        'txtJBClientStreetAddress01
        '
        Me.txtJBClientStreetAddress01.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientStreetAddress01"))
        Me.txtJBClientStreetAddress01.EditValue = ""
        Me.txtJBClientStreetAddress01.Location = New System.Drawing.Point(216, 264)
        Me.txtJBClientStreetAddress01.Name = "txtJBClientStreetAddress01"
        Me.txtJBClientStreetAddress01.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientStreetAddress01.TabIndex = 8
        '
        'txtJBClientSuburb
        '
        Me.txtJBClientSuburb.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientSuburb"))
        Me.txtJBClientSuburb.EditValue = ""
        Me.txtJBClientSuburb.Location = New System.Drawing.Point(216, 312)
        Me.txtJBClientSuburb.Name = "txtJBClientSuburb"
        Me.txtJBClientSuburb.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientSuburb.TabIndex = 10
        '
        'txtJBClientHomePhoneNumber
        '
        Me.txtJBClientHomePhoneNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientHomePhoneNumber"))
        Me.txtJBClientHomePhoneNumber.EditValue = ""
        Me.txtJBClientHomePhoneNumber.Location = New System.Drawing.Point(216, 392)
        Me.txtJBClientHomePhoneNumber.Name = "txtJBClientHomePhoneNumber"
        Me.txtJBClientHomePhoneNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientHomePhoneNumber.TabIndex = 13
        '
        'txtJBClientFaxNumber
        '
        Me.txtJBClientFaxNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientFaxNumber"))
        Me.txtJBClientFaxNumber.EditValue = ""
        Me.txtJBClientFaxNumber.Location = New System.Drawing.Point(216, 440)
        Me.txtJBClientFaxNumber.Name = "txtJBClientFaxNumber"
        Me.txtJBClientFaxNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientFaxNumber.TabIndex = 15
        '
        'txtJBClientMobileNumber
        '
        Me.txtJBClientMobileNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientMobileNumber"))
        Me.txtJBClientMobileNumber.EditValue = ""
        Me.txtJBClientMobileNumber.Location = New System.Drawing.Point(216, 464)
        Me.txtJBClientMobileNumber.Name = "txtJBClientMobileNumber"
        Me.txtJBClientMobileNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientMobileNumber.TabIndex = 16
        '
        'txtJBClientState
        '
        Me.txtJBClientState.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientState"))
        Me.txtJBClientState.EditValue = ""
        Me.txtJBClientState.Location = New System.Drawing.Point(216, 336)
        Me.txtJBClientState.Name = "txtJBClientState"
        Me.txtJBClientState.Size = New System.Drawing.Size(104, 20)
        Me.txtJBClientState.TabIndex = 11
        '
        'txtJBClientPostCode
        '
        Me.txtJBClientPostCode.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientPostCode"))
        Me.txtJBClientPostCode.EditValue = ""
        Me.txtJBClientPostCode.Location = New System.Drawing.Point(424, 336)
        Me.txtJBClientPostCode.Name = "txtJBClientPostCode"
        Me.txtJBClientPostCode.Size = New System.Drawing.Size(88, 20)
        Me.txtJBClientPostCode.TabIndex = 12
        '
        'pnlScheduling
        '
        Me.pnlScheduling.Controls.Add(Me.btnViewCalendar)
        Me.pnlScheduling.Controls.Add(Me.GroupLine9)
        Me.pnlScheduling.Controls.Add(Me.GroupLine8)
        Me.pnlScheduling.Controls.Add(Me.Label7)
        Me.pnlScheduling.Controls.Add(Me.btnAddAppointment)
        Me.pnlScheduling.Controls.Add(Me.btnEditAppointment)
        Me.pnlScheduling.Controls.Add(Me.btnRemoveAppointment)
        Me.pnlScheduling.Controls.Add(Me.dgAppointments)
        Me.pnlScheduling.Location = New System.Drawing.Point(24, 24)
        Me.pnlScheduling.Name = "pnlScheduling"
        Me.pnlScheduling.Size = New System.Drawing.Size(536, 448)
        Me.pnlScheduling.TabIndex = 15
        Me.pnlScheduling.Text = "Scheduling"
        '
        'btnViewCalendar
        '
        Me.btnViewCalendar.Location = New System.Drawing.Point(48, 88)
        Me.btnViewCalendar.Name = "btnViewCalendar"
        Me.btnViewCalendar.Size = New System.Drawing.Size(136, 23)
        Me.btnViewCalendar.TabIndex = 0
        Me.btnViewCalendar.Text = "View Calendar..."
        '
        'GroupLine9
        '
        Me.GroupLine9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupLine9.Location = New System.Drawing.Point(24, 128)
        Me.GroupLine9.Name = "GroupLine9"
        Me.GroupLine9.Size = New System.Drawing.Size(488, 16)
        Me.GroupLine9.TabIndex = 20
        Me.GroupLine9.TextString = "List View"
        Me.GroupLine9.TextWidth = 50
        '
        'GroupLine8
        '
        Me.GroupLine8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupLine8.Location = New System.Drawing.Point(24, 64)
        Me.GroupLine8.Name = "GroupLine8"
        Me.GroupLine8.Size = New System.Drawing.Size(488, 16)
        Me.GroupLine8.TabIndex = 19
        Me.GroupLine8.TextString = "Calendar View"
        Me.GroupLine8.TextWidth = 80
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(24, 32)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(488, 16)
        Me.Label7.TabIndex = 18
        Me.Label7.Text = "You may add appointments for direct labor using the list or calendar view."
        '
        'dgAppointments
        '
        Me.dgAppointments.DataSource = Me.dvAppointments
        '
        'dgAppointments.EmbeddedNavigator
        '
        Me.dgAppointments.EmbeddedNavigator.Name = ""
        Me.dgAppointments.Location = New System.Drawing.Point(48, 152)
        Me.dgAppointments.MainView = Me.gvAppointments
        Me.dgAppointments.Name = "dgAppointments"
        Me.dgAppointments.Size = New System.Drawing.Size(464, 248)
        Me.dgAppointments.TabIndex = 1
        Me.dgAppointments.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvAppointments})
        '
        'dvAppointments
        '
        Me.dvAppointments.Table = Me.DsCalendar.VAppointments
        '
        'gvAppointments
        '
        Me.gvAppointments.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.gvAppointments.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvAppointments.Appearance.FocusedCell.Options.UseBackColor = True
        Me.gvAppointments.Appearance.FocusedCell.Options.UseForeColor = True
        Me.gvAppointments.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.gvAppointments.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gvAppointments.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvAppointments.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvAppointments.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colAPBegin, Me.colEXName, Me.colTAName})
        Me.gvAppointments.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.gvAppointments.GridControl = Me.dgAppointments
        Me.gvAppointments.Name = "gvAppointments"
        Me.gvAppointments.OptionsBehavior.Editable = False
        Me.gvAppointments.OptionsCustomization.AllowFilter = False
        Me.gvAppointments.OptionsNavigation.AutoFocusNewRow = True
        Me.gvAppointments.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.gvAppointments.OptionsView.ShowGroupPanel = False
        Me.gvAppointments.OptionsView.ShowHorzLines = False
        Me.gvAppointments.OptionsView.ShowIndicator = False
        Me.gvAppointments.OptionsView.ShowVertLines = False
        '
        'colAPBegin
        '
        Me.colAPBegin.Caption = "Date"
        Me.colAPBegin.DisplayFormat.FormatString = "d"
        Me.colAPBegin.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colAPBegin.FieldName = "APBegin"
        Me.colAPBegin.Name = "colAPBegin"
        Me.colAPBegin.Visible = True
        Me.colAPBegin.VisibleIndex = 0
        '
        'colEXName
        '
        Me.colEXName.Caption = "Staff Member"
        Me.colEXName.FieldName = "EXName"
        Me.colEXName.Name = "colEXName"
        Me.colEXName.Visible = True
        Me.colEXName.VisibleIndex = 1
        '
        'colTAName
        '
        Me.colTAName.Caption = "Task"
        Me.colTAName.FieldName = "TAName"
        Me.colTAName.Name = "colTAName"
        Me.colTAName.Visible = True
        Me.colTAName.VisibleIndex = 2
        '
        'PnlOtherTrades
        '
        Me.PnlOtherTrades.Controls.Add(Me.btnAddOtherTrade)
        Me.PnlOtherTrades.Controls.Add(Me.dgOtherTrades)
        Me.PnlOtherTrades.Controls.Add(Me.btnEditOtherTrade)
        Me.PnlOtherTrades.Controls.Add(Me.btnRemoveOtherTrade)
        Me.PnlOtherTrades.Location = New System.Drawing.Point(80, 16)
        Me.PnlOtherTrades.Name = "PnlOtherTrades"
        Me.PnlOtherTrades.Size = New System.Drawing.Size(472, 384)
        Me.PnlOtherTrades.TabIndex = 16
        Me.PnlOtherTrades.Text = "Other Trades"
        '
        'pnlJobPurchaseOrders
        '
        Me.pnlJobPurchaseOrders.Controls.Add(Me.dgJobPurchaseOrders)
        Me.pnlJobPurchaseOrders.Controls.Add(Me.btnRemoveJobPurchaseOrder)
        Me.pnlJobPurchaseOrders.Controls.Add(Me.btnEditJobPurchaseOrder)
        Me.pnlJobPurchaseOrders.Controls.Add(Me.btnAddJobPurchaseOrder)
        Me.pnlJobPurchaseOrders.Location = New System.Drawing.Point(16, 24)
        Me.pnlJobPurchaseOrders.Name = "pnlJobPurchaseOrders"
        Me.pnlJobPurchaseOrders.Size = New System.Drawing.Size(496, 368)
        Me.pnlJobPurchaseOrders.TabIndex = 14
        Me.pnlJobPurchaseOrders.Text = "Job Purchase Orders"
        '
        'dgJobPurchaseOrders
        '
        Me.dgJobPurchaseOrders.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgJobPurchaseOrders.DataSource = Me.dvJobPurchaseOrders
        '
        'dgJobPurchaseOrders.EmbeddedNavigator
        '
        Me.dgJobPurchaseOrders.EmbeddedNavigator.Name = ""
        Me.dgJobPurchaseOrders.Location = New System.Drawing.Point(16, 32)
        Me.dgJobPurchaseOrders.MainView = Me.gvJobPurchaseOrders
        Me.dgJobPurchaseOrders.Name = "dgJobPurchaseOrders"
        Me.dgJobPurchaseOrders.ShowOnlyPredefinedDetails = True
        Me.dgJobPurchaseOrders.Size = New System.Drawing.Size(464, 296)
        Me.dgJobPurchaseOrders.TabIndex = 0
        Me.dgJobPurchaseOrders.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvJobPurchaseOrders})
        '
        'dvJobPurchaseOrders
        '
        Me.dvJobPurchaseOrders.RowFilter = "ORType = 'JPO'"
        Me.dvJobPurchaseOrders.Table = Me.DsOrders.VOrders
        '
        'DsOrders
        '
        Me.DsOrders.DataSetName = "dsOrders"
        Me.DsOrders.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'gvJobPurchaseOrders
        '
        Me.gvJobPurchaseOrders.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.gvJobPurchaseOrders.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvJobPurchaseOrders.Appearance.FocusedCell.Options.UseBackColor = True
        Me.gvJobPurchaseOrders.Appearance.FocusedCell.Options.UseForeColor = True
        Me.gvJobPurchaseOrders.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.gvJobPurchaseOrders.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gvJobPurchaseOrders.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvJobPurchaseOrders.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvJobPurchaseOrders.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn4, Me.GridColumn5, Me.GridColumn6})
        Me.gvJobPurchaseOrders.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.gvJobPurchaseOrders.GridControl = Me.dgJobPurchaseOrders
        Me.gvJobPurchaseOrders.Name = "gvJobPurchaseOrders"
        Me.gvJobPurchaseOrders.OptionsBehavior.Editable = False
        Me.gvJobPurchaseOrders.OptionsCustomization.AllowFilter = False
        Me.gvJobPurchaseOrders.OptionsNavigation.AutoFocusNewRow = True
        Me.gvJobPurchaseOrders.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.gvJobPurchaseOrders.OptionsView.ShowGroupPanel = False
        Me.gvJobPurchaseOrders.OptionsView.ShowHorzLines = False
        Me.gvJobPurchaseOrders.OptionsView.ShowIndicator = False
        Me.gvJobPurchaseOrders.OptionsView.ShowVertLines = False
        Me.gvJobPurchaseOrders.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn4, DevExpress.Data.ColumnSortOrder.Descending)})
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Order #"
        Me.GridColumn4.FieldName = "OROrderNumber"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 0
        Me.GridColumn4.Width = 38
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Date"
        Me.GridColumn5.FieldName = "ORDate"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 1
        Me.GridColumn5.Width = 106
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Description"
        Me.GridColumn6.FieldName = "ORDescription"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 2
        Me.GridColumn6.Width = 146
        '
        'btnRemoveJobPurchaseOrder
        '
        Me.btnRemoveJobPurchaseOrder.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveJobPurchaseOrder.Image = CType(resources.GetObject("btnRemoveJobPurchaseOrder.Image"), System.Drawing.Image)
        Me.btnRemoveJobPurchaseOrder.Location = New System.Drawing.Point(176, 336)
        Me.btnRemoveJobPurchaseOrder.Name = "btnRemoveJobPurchaseOrder"
        Me.btnRemoveJobPurchaseOrder.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveJobPurchaseOrder.TabIndex = 3
        Me.btnRemoveJobPurchaseOrder.Text = "Remove"
        '
        'btnEditJobPurchaseOrder
        '
        Me.btnEditJobPurchaseOrder.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEditJobPurchaseOrder.Image = CType(resources.GetObject("btnEditJobPurchaseOrder.Image"), System.Drawing.Image)
        Me.btnEditJobPurchaseOrder.Location = New System.Drawing.Point(96, 336)
        Me.btnEditJobPurchaseOrder.Name = "btnEditJobPurchaseOrder"
        Me.btnEditJobPurchaseOrder.Size = New System.Drawing.Size(72, 23)
        Me.btnEditJobPurchaseOrder.TabIndex = 2
        Me.btnEditJobPurchaseOrder.Text = "Edit..."
        '
        'btnAddJobPurchaseOrder
        '
        Me.btnAddJobPurchaseOrder.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddJobPurchaseOrder.Image = CType(resources.GetObject("btnAddJobPurchaseOrder.Image"), System.Drawing.Image)
        Me.btnAddJobPurchaseOrder.Location = New System.Drawing.Point(16, 336)
        Me.btnAddJobPurchaseOrder.Name = "btnAddJobPurchaseOrder"
        Me.btnAddJobPurchaseOrder.Size = New System.Drawing.Size(72, 23)
        Me.btnAddJobPurchaseOrder.TabIndex = 1
        Me.btnAddJobPurchaseOrder.Text = "Add..."
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;user id=sa;integrated security=SSPI;data sou" & _
        "rce=""SERVER\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'pnlAppliances
        '
        Me.pnlAppliances.Controls.Add(Me.btnRemoveAppliance)
        Me.pnlAppliances.Controls.Add(Me.btnAddAppliance)
        Me.pnlAppliances.Controls.Add(Me.dgAppliances)
        Me.pnlAppliances.Controls.Add(Me.btnEditAppliance)
        Me.pnlAppliances.Location = New System.Drawing.Point(48, 24)
        Me.pnlAppliances.Name = "pnlAppliances"
        Me.pnlAppliances.Size = New System.Drawing.Size(496, 352)
        Me.pnlAppliances.TabIndex = 17
        Me.pnlAppliances.Text = "Appliances and Plumbing Items"
        '
        'PnlJobInformation
        '
        Me.PnlJobInformation.Controls.Add(Me.dpLDDate)
        Me.PnlJobInformation.Controls.Add(Me.txtLDUser)
        Me.PnlJobInformation.Controls.Add(Me.Label22)
        Me.PnlJobInformation.Controls.Add(Me.LookUpEdit1)
        Me.PnlJobInformation.Controls.Add(Me.Label21)
        Me.PnlJobInformation.Controls.Add(Me.txtJBPriceQuoted)
        Me.PnlJobInformation.Controls.Add(Me.Label8)
        Me.PnlJobInformation.Controls.Add(Me.GroupLine5)
        Me.PnlJobInformation.Controls.Add(Me.txtJBJobDescription)
        Me.PnlJobInformation.Controls.Add(Me.txtEXIDBookingMeasurer)
        Me.PnlJobInformation.Controls.Add(Me.Label12)
        Me.PnlJobInformation.Controls.Add(Me.Label1)
        Me.PnlJobInformation.Controls.Add(Me.Label6)
        Me.PnlJobInformation.Controls.Add(Me.GroupLine2)
        Me.PnlJobInformation.Controls.Add(Me.Label15)
        Me.PnlJobInformation.Controls.Add(Me.RadioGroup1)
        Me.PnlJobInformation.Controls.Add(Me.GroupLine3)
        Me.PnlJobInformation.Controls.Add(Me.GroupLine4)
        Me.PnlJobInformation.Controls.Add(Me.Label5)
        Me.PnlJobInformation.Controls.Add(Me.chkJBIsAllOrderingComplete)
        Me.PnlJobInformation.Controls.Add(Me.GroupLine1)
        Me.PnlJobInformation.Controls.Add(Me.Label4)
        Me.PnlJobInformation.Controls.Add(Me.Label19)
        Me.PnlJobInformation.Location = New System.Drawing.Point(48, 0)
        Me.PnlJobInformation.Name = "PnlJobInformation"
        Me.PnlJobInformation.Size = New System.Drawing.Size(536, 480)
        Me.PnlJobInformation.TabIndex = 11
        Me.PnlJobInformation.Text = "Job Information"
        '
        'dpLDDate
        '
        Me.dpLDDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBBookingDate"))
        Me.dpLDDate.EditValue = New Date(2005, 7, 21, 0, 0, 0, 0)
        Me.dpLDDate.Location = New System.Drawing.Point(352, 408)
        Me.dpLDDate.Name = "dpLDDate"
        '
        'dpLDDate.Properties
        '
        Me.dpLDDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False
        Me.dpLDDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpLDDate.Size = New System.Drawing.Size(152, 20)
        Me.dpLDDate.TabIndex = 7
        '
        'txtLDUser
        '
        Me.txtLDUser.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBUser"))
        Me.txtLDUser.Location = New System.Drawing.Point(128, 408)
        Me.txtLDUser.Name = "txtLDUser"
        '
        'txtLDUser.Properties
        '
        Me.txtLDUser.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtLDUser.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXName", "", 45, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.txtLDUser.Properties.DataSource = Me.dvExpenses
        Me.txtLDUser.Properties.DisplayMember = "EXName"
        Me.txtLDUser.Properties.NullText = ""
        Me.txtLDUser.Properties.ShowFooter = False
        Me.txtLDUser.Properties.ShowHeader = False
        Me.txtLDUser.Properties.ShowLines = False
        Me.txtLDUser.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.txtLDUser.Properties.ValueMember = "EXName"
        Me.txtLDUser.Size = New System.Drawing.Size(176, 20)
        Me.txtLDUser.TabIndex = 6
        '
        'dvExpenses
        '
        Me.dvExpenses.RowFilter = "EGType <> 'OE'"
        Me.dvExpenses.Sort = "EXName"
        Me.dvExpenses.Table = Me.dataSet.VExpenses
        '
        'Label22
        '
        Me.Label22.Location = New System.Drawing.Point(48, 128)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(136, 21)
        Me.Label22.TabIndex = 33
        Me.Label22.Text = "Principal salesperson:"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LookUpEdit1
        '
        Me.LookUpEdit1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.EXIDRep"))
        Me.LookUpEdit1.Location = New System.Drawing.Point(200, 128)
        Me.LookUpEdit1.Name = "LookUpEdit1"
        '
        'LookUpEdit1.Properties
        '
        Me.LookUpEdit1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True
        Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookUpEdit1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXName")})
        Me.LookUpEdit1.Properties.DataSource = Me.dvSalesReps
        Me.LookUpEdit1.Properties.DisplayMember = "EXName"
        Me.LookUpEdit1.Properties.NullText = "--- Salesperson not specified ---"
        Me.LookUpEdit1.Properties.ShowFooter = False
        Me.LookUpEdit1.Properties.ShowHeader = False
        Me.LookUpEdit1.Properties.ShowLines = False
        Me.LookUpEdit1.Properties.ValueMember = "EXID"
        Me.LookUpEdit1.Size = New System.Drawing.Size(176, 20)
        Me.LookUpEdit1.TabIndex = 1
        '
        'dvSalesReps
        '
        Me.dvSalesReps.RowFilter = "EXAppearInSalesperson = 1"
        Me.dvSalesReps.Sort = "EXName"
        Me.dvSalesReps.Table = Me.dataSet.VExpenses
        '
        'Label21
        '
        Me.Label21.Location = New System.Drawing.Point(48, 248)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(432, 16)
        Me.Label21.TabIndex = 31
        Me.Label21.Text = "This figure is used to predict sales. In most cases, this will be the quoted pric" & _
        "e."
        '
        'txtJBPriceQuoted
        '
        Me.txtJBPriceQuoted.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBPriceQuoted"))
        Me.txtJBPriceQuoted.EditValue = ""
        Me.txtJBPriceQuoted.Location = New System.Drawing.Point(200, 272)
        Me.txtJBPriceQuoted.Name = "txtJBPriceQuoted"
        '
        'txtJBPriceQuoted.Properties
        '
        Me.txtJBPriceQuoted.Properties.Appearance.Options.UseTextOptions = True
        Me.txtJBPriceQuoted.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtJBPriceQuoted.Properties.DisplayFormat.FormatString = "c"
        Me.txtJBPriceQuoted.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtJBPriceQuoted.Properties.EditFormat.FormatString = "c"
        Me.txtJBPriceQuoted.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtJBPriceQuoted.Size = New System.Drawing.Size(176, 20)
        Me.txtJBPriceQuoted.TabIndex = 4
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(48, 272)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(152, 21)
        Me.Label8.TabIndex = 30
        Me.Label8.Text = "Estimated job price (BAOT):"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine5
        '
        Me.GroupLine5.Location = New System.Drawing.Point(24, 224)
        Me.GroupLine5.Name = "GroupLine5"
        Me.GroupLine5.Size = New System.Drawing.Size(480, 16)
        Me.GroupLine5.TabIndex = 27
        Me.GroupLine5.TextString = "Estimated Job Price"
        Me.GroupLine5.TextWidth = 110
        '
        'txtJBJobDescription
        '
        Me.txtJBJobDescription.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBJobDescription"))
        Me.txtJBJobDescription.EditValue = ""
        Me.txtJBJobDescription.Location = New System.Drawing.Point(200, 72)
        Me.txtJBJobDescription.Name = "txtJBJobDescription"
        '
        'txtJBJobDescription.Properties
        '
        Me.txtJBJobDescription.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtJBJobDescription.Properties.ContextMenu = Me.cmHistory
        Me.txtJBJobDescription.Properties.MaxItemCount = 15
        Me.txtJBJobDescription.Size = New System.Drawing.Size(176, 20)
        Me.txtJBJobDescription.TabIndex = 0
        '
        'cmHistory
        '
        Me.cmHistory.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.miClearHistory})
        '
        'miClearHistory
        '
        Me.miClearHistory.Index = 0
        Me.miClearHistory.Text = "Clear History"
        '
        'Label19
        '
        Me.Label19.Location = New System.Drawing.Point(48, 408)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(80, 21)
        Me.Label19.TabIndex = 14
        Me.Label19.Text = "Booked in by:"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlJobAddress
        '
        Me.pnlJobAddress.Controls.Add(Me.lblJobPostCode)
        Me.pnlJobAddress.Controls.Add(Me.txtJBJobStreetAddress01)
        Me.pnlJobAddress.Controls.Add(Me.rgJBJobAddressAsAbove)
        Me.pnlJobAddress.Controls.Add(Me.Label17)
        Me.pnlJobAddress.Controls.Add(Me.lblJobState)
        Me.pnlJobAddress.Controls.Add(Me.lblJobStreetAddress)
        Me.pnlJobAddress.Controls.Add(Me.lblJobSuburb)
        Me.pnlJobAddress.Controls.Add(Me.txtJBJobStreetAddress02)
        Me.pnlJobAddress.Controls.Add(Me.txtJBJobSuburb)
        Me.pnlJobAddress.Controls.Add(Me.txtJBJobState)
        Me.pnlJobAddress.Controls.Add(Me.txtJBJobPostCode)
        Me.pnlJobAddress.Location = New System.Drawing.Point(40, 16)
        Me.pnlJobAddress.Name = "pnlJobAddress"
        Me.pnlJobAddress.Size = New System.Drawing.Size(504, 400)
        Me.pnlJobAddress.TabIndex = 12
        Me.pnlJobAddress.Text = "Job Address"
        '
        'lblJobPostCode
        '
        Me.lblJobPostCode.Location = New System.Drawing.Point(288, 192)
        Me.lblJobPostCode.Name = "lblJobPostCode"
        Me.lblJobPostCode.Size = New System.Drawing.Size(96, 21)
        Me.lblJobPostCode.TabIndex = 23
        Me.lblJobPostCode.Text = "ZIP/postal code:"
        Me.lblJobPostCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJBJobStreetAddress01
        '
        Me.txtJBJobStreetAddress01.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBJobStreetAddress01"))
        Me.txtJBJobStreetAddress01.EditValue = ""
        Me.txtJBJobStreetAddress01.Location = New System.Drawing.Point(96, 96)
        Me.txtJBJobStreetAddress01.Name = "txtJBJobStreetAddress01"
        Me.txtJBJobStreetAddress01.Size = New System.Drawing.Size(368, 20)
        Me.txtJBJobStreetAddress01.TabIndex = 1
        '
        'rgJBJobAddressAsAbove
        '
        Me.rgJBJobAddressAsAbove.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBJobAddressAsAbove"))
        Me.rgJBJobAddressAsAbove.Location = New System.Drawing.Point(312, 8)
        Me.rgJBJobAddressAsAbove.Name = "rgJBJobAddressAsAbove"
        '
        'rgJBJobAddressAsAbove.Properties
        '
        Me.rgJBJobAddressAsAbove.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.rgJBJobAddressAsAbove.Properties.Appearance.Options.UseBackColor = True
        Me.rgJBJobAddressAsAbove.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.rgJBJobAddressAsAbove.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "Yes"), New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "No")})
        Me.rgJBJobAddressAsAbove.Size = New System.Drawing.Size(56, 56)
        Me.rgJBJobAddressAsAbove.TabIndex = 0
        '
        'Label17
        '
        Me.Label17.Location = New System.Drawing.Point(8, 24)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(304, 21)
        Me.Label17.TabIndex = 16
        Me.Label17.Text = "Is this job at the customer's address (same as previous)?"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobState
        '
        Me.lblJobState.Location = New System.Drawing.Point(8, 192)
        Me.lblJobState.Name = "lblJobState"
        Me.lblJobState.Size = New System.Drawing.Size(80, 21)
        Me.lblJobState.TabIndex = 11
        Me.lblJobState.Text = "State/region:"
        Me.lblJobState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobStreetAddress
        '
        Me.lblJobStreetAddress.Location = New System.Drawing.Point(8, 96)
        Me.lblJobStreetAddress.Name = "lblJobStreetAddress"
        Me.lblJobStreetAddress.Size = New System.Drawing.Size(88, 21)
        Me.lblJobStreetAddress.TabIndex = 13
        Me.lblJobStreetAddress.Text = "Street address:"
        Me.lblJobStreetAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobSuburb
        '
        Me.lblJobSuburb.Location = New System.Drawing.Point(8, 160)
        Me.lblJobSuburb.Name = "lblJobSuburb"
        Me.lblJobSuburb.Size = New System.Drawing.Size(80, 21)
        Me.lblJobSuburb.TabIndex = 10
        Me.lblJobSuburb.Text = "Suburb/town:"
        Me.lblJobSuburb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJBJobStreetAddress02
        '
        Me.txtJBJobStreetAddress02.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBJobStreetAddress02"))
        Me.txtJBJobStreetAddress02.EditValue = ""
        Me.txtJBJobStreetAddress02.Location = New System.Drawing.Point(96, 128)
        Me.txtJBJobStreetAddress02.Name = "txtJBJobStreetAddress02"
        Me.txtJBJobStreetAddress02.Size = New System.Drawing.Size(368, 20)
        Me.txtJBJobStreetAddress02.TabIndex = 2
        '
        'txtJBJobSuburb
        '
        Me.txtJBJobSuburb.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBJobSuburb"))
        Me.txtJBJobSuburb.EditValue = ""
        Me.txtJBJobSuburb.Location = New System.Drawing.Point(96, 160)
        Me.txtJBJobSuburb.Name = "txtJBJobSuburb"
        Me.txtJBJobSuburb.Size = New System.Drawing.Size(368, 20)
        Me.txtJBJobSuburb.TabIndex = 3
        '
        'txtJBJobState
        '
        Me.txtJBJobState.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBJobState"))
        Me.txtJBJobState.EditValue = ""
        Me.txtJBJobState.Location = New System.Drawing.Point(96, 192)
        Me.txtJBJobState.Name = "txtJBJobState"
        Me.txtJBJobState.Size = New System.Drawing.Size(112, 20)
        Me.txtJBJobState.TabIndex = 4
        '
        'txtJBJobPostCode
        '
        Me.txtJBJobPostCode.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBJobPostCode"))
        Me.txtJBJobPostCode.EditValue = ""
        Me.txtJBJobPostCode.Location = New System.Drawing.Point(384, 192)
        Me.txtJBJobPostCode.Name = "txtJBJobPostCode"
        Me.txtJBJobPostCode.Size = New System.Drawing.Size(80, 20)
        Me.txtJBJobPostCode.TabIndex = 5
        '
        'pnlClientInformation
        '
        Me.pnlClientInformation.Controls.Add(Me.rgLDUseContactFalse)
        Me.pnlClientInformation.Controls.Add(Me.Label3)
        Me.pnlClientInformation.Controls.Add(Me.pceContact)
        Me.pnlClientInformation.Controls.Add(Me.rgLDUseContactTrue)
        Me.pnlClientInformation.Controls.Add(Me.Label2)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientWorkPhoneNumber)
        Me.pnlClientInformation.Controls.Add(Me.Label9)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientEmail)
        Me.pnlClientInformation.Controls.Add(Me.Label20)
        Me.pnlClientInformation.Controls.Add(Me.Label39)
        Me.pnlClientInformation.Controls.Add(Me.GroupLine21)
        Me.pnlClientInformation.Controls.Add(Me.Label40)
        Me.pnlClientInformation.Controls.Add(Me.Label41)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientFirstName)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientStreetAddress02)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientStreetAddress01)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientSuburb)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientHomePhoneNumber)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientFaxNumber)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientMobileNumber)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientState)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientPostCode)
        Me.pnlClientInformation.Controls.Add(Me.txtJBReferenceName)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientSurname)
        Me.pnlClientInformation.Controls.Add(Me.Label10)
        Me.pnlClientInformation.Controls.Add(Me.Label11)
        Me.pnlClientInformation.Controls.Add(Me.Label13)
        Me.pnlClientInformation.Controls.Add(Me.GroupLine18)
        Me.pnlClientInformation.Controls.Add(Me.Label36)
        Me.pnlClientInformation.Controls.Add(Me.GroupLine20)
        Me.pnlClientInformation.Controls.Add(Me.Label37)
        Me.pnlClientInformation.Controls.Add(Me.Label38)
        Me.pnlClientInformation.Location = New System.Drawing.Point(16, 0)
        Me.pnlClientInformation.Name = "pnlClientInformation"
        Me.pnlClientInformation.Size = New System.Drawing.Size(520, 520)
        Me.pnlClientInformation.TabIndex = 0
        Me.pnlClientInformation.Text = "Customer Information"
        '
        'rgLDUseContactFalse
        '
        Me.rgLDUseContactFalse.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBUseContact"))
        Me.rgLDUseContactFalse.Location = New System.Drawing.Point(16, 24)
        Me.rgLDUseContactFalse.Name = "rgLDUseContactFalse"
        '
        'rgLDUseContactFalse.Properties
        '
        Me.rgLDUseContactFalse.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.rgLDUseContactFalse.Properties.Appearance.Options.UseBackColor = True
        Me.rgLDUseContactFalse.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.rgLDUseContactFalse.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "I don't want this customer to be in my list of contacts (eg. for one off customer" & _
        "s)")})
        Me.rgLDUseContactFalse.Size = New System.Drawing.Size(496, 24)
        Me.rgLDUseContactFalse.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(48, 72)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(288, 21)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Select an existing contact or create a new contact:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'pceContact
        '
        Me.pceContact.Location = New System.Drawing.Point(48, 96)
        Me.pceContact.Name = "pceContact"
        '
        'pceContact.Properties
        '
        Me.pceContact.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.pceContact.Properties.CloseOnLostFocus = False
        Me.pceContact.Properties.NullText = "<No contact is selected>"
        Me.pceContact.Properties.PopupControl = Me.pccContacts
        Me.pceContact.Properties.ShowPopupShadow = False
        Me.pceContact.Size = New System.Drawing.Size(464, 20)
        Me.pceContact.TabIndex = 3
        '
        'pccContacts
        '
        Me.pccContacts.Location = New System.Drawing.Point(128, 224)
        Me.pccContacts.Name = "pccContacts"
        Me.pccContacts.Size = New System.Drawing.Size(464, 336)
        Me.pccContacts.TabIndex = 6
        Me.pccContacts.Text = "PopupContainerControl1"
        '
        'rgLDUseContactTrue
        '
        Me.rgLDUseContactTrue.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBUseContact"))
        Me.rgLDUseContactTrue.Location = New System.Drawing.Point(16, 48)
        Me.rgLDUseContactTrue.Name = "rgLDUseContactTrue"
        '
        'rgLDUseContactTrue.Properties
        '
        Me.rgLDUseContactTrue.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.rgLDUseContactTrue.Properties.Appearance.Options.UseBackColor = True
        Me.rgLDUseContactTrue.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.rgLDUseContactTrue.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "I want this customer to be in my list of contacts (eg. for repeat customers)")})
        Me.rgLDUseContactTrue.Size = New System.Drawing.Size(496, 24)
        Me.rgLDUseContactTrue.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(72, 416)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 21)
        Me.Label2.TabIndex = 33
        Me.Label2.Text = "Work phone:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJBClientWorkPhoneNumber
        '
        Me.txtJBClientWorkPhoneNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientWorkPhoneNumber"))
        Me.txtJBClientWorkPhoneNumber.EditValue = ""
        Me.txtJBClientWorkPhoneNumber.Location = New System.Drawing.Point(216, 416)
        Me.txtJBClientWorkPhoneNumber.Name = "txtJBClientWorkPhoneNumber"
        Me.txtJBClientWorkPhoneNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientWorkPhoneNumber.TabIndex = 14
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(328, 336)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(96, 21)
        Me.Label9.TabIndex = 31
        Me.Label9.Text = "ZIP/postal code:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJBClientEmail
        '
        Me.txtJBClientEmail.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientEmail"))
        Me.txtJBClientEmail.EditValue = ""
        Me.txtJBClientEmail.Location = New System.Drawing.Point(216, 488)
        Me.txtJBClientEmail.Name = "txtJBClientEmail"
        Me.txtJBClientEmail.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientEmail.TabIndex = 17
        '
        'Label20
        '
        Me.Label20.Location = New System.Drawing.Point(72, 488)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(80, 21)
        Me.Label20.TabIndex = 29
        Me.Label20.Text = "Email:"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ListBox
        '
        Me.ListBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ListBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ListBox.IntegralHeight = False
        Me.ListBox.ItemHeight = 25
        Me.ListBox.Items.AddRange(New Object() {"Customer Information", "Job Address", "Job Information", "Job Purchase Orders", "Internal Orders", "Appliances & Plumbing Items", "Other Trades", "Scheduling", "Notes and Reminders"})
        Me.ListBox.Location = New System.Drawing.Point(8, 8)
        Me.ListBox.Name = "ListBox"
        Me.ListBox.Size = New System.Drawing.Size(168, 520)
        Me.ListBox.TabIndex = 0
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(648, 536)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(568, 536)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 3
        Me.btnOK.Text = "OK"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Jobs", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("JBID", "JBID"), New System.Data.Common.DataColumnMapping("ID", "ID"), New System.Data.Common.DataColumnMapping("JBScheduledStartDate", "JBScheduledStartDate"), New System.Data.Common.DataColumnMapping("JBScheduledFinishDate", "JBScheduledFinishDate"), New System.Data.Common.DataColumnMapping("JBClientFirstName", "JBClientFirstName"), New System.Data.Common.DataColumnMapping("JBClientSurname", "JBClientSurname"), New System.Data.Common.DataColumnMapping("JBClientName", "JBClientName"), New System.Data.Common.DataColumnMapping("JBClientStreetAddress01", "JBClientStreetAddress01"), New System.Data.Common.DataColumnMapping("JBClientStreetAddress02", "JBClientStreetAddress02"), New System.Data.Common.DataColumnMapping("JBClientSuburb", "JBClientSuburb"), New System.Data.Common.DataColumnMapping("JBClientState", "JBClientState"), New System.Data.Common.DataColumnMapping("JBClientPostCode", "JBClientPostCode"), New System.Data.Common.DataColumnMapping("JBClientAddress", "JBClientAddress"), New System.Data.Common.DataColumnMapping("JBClientHomePhoneNumber", "JBClientHomePhoneNumber"), New System.Data.Common.DataColumnMapping("JBClientWorkPhoneNumber", "JBClientWorkPhoneNumber"), New System.Data.Common.DataColumnMapping("JBClientFaxNumber", "JBClientFaxNumber"), New System.Data.Common.DataColumnMapping("JBClientMobileNumber", "JBClientMobileNumber"), New System.Data.Common.DataColumnMapping("JBJobAddressAsAbove", "JBJobAddressAsAbove"), New System.Data.Common.DataColumnMapping("JBJobStreetAddress01", "JBJobStreetAddress01"), New System.Data.Common.DataColumnMapping("JBJobStreetAddress02", "JBJobStreetAddress02"), New System.Data.Common.DataColumnMapping("JBJobSuburb", "JBJobSuburb"), New System.Data.Common.DataColumnMapping("JBJobState", "JBJobState"), New System.Data.Common.DataColumnMapping("JBJobPostCode", "JBJobPostCode"), New System.Data.Common.DataColumnMapping("JBJobAddress", "JBJobAddress"), New System.Data.Common.DataColumnMapping("JTID", "JTID"), New System.Data.Common.DataColumnMapping("JBReferenceName", "JBReferenceName"), New System.Data.Common.DataColumnMapping("EXIDBookingMeasurer", "EXIDBookingMeasurer"), New System.Data.Common.DataColumnMapping("JBJobDescription", "JBJobDescription"), New System.Data.Common.DataColumnMapping("JBDoorsType", "JBDoorsType"), New System.Data.Common.DataColumnMapping("JBDoorsStyle", "JBDoorsStyle"), New System.Data.Common.DataColumnMapping("JBDoorsColour", "JBDoorsColour"), New System.Data.Common.DataColumnMapping("JBIsMeasureOnSite", "JBIsMeasureOnSite"), New System.Data.Common.DataColumnMapping("JBIsAllOrderingComplete", "JBIsAllOrderingComplete"), New System.Data.Common.DataColumnMapping("JBDoorsHandles", "JBDoorsHandles"), New System.Data.Common.DataColumnMapping("JBDoorsTexture", "JBDoorsTexture"), New System.Data.Common.DataColumnMapping("JBIsCancelled", "JBIsCancelled"), New System.Data.Common.DataColumnMapping("JBDoorsSupplier", "JBDoorsSupplier"), New System.Data.Common.DataColumnMapping("JBClientEmail", "JBClientEmail"), New System.Data.Common.DataColumnMapping("JBPriceQuoted", "JBPriceQuoted"), New System.Data.Common.DataColumnMapping("JBGraniteBAOTPriceQuoted", "JBGraniteBAOTPriceQuoted"), New System.Data.Common.DataColumnMapping("JBCabinetryBAOTPriceQuoted", "JBCabinetryBAOTPriceQuoted"), New System.Data.Common.DataColumnMapping("EXIDRep", "EXIDRep"), New System.Data.Common.DataColumnMapping("JBBookingDate", "JBBookingDate"), New System.Data.Common.DataColumnMapping("JBUser", "JBUser"), New System.Data.Common.DataColumnMapping("CTID", "CTID"), New System.Data.Common.DataColumnMapping("JBUseContact", "JBUseContact")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Jobs WHERE (BRID = @Original_BRID) AND (JBID = @Original_JBID) AND (C" &
        "TID = @Original_CTID OR @Original_CTID IS NULL AND CTID IS NULL) AND (EXIDBookin" &
        "gMeasurer = @Original_EXIDBookingMeasurer OR @Original_EXIDBookingMeasurer IS NU" &
        "LL AND EXIDBookingMeasurer IS NULL) AND (EXIDRep = @Original_EXIDRep OR @Origina" &
        "l_EXIDRep IS NULL AND EXIDRep IS NULL) AND (ID = @Original_ID) AND (JBBookingDat" &
        "e = @Original_JBBookingDate OR @Original_JBBookingDate IS NULL AND JBBookingDate" &
        " IS NULL) AND (JBCabinetryBAOTPriceQuoted = @Original_JBCabinetryBAOTPriceQuoted" &
        " OR @Original_JBCabinetryBAOTPriceQuoted IS NULL AND JBCabinetryBAOTPriceQuoted " &
        "IS NULL) AND (JBClientAddress = @Original_JBClientAddress OR @Original_JBClientA" &
        "ddress IS NULL AND JBClientAddress IS NULL) AND (JBClientEmail = @Original_JBCli" &
        "entEmail OR @Original_JBClientEmail IS NULL AND JBClientEmail IS NULL) AND (JBCl" &
        "ientFaxNumber = @Original_JBClientFaxNumber OR @Original_JBClientFaxNumber IS NU" &
        "LL AND JBClientFaxNumber IS NULL) AND (JBClientFirstName = @Original_JBClientFir" &
        "stName OR @Original_JBClientFirstName IS NULL AND JBClientFirstName IS NULL) AND" &
        " (JBClientHomePhoneNumber = @Original_JBClientHomePhoneNumber OR @Original_JBCli" &
        "entHomePhoneNumber IS NULL AND JBClientHomePhoneNumber IS NULL) AND (JBClientMob" &
        "ileNumber = @Original_JBClientMobileNumber OR @Original_JBClientMobileNumber IS " &
        "NULL AND JBClientMobileNumber IS NULL) AND (JBClientName = @Original_JBClientNam" &
        "e OR @Original_JBClientName IS NULL AND JBClientName IS NULL) AND (JBClientPostC" &
        "ode = @Original_JBClientPostCode OR @Original_JBClientPostCode IS NULL AND JBCli" &
        "entPostCode IS NULL) AND (JBClientState = @Original_JBClientState OR @Original_J" &
        "BClientState IS NULL AND JBClientState IS NULL) AND (JBClientStreetAddress01 = @" &
        "Original_JBClientStreetAddress01 OR @Original_JBClientStreetAddress01 IS NULL AN" &
        "D JBClientStreetAddress01 IS NULL) AND (JBClientStreetAddress02 = @Original_JBCl" &
        "ientStreetAddress02 OR @Original_JBClientStreetAddress02 IS NULL AND JBClientStr" &
        "eetAddress02 IS NULL) AND (JBClientSuburb = @Original_JBClientSuburb OR @Origina" &
        "l_JBClientSuburb IS NULL AND JBClientSuburb IS NULL) AND (JBClientSurname = @Ori" &
        "ginal_JBClientSurname OR @Original_JBClientSurname IS NULL AND JBClientSurname I" &
        "S NULL) AND (JBClientWorkPhoneNumber = @Original_JBClientWorkPhoneNumber OR @Ori" &
        "ginal_JBClientWorkPhoneNumber IS NULL AND JBClientWorkPhoneNumber IS NULL) AND (" &
        "JBDoorsColour = @Original_JBDoorsColour OR @Original_JBDoorsColour IS NULL AND J" &
        "BDoorsColour IS NULL) AND (JBDoorsHandles = @Original_JBDoorsHandles OR @Origina" &
        "l_JBDoorsHandles IS NULL AND JBDoorsHandles IS NULL) AND (JBDoorsStyle = @Origin" &
        "al_JBDoorsStyle OR @Original_JBDoorsStyle IS NULL AND JBDoorsStyle IS NULL) AND " &
        "(JBDoorsSupplier = @Original_JBDoorsSupplier OR @Original_JBDoorsSupplier IS NUL" &
        "L AND JBDoorsSupplier IS NULL) AND (JBDoorsTexture = @Original_JBDoorsTexture OR" &
        " @Original_JBDoorsTexture IS NULL AND JBDoorsTexture IS NULL) AND (JBDoorsType =" &
        " @Original_JBDoorsType OR @Original_JBDoorsType IS NULL AND JBDoorsType IS NULL)" &
        " AND (JBGraniteBAOTPriceQuoted = @Original_JBGraniteBAOTPriceQuoted OR @Original" &
        "_JBGraniteBAOTPriceQuoted IS NULL AND JBGraniteBAOTPriceQuoted IS NULL) AND (JBI" &
        "sAllOrderingComplete = @Original_JBIsAllOrderingComplete OR @Original_JBIsAllOrd" &
        "eringComplete IS NULL AND JBIsAllOrderingComplete IS NULL) AND (JBIsCancelled = " &
        "@Original_JBIsCancelled OR @Original_JBIsCancelled IS NULL AND JBIsCancelled IS " &
        "NULL) AND (JBIsMeasureOnSite = @Original_JBIsMeasureOnSite OR @Original_JBIsMeas" &
        "ureOnSite IS NULL AND JBIsMeasureOnSite IS NULL) AND (JBJobAddress = @Original_J" &
        "BJobAddress OR @Original_JBJobAddress IS NULL AND JBJobAddress IS NULL) AND (JTID = @Original_J" &
        "TID OR @Original_JTID IS NULL AND JTID IS NULL) AND (JBJ" &
        "obAddressAsAbove = @Original_JBJobAddressAsAbove OR @Original_JBJobAddressAsAbov" &
        "e IS NULL AND JBJobAddressAsAbove IS NULL) AND (JBJobDescription = @Original_JBJ" &
        "obDescription OR @Original_JBJobDescription IS NULL AND JBJobDescription IS NULL" &
        ") AND (JBJobPostCode = @Original_JBJobPostCode OR @Original_JBJobPostCode IS NUL" &
        "L AND JBJobPostCode IS NULL) AND (JBJobState = @Original_JBJobState OR @Original" &
        "_JBJobState IS NULL AND JBJobState IS NULL) AND (JBJobStreetAddress01 = @Origina" &
        "l_JBJobStreetAddress01 OR @Original_JBJobStreetAddress01 IS NULL AND JBJobStreet" &
        "Address01 IS NULL) AND (JBJobStreetAddress02 = @Original_JBJobStreetAddress02 OR" &
        " @Original_JBJobStreetAddress02 IS NULL AND JBJobStreetAddress02 IS NULL) AND (J" &
        "BJobSuburb = @Original_JBJobSuburb OR @Original_JBJobSuburb IS NULL AND JBJobSub" &
        "urb IS NULL) AND (JBPriceQuoted = @Original_JBPriceQuoted OR @Original_JBPriceQu" &
        "oted IS NULL AND JBPriceQuoted IS NULL) AND (JBReferenceName = @Original_JBRefer" &
        "enceName OR @Original_JBReferenceName IS NULL AND JBReferenceName IS NULL) AND (" &
        "JBUseContact = @Original_JBUseContact OR @Original_JBUseContact IS NULL AND JBUs" &
        "eContact IS NULL) AND (JBUser = @Original_JBUser OR @Original_JBUser IS NULL AND" &
        " JBUser IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDBookingMeasurer", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDBookingMeasurer", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDRep", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDRep", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBBookingDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBBookingDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBCabinetryBAOTPriceQuoted", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBCabinetryBAOTPriceQuoted", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientFirstName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientFirstName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientHomePhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientName", System.Data.SqlDbType.VarChar, 102, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientSurname", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientWorkPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsColour", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsColour", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsHandles", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsHandles", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsStyle", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsStyle", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsSupplier", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsSupplier", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsTexture", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsTexture", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsType", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBGraniteBAOTPriceQuoted", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBGraniteBAOTPriceQuoted", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBIsAllOrderingComplete", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBIsAllOrderingComplete", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBIsCancelled", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBIsCancelled", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBIsMeasureOnSite", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBIsMeasureOnSite", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobAddress", System.Data.DataRowVersion.Original, Nothing))

        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JTID", System.Data.DataRowVersion.Original, Nothing))


        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobAddressAsAbove", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobDescription", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBPriceQuoted", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBPriceQuoted", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBReferenceName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBReferenceName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBUseContact", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBUseContact", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBUser", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBUser", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '

        ''inserting here babydha
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Jobs (BRID, ID,JBScheduledStartDate,JBScheduledFinishDate, JBClientFirstName, JBClientSurname, JBClientName, JBC" &
        "lientStreetAddress01, JBClientStreetAddress02, JBClientSuburb, JBClientState, JB" &
        "ClientPostCode, JBClientAddress, JBClientHomePhoneNumber, JBClientWorkPhoneNumbe" &
        "r, JBClientFaxNumber, JBClientMobileNumber, JBJobAddressAsAbove, JBJobStreetAddr" &
        "ess01, JBJobStreetAddress02, JBJobSuburb, JBJobState, JBJobPostCode, JBJobAddres" &
        "s, JTID, JBReferenceName, EXIDBookingMeasurer, JBJobDescription, JBDoorsType, JBDoorsS" &
        "tyle, JBDoorsColour, JBIsMeasureOnSite, JBIsAllOrderingComplete, JBDoorsHandles," &
        " JBDoorsTexture, JBIsCancelled, JBDoorsSupplier, JBClientEmail, JBPriceQuoted, J" &
        "BGraniteBAOTPriceQuoted, JBCabinetryBAOTPriceQuoted, EXIDRep, JBBookingDate, JBU" &
        "ser, CTID, JBUseContact) VALUES (@BRID, @ID,@JBScheduledStartDate, @JBScheduledFinishDate, @JBClientFirstName, @JBClientSurnam" &
        "e, @JBClientName, @JBClientStreetAddress01, @JBClientStreetAddress02, @JBClientS" &
        "uburb, @JBClientState, @JBClientPostCode, @JBClientAddress, @JBClientHomePhoneNu" &
        "mber, @JBClientWorkPhoneNumber, @JBClientFaxNumber, @JBClientMobileNumber, @JBJo" &
        "bAddressAsAbove, @JBJobStreetAddress01, @JBJobStreetAddress02, @JBJobSuburb, @JB" &
        "JobState, @JBJobPostCode, @JBJobAddress,1, @JBReferenceName,  @EXIDBookingMeasurer," &
        " @JBJobDescription, @JBDoorsType, @JBDoorsStyle, @JBDoorsColour, @JBIsMeasureOnS" &
        "ite, @JBIsAllOrderingComplete, @JBDoorsHandles, @JBDoorsTexture, @JBIsCancelled," &
        " @JBDoorsSupplier, @JBClientEmail, @JBPriceQuoted, @JBGraniteBAOTPriceQuoted, @J" &
        "BCabinetryBAOTPriceQuoted, @EXIDRep, @JBBookingDate, @JBUser, @CTID, @JBUseConta" &
        "ct); SELECT BRID, JBID, ID,JBScheduledStartDate,JBScheduledFinishDate, JBClientFirstName, JBClientSurname, JBClientName, JB" &
        "ClientStreetAddress01, JBClientStreetAddress02, JBClientSuburb, JBClientState, J" &
        "BClientPostCode, JBClientAddress, JBClientHomePhoneNumber, JBClientWorkPhoneNumb" &
        "er, JBClientFaxNumber, JBClientMobileNumber, JBJobAddressAsAbove, JBJobStreetAdd" &
        "ress01, JBJobStreetAddress02, JBJobSuburb, JBJobState, JBJobPostCode, JBJobAddre" &
        "ss, JBReferenceName, JTID, EXIDBookingMeasurer, JBJobDescription, JBDoorsType, JBDoors" &
        "Style, JBDoorsColour, JBIsMeasureOnSite, JBIsAllOrderingComplete, JBDoorsHandles" &
        ", JBDoorsTexture, JBIsCancelled, JBDoorsSupplier, JBClientEmail, JBPriceQuoted, " &
        "JBGraniteBAOTPriceQuoted, JBCabinetryBAOTPriceQuoted, EXIDRep, JBBookingDate, JB" &
        "User, CTID, JBUseContact FROM Jobs WHERE (BRID = @BRID) AND (JBID = SCOPE_IDENTI" &
        "TY()); INSERT INTO JobStatistics (BRID,JBID,JBDate,JBClientName) values " &
        " (@BRID,(select JBID FROM Jobs WHERE (BRID = @BRID) AND (JBID = SCOPE_IDENTITY())),@JBScheduledStartDate,@JBClientFirstName); "

        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))

        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBScheduledStartDate", System.Data.SqlDbType.DateTime, 8, "JBScheduledStartDate"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBScheduledFinishDate", System.Data.SqlDbType.DateTime, 8, "JBScheduledFinishDate"))

        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientFirstName", System.Data.SqlDbType.VarChar, 50, "JBClientFirstName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientSurname", System.Data.SqlDbType.VarChar, 50, "JBClientSurname"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientName", System.Data.SqlDbType.VarChar, 102, "JBClientName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, "JBClientStreetAddress01"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, "JBClientStreetAddress02"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientSuburb", System.Data.SqlDbType.VarChar, 50, "JBClientSuburb"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientState", System.Data.SqlDbType.VarChar, 3, "JBClientState"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientPostCode", System.Data.SqlDbType.VarChar, 20, "JBClientPostCode"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientAddress", System.Data.SqlDbType.VarChar, 259, "JBClientAddress"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, "JBClientHomePhoneNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, "JBClientWorkPhoneNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientFaxNumber", System.Data.SqlDbType.VarChar, 20, "JBClientFaxNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientMobileNumber", System.Data.SqlDbType.VarChar, 20, "JBClientMobileNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, "JBJobAddressAsAbove"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, "JBJobStreetAddress01"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, "JBJobStreetAddress02"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobSuburb", System.Data.SqlDbType.VarChar, 50, "JBJobSuburb"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobState", System.Data.SqlDbType.VarChar, 3, "JBJobState"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobPostCode", System.Data.SqlDbType.VarChar, 20, "JBJobPostCode"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobAddress", System.Data.SqlDbType.VarChar, 259, "JBJobAddress"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JTID", System.Data.SqlDbType.Int, 4, "JTID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBReferenceName", System.Data.SqlDbType.VarChar, 100, "JBReferenceName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDBookingMeasurer", System.Data.SqlDbType.Int, 4, "EXIDBookingMeasurer"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobDescription", System.Data.SqlDbType.VarChar, 50, "JBJobDescription"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsType", System.Data.SqlDbType.VarChar, 50, "JBDoorsType"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsStyle", System.Data.SqlDbType.VarChar, 50, "JBDoorsStyle"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsColour", System.Data.SqlDbType.VarChar, 50, "JBDoorsColour"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBIsMeasureOnSite", System.Data.SqlDbType.Bit, 1, "JBIsMeasureOnSite"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBIsAllOrderingComplete", System.Data.SqlDbType.Bit, 1, "JBIsAllOrderingComplete"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsHandles", System.Data.SqlDbType.VarChar, 50, "JBDoorsHandles"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsTexture", System.Data.SqlDbType.VarChar, 50, "JBDoorsTexture"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBIsCancelled", System.Data.SqlDbType.Bit, 1, "JBIsCancelled"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsSupplier", System.Data.SqlDbType.VarChar, 50, "JBDoorsSupplier"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientEmail", System.Data.SqlDbType.VarChar, 50, "JBClientEmail"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBPriceQuoted", System.Data.SqlDbType.Money, 8, "JBPriceQuoted"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBGraniteBAOTPriceQuoted", System.Data.SqlDbType.Money, 8, "JBGraniteBAOTPriceQuoted"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBCabinetryBAOTPriceQuoted", System.Data.SqlDbType.Money, 8, "JBCabinetryBAOTPriceQuoted"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDRep", System.Data.SqlDbType.Int, 4, "EXIDRep"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBBookingDate", System.Data.SqlDbType.DateTime, 8, "JBBookingDate"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBUser", System.Data.SqlDbType.VarChar, 50, "JBUser"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTID", System.Data.SqlDbType.BigInt, 8, "CTID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBUseContact", System.Data.SqlDbType.Bit, 1, "JBUseContact"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, JBID, ID,JBScheduledStartDate,JBScheduledFinishDate, JBClientFirstName, JBClientSurname, JBClientName, JBClient" &
        "StreetAddress01, JBClientStreetAddress02, JBClientSuburb, JBClientState, JBClien" &
        "tPostCode, JBClientAddress, JBClientHomePhoneNumber, JBClientWorkPhoneNumber, JB" &
        "ClientFaxNumber, JBClientMobileNumber, JBJobAddressAsAbove, JBJobStreetAddress01" &
        ", JBJobStreetAddress02, JBJobSuburb, JBJobState, JBJobPostCode, JBJobAddress, JTID, JB" &
        "ReferenceName, EXIDBookingMeasurer, JBJobDescription, JBDoorsType, JBDoorsStyle," &
        " JBDoorsColour, JBIsMeasureOnSite, JBIsAllOrderingComplete, JBDoorsHandles, JBDo" &
        "orsTexture, JBIsCancelled, JBDoorsSupplier, JBClientEmail, JBPriceQuoted, JBGran" &
        "iteBAOTPriceQuoted, JBCabinetryBAOTPriceQuoted, EXIDRep, JBBookingDate, JBUser, " &
        "CTID, JBUseContact FROM Jobs WHERE (BRID = @BRID) AND (JBID = @JBID)"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlUpdateCommand1
        '

        Me.SqlUpdateCommand1.CommandText = "UPDATE Jobs SET BRID = @BRID, ID = @ID,JBScheduledStartDate = @JBScheduledStartDate,JBScheduledFinishDate = @JBScheduledFinishDate, JBClientFirstName = @JBClientFirstName, J" &
        "BClientSurname = @JBClientSurname, JBClientName = @JBClientName, JBClientStreetA" &
        "ddress01 = @JBClientStreetAddress01, JBClientStreetAddress02 = @JBClientStreetAd" &
        "dress02, JBClientSuburb = @JBClientSuburb, JBClientState = @JBClientState, JBCli" &
        "entPostCode = @JBClientPostCode, JBClientAddress = @JBClientAddress, JBClientHom" &
        "ePhoneNumber = @JBClientHomePhoneNumber, JBClientWorkPhoneNumber = @JBClientWork" &
        "PhoneNumber, JBClientFaxNumber = @JBClientFaxNumber, JBClientMobileNumber = @JBC" &
        "lientMobileNumber, JBJobAddressAsAbove = @JBJobAddressAsAbove, JBJobStreetAddres" &
        "s01 = @JBJobStreetAddress01, JBJobStreetAddress02 = @JBJobStreetAddress02, JBJob" &
        "Suburb = @JBJobSuburb, JBJobState = @JBJobState, JBJobPostCode = @JBJobPostCode," &
        " JBJobAddress = @JBJobAddress, JTID = @JTID, JBReferenceName = @JBReferenceName, EXIDBookingMe" &
        "asurer = @EXIDBookingMeasurer, JBJobDescription = @JBJobDescription, JBDoorsType" &
        " = @JBDoorsType, JBDoorsStyle = @JBDoorsStyle, JBDoorsColour = @JBDoorsColour, J" &
        "BIsMeasureOnSite = @JBIsMeasureOnSite, JBIsAllOrderingComplete = @JBIsAllOrderin" &
        "gComplete, JBDoorsHandles = @JBDoorsHandles, JBDoorsTexture = @JBDoorsTexture, J" &
        "BIsCancelled = @JBIsCancelled, JBDoorsSupplier = @JBDoorsSupplier, JBClientEmail" &
        " = @JBClientEmail, JBPriceQuoted = @JBPriceQuoted, JBGraniteBAOTPriceQuoted = @J" &
        "BGraniteBAOTPriceQuoted, JBCabinetryBAOTPriceQuoted = @JBCabinetryBAOTPriceQuote" &
        "d, EXIDRep = @EXIDRep, JBBookingDate = @JBBookingDate, JBUser = @JBUser, CTID = " &
        "@CTID, JBUseContact = @JBUseContact WHERE (BRID = @Original_BRID) And (JBID = @O" &
        "riginal_JBID) And (CTID = @Original_CTID Or @Original_CTID Is NULL And CTID Is N" &
        "ULL) And (EXIDBookingMeasurer = @Original_EXIDBookingMeasurer Or @Original_EXIDB" &
        "ookingMeasurer Is NULL And EXIDBookingMeasurer Is NULL) And (EXIDRep = @Original" &
        "_EXIDRep Or @Original_EXIDRep Is NULL And EXIDRep Is NULL) And (ID = @Original_I" &
        "D) And (JBBookingDate = @Original_JBBookingDate Or @Original_JBBookingDate Is NU" &
        "LL And JBBookingDate Is NULL) And (JBCabinetryBAOTPriceQuoted = @Original_JBCabi" &
        "netryBAOTPriceQuoted Or @Original_JBCabinetryBAOTPriceQuoted Is NULL And JBCabin" &
        "etryBAOTPriceQuoted Is NULL) And (JBClientAddress = @Original_JBClientAddress Or" &
        " @Original_JBClientAddress Is NULL And JBClientAddress Is NULL) And (JBClientEma" &
        "il = @Original_JBClientEmail Or @Original_JBClientEmail Is NULL And JBClientEmai" &
        "l Is NULL) And (JBClientFaxNumber = @Original_JBClientFaxNumber Or @Original_JBC" &
        "lientFaxNumber Is NULL And JBClientFaxNumber Is NULL) And (JBClientFirstName = @" &
        "Original_JBClientFirstName Or @Original_JBClientFirstName Is NULL And JBClientFi" &
        "rstName Is NULL) And (JBClientHomePhoneNumber = @Original_JBClientHomePhoneNumbe" &
        "r Or @Original_JBClientHomePhoneNumber Is NULL And JBClientHomePhoneNumber Is NU" &
        "LL) And (JBClientMobileNumber = @Original_JBClientMobileNumber Or @Original_JBCl" &
        "ientMobileNumber Is NULL And JBClientMobileNumber Is NULL) And (JBClientName = @" &
        "Original_JBClientName Or @Original_JBClientName Is NULL And JBClientName Is NULL" &
        ") And (JBClientPostCode = @Original_JBClientPostCode Or @Original_JBClientPostCo" &
        "de Is NULL And JBClientPostCode Is NULL) And (JBClientState = @Original_JBClient" &
        "State Or @Original_JBClientState Is NULL And JBClientState Is NULL) And (JBClien" &
        "tStreetAddress01 = @Original_JBClientStreetAddress01 Or @Original_JBClientStreet" &
        "Address01 Is NULL And JBClientStreetAddress01 Is NULL) And (JBClientStreetAddres" &
        "s02 = @Original_JBClientStreetAddress02 Or @Original_JBClientStreetAddress02 Is " &
        "NULL And JBClientStreetAddress02 Is NULL) And (JBClientSuburb = @Original_JBClie" &
        "ntSuburb Or @Original_JBClientSuburb Is NULL And JBClientSuburb Is NULL) And (JB" &
        "ClientSurname = @Original_JBClientSurname Or @Original_JBClientSurname Is NULL A" &
        "ND JBClientSurname Is NULL) And (JBClientWorkPhoneNumber = @Original_JBClientWor" &
        "kPhoneNumber Or @Original_JBClientWorkPhoneNumber Is NULL And JBClientWorkPhoneN" &
        "umber Is NULL) And (JBDoorsColour = @Original_JBDoorsColour Or @Original_JBDoors" &
        "Colour Is NULL And JBDoorsColour Is NULL) And (JBDoorsHandles = @Original_JBDoor" &
        "sHandles Or @Original_JBDoorsHandles Is NULL And JBDoorsHandles Is NULL) And (JB" &
        "DoorsStyle = @Original_JBDoorsStyle Or @Original_JBDoorsStyle Is NULL And JBDoor" &
        "sStyle Is NULL) And (JBDoorsSupplier = @Original_JBDoorsSupplier Or @Original_JB" &
        "DoorsSupplier Is NULL And JBDoorsSupplier Is NULL) And (JBDoorsTexture = @Origin" &
        "al_JBDoorsTexture Or @Original_JBDoorsTexture Is NULL And JBDoorsTexture Is NULL" &
        ") And (JBDoorsType = @Original_JBDoorsType Or @Original_JBDoorsType Is NULL And " &
        "JBDoorsType Is NULL) And (JBGraniteBAOTPriceQuoted = @Original_JBGraniteBAOTPric" &
        "eQuoted Or @Original_JBGraniteBAOTPriceQuoted Is NULL And JBGraniteBAOTPriceQuot" &
        "ed Is NULL) And (JBIsAllOrderingComplete = @Original_JBIsAllOrderingComplete Or " &
        "@Original_JBIsAllOrderingComplete Is NULL And JBIsAllOrderingComplete Is NULL) A" &
        "ND (JBIsCancelled = @Original_JBIsCancelled Or @Original_JBIsCancelled Is NULL A" &
        "ND JBIsCancelled Is NULL) And (JBIsMeasureOnSite = @Original_JBIsMeasureOnSite O" &
        "R @Original_JBIsMeasureOnSite Is NULL And JBIsMeasureOnSite Is NULL) And (JBJobA" &
        "ddress = @Original_JBJobAddress Or @Original_JBJobAddress Is NULL And JBJobAddre" &
        "ss Is NULL) And (JTID = @Original_JTID Or @Original_JTID Is NULL And JTID Is N" &
        "ULL) And (JBJobAddressAsAbove = @Original_JBJobAddressAsAbove Or @Origina" &
        "l_JBJobAddressAsAbove Is NULL And JBJobAddressAsAbove Is NULL) And (JBJobDescrip" &
        "tion = @Original_JBJobDescription Or @Original_JBJobDescription Is NULL And JBJo" &
        "bDescription Is NULL) And (JBJobPostCode = @Original_JBJobPostCode Or @Original_" &
        "JBJobPostCode Is NULL And JBJobPostCode Is NULL) And (JBJobState = @Original_JBJ" &
        "obState Or @Original_JBJobState Is NULL And JBJobState Is NULL) And (JBJobStreet" &
        "Address01 = @Original_JBJobStreetAddress01 Or @Original_JBJobStreetAddress01 Is " &
        "NULL And JBJobStreetAddress01 Is NULL) And (JBJobStreetAddress02 = @Original_JBJ" &
        "obStreetAddress02 Or @Original_JBJobStreetAddress02 Is NULL And JBJobStreetAddre" &
        "ss02 Is NULL) And (JBJobSuburb = @Original_JBJobSuburb Or @Original_JBJobSuburb " &
        "Is NULL And JBJobSuburb Is NULL) And (JBPriceQuoted = @Original_JBPriceQuoted Or" &
        " @Original_JBPriceQuoted Is NULL And JBPriceQuoted Is NULL) And (JBReferenceName" &
        " = @Original_JBReferenceName Or @Original_JBReferenceName Is NULL And JBReferenc" &
        "eName Is NULL) And (JBUseContact = @Original_JBUseContact Or @Original_JBUseCont" &
        "act Is NULL And JBUseContact Is NULL) And (JBUser = @Original_JBUser Or @Origina" &
        "l_JBUser Is NULL And JBUser Is NULL); SELECT BRID, JBID, ID, JBClientFirstName, " &
        "JBClientSurname, JBClientName, JBClientStreetAddress01, JBClientStreetAddress02," &
        " JBClientSuburb, JBClientState, JBClientPostCode, JBClientAddress, JBClientHomeP" &
        "honeNumber, JBClientWorkPhoneNumber, JBClientFaxNumber, JBClientMobileNumber, JB" &
        "JobAddressAsAbove, JBJobStreetAddress01, JBJobStreetAddress02, JBJobSuburb, JBJo" &
        "bState, JBJobPostCode, JBJobAddress, JTID, JBReferenceName, EXIDBookingMeasurer, JBJob" &
        "Description, JBDoorsType, JBDoorsStyle, JBDoorsColour, JBIsMeasureOnSite, JBIsAl" &
        "lOrderingComplete, JBDoorsHandles, JBDoorsTexture, JBIsCancelled, JBDoorsSupplie" &
        "r, JBClientEmail, JBPriceQuoted, JBGraniteBAOTPriceQuoted, JBCabinetryBAOTPriceQ" &
        "uoted, EXIDRep, JBBookingDate, JBUser, CTID, JBUseContact FROM Jobs WHERE (BRID " &
        "= @BRID) And (JBID = @JBID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))

        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBScheduledStartDate", System.Data.SqlDbType.DateTime, 8, "JBScheduledStartDate"))

        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBScheduledFinishDate", System.Data.SqlDbType.DateTime, 8, "JBScheduledFinishDate"))


        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientFirstName", System.Data.SqlDbType.VarChar, 50, "JBClientFirstName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientSurname", System.Data.SqlDbType.VarChar, 50, "JBClientSurname"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientName", System.Data.SqlDbType.VarChar, 102, "JBClientName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, "JBClientStreetAddress01"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, "JBClientStreetAddress02"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientSuburb", System.Data.SqlDbType.VarChar, 50, "JBClientSuburb"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientState", System.Data.SqlDbType.VarChar, 3, "JBClientState"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientPostCode", System.Data.SqlDbType.VarChar, 20, "JBClientPostCode"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientAddress", System.Data.SqlDbType.VarChar, 259, "JBClientAddress"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, "JBClientHomePhoneNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, "JBClientWorkPhoneNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientFaxNumber", System.Data.SqlDbType.VarChar, 20, "JBClientFaxNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientMobileNumber", System.Data.SqlDbType.VarChar, 20, "JBClientMobileNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, "JBJobAddressAsAbove"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, "JBJobStreetAddress01"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, "JBJobStreetAddress02"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobSuburb", System.Data.SqlDbType.VarChar, 50, "JBJobSuburb"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobState", System.Data.SqlDbType.VarChar, 3, "JBJobState"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobPostCode", System.Data.SqlDbType.VarChar, 20, "JBJobPostCode"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobAddress", System.Data.SqlDbType.VarChar, 259, "JBJobAddress"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JTID", System.Data.SqlDbType.Int, 4, "JTID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBReferenceName", System.Data.SqlDbType.VarChar, 100, "JBReferenceName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDBookingMeasurer", System.Data.SqlDbType.Int, 4, "EXIDBookingMeasurer"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobDescription", System.Data.SqlDbType.VarChar, 50, "JBJobDescription"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsType", System.Data.SqlDbType.VarChar, 50, "JBDoorsType"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsStyle", System.Data.SqlDbType.VarChar, 50, "JBDoorsStyle"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsColour", System.Data.SqlDbType.VarChar, 50, "JBDoorsColour"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBIsMeasureOnSite", System.Data.SqlDbType.Bit, 1, "JBIsMeasureOnSite"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBIsAllOrderingComplete", System.Data.SqlDbType.Bit, 1, "JBIsAllOrderingComplete"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsHandles", System.Data.SqlDbType.VarChar, 50, "JBDoorsHandles"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsTexture", System.Data.SqlDbType.VarChar, 50, "JBDoorsTexture"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBIsCancelled", System.Data.SqlDbType.Bit, 1, "JBIsCancelled"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsSupplier", System.Data.SqlDbType.VarChar, 50, "JBDoorsSupplier"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientEmail", System.Data.SqlDbType.VarChar, 50, "JBClientEmail"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBPriceQuoted", System.Data.SqlDbType.Money, 8, "JBPriceQuoted"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBGraniteBAOTPriceQuoted", System.Data.SqlDbType.Money, 8, "JBGraniteBAOTPriceQuoted"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBCabinetryBAOTPriceQuoted", System.Data.SqlDbType.Money, 8, "JBCabinetryBAOTPriceQuoted"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDRep", System.Data.SqlDbType.Int, 4, "EXIDRep"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBBookingDate", System.Data.SqlDbType.DateTime, 8, "JBBookingDate"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBUser", System.Data.SqlDbType.VarChar, 50, "JBUser"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTID", System.Data.SqlDbType.BigInt, 8, "CTID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBUseContact", System.Data.SqlDbType.Bit, 1, "JBUseContact"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDBookingMeasurer", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDBookingMeasurer", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDRep", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDRep", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBBookingDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBBookingDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBCabinetryBAOTPriceQuoted", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBCabinetryBAOTPriceQuoted", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientFirstName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientFirstName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientHomePhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientName", System.Data.SqlDbType.VarChar, 102, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientSurname", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientWorkPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsColour", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsColour", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsHandles", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsHandles", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsStyle", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsStyle", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsSupplier", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsSupplier", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsTexture", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsTexture", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsType", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBGraniteBAOTPriceQuoted", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBGraniteBAOTPriceQuoted", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBIsAllOrderingComplete", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBIsAllOrderingComplete", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBIsCancelled", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBIsCancelled", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBIsMeasureOnSite", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBIsMeasureOnSite", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JTID", System.Data.DataRowVersion.Original, Nothing))

        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobAddressAsAbove", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobDescription", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBPriceQuoted", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBPriceQuoted", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBReferenceName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBReferenceName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBUseContact", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBUseContact", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBUser", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBUser", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'colNIPaidByClient1
        '
        Me.colNIPaidByClient1.Caption = "NIPaidByClient"
        Me.colNIPaidByClient1.FieldName = "NIPaidByClient"
        Me.colNIPaidByClient1.Name = "colNIPaidByClient1"
        Me.colNIPaidByClient1.Visible = True
        Me.colNIPaidByClient1.VisibleIndex = 0
        '
        'colNIOrderedByClient1
        '
        Me.colNIOrderedByClient1.Caption = "NIOrderedByClient"
        Me.colNIOrderedByClient1.FieldName = "NIOrderedByClient"
        Me.colNIOrderedByClient1.Name = "colNIOrderedByClient1"
        Me.colNIOrderedByClient1.Visible = True
        Me.colNIOrderedByClient1.VisibleIndex = 0
        '
        'colNIDesc1
        '
        Me.colNIDesc1.Caption = "NIDesc"
        Me.colNIDesc1.FieldName = "NIDesc"
        Me.colNIDesc1.Name = "colNIDesc1"
        Me.colNIDesc1.Visible = True
        Me.colNIDesc1.VisibleIndex = 0
        '
        'colNIName1
        '
        Me.colNIName1.Caption = "NIName"
        Me.colNIName1.FieldName = "NIName"
        Me.colNIName1.Name = "colNIName1"
        Me.colNIName1.Visible = True
        Me.colNIName1.VisibleIndex = 0
        '
        'RepositoryItemLookUpEdit1
        '
        Me.RepositoryItemLookUpEdit1.AutoHeight = False
        Me.RepositoryItemLookUpEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemLookUpEdit1.Name = "RepositoryItemLookUpEdit1"
        '
        'btnHelp
        '
        Me.btnHelp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnHelp.Image = CType(resources.GetObject("btnHelp.Image"), System.Drawing.Image)
        Me.btnHelp.Location = New System.Drawing.Point(8, 536)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.TabIndex = 2
        Me.btnHelp.Text = "Help"
        '
        'pnlMain
        '
        Me.pnlMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlMain.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.pnlMain.Controls.Add(Me.pnlScheduling)
        Me.pnlMain.Controls.Add(Me.PnlOtherTrades)
        Me.pnlMain.Controls.Add(Me.pnlNotes)
        Me.pnlMain.Controls.Add(Me.pnlJobPurchaseOrders)
        Me.pnlMain.Controls.Add(Me.PnlJobInformation)
        Me.pnlMain.Controls.Add(Me.pnlJobAddress)
        Me.pnlMain.Controls.Add(Me.pnlInternalOrders)
        Me.pnlMain.Controls.Add(Me.pnlClientInformation)
        Me.pnlMain.Controls.Add(Me.pnlAppliances)
        Me.pnlMain.Location = New System.Drawing.Point(184, 8)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(536, 520)
        Me.pnlMain.TabIndex = 1
        '
        'pnlNotes
        '
        Me.pnlNotes.Controls.Add(Me.dgNotes)
        Me.pnlNotes.Location = New System.Drawing.Point(48, 32)
        Me.pnlNotes.Name = "pnlNotes"
        Me.pnlNotes.Size = New System.Drawing.Size(480, 408)
        Me.pnlNotes.TabIndex = 19
        Me.pnlNotes.Text = "Notes and Reminders"
        '
        'dgNotes
        '
        Me.dgNotes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgNotes.DataSource = Me.DsNotes.IDNotes
        '
        'dgNotes.EmbeddedNavigator
        '
        Me.dgNotes.EmbeddedNavigator.Name = ""
        Me.dgNotes.Location = New System.Drawing.Point(16, 32)
        Me.dgNotes.MainView = Me.gvNotes
        Me.dgNotes.Name = "dgNotes"
        Me.dgNotes.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtEXID, Me.txtINDate, Me.txtINFollowUpText, Me.txtINNotes})
        Me.dgNotes.Size = New System.Drawing.Size(448, 360)
        Me.dgNotes.TabIndex = 1
        Me.dgNotes.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvNotes})
        '
        'DsNotes
        '
        Me.DsNotes.DataSetName = "dsNotes"
        Me.DsNotes.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'gvNotes
        '
        Me.gvNotes.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvNotes.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvNotes.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvNotes.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvNotes.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvNotes.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvNotes.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvNotes.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvNotes.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvNotes.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvNotes.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvNotes.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvNotes.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvNotes.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvNotes.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvNotes.Appearance.VertLine.Options.UseBackColor = True
        Me.gvNotes.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand1})
        Me.gvNotes.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.colINUser, Me.colINDate, Me.colINNotes, Me.colINFollowUpText})
        Me.gvNotes.GridControl = Me.dgNotes
        Me.gvNotes.Name = "gvNotes"
        Me.gvNotes.NewItemRowText = "Type here to add a new row"
        Me.gvNotes.OptionsCustomization.AllowFilter = False
        Me.gvNotes.OptionsCustomization.AllowRowSizing = True
        Me.gvNotes.OptionsView.ColumnAutoWidth = True
        Me.gvNotes.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvNotes.OptionsView.ShowGroupPanel = False
        Me.gvNotes.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colINDate, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'GridBand1
        '
        Me.GridBand1.Caption = "GridBand1"
        Me.GridBand1.Columns.Add(Me.colINUser)
        Me.GridBand1.Columns.Add(Me.colINDate)
        Me.GridBand1.Columns.Add(Me.colINNotes)
        Me.GridBand1.Columns.Add(Me.colINFollowUpText)
        Me.GridBand1.Name = "GridBand1"
        Me.GridBand1.OptionsBand.ShowCaption = False
        Me.GridBand1.Width = 466
        '
        'colINUser
        '
        Me.colINUser.Caption = "User"
        Me.colINUser.ColumnEdit = Me.txtEXID
        Me.colINUser.FieldName = "INUser"
        Me.colINUser.Name = "colINUser"
        Me.colINUser.Visible = True
        Me.colINUser.Width = 120
        '
        'txtEXID
        '
        Me.txtEXID.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXName")})
        Me.txtEXID.DataSource = Me.dvExpenses2
        Me.txtEXID.DisplayMember = "EXName"
        Me.txtEXID.Name = "txtEXID"
        Me.txtEXID.NullText = ""
        Me.txtEXID.ShowFooter = False
        Me.txtEXID.ShowHeader = False
        Me.txtEXID.ShowLines = False
        Me.txtEXID.ValueMember = "EXName"
        '
        'dvExpenses2
        '
        Me.dvExpenses2.RowFilter = "EGType <> 'OE'"
        Me.dvExpenses2.Sort = "EXName"
        Me.dvExpenses2.Table = Me.DsNotes.VExpenses
        '
        'colINDate
        '
        Me.colINDate.Caption = "Date"
        Me.colINDate.ColumnEdit = Me.txtINDate
        Me.colINDate.FieldName = "INDate"
        Me.colINDate.Name = "colINDate"
        Me.colINDate.Visible = True
        Me.colINDate.Width = 96
        '
        'txtINDate
        '
        Me.txtINDate.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtINDate.Name = "txtINDate"
        '
        'colINNotes
        '
        Me.colINNotes.Caption = "Notes"
        Me.colINNotes.ColumnEdit = Me.txtINNotes
        Me.colINNotes.FieldName = "INNotes"
        Me.colINNotes.Name = "colINNotes"
        Me.colINNotes.Visible = True
        Me.colINNotes.Width = 250
        '
        'txtINNotes
        '
        Me.txtINNotes.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.txtINNotes.Name = "txtINNotes"
        Me.txtINNotes.ReadOnly = True
        Me.txtINNotes.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        '
        'colINFollowUpText
        '
        Me.colINFollowUpText.Caption = "Follow Up Information"
        Me.colINFollowUpText.ColumnEdit = Me.txtINFollowUpText
        Me.colINFollowUpText.FieldName = "INFollowUpText"
        Me.colINFollowUpText.Name = "colINFollowUpText"
        Me.colINFollowUpText.RowIndex = 1
        Me.colINFollowUpText.Visible = True
        Me.colINFollowUpText.Width = 466
        '
        'txtINFollowUpText
        '
        Me.txtINFollowUpText.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.txtINFollowUpText.Name = "txtINFollowUpText"
        Me.txtINFollowUpText.ReadOnly = True
        Me.txtINFollowUpText.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        '
        'pnlInternalOrders
        '
        Me.pnlInternalOrders.Controls.Add(Me.btnRemoveInternalOrder)
        Me.pnlInternalOrders.Controls.Add(Me.btnEditInternalOrder)
        Me.pnlInternalOrders.Controls.Add(Me.btnAddInternalOrder)
        Me.pnlInternalOrders.Controls.Add(Me.dgInternalOrders)
        Me.pnlInternalOrders.Location = New System.Drawing.Point(56, 104)
        Me.pnlInternalOrders.Name = "pnlInternalOrders"
        Me.pnlInternalOrders.Size = New System.Drawing.Size(448, 296)
        Me.pnlInternalOrders.TabIndex = 20
        Me.pnlInternalOrders.Text = "Internal Orders"
        '
        'btnRemoveInternalOrder
        '
        Me.btnRemoveInternalOrder.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveInternalOrder.Image = CType(resources.GetObject("btnRemoveInternalOrder.Image"), System.Drawing.Image)
        Me.btnRemoveInternalOrder.Location = New System.Drawing.Point(176, 264)
        Me.btnRemoveInternalOrder.Name = "btnRemoveInternalOrder"
        Me.btnRemoveInternalOrder.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveInternalOrder.TabIndex = 3
        Me.btnRemoveInternalOrder.Text = "Remove"
        '
        'btnEditInternalOrder
        '
        Me.btnEditInternalOrder.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEditInternalOrder.Image = CType(resources.GetObject("btnEditInternalOrder.Image"), System.Drawing.Image)
        Me.btnEditInternalOrder.Location = New System.Drawing.Point(96, 264)
        Me.btnEditInternalOrder.Name = "btnEditInternalOrder"
        Me.btnEditInternalOrder.Size = New System.Drawing.Size(72, 23)
        Me.btnEditInternalOrder.TabIndex = 2
        Me.btnEditInternalOrder.Text = "Edit..."
        '
        'btnAddInternalOrder
        '
        Me.btnAddInternalOrder.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddInternalOrder.Image = CType(resources.GetObject("btnAddInternalOrder.Image"), System.Drawing.Image)
        Me.btnAddInternalOrder.Location = New System.Drawing.Point(16, 264)
        Me.btnAddInternalOrder.Name = "btnAddInternalOrder"
        Me.btnAddInternalOrder.Size = New System.Drawing.Size(72, 23)
        Me.btnAddInternalOrder.TabIndex = 1
        Me.btnAddInternalOrder.Text = "Add..."
        '
        'dgInternalOrders
        '
        Me.dgInternalOrders.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgInternalOrders.DataSource = Me.dvInternalOrders
        '
        'dgInternalOrders.EmbeddedNavigator
        '
        Me.dgInternalOrders.EmbeddedNavigator.Name = ""
        Me.dgInternalOrders.Location = New System.Drawing.Point(16, 32)
        Me.dgInternalOrders.MainView = Me.gvInternalOrders
        Me.dgInternalOrders.Name = "dgInternalOrders"
        Me.dgInternalOrders.ShowOnlyPredefinedDetails = True
        Me.dgInternalOrders.Size = New System.Drawing.Size(416, 224)
        Me.dgInternalOrders.TabIndex = 0
        Me.dgInternalOrders.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvInternalOrders})
        '
        'dvInternalOrders
        '
        Me.dvInternalOrders.RowFilter = "ORType = 'IO'"
        Me.dvInternalOrders.Table = Me.DsOrders.VOrders
        '
        'gvInternalOrders
        '
        Me.gvInternalOrders.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.gvInternalOrders.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvInternalOrders.Appearance.FocusedCell.Options.UseBackColor = True
        Me.gvInternalOrders.Appearance.FocusedCell.Options.UseForeColor = True
        Me.gvInternalOrders.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.gvInternalOrders.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gvInternalOrders.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvInternalOrders.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvInternalOrders.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colOROrderNumber, Me.colORDate, Me.colORDescription})
        Me.gvInternalOrders.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.gvInternalOrders.GridControl = Me.dgInternalOrders
        Me.gvInternalOrders.Name = "gvInternalOrders"
        Me.gvInternalOrders.OptionsBehavior.Editable = False
        Me.gvInternalOrders.OptionsCustomization.AllowFilter = False
        Me.gvInternalOrders.OptionsNavigation.AutoFocusNewRow = True
        Me.gvInternalOrders.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.gvInternalOrders.OptionsView.ShowGroupPanel = False
        Me.gvInternalOrders.OptionsView.ShowHorzLines = False
        Me.gvInternalOrders.OptionsView.ShowIndicator = False
        Me.gvInternalOrders.OptionsView.ShowVertLines = False
        Me.gvInternalOrders.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colOROrderNumber, DevExpress.Data.ColumnSortOrder.Descending)})
        '
        'colOROrderNumber
        '
        Me.colOROrderNumber.Caption = "Order #"
        Me.colOROrderNumber.FieldName = "OROrderNumber"
        Me.colOROrderNumber.Name = "colOROrderNumber"
        Me.colOROrderNumber.Visible = True
        Me.colOROrderNumber.VisibleIndex = 0
        Me.colOROrderNumber.Width = 51
        '
        'colORDate
        '
        Me.colORDate.Caption = "Date"
        Me.colORDate.FieldName = "ORDate"
        Me.colORDate.Name = "colORDate"
        Me.colORDate.Visible = True
        Me.colORDate.VisibleIndex = 1
        Me.colORDate.Width = 143
        '
        'colORDescription
        '
        Me.colORDescription.Caption = "Description"
        Me.colORDescription.FieldName = "ORDescription"
        Me.colORDescription.Name = "colORDescription"
        Me.colORDescription.Visible = True
        Me.colORDescription.VisibleIndex = 2
        Me.colORDescription.Width = 273
        '
        'daExpenses
        '
        Me.daExpenses.InsertCommand = Me.SqlInsertCommand3
        Me.daExpenses.SelectCommand = Me.SqlSelectCommand3
        Me.daExpenses.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VExpenses", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("EXName", "EXName"), New System.Data.Common.DataColumnMapping("EGType", "EGType"), New System.Data.Common.DataColumnMapping("EXAppearInCalendarRC", "EXAppearInCalendarRC"), New System.Data.Common.DataColumnMapping("EXAppearInCalendarDL", "EXAppearInCalendarDL"), New System.Data.Common.DataColumnMapping("EXCalendarName", "EXCalendarName"), New System.Data.Common.DataColumnMapping("EGName", "EGName"), New System.Data.Common.DataColumnMapping("EXAppearInMeasurer", "EXAppearInMeasurer"), New System.Data.Common.DataColumnMapping("EXAppearInSalesperson", "EXAppearInSalesperson")})})
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO VExpenses(BRID, EXName, EGType, EXAppearInCalendarRC, EXAppearInCalen" & _
        "darDL, EXCalendarName, EGName, EXAppearInMeasurer, EXAppearInSalesperson) VALUES" & _
        " (@BRID, @EXName, @EGType, @EXAppearInCalendarRC, @EXAppearInCalendarDL, @EXCale" & _
        "ndarName, @EGName, @EXAppearInMeasurer, @EXAppearInSalesperson); SELECT BRID, EX" & _
        "ID, EXName, EGType, EXAppearInCalendarRC, EXAppearInCalendarDL, EXCalendarName, " & _
        "EGName, EXAppearInMeasurer, EXAppearInSalesperson FROM VExpenses"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 2, "EGType"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAppearInCalendarRC", System.Data.SqlDbType.Bit, 1, "EXAppearInCalendarRC"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAppearInCalendarDL", System.Data.SqlDbType.Bit, 1, "EXAppearInCalendarDL"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXCalendarName", System.Data.SqlDbType.VarChar, 103, "EXCalendarName"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGName", System.Data.SqlDbType.VarChar, 50, "EGName"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAppearInMeasurer", System.Data.SqlDbType.Bit, 1, "EXAppearInMeasurer"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAppearInSalesperson", System.Data.SqlDbType.Bit, 1, "EXAppearInSalesperson"))
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT BRID, EXID, EXName, EGType, EXAppearInCalendarRC, EXAppearInCalendarDL, EX" & _
        "CalendarName, EGName, EXAppearInMeasurer, EXAppearInSalesperson FROM VExpenses W" & _
        "HERE (EXDiscontinued = 0) AND (BRID = @BRID)"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection
        Me.SqlSelectCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'daAppointments
        '
        Me.daAppointments.DeleteCommand = Me.SqlDeleteCommand2
        Me.daAppointments.InsertCommand = Me.SqlInsertCommand2
        Me.daAppointments.SelectCommand = Me.SqlSelectCommand2
        Me.daAppointments.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VAppointments", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("APID", "APID"), New System.Data.Common.DataColumnMapping("APBegin", "APBegin"), New System.Data.Common.DataColumnMapping("APEnd", "APEnd"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("APType", "APType"), New System.Data.Common.DataColumnMapping("APTypeID", "APTypeID"), New System.Data.Common.DataColumnMapping("APNotes", "APNotes"), New System.Data.Common.DataColumnMapping("APStatus", "APStatus"), New System.Data.Common.DataColumnMapping("APTask", "APTask"), New System.Data.Common.DataColumnMapping("APAllDay", "APAllDay")})})
        Me.daAppointments.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM Appointments WHERE (APID = @Original_APID) AND (BRID = @Original_BRID" &
        ");"

        'Me.SqlDeleteCommand2.CommandText = "UPDATE Jobs SET JBScheduledStartDate = NULL, JBScheduledFinishDate = NULL WHERE (JBID in " & " (select APTypeID from Appointments where (APID = @Original_APID)));DELETE FROM Appointments WHERE (APID = " & "@Original_APID) AND (BRID = @Original_BRID);"


        Me.SqlDeleteCommand2.Connection = Me.SqlConnection
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand2
        '

        Me.SqlInsertCommand2.CommandText = "INSERT INTO Appointments (BRID, APBegin, APEnd, EXID, APType, APTypeID, APNotes, " &
        "APStatus, APTask, APAllDay) VALUES (@BRID, @APBegin, @APEnd, @EXID, @APType, @AP" &
        "TypeID, @APNotes, @APStatus, @APTask, @APAllDay); SELECT BRID, APID, APBegin, AP" &
        "End, EXID, APType, APTypeID, APNotes, APStatus, APTask, APAllDay, APDescription," &
        " APClientName, EXName, ID, APJobDescription, APClientHomePhoneNumber, APClientWo" &
        "rkPhoneNumber, APClientMobileNumber, APSuburb, TAName, JBPriceQuoted, APAddressM" &
        "ultiline FROM VAppointments WHERE (APID = @@IDENTITY) AND (BRID = @BRID);" &
        "Update Jobs set JBScheduledStartDate = @APBegin, JBScheduledFinishDate = @APEnd WHERE (JBID = @APTypeID);"




        Me.SqlInsertCommand2.Connection = Me.SqlConnection
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APBegin", System.Data.SqlDbType.DateTime, 8, "APBegin"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APEnd", System.Data.SqlDbType.DateTime, 8, "APEnd"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APType", System.Data.SqlDbType.VarChar, 2, "APType"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTypeID", System.Data.SqlDbType.BigInt, 8, "APTypeID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APNotes", System.Data.SqlDbType.VarChar, 2000, "APNotes"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APStatus", System.Data.SqlDbType.Int, 4, "APStatus"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTask", System.Data.SqlDbType.Int, 4, "APTask"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APAllDay", System.Data.SqlDbType.Bit, 1, "APAllDay"))
        '
        'SqlSelectCommand2
        '
        'Me.SqlSelectCommand2.CommandText = "SELECT BRID, APID, APBegin, APEnd, EXID, APType, APTypeID, APNotes, APStatus, APT" &
        '"ask, APAllDay, APDescription, APClientName, EXName, ID, APJobDescription, APClie" &
        '"ntHomePhoneNumber, APClientWorkPhoneNumber, APClientMobileNumber, APSuburb, TANa" &
        '"me, JBPriceQuoted, APAddressMultiline FROM VAppointments WHERE (BRID = @BRID) AN" &
        '"D (APIsCancelled = 0) AND (APBegin < @TO_DATE OR @TO_DATE IS NULL) AND (APEnd > " &
        '"@FROM_DATE OR @FROM_DATE IS NULL)"



        Me.SqlSelectCommand2.CommandText = "SELECT BRID, APID, APBegin, APEnd, EXID, APType, APTypeID, APNotes, APStatus, APT" &
        "ask, APAllDay, APDescription, APClientName, EXName, ID, APJobDescription, APClie" &
        "ntHomePhoneNumber, APClientWorkPhoneNumber, APClientMobileNumber, APSuburb, TANa" &
        "me, JBPriceQuoted, APAddressMultiline FROM VAppointments WHERE (BRID = @BRID) AN" &
        "D (APIsCancelled = 0)"


        Me.SqlSelectCommand2.Connection = Me.SqlConnection
        Me.SqlSelectCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TO_DATE", System.Data.SqlDbType.DateTime, 8, "APBegin"))
        Me.SqlSelectCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FROM_DATE", System.Data.SqlDbType.DateTime, 8, "APEnd"))
        '
        'SqlUpdateCommand2
        '

        Me.SqlUpdateCommand2.CommandText = "UPDATE Appointments SET BRID = @BRID, APBegin = @APBegin, APEnd = @APEnd, EXID = " &
        "@EXID, APType = @APType, APTypeID = @APTypeID, APNotes = @APNotes, APStatus = @A" &
        "PStatus, APTask = @APTask, APAllDay = @APAllDay WHERE (APID = @Original_APID) AN" &
        "D (BRID = @Original_BRID); SELECT BRID, APID, APBegin, APEnd, EXID, APType, APTy" &
        "peID, APNotes, APStatus, APTask, APAllDay, APDescription, APClientName, EXName, " &
        "ID, APJobDescription, APClientHomePhoneNumber, APClientWorkPhoneNumber, APClient" &
        "MobileNumber, APSuburb, TAName, JBPriceQuoted, APAddressMultiline FROM VAppointm" &
        "ents WHERE (APID = @APID) AND (BRID = @BRID); Update Jobs set JBScheduledStartDate = @APBegin, JBScheduledFinishDate = @APEnd " & " where (JBID = @APTypeID);"

        Me.SqlUpdateCommand2.Connection = Me.SqlConnection
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APBegin", System.Data.SqlDbType.DateTime, 8, "APBegin"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APEnd", System.Data.SqlDbType.DateTime, 8, "APEnd"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APType", System.Data.SqlDbType.VarChar, 2, "APType"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTypeID", System.Data.SqlDbType.BigInt, 8, "APTypeID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APNotes", System.Data.SqlDbType.VarChar, 2000, "APNotes"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APStatus", System.Data.SqlDbType.Int, 4, "APStatus"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTask", System.Data.SqlDbType.Int, 4, "APTask"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APAllDay", System.Data.SqlDbType.Bit, 1, "APAllDay"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APID", System.Data.SqlDbType.BigInt, 8, "APID"))
        '
        'Storage
        '
        Me.Storage.Appointments.DataSource = Me.dvAppointments
        Me.Storage.Appointments.Mappings.AllDay = "APAllDay"
        Me.Storage.Appointments.Mappings.End = "APEnd"
        Me.Storage.Appointments.Mappings.Label = "APTask"
        Me.Storage.Appointments.Mappings.Location = "APSuburb"
        Me.Storage.Appointments.Mappings.ResourceId = "EXID"
        Me.Storage.Appointments.Mappings.Start = "APBegin"
        Me.Storage.Appointments.Mappings.Status = "APStatus"
        Me.Storage.Appointments.Mappings.Subject = "APDescription"
        Me.Storage.Appointments.Statuses.Add(New DevExpress.XtraScheduler.AppointmentStatus(DevExpress.XtraScheduler.AppointmentStatusType.Free, "Free", "&Free"))
        Me.Storage.Appointments.Statuses.Add(New DevExpress.XtraScheduler.AppointmentStatus(DevExpress.XtraScheduler.AppointmentStatusType.Tentative, System.Drawing.Color.FromArgb(CType(74, Byte), CType(135, Byte), CType(226, Byte)), "Tentative", "&Tentative"))
        Me.Storage.Appointments.Statuses.Add(New DevExpress.XtraScheduler.AppointmentStatus(DevExpress.XtraScheduler.AppointmentStatusType.Busy, System.Drawing.Color.FromArgb(CType(74, Byte), CType(135, Byte), CType(226, Byte)), "Busy", "&Busy"))
        Me.Storage.Appointments.Statuses.Add(New DevExpress.XtraScheduler.AppointmentStatus(DevExpress.XtraScheduler.AppointmentStatusType.OutOfOffice, System.Drawing.Color.FromArgb(CType(217, Byte), CType(83, Byte), CType(83, Byte)), "Off Work", "&Off Work"))
        Me.Storage.Resources.DataSource = Me.dvResources
        Me.Storage.Resources.Mappings.Caption = "EXCalendarName"
        Me.Storage.Resources.Mappings.Id = "EXID"
        '
        'daTasks
        '
        Me.daTasks.InsertCommand = Me.SqlCommand1
        Me.daTasks.SelectCommand = Me.SqlCommand2
        Me.daTasks.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Tasks", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("TAName", "TAName"), New System.Data.Common.DataColumnMapping("TAControlsJobDate", "TAControlsJobDate"), New System.Data.Common.DataColumnMapping("TAMenuCaption", "TAMenuCaption"), New System.Data.Common.DataColumnMapping("TAColourR", "TAColourR"), New System.Data.Common.DataColumnMapping("TAColourG", "TAColourG"), New System.Data.Common.DataColumnMapping("TAColourB", "TAColourB"), New System.Data.Common.DataColumnMapping("TAID", "TAID")})})
        '
        'SqlCommand1
        '
        Me.SqlCommand1.CommandText = "INSERT INTO Tasks(TAName, TAControlsJobDate, TAMenuCaption, TAColourR, TAColourG," & _
        " TAColourB, TAID) VALUES (@TAName, @TAControlsJobDate, @TAMenuCaption, @TAColour" & _
        "R, @TAColourG, @TAColourB, @TAID); SELECT TAName, TAControlsJobDate, TAMenuCapti" & _
        "on, TAColourR, TAColourG, TAColourB, TAID FROM Tasks ORDER BY TAID"
        Me.SqlCommand1.Connection = Me.SqlConnection
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAName", System.Data.SqlDbType.VarChar, 50, "TAName"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAControlsJobDate", System.Data.SqlDbType.Bit, 1, "TAControlsJobDate"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAMenuCaption", System.Data.SqlDbType.VarChar, 50, "TAMenuCaption"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourR", System.Data.SqlDbType.SmallInt, 2, "TAColourR"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourG", System.Data.SqlDbType.SmallInt, 2, "TAColourG"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourB", System.Data.SqlDbType.SmallInt, 2, "TAColourB"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAID", System.Data.SqlDbType.Int, 4, "TAID"))
        '
        'SqlCommand2
        '
        Me.SqlCommand2.CommandText = "SELECT TAName, TAControlsJobDate, TAMenuCaption, TAColourR, TAColourG, TAColourB," & _
        " TAID FROM Tasks ORDER BY TAID"
        Me.SqlCommand2.Connection = Me.SqlConnection
        '
        'daLeads
        '
        Me.daLeads.DeleteCommand = Me.SqlDeleteCommand4
        Me.daLeads.InsertCommand = Me.SqlInsertCommand4
        Me.daLeads.SelectCommand = Me.SqlSelectCommand4
        Me.daLeads.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Leads", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("LDID", "LDID"), New System.Data.Common.DataColumnMapping("ID", "ID"), New System.Data.Common.DataColumnMapping("LDDate", "LDDate"), New System.Data.Common.DataColumnMapping("LDClientFirstName", "LDClientFirstName"), New System.Data.Common.DataColumnMapping("LDClientSurname", "LDClientSurname"), New System.Data.Common.DataColumnMapping("LDClientName", "LDClientName"), New System.Data.Common.DataColumnMapping("LDClientStreetAddress01", "LDClientStreetAddress01"), New System.Data.Common.DataColumnMapping("LDClientStreetAddress02", "LDClientStreetAddress02"), New System.Data.Common.DataColumnMapping("LDClientSuburb", "LDClientSuburb"), New System.Data.Common.DataColumnMapping("LDClientState", "LDClientState"), New System.Data.Common.DataColumnMapping("LDClientPostCode", "LDClientPostCode"), New System.Data.Common.DataColumnMapping("LDClientHomePhoneNumber", "LDClientHomePhoneNumber"), New System.Data.Common.DataColumnMapping("LDClientWorkPhoneNumber", "LDClientWorkPhoneNumber"), New System.Data.Common.DataColumnMapping("LDClientFaxNumber", "LDClientFaxNumber"), New System.Data.Common.DataColumnMapping("LDClientMobileNumber", "LDClientMobileNumber"), New System.Data.Common.DataColumnMapping("LDJobAddressAsAbove", "LDJobAddressAsAbove"), New System.Data.Common.DataColumnMapping("LDJobStreetAddress01", "LDJobStreetAddress01"), New System.Data.Common.DataColumnMapping("LDJobStreetAddress02", "LDJobStreetAddress02"), New System.Data.Common.DataColumnMapping("LDJobSuburb", "LDJobSuburb"), New System.Data.Common.DataColumnMapping("LDJobState", "LDJobState"), New System.Data.Common.DataColumnMapping("LDJobPostCode", "LDJobPostCode"), New System.Data.Common.DataColumnMapping("LDJobAddress", "LDJobAddress"), New System.Data.Common.DataColumnMapping("LDReferenceName", "LDReferenceName"), New System.Data.Common.DataColumnMapping("LSID", "LSID"), New System.Data.Common.DataColumnMapping("LDHasAppointments", "LDHasAppointments"), New System.Data.Common.DataColumnMapping("LDUser", "LDUser"), New System.Data.Common.DataColumnMapping("LDJobDescription", "LDJobDescription"), New System.Data.Common.DataColumnMapping("EXIDRep", "EXIDRep"), New System.Data.Common.DataColumnMapping("LDQuoteReceived", "LDQuoteReceived"), New System.Data.Common.DataColumnMapping("LDClientAddress", "LDClientAddress"), New System.Data.Common.DataColumnMapping("LDClientEmail", "LDClientEmail"), New System.Data.Common.DataColumnMapping("LDJobAddressMultiline", "LDJobAddressMultiline"), New System.Data.Common.DataColumnMapping("LDIsCancelled", "LDIsCancelled"), New System.Data.Common.DataColumnMapping("CTID", "CTID"), New System.Data.Common.DataColumnMapping("LDUseContact", "LDUseContact")})})
        Me.daLeads.UpdateCommand = Me.SqlUpdateCommand4
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = "DELETE FROM Leads WHERE (BRID = @Original_BRID) AND (LDID = @Original_LDID) AND (" & _
        "CTID = @Original_CTID OR @Original_CTID IS NULL AND CTID IS NULL) AND (EXIDRep =" & _
        " @Original_EXIDRep OR @Original_EXIDRep IS NULL AND EXIDRep IS NULL) AND (ID = @" & _
        "Original_ID) AND (LDClientAddress = @Original_LDClientAddress OR @Original_LDCli" & _
        "entAddress IS NULL AND LDClientAddress IS NULL) AND (LDClientEmail = @Original_L" & _
        "DClientEmail OR @Original_LDClientEmail IS NULL AND LDClientEmail IS NULL) AND (" & _
        "LDClientFaxNumber = @Original_LDClientFaxNumber OR @Original_LDClientFaxNumber I" & _
        "S NULL AND LDClientFaxNumber IS NULL) AND (LDClientFirstName = @Original_LDClien" & _
        "tFirstName OR @Original_LDClientFirstName IS NULL AND LDClientFirstName IS NULL)" & _
        " AND (LDClientHomePhoneNumber = @Original_LDClientHomePhoneNumber OR @Original_L" & _
        "DClientHomePhoneNumber IS NULL AND LDClientHomePhoneNumber IS NULL) AND (LDClien" & _
        "tMobileNumber = @Original_LDClientMobileNumber OR @Original_LDClientMobileNumber" & _
        " IS NULL AND LDClientMobileNumber IS NULL) AND (LDClientName = @Original_LDClien" & _
        "tName OR @Original_LDClientName IS NULL AND LDClientName IS NULL) AND (LDClientP" & _
        "ostCode = @Original_LDClientPostCode OR @Original_LDClientPostCode IS NULL AND L" & _
        "DClientPostCode IS NULL) AND (LDClientState = @Original_LDClientState OR @Origin" & _
        "al_LDClientState IS NULL AND LDClientState IS NULL) AND (LDClientStreetAddress01" & _
        " = @Original_LDClientStreetAddress01 OR @Original_LDClientStreetAddress01 IS NUL" & _
        "L AND LDClientStreetAddress01 IS NULL) AND (LDClientStreetAddress02 = @Original_" & _
        "LDClientStreetAddress02 OR @Original_LDClientStreetAddress02 IS NULL AND LDClien" & _
        "tStreetAddress02 IS NULL) AND (LDClientSuburb = @Original_LDClientSuburb OR @Ori" & _
        "ginal_LDClientSuburb IS NULL AND LDClientSuburb IS NULL) AND (LDClientSurname = " & _
        "@Original_LDClientSurname OR @Original_LDClientSurname IS NULL AND LDClientSurna" & _
        "me IS NULL) AND (LDClientWorkPhoneNumber = @Original_LDClientWorkPhoneNumber OR " & _
        "@Original_LDClientWorkPhoneNumber IS NULL AND LDClientWorkPhoneNumber IS NULL) A" & _
        "ND (LDDate = @Original_LDDate OR @Original_LDDate IS NULL AND LDDate IS NULL) AN" & _
        "D (LDHasAppointments = @Original_LDHasAppointments OR @Original_LDHasAppointment" & _
        "s IS NULL AND LDHasAppointments IS NULL) AND (LDIsCancelled = @Original_LDIsCanc" & _
        "elled OR @Original_LDIsCancelled IS NULL AND LDIsCancelled IS NULL) AND (LDJobAd" & _
        "dress = @Original_LDJobAddress OR @Original_LDJobAddress IS NULL AND LDJobAddres" & _
        "s IS NULL) AND (LDJobAddressAsAbove = @Original_LDJobAddressAsAbove OR @Original" & _
        "_LDJobAddressAsAbove IS NULL AND LDJobAddressAsAbove IS NULL) AND (LDJobAddressM" & _
        "ultiline = @Original_LDJobAddressMultiline OR @Original_LDJobAddressMultiline IS" & _
        " NULL AND LDJobAddressMultiline IS NULL) AND (LDJobDescription = @Original_LDJob" & _
        "Description OR @Original_LDJobDescription IS NULL AND LDJobDescription IS NULL) " & _
        "AND (LDJobPostCode = @Original_LDJobPostCode OR @Original_LDJobPostCode IS NULL " & _
        "AND LDJobPostCode IS NULL) AND (LDJobState = @Original_LDJobState OR @Original_L" & _
        "DJobState IS NULL AND LDJobState IS NULL) AND (LDJobStreetAddress01 = @Original_" & _
        "LDJobStreetAddress01 OR @Original_LDJobStreetAddress01 IS NULL AND LDJobStreetAd" & _
        "dress01 IS NULL) AND (LDJobStreetAddress02 = @Original_LDJobStreetAddress02 OR @" & _
        "Original_LDJobStreetAddress02 IS NULL AND LDJobStreetAddress02 IS NULL) AND (LDJ" & _
        "obSuburb = @Original_LDJobSuburb OR @Original_LDJobSuburb IS NULL AND LDJobSubur" & _
        "b IS NULL) AND (LDQuoteReceived = @Original_LDQuoteReceived OR @Original_LDQuote" & _
        "Received IS NULL AND LDQuoteReceived IS NULL) AND (LDReferenceName = @Original_L" & _
        "DReferenceName OR @Original_LDReferenceName IS NULL AND LDReferenceName IS NULL)" & _
        " AND (LDUseContact = @Original_LDUseContact OR @Original_LDUseContact IS NULL AN" & _
        "D LDUseContact IS NULL) AND (LDUser = @Original_LDUser OR @Original_LDUser IS NU" & _
        "LL AND LDUser IS NULL) AND (LSID = @Original_LSID OR @Original_LSID IS NULL AND " & _
        "LSID IS NULL)"
        Me.SqlDeleteCommand4.Connection = Me.SqlConnection
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDRep", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDRep", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientFirstName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientFirstName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientHomePhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientName", System.Data.SqlDbType.VarChar, 102, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientSurname", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientWorkPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDHasAppointments", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDHasAppointments", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDIsCancelled", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDIsCancelled", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobAddressAsAbove", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobAddressMultiline", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobAddressMultiline", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobDescription", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDQuoteReceived", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDQuoteReceived", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDReferenceName", System.Data.SqlDbType.VarChar, 200, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDReferenceName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDUseContact", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDUseContact", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDUser", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDUser", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LSID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LSID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = "INSERT INTO Leads(BRID, ID, LDDate, LDClientFirstName, LDClientSurname, LDClientS" & _
        "treetAddress01, LDClientStreetAddress02, LDClientSuburb, LDClientState, LDClient" & _
        "PostCode, LDClientHomePhoneNumber, LDClientWorkPhoneNumber, LDClientFaxNumber, L" & _
        "DClientMobileNumber, LDJobAddressAsAbove, LDJobStreetAddress01, LDJobStreetAddre" & _
        "ss02, LDJobSuburb, LDJobState, LDJobPostCode, LDReferenceName, LSID, LDHasAppoin" & _
        "tments, LDUser, LDJobDescription, EXIDRep, LDQuoteReceived, LDClientEmail, LDIsC" & _
        "ancelled, CTID, LDUseContact) VALUES (@BRID, @ID, @LDDate, @LDClientFirstName, @" & _
        "LDClientSurname, @LDClientStreetAddress01, @LDClientStreetAddress02, @LDClientSu" & _
        "burb, @LDClientState, @LDClientPostCode, @LDClientHomePhoneNumber, @LDClientWork" & _
        "PhoneNumber, @LDClientFaxNumber, @LDClientMobileNumber, @LDJobAddressAsAbove, @L" & _
        "DJobStreetAddress01, @LDJobStreetAddress02, @LDJobSuburb, @LDJobState, @LDJobPos" & _
        "tCode, @LDReferenceName, @LSID, @LDHasAppointments, @LDUser, @LDJobDescription, " & _
        "@EXIDRep, @LDQuoteReceived, @LDClientEmail, @LDIsCancelled, @CTID, @LDUseContact" & _
        "); SELECT BRID, LDID, ID, LDDate, LDClientFirstName, LDClientSurname, LDClientNa" & _
        "me, LDClientStreetAddress01, LDClientStreetAddress02, LDClientSuburb, LDClientSt" & _
        "ate, LDClientPostCode, LDClientHomePhoneNumber, LDClientWorkPhoneNumber, LDClien" & _
        "tFaxNumber, LDClientMobileNumber, LDJobAddressAsAbove, LDJobStreetAddress01, LDJ" & _
        "obStreetAddress02, LDJobSuburb, LDJobState, LDJobPostCode, LDJobAddress, LDRefer" & _
        "enceName, LSID, LDHasAppointments, LDUser, LDJobDescription, EXIDRep, LDQuoteRec" & _
        "eived, LDClientAddress, LDClientEmail, LDJobAddressMultiline, LDIsCancelled, CTI" & _
        "D, LDUseContact FROM Leads WHERE (BRID = @BRID) AND (LDID = @@IDENTITY)"
        Me.SqlInsertCommand4.Connection = Me.SqlConnection
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDDate", System.Data.SqlDbType.DateTime, 8, "LDDate"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientFirstName", System.Data.SqlDbType.VarChar, 50, "LDClientFirstName"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientSurname", System.Data.SqlDbType.VarChar, 50, "LDClientSurname"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, "LDClientStreetAddress01"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, "LDClientStreetAddress02"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientSuburb", System.Data.SqlDbType.VarChar, 50, "LDClientSuburb"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientState", System.Data.SqlDbType.VarChar, 3, "LDClientState"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientPostCode", System.Data.SqlDbType.VarChar, 20, "LDClientPostCode"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, "LDClientHomePhoneNumber"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, "LDClientWorkPhoneNumber"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientFaxNumber", System.Data.SqlDbType.VarChar, 20, "LDClientFaxNumber"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientMobileNumber", System.Data.SqlDbType.VarChar, 20, "LDClientMobileNumber"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, "LDJobAddressAsAbove"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, "LDJobStreetAddress01"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, "LDJobStreetAddress02"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobSuburb", System.Data.SqlDbType.VarChar, 50, "LDJobSuburb"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobState", System.Data.SqlDbType.VarChar, 3, "LDJobState"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobPostCode", System.Data.SqlDbType.VarChar, 20, "LDJobPostCode"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDReferenceName", System.Data.SqlDbType.VarChar, 200, "LDReferenceName"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LSID", System.Data.SqlDbType.Int, 4, "LSID"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDHasAppointments", System.Data.SqlDbType.Bit, 1, "LDHasAppointments"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDUser", System.Data.SqlDbType.VarChar, 50, "LDUser"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobDescription", System.Data.SqlDbType.VarChar, 50, "LDJobDescription"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDRep", System.Data.SqlDbType.Int, 4, "EXIDRep"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDQuoteReceived", System.Data.SqlDbType.Bit, 1, "LDQuoteReceived"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientEmail", System.Data.SqlDbType.VarChar, 50, "LDClientEmail"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDIsCancelled", System.Data.SqlDbType.Bit, 1, "LDIsCancelled"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTID", System.Data.SqlDbType.BigInt, 8, "CTID"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDUseContact", System.Data.SqlDbType.Bit, 1, "LDUseContact"))
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT BRID, LDID, ID, LDDate, LDClientFirstName, LDClientSurname, LDClientName, " & _
        "LDClientStreetAddress01, LDClientStreetAddress02, LDClientSuburb, LDClientState," & _
        " LDClientPostCode, LDClientHomePhoneNumber, LDClientWorkPhoneNumber, LDClientFax" & _
        "Number, LDClientMobileNumber, LDJobAddressAsAbove, LDJobStreetAddress01, LDJobSt" & _
        "reetAddress02, LDJobSuburb, LDJobState, LDJobPostCode, LDJobAddress, LDReference" & _
        "Name, LSID, LDHasAppointments, LDUser, LDJobDescription, EXIDRep, LDQuoteReceive" & _
        "d, LDClientAddress, LDClientEmail, LDJobAddressMultiline, LDIsCancelled, CTID, L" & _
        "DUseContact FROM Leads WHERE (BRID = @BRID) AND (LDID = @LDID)"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection
        Me.SqlSelectCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDID", System.Data.SqlDbType.BigInt, 8, "LDID"))
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = "UPDATE Leads SET BRID = @BRID, ID = @ID, LDDate = @LDDate, LDClientFirstName = @L" & _
        "DClientFirstName, LDClientSurname = @LDClientSurname, LDClientStreetAddress01 = " & _
        "@LDClientStreetAddress01, LDClientStreetAddress02 = @LDClientStreetAddress02, LD" & _
        "ClientSuburb = @LDClientSuburb, LDClientState = @LDClientState, LDClientPostCode" & _
        " = @LDClientPostCode, LDClientHomePhoneNumber = @LDClientHomePhoneNumber, LDClie" & _
        "ntWorkPhoneNumber = @LDClientWorkPhoneNumber, LDClientFaxNumber = @LDClientFaxNu" & _
        "mber, LDClientMobileNumber = @LDClientMobileNumber, LDJobAddressAsAbove = @LDJob" & _
        "AddressAsAbove, LDJobStreetAddress01 = @LDJobStreetAddress01, LDJobStreetAddress" & _
        "02 = @LDJobStreetAddress02, LDJobSuburb = @LDJobSuburb, LDJobState = @LDJobState" & _
        ", LDJobPostCode = @LDJobPostCode, LDReferenceName = @LDReferenceName, LSID = @LS" & _
        "ID, LDHasAppointments = @LDHasAppointments, LDUser = @LDUser, LDJobDescription =" & _
        " @LDJobDescription, EXIDRep = @EXIDRep, LDQuoteReceived = @LDQuoteReceived, LDCl" & _
        "ientEmail = @LDClientEmail, LDIsCancelled = @LDIsCancelled, CTID = @CTID, LDUseC" & _
        "ontact = @LDUseContact WHERE (BRID = @Original_BRID) AND (LDID = @Original_LDID)" & _
        " AND (CTID = @Original_CTID OR @Original_CTID IS NULL AND CTID IS NULL) AND (EXI" & _
        "DRep = @Original_EXIDRep OR @Original_EXIDRep IS NULL AND EXIDRep IS NULL) AND (" & _
        "ID = @Original_ID) AND (LDClientEmail = @Original_LDClientEmail OR @Original_LDC" & _
        "lientEmail IS NULL AND LDClientEmail IS NULL) AND (LDClientFaxNumber = @Original" & _
        "_LDClientFaxNumber OR @Original_LDClientFaxNumber IS NULL AND LDClientFaxNumber " & _
        "IS NULL) AND (LDClientFirstName = @Original_LDClientFirstName OR @Original_LDCli" & _
        "entFirstName IS NULL AND LDClientFirstName IS NULL) AND (LDClientHomePhoneNumber" & _
        " = @Original_LDClientHomePhoneNumber OR @Original_LDClientHomePhoneNumber IS NUL" & _
        "L AND LDClientHomePhoneNumber IS NULL) AND (LDClientMobileNumber = @Original_LDC" & _
        "lientMobileNumber OR @Original_LDClientMobileNumber IS NULL AND LDClientMobileNu" & _
        "mber IS NULL) AND (LDClientPostCode = @Original_LDClientPostCode OR @Original_LD" & _
        "ClientPostCode IS NULL AND LDClientPostCode IS NULL) AND (LDClientState = @Origi" & _
        "nal_LDClientState OR @Original_LDClientState IS NULL AND LDClientState IS NULL) " & _
        "AND (LDClientStreetAddress01 = @Original_LDClientStreetAddress01 OR @Original_LD" & _
        "ClientStreetAddress01 IS NULL AND LDClientStreetAddress01 IS NULL) AND (LDClient" & _
        "StreetAddress02 = @Original_LDClientStreetAddress02 OR @Original_LDClientStreetA" & _
        "ddress02 IS NULL AND LDClientStreetAddress02 IS NULL) AND (LDClientSuburb = @Ori" & _
        "ginal_LDClientSuburb OR @Original_LDClientSuburb IS NULL AND LDClientSuburb IS N" & _
        "ULL) AND (LDClientSurname = @Original_LDClientSurname OR @Original_LDClientSurna" & _
        "me IS NULL AND LDClientSurname IS NULL) AND (LDClientWorkPhoneNumber = @Original" & _
        "_LDClientWorkPhoneNumber OR @Original_LDClientWorkPhoneNumber IS NULL AND LDClie" & _
        "ntWorkPhoneNumber IS NULL) AND (LDDate = @Original_LDDate OR @Original_LDDate IS" & _
        " NULL AND LDDate IS NULL) AND (LDHasAppointments = @Original_LDHasAppointments O" & _
        "R @Original_LDHasAppointments IS NULL AND LDHasAppointments IS NULL) AND (LDIsCa" & _
        "ncelled = @Original_LDIsCancelled OR @Original_LDIsCancelled IS NULL AND LDIsCan" & _
        "celled IS NULL) AND (LDJobAddressAsAbove = @Original_LDJobAddressAsAbove OR @Ori" & _
        "ginal_LDJobAddressAsAbove IS NULL AND LDJobAddressAsAbove IS NULL) AND (LDJobDes" & _
        "cription = @Original_LDJobDescription OR @Original_LDJobDescription IS NULL AND " & _
        "LDJobDescription IS NULL) AND (LDJobPostCode = @Original_LDJobPostCode OR @Origi" & _
        "nal_LDJobPostCode IS NULL AND LDJobPostCode IS NULL) AND (LDJobState = @Original" & _
        "_LDJobState OR @Original_LDJobState IS NULL AND LDJobState IS NULL) AND (LDJobSt" & _
        "reetAddress01 = @Original_LDJobStreetAddress01 OR @Original_LDJobStreetAddress01" & _
        " IS NULL AND LDJobStreetAddress01 IS NULL) AND (LDJobStreetAddress02 = @Original" & _
        "_LDJobStreetAddress02 OR @Original_LDJobStreetAddress02 IS NULL AND LDJobStreetA" & _
        "ddress02 IS NULL) AND (LDJobSuburb = @Original_LDJobSuburb OR @Original_LDJobSub" & _
        "urb IS NULL AND LDJobSuburb IS NULL) AND (LDQuoteReceived = @Original_LDQuoteRec" & _
        "eived OR @Original_LDQuoteReceived IS NULL AND LDQuoteReceived IS NULL) AND (LDR" & _
        "eferenceName = @Original_LDReferenceName OR @Original_LDReferenceName IS NULL AN" & _
        "D LDReferenceName IS NULL) AND (LDUseContact = @Original_LDUseContact OR @Origin" & _
        "al_LDUseContact IS NULL AND LDUseContact IS NULL) AND (LDUser = @Original_LDUser" & _
        " OR @Original_LDUser IS NULL AND LDUser IS NULL) AND (LSID = @Original_LSID OR @" & _
        "Original_LSID IS NULL AND LSID IS NULL); SELECT BRID, LDID, ID, LDDate, LDClient" & _
        "FirstName, LDClientSurname, LDClientName, LDClientStreetAddress01, LDClientStree" & _
        "tAddress02, LDClientSuburb, LDClientState, LDClientPostCode, LDClientHomePhoneNu" & _
        "mber, LDClientWorkPhoneNumber, LDClientFaxNumber, LDClientMobileNumber, LDJobAdd" & _
        "ressAsAbove, LDJobStreetAddress01, LDJobStreetAddress02, LDJobSuburb, LDJobState" & _
        ", LDJobPostCode, LDJobAddress, LDReferenceName, LSID, LDHasAppointments, LDUser," & _
        " LDJobDescription, EXIDRep, LDQuoteReceived, LDClientAddress, LDClientEmail, LDJ" & _
        "obAddressMultiline, LDIsCancelled, CTID, LDUseContact FROM Leads WHERE (BRID = @" & _
        "BRID) AND (LDID = @LDID)"
        Me.SqlUpdateCommand4.Connection = Me.SqlConnection
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDDate", System.Data.SqlDbType.DateTime, 8, "LDDate"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientFirstName", System.Data.SqlDbType.VarChar, 50, "LDClientFirstName"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientSurname", System.Data.SqlDbType.VarChar, 50, "LDClientSurname"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, "LDClientStreetAddress01"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, "LDClientStreetAddress02"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientSuburb", System.Data.SqlDbType.VarChar, 50, "LDClientSuburb"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientState", System.Data.SqlDbType.VarChar, 3, "LDClientState"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientPostCode", System.Data.SqlDbType.VarChar, 20, "LDClientPostCode"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, "LDClientHomePhoneNumber"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, "LDClientWorkPhoneNumber"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientFaxNumber", System.Data.SqlDbType.VarChar, 20, "LDClientFaxNumber"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientMobileNumber", System.Data.SqlDbType.VarChar, 20, "LDClientMobileNumber"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, "LDJobAddressAsAbove"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, "LDJobStreetAddress01"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, "LDJobStreetAddress02"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobSuburb", System.Data.SqlDbType.VarChar, 50, "LDJobSuburb"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobState", System.Data.SqlDbType.VarChar, 3, "LDJobState"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobPostCode", System.Data.SqlDbType.VarChar, 20, "LDJobPostCode"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDReferenceName", System.Data.SqlDbType.VarChar, 200, "LDReferenceName"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LSID", System.Data.SqlDbType.Int, 4, "LSID"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDHasAppointments", System.Data.SqlDbType.Bit, 1, "LDHasAppointments"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDUser", System.Data.SqlDbType.VarChar, 50, "LDUser"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobDescription", System.Data.SqlDbType.VarChar, 50, "LDJobDescription"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDRep", System.Data.SqlDbType.Int, 4, "EXIDRep"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDQuoteReceived", System.Data.SqlDbType.Bit, 1, "LDQuoteReceived"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientEmail", System.Data.SqlDbType.VarChar, 50, "LDClientEmail"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDIsCancelled", System.Data.SqlDbType.Bit, 1, "LDIsCancelled"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTID", System.Data.SqlDbType.BigInt, 8, "CTID"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDUseContact", System.Data.SqlDbType.Bit, 1, "LDUseContact"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDRep", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDRep", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientFirstName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientFirstName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientHomePhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientSurname", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientWorkPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDHasAppointments", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDHasAppointments", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDIsCancelled", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDIsCancelled", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobAddressAsAbove", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobDescription", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDQuoteReceived", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDQuoteReceived", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDReferenceName", System.Data.SqlDbType.VarChar, 200, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDReferenceName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDUseContact", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDUseContact", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDUser", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDUser", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LSID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDID", System.Data.SqlDbType.BigInt, 8, "LDID"))
        '
        'daINFollowUpTypes
        '
        Me.daINFollowUpTypes.DeleteCommand = Me.SqlCommand3
        Me.daINFollowUpTypes.InsertCommand = Me.SqlCommand4
        Me.daINFollowUpTypes.SelectCommand = Me.SqlCommand5
        Me.daINFollowUpTypes.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "INFollowUpTypes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("INFollowUpType", "INFollowUpType"), New System.Data.Common.DataColumnMapping("INFollowUpTypeDesc", "INFollowUpTypeDesc")})})
        Me.daINFollowUpTypes.UpdateCommand = Me.SqlCommand6
        '
        'SqlCommand3
        '
        Me.SqlCommand3.CommandText = "DELETE FROM INFollowUpTypes WHERE (INFollowUpType = @Original_INFollowUpType) AND" & _
        " (INFollowUpTypeDesc = @Original_INFollowUpTypeDesc OR @Original_INFollowUpTypeD" & _
        "esc IS NULL AND INFollowUpTypeDesc IS NULL)"
        Me.SqlCommand3.Connection = Me.SqlConnection
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpTypeDesc", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpTypeDesc", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlCommand4
        '
        Me.SqlCommand4.CommandText = "INSERT INTO INFollowUpTypes(INFollowUpType, INFollowUpTypeDesc) VALUES (@INFollow" & _
        "UpType, @INFollowUpTypeDesc); SELECT INFollowUpType, INFollowUpTypeDesc FROM INF" & _
        "ollowUpTypes WHERE (INFollowUpType = @INFollowUpType)"
        Me.SqlCommand4.Connection = Me.SqlConnection
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpType", System.Data.SqlDbType.VarChar, 2, "INFollowUpType"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpTypeDesc", System.Data.SqlDbType.VarChar, 50, "INFollowUpTypeDesc"))
        '
        'SqlCommand5
        '
        Me.SqlCommand5.CommandText = "SELECT INFollowUpType, INFollowUpTypeDesc FROM INFollowUpTypes WHERE (INFollowUpT" & _
        "ypeEnabled = 1)"
        Me.SqlCommand5.Connection = Me.SqlConnection
        '
        'SqlCommand6
        '
        Me.SqlCommand6.CommandText = "UPDATE INFollowUpTypes SET INFollowUpType = @INFollowUpType, INFollowUpTypeDesc =" & _
        " @INFollowUpTypeDesc WHERE (INFollowUpType = @Original_INFollowUpType) AND (INFo" & _
        "llowUpTypeDesc = @Original_INFollowUpTypeDesc OR @Original_INFollowUpTypeDesc IS" & _
        " NULL AND INFollowUpTypeDesc IS NULL); SELECT INFollowUpType, INFollowUpTypeDesc" & _
        " FROM INFollowUpTypes WHERE (INFollowUpType = @INFollowUpType)"
        Me.SqlCommand6.Connection = Me.SqlConnection
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpType", System.Data.SqlDbType.VarChar, 2, "INFollowUpType"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpTypeDesc", System.Data.SqlDbType.VarChar, 50, "INFollowUpTypeDesc"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpTypeDesc", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpTypeDesc", System.Data.DataRowVersion.Original, Nothing))
        '
        'daIDNotes
        '
        Me.daIDNotes.DeleteCommand = Me.SqlDeleteCommand6
        Me.daIDNotes.InsertCommand = Me.SqlInsertCommand6
        Me.daIDNotes.SelectCommand = Me.SqlSelectCommand6
        Me.daIDNotes.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "IDNotes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("INID", "INID"), New System.Data.Common.DataColumnMapping("ID", "ID"), New System.Data.Common.DataColumnMapping("INUser", "INUser"), New System.Data.Common.DataColumnMapping("INDate", "INDate"), New System.Data.Common.DataColumnMapping("INNotes", "INNotes"), New System.Data.Common.DataColumnMapping("INFollowUpType", "INFollowUpType"), New System.Data.Common.DataColumnMapping("INFollowUpDate", "INFollowUpDate"), New System.Data.Common.DataColumnMapping("INFollowUpComplete", "INFollowUpComplete"), New System.Data.Common.DataColumnMapping("EXIDFollowUp", "EXIDFollowUp"), New System.Data.Common.DataColumnMapping("INFollowUpText", "INFollowUpText")})})
        Me.daIDNotes.UpdateCommand = Me.SqlUpdateCommand6
        '
        'SqlDeleteCommand6
        '
        Me.SqlDeleteCommand6.CommandText = "DELETE FROM IDNotes WHERE (BRID = @Original_BRID) AND (INID = @Original_INID) AND" & _
        " (EXIDFollowUp = @Original_EXIDFollowUp OR @Original_EXIDFollowUp IS NULL AND EX" & _
        "IDFollowUp IS NULL) AND (ID = @Original_ID) AND (INDate = @Original_INDate OR @O" & _
        "riginal_INDate IS NULL AND INDate IS NULL) AND (INFollowUpComplete = @Original_I" & _
        "NFollowUpComplete OR @Original_INFollowUpComplete IS NULL AND INFollowUpComplete" & _
        " IS NULL) AND (INFollowUpDate = @Original_INFollowUpDate OR @Original_INFollowUp" & _
        "Date IS NULL AND INFollowUpDate IS NULL) AND (INFollowUpText = @Original_INFollo" & _
        "wUpText OR @Original_INFollowUpText IS NULL AND INFollowUpText IS NULL) AND (INF" & _
        "ollowUpType = @Original_INFollowUpType OR @Original_INFollowUpType IS NULL AND I" & _
        "NFollowUpType IS NULL) AND (INNotes = @Original_INNotes OR @Original_INNotes IS " & _
        "NULL AND INNotes IS NULL) AND (INUser = @Original_INUser OR @Original_INUser IS " & _
        "NULL AND INUser IS NULL)"
        Me.SqlDeleteCommand6.Connection = Me.SqlConnection
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDFollowUp", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDFollowUp", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpComplete", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpComplete", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpText", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpText", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INNotes", System.Data.SqlDbType.VarChar, 1000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INUser", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INUser", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand6
        '
        Me.SqlInsertCommand6.CommandText = "INSERT INTO IDNotes(BRID, ID, INUser, INDate, INNotes, INFollowUpType, INFollowUp" & _
        "Date, INFollowUpComplete, EXIDFollowUp, INFollowUpText) VALUES (@BRID, @ID, @INU" & _
        "ser, @INDate, @INNotes, @INFollowUpType, @INFollowUpDate, @INFollowUpComplete, @" & _
        "EXIDFollowUp, @INFollowUpText); SELECT BRID, INID, ID, INUser, INDate, INNotes, " & _
        "INFollowUpType, INFollowUpDate, INFollowUpComplete, EXIDFollowUp, INFollowUpText" & _
        " FROM IDNotes WHERE (BRID = @BRID) AND (INID = @@IDENTITY)"
        Me.SqlInsertCommand6.Connection = Me.SqlConnection
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INUser", System.Data.SqlDbType.VarChar, 50, "INUser"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INDate", System.Data.SqlDbType.DateTime, 8, "INDate"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INNotes", System.Data.SqlDbType.VarChar, 1000, "INNotes"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpType", System.Data.SqlDbType.VarChar, 2, "INFollowUpType"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpDate", System.Data.SqlDbType.DateTime, 8, "INFollowUpDate"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpComplete", System.Data.SqlDbType.Bit, 1, "INFollowUpComplete"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDFollowUp", System.Data.SqlDbType.Int, 4, "EXIDFollowUp"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpText", System.Data.SqlDbType.VarChar, 100, "INFollowUpText"))
        '
        'SqlSelectCommand6
        '
        Me.SqlSelectCommand6.CommandText = "SELECT BRID, INID, ID, INUser, INDate, INNotes, INFollowUpType, INFollowUpDate, I" & _
        "NFollowUpComplete, EXIDFollowUp, INFollowUpText FROM IDNotes WHERE (BRID = @BRID" & _
        ") AND (ID = @ID)"
        Me.SqlSelectCommand6.Connection = Me.SqlConnection
        Me.SqlSelectCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        '
        'SqlUpdateCommand6
        '
        Me.SqlUpdateCommand6.CommandText = "UPDATE IDNotes SET BRID = @BRID, ID = @ID, INUser = @INUser, INDate = @INDate, IN" & _
        "Notes = @INNotes, INFollowUpType = @INFollowUpType, INFollowUpDate = @INFollowUp" & _
        "Date, INFollowUpComplete = @INFollowUpComplete, EXIDFollowUp = @EXIDFollowUp, IN" & _
        "FollowUpText = @INFollowUpText WHERE (BRID = @Original_BRID) AND (INID = @Origin" & _
        "al_INID) AND (EXIDFollowUp = @Original_EXIDFollowUp OR @Original_EXIDFollowUp IS" & _
        " NULL AND EXIDFollowUp IS NULL) AND (ID = @Original_ID) AND (INDate = @Original_" & _
        "INDate OR @Original_INDate IS NULL AND INDate IS NULL) AND (INFollowUpComplete =" & _
        " @Original_INFollowUpComplete OR @Original_INFollowUpComplete IS NULL AND INFoll" & _
        "owUpComplete IS NULL) AND (INFollowUpDate = @Original_INFollowUpDate OR @Origina" & _
        "l_INFollowUpDate IS NULL AND INFollowUpDate IS NULL) AND (INFollowUpText = @Orig" & _
        "inal_INFollowUpText OR @Original_INFollowUpText IS NULL AND INFollowUpText IS NU" & _
        "LL) AND (INFollowUpType = @Original_INFollowUpType OR @Original_INFollowUpType I" & _
        "S NULL AND INFollowUpType IS NULL) AND (INNotes = @Original_INNotes OR @Original" & _
        "_INNotes IS NULL AND INNotes IS NULL) AND (INUser = @Original_INUser OR @Origina" & _
        "l_INUser IS NULL AND INUser IS NULL); SELECT BRID, INID, ID, INUser, INDate, INN" & _
        "otes, INFollowUpType, INFollowUpDate, INFollowUpComplete, EXIDFollowUp, INFollow" & _
        "UpText FROM IDNotes WHERE (BRID = @BRID) AND (INID = @INID)"
        Me.SqlUpdateCommand6.Connection = Me.SqlConnection
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INUser", System.Data.SqlDbType.VarChar, 50, "INUser"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INDate", System.Data.SqlDbType.DateTime, 8, "INDate"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INNotes", System.Data.SqlDbType.VarChar, 1000, "INNotes"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpType", System.Data.SqlDbType.VarChar, 2, "INFollowUpType"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpDate", System.Data.SqlDbType.DateTime, 8, "INFollowUpDate"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpComplete", System.Data.SqlDbType.Bit, 1, "INFollowUpComplete"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDFollowUp", System.Data.SqlDbType.Int, 4, "EXIDFollowUp"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpText", System.Data.SqlDbType.VarChar, 100, "INFollowUpText"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDFollowUp", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDFollowUp", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpComplete", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpComplete", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpText", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpText", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INNotes", System.Data.SqlDbType.VarChar, 1000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INUser", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INUser", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INID", System.Data.SqlDbType.BigInt, 8, "INID"))
        '
        'daContacts
        '
        Me.daContacts.DeleteCommand = Me.SqlDeleteCommand3
        Me.daContacts.InsertCommand = Me.SqlInsertCommand7
        Me.daContacts.SelectCommand = Me.SqlSelectCommand7
        Me.daContacts.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Contacts", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("CTID", "CTID"), New System.Data.Common.DataColumnMapping("CTSurname", "CTSurname"), New System.Data.Common.DataColumnMapping("CTFirstName", "CTFirstName"), New System.Data.Common.DataColumnMapping("CTStreetAddress01", "CTStreetAddress01"), New System.Data.Common.DataColumnMapping("CTStreetAddress02", "CTStreetAddress02"), New System.Data.Common.DataColumnMapping("CTSuburb", "CTSuburb"), New System.Data.Common.DataColumnMapping("CTState", "CTState"), New System.Data.Common.DataColumnMapping("CTPostCode", "CTPostCode"), New System.Data.Common.DataColumnMapping("CTHomePhoneNumber", "CTHomePhoneNumber"), New System.Data.Common.DataColumnMapping("CTWorkPhoneNumber", "CTWorkPhoneNumber"), New System.Data.Common.DataColumnMapping("CTMobileNumber", "CTMobileNumber"), New System.Data.Common.DataColumnMapping("CTFaxNumber", "CTFaxNumber"), New System.Data.Common.DataColumnMapping("CTEmail", "CTEmail"), New System.Data.Common.DataColumnMapping("CTReferenceName", "CTReferenceName"), New System.Data.Common.DataColumnMapping("CTName", "CTName"), New System.Data.Common.DataColumnMapping("CTAddress", "CTAddress"), New System.Data.Common.DataColumnMapping("CTAddressMultiline", "CTAddressMultiline")})})
        Me.daContacts.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM Contacts WHERE (BRID = @Original_BRID) AND (CTID = @Original_CTID) AN" & _
        "D (CTAddress = @Original_CTAddress OR @Original_CTAddress IS NULL AND CTAddress " & _
        "IS NULL) AND (CTAddressMultiline = @Original_CTAddressMultiline OR @Original_CTA" & _
        "ddressMultiline IS NULL AND CTAddressMultiline IS NULL) AND (CTEmail = @Original" & _
        "_CTEmail OR @Original_CTEmail IS NULL AND CTEmail IS NULL) AND (CTFaxNumber = @O" & _
        "riginal_CTFaxNumber OR @Original_CTFaxNumber IS NULL AND CTFaxNumber IS NULL) AN" & _
        "D (CTFirstName = @Original_CTFirstName OR @Original_CTFirstName IS NULL AND CTFi" & _
        "rstName IS NULL) AND (CTHomePhoneNumber = @Original_CTHomePhoneNumber OR @Origin" & _
        "al_CTHomePhoneNumber IS NULL AND CTHomePhoneNumber IS NULL) AND (CTMobileNumber " & _
        "= @Original_CTMobileNumber OR @Original_CTMobileNumber IS NULL AND CTMobileNumbe" & _
        "r IS NULL) AND (CTName = @Original_CTName OR @Original_CTName IS NULL AND CTName" & _
        " IS NULL) AND (CTPostCode = @Original_CTPostCode OR @Original_CTPostCode IS NULL" & _
        " AND CTPostCode IS NULL) AND (CTReferenceName = @Original_CTReferenceName OR @Or" & _
        "iginal_CTReferenceName IS NULL AND CTReferenceName IS NULL) AND (CTState = @Orig" & _
        "inal_CTState OR @Original_CTState IS NULL AND CTState IS NULL) AND (CTStreetAddr" & _
        "ess01 = @Original_CTStreetAddress01 OR @Original_CTStreetAddress01 IS NULL AND C" & _
        "TStreetAddress01 IS NULL) AND (CTStreetAddress02 = @Original_CTStreetAddress02 O" & _
        "R @Original_CTStreetAddress02 IS NULL AND CTStreetAddress02 IS NULL) AND (CTSubu" & _
        "rb = @Original_CTSuburb OR @Original_CTSuburb IS NULL AND CTSuburb IS NULL) AND " & _
        "(CTSurname = @Original_CTSurname OR @Original_CTSurname IS NULL AND CTSurname IS" & _
        " NULL) AND (CTWorkPhoneNumber = @Original_CTWorkPhoneNumber OR @Original_CTWorkP" & _
        "honeNumber IS NULL AND CTWorkPhoneNumber IS NULL)"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTAddressMultiline", System.Data.SqlDbType.VarChar, 261, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTAddressMultiline", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTFirstName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTFirstName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTHomePhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTName", System.Data.SqlDbType.VarChar, 102, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTReferenceName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTReferenceName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTSurname", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTWorkPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand7
        '
        Me.SqlInsertCommand7.CommandText = "INSERT INTO Contacts(BRID, CTSurname, CTFirstName, CTStreetAddress01, CTStreetAdd" & _
        "ress02, CTSuburb, CTState, CTPostCode, CTHomePhoneNumber, CTWorkPhoneNumber, CTM" & _
        "obileNumber, CTFaxNumber, CTEmail, CTReferenceName, CTName, CTAddress, CTAddress" & _
        "Multiline) VALUES (@BRID, @CTSurname, @CTFirstName, @CTStreetAddress01, @CTStree" & _
        "tAddress02, @CTSuburb, @CTState, @CTPostCode, @CTHomePhoneNumber, @CTWorkPhoneNu" & _
        "mber, @CTMobileNumber, @CTFaxNumber, @CTEmail, @CTReferenceName, @CTName, @CTAdd" & _
        "ress, @CTAddressMultiline); SELECT BRID, CTID, CTSurname, CTFirstName, CTStreetA" & _
        "ddress01, CTStreetAddress02, CTSuburb, CTState, CTPostCode, CTHomePhoneNumber, C" & _
        "TWorkPhoneNumber, CTMobileNumber, CTFaxNumber, CTEmail, CTReferenceName, CTName," & _
        " CTAddress, CTAddressMultiline FROM Contacts WHERE (BRID = @BRID) AND (CTID = @@" & _
        "IDENTITY)"
        Me.SqlInsertCommand7.Connection = Me.SqlConnection
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTSurname", System.Data.SqlDbType.VarChar, 50, "CTSurname"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTFirstName", System.Data.SqlDbType.VarChar, 50, "CTFirstName"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTStreetAddress01", System.Data.SqlDbType.VarChar, 100, "CTStreetAddress01"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTStreetAddress02", System.Data.SqlDbType.VarChar, 100, "CTStreetAddress02"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTSuburb", System.Data.SqlDbType.VarChar, 50, "CTSuburb"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTState", System.Data.SqlDbType.VarChar, 3, "CTState"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTPostCode", System.Data.SqlDbType.VarChar, 20, "CTPostCode"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, "CTHomePhoneNumber"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, "CTWorkPhoneNumber"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTMobileNumber", System.Data.SqlDbType.VarChar, 20, "CTMobileNumber"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTFaxNumber", System.Data.SqlDbType.VarChar, 20, "CTFaxNumber"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTEmail", System.Data.SqlDbType.VarChar, 50, "CTEmail"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTReferenceName", System.Data.SqlDbType.VarChar, 100, "CTReferenceName"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTName", System.Data.SqlDbType.VarChar, 102, "CTName"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTAddress", System.Data.SqlDbType.VarChar, 259, "CTAddress"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTAddressMultiline", System.Data.SqlDbType.VarChar, 261, "CTAddressMultiline"))
        '
        'SqlSelectCommand7
        '
        Me.SqlSelectCommand7.CommandText = "SELECT BRID, CTID, CTSurname, CTFirstName, CTStreetAddress01, CTStreetAddress02, " & _
        "CTSuburb, CTState, CTPostCode, CTHomePhoneNumber, CTWorkPhoneNumber, CTMobileNum" & _
        "ber, CTFaxNumber, CTEmail, CTReferenceName, CTName, CTAddress, CTAddressMultilin" & _
        "e FROM Contacts WHERE (BRID = @BRID) AND (CTID = @CTID)"
        Me.SqlSelectCommand7.Connection = Me.SqlConnection
        Me.SqlSelectCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTID", System.Data.SqlDbType.BigInt, 8, "CTID"))
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE Contacts SET BRID = @BRID, CTSurname = @CTSurname, CTFirstName = @CTFirstN" & _
        "ame, CTStreetAddress01 = @CTStreetAddress01, CTStreetAddress02 = @CTStreetAddres" & _
        "s02, CTSuburb = @CTSuburb, CTState = @CTState, CTPostCode = @CTPostCode, CTHomeP" & _
        "honeNumber = @CTHomePhoneNumber, CTWorkPhoneNumber = @CTWorkPhoneNumber, CTMobil" & _
        "eNumber = @CTMobileNumber, CTFaxNumber = @CTFaxNumber, CTEmail = @CTEmail, CTRef" & _
        "erenceName = @CTReferenceName, CTName = @CTName, CTAddress = @CTAddress, CTAddre" & _
        "ssMultiline = @CTAddressMultiline WHERE (BRID = @Original_BRID) AND (CTID = @Ori" & _
        "ginal_CTID) AND (CTAddress = @Original_CTAddress OR @Original_CTAddress IS NULL " & _
        "AND CTAddress IS NULL) AND (CTAddressMultiline = @Original_CTAddressMultiline OR" & _
        " @Original_CTAddressMultiline IS NULL AND CTAddressMultiline IS NULL) AND (CTEma" & _
        "il = @Original_CTEmail OR @Original_CTEmail IS NULL AND CTEmail IS NULL) AND (CT" & _
        "FaxNumber = @Original_CTFaxNumber OR @Original_CTFaxNumber IS NULL AND CTFaxNumb" & _
        "er IS NULL) AND (CTFirstName = @Original_CTFirstName OR @Original_CTFirstName IS" & _
        " NULL AND CTFirstName IS NULL) AND (CTHomePhoneNumber = @Original_CTHomePhoneNum" & _
        "ber OR @Original_CTHomePhoneNumber IS NULL AND CTHomePhoneNumber IS NULL) AND (C" & _
        "TMobileNumber = @Original_CTMobileNumber OR @Original_CTMobileNumber IS NULL AND" & _
        " CTMobileNumber IS NULL) AND (CTName = @Original_CTName OR @Original_CTName IS N" & _
        "ULL AND CTName IS NULL) AND (CTPostCode = @Original_CTPostCode OR @Original_CTPo" & _
        "stCode IS NULL AND CTPostCode IS NULL) AND (CTReferenceName = @Original_CTRefere" & _
        "nceName OR @Original_CTReferenceName IS NULL AND CTReferenceName IS NULL) AND (C" & _
        "TState = @Original_CTState OR @Original_CTState IS NULL AND CTState IS NULL) AND" & _
        " (CTStreetAddress01 = @Original_CTStreetAddress01 OR @Original_CTStreetAddress01" & _
        " IS NULL AND CTStreetAddress01 IS NULL) AND (CTStreetAddress02 = @Original_CTStr" & _
        "eetAddress02 OR @Original_CTStreetAddress02 IS NULL AND CTStreetAddress02 IS NUL" & _
        "L) AND (CTSuburb = @Original_CTSuburb OR @Original_CTSuburb IS NULL AND CTSuburb" & _
        " IS NULL) AND (CTSurname = @Original_CTSurname OR @Original_CTSurname IS NULL AN" & _
        "D CTSurname IS NULL) AND (CTWorkPhoneNumber = @Original_CTWorkPhoneNumber OR @Or" & _
        "iginal_CTWorkPhoneNumber IS NULL AND CTWorkPhoneNumber IS NULL); SELECT BRID, CT" & _
        "ID, CTSurname, CTFirstName, CTStreetAddress01, CTStreetAddress02, CTSuburb, CTSt" & _
        "ate, CTPostCode, CTHomePhoneNumber, CTWorkPhoneNumber, CTMobileNumber, CTFaxNumb" & _
        "er, CTEmail, CTReferenceName, CTName, CTAddress, CTAddressMultiline FROM Contact" & _
        "s WHERE (BRID = @BRID) AND (CTID = @CTID)"
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTSurname", System.Data.SqlDbType.VarChar, 50, "CTSurname"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTFirstName", System.Data.SqlDbType.VarChar, 50, "CTFirstName"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTStreetAddress01", System.Data.SqlDbType.VarChar, 100, "CTStreetAddress01"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTStreetAddress02", System.Data.SqlDbType.VarChar, 100, "CTStreetAddress02"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTSuburb", System.Data.SqlDbType.VarChar, 50, "CTSuburb"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTState", System.Data.SqlDbType.VarChar, 3, "CTState"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTPostCode", System.Data.SqlDbType.VarChar, 20, "CTPostCode"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, "CTHomePhoneNumber"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, "CTWorkPhoneNumber"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTMobileNumber", System.Data.SqlDbType.VarChar, 20, "CTMobileNumber"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTFaxNumber", System.Data.SqlDbType.VarChar, 20, "CTFaxNumber"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTEmail", System.Data.SqlDbType.VarChar, 50, "CTEmail"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTReferenceName", System.Data.SqlDbType.VarChar, 100, "CTReferenceName"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTName", System.Data.SqlDbType.VarChar, 102, "CTName"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTAddress", System.Data.SqlDbType.VarChar, 259, "CTAddress"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTAddressMultiline", System.Data.SqlDbType.VarChar, 261, "CTAddressMultiline"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTAddressMultiline", System.Data.SqlDbType.VarChar, 261, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTAddressMultiline", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTFirstName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTFirstName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTHomePhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTName", System.Data.SqlDbType.VarChar, 102, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTReferenceName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTReferenceName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTSurname", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTWorkPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTID", System.Data.SqlDbType.BigInt, 8, "CTID"))
        '
        'daOrders_Materials_SubItems
        '
        Me.daOrders_Materials_SubItems.ContinueUpdateOnError = True
        Me.daOrders_Materials_SubItems.DeleteCommand = Me.SqlDeleteCommand10
        Me.daOrders_Materials_SubItems.InsertCommand = Me.SqlInsertCommand8
        Me.daOrders_Materials_SubItems.SelectCommand = Me.SqlSelectCommand8
        Me.daOrders_Materials_SubItems.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Orders_Materials_SubItems", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("OMSID", "OMSID"), New System.Data.Common.DataColumnMapping("OMID", "OMID"), New System.Data.Common.DataColumnMapping("OMSBrand", "OMSBrand"), New System.Data.Common.DataColumnMapping("OMSModelNumber", "OMSModelNumber"), New System.Data.Common.DataColumnMapping("OMSDescription", "OMSDescription"), New System.Data.Common.DataColumnMapping("OMSQty", "OMSQty"), New System.Data.Common.DataColumnMapping("OMSQuotedCost", "OMSQuotedCost")})})
        Me.daOrders_Materials_SubItems.UpdateCommand = Me.SqlUpdateCommand10
        '
        'SqlDeleteCommand10
        '
        Me.SqlDeleteCommand10.CommandText = "DELETE FROM Orders_Materials_SubItems WHERE (BRID = @Original_BRID) AND (OMSID = " & _
        "@Original_OMSID) AND (OMID = @Original_OMID) AND (OMSBrand = @Original_OMSBrand " & _
        "OR @Original_OMSBrand IS NULL AND OMSBrand IS NULL) AND (OMSDescription = @Origi" & _
        "nal_OMSDescription OR @Original_OMSDescription IS NULL AND OMSDescription IS NUL" & _
        "L) AND (OMSModelNumber = @Original_OMSModelNumber OR @Original_OMSModelNumber IS" & _
        " NULL AND OMSModelNumber IS NULL) AND (OMSQty = @Original_OMSQty OR @Original_OM" & _
        "SQty IS NULL AND OMSQty IS NULL) AND (OMSQuotedCost = @Original_OMSQuotedCost OR" & _
        " @Original_OMSQuotedCost IS NULL AND OMSQuotedCost IS NULL)"
        Me.SqlDeleteCommand10.Connection = Me.SqlConnection
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSBrand", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSBrand", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSDescription", System.Data.SqlDbType.VarChar, 500, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSModelNumber", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSModelNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSQty", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSQty", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSQuotedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSQuotedCost", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand8
        '
        Me.SqlInsertCommand8.CommandText = "INSERT INTO Orders_Materials_SubItems(BRID, OMID, OMSBrand, OMSModelNumber, OMSDe" & _
        "scription, OMSQty, OMSQuotedCost) VALUES (@BRID, @OMID, @OMSBrand, @OMSModelNumb" & _
        "er, @OMSDescription, @OMSQty, @OMSQuotedCost); SELECT BRID, OMSID, OMID, OMSBran" & _
        "d, OMSModelNumber, OMSDescription, OMSQty, OMSQuotedCost FROM Orders_Materials_S" & _
        "ubItems WHERE (BRID = @BRID) AND (OMSID = @@IDENTITY)"
        Me.SqlInsertCommand8.Connection = Me.SqlConnection
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMID", System.Data.SqlDbType.BigInt, 8, "OMID"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSBrand", System.Data.SqlDbType.VarChar, 50, "OMSBrand"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSModelNumber", System.Data.SqlDbType.VarChar, 50, "OMSModelNumber"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSDescription", System.Data.SqlDbType.VarChar, 500, "OMSDescription"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSQty", System.Data.SqlDbType.Int, 4, "OMSQty"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSQuotedCost", System.Data.SqlDbType.Money, 8, "OMSQuotedCost"))
        '
        'SqlSelectCommand8
        '
        Me.SqlSelectCommand8.CommandText = "SELECT BRID, OMSID, OMID, OMSBrand, OMSModelNumber, OMSDescription, OMSQty, OMSQu" & _
        "otedCost FROM VOrders_Materials_SubItems WHERE (BRID = @BRID) AND (JBID = @JBID)" & _
        ""
        Me.SqlSelectCommand8.Connection = Me.SqlConnection
        Me.SqlSelectCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlUpdateCommand10
        '
        Me.SqlUpdateCommand10.CommandText = "UPDATE Orders_Materials_SubItems SET BRID = @BRID, OMID = @OMID, OMSBrand = @OMSB" & _
        "rand, OMSModelNumber = @OMSModelNumber, OMSDescription = @OMSDescription, OMSQty" & _
        " = @OMSQty, OMSQuotedCost = @OMSQuotedCost WHERE (BRID = @Original_BRID) AND (OM" & _
        "SID = @Original_OMSID) AND (OMID = @Original_OMID) AND (OMSBrand = @Original_OMS" & _
        "Brand OR @Original_OMSBrand IS NULL AND OMSBrand IS NULL) AND (OMSDescription = " & _
        "@Original_OMSDescription OR @Original_OMSDescription IS NULL AND OMSDescription " & _
        "IS NULL) AND (OMSModelNumber = @Original_OMSModelNumber OR @Original_OMSModelNum" & _
        "ber IS NULL AND OMSModelNumber IS NULL) AND (OMSQty = @Original_OMSQty OR @Origi" & _
        "nal_OMSQty IS NULL AND OMSQty IS NULL) AND (OMSQuotedCost = @Original_OMSQuotedC" & _
        "ost OR @Original_OMSQuotedCost IS NULL AND OMSQuotedCost IS NULL); SELECT BRID, " & _
        "OMSID, OMID, OMSBrand, OMSModelNumber, OMSDescription, OMSQty, OMSQuotedCost FRO" & _
        "M Orders_Materials_SubItems WHERE (BRID = @BRID) AND (OMSID = @OMSID)"
        Me.SqlUpdateCommand10.Connection = Me.SqlConnection
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMID", System.Data.SqlDbType.BigInt, 8, "OMID"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSBrand", System.Data.SqlDbType.VarChar, 50, "OMSBrand"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSModelNumber", System.Data.SqlDbType.VarChar, 50, "OMSModelNumber"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSDescription", System.Data.SqlDbType.VarChar, 500, "OMSDescription"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSQty", System.Data.SqlDbType.Int, 4, "OMSQty"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSQuotedCost", System.Data.SqlDbType.Money, 8, "OMSQuotedCost"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSBrand", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSBrand", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSDescription", System.Data.SqlDbType.VarChar, 500, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSModelNumber", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSModelNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSQty", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSQty", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSQuotedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSQuotedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSID", System.Data.SqlDbType.BigInt, 8, "OMSID"))
        '
        'daOrders
        '
        Me.daOrders.ContinueUpdateOnError = True
        Me.daOrders.DeleteCommand = Me.SqlCommand7
        Me.daOrders.InsertCommand = Me.SqlCommand8
        Me.daOrders.SelectCommand = Me.SqlCommand9
        Me.daOrders.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VOrders", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("ORID", "ORID"), New System.Data.Common.DataColumnMapping("JBID", "JBID"), New System.Data.Common.DataColumnMapping("ORType", "ORType"), New System.Data.Common.DataColumnMapping("OROrderNumber", "OROrderNumber"), New System.Data.Common.DataColumnMapping("ORDate", "ORDate"), New System.Data.Common.DataColumnMapping("CTIDSupplier", "CTIDSupplier"), New System.Data.Common.DataColumnMapping("ORShipToJobAddress", "ORShipToJobAddress"), New System.Data.Common.DataColumnMapping("CTIDShipTo", "CTIDShipTo"), New System.Data.Common.DataColumnMapping("ORSupplierInvoiceNumber", "ORSupplierInvoiceNumber"), New System.Data.Common.DataColumnMapping("EXIDOrderedBy", "EXIDOrderedBy"), New System.Data.Common.DataColumnMapping("ORQuotedCost", "ORQuotedCost"), New System.Data.Common.DataColumnMapping("ORInvoicedCost", "ORInvoicedCost"), New System.Data.Common.DataColumnMapping("ORDescription", "ORDescription"), New System.Data.Common.DataColumnMapping("ORDeliveryDate", "ORDeliveryDate"), New System.Data.Common.DataColumnMapping("ORNotes", "ORNotes"), New System.Data.Common.DataColumnMapping("ORReceived", "ORReceived")})})
        Me.daOrders.UpdateCommand = Me.SqlCommand10
        '
        'SqlCommand7
        '
        Me.SqlCommand7.CommandText = "DELETE FROM Orders WHERE (BRID = @Original_BRID) AND (ORID = @Original_ORID) AND " & _
        "(CTIDShipTo = @Original_CTIDShipTo OR @Original_CTIDShipTo IS NULL AND CTIDShipT" & _
        "o IS NULL) AND (CTIDSupplier = @Original_CTIDSupplier OR @Original_CTIDSupplier " & _
        "IS NULL AND CTIDSupplier IS NULL) AND (EXIDOrderedBy = @Original_EXIDOrderedBy O" & _
        "R @Original_EXIDOrderedBy IS NULL AND EXIDOrderedBy IS NULL) AND (JBID = @Origin" & _
        "al_JBID OR @Original_JBID IS NULL AND JBID IS NULL) AND (ORDate = @Original_ORDa" & _
        "te OR @Original_ORDate IS NULL AND ORDate IS NULL) AND (ORDeliveryDate = @Origin" & _
        "al_ORDeliveryDate OR @Original_ORDeliveryDate IS NULL AND ORDeliveryDate IS NULL" & _
        ") AND (ORDescription = @Original_ORDescription OR @Original_ORDescription IS NUL" & _
        "L AND ORDescription IS NULL) AND (ORInvoicedCost = @Original_ORInvoicedCost OR @" & _
        "Original_ORInvoicedCost IS NULL AND ORInvoicedCost IS NULL) AND (ORNotes = @Orig" & _
        "inal_ORNotes OR @Original_ORNotes IS NULL AND ORNotes IS NULL) AND (OROrderNumbe" & _
        "r = @Original_OROrderNumber OR @Original_OROrderNumber IS NULL AND OROrderNumber" & _
        " IS NULL) AND (ORQuotedCost = @Original_ORQuotedCost OR @Original_ORQuotedCost I" & _
        "S NULL AND ORQuotedCost IS NULL) AND (ORReceived = @Original_ORReceived OR @Orig" & _
        "inal_ORReceived IS NULL AND ORReceived IS NULL) AND (ORShipToJobAddress = @Origi" & _
        "nal_ORShipToJobAddress OR @Original_ORShipToJobAddress IS NULL AND ORShipToJobAd" & _
        "dress IS NULL) AND (ORSupplierInvoiceNumber = @Original_ORSupplierInvoiceNumber " & _
        "OR @Original_ORSupplierInvoiceNumber IS NULL AND ORSupplierInvoiceNumber IS NULL" & _
        ") AND (ORType = @Original_ORType OR @Original_ORType IS NULL AND ORType IS NULL)" & _
        ""
        Me.SqlCommand7.Connection = Me.SqlConnection
        Me.SqlCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTIDShipTo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTIDShipTo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTIDSupplier", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTIDSupplier", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDOrderedBy", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDOrderedBy", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORDeliveryDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORDeliveryDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORDescription", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORInvoicedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORInvoicedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORNotes", System.Data.SqlDbType.VarChar, 500, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OROrderNumber", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OROrderNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORQuotedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORQuotedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORReceived", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORReceived", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORShipToJobAddress", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORShipToJobAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORSupplierInvoiceNumber", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORSupplierInvoiceNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORType", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORType", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlCommand8
        '
        Me.SqlCommand8.CommandText = "INSERT INTO Orders (BRID, JBID, ORType, OROrderNumber, ORDate, CTIDSupplier, ORSh" & _
        "ipToJobAddress, CTIDShipTo, ORSupplierInvoiceNumber, EXIDOrderedBy, ORQuotedCost" & _
        ", ORInvoicedCost, ORDescription, ORDeliveryDate, ORNotes, ORReceived) VALUES (@B" & _
        "RID, @JBID, @ORType, @OROrderNumber, @ORDate, @CTIDSupplier, @ORShipToJobAddress" & _
        ", @CTIDShipTo, @ORSupplierInvoiceNumber, @EXIDOrderedBy, @ORQuotedCost, @ORInvoi" & _
        "cedCost, @ORDescription, @ORDeliveryDate, @ORNotes, @ORReceived); SELECT BRID, O" & _
        "RID, JBID, ORType, OROrderNumber, ORDate, CTIDSupplier, ORShipToJobAddress, CTID" & _
        "ShipTo, ORSupplierInvoiceNumber, EXIDOrderedBy, ORQuotedCost, ORInvoicedCost, OR" & _
        "Description, ORDeliveryDate, ORNotes, ORReceived, ORIsComplete, ORInvoicedCostTo" & _
        "tal, ORStatus, ID FROM VOrders WHERE (BRID = @BRID) AND (ORID = @@IDENTITY)"
        Me.SqlCommand8.Connection = Me.SqlConnection
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORType", System.Data.SqlDbType.VarChar, 3, "ORType"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OROrderNumber", System.Data.SqlDbType.BigInt, 8, "OROrderNumber"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORDate", System.Data.SqlDbType.DateTime, 8, "ORDate"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTIDSupplier", System.Data.SqlDbType.BigInt, 8, "CTIDSupplier"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORShipToJobAddress", System.Data.SqlDbType.Bit, 1, "ORShipToJobAddress"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTIDShipTo", System.Data.SqlDbType.BigInt, 8, "CTIDShipTo"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORSupplierInvoiceNumber", System.Data.SqlDbType.VarChar, 50, "ORSupplierInvoiceNumber"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDOrderedBy", System.Data.SqlDbType.Int, 4, "EXIDOrderedBy"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORQuotedCost", System.Data.SqlDbType.Money, 8, "ORQuotedCost"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORInvoicedCost", System.Data.SqlDbType.Money, 8, "ORInvoicedCost"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORDescription", System.Data.SqlDbType.VarChar, 50, "ORDescription"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORDeliveryDate", System.Data.SqlDbType.DateTime, 8, "ORDeliveryDate"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORNotes", System.Data.SqlDbType.VarChar, 500, "ORNotes"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORReceived", System.Data.SqlDbType.Bit, 1, "ORReceived"))
        '
        'SqlCommand9
        '
        Me.SqlCommand9.CommandText = "SELECT BRID, ORID, JBID, ORType, OROrderNumber, ORDate, CTIDSupplier, ORShipToJob" & _
        "Address, CTIDShipTo, ORSupplierInvoiceNumber, EXIDOrderedBy, ORQuotedCost, ORInv" & _
        "oicedCost, ORDescription, ORDeliveryDate, ORNotes, ORReceived, ORIsComplete, ORI" & _
        "nvoicedCostTotal, ORStatus, ID FROM VOrders WHERE (BRID = @BRID) AND (JBID = @JB" & _
        "ID)"
        Me.SqlCommand9.Connection = Me.SqlConnection
        Me.SqlCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlCommand10
        '
        Me.SqlCommand10.CommandText = "UPDATE Orders SET BRID = @BRID, JBID = @JBID, ORType = @ORType, OROrderNumber = @" & _
        "OROrderNumber, ORDate = @ORDate, CTIDSupplier = @CTIDSupplier, ORShipToJobAddres" & _
        "s = @ORShipToJobAddress, CTIDShipTo = @CTIDShipTo, ORSupplierInvoiceNumber = @OR" & _
        "SupplierInvoiceNumber, EXIDOrderedBy = @EXIDOrderedBy, ORQuotedCost = @ORQuotedC" & _
        "ost, ORInvoicedCost = @ORInvoicedCost, ORDescription = @ORDescription, ORDeliver" & _
        "yDate = @ORDeliveryDate, ORNotes = @ORNotes, ORReceived = @ORReceived WHERE (BRI" & _
        "D = @Original_BRID) AND (ORID = @Original_ORID) AND (CTIDShipTo = @Original_CTID" & _
        "ShipTo OR @Original_CTIDShipTo IS NULL AND CTIDShipTo IS NULL) AND (CTIDSupplier" & _
        " = @Original_CTIDSupplier OR @Original_CTIDSupplier IS NULL AND CTIDSupplier IS " & _
        "NULL) AND (EXIDOrderedBy = @Original_EXIDOrderedBy OR @Original_EXIDOrderedBy IS" & _
        " NULL AND EXIDOrderedBy IS NULL) AND (JBID = @Original_JBID OR @Original_JBID IS" & _
        " NULL AND JBID IS NULL) AND (ORDate = @Original_ORDate OR @Original_ORDate IS NU" & _
        "LL AND ORDate IS NULL) AND (ORDeliveryDate = @Original_ORDeliveryDate OR @Origin" & _
        "al_ORDeliveryDate IS NULL AND ORDeliveryDate IS NULL) AND (ORDescription = @Orig" & _
        "inal_ORDescription OR @Original_ORDescription IS NULL AND ORDescription IS NULL)" & _
        " AND (ORInvoicedCost = @Original_ORInvoicedCost OR @Original_ORInvoicedCost IS N" & _
        "ULL AND ORInvoicedCost IS NULL) AND (ORNotes = @Original_ORNotes OR @Original_OR" & _
        "Notes IS NULL AND ORNotes IS NULL) AND (OROrderNumber = @Original_OROrderNumber " & _
        "OR @Original_OROrderNumber IS NULL AND OROrderNumber IS NULL) AND (ORQuotedCost " & _
        "= @Original_ORQuotedCost OR @Original_ORQuotedCost IS NULL AND ORQuotedCost IS N" & _
        "ULL) AND (ORReceived = @Original_ORReceived OR @Original_ORReceived IS NULL AND " & _
        "ORReceived IS NULL) AND (ORShipToJobAddress = @Original_ORShipToJobAddress OR @O" & _
        "riginal_ORShipToJobAddress IS NULL AND ORShipToJobAddress IS NULL) AND (ORSuppli" & _
        "erInvoiceNumber = @Original_ORSupplierInvoiceNumber OR @Original_ORSupplierInvoi" & _
        "ceNumber IS NULL AND ORSupplierInvoiceNumber IS NULL) AND (ORType = @Original_OR" & _
        "Type OR @Original_ORType IS NULL AND ORType IS NULL); SELECT BRID, ORID, JBID, O" & _
        "RType, OROrderNumber, ORDate, CTIDSupplier, ORShipToJobAddress, CTIDShipTo, ORSu" & _
        "pplierInvoiceNumber, EXIDOrderedBy, ORQuotedCost, ORInvoicedCost, ORDescription," & _
        " ORDeliveryDate, ORNotes, ORReceived, ORIsComplete, ORInvoicedCostTotal, ORStatu" & _
        "s, ID FROM VOrders WHERE (BRID = @BRID) AND (ORID = @ORID)"
        Me.SqlCommand10.Connection = Me.SqlConnection
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORType", System.Data.SqlDbType.VarChar, 3, "ORType"))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OROrderNumber", System.Data.SqlDbType.BigInt, 8, "OROrderNumber"))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORDate", System.Data.SqlDbType.DateTime, 8, "ORDate"))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTIDSupplier", System.Data.SqlDbType.BigInt, 8, "CTIDSupplier"))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORShipToJobAddress", System.Data.SqlDbType.Bit, 1, "ORShipToJobAddress"))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTIDShipTo", System.Data.SqlDbType.BigInt, 8, "CTIDShipTo"))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORSupplierInvoiceNumber", System.Data.SqlDbType.VarChar, 50, "ORSupplierInvoiceNumber"))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDOrderedBy", System.Data.SqlDbType.Int, 4, "EXIDOrderedBy"))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORQuotedCost", System.Data.SqlDbType.Money, 8, "ORQuotedCost"))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORInvoicedCost", System.Data.SqlDbType.Money, 8, "ORInvoicedCost"))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORDescription", System.Data.SqlDbType.VarChar, 50, "ORDescription"))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORDeliveryDate", System.Data.SqlDbType.DateTime, 8, "ORDeliveryDate"))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORNotes", System.Data.SqlDbType.VarChar, 500, "ORNotes"))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORReceived", System.Data.SqlDbType.Bit, 1, "ORReceived"))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTIDShipTo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTIDShipTo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTIDSupplier", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTIDSupplier", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDOrderedBy", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDOrderedBy", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORDeliveryDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORDeliveryDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORDescription", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORInvoicedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORInvoicedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORNotes", System.Data.SqlDbType.VarChar, 500, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OROrderNumber", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OROrderNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORQuotedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORQuotedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORReceived", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORReceived", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORShipToJobAddress", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORShipToJobAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORSupplierInvoiceNumber", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORSupplierInvoiceNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORType", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORID", System.Data.SqlDbType.BigInt, 8, "ORID"))
        '
        'daOrders_Materials
        '
        Me.daOrders_Materials.ContinueUpdateOnError = True
        Me.daOrders_Materials.DeleteCommand = Me.SqlDeleteCommand8
        Me.daOrders_Materials.InsertCommand = Me.SqlCommand11
        Me.daOrders_Materials.SelectCommand = Me.SqlCommand12
        Me.daOrders_Materials.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VOrders_Materials", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("OMID", "OMID"), New System.Data.Common.DataColumnMapping("ORID", "ORID"), New System.Data.Common.DataColumnMapping("MTID", "MTID"), New System.Data.Common.DataColumnMapping("OMPrimaryAmount", "OMPrimaryAmount"), New System.Data.Common.DataColumnMapping("OMQuotedCost", "OMQuotedCost"), New System.Data.Common.DataColumnMapping("OMInvoicedCost", "OMInvoicedCost"), New System.Data.Common.DataColumnMapping("OMUseInvoicedCost", "OMUseInvoicedCost"), New System.Data.Common.DataColumnMapping("OMBrand", "OMBrand"), New System.Data.Common.DataColumnMapping("OMModelNumber", "OMModelNumber"), New System.Data.Common.DataColumnMapping("OMDescription", "OMDescription"), New System.Data.Common.DataColumnMapping("OMQty", "OMQty")})})
        Me.daOrders_Materials.UpdateCommand = Me.SqlUpdateCommand8
        '
        'SqlDeleteCommand8
        '
        Me.SqlDeleteCommand8.CommandText = "DELETE FROM Orders_Materials WHERE (BRID = @Original_BRID) AND (OMID = @Original_" & _
        "OMID) AND (MTID = @Original_MTID OR @Original_MTID IS NULL AND MTID IS NULL) AND" & _
        " (OMBrand = @Original_OMBrand OR @Original_OMBrand IS NULL AND OMBrand IS NULL) " & _
        "AND (OMDescription = @Original_OMDescription OR @Original_OMDescription IS NULL " & _
        "AND OMDescription IS NULL) AND (OMInvoicedCost = @Original_OMInvoicedCost OR @Or" & _
        "iginal_OMInvoicedCost IS NULL AND OMInvoicedCost IS NULL) AND (OMModelNumber = @" & _
        "Original_OMModelNumber OR @Original_OMModelNumber IS NULL AND OMModelNumber IS N" & _
        "ULL) AND (OMPrimaryAmount = @Original_OMPrimaryAmount OR @Original_OMPrimaryAmou" & _
        "nt IS NULL AND OMPrimaryAmount IS NULL) AND (OMQty = @Original_OMQty OR @Origina" & _
        "l_OMQty IS NULL AND OMQty IS NULL) AND (OMQuotedCost = @Original_OMQuotedCost OR" & _
        " @Original_OMQuotedCost IS NULL AND OMQuotedCost IS NULL) AND (OMUseInvoicedCost" & _
        " = @Original_OMUseInvoicedCost OR @Original_OMUseInvoicedCost IS NULL AND OMUseI" & _
        "nvoicedCost IS NULL) AND (ORID = @Original_ORID)"
        Me.SqlDeleteCommand8.Connection = Me.SqlConnection
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMBrand", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMBrand", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMDescription", System.Data.SqlDbType.VarChar, 500, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMInvoicedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMInvoicedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMModelNumber", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMModelNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMPrimaryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OMPrimaryAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMQty", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMQty", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMQuotedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMQuotedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMUseInvoicedCost", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMUseInvoicedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlCommand11
        '
        Me.SqlCommand11.CommandText = "INSERT INTO Orders_Materials (BRID, ORID, MTID, OMPrimaryAmount, OMQuotedCost, OM" & _
        "InvoicedCost, OMUseInvoicedCost, OMBrand, OMModelNumber, OMDescription, OMQty) V" & _
        "ALUES (@BRID, @ORID, @MTID, @OMPrimaryAmount, @OMQuotedCost, @OMInvoicedCost, @O" & _
        "MUseInvoicedCost, @OMBrand, @OMModelNumber, @OMDescription, @OMQty); SELECT BRID" & _
        ", OMID, ORID, MTID, OMPrimaryAmount, OMQuotedCost, OMInvoicedCost, OMUseInvoiced" & _
        "Cost, OMBrand, OMModelNumber, OMDescription, OMQty, MTPrimaryUOMShortName FROM V" & _
        "Orders_Materials WHERE (BRID = @BRID) AND (OMID = @@IDENTITY)"
        Me.SqlCommand11.Connection = Me.SqlConnection
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORID", System.Data.SqlDbType.BigInt, 8, "ORID"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMPrimaryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OMPrimaryAmount", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMQuotedCost", System.Data.SqlDbType.Money, 8, "OMQuotedCost"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMInvoicedCost", System.Data.SqlDbType.Money, 8, "OMInvoicedCost"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMUseInvoicedCost", System.Data.SqlDbType.Bit, 1, "OMUseInvoicedCost"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMBrand", System.Data.SqlDbType.VarChar, 50, "OMBrand"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMModelNumber", System.Data.SqlDbType.VarChar, 50, "OMModelNumber"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMDescription", System.Data.SqlDbType.VarChar, 500, "OMDescription"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMQty", System.Data.SqlDbType.Int, 4, "OMQty"))
        '
        'SqlCommand12
        '
        Me.SqlCommand12.CommandText = "SELECT BRID, OMID, ORID, MTID, OMPrimaryAmount, OMQuotedCost, OMInvoicedCost, OMU" & _
        "seInvoicedCost, OMBrand, OMModelNumber, OMDescription, OMQty, MTPrimaryUOMShortN" & _
        "ame FROM VOrders_Materials WHERE (BRID = @BRID) AND (JBID = @JBID)"
        Me.SqlCommand12.Connection = Me.SqlConnection
        Me.SqlCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlUpdateCommand8
        '
        Me.SqlUpdateCommand8.CommandText = "UPDATE Orders_Materials SET BRID = @BRID, ORID = @ORID, MTID = @MTID, OMPrimaryAm" & _
        "ount = @OMPrimaryAmount, OMQuotedCost = @OMQuotedCost, OMInvoicedCost = @OMInvoi" & _
        "cedCost, OMUseInvoicedCost = @OMUseInvoicedCost, OMBrand = @OMBrand, OMModelNumb" & _
        "er = @OMModelNumber, OMDescription = @OMDescription, OMQty = @OMQty WHERE (BRID " & _
        "= @Original_BRID) AND (OMID = @Original_OMID) AND (MTID = @Original_MTID OR @Ori" & _
        "ginal_MTID IS NULL AND MTID IS NULL) AND (OMBrand = @Original_OMBrand OR @Origin" & _
        "al_OMBrand IS NULL AND OMBrand IS NULL) AND (OMDescription = @Original_OMDescrip" & _
        "tion OR @Original_OMDescription IS NULL AND OMDescription IS NULL) AND (OMInvoic" & _
        "edCost = @Original_OMInvoicedCost OR @Original_OMInvoicedCost IS NULL AND OMInvo" & _
        "icedCost IS NULL) AND (OMModelNumber = @Original_OMModelNumber OR @Original_OMMo" & _
        "delNumber IS NULL AND OMModelNumber IS NULL) AND (OMPrimaryAmount = @Original_OM" & _
        "PrimaryAmount OR @Original_OMPrimaryAmount IS NULL AND OMPrimaryAmount IS NULL) " & _
        "AND (OMQty = @Original_OMQty OR @Original_OMQty IS NULL AND OMQty IS NULL) AND (" & _
        "OMQuotedCost = @Original_OMQuotedCost OR @Original_OMQuotedCost IS NULL AND OMQu" & _
        "otedCost IS NULL) AND (OMUseInvoicedCost = @Original_OMUseInvoicedCost OR @Origi" & _
        "nal_OMUseInvoicedCost IS NULL AND OMUseInvoicedCost IS NULL) AND (ORID = @Origin" & _
        "al_ORID); SELECT BRID, OMID, ORID, MTID, OMPrimaryAmount, OMQuotedCost, OMInvoic" & _
        "edCost, OMUseInvoicedCost, OMBrand, OMModelNumber, OMDescription, OMQty, MTPrima" & _
        "ryUOMShortName FROM VOrders_Materials WHERE (BRID = @BRID) AND (OMID = @OMID)"
        Me.SqlUpdateCommand8.Connection = Me.SqlConnection
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORID", System.Data.SqlDbType.BigInt, 8, "ORID"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMPrimaryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OMPrimaryAmount", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMQuotedCost", System.Data.SqlDbType.Money, 8, "OMQuotedCost"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMInvoicedCost", System.Data.SqlDbType.Money, 8, "OMInvoicedCost"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMUseInvoicedCost", System.Data.SqlDbType.Bit, 1, "OMUseInvoicedCost"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMBrand", System.Data.SqlDbType.VarChar, 50, "OMBrand"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMModelNumber", System.Data.SqlDbType.VarChar, 50, "OMModelNumber"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMDescription", System.Data.SqlDbType.VarChar, 500, "OMDescription"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMQty", System.Data.SqlDbType.Int, 4, "OMQty"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMBrand", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMBrand", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMDescription", System.Data.SqlDbType.VarChar, 500, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMInvoicedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMInvoicedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMModelNumber", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMModelNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMPrimaryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OMPrimaryAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMQty", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMQty", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMQuotedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMQuotedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMUseInvoicedCost", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMUseInvoicedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMID", System.Data.SqlDbType.BigInt, 8, "OMID"))
        '
        'daOrders_StockedItems
        '
        Me.daOrders_StockedItems.ContinueUpdateOnError = True
        Me.daOrders_StockedItems.DeleteCommand = Me.SqlDeleteCommand11
        Me.daOrders_StockedItems.InsertCommand = Me.SqlInsertCommand10
        Me.daOrders_StockedItems.SelectCommand = Me.SqlSelectCommand10
        Me.daOrders_StockedItems.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VOrders_StockedItems", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("OSID", "OSID"), New System.Data.Common.DataColumnMapping("ORID", "ORID"), New System.Data.Common.DataColumnMapping("SIID", "SIID"), New System.Data.Common.DataColumnMapping("OSInventoryAmount", "OSInventoryAmount"), New System.Data.Common.DataColumnMapping("OSInventoryDate", "OSInventoryDate")})})
        Me.daOrders_StockedItems.UpdateCommand = Me.SqlUpdateCommand11
        '
        'SqlDeleteCommand11
        '
        Me.SqlDeleteCommand11.CommandText = "DELETE FROM Orders_StockedItems WHERE (BRID = @Original_BRID) AND (OSID = @Origin" & _
        "al_OSID) AND (ORID = @Original_ORID OR @Original_ORID IS NULL AND ORID IS NULL) " & _
        "AND (OSInventoryAmount = @Original_OSInventoryAmount OR @Original_OSInventoryAmo" & _
        "unt IS NULL AND OSInventoryAmount IS NULL) AND (OSInventoryDate = @Original_OSIn" & _
        "ventoryDate OR @Original_OSInventoryDate IS NULL AND OSInventoryDate IS NULL) AN" & _
        "D (SIID = @Original_SIID OR @Original_SIID IS NULL AND SIID IS NULL)"
        Me.SqlDeleteCommand11.Connection = Me.SqlConnection
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OSID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OSInventoryAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OSInventoryDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OSInventoryDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SIID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand10
        '
        Me.SqlInsertCommand10.CommandText = "INSERT INTO Orders_StockedItems (BRID, ORID, SIID, OSInventoryAmount, OSInventory" & _
        "Date) VALUES (@BRID, @ORID, @SIID, @OSInventoryAmount, @OSInventoryDate); SELECT" & _
        " BRID, OSID, ORID, SIID, OSInventoryAmount, OSInventoryDate, SIInventoryUOMShort" & _
        "Name FROM VOrders_StockedItems WHERE (BRID = @BRID) AND (OSID = @@IDENTITY)"
        Me.SqlInsertCommand10.Connection = Me.SqlConnection
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORID", System.Data.SqlDbType.BigInt, 8, "ORID"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIID", System.Data.SqlDbType.Int, 4, "SIID"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OSInventoryAmount", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OSInventoryDate", System.Data.SqlDbType.DateTime, 8, "OSInventoryDate"))
        '
        'SqlSelectCommand10
        '
        Me.SqlSelectCommand10.CommandText = "SELECT BRID, OSID, ORID, SIID, OSInventoryAmount, OSInventoryDate, SIInventoryUOM" & _
        "ShortName FROM VOrders_StockedItems WHERE (BRID = @BRID) AND (JBID = @JBID)"
        Me.SqlSelectCommand10.Connection = Me.SqlConnection
        Me.SqlSelectCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlUpdateCommand11
        '
        Me.SqlUpdateCommand11.CommandText = "UPDATE Orders_StockedItems SET BRID = @BRID, ORID = @ORID, SIID = @SIID, OSInvent" & _
        "oryAmount = @OSInventoryAmount, OSInventoryDate = @OSInventoryDate WHERE (BRID =" & _
        " @Original_BRID) AND (OSID = @Original_OSID) AND (ORID = @Original_ORID OR @Orig" & _
        "inal_ORID IS NULL AND ORID IS NULL) AND (OSInventoryAmount = @Original_OSInvento" & _
        "ryAmount OR @Original_OSInventoryAmount IS NULL AND OSInventoryAmount IS NULL) A" & _
        "ND (OSInventoryDate = @Original_OSInventoryDate OR @Original_OSInventoryDate IS " & _
        "NULL AND OSInventoryDate IS NULL) AND (SIID = @Original_SIID OR @Original_SIID I" & _
        "S NULL AND SIID IS NULL); SELECT BRID, OSID, ORID, SIID, OSInventoryAmount, OSIn" & _
        "ventoryDate, SIInventoryUOMShortName FROM VOrders_StockedItems WHERE (BRID = @BR" & _
        "ID) AND (OSID = @OSID)"
        Me.SqlUpdateCommand11.Connection = Me.SqlConnection
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORID", System.Data.SqlDbType.BigInt, 8, "ORID"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIID", System.Data.SqlDbType.Int, 4, "SIID"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OSInventoryAmount", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OSInventoryDate", System.Data.SqlDbType.DateTime, 8, "OSInventoryDate"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OSID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OSInventoryAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OSInventoryDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OSInventoryDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OSID", System.Data.SqlDbType.BigInt, 8, "OSID"))
        '
        'daMaterials
        '
        Me.daMaterials.InsertCommand = Me.SqlCommand13
        Me.daMaterials.SelectCommand = Me.SqlCommand14
        Me.daMaterials.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VMaterials", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("MTID", "MTID"), New System.Data.Common.DataColumnMapping("MTName", "MTName"), New System.Data.Common.DataColumnMapping("MTInventoryUOM", "MTInventoryUOM"), New System.Data.Common.DataColumnMapping("MTPrimaryUOM", "MTPrimaryUOM"), New System.Data.Common.DataColumnMapping("MTDiscontinued", "MTDiscontinued"), New System.Data.Common.DataColumnMapping("MTStocked", "MTStocked"), New System.Data.Common.DataColumnMapping("MGID", "MGID"), New System.Data.Common.DataColumnMapping("MTCurrentPurchaseCost", "MTCurrentPurchaseCost"), New System.Data.Common.DataColumnMapping("MTCurrentWasteFactorAllowance", "MTCurrentWasteFactorAllowance"), New System.Data.Common.DataColumnMapping("MGControlledAtHeadOffice", "MGControlledAtHeadOffice"), New System.Data.Common.DataColumnMapping("MTCurrentInventory", "MTCurrentInventory"), New System.Data.Common.DataColumnMapping("MTCurrentWasteFactor", "MTCurrentWasteFactor"), New System.Data.Common.DataColumnMapping("MGAssignToPortion", "MGAssignToPortion"), New System.Data.Common.DataColumnMapping("MMTID", "MMTID"), New System.Data.Common.DataColumnMapping("MTCurrentWastePercentage", "MTCurrentWastePercentage"), New System.Data.Common.DataColumnMapping("MTPrimaryUOMShortName", "MTPrimaryUOMShortName")})})
        '
        'SqlCommand13
        '
        Me.SqlCommand13.CommandText = "INSERT INTO VMaterials(BRID, MTName, MTInventoryUOM, MTPrimaryUOM, MTDiscontinued" & _
        ", MTStocked, MGID, MTCurrentPurchaseCost, MTCurrentWasteFactorAllowance, MGContr" & _
        "olledAtHeadOffice, MTCurrentInventory, MTCurrentWasteFactor, MGAssignToPortion, " & _
        "MMTID, MTCurrentWastePercentage, MTPrimaryUOMShortName) VALUES (@BRID, @MTName, " & _
        "@MTInventoryUOM, @MTPrimaryUOM, @MTDiscontinued, @MTStocked, @MGID, @MTCurrentPu" & _
        "rchaseCost, @MTCurrentWasteFactorAllowance, @MGControlledAtHeadOffice, @MTCurren" & _
        "tInventory, @MTCurrentWasteFactor, @MGAssignToPortion, @MMTID, @MTCurrentWastePe" & _
        "rcentage, @MTPrimaryUOMShortName); SELECT BRID, MTID, MTName, MTInventoryUOM, MT" & _
        "PrimaryUOM, MTDiscontinued, MTStocked, MGID, MTCurrentPurchaseCost, MTCurrentWas" & _
        "teFactorAllowance, MGControlledAtHeadOffice, MTCurrentInventory, MTCurrentWasteF" & _
        "actor, MGAssignToPortion, MMTID, MTCurrentWastePercentage, MTPrimaryUOMShortName" & _
        " FROM VMaterials"
        Me.SqlCommand13.Connection = Me.SqlConnection
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTName", System.Data.SqlDbType.VarChar, 50, "MTName"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTInventoryUOM", System.Data.SqlDbType.VarChar, 2, "MTInventoryUOM"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTPrimaryUOM", System.Data.SqlDbType.VarChar, 2, "MTPrimaryUOM"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTDiscontinued", System.Data.SqlDbType.Bit, 1, "MTDiscontinued"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTStocked", System.Data.SqlDbType.Bit, 1, "MTStocked"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTCurrentPurchaseCost", System.Data.SqlDbType.Money, 8, "MTCurrentPurchaseCost"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTCurrentWasteFactorAllowance", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "MTCurrentWasteFactorAllowance", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGControlledAtHeadOffice", System.Data.SqlDbType.Bit, 1, "MGControlledAtHeadOffice"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTCurrentInventory", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "MTCurrentInventory", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTCurrentWasteFactor", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "MTCurrentWasteFactor", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGAssignToPortion", System.Data.SqlDbType.VarChar, 2, "MGAssignToPortion"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTID", System.Data.SqlDbType.Int, 4, "MMTID"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTCurrentWastePercentage", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(4, Byte), "MTCurrentWastePercentage", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTPrimaryUOMShortName", System.Data.SqlDbType.VarChar, 50, "MTPrimaryUOMShortName"))
        '
        'SqlCommand14
        '
        Me.SqlCommand14.CommandText = "SELECT BRID, MTID, MTName, MTInventoryUOM, MTPrimaryUOM, MTDiscontinued, MTStocke" & _
        "d, MGID, MTCurrentPurchaseCost, MTCurrentWasteFactorAllowance, MGControlledAtHea" & _
        "dOffice, MTCurrentInventory, MTCurrentWasteFactor, MGAssignToPortion, MMTID, MTC" & _
        "urrentWastePercentage, MTPrimaryUOMShortName FROM VMaterials WHERE (BRID = @BRID" & _
        ") AND (MTDiscontinued = 0)"
        Me.SqlCommand14.Connection = Me.SqlConnection
        Me.SqlCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'daStockedItems
        '
        Me.daStockedItems.InsertCommand = Me.SqlCommand15
        Me.daStockedItems.SelectCommand = Me.SqlCommand16
        Me.daStockedItems.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VStockedItems", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("SIID", "SIID"), New System.Data.Common.DataColumnMapping("MTID", "MTID"), New System.Data.Common.DataColumnMapping("SIConversionToPrimary", "SIConversionToPrimary"), New System.Data.Common.DataColumnMapping("MTPrimaryUOM", "MTPrimaryUOM"), New System.Data.Common.DataColumnMapping("SIName", "SIName"), New System.Data.Common.DataColumnMapping("SIInventoryUOM", "SIInventoryUOM"), New System.Data.Common.DataColumnMapping("SICurrentInventory", "SICurrentInventory"), New System.Data.Common.DataColumnMapping("SICurrentValue", "SICurrentValue"), New System.Data.Common.DataColumnMapping("MTPrimaryUOMShortName", "MTPrimaryUOMShortName"), New System.Data.Common.DataColumnMapping("SICurrentPurchaseCost", "SICurrentPurchaseCost"), New System.Data.Common.DataColumnMapping("SICurrentInventoryInPrimaryUOM", "SICurrentInventoryInPrimaryUOM"), New System.Data.Common.DataColumnMapping("SIInventoryUOMShortName", "SIInventoryUOMShortName")})})
        '
        'SqlCommand15
        '
        Me.SqlCommand15.CommandText = "INSERT INTO VStockedItems(BRID, MTID, SIConversionToPrimary, MTPrimaryUOM, SIName" & _
        ", SIInventoryUOM, SICurrentInventory, SICurrentValue, MTPrimaryUOMShortName, SIC" & _
        "urrentPurchaseCost, SICurrentInventoryInPrimaryUOM, SIInventoryUOMShortName) VAL" & _
        "UES (@BRID, @MTID, @SIConversionToPrimary, @MTPrimaryUOM, @SIName, @SIInventoryU" & _
        "OM, @SICurrentInventory, @SICurrentValue, @MTPrimaryUOMShortName, @SICurrentPurc" & _
        "haseCost, @SICurrentInventoryInPrimaryUOM, @SIInventoryUOMShortName); SELECT BRI" & _
        "D, SIID, MTID, SIConversionToPrimary, MTPrimaryUOM, SIName, SIInventoryUOM, SICu" & _
        "rrentInventory, SICurrentValue, MTPrimaryUOMShortName, SICurrentPurchaseCost, SI" & _
        "CurrentInventoryInPrimaryUOM, SIInventoryUOMShortName FROM VStockedItems"
        Me.SqlCommand15.Connection = Me.SqlConnection
        Me.SqlCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        Me.SqlCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIConversionToPrimary", System.Data.SqlDbType.Real, 4, "SIConversionToPrimary"))
        Me.SqlCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTPrimaryUOM", System.Data.SqlDbType.VarChar, 2, "MTPrimaryUOM"))
        Me.SqlCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIName", System.Data.SqlDbType.VarChar, 50, "SIName"))
        Me.SqlCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIInventoryUOM", System.Data.SqlDbType.VarChar, 2, "SIInventoryUOM"))
        Me.SqlCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SICurrentInventory", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "SICurrentInventory", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SICurrentValue", System.Data.SqlDbType.Money, 8, "SICurrentValue"))
        Me.SqlCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTPrimaryUOMShortName", System.Data.SqlDbType.VarChar, 50, "MTPrimaryUOMShortName"))
        Me.SqlCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SICurrentPurchaseCost", System.Data.SqlDbType.Money, 8, "SICurrentPurchaseCost"))
        Me.SqlCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SICurrentInventoryInPrimaryUOM", System.Data.SqlDbType.Real, 4, "SICurrentInventoryInPrimaryUOM"))
        Me.SqlCommand15.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIInventoryUOMShortName", System.Data.SqlDbType.VarChar, 50, "SIInventoryUOMShortName"))
        '
        'SqlCommand16
        '
        Me.SqlCommand16.CommandText = "SELECT BRID, SIID, MTID, SIConversionToPrimary, MTPrimaryUOM, SIName, SIInventory" & _
        "UOM, SICurrentInventory, SICurrentValue, MTPrimaryUOMShortName, SICurrentPurchas" & _
        "eCost, SICurrentInventoryInPrimaryUOM, SIInventoryUOMShortName FROM VStockedItem" & _
        "s WHERE (BRID = @BRID)"
        Me.SqlCommand16.Connection = Me.SqlConnection
        Me.SqlCommand16.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'daJobNonCoreSalesItems
        '
        Me.daJobNonCoreSalesItems.DeleteCommand = Me.SqlDeleteCommand9
        Me.daJobNonCoreSalesItems.InsertCommand = Me.SqlInsertCommand11
        Me.daJobNonCoreSalesItems.SelectCommand = Me.SqlSelectCommand11
        Me.daJobNonCoreSalesItems.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VJobsNonCoreSalesItems", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("NIID", "NIID"), New System.Data.Common.DataColumnMapping("JBID", "JBID"), New System.Data.Common.DataColumnMapping("NIName", "NIName"), New System.Data.Common.DataColumnMapping("NIDesc", "NIDesc"), New System.Data.Common.DataColumnMapping("NICost", "NICost"), New System.Data.Common.DataColumnMapping("NIPrice", "NIPrice"), New System.Data.Common.DataColumnMapping("NIType", "NIType"), New System.Data.Common.DataColumnMapping("NIUserType", "NIUserType"), New System.Data.Common.DataColumnMapping("NIBrand", "NIBrand"), New System.Data.Common.DataColumnMapping("NISupplier", "NISupplier"), New System.Data.Common.DataColumnMapping("EXIDRep", "EXIDRep"), New System.Data.Common.DataColumnMapping("NICommissionAsPercent", "NICommissionAsPercent"), New System.Data.Common.DataColumnMapping("NICommissionAmount", "NICommissionAmount"), New System.Data.Common.DataColumnMapping("NICommissionPercent", "NICommissionPercent"), New System.Data.Common.DataColumnMapping("NIOrderedByClient", "NIOrderedByClient"), New System.Data.Common.DataColumnMapping("NIPaidByClient", "NIPaidByClient"), New System.Data.Common.DataColumnMapping("NIDeliveredToClient", "NIDeliveredToClient"), New System.Data.Common.DataColumnMapping("NIDeliveryDate", "NIDeliveryDate"), New System.Data.Common.DataColumnMapping("NIPayCommission", "NIPayCommission"), New System.Data.Common.DataColumnMapping("NIReceived", "NIReceived")})})
        Me.daJobNonCoreSalesItems.UpdateCommand = Me.SqlUpdateCommand9
        '
        'SqlDeleteCommand9
        '
        Me.SqlDeleteCommand9.CommandText = "DELETE FROM JobsNonCoreSalesItems WHERE (BRID = @Original_BRID) AND (NIID = @Orig" & _
        "inal_NIID) AND (EXIDRep = @Original_EXIDRep OR @Original_EXIDRep IS NULL AND EXI" & _
        "DRep IS NULL) AND (JBID = @Original_JBID) AND (NIBrand = @Original_NIBrand OR @O" & _
        "riginal_NIBrand IS NULL AND NIBrand IS NULL) AND (NICommissionAmount = @Original" & _
        "_NICommissionAmount OR @Original_NICommissionAmount IS NULL AND NICommissionAmou" & _
        "nt IS NULL) AND (NICommissionAsPercent = @Original_NICommissionAsPercent OR @Ori" & _
        "ginal_NICommissionAsPercent IS NULL AND NICommissionAsPercent IS NULL) AND (NICo" & _
        "mmissionPercent = @Original_NICommissionPercent OR @Original_NICommissionPercent" & _
        " IS NULL AND NICommissionPercent IS NULL) AND (NICost = @Original_NICost OR @Ori" & _
        "ginal_NICost IS NULL AND NICost IS NULL) AND (NIDeliveredToClient = @Original_NI" & _
        "DeliveredToClient OR @Original_NIDeliveredToClient IS NULL AND NIDeliveredToClie" & _
        "nt IS NULL) AND (NIDeliveryDate = @Original_NIDeliveryDate OR @Original_NIDelive" & _
        "ryDate IS NULL AND NIDeliveryDate IS NULL) AND (NIDesc = @Original_NIDesc OR @Or" & _
        "iginal_NIDesc IS NULL AND NIDesc IS NULL) AND (NIName = @Original_NIName OR @Ori" & _
        "ginal_NIName IS NULL AND NIName IS NULL) AND (NIOrderedByClient = @Original_NIOr" & _
        "deredByClient OR @Original_NIOrderedByClient IS NULL AND NIOrderedByClient IS NU" & _
        "LL) AND (NIPaidByClient = @Original_NIPaidByClient OR @Original_NIPaidByClient I" & _
        "S NULL AND NIPaidByClient IS NULL) AND (NIPayCommission = @Original_NIPayCommiss" & _
        "ion OR @Original_NIPayCommission IS NULL AND NIPayCommission IS NULL) AND (NIPri" & _
        "ce = @Original_NIPrice OR @Original_NIPrice IS NULL AND NIPrice IS NULL) AND (NI" & _
        "Received = @Original_NIReceived OR @Original_NIReceived IS NULL AND NIReceived I" & _
        "S NULL) AND (NISupplier = @Original_NISupplier OR @Original_NISupplier IS NULL A" & _
        "ND NISupplier IS NULL) AND (NIType = @Original_NIType) AND (NIUserType = @Origin" & _
        "al_NIUserType OR @Original_NIUserType IS NULL AND NIUserType IS NULL)"
        Me.SqlDeleteCommand9.Connection = Me.SqlConnection
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDRep", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDRep", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIBrand", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIBrand", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICommissionAmount", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NICommissionAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICommissionAsPercent", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NICommissionAsPercent", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICommissionPercent", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "NICommissionPercent", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NICost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIDeliveredToClient", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIDeliveredToClient", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIDeliveryDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIDeliveryDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIDesc", System.Data.SqlDbType.VarChar, 2000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIDesc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIOrderedByClient", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIOrderedByClient", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIPaidByClient", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIPaidByClient", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIPayCommission", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIPayCommission", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIPrice", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIPrice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIReceived", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIReceived", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NISupplier", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NISupplier", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIUserType", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIUserType", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand11
        '
        Me.SqlInsertCommand11.CommandText = "INSERT INTO JobsNonCoreSalesItems (BRID, JBID, NIName, NIDesc, NICost, NIPrice, N" & _
        "IType, NIUserType, NIBrand, NISupplier, EXIDRep, NICommissionAsPercent, NICommis" & _
        "sionAmount, NICommissionPercent, NIOrderedByClient, NIPaidByClient, NIDeliveredT" & _
        "oClient, NIDeliveryDate, NIPayCommission, NIReceived) VALUES (@BRID, @JBID, @NIN" & _
        "ame, @NIDesc, @NICost, @NIPrice, @NIType, @NIUserType, @NIBrand, @NISupplier, @E" & _
        "XIDRep, @NICommissionAsPercent, @NICommissionAmount, @NICommissionPercent, @NIOr" & _
        "deredByClient, @NIPaidByClient, @NIDeliveredToClient, @NIDeliveryDate, @NIPayCom" & _
        "mission, @NIReceived); SELECT BRID, NIID, JBID, NIName, NIDesc, NICost, NIPrice," & _
        " NIType, NIUserType, NIBrand, NISupplier, EXIDRep, NICommissionAsPercent, NIComm" & _
        "issionAmount, NICommissionPercent, NIOrderedByClient, NIPaidByClient, NIDelivere" & _
        "dToClient, NIDeliveryDate, NIOrderedBy, NIPaidBy, EXName, NIPayCommission, NIRec" & _
        "eived FROM VJobsNonCoreSalesItems WHERE (BRID = @BRID) AND (NIID = @@IDENTITY)"
        Me.SqlInsertCommand11.Connection = Me.SqlConnection
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIName", System.Data.SqlDbType.VarChar, 50, "NIName"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIDesc", System.Data.SqlDbType.VarChar, 2000, "NIDesc"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICost", System.Data.SqlDbType.Money, 8, "NICost"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIPrice", System.Data.SqlDbType.Money, 8, "NIPrice"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIType", System.Data.SqlDbType.VarChar, 2, "NIType"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIUserType", System.Data.SqlDbType.VarChar, 50, "NIUserType"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIBrand", System.Data.SqlDbType.VarChar, 50, "NIBrand"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NISupplier", System.Data.SqlDbType.VarChar, 50, "NISupplier"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDRep", System.Data.SqlDbType.Int, 4, "EXIDRep"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICommissionAsPercent", System.Data.SqlDbType.Int, 4, "NICommissionAsPercent"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICommissionAmount", System.Data.SqlDbType.Money, 8, "NICommissionAmount"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICommissionPercent", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "NICommissionPercent", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIOrderedByClient", System.Data.SqlDbType.Bit, 1, "NIOrderedByClient"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIPaidByClient", System.Data.SqlDbType.Bit, 1, "NIPaidByClient"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIDeliveredToClient", System.Data.SqlDbType.Bit, 1, "NIDeliveredToClient"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIDeliveryDate", System.Data.SqlDbType.DateTime, 8, "NIDeliveryDate"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIPayCommission", System.Data.SqlDbType.Bit, 1, "NIPayCommission"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIReceived", System.Data.SqlDbType.Bit, 1, "NIReceived"))
        '
        'SqlSelectCommand11
        '
        Me.SqlSelectCommand11.CommandText = "SELECT BRID, NIID, JBID, NIName, NIDesc, NICost, NIPrice, NIType, NIUserType, NIB" & _
        "rand, NISupplier, EXIDRep, NICommissionAsPercent, NICommissionAmount, NICommissi" & _
        "onPercent, NIOrderedByClient, NIPaidByClient, NIDeliveredToClient, NIDeliveryDat" & _
        "e, NIOrderedBy, NIPaidBy, EXName, NIPayCommission, NIReceived FROM VJobsNonCoreS" & _
        "alesItems WHERE (BRID = @BRID) AND (JBID = @JBID)"
        Me.SqlSelectCommand11.Connection = Me.SqlConnection
        Me.SqlSelectCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlUpdateCommand9
        '
        Me.SqlUpdateCommand9.CommandText = "UPDATE JobsNonCoreSalesItems SET BRID = @BRID, JBID = @JBID, NIName = @NIName, NI" & _
        "Desc = @NIDesc, NICost = @NICost, NIPrice = @NIPrice, NIType = @NIType, NIUserTy" & _
        "pe = @NIUserType, NIBrand = @NIBrand, NISupplier = @NISupplier, EXIDRep = @EXIDR" & _
        "ep, NICommissionAsPercent = @NICommissionAsPercent, NICommissionAmount = @NIComm" & _
        "issionAmount, NICommissionPercent = @NICommissionPercent, NIOrderedByClient = @N" & _
        "IOrderedByClient, NIPaidByClient = @NIPaidByClient, NIDeliveredToClient = @NIDel" & _
        "iveredToClient, NIDeliveryDate = @NIDeliveryDate, NIPayCommission = @NIPayCommis" & _
        "sion, NIReceived = @NIReceived WHERE (BRID = @Original_BRID) AND (NIID = @Origin" & _
        "al_NIID) AND (EXIDRep = @Original_EXIDRep OR @Original_EXIDRep IS NULL AND EXIDR" & _
        "ep IS NULL) AND (JBID = @Original_JBID) AND (NIBrand = @Original_NIBrand OR @Ori" & _
        "ginal_NIBrand IS NULL AND NIBrand IS NULL) AND (NICommissionAmount = @Original_N" & _
        "ICommissionAmount OR @Original_NICommissionAmount IS NULL AND NICommissionAmount" & _
        " IS NULL) AND (NICommissionAsPercent = @Original_NICommissionAsPercent OR @Origi" & _
        "nal_NICommissionAsPercent IS NULL AND NICommissionAsPercent IS NULL) AND (NIComm" & _
        "issionPercent = @Original_NICommissionPercent OR @Original_NICommissionPercent I" & _
        "S NULL AND NICommissionPercent IS NULL) AND (NICost = @Original_NICost OR @Origi" & _
        "nal_NICost IS NULL AND NICost IS NULL) AND (NIDeliveredToClient = @Original_NIDe" & _
        "liveredToClient OR @Original_NIDeliveredToClient IS NULL AND NIDeliveredToClient" & _
        " IS NULL) AND (NIDeliveryDate = @Original_NIDeliveryDate OR @Original_NIDelivery" & _
        "Date IS NULL AND NIDeliveryDate IS NULL) AND (NIDesc = @Original_NIDesc OR @Orig" & _
        "inal_NIDesc IS NULL AND NIDesc IS NULL) AND (NIName = @Original_NIName OR @Origi" & _
        "nal_NIName IS NULL AND NIName IS NULL) AND (NIOrderedByClient = @Original_NIOrde" & _
        "redByClient OR @Original_NIOrderedByClient IS NULL AND NIOrderedByClient IS NULL" & _
        ") AND (NIPaidByClient = @Original_NIPaidByClient OR @Original_NIPaidByClient IS " & _
        "NULL AND NIPaidByClient IS NULL) AND (NIPayCommission = @Original_NIPayCommissio" & _
        "n OR @Original_NIPayCommission IS NULL AND NIPayCommission IS NULL) AND (NIPrice" & _
        " = @Original_NIPrice OR @Original_NIPrice IS NULL AND NIPrice IS NULL) AND (NIRe" & _
        "ceived = @Original_NIReceived OR @Original_NIReceived IS NULL AND NIReceived IS " & _
        "NULL) AND (NISupplier = @Original_NISupplier OR @Original_NISupplier IS NULL AND" & _
        " NISupplier IS NULL) AND (NIType = @Original_NIType) AND (NIUserType = @Original" & _
        "_NIUserType OR @Original_NIUserType IS NULL AND NIUserType IS NULL); SELECT BRID" & _
        ", NIID, JBID, NIName, NIDesc, NICost, NIPrice, NIType, NIUserType, NIBrand, NISu" & _
        "pplier, EXIDRep, NICommissionAsPercent, NICommissionAmount, NICommissionPercent," & _
        " NIOrderedByClient, NIPaidByClient, NIDeliveredToClient, NIDeliveryDate, NIOrder" & _
        "edBy, NIPaidBy, EXName, NIPayCommission, NIReceived FROM VJobsNonCoreSalesItems " & _
        "WHERE (BRID = @BRID) AND (NIID = @NIID)"
        Me.SqlUpdateCommand9.Connection = Me.SqlConnection
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIName", System.Data.SqlDbType.VarChar, 50, "NIName"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIDesc", System.Data.SqlDbType.VarChar, 2000, "NIDesc"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICost", System.Data.SqlDbType.Money, 8, "NICost"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIPrice", System.Data.SqlDbType.Money, 8, "NIPrice"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIType", System.Data.SqlDbType.VarChar, 2, "NIType"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIUserType", System.Data.SqlDbType.VarChar, 50, "NIUserType"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIBrand", System.Data.SqlDbType.VarChar, 50, "NIBrand"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NISupplier", System.Data.SqlDbType.VarChar, 50, "NISupplier"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDRep", System.Data.SqlDbType.Int, 4, "EXIDRep"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICommissionAsPercent", System.Data.SqlDbType.Int, 4, "NICommissionAsPercent"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICommissionAmount", System.Data.SqlDbType.Money, 8, "NICommissionAmount"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICommissionPercent", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "NICommissionPercent", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIOrderedByClient", System.Data.SqlDbType.Bit, 1, "NIOrderedByClient"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIPaidByClient", System.Data.SqlDbType.Bit, 1, "NIPaidByClient"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIDeliveredToClient", System.Data.SqlDbType.Bit, 1, "NIDeliveredToClient"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIDeliveryDate", System.Data.SqlDbType.DateTime, 8, "NIDeliveryDate"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIPayCommission", System.Data.SqlDbType.Bit, 1, "NIPayCommission"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIReceived", System.Data.SqlDbType.Bit, 1, "NIReceived"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDRep", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDRep", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIBrand", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIBrand", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICommissionAmount", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NICommissionAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICommissionAsPercent", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NICommissionAsPercent", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICommissionPercent", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "NICommissionPercent", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NICost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIDeliveredToClient", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIDeliveredToClient", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIDeliveryDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIDeliveryDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIDesc", System.Data.SqlDbType.VarChar, 2000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIDesc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIOrderedByClient", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIOrderedByClient", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIPaidByClient", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIPaidByClient", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIPayCommission", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIPayCommission", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIPrice", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIPrice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIReceived", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIReceived", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NISupplier", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NISupplier", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIUserType", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIUserType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIID", System.Data.SqlDbType.BigInt, 8, "NIID"))
        '
        'frmBooking2
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(730, 568)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.btnHelp)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.ListBox)
        Me.Controls.Add(Me.pccContacts)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBooking2"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Booking"
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkJBIsAllOrderingComplete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEXIDBookingMeasurer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvMeasurers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsCalendar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvResources, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgOtherTrades, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvOtherTrades, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvOtherTrades, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvAppliances, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgAppliances, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvAppliances, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientFirstName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBReferenceName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientSuburb.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientHomePhoneNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientFaxNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientMobileNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientState.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientPostCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlScheduling, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlScheduling.ResumeLayout(False)
        CType(Me.dgAppointments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvAppointments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvAppointments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PnlOtherTrades, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PnlOtherTrades.ResumeLayout(False)
        CType(Me.pnlJobPurchaseOrders, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlJobPurchaseOrders.ResumeLayout(False)
        CType(Me.dgJobPurchaseOrders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvJobPurchaseOrders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsOrders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvJobPurchaseOrders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlAppliances, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlAppliances.ResumeLayout(False)
        CType(Me.PnlJobInformation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PnlJobInformation.ResumeLayout(False)
        CType(Me.dpLDDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDUser.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvExpenses, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvSalesReps, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBPriceQuoted.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBJobDescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlJobAddress, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlJobAddress.ResumeLayout(False)
        CType(Me.txtJBJobStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgJBJobAddressAsAbove.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBJobStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBJobSuburb.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBJobState.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBJobPostCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlClientInformation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlClientInformation.ResumeLayout(False)
        CType(Me.rgLDUseContactFalse.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pceContact.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pccContacts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgLDUseContactTrue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientWorkPhoneNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemLookUpEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMain.ResumeLayout(False)
        CType(Me.pnlNotes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlNotes.ResumeLayout(False)
        CType(Me.dgNotes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsNotes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvNotes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEXID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvExpenses2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtINDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtINNotes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtINFollowUpText, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlInternalOrders, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlInternalOrders.ResumeLayout(False)
        CType(Me.dgInternalOrders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvInternalOrders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvInternalOrders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Storage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnOK.Enabled = Not HasRole("branch_read_only")
        ListBox.SelectedIndex = 0
    End Sub

    Private Sub LoadTreeViews(ByVal BRID As Integer)
    End Sub

    ' Called for all jobs before load
    Private Sub FillPreliminaryData(ByVal BRID As Integer)
        ' --- EXPENSES ---
        daExpenses.SelectCommand.Parameters("@BRID").Value = BRID
        daExpenses.Fill(dataSet)
        daExpenses.Fill(DsNotes)
        daExpenses.Fill(DsCalendar)
        daExpenses.Fill(DsOrders)
        ' --- TASKS ---
        daTasks.Fill(DsCalendar)
        ' --- INFollowUpTypes ---
        daINFollowUpTypes.Fill(DsNotes)
        ' Materials
        daMaterials.SelectCommand.Parameters("@BRID").Value = BRID
        daMaterials.Fill(DsOrders)
        daStockedItems.SelectCommand.Parameters("@BRID").Value = BRID
        daStockedItems.Fill(DsOrders)
    End Sub

    ' Called for existing jobs
    Private Sub FillData()
        ' --- ID NOTES ---
        daIDNotes.SelectCommand.Parameters("@BRID").Value = dataRow("BRID")
        daIDNotes.SelectCommand.Parameters("@ID").Value = dataRow("ID")
        daIDNotes.Fill(DsNotes)
        ' --- ORDERS ---
        daOrders.SelectCommand.Parameters("@BRID").Value = dataRow("BRID")
        daOrders.SelectCommand.Parameters("@JBID").Value = dataRow("JBID")
        daOrders.Fill(DsOrders)
        ' --- ORDERS_MATERIALS ---
        daOrders_Materials.SelectCommand.Parameters("@BRID").Value = dataRow("BRID")
        daOrders_Materials.SelectCommand.Parameters("@JBID").Value = dataRow("JBID")
        daOrders_Materials.Fill(DsOrders)
        ' --- ORDERS_MATERIALS_SUBITEMS ---
        daOrders_Materials_SubItems.SelectCommand.Parameters("@BRID").Value = dataRow("BRID")
        daOrders_Materials_SubItems.SelectCommand.Parameters("@JBID").Value = dataRow("JBID")
        daOrders_Materials_SubItems.Fill(DsOrders)
        ' --- ORDERS_STOCKEDITEMS ---
        daOrders_StockedItems.SelectCommand.Parameters("@BRID").Value = dataRow("BRID")
        daOrders_StockedItems.SelectCommand.Parameters("@JBID").Value = dataRow("JBID")
        daOrders_StockedItems.Fill(DsOrders)
        ' --- APPLIANCES AND OTHER TRADES ---
        daJobNonCoreSalesItems.SelectCommand.Parameters("@BRID").Value = dataRow("BRID")
        daJobNonCoreSalesItems.SelectCommand.Parameters("@JBID").Value = dataRow("JBID")
        daJobNonCoreSalesItems.Fill(dataSet)
        ' --- CONTACT ---
        If Not dataRow("CTID") Is DBNull.Value Then
            If LoadContactDataRow(dataRow("BRID"), dataRow("CTID")) Then
                pceContact.Text = ContactDataRow("CTFirstName") & " (" & ContactDataRow("CTStreetAddress01") & ")"
            End If
        End If
        ' --- CALEDANR ---
        SetUpDateFilter()
        FillCalendar()
    End Sub

    ' Called for new jobs
    Private Sub FillInitData()
        ' --- CALEDANR ---
        SetUpDateFilter()
        FillCalendar()
    End Sub

    ' Called for quote request copied to jobs (book as job)
    Private Sub FillCopiedData()
        ' --- ID NOTES ---
        daIDNotes.SelectCommand.Parameters("@BRID").Value = dataRow("BRID")
        daIDNotes.SelectCommand.Parameters("@ID").Value = dataRow("ID")
        daIDNotes.Fill(DsNotes)
        ' --- CONTACT ---
        If Not dataRow("CTID") Is DBNull.Value Then
            If LoadContactDataRow(dataRow("BRID"), dataRow("CTID")) Then
                pceContact.Text = ContactDataRow("CTFirstName") & " (" & ContactDataRow("CTStreetAddress01") & ")"
            End If
        End If
        ' --- CALEDANR ---
        SetUpDateFilter()
        FillCalendar()
    End Sub

    Private Sub UpdateData()
        SqlDataAdapter.Update(dataSet)
        daIDNotes.Update(DsNotes)
        daJobNonCoreSalesItems.Update(dataSet)

        ' Update JBID
        For Each row As DataRow In DsOrders.VOrders
            If row.RowState <> DataRowState.Deleted Then
                row("JBID") = dataRow("JBID")
            End If
        Next

        'update orders
        Dim deletionsDataSet As DataSet = DsOrders.GetChanges(DataRowState.Deleted)
        Dim insertionsDataSet As DataSet = DsOrders.GetChanges(DataRowState.Added + DataRowState.Modified)
        If Not deletionsDataSet Is Nothing Then
            daOrders_Materials_SubItems.Update(deletionsDataSet)
            daOrders_Materials.Update(deletionsDataSet)
            daOrders_StockedItems.Update(deletionsDataSet)
            daOrders.Update(deletionsDataSet)
        End If
        'If Not insertionsDataSet Is Nothing Then
        daOrders.Update(DsOrders)
        daOrders_Materials.Update(DsOrders)
        daOrders_StockedItems.Update(DsOrders)
        daOrders_Materials_SubItems.Update(DsOrders)
        'End If

        UpdateAppointmentsWithJobData() ' Must update after SqlDataAdapter
        daAppointments.Update(DsCalendar.VAppointments)
        dvAppointments.RowFilter = "(BRID = " & dataRow("BRID") & ") AND (APType = 'JB') AND (APTypeID = " & dataRow("JBID") & ")"
    End Sub

    Private Sub EnableDisable()
        lblJobStreetAddress.Enabled = Not rgJBJobAddressAsAbove.EditValue
        lblJobSuburb.Enabled = Not rgJBJobAddressAsAbove.EditValue
        lblJobState.Enabled = Not rgJBJobAddressAsAbove.EditValue
        lblJobPostCode.Enabled = Not rgJBJobAddressAsAbove.EditValue
        txtJBJobStreetAddress01.Enabled = Not rgJBJobAddressAsAbove.EditValue
        txtJBJobStreetAddress02.Enabled = Not rgJBJobAddressAsAbove.EditValue
        txtJBJobSuburb.Enabled = Not rgJBJobAddressAsAbove.EditValue
        txtJBJobState.Enabled = Not rgJBJobAddressAsAbove.EditValue
        txtJBJobPostCode.Enabled = Not rgJBJobAddressAsAbove.EditValue
    End Sub

    Private Const DEFAULT_SORT As String = "EGName, EXName"
    Private Sub SetUpCalendar()
        Storage.Appointments.Labels.Clear()
        For Each task As DataRow In DsCalendar.Tasks.Rows
            Storage.Appointments.Labels.Add(System.Drawing.Color.FromArgb(task("TAColourR"), task("TAColourG"), task("TAColourB")), _
                task("TAName"), task("TAMenuCaption"))
        Next
        ' --- SET UP FILTER ---
        dvResources.RowFilter = "EXAppearInCalendarDL = 1"
        dvResources.Sort = DEFAULT_SORT
    End Sub

    Public CalendarFromDate As Object = Nothing
    Public CalendarToDate As Object = Nothing
    Private Sub FillCalendar()
        ' --- APPOINTMENTS ---
        daAppointments.SelectCommand.Parameters("@BRID").Value = dataRow("BRID")
        Me.daAppointments.SelectCommand.Parameters("@FROM_DATE").Value = IsNull(CalendarFromDate, DBNull.Value)
        Me.daAppointments.SelectCommand.Parameters("@TO_DATE").Value = IsNull(CalendarToDate, DBNull.Value)
        DsCalendar.VAppointments.Clear()
        daAppointments.Fill(DsCalendar.VAppointments)
        dvAppointments.RowFilter = "(BRID = " & dataRow("BRID") & ") AND (APType = 'JB') AND (APTypeID = " & dataRow("JBID") & ")"
        ' --- Calendar ---
        SetUpCalendar()
    End Sub

    Private UpdatingDateFilter As Boolean = False
    Public Sub UpdateDateFilter(ByVal FromDate As Object, ByVal ToDate As Object)
        If Not UpdatingDateFilter Then
            UpdatingDateFilter = True
            Dim changes As DataSet = DsCalendar.GetChanges
            Dim originalFromDate As Object = CalendarFromDate
            Dim originalToDate As Object = CalendarToDate
            CalendarFromDate = FromDate
            CalendarToDate = ToDate
            FillCalendar()
            If Not changes Is Nothing Then
                Try
                    Library.UpdateOriginalDataSet(changes, DsCalendar)
                Catch ex As OriginalRowNotFoundException
                    Message.ShowMessage("The date filter could not be changed because you have made changes to appointments outside the date range you selected.", MessageBoxIcon.Exclamation)
                    CalendarFromDate = originalFromDate
                    CalendarToDate = originalToDate
                    FillCalendar()
                    Library.UpdateOriginalDataSet(changes, DsCalendar)
                    UpdatingDateFilter = False
                    Throw New CouldNotUpdateDateFilterException(originalFromDate, originalToDate, ex)
                End Try
            End If
            UpdatingDateFilter = False
        End If
    End Sub

    Private Sub SetUpDateFilter()
        ' Set Up Default Advanced Filter
        CalendarFromDate = IsNull(dataRow("JBBookingDate"), Nothing)
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        ' EndEdit() to end editing the dataset record so that we can update
        dataRow.EndEdit()

        If ValidateForm() Then
            WriteMRU(txtJBJobDescription, UserAppDataPath)
            UpdateData()

            OK = True
            Me.Close()
        End If
    End Sub

    Private Function ValidateForm() As Boolean
        Return True
    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            dataRow.EndEdit()
            If dataSet.HasChanges Or DsNotes.HasChanges Or DsCalendar.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If response = MsgBoxResult.No Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private HelpTopic As String = Nothing
    Private Sub ListBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox.SelectedIndexChanged
        Dim pnl As DevExpress.XtraEditors.GroupControl

        For Each pnl In pnlMain.Controls
            pnl.Enabled = False
        Next
        pnl = Nothing
        Select Case ListBox.SelectedIndex
            Case 0
                pnl = pnlClientInformation
                HelpTopic = "ClientInformation"
            Case 1
                pnl = pnlJobAddress
                HelpTopic = "JobAddress"
            Case 2
                pnl = PnlJobInformation
                HelpTopic = "JobInformation"
            Case 3
                pnl = pnlJobPurchaseOrders
                HelpTopic = "StockedMaterial"
            Case 4
                pnl = pnlInternalOrders
                HelpTopic = "InternalOrders"
            Case 5
                pnl = pnlAppliances
                HelpTopic = "Appliances"
            Case 6
                pnl = PnlOtherTrades
                HelpTopic = "OtherTrades"
            Case 7
                pnl = pnlScheduling
                HelpTopic = "Scheduling"
                UpdateAppointmentsWithJobData()
            Case 8
                pnl = pnlNotes
                HelpTopic = "Notes"
        End Select
        If Not pnl Is Nothing Then
            pnl.Enabled = True
            pnl.Dock = DockStyle.Fill
            Power.Library.Library.CenterControls(pnl)
            pnl.BringToFront()
        End If

    End Sub

#Region " Job Purchase Orders "

    Public ReadOnly Property SelectedJobPurchaseOrder() As dataRow
        Get
            If Not gvJobPurchaseOrders.GetSelectedRows Is Nothing Then
                Return gvJobPurchaseOrders.GetDataRow(gvJobPurchaseOrders.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub dgJobPurchaseOrders_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgJobPurchaseOrders.DoubleClick
        btnEditJobPurchaseOrder_Click(sender, e)
    End Sub

    Private Sub btnAddJobPurchaseOrder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddJobPurchaseOrder.Click
        Dim c As Cursor = Me.Cursor
        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        frmJobPurchaseOrder2.Add(dvJobPurchaseOrders.Table, dataRow, True)
        Me.Cursor = c
    End Sub

    Private Sub btnEditJobPurchaseOrder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditJobPurchaseOrder.Click
        If Not SelectedJobPurchaseOrder Is Nothing Then
            If SelectedJobPurchaseOrder.RowState = DataRowState.Added Then
                frmJobPurchaseOrder2.Edit(SelectedJobPurchaseOrder, dataRow, True)
            Else
                If AddOrderLock(SelectedJobPurchaseOrder.Item("ORID")) Then
                    frmJobPurchaseOrder2.Edit(SelectedJobPurchaseOrder, dataRow, True)
                Else
                    Message.CurrentlyAccessed("order")
                End If
            End If
        End If
    End Sub

    Private Sub btnRemoveJobPurchaseOrder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveJobPurchaseOrder.Click
        If Not SelectedJobPurchaseOrder Is Nothing Then
            If SelectedJobPurchaseOrder.RowState = DataRowState.Added Then
                SelectedJobPurchaseOrder.Delete()
            Else
                If AddOrderLock(SelectedJobPurchaseOrder.Item("ORID")) Then
                    SelectedJobPurchaseOrder.Delete()
                Else
                    Message.CurrentlyAccessed("order")
                End If
            End If
        End If
    End Sub

#End Region

#Region " Internal Orders "

    Public ReadOnly Property SelectedInternalOrder() As dataRow
        Get
            If Not gvInternalOrders.GetSelectedRows Is Nothing Then
                Return gvInternalOrders.GetDataRow(gvInternalOrders.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub dgInternalOrders_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgInternalOrders.DoubleClick
        btnEditInternalOrder_Click(sender, e)
    End Sub

    Private Sub btnAddInternalOrder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddInternalOrder.Click
        Dim c As Cursor = Me.Cursor
        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        frmInternalOrder2.Add(dvInternalOrders.Table, dataRow("BRID"), dataRow("JBID"), dataRow("ID"))
        Me.Cursor = c
    End Sub

    Private Sub btnEditInternalOrder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditInternalOrder.Click
        If Not SelectedInternalOrder Is Nothing Then
            If SelectedInternalOrder.RowState = DataRowState.Added Then
                frmInternalOrder2.Edit(SelectedInternalOrder)
            Else
                If AddOrderLock(SelectedInternalOrder.Item("ORID")) Then
                    frmInternalOrder2.Edit(SelectedInternalOrder)
                Else
                    Message.CurrentlyAccessed("order")
                End If
            End If
        End If
    End Sub

    Private Sub btnRemoveInternalOrder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveInternalOrder.Click
        If Not SelectedInternalOrder Is Nothing Then
            If SelectedInternalOrder.RowState = DataRowState.Added Then
                SelectedInternalOrder.Delete()
            Else
                If AddOrderLock(SelectedInternalOrder.Item("ORID")) Then
                    SelectedInternalOrder.Delete()
                Else
                    Message.CurrentlyAccessed("order")
                End If
            End If
        End If
    End Sub

#End Region

    Private EditedOrderList As New ArrayList

    Private Function AddOrderLock(ByVal ORID As Long) As Boolean
        If Not IsInEditOrderList(ORID) Then
            If DataAccess.spExecLockRequest("sp_GetOrderLock", dataRow("BRID"), ORID, SqlConnection) Then
                EditedOrderList.Add(ORID)
                Return True
            Else
                Return False
            End If
        Else
            Return True
        End If
    End Function

    Private Function IsInEditOrderList(ByVal APID As Long) As Boolean
        For Each lng As Long In EditedOrderList
            If lng = APID Then
                Return True
            End If
        Next
        Return False
    End Function

    Private Sub ReleaseOrderLocks()
        For Each lng As Long In EditedOrderList
            DataAccess.spExecLockRequest("sp_ReleaseOrderLock", dataRow("BRID"), lng, SqlConnection)
        Next
    End Sub

#Region " Appliances "

    Public ReadOnly Property SelectedAppliance() As dataRow
        Get
            If Not gvAppliances.GetSelectedRows Is Nothing Then
                Return gvAppliances.GetDataRow(gvAppliances.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub dgAppliances_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgAppliances.DoubleClick
        btnEditAppliance_Click(sender, e)
    End Sub

    Private Sub btnAddAppliance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAppliance.Click
        frmBookingAppliance2.Add(dvAppliances.Table, dataRow("BRID"), dataRow("JBID"))
    End Sub

    Private Sub btnEditAppliance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditAppliance.Click
        If Not SelectedAppliance Is Nothing Then
            frmBookingAppliance2.Edit(SelectedAppliance)
        End If
    End Sub

    Private Sub btnRemoveAppliance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveAppliance.Click
        If Not SelectedAppliance Is Nothing Then
            SelectedAppliance.Delete()
        End If
    End Sub

#End Region

#Region " Other Trades "

    Public ReadOnly Property SelectedOtherTrade() As dataRow
        Get
            If Not gvOtherTrades.GetSelectedRows Is Nothing Then
                Return gvOtherTrades.GetDataRow(gvOtherTrades.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub dgOtherTrades_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgOtherTrades.DoubleClick
        btnEditOtherTrade_Click(sender, e)
    End Sub

    Private Sub btnAddOtherTrade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddOtherTrade.Click
        frmBookingOtherTrade2.Add(dvAppliances.Table, dataRow("BRID"), dataRow("JBID"))
    End Sub

    Private Sub btnEditOtherTrade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditOtherTrade.Click
        If Not SelectedOtherTrade Is Nothing Then
            frmBookingOtherTrade2.Edit(SelectedOtherTrade)
        End If
    End Sub

    Private Sub btnRemoveOtherTrade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveOtherTrade.Click
        If Not SelectedOtherTrade Is Nothing Then
            SelectedOtherTrade.Delete()
        End If
    End Sub

#End Region

#Region " Appointments "

    Public ReadOnly Property SelectedAppointment() As dataRow
        Get
            If Not gvAppointments.GetSelectedRows Is Nothing Then
                Return gvAppointments.GetDataRow(gvAppointments.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub Calendar_NewAllDayEvent(ByVal sender As Object, ByVal e As System.EventArgs)
        frmAppointment2.Add(SqlConnection, Storage, dvAppointments.Table, dataRow("BRID"), "JB", dataRow("JBID"), True, 1, , , , Me)
    End Sub

    Private Sub Calendar_NewAppointment(ByVal sender As Object, ByVal e As System.EventArgs)
        frmAppointment2.Add(SqlConnection, Storage, dvAppointments.Table, dataRow("BRID"), "JB", dataRow("JBID"), False, 1, , , , Me)
    End Sub

    Private Sub btnAddAppointment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAppointment.Click
        Calendar_NewAllDayEvent(sender, e)
    End Sub

    Private Sub Calendar_EditAppointment(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not SelectedAppointment Is Nothing Then
            'If Not (SelectedAppointment.Item("BRID") = Me.BRID And SelectedAppointment.Item("APType") = "JB" And SelectedAppointment.Item("APTypeID") = JBID) Then
            '    If Message.AppointmentNotCurrentObject("job", Message.ObjectAction.Edit) = MsgBoxResult.No Then
            '        Exit Sub
            '    End If
            'End If
            If SelectedAppointment.RowState = DataRowState.Added Then
                frmAppointment2.Edit(SqlConnection, Storage, SelectedAppointment, 1)
            Else
                If AddAppointmentLock(SelectedAppointment.Item("APID")) Then
                    frmAppointment2.Edit(SqlConnection, Storage, SelectedAppointment, 1)
                Else
                    Message.CurrentlyAccessed("appointment")
                End If
            End If
        End If
    End Sub

    Private Sub dgAppointments_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgAppointments.DoubleClick
        Calendar_EditAppointment(sender, e)
    End Sub

    Private Sub btnEditAppointment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditAppointment.Click
        Calendar_EditAppointment(sender, e)
    End Sub

    Private Sub btnRemoveAppointment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveAppointment.Click
        If Not SelectedAppointment Is Nothing Then
            'If Not (SelectedAppointment.Item("BRID") = Me.BRID And SelectedAppointment.Item("APType") = "JB" And SelectedAppointment.Item("APTypeID") = JBID) Then
            '    If Message.AppointmentNotCurrentObject("job", Message.ObjectAction.Delete) = MsgBoxResult.No Then
            '        Exit Sub
            '    End If
            'End If
            If SelectedAppointment.RowState = DataRowState.Added Then
                SelectedAppointment.Delete()
            Else
                If AddAppointmentLock(SelectedAppointment.Item("APID")) Then
                    SelectedAppointment.Delete()
                Else
                    Message.CurrentlyAccessed("appointment")
                End If
            End If
        End If
    End Sub

    Private Sub btnViewCalendar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewCalendar.Click
        Dim gui As New frmCalendar2(SqlConnection, DsCalendar, dataRow("BRID"), "JB", dataRow("JBID"), Me)
        gui.EditedAppointmentList = EditedAppointmentList
        gui.Calendar.Calendar.SelectedInterval.Start = Today.Date
        gui.Calendar.Calendar.SelectedInterval.End = Today.Date
        gui.Calendar.Calendar.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Week
        gui.ShowDialog(Me)
    End Sub

    Private EditedAppointmentList As New ArrayList

    Private Function AddAppointmentLock(ByVal APID As Long) As Boolean
        If Not IsInEditList(APID) Then
            If DataAccess.spExecLockRequest("sp_GetAppointmentLock", dataRow("BRID"), APID, SqlConnection) Then
                EditedAppointmentList.Add(APID)
                Return True
            Else
                Return False
            End If
        Else
            Return True
        End If
    End Function

    Private Function IsInEditList(ByVal APID As Long) As Boolean
        For Each lng As Long In EditedAppointmentList
            If lng = APID Then
                Return True
            End If
        Next
        Return False
    End Function

    Private Sub ReleaseAppointmentLocks()
        For Each lng As Long In EditedAppointmentList
            DataAccess.spExecLockRequest("sp_ReleaseAppointmentLock", dataRow("BRID"), lng, SqlConnection)
        Next
    End Sub

#End Region

#Region " Notes "

    Public ReadOnly Property SelectedNote() As dataRow
        Get
            If Not gvNotes.GetSelectedRows Is Nothing Then
                Return gvNotes.GetDataRow(gvNotes.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub gvNotes_InitNewRow(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) _
        Handles gvNotes.InitNewRow
        gvNotes.GetDataRow(e.RowHandle)("BRID") = dataRow("BRID")
        gvNotes.GetDataRow(e.RowHandle)("ID") = dataRow("ID")
        gvNotes.GetDataRow(e.RowHandle)("INFollowUpText") = DsNotes.INFollowUpTypes.FindByINFollowUpType _
            (gvNotes.GetDataRow(e.RowHandle)("INFollowUpType"))("INFollowUpTypeDesc")
    End Sub

    Private Sub gvNotes_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) _
    Handles gvNotes.KeyDown
        If e.KeyCode = Keys.Delete Then
            If gvNotes.SelectedRowsCount > 0 Then
                gvNotes.GetDataRow(gvNotes.GetSelectedRows(0)).Delete()
            End If
        End If
    End Sub

    Private Sub gvNotes_InvalidRowException(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) _
    Handles gvNotes.InvalidRowException
        GridView_InvalidRowException(sender, e)
    End Sub

    Private Sub txtINFollowUpText_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) _
    Handles txtINFollowUpText.ButtonClick
        If SelectedNote Is Nothing Then
            gvNotes.AddNewRow()
        End If
        frmFollowUp2.Edit(SelectedNote)
        gvNotes.RefreshRow(gvNotes.GetSelectedRows(0))
    End Sub

    Private Sub txtINNotes_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) _
    Handles txtINNotes.ButtonClick
        If SelectedNote Is Nothing Then
            gvNotes.AddNewRow()
        End If
        SelectedNote("INNotes") = frmMemo.Edit(IsNull(SelectedNote("INNotes"), ""), "Notes")
        gvNotes.RefreshRow(gvNotes.GetSelectedRows(0))
    End Sub

#End Region

    Private Sub Decimal_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) Handles txtJBPriceQuoted.ParseEditValue
        Format.Decimal_ParseEditValue(sender, e)
    End Sub

    Private Sub Date_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) _
    Handles txtINDate.Validating
        Library.DateEdit_Validating(sender, e)
    End Sub

    Private Sub rgJBJobAddressAsAbove_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgJBJobAddressAsAbove.SelectedIndexChanged
        EnableDisable()
    End Sub

    Private Sub btnHelp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHelp.Click
        If HelpTopic Is Nothing Then
            ShowHelpTopic(Me, "JobBookingsTerms.html")
        Else
            ShowHelpTopic(Me, "JobBookingsTerms.html#" & HelpTopic)
        End If
    End Sub

    Private Sub txtJBClientFirstName_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtJBClientFirstName.EditValueChanged, txtJBClientSurname.EditValueChanged, txtJBClientFirstName.Validated, txtJBClientSurname.Validated
        If Not dataRow Is Nothing Then
            Me.Text = "Job Booking " & dataRow("ID") & " - " & FormatName(dataRow("JBClientFirstName"), dataRow("JBClientSurname"))
        Else
            Me.Text = "Job Booking"
        End If
    End Sub

    Private Sub miClearHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miClearHistory.Click
        ClearHistory(sender, e)
    End Sub

    Private Sub UpdateAppointmentsWithJobData()
        For Each appointment As DataRowView In dvAppointments
            PopulateAppointmentWithJobData(appointment.Row)
        Next
    End Sub

    Public Sub PopulateAppointmentWithJobData(ByVal appointment As DsCalendar.VAppointmentsRow)
        If DataRowFieldDifferent(appointment("APTypeID"), dataRow("JBID")) Then appointment("APTypeID") = dataRow("JBID")
        'If DataRowFieldDifferent(appointment("APTypeID"), dataRow("JBID")) Then
        '    appointment("APTypeID") = dataRow("JBID")
        '    dateJBID = Convert.ToInt64(dataRow("JBID"))
        'Else
        '    dateJBID = Convert.ToInt64(dataRow("JBID"))
        'End If
        If DataRowFieldDifferent(appointment("APClientName"), FormatName(dataRow("JBClientFirstName"), dataRow("JBClientSurname"))) Then appointment("APClientName") = FormatName(dataRow("JBClientFirstName"), dataRow("JBClientSurname"))
        If DataRowFieldDifferent(appointment("ID"), dataRow("ID")) Then appointment("ID") = dataRow("ID")
        If DataRowFieldDifferent(appointment("APDescription"), appointment("ID") & ": " & appointment("APClientName")) Then appointment("APDescription") = appointment("ID") & ": " & appointment("APClientName")
        If DataRowFieldDifferent(appointment("APJobDescription"), dataRow("JBJobDescription")) Then appointment("APJobDescription") = dataRow("JBJobDescription")
        If DataRowFieldDifferent(appointment("APClientHomePhoneNumber"), dataRow("JBClientHomePhoneNumber")) Then appointment("APClientHomePhoneNumber") = dataRow("JBClientHomePhoneNumber")
        If DataRowFieldDifferent(appointment("APClientWorkPhoneNumber"), dataRow("JBClientWorkPhoneNumber")) Then appointment("APClientWorkPhoneNumber") = dataRow("JBClientWorkPhoneNumber")
        If DataRowFieldDifferent(appointment("APClientMobileNumber"), dataRow("JBClientMobileNumber")) Then appointment("APClientMobileNumber") = dataRow("JBClientMobileNumber")
        If DataRowFieldDifferent(appointment("JBPriceQuoted"), dataRow("JBPriceQuoted")) Then appointment("JBPriceQuoted") = dataRow("JBPriceQuoted")
        If IsNull(dataRow("JBJobAddressAsAbove"), True) Then
            If DataRowFieldDifferent(appointment("APAddressMultiline"), FormatAddressMultiline(dataRow("JBClientStreetAddress01"), dataRow("JBClientStreetAddress02"), dataRow("JBClientSuburb"), dataRow("JBClientState"), dataRow("JBClientPostCode"))) Then appointment("APAddressMultiline") = FormatAddressMultiline(dataRow("JBClientStreetAddress01"), dataRow("JBClientStreetAddress02"), dataRow("JBClientSuburb"), dataRow("JBClientState"), dataRow("JBClientPostCode"))
            If DataRowFieldDifferent(appointment("APSuburb"), dataRow("JBClientSuburb")) Then appointment("APSuburb") = dataRow("JBClientSuburb")
        Else
            If DataRowFieldDifferent(appointment("APAddressMultiline"), FormatAddressMultiline(dataRow("JBJobStreetAddress01"), dataRow("JBJobStreetAddress02"), dataRow("JBJobSuburb"), dataRow("JBJobState"), dataRow("JBJobPostCode"))) Then appointment("APAddressMultiline") = FormatAddressMultiline(dataRow("JBJobStreetAddress01"), dataRow("JBJobStreetAddress02"), dataRow("JBJobSuburb"), dataRow("JBJobState"), dataRow("JBJobPostCode"))
            If DataRowFieldDifferent(appointment("APSuburb"), dataRow("JBJobSuburb")) Then appointment("APSuburb") = dataRow("JBJobSuburb")
        End If
        'scheduledStartDate = appointment("APBegin")
        'scheduledEndDate = appointment("APEnd")
    End Sub

    Private contactsLoaded As Boolean = False
    Private lvContacts As lvContacts2
    Private dropDownOpen As Boolean = False
    Private Sub pceContact_Popup(ByVal sender As Object, ByVal e As System.EventArgs) Handles pceContact.Popup
        If contactsLoaded Then
            If Not ReopeningPopup Then
                If Not dataRow("CTID") Is DBNull.Value Then
                    lvContacts.SelectRow(dataRow("BRID"), dataRow("CTID"))
                End If
            End If
        Else
            Dim c As Cursor = Me.Cursor
            Me.Cursor = Cursors.WaitCursor
            lvContacts = New lvContacts2(Me.SqlConnection, dataRow("BRID"))
            AddHandler lvContacts.ContactSelected, AddressOf Me.ContactSelected
            AddHandler lvContacts.PopupClosed, AddressOf Me.ContactsPopupClosed
            lvContacts.DoubleClickAction = lvContacts2.ContactAction.SelectContact
            lvContacts.Dock = DockStyle.Fill
            lvContacts.Visible = False
            pccContacts.Controls.Add(lvContacts)
            lvContacts.Visible = True
            If Not ReopeningPopup Then
                If Not dataRow("CTID") Is DBNull.Value Then
                    lvContacts.SelectRow(dataRow("BRID"), dataRow("CTID"))
                End If
            End If
            lvContacts.Select()
            contactsLoaded = True
            Me.Cursor = c
        End If
        dropDownOpen = True
    End Sub

    Private ReopeningPopup As Boolean = False
    Private Sub ContactsPopupClosed()
        'Dim BRID As Integer
        'Dim CTID As Long
        'BRID = lvContacts.SelectedRowField("BRID")
        'CTID = lvContacts.SelectedRowField("CTID")
        ReopeningPopup = True
        Me.pceContact.ShowPopup()
        ReopeningPopup = False
        'lvContacts.SelectRow(BRID, CTID)
    End Sub

    Private ContactDataRow As dataRow
    Private Function LoadContactDataRow(ByVal BRID As Integer, ByVal CTID As Long) As Boolean
        dataSet.Contacts.Clear()
        daContacts.SelectCommand.Parameters("@BRID").Value = BRID
        daContacts.SelectCommand.Parameters("@CTID").Value = CTID
        daContacts.Fill(dataSet)
        If dataSet.Contacts.Count > 0 Then
            ContactDataRow = dataSet.Contacts(0)
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub ContactSelected(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal BRID As Integer, ByVal CTID As Long)
        Dim oldCTID As Object = dataRow("CTID")
        If LoadContactDataRow(BRID, CTID) Then
            Dim copyAcross As Boolean
            If IsNull(ContactDataRow("CTID"), -1) = IsNull(oldCTID, -2) And Not (IsNull(dataRow("JBClientSurname"), "") = IsNull(ContactDataRow("CTSurname"), "") And _
                    IsNull(dataRow("JBClientFirstName"), "") = IsNull(ContactDataRow("CTFirstName"), "") And _
                    IsNull(dataRow("JBReferenceName"), "") = IsNull(ContactDataRow("CTReferenceName"), "") And _
                    IsNull(dataRow("JBClientStreetAddress01"), "") = IsNull(ContactDataRow("CTStreetAddress01"), "") And _
                    IsNull(dataRow("JBClientStreetAddress02"), "") = IsNull(ContactDataRow("CTStreetAddress02"), "") And _
                    IsNull(dataRow("JBClientSuburb"), "") = IsNull(ContactDataRow("CTSuburb"), "") And _
                    IsNull(dataRow("JBClientState"), "") = IsNull(ContactDataRow("CTState"), "") And _
                    IsNull(dataRow("JBClientPostCode"), "") = IsNull(ContactDataRow("CTPostCode"), "") And _
                    IsNull(dataRow("JBClientHomePhoneNumber"), "") = IsNull(ContactDataRow("CTHomePhoneNumber"), "") And _
                    IsNull(dataRow("JBClientWorkPhoneNumber"), "") = IsNull(ContactDataRow("CTWorkPhoneNumber"), "") And _
                    IsNull(dataRow("JBClientFaxNumber"), "") = IsNull(ContactDataRow("CTFaxNumber"), "") And _
                    IsNull(dataRow("JBClientMobileNumber"), "") = IsNull(ContactDataRow("CTMobileNumber"), "") And _
                    IsNull(dataRow("JBClientEmail"), "") = IsNull(ContactDataRow("CTEmail"), "")) Then
                copyAcross = (Message.AskCopyContactDetails("job") = DialogResult.Yes)
            Else
                copyAcross = True
            End If
            If copyAcross Then
                With ContactDataRow
                    pceContact.Text = .Item("CTFirstName") & " (" & .Item("CTStreetAddress01") & ")"
                    dataRow("CTID") = .Item("CTID")
                    dataRow("JBClientSurname") = .Item("CTSurname")
                    dataRow("JBClientFirstName") = .Item("CTFirstName")
                    dataRow("JBReferenceName") = .Item("CTReferenceName")
                    dataRow("JBClientStreetAddress01") = .Item("CTStreetAddress01")
                    dataRow("JBClientStreetAddress02") = .Item("CTStreetAddress02")
                    dataRow("JBClientSuburb") = .Item("CTSuburb")
                    dataRow("JBClientState") = .Item("CTState")
                    dataRow("JBClientPostCode") = .Item("CTPostCode")
                    dataRow("JBClientHomePhoneNumber") = .Item("CTHomePhoneNumber")
                    dataRow("JBClientWorkPhoneNumber") = .Item("CTWorkPhoneNumber")
                    dataRow("JBClientFaxNumber") = .Item("CTFaxNumber")
                    dataRow("JBClientMobileNumber") = .Item("CTMobileNumber")
                    dataRow("JBClientEmail") = .Item("CTEmail")
                End With
            End If
            dropDownOpen = False
            pceContact.ClosePopup()
        End If
    End Sub

    Private Sub pceContact_CloseUp(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.CloseUpEventArgs) Handles pceContact.CloseUp
        If Not dataRow("CTID") Is DBNull.Value Then
            If LoadContactDataRow(dataRow("BRID"), dataRow("CTID")) Then
                'pceContact.Text = ContactDataRow("CTName") & " (" & ContactDataRow("CTAddress") & ")"
                pceContact.Text = ContactDataRow("CTFirstName") & " (" & ContactDataRow("CTStreetAddress01") & ")"

            End If
        End If
        e.AcceptValue = True
    End Sub

    Private Sub rgLDUseContactTrue_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgLDUseContactTrue.SelectedIndexChanged
        If rgLDUseContactTrue.EditValue = True Then
            rgLDUseContactTrue.DoValidate()
            rgLDUseContactFalse.EditValue = rgLDUseContactTrue.EditValue
            rgLDUseContactFalse.DoValidate()
            'DataRow("LDUseContact") = False
            dataRow.EndEdit()
            Me.pceContact.Enabled = True
            Me.txtJBClientSurname.Properties.ReadOnly = True
            Me.txtJBClientFirstName.Properties.ReadOnly = True
            Me.txtJBReferenceName.Properties.ReadOnly = True
            Me.txtJBClientStreetAddress01.Properties.ReadOnly = True
            Me.txtJBClientStreetAddress02.Properties.ReadOnly = True
            Me.txtJBClientSuburb.Properties.ReadOnly = True
            Me.txtJBClientState.Properties.ReadOnly = True
            Me.txtJBClientPostCode.Properties.ReadOnly = True
            Me.txtJBClientHomePhoneNumber.Properties.ReadOnly = True
            Me.txtJBClientWorkPhoneNumber.Properties.ReadOnly = True
            Me.txtJBClientFaxNumber.Properties.ReadOnly = True
            Me.txtJBClientMobileNumber.Properties.ReadOnly = True
            Me.txtJBClientEmail.Properties.ReadOnly = True
        End If
    End Sub

    Private Sub rgLDUseContactFalse_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgLDUseContactFalse.SelectedIndexChanged
        If rgLDUseContactFalse.EditValue = False Then
            rgLDUseContactFalse.DoValidate()
            rgLDUseContactTrue.EditValue = rgLDUseContactFalse.EditValue
            rgLDUseContactTrue.DoValidate()
            'DataRow("LDUseContact") = False
            dataRow.EndEdit()
            dataRow("CTID") = DBNull.Value
            Me.pceContact.EditValue = Nothing
            Me.pceContact.Enabled = False
            Me.txtJBClientSurname.Properties.ReadOnly = False
            Me.txtJBClientFirstName.Properties.ReadOnly = False
            Me.txtJBReferenceName.Properties.ReadOnly = False
            Me.txtJBClientStreetAddress01.Properties.ReadOnly = False
            Me.txtJBClientStreetAddress02.Properties.ReadOnly = False
            Me.txtJBClientSuburb.Properties.ReadOnly = False
            Me.txtJBClientState.Properties.ReadOnly = False
            Me.txtJBClientPostCode.Properties.ReadOnly = False
            Me.txtJBClientHomePhoneNumber.Properties.ReadOnly = False
            Me.txtJBClientWorkPhoneNumber.Properties.ReadOnly = False
            Me.txtJBClientFaxNumber.Properties.ReadOnly = False
            Me.txtJBClientMobileNumber.Properties.ReadOnly = False
            Me.txtJBClientEmail.Properties.ReadOnly = False
        End If
    End Sub

End Class

Public Class CouldNotUpdateDateFilterException
    Inherits Exception

    Private hOriginalFromDate As Object
    Private hOriginalToDate As Object

    Sub New(ByVal originalFromDate As Object, ByVal originalToDate As Object)
        MyBase.New("The date filter could not be changed because you have made changes to appointments outside the date range you selected.")
        hOriginalFromDate = originalFromDate
        hOriginalToDate = originalToDate
    End Sub

    Sub New(ByVal originalFromDate As Object, ByVal originalToDate As Object, ByVal innerException As Exception)
        MyBase.New("The date filter could not be changed because you have made changes to appointments outside the date range you selected.", innerException)
        hOriginalFromDate = originalFromDate
        hOriginalToDate = originalToDate
    End Sub

    Public ReadOnly Property OriginalFromDate() As Object
        Get
            Return hOriginalFromDate
        End Get
    End Property

    Public ReadOnly Property OriginalToDate() As Object
        Get
            Return hOriginalToDate
        End Get
    End Property

End Class

