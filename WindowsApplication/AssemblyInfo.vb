Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Franchise Manager")>
<Assembly: AssemblyDescription("Franchise Manager for Align Kitchens")>
<Assembly: AssemblyCompany("Power Interactive")> 
<Assembly: AssemblyProduct("Franchise Manager")> 
<Assembly: AssemblyCopyright("� 2005 Power Interactive")> 
<Assembly: AssemblyTrademark("")> 
<Assembly: CLSCompliant(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("4DE33DE0-C0FC-4A4D-9A78-EFBDF654F659")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("1.0.40.*")> 
