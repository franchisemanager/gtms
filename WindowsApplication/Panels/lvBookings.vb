Imports Power.Forms

Public Class lvBookings
    Inherits System.Windows.Forms.UserControl
    Implements IListControl, IPrintableControl, ISelectedItemReports

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Connection As SqlClient.SqlConnection, ByVal BRID As Integer, ByVal ExplorerForm As frmExplorer)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.Connection = Connection
        Me.BRID = BRID
        hExplorerForm = ExplorerForm
        SetUpPermissions()
        FillDataSet()
    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DsGTMS As WindowsApplication.dsGTMS
    Friend WithEvents hSqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colJBClientName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colJBJobAddress As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colJBScheduledStartDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colJBBookingStatus As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents RepositoryItemComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents colJBBookingDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BarAndDockingController1 As DevExpress.XtraBars.BarAndDockingController
    Friend WithEvents ImageListToolBar As System.Windows.Forms.ImageList
    Friend WithEvents PopupMenu1 As DevExpress.XtraBars.PopupMenu
    Friend WithEvents subReportsOnSelectedItem As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnAppliancesOrderForm As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnOtherTradesOrderForm As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subFormsOnSelectedItem As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnStockedMaterialinJobForm As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnClientSatisfactionAndPaymentForm As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnClientSurveyForm As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnEdit As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnDelete As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnUndelete As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents bStandard As DevExpress.XtraBars.Bar
    Friend WithEvents btnHelp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnNew As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnCopyLeadToJob As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPrintPreview As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnQuickPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subReportsOnSelectedItem2 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents subFormsOnSelectedItem2 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim StyleFormatCondition1 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition
        Dim StyleFormatCondition2 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(lvBookings))
        Me.colJBBookingStatus = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox
        Me.hSqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.DsGTMS = New WindowsApplication.dsGTMS
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colID = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colJBClientName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colJBJobAddress = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colJBScheduledStartDate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colJBBookingDate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.BarAndDockingController1 = New DevExpress.XtraBars.BarAndDockingController(Me.components)
        Me.ImageListToolBar = New System.Windows.Forms.ImageList(Me.components)
        Me.PopupMenu1 = New DevExpress.XtraBars.PopupMenu
        Me.subReportsOnSelectedItem = New DevExpress.XtraBars.BarSubItem
        Me.btnAppliancesOrderForm = New DevExpress.XtraBars.BarButtonItem
        Me.btnOtherTradesOrderForm = New DevExpress.XtraBars.BarButtonItem
        Me.subFormsOnSelectedItem = New DevExpress.XtraBars.BarSubItem
        Me.btnStockedMaterialinJobForm = New DevExpress.XtraBars.BarButtonItem
        Me.btnClientSatisfactionAndPaymentForm = New DevExpress.XtraBars.BarButtonItem
        Me.btnClientSurveyForm = New DevExpress.XtraBars.BarButtonItem
        Me.btnEdit = New DevExpress.XtraBars.BarButtonItem
        Me.btnDelete = New DevExpress.XtraBars.BarButtonItem
        Me.btnUndelete = New DevExpress.XtraBars.BarButtonItem
        Me.BarManager1 = New DevExpress.XtraBars.BarManager
        Me.bStandard = New DevExpress.XtraBars.Bar
        Me.btnNew = New DevExpress.XtraBars.BarButtonItem
        Me.btnHelp = New DevExpress.XtraBars.BarButtonItem
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
        Me.btnCopyLeadToJob = New DevExpress.XtraBars.BarButtonItem
        Me.btnPrintPreview = New DevExpress.XtraBars.BarButtonItem
        Me.btnQuickPrint = New DevExpress.XtraBars.BarButtonItem
        Me.btnPrint = New DevExpress.XtraBars.BarButtonItem
        Me.subReportsOnSelectedItem2 = New DevExpress.XtraBars.BarSubItem
        Me.subFormsOnSelectedItem2 = New DevExpress.XtraBars.BarSubItem
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'colJBBookingStatus
        '
        Me.colJBBookingStatus.Caption = "Status"
        Me.colJBBookingStatus.FieldName = "JBBookingStatus"
        Me.colJBBookingStatus.Name = "colJBBookingStatus"
        Me.colJBBookingStatus.OptionsColumn.AllowEdit = False
        Me.colJBBookingStatus.OptionsColumn.ReadOnly = True
        Me.colJBBookingStatus.Visible = True
        Me.colJBBookingStatus.VisibleIndex = 5
        Me.colJBBookingStatus.Width = 184
        '
        'RepositoryItemComboBox1
        '
        Me.RepositoryItemComboBox1.AutoHeight = False
        Me.RepositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox1.Items.AddRange(New Object() {"Not Scheduled", "Orders Incomplete", "Orders Complete", "Job Commenced", "Job Complete", "Cancelled"})
        Me.RepositoryItemComboBox1.Name = "RepositoryItemComboBox1"
        '
        'hSqlConnection
        '
        Me.hSqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;integrated security=SSPI;data source=""SERVER" & _
        "\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VJobs", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("JBID", "JBID"), New System.Data.Common.DataColumnMapping("ID", "ID"), New System.Data.Common.DataColumnMapping("JBScheduledStartDate", "JBScheduledStartDate"), New System.Data.Common.DataColumnMapping("JBClientName", "JBClientName"), New System.Data.Common.DataColumnMapping("JBJobAddress", "JBJobAddress"), New System.Data.Common.DataColumnMapping("JBPrice", "JBPrice"), New System.Data.Common.DataColumnMapping("JBBookingStatus", "JBBookingStatus"), New System.Data.Common.DataColumnMapping("JBIsCancelled", "JBIsCancelled")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Jobs WHERE (BRID = @Original_BRID) AND (JBID = @Original_JBID) AND (I" & _
        "D = @Original_ID) AND (JBBookingStatus = @Original_JBBookingStatus OR @Original_" & _
        "JBBookingStatus IS NULL AND JBBookingStatus IS NULL) AND (JBClientName = @Origin" & _
        "al_JBClientName OR @Original_JBClientName IS NULL AND JBClientName IS NULL) AND " & _
        "(JBIsCancelled = @Original_JBIsCancelled OR @Original_JBIsCancelled IS NULL AND " & _
        "JBIsCancelled IS NULL) AND (JBJobAddress = @Original_JBJobAddress OR @Original_J" & _
        "BJobAddress IS NULL AND JBJobAddress IS NULL) AND (JBPrice = @Original_JBPrice O" & _
        "R @Original_JBPrice IS NULL AND JBPrice IS NULL) AND (JBScheduledStartDate = @Or" & _
        "iginal_JBScheduledStartDate OR @Original_JBScheduledStartDate IS NULL AND JBSche" & _
        "duledStartDate IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.hSqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBBookingStatus", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBBookingStatus", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientName", System.Data.SqlDbType.VarChar, 102, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBIsCancelled", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBIsCancelled", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBPrice", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBPrice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBScheduledStartDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBScheduledStartDate", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Jobs (BRID, ID, JBPrice, JBIsCancelled) VALUES (@BRID, @ID, @JBPrice," & _
        " @JBIsCancelled); SELECT BRID, JBID, ID, JBScheduledStartDate, JBClientName, JBJ" & _
        "obAddress, JBPrice, JBBookingStatus, JBIsCancelled, JBBookingDate FROM VJobs WHE" & _
        "RE (BRID = @BRID) AND (JBID = @@IDENTITY) ORDER BY JBID DESC"
        Me.SqlInsertCommand1.Connection = Me.hSqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBPrice", System.Data.SqlDbType.Money, 8, "JBPrice"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBIsCancelled", System.Data.SqlDbType.Bit, 1, "JBIsCancelled"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, JBID, ID, JBScheduledStartDate, JBClientName, JBJobAddress, JBPrice," & _
        " JBBookingStatus, JBIsCancelled, JBBookingDate FROM VJobs WHERE (BRID = @BRID) O" & _
        "RDER BY JBID DESC"
        Me.SqlSelectCommand1.Connection = Me.hSqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Jobs SET BRID = @BRID, ID = @ID, JBPrice = @JBPrice, JBIsCancelled = @JBIs" & _
        "Cancelled WHERE (BRID = @Original_BRID) AND (JBID = @Original_JBID) AND (ID = @O" & _
        "riginal_ID) AND (JBIsCancelled = @Original_JBIsCancelled OR @Original_JBIsCancel" & _
        "led IS NULL AND JBIsCancelled IS NULL) AND (JBPrice = @Original_JBPrice OR @Orig" & _
        "inal_JBPrice IS NULL AND JBPrice IS NULL); SELECT BRID, JBID, ID, JBScheduledSta" & _
        "rtDate, JBClientName, JBJobAddress, JBPrice, JBBookingStatus, JBIsCancelled, JBB" & _
        "ookingDate FROM VJobs WHERE (BRID = @BRID) AND (JBID = @JBID) ORDER BY JBID DESC" & _
        ""
        Me.SqlUpdateCommand1.Connection = Me.hSqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBPrice", System.Data.SqlDbType.Money, 8, "JBPrice"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBIsCancelled", System.Data.SqlDbType.Bit, 1, "JBIsCancelled"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBIsCancelled", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBIsCancelled", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBPrice", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBPrice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'DsGTMS
        '
        Me.DsGTMS.DataSetName = "dsGTMS"
        Me.DsGTMS.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.DsGTMS.VJobs
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'GridControl1.EmbeddedNavigator
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemComboBox1})
        Me.GridControl1.Size = New System.Drawing.Size(632, 392)
        Me.GridControl1.Styles.AddReplace("CardBorder", New DevExpress.Utils.ViewStyleEx("CardBorder", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.InactiveBorder, System.Drawing.SystemColors.WindowFrame, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("BandPanelBackground", New DevExpress.Utils.ViewStyleEx("BandPanelBackground", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ControlDark, System.Drawing.Color.DarkSalmon, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("EmptySpace", New DevExpress.Utils.ViewStyleEx("EmptySpace", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("FieldValue", New DevExpress.Utils.ViewStyleEx("FieldValue", "CardView", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("BandPanel", New DevExpress.Utils.ViewStyleEx("BandPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("CardButton", New DevExpress.Utils.ViewStyleEx("CardButton", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("FocusedCardCaption", New DevExpress.Utils.ViewStyleEx("FocusedCardCaption", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ActiveCaption, System.Drawing.SystemColors.ActiveCaptionText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("CardCaption", New DevExpress.Utils.ViewStyleEx("CardCaption", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.InactiveCaption, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("HeaderPanelBackground", New DevExpress.Utils.ViewStyleEx("HeaderPanelBackground", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ControlDark, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("SeparatorLine", New DevExpress.Utils.ViewStyleEx("SeparatorLine", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ActiveBorder, System.Drawing.SystemColors.ActiveBorder, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("FieldCaption", New DevExpress.Utils.ViewStyleEx("FieldCaption", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Top, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
        Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
        Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID, Me.colJBClientName, Me.colJBJobAddress, Me.colJBScheduledStartDate, Me.colJBBookingStatus, Me.colJBBookingDate})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        StyleFormatCondition1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Strikeout)
        StyleFormatCondition1.Appearance.ForeColor = System.Drawing.SystemColors.GrayText
        StyleFormatCondition1.Appearance.Options.UseFont = True
        StyleFormatCondition1.Appearance.Options.UseForeColor = True
        StyleFormatCondition1.ApplyToRow = True
        StyleFormatCondition1.Column = Me.colJBBookingStatus
        StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal
        StyleFormatCondition1.Value1 = "Cancelled"
        StyleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red
        StyleFormatCondition2.Appearance.Options.UseForeColor = True
        StyleFormatCondition2.ApplyToRow = True
        StyleFormatCondition2.Column = Me.colJBBookingStatus
        StyleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal
        StyleFormatCondition2.Value1 = "Not Scheduled"
        Me.GridView1.FormatConditions.AddRange(New DevExpress.XtraGrid.StyleFormatCondition() {StyleFormatCondition1, StyleFormatCondition2})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsNavigation.AutoFocusNewRow = True
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowGroupedColumns = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.OptionsView.ShowHorzLines = False
        Me.GridView1.OptionsView.ShowIndicator = False
        Me.GridView1.OptionsView.ShowVertLines = False
        Me.GridView1.RowHeight = 8
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colID, DevExpress.Data.ColumnSortOrder.Descending)})
        '
        'colID
        '
        Me.colID.Caption = "ID"
        Me.colID.FieldName = "ID"
        Me.colID.Name = "colID"
        Me.colID.OptionsColumn.AllowEdit = False
        Me.colID.OptionsColumn.ReadOnly = True
        Me.colID.Visible = True
        Me.colID.VisibleIndex = 0
        Me.colID.Width = 45
        '
        'colJBClientName
        '
        Me.colJBClientName.Caption = "Customer Name"
        Me.colJBClientName.FieldName = "JBClientName"
        Me.colJBClientName.Name = "colJBClientName"
        Me.colJBClientName.OptionsColumn.AllowEdit = False
        Me.colJBClientName.OptionsColumn.ReadOnly = True
        Me.colJBClientName.Visible = True
        Me.colJBClientName.VisibleIndex = 1
        Me.colJBClientName.Width = 227
        '
        'colJBJobAddress
        '
        Me.colJBJobAddress.Caption = "Job Address"
        Me.colJBJobAddress.FieldName = "JBJobAddress"
        Me.colJBJobAddress.Name = "colJBJobAddress"
        Me.colJBJobAddress.OptionsColumn.AllowEdit = False
        Me.colJBJobAddress.OptionsColumn.ReadOnly = True
        Me.colJBJobAddress.Visible = True
        Me.colJBJobAddress.VisibleIndex = 2
        Me.colJBJobAddress.Width = 307
        '
        'colJBScheduledStartDate
        '
        Me.colJBScheduledStartDate.Caption = "Start Date"
        Me.colJBScheduledStartDate.DisplayFormat.FormatString = "d"
        Me.colJBScheduledStartDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colJBScheduledStartDate.FieldName = "JBScheduledStartDate"
        Me.colJBScheduledStartDate.Name = "colJBScheduledStartDate"
        Me.colJBScheduledStartDate.OptionsColumn.AllowEdit = False
        Me.colJBScheduledStartDate.OptionsColumn.ReadOnly = True
        Me.colJBScheduledStartDate.Visible = True
        Me.colJBScheduledStartDate.VisibleIndex = 4
        Me.colJBScheduledStartDate.Width = 173
        '
        'colJBBookingDate
        '
        Me.colJBBookingDate.Caption = "Booking Date"
        Me.colJBBookingDate.FieldName = "JBBookingDate"
        Me.colJBBookingDate.Name = "colJBBookingDate"
        Me.colJBBookingDate.OptionsColumn.AllowEdit = False
        Me.colJBBookingDate.Visible = True
        Me.colJBBookingDate.VisibleIndex = 3
        Me.colJBBookingDate.Width = 171
        '
        'BarAndDockingController1
        '
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseForeColor = True
        Me.BarAndDockingController1.PaintStyleName = "OfficeXP"
        '
        'ImageListToolBar
        '
        Me.ImageListToolBar.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImageListToolBar.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageListToolBar.ImageStream = CType(resources.GetObject("ImageListToolBar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListToolBar.TransparentColor = System.Drawing.Color.Transparent
        '
        'PopupMenu1
        '
        Me.PopupMenu1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.subReportsOnSelectedItem2, True), New DevExpress.XtraBars.LinkPersistInfo(Me.subFormsOnSelectedItem2), New DevExpress.XtraBars.LinkPersistInfo(Me.btnEdit, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnDelete), New DevExpress.XtraBars.LinkPersistInfo(Me.btnUndelete, True)})
        Me.PopupMenu1.Manager = Me.BarManager1
        Me.PopupMenu1.Name = "PopupMenu1"
        '
        'subReportsOnSelectedItem
        '
        Me.subReportsOnSelectedItem.Caption = "Reports on Selected Job"
        Me.subReportsOnSelectedItem.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.subReportsOnSelectedItem.Id = 78
        Me.subReportsOnSelectedItem.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnAppliancesOrderForm), New DevExpress.XtraBars.LinkPersistInfo(Me.btnOtherTradesOrderForm)})
        Me.subReportsOnSelectedItem.Name = "subReportsOnSelectedItem"
        '
        'btnAppliancesOrderForm
        '
        Me.btnAppliancesOrderForm.Caption = "&Appliances Order Form"
        Me.btnAppliancesOrderForm.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnAppliancesOrderForm.Id = 60
        Me.btnAppliancesOrderForm.Name = "btnAppliancesOrderForm"
        '
        'btnOtherTradesOrderForm
        '
        Me.btnOtherTradesOrderForm.Caption = "&Other Trades Order Form"
        Me.btnOtherTradesOrderForm.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnOtherTradesOrderForm.Id = 61
        Me.btnOtherTradesOrderForm.Name = "btnOtherTradesOrderForm"
        '
        'subFormsOnSelectedItem
        '
        Me.subFormsOnSelectedItem.Caption = "Forms on Selected Job"
        Me.subFormsOnSelectedItem.CategoryGuid = New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")
        Me.subFormsOnSelectedItem.Id = 79
        Me.subFormsOnSelectedItem.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnStockedMaterialinJobForm), New DevExpress.XtraBars.LinkPersistInfo(Me.btnClientSatisfactionAndPaymentForm), New DevExpress.XtraBars.LinkPersistInfo(Me.btnClientSurveyForm)})
        Me.subFormsOnSelectedItem.Name = "subFormsOnSelectedItem"
        '
        'btnStockedMaterialinJobForm
        '
        Me.btnStockedMaterialinJobForm.Caption = "&Tracked Material in Job Form"
        Me.btnStockedMaterialinJobForm.CategoryGuid = New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")
        Me.btnStockedMaterialinJobForm.Id = 62
        Me.btnStockedMaterialinJobForm.Name = "btnStockedMaterialinJobForm"
        '
        'btnClientSatisfactionAndPaymentForm
        '
        Me.btnClientSatisfactionAndPaymentForm.Caption = "Customer Satisfaction and Payment Form"
        Me.btnClientSatisfactionAndPaymentForm.CategoryGuid = New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")
        Me.btnClientSatisfactionAndPaymentForm.Id = 80
        Me.btnClientSatisfactionAndPaymentForm.Name = "btnClientSatisfactionAndPaymentForm"
        '
        'btnClientSurveyForm
        '
        Me.btnClientSurveyForm.Caption = "Customer Survey Form"
        Me.btnClientSurveyForm.CategoryGuid = New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")
        Me.btnClientSurveyForm.Id = 81
        Me.btnClientSurveyForm.Name = "btnClientSurveyForm"
        '
        'btnEdit
        '
        Me.btnEdit.Caption = "&Edit..."
        Me.btnEdit.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnEdit.Id = 20
        Me.btnEdit.ImageIndex = 1
        Me.btnEdit.ImageIndexDisabled = 1
        Me.btnEdit.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.E))
        Me.btnEdit.Name = "btnEdit"
        '
        'btnDelete
        '
        Me.btnDelete.Caption = "&Cancel"
        Me.btnDelete.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnDelete.Id = 23
        Me.btnDelete.ImageIndex = 2
        Me.btnDelete.ImageIndexDisabled = 2
        Me.btnDelete.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.D))
        Me.btnDelete.Name = "btnDelete"
        '
        'btnUndelete
        '
        Me.btnUndelete.Caption = "&Uncancel"
        Me.btnUndelete.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnUndelete.Id = 86
        Me.btnUndelete.Name = "btnUndelete"
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.bStandard})
        Me.BarManager1.Categories.AddRange(New DevExpress.XtraBars.BarManagerCategory() {New DevExpress.XtraBars.BarManagerCategory("Built-in Menus", New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd"), False), New DevExpress.XtraBars.BarManagerCategory("File", New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")), New DevExpress.XtraBars.BarManagerCategory("Reports", New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")), New DevExpress.XtraBars.BarManagerCategory("Forms", New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")), New DevExpress.XtraBars.BarManagerCategory("Help", New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785"))})
        Me.BarManager1.Controller = Me.BarAndDockingController1
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Images = Me.ImageListToolBar
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnNew, Me.btnEdit, Me.btnDelete, Me.btnCopyLeadToJob, Me.btnAppliancesOrderForm, Me.btnOtherTradesOrderForm, Me.btnStockedMaterialinJobForm, Me.btnHelp, Me.btnPrintPreview, Me.btnQuickPrint, Me.btnPrint, Me.subReportsOnSelectedItem, Me.subFormsOnSelectedItem, Me.btnClientSatisfactionAndPaymentForm, Me.btnClientSurveyForm, Me.btnUndelete, Me.subReportsOnSelectedItem2, Me.subFormsOnSelectedItem2})
        Me.BarManager1.MainMenu = Me.bStandard
        Me.BarManager1.MaxItemId = 91
        '
        'bStandard
        '
        Me.bStandard.BarName = "Standard Toolbar"
        Me.bStandard.DockCol = 0
        Me.bStandard.DockRow = 0
        Me.bStandard.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bStandard.FloatLocation = New System.Drawing.Point(44, 188)
        Me.bStandard.FloatSize = New System.Drawing.Size(659, 24)
        Me.bStandard.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnNew, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnEdit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnDelete, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(Me.subReportsOnSelectedItem, True), New DevExpress.XtraBars.LinkPersistInfo(Me.subFormsOnSelectedItem, True), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnHelp, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.bStandard.OptionsBar.AllowQuickCustomization = False
        Me.bStandard.OptionsBar.DisableClose = True
        Me.bStandard.OptionsBar.DrawDragBorder = False
        Me.bStandard.OptionsBar.MultiLine = True
        Me.bStandard.OptionsBar.UseWholeRow = True
        Me.bStandard.Text = "Standard Toolbar"
        Me.bStandard.Visible = False
        '
        'btnNew
        '
        Me.btnNew.Caption = "&New..."
        Me.btnNew.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnNew.Id = 19
        Me.btnNew.ImageIndex = 0
        Me.btnNew.ImageIndexDisabled = 0
        Me.btnNew.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N))
        Me.btnNew.Name = "btnNew"
        '
        'btnHelp
        '
        Me.btnHelp.Caption = "&Help"
        Me.btnHelp.CategoryGuid = New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")
        Me.btnHelp.Id = 68
        Me.btnHelp.ImageIndex = 4
        Me.btnHelp.ImageIndexDisabled = 4
        Me.btnHelp.Name = "btnHelp"
        '
        'btnCopyLeadToJob
        '
        Me.btnCopyLeadToJob.Caption = "Book As &Job..."
        Me.btnCopyLeadToJob.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnCopyLeadToJob.Enabled = False
        Me.btnCopyLeadToJob.Id = 24
        Me.btnCopyLeadToJob.ImageIndex = 3
        Me.btnCopyLeadToJob.ImageIndexDisabled = 3
        Me.btnCopyLeadToJob.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.J))
        Me.btnCopyLeadToJob.Name = "btnCopyLeadToJob"
        '
        'btnPrintPreview
        '
        Me.btnPrintPreview.Caption = "Print Pre&view"
        Me.btnPrintPreview.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrintPreview.Id = 44
        Me.btnPrintPreview.ImageIndex = 10
        Me.btnPrintPreview.ImageIndexDisabled = 10
        Me.btnPrintPreview.Name = "btnPrintPreview"
        '
        'btnQuickPrint
        '
        Me.btnQuickPrint.Caption = "Print"
        Me.btnQuickPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnQuickPrint.Id = 46
        Me.btnQuickPrint.ImageIndex = 11
        Me.btnQuickPrint.ImageIndexDisabled = 11
        Me.btnQuickPrint.Name = "btnQuickPrint"
        '
        'btnPrint
        '
        Me.btnPrint.Caption = "&Print..."
        Me.btnPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrint.Id = 43
        Me.btnPrint.ImageIndex = 11
        Me.btnPrint.ImageIndexDisabled = 11
        Me.btnPrint.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P))
        Me.btnPrint.Name = "btnPrint"
        '
        'subReportsOnSelectedItem2
        '
        Me.subReportsOnSelectedItem2.Caption = "&Reports on Selected Job"
        Me.subReportsOnSelectedItem2.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subReportsOnSelectedItem2.Id = 87
        Me.subReportsOnSelectedItem2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnAppliancesOrderForm), New DevExpress.XtraBars.LinkPersistInfo(Me.btnOtherTradesOrderForm)})
        Me.subReportsOnSelectedItem2.Name = "subReportsOnSelectedItem2"
        '
        'subFormsOnSelectedItem2
        '
        Me.subFormsOnSelectedItem2.Caption = "&Forms on Selected Job"
        Me.subFormsOnSelectedItem2.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subFormsOnSelectedItem2.Id = 88
        Me.subFormsOnSelectedItem2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnStockedMaterialinJobForm), New DevExpress.XtraBars.LinkPersistInfo(Me.btnClientSatisfactionAndPaymentForm), New DevExpress.XtraBars.LinkPersistInfo(Me.btnClientSurveyForm)})
        Me.subFormsOnSelectedItem2.Name = "subFormsOnSelectedItem2"
        '
        'lvBookings
        '
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "lvBookings"
        Me.Size = New System.Drawing.Size(632, 392)
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return hSqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            hSqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Public Function FillDataSet() As Integer Implements IListControl.FillDataSet
        ' Open connection
        Dim trans As SqlClient.SqlTransaction = Connection.BeginTransaction(IsolationLevel.ReadUncommitted)
        Power.Library.Library.ApplyTransactionToAllDataAdapters(trans, Me)
        'Me.GridControl1.DataSource.Clear()
        FillDataSet = SqlDataAdapter.Fill(DsGTMS)
        'Close connecton
        trans.Commit()
    End Function

    Private Sub GridView1_RowCountChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.RowCountChanged
        ExplorerForm.UpdateNumItems(NumItems)
        ExplorerForm.UpdateVisibleItems(VisibleItems)
    End Sub

    Public ReadOnly Property VisibleItems() As Integer Implements Power.Library.IListControl.VisibleItems
        Get
            Return GridView1.RowCount
        End Get
    End Property

    Public ReadOnly Property NumItems() As Integer Implements Power.Library.IListControl.NumItems
        Get
            Return DsGTMS.Tables(SqlDataAdapter.TableMappings(0).DataSetTable).Rows.Count
        End Get
    End Property

    Private hBRID As Integer
    Private Property BRID() As Integer
        Get
            Return hBRID
        End Get
        Set(ByVal Value As Integer)
            hBRID = Value
            SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = Value
        End Set
    End Property

    Public ReadOnly Property SelectedRow() As DataRow
        Get
            If Not GridView1.GetSelectedRows Is Nothing Then
                Return GridView1.GetDataRow(GridView1.GetSelectedRows(0))
            End If
        End Get
    End Property

    Public ReadOnly Property SelectedRowField(ByVal Field As String) As Object
        Get
            If Not SelectedRow Is Nothing Then
                Return SelectedRow.Item(Field)
            Else
                Return DBNull.Value
            End If
        End Get
    End Property

    Public Function SelectRow(ByVal BRID As Int32, ByVal JBID As Int64) As Boolean
        Dim i As Integer = 0
        Do Until False
            Try
                If GridView1.GetDataRow(GridView1.GetVisibleRowHandle(i))("BRID") = BRID And _
                        GridView1.GetDataRow(GridView1.GetVisibleRowHandle(i))("JBID") = JBID Then
                    GridView1.ClearSelection()
                    GridView1.SelectRow(GridView1.GetVisibleRowHandle(i))
                    Return True
                End If
                i += 1
            Catch ex As NullReferenceException
                Return False
            End Try
        Loop
        Return False
    End Function

#Region " New, Open and Delete "

    Private Sub GridControl1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridControl1.DoubleClick
        List_Edit()
    End Sub

    Public Sub List_New() Implements IListControl.List_New
        Dim c As Cursor = ParentForm.Cursor
        ParentForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            Dim gui As frmBooking = frmBooking.Add(BRID)
            gui.ShowDialog(Me)
            FillDataSet()
        Catch ex As ObjectLockedException
            Message.CurrentlyAccessed("job")
        End Try
        ParentForm.Cursor = c
    End Sub

    Public Sub List_Edit() Implements IListControl.List_Edit
        If Not SelectedRow Is Nothing Then
            Dim c As Cursor = ParentForm.Cursor
            ParentForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If JobExists(SelectedRowField("BRID"), SelectedRowField("JBID")) Then
                Try
                    Dim gui As frmBooking = frmBooking.Edit(SelectedRowField("BRID"), SelectedRowField("JBID"))
                    gui.ShowDialog(Me)
                    FillDataSet()
                Catch ex As ObjectLockedException
                    Message.CurrentlyAccessed("job")
                End Try
            Else
                Message.AlreadyDeleted("job", Message.ObjectAction.Edit)
                Me.GridControl1.DataSource.Clear()
                FillDataSet()
            End If
            ParentForm.Cursor = c
        End If
    End Sub

    Public Sub List_Delete() Implements IListControl.List_Delete
        If Not SelectedRow Is Nothing Then
            If Message.AskDeleteObject("job", Message.ObjectAction.Cancel) = MsgBoxResult.Yes Then
                If JobExists(SelectedRowField("BRID"), SelectedRowField("JBID")) Then
                    If DataAccess.spExecLockRequest("sp_GetJobLock", SelectedRowField("BRID"), SelectedRowField("JBID"), Connection) Then
                        SelectedRow.Item("JBIsCancelled") = True
                        SqlDataAdapter.Update(DsGTMS)
                        DataAccess.spExecLockRequest("sp_ReleaseJobLock", SelectedRowField("BRID"), SelectedRowField("JBID"), Connection)
                    Else
                        Message.CurrentlyAccessed("job")
                    End If
                Else
                    Message.AlreadyDeleted("job", Message.ObjectAction.Cancel)
                    Me.GridControl1.DataSource.Clear()
                    FillDataSet()
                End If
            End If
        End If
    End Sub

    Public Sub List_Undelete() Implements Power.Library.IListControl.List_Undelete
        If Not SelectedRow Is Nothing Then
            If DataAccess.spExecLockRequest("sp_GetJobLock", SelectedRowField("BRID"), SelectedRowField("JBID"), Connection) Then
                SelectedRow.Item("JBIsCancelled") = False
                SqlDataAdapter.Update(DsGTMS)
                DataAccess.spExecLockRequest("sp_ReleaseJobLock", SelectedRowField("BRID"), SelectedRowField("JBID"), Connection)
            Else
                Message.CurrentlyAccessed("job")
            End If
        End If
    End Sub

    Private Function JobExists(ByVal BRID As Int32, ByVal JBID As Int64) As Boolean
        Dim cnn As SqlClient.SqlConnection = Connection
        Dim cmd As New SqlClient.SqlCommand("SELECT Count(ID) FROM Jobs WHERE BRID = @BRID AND JBID = @JBID", cnn)
        cmd.CommandType = CommandType.Text
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@JBID", JBID)
        Return cmd.ExecuteScalar > 0
    End Function

#End Region

    Private hExplorerForm As Form
    Public ReadOnly Property ExplorerForm() As frmExplorer
        Get
            Return hExplorerForm
        End Get
    End Property

    Public Sub ShowOrderFormAppliances()
        If Not SelectedRow Is Nothing Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repOrderFormAppliances
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repOrderFormNonCoreSalesItems](@BRID, @JBID) WHERE NIType = 'AP'", Connection)
            cmd.Parameters.Add("@BRID", SelectedRowField("BRID"))
            cmd.Parameters.Add("@JBID", SelectedRowField("JBID"))
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repOrderFormNonCoreSalesItems)
            rep.SetDataSource(ds)
            'rep.SetParameterValue("Filename", MyBusinessPlan.Filename)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Appliances Order Form"
            gui.Show()
        End If
    End Sub

    Public Sub ShowOrderFormOtherTrades()
        If Not SelectedRow Is Nothing Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repOrderFormOtherTrades
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repOrderFormNonCoreSalesItems](@BRID, @JBID) WHERE NIType = 'OT'", Connection)
            cmd.Parameters.Add("@BRID", SelectedRowField("BRID"))
            cmd.Parameters.Add("@JBID", SelectedRowField("JBID"))
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repOrderFormNonCoreSalesItems)
            rep.SetDataSource(ds)
            'rep.SetParameterValue("Filename", MyBusinessPlan.Filename)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Other Trades Order Form"
            gui.Show()
        End If
    End Sub

    Public Sub ShowStockedMaterialinJobForm()
        If Not SelectedRow Is Nothing Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repStockedMaterialInJobForm
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[Jobs] WHERE BRID = @BRID AND JBID = @JBID", Connection)
            cmd.Parameters.Add("@BRID", SelectedRowField("BRID"))
            cmd.Parameters.Add("@JBID", SelectedRowField("JBID"))
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.Jobs)
            cmd.CommandText = "SELECT * FROM dbo.[subJobs_Materials](@BRID, @JBID)"
            ReadUncommittedFill(da, ds.subJobs_Materials)
            cmd.CommandText = "SELECT * FROM dbo.[subJobs_StockedItems](@BRID, @JBID)"
            ReadUncommittedFill(da, ds.subJobs_StockedItems)
            rep.SetDataSource(ds)
            'rep.SetParameterValue("Filename", MyBusinessPlan.Filename)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Tracked Material in Job Form"
            gui.Show()
        End If
    End Sub

    Public Sub ShowClientSatisfactionAndPaymentForm()
        If Not SelectedRow Is Nothing Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repStaticSatisfactionAndPayment
            rep.Load()
            Dim ds As New dsForms
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[Jobs] WHERE BRID = @BRID AND JBID = @JBID", Connection)
            cmd.Parameters.Add("@BRID", SelectedRowField("BRID"))
            cmd.Parameters.Add("@JBID", SelectedRowField("JBID"))
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.Jobs)
            cmd = New SqlClient.SqlCommand("SELECT * FROM dbo.[JobStatistics] WHERE BRID = @BRID AND JBID = @JBID", Connection)
            cmd.Parameters.Add("@BRID", SelectedRowField("BRID"))
            cmd.Parameters.Add("@JBID", SelectedRowField("JBID"))
            da = New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.JobStatistics)
            cmd = New SqlClient.SqlCommand("SELECT * FROM dbo.[VBranches] WHERE BRID = @BRID", Connection)
            cmd.Parameters.Add("@BRID", SelectedRowField("BRID"))
            da = New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.VBranches)
            rep.SetDataSource(ds)
            'rep.SetParameterValue("Filename", MyBusinessPlan.Filename)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Customer Satisfaction and Payment Form"
            gui.Show()
        End If
    End Sub

    Public Sub ShowClientSurveyForm()
        If Not SelectedRow Is Nothing Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repStaticClientSurvey
            rep.Load()
            Dim ds As New dsForms
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[Jobs] WHERE BRID = @BRID AND JBID = @JBID", Connection)
            cmd.Parameters.Add("@BRID", SelectedRowField("BRID"))
            cmd.Parameters.Add("@JBID", SelectedRowField("JBID"))
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.Jobs)
            cmd = New SqlClient.SqlCommand("SELECT * FROM dbo.[VBranches] WHERE BRID = @BRID", Connection)
            cmd.Parameters.Add("@BRID", SelectedRowField("BRID"))
            da = New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.VBranches)
            rep.SetDataSource(ds)
            'rep.SetParameterValue("Filename", MyBusinessPlan.Filename)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Customer Survey Form"
            gui.Show()
        End If
    End Sub

#Region " Printing "

    Public Sub Print(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.Print
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.PrintDlg()
    End Sub

    Public Sub PrintPreview(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.PrintPreview
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.ShowPreview()
    End Sub

    Public Sub QuickPrint(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.QuickPrint
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.Print(PrintingSystem.PageSettings.PrinterName)
    End Sub

#End Region

    Private Sub GridView1_ShowGridMenu(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.GridMenuEventArgs) Handles GridView1.ShowGridMenu
        If SelectedRow Is Nothing Then Exit Sub
        Dim View As DevExpress.XtraGrid.Views.Grid.GridView = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
        Dim HitInfo As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo
        HitInfo = View.CalcHitInfo(e.Point)
        If HitInfo.InRow Then
            Dim p2 As New Point(e.Point.X + View.GridControl.Left, e.Point.Y + View.GridControl.Top)
            btnUndelete.Enabled = SelectedRow.Item("JBIsCancelled")
            PopupMenu1.ShowPopup(Me.PointToScreen(p2))
        End If
    End Sub

    Private Sub btnNew_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnNew.ItemClick
        List_New()
    End Sub

    Private Sub btnEdit_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnEdit.ItemClick
        List_Edit()
    End Sub

    Private Sub btnDelete_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnDelete.ItemClick
        List_Delete()
    End Sub

    Private Sub btnQuickPrint_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnQuickPrint.ItemClick
        QuickPrint(ExplorerForm.PrintingSystem)
    End Sub

    Private Sub btnPrintPreview_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrintPreview.ItemClick
        PrintPreview(ExplorerForm.PrintingSystem)
    End Sub

    Private Sub btnPrint_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrint.ItemClick
        Print(ExplorerForm.PrintingSystem)
    End Sub

    Private Sub btnAppliancesOrderForm_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnAppliancesOrderForm.ItemClick
        Me.ShowOrderFormAppliances()
    End Sub

    Private Sub btnOtherTradesOrderForm_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnOtherTradesOrderForm.ItemClick
        Me.ShowOrderFormOtherTrades()
    End Sub

    Private Sub btnStockedMaterialinJobForm_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnStockedMaterialinJobForm.ItemClick
        Me.ShowStockedMaterialinJobForm()
    End Sub

    Private Sub btnClientSatisfactionAndPaymentForm_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnClientSatisfactionAndPaymentForm.ItemClick
        Me.ShowClientSatisfactionAndPaymentForm()
    End Sub

    Private Sub btnClientSurveyForm_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnClientSurveyForm.ItemClick
        Me.ShowClientSurveyForm()
    End Sub

    Private Sub btnHelp_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHelp.ItemClick
        ShowHelpTopic(ExplorerForm, "JobBookings.html")
    End Sub

    Private Sub btnUndelete_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnUndelete.ItemClick
        List_Undelete()
    End Sub

    Public ReadOnly Property SelectedItemForms() As DevExpress.XtraBars.BarItem Implements ISelectedItemReports.SelectedItemForms
        Get
            Return subFormsOnSelectedItem2
        End Get
    End Property

    Public ReadOnly Property SelectedItemReports() As DevExpress.XtraBars.BarItem Implements ISelectedItemReports.SelectedItemReports
        Get
            Return subReportsOnSelectedItem2
        End Get
    End Property

    Public ReadOnly Property AllowDelete() As Boolean Implements Power.Library.IListControl.AllowDelete
        Get
            Return Not HasRole("branch_read_only")
        End Get
    End Property

    Public ReadOnly Property AllowEdit() As Boolean Implements Power.Library.IListControl.AllowEdit
        Get
            Return True
        End Get
    End Property

    Public ReadOnly Property AllowNew() As Boolean Implements Power.Library.IListControl.AllowNew
        Get
            Return Not HasRole("branch_read_only")
        End Get
    End Property

    Public ReadOnly Property AllowUndelete() As Boolean Implements Power.Library.IListControl.AllowUndelete
        Get
            Return Not HasRole("branch_read_only")
        End Get
    End Property

    Public ReadOnly Property DeleteCaption() As String Implements Power.Library.IListControl.DeleteCaption
        Get
            Return "&Cancel"
        End Get
    End Property

    Public ReadOnly Property UndeleteCaption() As String Implements Power.Library.IListControl.UndeleteCaption
        Get
            Return "&Uncancel"
        End Get
    End Property

    Public Sub SetUpPermissions()
        For Each cmd As CommandAccess In Commands
            If Not Me.BarManager1.Items(cmd.Command) Is Nothing Then  ' This will not fail if the cmd.Command is not found
                Me.BarManager1.Items(cmd.Command).Tag = cmd
                Me.BarManager1.Items(cmd.Command).Enabled = cmd.Allowed
            End If
        Next
    End Sub

End Class
