Imports DevExpress.XtraReports.UI
Imports System.ComponentModel.Design
Imports DevExpress.XtraReports.UserDesigner
Imports DevExpress.XtraReports.UserDesigner.Native

Public Class repGenericJobsMailout
    Inherits Power.Forms.XtraReport

#Region " Component Designer generated code "

    Public Sub New(ByVal Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Designer support
        Container.Add(Me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.
    'Do not modify it using the code editor.
    Friend WithEvents dataSet As WindowsApplication.dsReportsBranchJobsMailMerge
    Friend WithEvents pbLogo As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrRichTextBox1 As DevExpress.XtraReports.UI.XRRichTextBox
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(repGenericJobsMailout))
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo
        Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel
        Me.dataSet = New WindowsApplication.dsReportsBranchJobsMailMerge
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrRichTextBox1 = New DevExpress.XtraReports.UI.XRRichTextBox
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
        Me.pbLogo = New DevExpress.XtraReports.UI.XRPictureBox

        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo1, Me.XrLabel12, Me.XrLabel11, Me.XrRichTextBox1})
        Me.Detail.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Detail.Height = 458
        Me.Detail.Name = "Detail"
        Me.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
        Me.Detail.ParentStyleUsing.UseFont = False
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Location = New System.Drawing.Point(0, 17)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.XrPageInfo1.Size = New System.Drawing.Size(342, 33)
        '
        'XrLabel12
        '
        Me.XrLabel12.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.dataSet, "Job Mail Merge.Customer Address (Multiline)", "")})
        Me.XrLabel12.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.XrLabel12.Location = New System.Drawing.Point(0, 67)
        Me.XrLabel12.Multiline = True
        Me.XrLabel12.Name = "XrLabel12"
        Me.XrLabel12.ParentStyleUsing.UseFont = False
        Me.XrLabel12.Size = New System.Drawing.Size(342, 50)
        Me.XrLabel12.Text = "XrLabel12"
        '
        'dataSet
        '
        Me.dataSet.DataSetName = "dsReportsBranchJobsMailouts"
        Me.dataSet.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'XrLabel11
        '
        Me.XrLabel11.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.dataSet, "Job Mail Merge.Customer Name", "")})
        Me.XrLabel11.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.XrLabel11.Location = New System.Drawing.Point(0, 50)
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.ParentStyleUsing.UseFont = False
        Me.XrLabel11.Size = New System.Drawing.Size(342, 17)
        Me.XrLabel11.Text = "XrLabel11"
        '
        'XrRichTextBox1
        '
        Me.XrRichTextBox1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrRichTextBox1.Location = New System.Drawing.Point(0, 150)
        Me.XrRichTextBox1.Name = "XrRichTextBox1"
        Me.XrRichTextBox1.ParentStyleUsing.UseBackColor = False
        Me.XrRichTextBox1.ParentStyleUsing.UseFont = False
        Me.XrRichTextBox1.RtfText = CType(resources.GetObject("XrRichTextBox1.RtfText"), DevExpress.XtraReports.UI.SerializableString)
        Me.XrRichTextBox1.Size = New System.Drawing.Size(567, 300)
        '
        'XrLabel4
        '
        Me.XrLabel4.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.XrLabel4.ForeColor = System.Drawing.Color.DimGray
        Me.XrLabel4.Location = New System.Drawing.Point(150, 42)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.ParentStyleUsing.UseFont = False
        Me.XrLabel4.ParentStyleUsing.UseForeColor = False
        Me.XrLabel4.Size = New System.Drawing.Size(33, 16)
        Me.XrLabel4.Text = "Fax:"
        '
        'XrLabel15
        '
        Me.XrLabel15.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.dataSet, "Job Mail Merge.Branch Fax", "")})
        Me.XrLabel15.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.XrLabel15.ForeColor = System.Drawing.Color.DimGray
        Me.XrLabel15.Location = New System.Drawing.Point(183, 42)
        Me.XrLabel15.Name = "XrLabel15"
        Me.XrLabel15.ParentStyleUsing.UseFont = False
        Me.XrLabel15.ParentStyleUsing.UseForeColor = False
        Me.XrLabel15.Size = New System.Drawing.Size(175, 17)
        Me.XrLabel15.Text = "XrLabel6"
        '
        'XrLabel14
        '
        Me.XrLabel14.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.XrLabel14.ForeColor = System.Drawing.Color.DimGray
        Me.XrLabel14.Location = New System.Drawing.Point(0, 42)
        Me.XrLabel14.Name = "XrLabel14"
        Me.XrLabel14.ParentStyleUsing.UseFont = False
        Me.XrLabel14.ParentStyleUsing.UseForeColor = False
        Me.XrLabel14.Size = New System.Drawing.Size(50, 16)
        Me.XrLabel14.Text = "Phone:"
        '
        'XrLabel2
        '
        Me.XrLabel2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.dataSet, "Job Mail Merge.Branch Address", "")})
        Me.XrLabel2.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.XrLabel2.ForeColor = System.Drawing.Color.DimGray
        Me.XrLabel2.Location = New System.Drawing.Point(0, 25)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.ParentStyleUsing.UseBorderColor = False
        Me.XrLabel2.ParentStyleUsing.UseFont = False
        Me.XrLabel2.ParentStyleUsing.UseForeColor = False
        Me.XrLabel2.Size = New System.Drawing.Size(358, 17)
        Me.XrLabel2.Text = "XrLabel2"
        '
        'XrLabel1
        '
        Me.XrLabel1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.dataSet, "Job Mail Merge.Branch Business Name", "")})
        Me.XrLabel1.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel1.Location = New System.Drawing.Point(0, 0)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.ParentStyleUsing.UseFont = False
        Me.XrLabel1.Size = New System.Drawing.Size(358, 25)
        Me.XrLabel1.Text = "XrLabel1"
        '
        'XrLabel10
        '
        Me.XrLabel10.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.dataSet, "Job Mail Merge.Branch Email", "")})
        Me.XrLabel10.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.XrLabel10.ForeColor = System.Drawing.Color.DimGray
        Me.XrLabel10.Location = New System.Drawing.Point(50, 58)
        Me.XrLabel10.Name = "XrLabel10"
        Me.XrLabel10.ParentStyleUsing.UseFont = False
        Me.XrLabel10.ParentStyleUsing.UseForeColor = False
        Me.XrLabel10.Size = New System.Drawing.Size(308, 17)
        Me.XrLabel10.Text = "XrLabel10"
        '
        'XrLabel3
        '
        Me.XrLabel3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.dataSet, "Job Mail Merge.Branch Phone", "")})
        Me.XrLabel3.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.XrLabel3.ForeColor = System.Drawing.Color.DimGray
        Me.XrLabel3.Location = New System.Drawing.Point(50, 42)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.ParentStyleUsing.UseFont = False
        Me.XrLabel3.ParentStyleUsing.UseForeColor = False
        Me.XrLabel3.Size = New System.Drawing.Size(100, 17)
        Me.XrLabel3.Text = "XrLabel3"
        '
        'XrLabel9
        '
        Me.XrLabel9.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.XrLabel9.ForeColor = System.Drawing.Color.DimGray
        Me.XrLabel9.Location = New System.Drawing.Point(0, 58)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.ParentStyleUsing.UseFont = False
        Me.XrLabel9.ParentStyleUsing.UseForeColor = False
        Me.XrLabel9.Size = New System.Drawing.Size(50, 16)
        Me.XrLabel9.Text = "Email:"

        '
        'pbLogo
        '
        Me.pbLogo.Image = CType(resources.GetObject("pbLogo.Image"), System.Drawing.Image)
        Me.pbLogo.Location = New System.Drawing.Point(358, 0)
        Me.pbLogo.Name = "pbLogo"
        Me.pbLogo.Size = New System.Drawing.Size(208, 67)

        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel10, Me.XrLabel9, Me.XrLabel15, Me.XrLabel4, Me.XrLabel3, Me.XrLabel14, Me.XrLabel2, Me.pbLogo, Me.XrLabel1})
        Me.PageHeader.Height = 83
        Me.PageHeader.Name = "PageHeader"
        '
        'repGenericJobsMailout
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader})
        Me.DataMember = "Job Mail Merge"
        Me.DataSource = Me.dataSet
        Me.Margins = New System.Drawing.Printing.Margins(130, 130, 50, 100)
        Me.PageHeight = 1169
        Me.PageWidth = 827
        Me.PaperKind = System.Drawing.Printing.PaperKind.A4
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

End Class

