Imports Power.Forms

Public Class lvSecondaryEnquirySources
    Inherits System.Windows.Forms.UserControl
    Implements IListControl, IPrintableControl

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Connection As SqlClient.SqlConnection, ByVal BRID As Integer, ByVal ExplorerForm As frmExplorer)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.Connection = Connection
        Me.BRID = BRID
        hExplorerForm = ExplorerForm
        FillDataSet()
        EnableDisable()
    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents hSqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents DsGTMS As WindowsApplication.dsGTMS
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents colSSName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSSDesc As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents bStandard As DevExpress.XtraBars.Bar
    Friend WithEvents btnDelete As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subReportsOnSelectedItem As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnQuickPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPrintPreview As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnHelp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarAndDockingController1 As DevExpress.XtraBars.BarAndDockingController
    Friend WithEvents ImageListToolBar As System.Windows.Forms.ImageList
    Friend WithEvents btnPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subReportsOnSelectedItem2 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents PopupMenu1 As DevExpress.XtraBars.PopupMenu
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(lvSecondaryEnquirySources))
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.DsGTMS = New WindowsApplication.dsGTMS
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colSSName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSSDesc = New DevExpress.XtraGrid.Columns.GridColumn
        Me.hSqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.BarManager1 = New DevExpress.XtraBars.BarManager
        Me.bStandard = New DevExpress.XtraBars.Bar
        Me.btnDelete = New DevExpress.XtraBars.BarButtonItem
        Me.subReportsOnSelectedItem = New DevExpress.XtraBars.BarSubItem
        Me.btnQuickPrint = New DevExpress.XtraBars.BarButtonItem
        Me.btnPrintPreview = New DevExpress.XtraBars.BarButtonItem
        Me.btnHelp = New DevExpress.XtraBars.BarButtonItem
        Me.BarAndDockingController1 = New DevExpress.XtraBars.BarAndDockingController(Me.components)
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
        Me.ImageListToolBar = New System.Windows.Forms.ImageList(Me.components)
        Me.btnPrint = New DevExpress.XtraBars.BarButtonItem
        Me.subReportsOnSelectedItem2 = New DevExpress.XtraBars.BarSubItem
        Me.PopupMenu1 = New DevExpress.XtraBars.PopupMenu
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.DsGTMS.LeadSubSources
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'GridControl1.EmbeddedNavigator
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.Location = New System.Drawing.Point(0, 24)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(464, 336)
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'DsGTMS
        '
        Me.DsGTMS.DataSetName = "dsGTMS"
        Me.DsGTMS.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'GridView1
        '
        Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
        Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
        Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
        Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
        Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
        Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSSName, Me.colSSDesc})
        Me.GridView1.CustomizationFormBounds = New System.Drawing.Rectangle(1062, 743, 208, 156)
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colSSName, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colSSName
        '
        Me.colSSName.Caption = "Name"
        Me.colSSName.FieldName = "SSName"
        Me.colSSName.Name = "colSSName"
        Me.colSSName.Visible = True
        Me.colSSName.VisibleIndex = 0
        Me.colSSName.Width = 112
        '
        'colSSDesc
        '
        Me.colSSDesc.Caption = "Description"
        Me.colSSDesc.FieldName = "SSDesc"
        Me.colSSDesc.Name = "colSSDesc"
        Me.colSSDesc.Width = 338
        '
        'hSqlConnection
        '
        Me.hSqlConnection.ConnectionString = "workstation id=WORKSTATION3;packet size=4096;integrated security=SSPI;data source" & _
        "=""server\dev"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "LeadSubSources", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("SSID", "SSID"), New System.Data.Common.DataColumnMapping("SSName", "SSName"), New System.Data.Common.DataColumnMapping("SSDesc", "SSDesc")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM LeadSubSources WHERE (BRID = @Original_BRID) AND (SSID = @Original_SS" & _
        "ID) AND (SSDesc = @Original_SSDesc OR @Original_SSDesc IS NULL AND SSDesc IS NUL" & _
        "L) AND (SSName = @Original_SSName OR @Original_SSName IS NULL AND SSName IS NULL" & _
        ")"
        Me.SqlDeleteCommand1.Connection = Me.hSqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SSID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SSDesc", System.Data.SqlDbType.VarChar, 500, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SSDesc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SSName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SSName", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO LeadSubSources(BRID, SSName, SSDesc) VALUES (@BRID, @SSName, @SSDesc)" & _
        "; SELECT BRID, SSID, SSName, SSDesc FROM LeadSubSources WHERE (BRID = @BRID) AND" & _
        " (SSID = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.hSqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SSName", System.Data.SqlDbType.VarChar, 50, "SSName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SSDesc", System.Data.SqlDbType.VarChar, 500, "SSDesc"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, SSID, SSName, SSDesc FROM LeadSubSources WHERE (BRID = @BRID)"
        Me.SqlSelectCommand1.Connection = Me.hSqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE LeadSubSources SET BRID = @BRID, SSName = @SSName, SSDesc = @SSDesc WHERE " & _
        "(BRID = @Original_BRID) AND (SSID = @Original_SSID) AND (SSDesc = @Original_SSDe" & _
        "sc OR @Original_SSDesc IS NULL AND SSDesc IS NULL) AND (SSName = @Original_SSNam" & _
        "e OR @Original_SSName IS NULL AND SSName IS NULL); SELECT BRID, SSID, SSName, SS" & _
        "Desc FROM LeadSubSources WHERE (BRID = @BRID) AND (SSID = @SSID)"
        Me.SqlUpdateCommand1.Connection = Me.hSqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SSName", System.Data.SqlDbType.VarChar, 50, "SSName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SSDesc", System.Data.SqlDbType.VarChar, 500, "SSDesc"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SSID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SSDesc", System.Data.SqlDbType.VarChar, 500, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SSDesc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SSName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SSName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SSID", System.Data.SqlDbType.Int, 4, "SSID"))
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.bStandard})
        Me.BarManager1.Categories.AddRange(New DevExpress.XtraBars.BarManagerCategory() {New DevExpress.XtraBars.BarManagerCategory("Built-in Menus", New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd"), False), New DevExpress.XtraBars.BarManagerCategory("File", New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")), New DevExpress.XtraBars.BarManagerCategory("Reports", New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")), New DevExpress.XtraBars.BarManagerCategory("Forms", New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")), New DevExpress.XtraBars.BarManagerCategory("Help", New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785"))})
        Me.BarManager1.Controller = Me.BarAndDockingController1
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Images = Me.ImageListToolBar
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnDelete, Me.btnHelp, Me.btnPrintPreview, Me.btnQuickPrint, Me.btnPrint, Me.subReportsOnSelectedItem, Me.subReportsOnSelectedItem2})
        Me.BarManager1.MainMenu = Me.bStandard
        Me.BarManager1.MaxItemId = 93
        '
        'bStandard
        '
        Me.bStandard.BarName = "Standard Toolbar"
        Me.bStandard.DockCol = 0
        Me.bStandard.DockRow = 0
        Me.bStandard.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bStandard.FloatLocation = New System.Drawing.Point(44, 188)
        Me.bStandard.FloatSize = New System.Drawing.Size(659, 24)
        Me.bStandard.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnDelete, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(Me.subReportsOnSelectedItem, True), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnQuickPrint, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnPrintPreview, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnHelp, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.bStandard.OptionsBar.AllowQuickCustomization = False
        Me.bStandard.OptionsBar.DisableClose = True
        Me.bStandard.OptionsBar.DrawDragBorder = False
        Me.bStandard.OptionsBar.MultiLine = True
        Me.bStandard.OptionsBar.UseWholeRow = True
        Me.bStandard.Text = "Standard Toolbar"
        '
        'btnDelete
        '
        Me.btnDelete.Caption = "&Delete"
        Me.btnDelete.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnDelete.Id = 23
        Me.btnDelete.ImageIndex = 13
        Me.btnDelete.ImageIndexDisabled = 13
        Me.btnDelete.Name = "btnDelete"
        '
        'subReportsOnSelectedItem
        '
        Me.subReportsOnSelectedItem.Caption = "Reports on Selected Item"
        Me.subReportsOnSelectedItem.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.subReportsOnSelectedItem.Id = 78
        Me.subReportsOnSelectedItem.Name = "subReportsOnSelectedItem"
        Me.subReportsOnSelectedItem.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInCustomizing
        '
        'btnQuickPrint
        '
        Me.btnQuickPrint.Caption = "Print"
        Me.btnQuickPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnQuickPrint.Id = 46
        Me.btnQuickPrint.ImageIndex = 11
        Me.btnQuickPrint.ImageIndexDisabled = 11
        Me.btnQuickPrint.Name = "btnQuickPrint"
        '
        'btnPrintPreview
        '
        Me.btnPrintPreview.Caption = "Print Pre&view"
        Me.btnPrintPreview.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrintPreview.Id = 44
        Me.btnPrintPreview.ImageIndex = 10
        Me.btnPrintPreview.ImageIndexDisabled = 10
        Me.btnPrintPreview.Name = "btnPrintPreview"
        '
        'btnHelp
        '
        Me.btnHelp.Caption = "&Help"
        Me.btnHelp.CategoryGuid = New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")
        Me.btnHelp.Id = 68
        Me.btnHelp.ImageIndex = 4
        Me.btnHelp.ImageIndexDisabled = 4
        Me.btnHelp.Name = "btnHelp"
        '
        'BarAndDockingController1
        '
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseForeColor = True
        Me.BarAndDockingController1.PaintStyleName = "OfficeXP"
        '
        'ImageListToolBar
        '
        Me.ImageListToolBar.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImageListToolBar.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageListToolBar.ImageStream = CType(resources.GetObject("ImageListToolBar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListToolBar.TransparentColor = System.Drawing.Color.Transparent
        '
        'btnPrint
        '
        Me.btnPrint.Caption = "&Print..."
        Me.btnPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrint.Id = 43
        Me.btnPrint.ImageIndex = 11
        Me.btnPrint.ImageIndexDisabled = 11
        Me.btnPrint.Name = "btnPrint"
        '
        'subReportsOnSelectedItem2
        '
        Me.subReportsOnSelectedItem2.Caption = "&Reports on Selected Item"
        Me.subReportsOnSelectedItem2.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subReportsOnSelectedItem2.Id = 87
        Me.subReportsOnSelectedItem2.Name = "subReportsOnSelectedItem2"
        '
        'PopupMenu1
        '
        Me.PopupMenu1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.subReportsOnSelectedItem, True)})
        Me.PopupMenu1.Manager = Me.BarManager1
        Me.PopupMenu1.Name = "PopupMenu1"
        '
        'lvSecondaryEnquirySources
        '
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "lvSecondaryEnquirySources"
        Me.Size = New System.Drawing.Size(464, 360)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return hSqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            hSqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private Sub EnableDisable()
        btnDelete.Enabled = AllowDelete
    End Sub

    Public Function FillDataSet() As Integer Implements IListControl.FillDataSet
        ' Open connection
        Dim trans As SqlClient.SqlTransaction = Connection.BeginTransaction(IsolationLevel.ReadUncommitted)
        Power.Library.Library.ApplyTransactionToAllDataAdapters(trans, Me)
        GridControl1.DataSource.Clear()
        FillDataSet = SqlDataAdapter.Fill(DsGTMS)
        'Close connecton
        trans.Commit()
    End Function

    Private Sub GridView1_RowCountChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.RowCountChanged
        ExplorerForm.UpdateNumItems(NumItems)
        ExplorerForm.UpdateVisibleItems(VisibleItems)
    End Sub

    Public ReadOnly Property VisibleItems() As Integer Implements Power.Library.IListControl.VisibleItems
        Get
            Return GridView1.RowCount
        End Get
    End Property

    Public ReadOnly Property NumItems() As Integer Implements Power.Library.IListControl.NumItems
        Get
            Return DsGTMS.Tables(SqlDataAdapter.TableMappings(0).DataSetTable).Rows.Count
        End Get
    End Property

    Private hBRID As Integer
    Private Property BRID() As Integer
        Get
            Return hBRID
        End Get
        Set(ByVal Value As Integer)
            hBRID = Value
            SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = Value
        End Set
    End Property

    Public ReadOnly Property SelectedRow() As DataRow
        Get
            If Not GridView1.GetSelectedRows Is Nothing Then
                Return GridView1.GetDataRow(GridView1.GetSelectedRows(0))
            End If
        End Get
    End Property

    Public ReadOnly Property SelectedRowField(ByVal Field As String) As Object
        Get
            If Not SelectedRow Is Nothing Then
                Return SelectedRow.Item(Field)
            Else
                Return DBNull.Value
            End If
        End Get
    End Property

    Private hExplorerForm As Form
    Public ReadOnly Property ExplorerForm() As frmExplorer
        Get
            Return hExplorerForm
        End Get
    End Property

#Region " New, Open, Delete "

    Private Sub btnDelete_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnDelete.ItemClick
        List_Delete()
    End Sub

    Public Sub List_Delete() Implements IListControl.List_Delete
        If Not SelectedRow Is Nothing Then
            If Message.AskDeleteObject("secondary enquiry source", Message.ObjectAction.Delete) = MsgBoxResult.Yes Then
                If DataAccess.spExecLockRequest_All("sp_GetLeadLock_BRID", BRID, Connection) Then
                    SelectedRow.Delete()
                    Try
                        SqlDataAdapter.Update(DsGTMS)
                    Catch ex As Exception
                        Me.GridControl1.DataSource.Clear()
                        FillDataSet()
                    End Try
                    DataAccess.spExecLockRequest_All("sp_ReleaseLeadLock_BRID", BRID, Connection)
                Else
                    DevExpress.XtraEditors.XtraMessageBox.Show("Secondary lead sources cannot be deleted while any users have appointments open in the system." & _
                        vbNewLine & vbNewLine & "Please ensure all users close any appointments that are open, and try again.", _
                        "Franchise Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    DsGTMS.RejectChanges()
                End If
            End If
        End If
    End Sub

    Public Sub List_Edit() Implements IListControl.List_Edit

    End Sub

    Public Sub List_New() Implements IListControl.List_New

    End Sub

    Public Sub List_Cancel() Implements Power.Library.IListControl.List_Cancel

    End Sub

    Public Sub List_Uncancel() Implements Power.Library.IListControl.List_Uncancel

    End Sub

#End Region

#Region " Printing "

    Private Sub btnPrint_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrint.ItemClick
        Print(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub Print(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.Print
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.PrintDlg()
    End Sub

    Private Sub btnPrintPreview_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrintPreview.ItemClick
        PrintPreview(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub PrintPreview(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.PrintPreview
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(ExplorerForm.PrintingSystem)
        link.Component = GridControl1
        link.ShowPreview()
    End Sub

    Private Sub btnQuickPrint_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnQuickPrint.ItemClick
        QuickPrint(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub QuickPrint(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.QuickPrint
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.Print(PrintingSystem.PageSettings.PrinterName)
    End Sub

#End Region

    Private Sub GridView1_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles GridView1.InitNewRow
        GridView1.GetDataRow(e.RowHandle)("BRID") = BRID
    End Sub

    Private Sub GridView1_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles GridView1.CellValueChanged
        If HasRole("branch_read_only") Then
            DsGTMS.RejectChanges()
            Exit Sub
        End If
        If GridView1.GetDataRow(e.RowHandle)("SSName") Is DBNull.Value Then
            GridView1.GetDataRow(e.RowHandle).Delete()
        End If
        GridView1.UpdateCurrentRow()
        If DataAccess.spExecLockRequest_All("sp_GetLeadLock_BRID", BRID, Connection) Then
            Try
                SqlDataAdapter.Update(DsGTMS)
            Catch ex As Exception
                Me.GridControl1.DataSource.Clear()
                FillDataSet()
            End Try
            DataAccess.spExecLockRequest_All("sp_ReleaseLeadLock_BRID", BRID, Connection)
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show("Secondary lead sources cannot be added or changed while any users have appointments open in the system." & _
                vbNewLine & vbNewLine & "Please ensure all users close any appointments that are open, and try again.", _
                "Franchise Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            DsGTMS.RejectChanges()
        End If
    End Sub

    Public ReadOnly Property AllowDelete() As Boolean Implements Power.Library.IListControl.AllowDelete
        Get
            Return Not HasRole("branch_read_only")
        End Get
    End Property

    Public ReadOnly Property AllowEdit() As Boolean Implements Power.Library.IListControl.AllowEdit
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property AllowNew() As Boolean Implements Power.Library.IListControl.AllowNew
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property AllowCancel() As Boolean Implements Power.Library.IListControl.AllowCancel
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property AllowUncancel() As Boolean Implements Power.Library.IListControl.AllowUncancel
        Get
            Return False
        End Get
    End Property

    Private Sub btnHelp_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHelp.ItemClick
        ShowHelpTopic(ExplorerForm, "SecondaryEnquirySources.html")
    End Sub
End Class
