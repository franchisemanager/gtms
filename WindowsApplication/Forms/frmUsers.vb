Public Class frmUsers
    Inherits DevExpress.XtraEditors.XtraForm

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Connection As SqlClient.SqlConnection, ByVal BRID As Integer)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.Connection = Connection
        Me.BRID = BRID
        FillDataSet()
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DsHeadOffice As WindowsApplication.dsHeadOffice
    Friend WithEvents colUSName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colUSUsername As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colUSDisabled_Display As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents btnRemoveUser As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEditUser As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAddUser As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dgUsers As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvUsers As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents daUsers As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents btnClose As DevExpress.XtraEditors.SimpleButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmUsers))
        Dim StyleFormatCondition1 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition
        Me.colUSDisabled_Display = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Button1 = New DevExpress.XtraEditors.SimpleButton
        Me.dgUsers = New DevExpress.XtraGrid.GridControl
        Me.DsHeadOffice = New WindowsApplication.dsHeadOffice
        Me.gvUsers = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colUSName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colUSUsername = New DevExpress.XtraGrid.Columns.GridColumn
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.daUsers = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage
        Me.btnRemoveUser = New DevExpress.XtraEditors.SimpleButton
        Me.btnEditUser = New DevExpress.XtraEditors.SimpleButton
        Me.btnAddUser = New DevExpress.XtraEditors.SimpleButton
        Me.btnClose = New DevExpress.XtraEditors.SimpleButton
        CType(Me.dgUsers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsHeadOffice, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvUsers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        Me.SuspendLayout()
        '
        'colUSDisabled_Display
        '
        Me.colUSDisabled_Display.Caption = "Status"
        Me.colUSDisabled_Display.FieldName = "USDisabled_Display"
        Me.colUSDisabled_Display.Name = "colUSDisabled_Display"
        Me.colUSDisabled_Display.Visible = True
        Me.colUSDisabled_Display.VisibleIndex = 2
        Me.colUSDisabled_Display.Width = 315
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.Location = New System.Drawing.Point(8, 400)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(72, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Help"
        '
        'dgUsers
        '
        Me.dgUsers.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgUsers.DataSource = Me.DsHeadOffice.VUsers
        '
        'dgUsers.EmbeddedNavigator
        '
        Me.dgUsers.EmbeddedNavigator.Name = ""
        Me.dgUsers.Location = New System.Drawing.Point(8, 16)
        Me.dgUsers.MainView = Me.gvUsers
        Me.dgUsers.Name = "dgUsers"
        Me.dgUsers.Size = New System.Drawing.Size(584, 304)
        Me.dgUsers.TabIndex = 0
        Me.dgUsers.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvUsers})
        '
        'DsHeadOffice
        '
        Me.DsHeadOffice.DataSetName = "dsHeadOffice"
        Me.DsHeadOffice.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'gvUsers
        '
        Me.gvUsers.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.gvUsers.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvUsers.Appearance.FocusedCell.Options.UseBackColor = True
        Me.gvUsers.Appearance.FocusedCell.Options.UseForeColor = True
        Me.gvUsers.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.gvUsers.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gvUsers.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvUsers.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvUsers.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colUSName, Me.colUSUsername, Me.colUSDisabled_Display})
        Me.gvUsers.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        StyleFormatCondition1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Strikeout, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        StyleFormatCondition1.Appearance.ForeColor = System.Drawing.SystemColors.GrayText
        StyleFormatCondition1.Appearance.Options.UseFont = True
        StyleFormatCondition1.Appearance.Options.UseForeColor = True
        StyleFormatCondition1.ApplyToRow = True
        StyleFormatCondition1.Column = Me.colUSDisabled_Display
        StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal
        StyleFormatCondition1.Value1 = "Disabled"
        Me.gvUsers.FormatConditions.AddRange(New DevExpress.XtraGrid.StyleFormatCondition() {StyleFormatCondition1})
        Me.gvUsers.GridControl = Me.dgUsers
        Me.gvUsers.Name = "gvUsers"
        Me.gvUsers.OptionsBehavior.Editable = False
        Me.gvUsers.OptionsCustomization.AllowFilter = False
        Me.gvUsers.OptionsNavigation.AutoFocusNewRow = True
        Me.gvUsers.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.gvUsers.OptionsView.ShowGroupPanel = False
        Me.gvUsers.OptionsView.ShowHorzLines = False
        Me.gvUsers.OptionsView.ShowIndicator = False
        Me.gvUsers.OptionsView.ShowVertLines = False
        Me.gvUsers.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colUSName, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colUSName
        '
        Me.colUSName.Caption = "User's Name"
        Me.colUSName.FieldName = "USName"
        Me.colUSName.Name = "colUSName"
        Me.colUSName.Visible = True
        Me.colUSName.VisibleIndex = 0
        Me.colUSName.Width = 478
        '
        'colUSUsername
        '
        Me.colUSUsername.Caption = "Program Username"
        Me.colUSUsername.FieldName = "USUsername"
        Me.colUSUsername.Name = "colUSUsername"
        Me.colUSUsername.Visible = True
        Me.colUSUsername.VisibleIndex = 1
        Me.colUSUsername.Width = 314
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;integrated security=SSPI;data source=""SERVER" & _
        "\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'daUsers
        '
        Me.daUsers.DeleteCommand = Me.SqlDeleteCommand1
        Me.daUsers.InsertCommand = Me.SqlInsertCommand1
        Me.daUsers.SelectCommand = Me.SqlSelectCommand1
        Me.daUsers.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VUsers", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("USID", "USID"), New System.Data.Common.DataColumnMapping("USUsername", "USUsername"), New System.Data.Common.DataColumnMapping("USPassword", "USPassword"), New System.Data.Common.DataColumnMapping("USIsHeadOffice", "USIsHeadOffice"), New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("USDisabled", "USDisabled"), New System.Data.Common.DataColumnMapping("USName", "USName")})})
        Me.daUsers.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Users WHERE (USID = @Original_USID) AND (BRID = @Original_BRID OR @Or" & _
        "iginal_BRID IS NULL AND BRID IS NULL) AND (USDisabled = @Original_USDisabled) AN" & _
        "D (USIsHeadOffice = @Original_USIsHeadOffice) AND (USName = @Original_USName OR " & _
        "@Original_USName IS NULL AND USName IS NULL) AND (USPassword = @Original_USPassw" & _
        "ord OR @Original_USPassword IS NULL AND USPassword IS NULL) AND (USUsername = @O" & _
        "riginal_USUsername OR @Original_USUsername IS NULL AND USUsername IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USDisabled", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USDisabled", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USIsHeadOffice", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USIsHeadOffice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USPassword", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USPassword", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USUsername", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USUsername", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Users (USUsername, USPassword, USIsHeadOffice, BRID, USDisabled, USNa" & _
        "me) VALUES (@USUsername, @USPassword, @USIsHeadOffice, @BRID, @USDisabled, @USNa" & _
        "me); SELECT USID, USUsername, USPassword, USIsHeadOffice, BRID, USDisabled, USNa" & _
        "me, USDisabled_Display FROM VUsers WHERE (USID = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USUsername", System.Data.SqlDbType.VarChar, 50, "USUsername"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USPassword", System.Data.SqlDbType.VarChar, 50, "USPassword"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USIsHeadOffice", System.Data.SqlDbType.Bit, 1, "USIsHeadOffice"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USDisabled", System.Data.SqlDbType.Bit, 1, "USDisabled"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USName", System.Data.SqlDbType.VarChar, 50, "USName"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT USID, USUsername, USPassword, USIsHeadOffice, BRID, USDisabled, USName, US" & _
        "Disabled_Display FROM VUsers WHERE (USIsHeadOffice = 0) AND (BRID = @BRID)"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Users SET USUsername = @USUsername, USPassword = @USPassword, USIsHeadOffi" & _
        "ce = @USIsHeadOffice, BRID = @BRID, USDisabled = @USDisabled, USName = @USName W" & _
        "HERE (USID = @Original_USID) AND (BRID = @Original_BRID OR @Original_BRID IS NUL" & _
        "L AND BRID IS NULL) AND (USDisabled = @Original_USDisabled) AND (USIsHeadOffice " & _
        "= @Original_USIsHeadOffice) AND (USName = @Original_USName OR @Original_USName I" & _
        "S NULL AND USName IS NULL) AND (USPassword = @Original_USPassword OR @Original_U" & _
        "SPassword IS NULL AND USPassword IS NULL) AND (USUsername = @Original_USUsername" & _
        " OR @Original_USUsername IS NULL AND USUsername IS NULL); SELECT USID, USUsernam" & _
        "e, USPassword, USIsHeadOffice, BRID, USDisabled, USName, USDisabled_Display FROM" & _
        " VUsers WHERE (USID = @USID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USUsername", System.Data.SqlDbType.VarChar, 50, "USUsername"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USPassword", System.Data.SqlDbType.VarChar, 50, "USPassword"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USIsHeadOffice", System.Data.SqlDbType.Bit, 1, "USIsHeadOffice"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USDisabled", System.Data.SqlDbType.Bit, 1, "USDisabled"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USName", System.Data.SqlDbType.VarChar, 50, "USName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USDisabled", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USDisabled", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USIsHeadOffice", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USIsHeadOffice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USPassword", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USPassword", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USUsername", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USUsername", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USID", System.Data.SqlDbType.Int, 4, "USID"))
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.XtraTabControl1.Location = New System.Drawing.Point(8, 8)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(608, 384)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1})
        Me.XtraTabControl1.Text = "XtraTabControl1"
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.btnRemoveUser)
        Me.XtraTabPage1.Controls.Add(Me.btnEditUser)
        Me.XtraTabPage1.Controls.Add(Me.btnAddUser)
        Me.XtraTabPage1.Controls.Add(Me.dgUsers)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(602, 358)
        Me.XtraTabPage1.Text = "User List"
        '
        'btnRemoveUser
        '
        Me.btnRemoveUser.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveUser.Image = CType(resources.GetObject("btnRemoveUser.Image"), System.Drawing.Image)
        Me.btnRemoveUser.Location = New System.Drawing.Point(168, 328)
        Me.btnRemoveUser.Name = "btnRemoveUser"
        Me.btnRemoveUser.TabIndex = 3
        Me.btnRemoveUser.Text = "Remove"
        '
        'btnEditUser
        '
        Me.btnEditUser.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEditUser.Image = CType(resources.GetObject("btnEditUser.Image"), System.Drawing.Image)
        Me.btnEditUser.Location = New System.Drawing.Point(88, 328)
        Me.btnEditUser.Name = "btnEditUser"
        Me.btnEditUser.TabIndex = 2
        Me.btnEditUser.Text = "Edit..."
        '
        'btnAddUser
        '
        Me.btnAddUser.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddUser.Image = CType(resources.GetObject("btnAddUser.Image"), System.Drawing.Image)
        Me.btnAddUser.Location = New System.Drawing.Point(8, 328)
        Me.btnAddUser.Name = "btnAddUser"
        Me.btnAddUser.TabIndex = 1
        Me.btnAddUser.Text = "Add..."
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(544, 400)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(72, 23)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "Close"
        '
        'frmUsers
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(626, 432)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Controls.Add(Me.Button1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUsers"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Users"
        CType(Me.dgUsers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsHeadOffice, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvUsers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return SqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            SqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnAddUser.Enabled = Not HasRole("branch_read_only") ' Disabled to allow head office users to administer security, but not change data
        btnRemoveUser.Enabled = Not HasRole("branch_read_only") ' Disabled to allow head office users to administer security, but not change data
    End Sub

    Public Function FillDataSet() As Integer
        ' Open connection
        Dim trans As SqlClient.SqlTransaction = Connection.BeginTransaction(IsolationLevel.ReadUncommitted)
        Power.Library.Library.ApplyTransactionToAllDataAdapters(trans, Me)
        'Me.dgUsers.DataSource.Clear()
        FillDataSet = daUsers.Fill(DsHeadOffice)
        'Close connecton
        trans.Commit()
    End Function

    Private hBRID As Integer
    Private Property BRID() As Integer
        Get
            Return hBRID
        End Get
        Set(ByVal Value As Integer)
            hBRID = Value
            daUsers.SelectCommand.Parameters("@BRID").Value = Value
        End Set
    End Property

    Public ReadOnly Property SelectedUser() As DataRow
        Get
            If Not gvUsers.GetSelectedRows Is Nothing Then
                Return gvUsers.GetDataRow(gvUsers.GetSelectedRows(0))
            End If
        End Get
    End Property

    Public Function SelectUser(ByVal USID As Int32) As Boolean
        Dim i As Integer = 0
        Do Until False
            Try
                If gvUsers.GetDataRow(gvUsers.GetVisibleRowHandle(i))("USID") = USID Then
                    gvUsers.ClearSelection()
                    gvUsers.SelectRow(gvUsers.GetVisibleRowHandle(i))
                    Return True
                End If
                i += 1
            Catch ex As NullReferenceException
                Return False
            End Try
        Loop
        Return False
    End Function

    Private Sub dgUsers_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgUsers.DoubleClick
        btnEditUser_Click(sender, e)
    End Sub

    Private Sub btnAddUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddUser.Click
        Dim c As Cursor = Me.Cursor
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        frmUser.Add(False, False, BRID)
        FillDataSet()
        Me.Cursor = c
    End Sub

    Private Sub btnEditUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditUser.Click
        If Not SelectedUser Is Nothing Then
            Dim c As Cursor = Me.Cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If UserExists(SelectedUser.Item("USID")) Then
                frmUser.Edit(False, SelectedUser.Item("USID"))
                FillDataSet()
            Else
                Message.AlreadyDeleted("item", Message.ObjectAction.Edit)
                Me.dgUsers.DataSource.Clear()
                FillDataSet()
            End If
            Me.Cursor = c
        End If
    End Sub

    Private Sub btnRemoveUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveUser.Click
        If Not SelectedUser Is Nothing Then
            If Message.Delete("user") = MsgBoxResult.Yes Then
                If UserExists(SelectedUser.Item("USID")) Then
                    Dim USID As Int64 = SelectedUser.Item("USID")
                    If DataAccess.spExecLockRequest("sp_GetUserLock", SelectedUser.Item("USID"), Connection) Then
                        SelectedUser.Delete()
                        Try
                            daUsers.Update(DsHeadOffice)
                        Catch ex As Exception
                            Me.dgUsers.DataSource.Clear()
                            FillDataSet()
                        End Try
                        DataAccess.spExecLockRequest("sp_ReleaseUserLock", USID, Connection)
                    Else
                        Message.CurrentlyAccessed("item")
                    End If
                Else
                    Me.dgUsers.DataSource.Clear()
                    FillDataSet()
                End If
            End If
        End If
    End Sub

    Private Function UserExists(ByVal USID As Integer) As Boolean
        Dim cnn As SqlClient.SqlConnection = Connection
        Dim cmd As New SqlClient.SqlCommand("SELECT Count(USID) FROM Users WHERE USID = @USID", cnn)
        cmd.CommandType = CommandType.Text
        cmd.Parameters.Add("@USID", USID)
        Return cmd.ExecuteScalar > 0
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ShowHelpTopic(Me, "BranchUsersTerms.html")
    End Sub

End Class

