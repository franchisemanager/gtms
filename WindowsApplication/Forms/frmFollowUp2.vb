Public Class frmFollowUp2
    Inherits DevExpress.XtraEditors.XtraForm

    Private DataRow As DataRow

    Public Shared Sub Edit(ByVal row As DataRow)
        Dim gui As New frmFollowUp2

        With gui
            CopyDataTable(row.Table.DataSet, .dataSet, "VExpenses")
            .dataSet.VExpenses.AcceptChanges()

            CopyDataTable(row.Table.DataSet, .dataSet, "INFollowUpTypes")
            .dataSet.INFollowUpTypes.AcceptChanges()

            .DataRow = .dataSet.Tables(row.Table.TableName).Rows.Add(row.ItemArray)
            .dataSet.AcceptChanges()

            If gui.ShowDialog = .DialogResult.OK Then
                UpdateOriginalRow(.DataRow, row)
            End If
        End With
    End Sub

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents btnHelp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmHistory As System.Windows.Forms.ContextMenu
    Friend WithEvents miClearHistory As System.Windows.Forms.MenuItem
    Friend WithEvents dataSet As WindowsApplication.dsNotes
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents dvExpenses As System.Data.DataView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtINFollowUpDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents txtEXIDFollowUp As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents txtINFollowUpType As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents chkINFollowUpComplete As DevExpress.XtraEditors.CheckEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmFollowUp2))
        Me.dataSet = New WindowsApplication.dsNotes
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.cmHistory = New System.Windows.Forms.ContextMenu
        Me.miClearHistory = New System.Windows.Forms.MenuItem
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.btnHelp = New DevExpress.XtraEditors.SimpleButton
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage
        Me.chkINFollowUpComplete = New DevExpress.XtraEditors.CheckEdit
        Me.txtEXIDFollowUp = New DevExpress.XtraEditors.LookUpEdit
        Me.dvExpenses = New System.Data.DataView
        Me.txtINFollowUpDate = New DevExpress.XtraEditors.DateEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtINFollowUpType = New DevExpress.XtraEditors.LookUpEdit
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.chkINFollowUpComplete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEXIDFollowUp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvExpenses, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtINFollowUpDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtINFollowUpType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dataSet
        '
        Me.dataSet.DataSetName = "dsNotes"
        Me.dataSet.EnforceConstraints = False
        Me.dataSet.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=WORKSTATION3;packet size=4096;integrated security=SSPI;data source" & _
        "=""server\dev"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;integrated security=SSPI;data source=SERVER;" & _
        "persist security info=False;initial catalog=GTMS_DEV"
        '
        'cmHistory
        '
        Me.cmHistory.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.miClearHistory})
        '
        'miClearHistory
        '
        Me.miClearHistory.Index = 0
        Me.miClearHistory.Text = "Clear History"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(256, 192)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(176, 192)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "OK"
        '
        'btnHelp
        '
        Me.btnHelp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnHelp.Image = CType(resources.GetObject("btnHelp.Image"), System.Drawing.Image)
        Me.btnHelp.Location = New System.Drawing.Point(8, 192)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(72, 24)
        Me.btnHelp.TabIndex = 1
        Me.btnHelp.Text = "Help"
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.chkINFollowUpComplete)
        Me.XtraTabPage1.Controls.Add(Me.txtEXIDFollowUp)
        Me.XtraTabPage1.Controls.Add(Me.txtINFollowUpDate)
        Me.XtraTabPage1.Controls.Add(Me.Label3)
        Me.XtraTabPage1.Controls.Add(Me.Label2)
        Me.XtraTabPage1.Controls.Add(Me.Label1)
        Me.XtraTabPage1.Controls.Add(Me.txtINFollowUpType)
        Me.XtraTabPage1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(311, 146)
        Me.XtraTabPage1.Text = "Follow Up Details"
        '
        'chkINFollowUpComplete
        '
        Me.chkINFollowUpComplete.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "IDNotes.INFollowUpComplete"))
        Me.chkINFollowUpComplete.Location = New System.Drawing.Point(8, 120)
        Me.chkINFollowUpComplete.Name = "chkINFollowUpComplete"
        '
        'chkINFollowUpComplete.Properties
        '
        Me.chkINFollowUpComplete.Properties.Caption = "Follow up completed"
        Me.chkINFollowUpComplete.Size = New System.Drawing.Size(128, 19)
        Me.chkINFollowUpComplete.TabIndex = 6
        '
        'txtEXIDFollowUp
        '
        Me.txtEXIDFollowUp.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtEXIDFollowUp.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "IDNotes.EXIDFollowUp"))
        Me.txtEXIDFollowUp.Location = New System.Drawing.Point(120, 88)
        Me.txtEXIDFollowUp.Name = "txtEXIDFollowUp"
        '
        'txtEXIDFollowUp.Properties
        '
        Me.txtEXIDFollowUp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtEXIDFollowUp.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXName")})
        Me.txtEXIDFollowUp.Properties.DataSource = Me.dvExpenses
        Me.txtEXIDFollowUp.Properties.DisplayMember = "EXName"
        Me.txtEXIDFollowUp.Properties.NullText = ""
        Me.txtEXIDFollowUp.Properties.ShowFooter = False
        Me.txtEXIDFollowUp.Properties.ShowHeader = False
        Me.txtEXIDFollowUp.Properties.ValueMember = "EXID"
        Me.txtEXIDFollowUp.Size = New System.Drawing.Size(181, 20)
        Me.txtEXIDFollowUp.TabIndex = 5
        '
        'dvExpenses
        '
        Me.dvExpenses.RowFilter = "EGType <> 'OE'"
        Me.dvExpenses.Sort = "EXName"
        Me.dvExpenses.Table = Me.dataSet.VExpenses
        '
        'txtINFollowUpDate
        '
        Me.txtINFollowUpDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtINFollowUpDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "IDNotes.INFollowUpDate"))
        Me.txtINFollowUpDate.EditValue = Nothing
        Me.txtINFollowUpDate.Location = New System.Drawing.Point(120, 56)
        Me.txtINFollowUpDate.Name = "txtINFollowUpDate"
        '
        'txtINFollowUpDate.Properties
        '
        Me.txtINFollowUpDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtINFollowUpDate.Size = New System.Drawing.Size(181, 20)
        Me.txtINFollowUpDate.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(8, 88)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(112, 20)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Assigned to:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(8, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(112, 20)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Date:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(112, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Follow up type:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtINFollowUpType
        '
        Me.txtINFollowUpType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtINFollowUpType.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "IDNotes.INFollowUpType"))
        Me.txtINFollowUpType.Location = New System.Drawing.Point(120, 24)
        Me.txtINFollowUpType.Name = "txtINFollowUpType"
        '
        'txtINFollowUpType.Properties
        '
        Me.txtINFollowUpType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtINFollowUpType.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("INFollowUpTypeDesc")})
        Me.txtINFollowUpType.Properties.DataSource = Me.dataSet.INFollowUpTypes
        Me.txtINFollowUpType.Properties.DisplayMember = "INFollowUpTypeDesc"
        Me.txtINFollowUpType.Properties.ShowFooter = False
        Me.txtINFollowUpType.Properties.ShowHeader = False
        Me.txtINFollowUpType.Properties.ValueMember = "INFollowUpType"
        Me.txtINFollowUpType.Size = New System.Drawing.Size(181, 20)
        Me.txtINFollowUpType.TabIndex = 1
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.XtraTabControl1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XtraTabControl1.Location = New System.Drawing.Point(8, 8)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(320, 176)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1})
        Me.XtraTabControl1.Text = "Appliance Details"
        '
        'frmFollowUp2
        '
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(338, 224)
        Me.Controls.Add(Me.btnHelp)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFollowUp2"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Follow Up"
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.chkINFollowUpComplete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEXIDFollowUp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvExpenses, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtINFollowUpDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtINFollowUpType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
    End Sub

    Private Sub EnableDisable()
        Me.txtINFollowUpDate.Enabled = Not txtINFollowUpType.EditValue = "DF"
        Me.txtEXIDFollowUp.Enabled = Not txtINFollowUpType.EditValue = "DF"
        Me.chkINFollowUpComplete.Enabled = Not txtINFollowUpType.EditValue = "DF"
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        DataRow.EndEdit()
        If ValidateForm() Then
            UpdateINFollowUpText()
            OK = True
            Me.Close()
        End If
    End Sub

    Private Function ValidateForm() As Boolean
        If txtINFollowUpType.EditValue Is DBNull.Value Then
            DevExpress.XtraEditors.XtraMessageBox.Show("You must select a follow up type.", "Franchise Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtINFollowUpType.Focus()
            Return False
        End If
        If txtINFollowUpType.EditValue <> "DF" Then
            If txtINFollowUpDate.EditValue Is DBNull.Value Then
                DevExpress.XtraEditors.XtraMessageBox.Show("You must select a date.", "Franchise Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtEXIDFollowUp.Focus()
                Return False
            End If
        End If
        Return True
    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            DataRow.EndEdit()
            If dataSet.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If Not response = MsgBoxResult.Yes Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub txtNIPrice_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs)
        Format.Decimal_ParseEditValue(sender, e)
    End Sub

    Private Sub Date_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Library.DateEdit_Validating(sender, e)
    End Sub

    Private Sub UpdateINFollowUpText()
        Select Case DataRow("INFollowUpType")
            Case "DF"
                DataRow("INFollowUpText") = dataSet.INFollowUpTypes.FindByINFollowUpType(DataRow("INFollowUpType"))("INFollowUpTypeDesc")
            Case "FB"
                If DataRow("INFollowUpComplete") Then
                    DataRow("INFollowUpText") = "Follow up completed"
                Else
                    DataRow("INFollowUpText") = dataSet.INFollowUpTypes.FindByINFollowUpType(DataRow("INFollowUpType"))("INFollowUpTypeDesc") & " " & _
                        CDate(DataRow("INFollowUpDate")).ToLongDateString
                End If
                If Not DataRow("EXIDFollowUp") Is DBNull.Value Then
                    If Not dataSet.VExpenses.FindByBRIDEXID(DataRow("BRID"), DataRow("EXIDFollowUp")) Is Nothing Then
                        DataRow("INFollowUpText") = DataRow("INFollowUpText") & " (" & _
                            dataSet.VExpenses.FindByBRIDEXID(DataRow("BRID"), DataRow("EXIDFollowUp"))("EXName") & ")"
                    End If
                End If
            Case "FO"
                    If DataRow("INFollowUpComplete") Then
                        DataRow("INFollowUpText") = "Follow up completed"
                    Else
                        DataRow("INFollowUpText") = dataSet.INFollowUpTypes.FindByINFollowUpType(DataRow("INFollowUpType"))("INFollowUpTypeDesc") & " " & _
                            CDate(DataRow("INFollowUpDate")).ToLongDateString
                    End If
                If Not DataRow("EXIDFollowUp") Is DBNull.Value Then
                    If Not dataSet.VExpenses.FindByBRIDEXID(DataRow("BRID"), DataRow("EXIDFollowUp")) Is Nothing Then
                        DataRow("INFollowUpText") = DataRow("INFollowUpText") & " (" & _
                            dataSet.VExpenses.FindByBRIDEXID(DataRow("BRID"), DataRow("EXIDFollowUp"))("EXName") & ")"
                    End If
                End If
        End Select
    End Sub

    Private Sub txtINFollowUpType_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtINFollowUpType.EditValueChanged
        EnableDisable()
    End Sub

    Private Sub txtINFollowUpDate_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtINFollowUpDate.Validating
        Library.DateEdit_Validating(sender, e)
    End Sub
End Class
