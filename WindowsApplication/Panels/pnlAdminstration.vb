Public Class pnlAdminstration
    Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents LinkLabel4 As System.Windows.Forms.LinkLabel
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents PictureEdit1 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Panel11 As System.Windows.Forms.Panel
    Friend WithEvents Panel12 As System.Windows.Forms.Panel
    Friend WithEvents Panel13 As System.Windows.Forms.Panel
    Friend WithEvents Panel14 As System.Windows.Forms.Panel
    Friend WithEvents Panel15 As System.Windows.Forms.Panel
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(pnlAdminstration))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel
        Me.Panel15 = New System.Windows.Forms.Panel
        Me.Label14 = New System.Windows.Forms.Label
        Me.Panel14 = New System.Windows.Forms.Panel
        Me.Label13 = New System.Windows.Forms.Label
        Me.Panel13 = New System.Windows.Forms.Panel
        Me.Label12 = New System.Windows.Forms.Label
        Me.Panel12 = New System.Windows.Forms.Panel
        Me.Label11 = New System.Windows.Forms.Label
        Me.Panel11 = New System.Windows.Forms.Panel
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Panel10 = New System.Windows.Forms.Panel
        Me.LinkLabel4 = New System.Windows.Forms.LinkLabel
        Me.Panel9 = New System.Windows.Forms.Panel
        Me.Label18 = New System.Windows.Forms.Label
        Me.Panel8 = New System.Windows.Forms.Panel
        Me.Label16 = New System.Windows.Forms.Label
        Me.Panel7 = New System.Windows.Forms.Panel
        Me.Label6 = New System.Windows.Forms.Label
        Me.Panel6 = New System.Windows.Forms.Panel
        Me.Label19 = New System.Windows.Forms.Label
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.PictureEdit1 = New DevExpress.XtraEditors.PictureEdit
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.AutoScroll = True
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.LinkLabel1)
        Me.Panel1.Controls.Add(Me.Panel15)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.Panel14)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.Panel13)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.Panel12)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.Panel11)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.Label15)
        Me.Panel1.Controls.Add(Me.Panel10)
        Me.Panel1.Controls.Add(Me.LinkLabel4)
        Me.Panel1.Controls.Add(Me.Panel9)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Controls.Add(Me.Panel8)
        Me.Panel1.Controls.Add(Me.Label16)
        Me.Panel1.Controls.Add(Me.Panel7)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Panel6)
        Me.Panel1.Controls.Add(Me.Label19)
        Me.Panel1.Controls.Add(Me.Panel5)
        Me.Panel1.Controls.Add(Me.Label20)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Panel4)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.DockPadding.Left = 8
        Me.Panel1.DockPadding.Right = 24
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(624, 800)
        Me.Panel1.TabIndex = 7
        '
        'LinkLabel1
        '
        Me.LinkLabel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.LinkLabel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel1.Location = New System.Drawing.Point(56, 808)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(523, 24)
        Me.LinkLabel1.TabIndex = 14
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Click here for more help with Bookings"
        '
        'Panel15
        '
        Me.Panel15.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel15.Location = New System.Drawing.Point(56, 792)
        Me.Panel15.Name = "Panel15"
        Me.Panel15.Size = New System.Drawing.Size(523, 16)
        Me.Panel15.TabIndex = 38
        '
        'Label14
        '
        Me.Label14.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label14.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(56, 760)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(523, 32)
        Me.Label14.TabIndex = 12
        Me.Label14.Text = "Once a job is booked, the relevant details of the job are automatically transferr" & _
        "ed to the Accounting section, the job will take on an ""Uninvoiced"" status in the" & _
        " Accounting section. "
        '
        'Panel14
        '
        Me.Panel14.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel14.Location = New System.Drawing.Point(56, 744)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(523, 16)
        Me.Panel14.TabIndex = 37
        '
        'Label13
        '
        Me.Label13.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label13.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(56, 704)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(523, 40)
        Me.Label13.TabIndex = 12
        Me.Label13.Text = "Changes to bookings can be easily made by selecting the job from the job list and" & _
        " clicking Edit. All changes made to bookings will be documented in a report whic" & _
        "h can be viewed and printed."
        '
        'Panel13
        '
        Me.Panel13.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel13.Location = New System.Drawing.Point(56, 688)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(523, 16)
        Me.Panel13.TabIndex = 36
        '
        'Label12
        '
        Me.Label12.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label12.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(56, 656)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(523, 32)
        Me.Label12.TabIndex = 12
        Me.Label12.Text = "Once a job is booked, then it is up to us to have everything measured, ordered an" & _
        "d ready so that the job takes place on the days that were allocated. "
        '
        'Panel12
        '
        Me.Panel12.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel12.Location = New System.Drawing.Point(56, 640)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(523, 16)
        Me.Panel12.TabIndex = 35
        '
        'Label11
        '
        Me.Label11.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label11.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(56, 584)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(523, 56)
        Me.Label11.TabIndex = 12
        Me.Label11.Text = "To book a job select the appointment and click ""Book as Job"" button. You will nee" & _
        "d to select a date/s and an installer. Other information such as other trades, a" & _
        "ppliances etc are also entered at this stage. It is important that jobs are book" & _
        "ed and entered into the system as soon as they are confirmed.  "
        '
        'Panel11
        '
        Me.Panel11.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel11.Location = New System.Drawing.Point(56, 568)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(523, 16)
        Me.Panel11.TabIndex = 34
        '
        'Label10
        '
        Me.Label10.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label10.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(56, 528)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(523, 40)
        Me.Label10.TabIndex = 12
        Me.Label10.Text = "Generally bookings will be entered by the administration staff. Training will be " & _
        "required so that the correct number of days is allocated to a job, and enough ti" & _
        "me is allowed for measuring and ordering.  "
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.Color.White
        Me.Label15.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label15.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.FromArgb(CType(0, Byte), CType(51, Byte), CType(153, Byte))
        Me.Label15.Location = New System.Drawing.Point(56, 504)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(523, 24)
        Me.Label15.TabIndex = 21
        Me.Label15.Text = "Bookings"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Panel10
        '
        Me.Panel10.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel10.Location = New System.Drawing.Point(56, 488)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(523, 16)
        Me.Panel10.TabIndex = 33
        '
        'LinkLabel4
        '
        Me.LinkLabel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.LinkLabel4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel4.Location = New System.Drawing.Point(56, 464)
        Me.LinkLabel4.Name = "LinkLabel4"
        Me.LinkLabel4.Size = New System.Drawing.Size(523, 24)
        Me.LinkLabel4.TabIndex = 15
        Me.LinkLabel4.TabStop = True
        Me.LinkLabel4.Text = "Click here for more help with Appointments"
        '
        'Panel9
        '
        Me.Panel9.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel9.Location = New System.Drawing.Point(56, 448)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(523, 16)
        Me.Panel9.TabIndex = 32
        '
        'Label18
        '
        Me.Label18.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label18.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(56, 400)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(523, 48)
        Me.Label18.TabIndex = 12
        Me.Label18.Text = "Changes to appointments can be easily made by selecting the appointment from the " & _
        "Appointments list and clicking Edit. All changes made to appointments will be do" & _
        "cumented in a report which can be viewed and printed."
        '
        'Panel8
        '
        Me.Panel8.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel8.Location = New System.Drawing.Point(56, 384)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(523, 16)
        Me.Panel8.TabIndex = 31
        '
        'Label16
        '
        Me.Label16.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label16.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(56, 336)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(523, 48)
        Me.Label16.TabIndex = 12
        Me.Label16.Text = "It is important that appointments are entered into the system by the administrati" & _
        "on staff as soon as they are received. It is just as important that where a date" & _
        ", time and a salesperson are to be allocated to this appointment that these thin" & _
        "gs are done ""on the spot"".  "
        '
        'Panel7
        '
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel7.Location = New System.Drawing.Point(56, 320)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(523, 16)
        Me.Panel7.TabIndex = 30
        '
        'Label6
        '
        Me.Label6.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(56, 240)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(523, 80)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "An appointment is required  in order to produce the quotation.  Generally appoint" & _
        "ments are booked in to be completed by a salesperson on site with the client. So" & _
        "metimes however we are faxed or emailed information to quote on and theorectical" & _
        "ly the appointment is made at the time we received the fax or email. When an app" & _
        "ointment is entered into the system it is given an ID number, and other informat" & _
        "ion about the appointment is entered on the appointments screen.  "
        '
        'Panel6
        '
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel6.Location = New System.Drawing.Point(56, 224)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(523, 16)
        Me.Panel6.TabIndex = 29
        '
        'Label19
        '
        Me.Label19.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label19.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(56, 184)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(523, 40)
        Me.Label19.TabIndex = 12
        Me.Label19.Text = "It should be noted that this part of the system only deals with appointments. An " & _
        "enquiry or a lead that does not result in an appointment means nothing to your b" & _
        "usiness and is therefore not entered. "
        '
        'Panel5
        '
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel5.Location = New System.Drawing.Point(56, 168)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(523, 16)
        Me.Panel5.TabIndex = 28
        '
        'Label20
        '
        Me.Label20.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label20.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(56, 128)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(523, 40)
        Me.Label20.TabIndex = 12
        Me.Label20.Text = "It is important that the information you enter in this section is complete and ac" & _
        "curate. The informationn you enter here will be used later on in the system, and" & _
        " it will be used in compiling reports."
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(0, Byte), CType(51, Byte), CType(153, Byte))
        Me.Label1.Location = New System.Drawing.Point(56, 104)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(523, 24)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Appointments"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Panel4
        '
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel4.Location = New System.Drawing.Point(56, 88)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(523, 16)
        Me.Panel4.TabIndex = 27
        '
        'Label2
        '
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(56, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(523, 56)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "This section of the program is for recording administration information about app" & _
        "ointments and job bookings. Below is some information you should note before ent" & _
        "ering data into these sections. In a small business these functions will probabl" & _
        "y be performed by the one person. In a larger business more people will probably" & _
        " be involved.  "
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.Color.White
        Me.Label17.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label17.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.FromArgb(CType(0, Byte), CType(51, Byte), CType(153, Byte))
        Me.Label17.Location = New System.Drawing.Point(56, 8)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(523, 24)
        Me.Label17.TabIndex = 21
        Me.Label17.Text = "Administration"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Panel3
        '
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel3.Location = New System.Drawing.Point(56, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(523, 8)
        Me.Panel3.TabIndex = 26
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.PictureEdit1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel2.Location = New System.Drawing.Point(8, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(48, 832)
        Me.Panel2.TabIndex = 22
        Me.Panel2.Visible = False
        '
        'PictureEdit1
        '
        Me.PictureEdit1.EditValue = CType(resources.GetObject("PictureEdit1.EditValue"), Object)
        Me.PictureEdit1.Location = New System.Drawing.Point(8, 8)
        Me.PictureEdit1.Name = "PictureEdit1"
        '
        'PictureEdit1.Properties
        '
        Me.PictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit1.Size = New System.Drawing.Size(32, 32)
        Me.PictureEdit1.TabIndex = 16
        '
        'pnlAdminstration
        '
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.Controls.Add(Me.Panel1)
        Me.Name = "pnlAdminstration"
        Me.Size = New System.Drawing.Size(624, 800)
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Panel1_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Panel1.Resize
        VerticalResizeLabels(Panel1)
    End Sub

End Class
