Imports System
Imports System.Reflection

Module Main

    Public ConnectionString As String

    ' Login permissions
    Public IsHeadOffice As Boolean
    Public BRID As Integer
    Public Roles As ArrayList
    Public Commands As ArrayList

    Sub Main()
        ' ConnectionString is in the Main module
        'ConnectionString = Replace(Replace(System.Configuration.ConfigurationSettings.AppSettings.Get("SqlConnection.ConnectionString"),
        '     "{user}", "GTMSAPP"), "{pass}", "granite42")

        'ConnectionString = Replace(Replace(System.Configuration.ConfigurationSettings.AppSettings.Get("SqlConnection.ConnectionString"), "{user}", "lizsmith"), "{pass}", "SmithLiz1234")

        ConnectionString = Replace(Replace(System.Configuration.ConfigurationSettings.AppSettings.Get("SqlConnection.ConnectionString"),
             "{user}", "sa"), "{pass}", "vss2@17")

        If Login() Then Application.Run()
    End Sub

    Public Function UserAppDataPath()
        Return Application.UserAppDataPath & "\.."
    End Function

    Public Function UserAppDataPathWithVerson()
        Return Application.UserAppDataPath
    End Function

    Public Function Login() As Boolean
        If ShowLogin(IsHeadOffice, BRID, Roles, Commands) Then
            If IsHeadOffice Then
                Dim gui As New frmExplorerHeadOffice
                gui.Show()
                gui.SetUpScreen()
            Else
                Dim gui As New frmExplorer(BRID)
                gui.WindowState = FormWindowState.Maximized
                gui.Show()
                gui.SetUpScreen()
            End If
            Return True
        Else
            Return False
        End If
    End Function

    Private Function ShowLogin(ByRef IsHeadOffice As Boolean, ByRef BRID As Integer, ByRef Roles As ArrayList, ByRef Commands As ArrayList, Optional ByVal SplashScreen As frmSplash = Nothing) As Boolean
        Dim ConnectionSuccessful As Boolean
        Dim gui As New frmLogin(ConnectionSuccessful, SplashScreen)
        If Not ConnectionSuccessful Then GoTo ConnectionFailed

        If gui.ShowDialog = DialogResult.OK Then
            Dim USID As Integer
            If sp_Login(gui.txtUsername.EditValue, gui.txtPassword.EditValue, gui.chkUSIsHeadOffice.EditValue, _
                    IsNull(gui.txtBRID.EditValue, 0), USID, gui.SqlConnection) Then
                IsHeadOffice = gui.chkUSIsHeadOffice.EditValue
                BRID = IsNull(gui.txtBRID.EditValue, 0)
                Roles = sp_GetRoles(USID, gui.SqlConnection)
                Commands = sp_GetRoleCommands(USID, gui.SqlConnection)
            Else
                GoTo LoginIncorrect
            End If
            ShowLogin = True
        Else
            ShowLogin = False
        End If


        Exit Function

ConnectionFailed:
        Message.ShowMessage("Could not establish connection.  Please contact your system administrator.", MessageBoxIcon.Exclamation)
        Exit Function

LoginIncorrect:
        gui.SqlConnection.Close()
        Message.ShowMessage("Incorrect login! Please verify your username, password and branch.", MessageBoxIcon.Exclamation)
        ShowLogin = ShowLogin(IsHeadOffice, BRID, Roles, Commands, SplashScreen)
        Exit Function
    End Function

    Public Function HasRole(ByVal Role As String) As Boolean
        For Each r As String In Roles
            If r.Equals(Role) Then
                Return True
            End If
        Next
        Return False
    End Function

    Public Function HasCommand(ByVal Command As String) As Boolean
        For Each c As CommandAccess In Commands
            If c.Command.Equals(Command) And c.Allowed Then
                Return True
            End If
        Next
        Return False
    End Function

    Public Sub ShowHelp(ByVal parentControl As System.Windows.Forms.Control)
        Help.ShowHelp(parentControl, Application.StartupPath & "\FranchiseManagerHelp.chm")
    End Sub

    Public Sub ShowHelpTopic(ByVal parentControl As System.Windows.Forms.Control, ByVal topic As String)
        Help.ShowHelp(parentControl, Application.StartupPath & "\FranchiseManagerHelp.chm", HelpNavigator.Topic, topic)
    End Sub

End Module

Public Module Message

    Public Function AskCancelChanges() As Windows.Forms.DialogResult
        Return DevExpress.XtraEditors.XtraMessageBox.Show("Your changes will be lost, are you sure you wish to cancel?", _
                    "Franchise Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)
    End Function

    Public Function AskDeleteObject(ByVal obj As String, ByVal action As Message.ObjectAction) As Windows.Forms.DialogResult
        Select Case action
            Case ObjectAction.Cancel
                Return DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you want to cancel this " & obj.ToLower & "?", _
                            "Franchise Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)
            Case ObjectAction.Delete
                Return DevExpress.XtraEditors.XtraMessageBox.Show("You are about to delete this " & obj & ".  If this " & obj & " is used used in any reporting," & _
                            vbNewLine & "this will delete all related data in the system and may effect past figures.  This operation cannot be undone." & _
                            vbNewLine & vbNewLine & "Are you sure you want to delete this " & obj.ToLower & "?", _
                            "Franchise Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)
            Case ObjectAction.Discontinue
                Return DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you want to discontinue this " & obj.ToLower & "?", _
                            "Franchise Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)
        End Select
    End Function

    Public Sub ShowMessage_AccessDenied()
        Message.ShowMessage("You do not have correct permissions to access to this function.", MessageBoxIcon.Exclamation)
    End Sub

    Public Sub ShowMessage_MailoutTooLarge(ByVal maxCustomers As Integer)
        Message.ShowMessage("You cannot do a mail merge on more than " & maxCustomers & " customers.", MessageBoxIcon.Exclamation)
    End Sub

    Public Sub ShowMessage(ByVal text As String, ByVal icon As System.Windows.Forms.MessageBoxIcon)
        DevExpress.XtraEditors.XtraMessageBox.Show(text, "Franchise Manager", MessageBoxButtons.OK, icon)
    End Sub

    Public Sub ShowMessage_DateInSearchRow()
        Message.ShowMessage("You cannot do a Mail Merge with a date in the search row.  Please remove this date and try again.", MessageBoxIcon.Exclamation)
    End Sub

    Public Sub ShowMessage_NumberInSearchRow()
        Message.ShowMessage("You cannot do a Mail Merge with a number (or currency) in the search row.  Please remove this number (or currency) and try again.", MessageBoxIcon.Exclamation)
    End Sub

    Public Function AskCopyContactDetails(ByVal obj As String) As System.Windows.Forms.DialogResult
        Return Message.AskQuestion("The details have changed for this contact.  By selecting this contact again, you will update the customer information for this " & obj & " with the latest details for this contact.  This will update the contact details for staff calendars and associated reports and may also affect the job address for this " & obj & "." & _
                    vbNewLine & vbNewLine & "Do you wish to update this " & obj & " with the latest details for this contact?")
    End Function

    Public Function AskQuestion(ByVal text As String) As System.Windows.Forms.DialogResult
        Return DevExpress.XtraEditors.XtraMessageBox.Show(text, "Franchise Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
    End Function

    Public Function AppointmentNotCurrentObject(ByVal obj As String, ByVal action As Message.ObjectAction) As Windows.Forms.DialogResult
        Select Case action
            Case ObjectAction.Edit
                Return DevExpress.XtraEditors.XtraMessageBox.Show("This appointment does not belong to this " & obj.ToLower & _
                    ".  Are you sure you want to edit this appointment?", _
                    "Franchise Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
            Case ObjectAction.Delete
                Return DevExpress.XtraEditors.XtraMessageBox.Show("This appointment does not belong to this " & obj.ToLower & _
                    ".  Are you sure you want to delete this appointment?", _
                    "Franchise Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
        End Select
    End Function

    Public Function Delete(ByVal obj As String) As Windows.Forms.DialogResult
        Return DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you want to delete this " & obj.ToLower & "?", _
                    "Franchise Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)
    End Function

    Public Sub CurrentlyAccessed(ByVal obj As String)
        Message.ShowMessage("This " & obj.ToLower & " is currently being accessed by another user.", MessageBoxIcon.Exclamation)
    End Sub

    Public Sub AlreadyDeleted(ByVal obj As String, ByVal action As Message.ObjectAction)
        Select Case action
            Case ObjectAction.Cancel
                Message.ShowMessage("This " & obj.ToLower & " has been deleted by another user and cannot be cancelled.", MessageBoxIcon.Exclamation)
            Case ObjectAction.Delete
                Message.ShowMessage("This " & obj.ToLower & " has been deleted by another user and cannot be deleted.", MessageBoxIcon.Exclamation)
            Case ObjectAction.Discontinue
                Message.ShowMessage("This " & obj.ToLower & " has been deleted by another user and cannot be discontinued.", MessageBoxIcon.Exclamation)
            Case ObjectAction.Edit
                Message.ShowMessage("This " & obj.ToLower & " has been deleted by another user and cannot be edited.", MessageBoxIcon.Exclamation)
        End Select

    End Sub

    Public Sub SelectFromList(ByVal itemName As String)
        Message.ShowMessage("You cannot add this item. You may however select from the list of " & itemName.ToLower & ".", MsgBoxStyle.Information)
    End Sub

    Public Sub AlreadyExistsInJob(ByVal itemName As String)
        Message.ShowMessage(itemName & " could not be added, as it already exists in the job.", MsgBoxStyle.Information)
    End Sub

    Public Function SaveChangesForJobFigures() As Windows.Forms.DialogResult
        Return DevExpress.XtraEditors.XtraMessageBox.Show("Changes have been made to your job.  To reflect these in your job price summary you must save your changes." & _
                    vbNewLine & vbNewLine & "Do you want to save your changes now?", _
                    "Franchise Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)
    End Function

    Public Enum ObjectAction
        Edit = 1
        Delete = 2
        Cancel = 3
        Discontinue = 4
    End Enum

End Module

Public Module DataAccess

    Public Sub sp_UpdateJobStatistics(ByVal BRID As Integer, ByVal JBID As Long, ByVal SqlConnection As SqlClient.SqlConnection)
        Dim i As Integer = 0
        Dim succeeded As Boolean = False
        While i < 3 And Not succeeded ' try 3 attempts
            Dim trans As SqlClient.SqlTransaction
            Try
                Dim cmd As SqlClient.SqlCommand
                trans = SqlConnection.BeginTransaction(IsolationLevel.ReadUncommitted)
                cmd = New SqlClient.SqlCommand("exec dbo.sp_UpdateJobStatistics @BRID = @BRID, @JBID = @JBID", SqlConnection, trans)
                cmd.Parameters.Add("@BRID", BRID)
                cmd.Parameters.Add("@JBID", JBID)
                cmd.ExecuteNonQuery()
                succeeded = True
                trans.Commit()
            Catch ex As Exception
                trans.Rollback()
            End Try
            i = i + 1
        End While
        If Not succeeded Then Message.ShowMessage("The figures could not be recalculated for this job.  This report will contain the last available figures." & _
            vbNewLine & vbNewLine & "To see more recent figures, please try again later.", MessageBoxIcon.Exclamation)
    End Sub

    Public Function sp_GetBRShortName(ByVal BRID As Integer, ByVal Connection As SqlClient.SqlConnection) As String
        Dim cnn As SqlClient.SqlConnection
        cnn = Connection
        Dim cmd As New SqlClient.SqlCommand("SELECT BRShortName FROM Branches WHERE BRID = @BRID", cnn)
        cmd.CommandType = CommandType.Text
        cmd.Parameters.Add("@BRID", BRID)
        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            sp_GetBRShortName = cmd.ExecuteScalar()
            cnn.Close()
        Else
            sp_GetBRShortName = cmd.ExecuteScalar()
        End If
    End Function

    Public Function sp_GetBRID(ByVal Username As String, ByVal Connection As SqlClient.SqlConnection) As Integer
        Dim cnn As SqlClient.SqlConnection
        cnn = Connection
        Dim cmd As New SqlClient.SqlCommand("sp_GetBRID", cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@Username", Username)
        cmd.Parameters.Add("@BRID", SqlDbType.Int).Direction = ParameterDirection.Output
        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            If cmd.Parameters("@BRID").Value Is DBNull.Value Then
                sp_GetBRID = -1
            Else
                sp_GetBRID = cmd.Parameters("@BRID").Value
            End If
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            If cmd.Parameters("@BRID").Value Is DBNull.Value Then
                sp_GetBRID = -1
            Else
                sp_GetBRID = cmd.Parameters("@BRID").Value
            End If
        End If
    End Function

    Public Function sp_GetRoles(ByVal USID As String, ByVal Connection As SqlClient.SqlConnection) As ArrayList
        sp_GetRoles = New ArrayList
        Dim cnn As SqlClient.SqlConnection
        cnn = Connection
        Dim cmd As New SqlClient.SqlCommand("sp_GetRoles", cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@USID", USID)
        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader()
            Do While reader.Read
                sp_GetRoles.Add(reader("Role"))
            Loop
            reader.Close()
            cnn.Close()
        Else
            Dim reader As SqlClient.SqlDataReader
            reader = cmd.ExecuteReader()
            Do While reader.Read
                sp_GetRoles.Add(reader("Role"))
            Loop
            reader.Close()
        End If
    End Function

    Public Function sp_GetRoleCommands(ByVal USID As String, ByVal Connection As SqlClient.SqlConnection) As ArrayList
        sp_GetRoleCommands = New ArrayList
        Dim cnn As SqlClient.SqlConnection
        cnn = Connection
        Dim cmd As New SqlClient.SqlCommand("sp_GetRoleCommands", cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@USID", USID)
        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            Dim reader As SqlClient.SqlDataReader = cmd.ExecuteReader()
            Do While reader.Read
                sp_GetRoleCommands.Add(New CommandAccess(reader("Command"), reader("Allowed")))
            Loop
            reader.Close()
            cnn.Close()
        Else
            Dim reader As SqlClient.SqlDataReader
            reader = cmd.ExecuteReader()
            Do While reader.Read
                sp_GetRoleCommands.Add(New CommandAccess(reader("Command"), reader("Allowed")))
            Loop
            reader.Close()
        End If
    End Function

    Public Function sp_GetIsHeadOffice(ByVal Username As String, ByVal Connection As SqlClient.SqlConnection) As Boolean
        Dim cnn As SqlClient.SqlConnection
        cnn = Connection
        Dim cmd As New SqlClient.SqlCommand("SELECT USIsHeadOffice FROM Users WHERE USUserName = @Username", cnn)
        cmd.CommandType = CommandType.Text
        cmd.Parameters.Add("@Username", Username)
        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            sp_GetIsHeadOffice = cmd.ExecuteScalar
            cnn.Close()
        Else
            sp_GetIsHeadOffice = cmd.ExecuteScalar
        End If
    End Function

    Public Function sp_Login(ByVal Username As String, ByVal Password As String, ByVal IsHeadOffice As Boolean, ByVal BRID As Integer, ByRef USID As Integer, ByVal Connection As SqlClient.SqlConnection) As Boolean
        Dim cnn As SqlClient.SqlConnection
        cnn = Connection
        Dim cmd As New SqlClient.SqlCommand("sp_Login", cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@USUsername", Username)
        cmd.Parameters.Add("@USPassword", Password)
        cmd.Parameters.Add("@USIsHeadOffice", IsHeadOffice)
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@Successful", SqlDbType.Bit).Direction = ParameterDirection.Output
        cmd.Parameters.Add("@USID", SqlDbType.Int).Direction = ParameterDirection.Output
        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            sp_Login = cmd.Parameters("@Successful").Value
            USID = cmd.Parameters("@USID").Value
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            sp_Login = cmd.Parameters("@Successful").Value
            USID = cmd.Parameters("@USID").Value
        End If
    End Function


    Public Function sp_GetNextID(ByVal BRID As Integer, ByVal Transaction As SqlClient.SqlTransaction) As Long
        Dim cnn As SqlClient.SqlConnection
        cnn = Transaction.Connection
        Dim cmd As New SqlClient.SqlCommand("sp_GetNextID", cnn)
        cmd.Transaction = Transaction
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@NextID", SqlDbType.BigInt).Direction = ParameterDirection.Output

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            sp_GetNextID = cmd.Parameters("@NextID").Value
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            sp_GetNextID = cmd.Parameters("@NextID").Value
        End If
    End Function

    Public Function sp_GetNextID(ByVal BRID As Integer, ByVal Connection As SqlClient.SqlConnection) As Long
        Dim cnn As SqlClient.SqlConnection
        cnn = Connection
        Dim cmd As New SqlClient.SqlCommand("sp_GetNextID", cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@NextID", SqlDbType.BigInt).Direction = ParameterDirection.Output

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            sp_GetNextID = cmd.Parameters("@NextID").Value
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            sp_GetNextID = cmd.Parameters("@NextID").Value
        End If
    End Function

    Public Function sp_GetNextOrderNumber(ByVal BRID As Integer, ByVal Connection As SqlClient.SqlConnection) As Long
        Dim cnn As SqlClient.SqlConnection
        cnn = Connection
        Dim cmd As New SqlClient.SqlCommand("sp_GetNextOrderNumber", cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@NextID", SqlDbType.BigInt).Direction = ParameterDirection.Output

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            sp_GetNextOrderNumber = cmd.Parameters("@NextID").Value
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            sp_GetNextOrderNumber = cmd.Parameters("@NextID").Value
        End If
    End Function

    Public Sub sp_AddCoreMaterialsToJob(ByVal BRID As Integer, ByVal JBID As Long, ByVal Transaction As SqlClient.SqlTransaction)
        Dim cnn As SqlClient.SqlConnection
        cnn = Transaction.Connection
        Dim cmd As New SqlClient.SqlCommand("sp_AddCoreMaterialsToJob", cnn)
        cmd.Transaction = Transaction
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@JBID", JBID)

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
        End If
    End Sub

    ' Sets JBID as the new JBID and return True if Successful, else
    ' sets JBID to the EXISTING job and returns false
    Public Function sp_CopyLeadToJob(ByVal BRID As Int32, ByVal LDID As Int64, ByRef JBID As Int64, ByVal Transaction As SqlClient.SqlTransaction) As Boolean
        Dim cnn As SqlClient.SqlConnection
        cnn = Transaction.Connection
        Dim cmd As New SqlClient.SqlCommand("sp_CopyLeadToJob", cnn)
        cmd.Transaction = Transaction
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@LDID", LDID)
        cmd.Parameters.Add("@JBID", SqlDbType.BigInt).Direction = ParameterDirection.Output
        cmd.Parameters.Add("@Successful", SqlDbType.Bit).Direction = ParameterDirection.Output

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            sp_CopyLeadToJob = cmd.Parameters("@Successful").Value
            JBID = cmd.Parameters("@JBID").Value
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            sp_CopyLeadToJob = cmd.Parameters("@Successful").Value
            JBID = cmd.Parameters("@JBID").Value
        End If
    End Function

    Public Sub sp_RemoveExpenseFromJobs(ByVal BRID As Integer, ByVal EXID As Integer, ByVal Transaction As SqlClient.SqlTransaction)
        Dim cnn As SqlClient.SqlConnection
        cnn = Transaction.Connection
        Dim cmd As New SqlClient.SqlCommand("sp_RemoveExpenseFromJobs", cnn)
        cmd.Transaction = Transaction
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@EXID", EXID)

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
        End If
    End Sub

    Public Sub sp_RemoveMaterialFromJobs(ByVal BRID As Integer, ByVal MTID As Integer, ByVal Transaction As SqlClient.SqlTransaction)
        Dim cnn As SqlClient.SqlConnection
        cnn = Transaction.Connection
        Dim cmd As New SqlClient.SqlCommand("sp_RemoveMaterialFromJobs", cnn)
        cmd.Transaction = Transaction
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@MTID", MTID)

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
        End If
    End Sub

    Public Sub sp_RemoveMasterMaterialFromJobs(ByVal MMTID As Integer, ByVal Transaction As SqlClient.SqlTransaction)
        Dim cnn As SqlClient.SqlConnection
        cnn = Transaction.Connection
        Dim cmd As New SqlClient.SqlCommand("sp_RemoveMasterMaterialFromJobs", cnn)
        cmd.Transaction = Transaction
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@MMTID", MMTID)

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
        End If
    End Sub

    Public Function ShowHours(ByVal BRID As Integer, ByVal Connection As SqlClient.SqlConnection) As Boolean
        Return CBool(ExecuteScalar("SELECT BRShowHours FROM Branches WHERE BRID = @BRID", CommandType.Text, New SqlClient.SqlParameter() {New SqlClient.SqlParameter("@BRID", BRID)}, Connection))
    End Function

    Public Function ShowHours(ByVal BRID As Integer, ByVal Transaction As SqlClient.SqlTransaction) As Boolean
        Return CBool(ExecuteScalar("SELECT BRShowHours FROM Branches WHERE BRID = @BRID", CommandType.Text, New SqlClient.SqlParameter() {New SqlClient.SqlParameter("@BRID", BRID)}, Transaction))
    End Function

    Public Function OtherOpeningInventoryExists(ByVal BRID As Integer, ByVal Connection As SqlClient.SqlConnection) As Boolean
        Return CBool(ExecuteScalar("SELECT dbo.OtherOpeningInventoryExists(@BRID, NULL)", CommandType.Text, New SqlClient.SqlParameter() {New SqlClient.SqlParameter("@BRID", BRID)}, Connection))
    End Function

    Public Function OtherOpeningInventoryExists(ByVal BRID As Integer, ByVal SAID As Integer, ByVal Connection As SqlClient.SqlConnection) As Boolean
        Return CBool(ExecuteScalar("SELECT dbo.OtherOpeningInventoryExists(@BRID, @SAID)", CommandType.Text, New SqlClient.SqlParameter() {New SqlClient.SqlParameter("@BRID", BRID), New SqlClient.SqlParameter("@SAID", SAID)}, Connection))
    End Function

    Public Function EGType(ByVal BRID As Integer, ByVal EXID As Integer, ByVal Connection As SqlClient.SqlConnection) As String
        Return ExecuteScalar("SELECT dbo.EGType(@BRID, @EXID)", CommandType.Text, _
        New SqlClient.SqlParameter() {New SqlClient.SqlParameter("@BRID", BRID), New SqlClient.SqlParameter("@EXID", EXID)}, Connection)
    End Function

    Public Function EGType(ByVal BRID As Integer, ByVal EXID As Integer, ByVal Transaction As SqlClient.SqlTransaction) As String
        Return ExecuteScalar("SELECT dbo.EGType(@BRID, @EXID)", CommandType.Text, _
        New SqlClient.SqlParameter() {New SqlClient.SqlParameter("@BRID", BRID), New SqlClient.SqlParameter("@EXID", EXID)}, Transaction)
    End Function

    Public Function EGType(ByVal EGID As Integer, ByVal Connection As SqlClient.SqlConnection) As String
        Return ExecuteScalar("SELECT EGType FROM ExpenseGroups WHERE EGID = @EGID", CommandType.Text, _
                New SqlClient.SqlParameter() {New SqlClient.SqlParameter("@EGID", EGID)}, Connection)
    End Function

    Public Function SIIsUsed(ByVal BRID As Integer, ByVal SIID As Integer, ByVal Connection As SqlClient.SqlConnection) As Boolean
        Return ExecuteScalar("SELECT dbo.SIIsUsed(@BRID, @SIID)", CommandType.Text, New SqlClient.SqlParameter() _
                {New SqlClient.SqlParameter("@BRID", BRID), New SqlClient.SqlParameter("@SIID", SIID)}, Connection)
    End Function

    Public Function MSIIsUsed(ByVal MSIID As Integer, ByVal Connection As SqlClient.SqlConnection) As Boolean
        Return ExecuteScalar("SELECT dbo.MSIIsUsed(@MSIID)", CommandType.Text, New SqlClient.SqlParameter() _
                {New SqlClient.SqlParameter("@MSIID", MSIID)}, Connection)
    End Function

    Public Function MGID(ByVal BRID As Integer, ByVal MTID As Integer, ByVal Connection As SqlClient.SqlConnection) As Integer
        Return ExecuteScalar("SELECT MGID FROM Materials WHERE BRID = @BRID AND MTID = @MTID", CommandType.Text, New SqlClient.SqlParameter() _
                {New SqlClient.SqlParameter("@BRID", BRID), New SqlClient.SqlParameter("@MTID", MTID)}, Connection)
    End Function

    Public Function MGID(ByVal MMTID As Integer, ByVal Connection As SqlClient.SqlConnection) As Integer
        Return ExecuteScalar("SELECT MGID FROM Master_Materials WHERE MMTID = @MMTID", CommandType.Text, New SqlClient.SqlParameter() _
                {New SqlClient.SqlParameter("@MMTID", MMTID)}, Connection)
    End Function

    Public Function MGID(ByVal BRID As Integer, ByVal MTID As Integer, ByVal Transaction As SqlClient.SqlTransaction) As Integer
        Dim cnn As SqlClient.SqlConnection
        cnn = Transaction.Connection
        Dim cmd As New SqlClient.SqlCommand("SELECT MGID FROM Materials WHERE BRID = @BRID AND MTID = @MTID", cnn)
        cmd.Transaction = Transaction
        cmd.CommandType = CommandType.Text
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@MTID", MTID)

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            MGID = cmd.ExecuteScalar
            cnn.Close()
        Else
            MGID = cmd.ExecuteScalar()
        End If
    End Function

    Public Function MGID(ByVal MMTID As Integer, ByVal Transaction As SqlClient.SqlTransaction) As String
        Dim cnn As SqlClient.SqlConnection
        cnn = Transaction.Connection
        Dim cmd As New SqlClient.SqlCommand("SELECT MGID FROM Master_Materials WHERE MMTID = @MMTID", cnn)
        cmd.Transaction = Transaction
        cmd.CommandType = CommandType.Text
        cmd.Parameters.Add("@MMTID", MMTID)

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            MGID = cmd.ExecuteScalar
            cnn.Close()
        Else
            MGID = cmd.ExecuteScalar()
        End If
    End Function

    Public Function spNew_Branch(ByVal Transaction As SqlClient.SqlTransaction) As Integer
        Dim cnn As SqlClient.SqlConnection
        cnn = Transaction.Connection
        Dim cmd As New SqlClient.SqlCommand("spNew_Branch", cnn)
        cmd.Transaction = Transaction
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@BRID", SqlDbType.Int).Direction = ParameterDirection.Output

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            spNew_Branch = cmd.Parameters("@BRID").Value
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            spNew_Branch = cmd.Parameters("@BRID").Value
        End If
    End Function

    Public Function spNew_User(ByVal USIsHeadOffice As Boolean, ByVal Transaction As SqlClient.SqlTransaction, Optional ByVal BRID As Integer = Nothing) As Integer
        Dim cnn As SqlClient.SqlConnection
        cnn = Transaction.Connection
        Dim cmd As New SqlClient.SqlCommand("spNew_User", cnn)
        cmd.Transaction = Transaction
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@USIsHeadOffice", USIsHeadOffice)
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@USID", SqlDbType.BigInt).Direction = ParameterDirection.Output

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            spNew_User = cmd.Parameters("@USID").Value
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            spNew_User = cmd.Parameters("@USID").Value
        End If
    End Function

    Public Function spNew_Expense(ByVal BRID As Integer, ByVal EGID As Int32, ByVal Transaction As SqlClient.SqlTransaction) As Integer
        Dim cnn As SqlClient.SqlConnection
        cnn = Transaction.Connection
        Dim cmd As New SqlClient.SqlCommand("spNew_Expense", cnn)
        cmd.Transaction = Transaction
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@EGID", EGID)
        cmd.Parameters.Add("@EXID", SqlDbType.Int).Direction = ParameterDirection.Output

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            spNew_Expense = cmd.Parameters("@EXID").Value
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            spNew_Expense = cmd.Parameters("@EXID").Value
        End If
    End Function

    Public Function spNew_StockAdjustment(ByVal BRID As Integer, ByVal Transaction As SqlClient.SqlTransaction) As Integer
        Dim cnn As SqlClient.SqlConnection
        cnn = Transaction.Connection
        Dim cmd As New SqlClient.SqlCommand("spNew_StockAdjustment", cnn)
        cmd.Transaction = Transaction
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@SAID", SqlDbType.Int).Direction = ParameterDirection.Output

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            spNew_StockAdjustment = cmd.Parameters("@SAID").Value
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            spNew_StockAdjustment = cmd.Parameters("@SAID").Value
        End If
    End Function

    Public Function spNew_Material(ByVal BRID As Integer, ByVal MGID As Int32, ByVal Transaction As SqlClient.SqlTransaction) As Integer
        Dim cnn As SqlClient.SqlConnection
        cnn = Transaction.Connection
        Dim cmd As New SqlClient.SqlCommand("spNew_Material", cnn)
        cmd.Transaction = Transaction
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@MGID", MGID)
        cmd.Parameters.Add("@MTID", SqlDbType.Int).Direction = ParameterDirection.Output

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            spNew_Material = cmd.Parameters("@MTID").Value
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            spNew_Material = cmd.Parameters("@MTID").Value
        End If
    End Function

    Public Function spNew_Appointment(ByVal BRID As Integer, ByVal APType As String, ByVal APTypeID As Long, ByVal Transaction As SqlClient.SqlTransaction) As Long
        Dim cnn As SqlClient.SqlConnection
        cnn = Transaction.Connection
        Dim cmd As New SqlClient.SqlCommand("spNew_Appointment", cnn)
        cmd.Transaction = Transaction
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@APType", APType)
        cmd.Parameters.Add("@APTypeID", APTypeID)
        cmd.Parameters.Add("@APID", SqlDbType.BigInt).Direction = ParameterDirection.Output

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            spNew_Appointment = cmd.Parameters("@APID").Value
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            spNew_Appointment = cmd.Parameters("@APID").Value
        End If
    End Function

    Public Function spNew_Job(ByVal BRID As Integer, ByVal ID As Long, ByVal Transaction As SqlClient.SqlTransaction) As Long
        Dim cnn As SqlClient.SqlConnection
        cnn = Transaction.Connection
        Dim cmd As New SqlClient.SqlCommand("spNew_Job", cnn)
        cmd.Transaction = Transaction
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@ID", ID)
        cmd.Parameters.Add("@JBID", SqlDbType.BigInt).Direction = ParameterDirection.Output

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            spNew_Job = cmd.Parameters("@JBID").Value
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            spNew_Job = cmd.Parameters("@JBID").Value
        End If
    End Function

    Public Function spNew_Lead(ByVal BRID As Integer, ByVal ID As Long, ByVal Transaction As SqlClient.SqlTransaction) As Long
        Dim cnn As SqlClient.SqlConnection
        cnn = Transaction.Connection
        Dim cmd As New SqlClient.SqlCommand("spNew_Lead", cnn)
        cmd.Transaction = Transaction
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@ID", ID)
        cmd.Parameters.Add("@LDID", SqlDbType.BigInt).Direction = ParameterDirection.Output

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            spNew_Lead = cmd.Parameters("@LDID").Value
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            spNew_Lead = cmd.Parameters("@LDID").Value
        End If
    End Function

    Public Function spNew_Master_Material(ByVal MGID As Integer, ByVal Transaction As SqlClient.SqlTransaction) As Long
        Dim cnn As SqlClient.SqlConnection
        cnn = Transaction.Connection
        Dim cmd As New SqlClient.SqlCommand("spNew_Master_Material", cnn)
        cmd.Transaction = Transaction
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@MGID", MGID)
        cmd.Parameters.Add("@MMTID", SqlDbType.BigInt).Direction = ParameterDirection.Output

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            spNew_Master_Material = cmd.Parameters("@MMTID").Value
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            spNew_Master_Material = cmd.Parameters("@MMTID").Value
        End If
    End Function

    Public Function spNew_NonCoreSalesItem(ByVal BRID As Integer, ByVal JBID As Long, ByVal NIType As String, ByVal Transaction As SqlClient.SqlTransaction) As Long
        Dim cnn As SqlClient.SqlConnection
        cnn = Transaction.Connection
        Dim cmd As New SqlClient.SqlCommand("spNew_NonCoreSalesItem", cnn)
        cmd.Transaction = Transaction
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@JBID", JBID)
        cmd.Parameters.Add("@NIType", NIType)
        cmd.Parameters.Add("@NIID", SqlDbType.BigInt).Direction = ParameterDirection.Output

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            spNew_NonCoreSalesItem = cmd.Parameters("@NIID").Value
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            spNew_NonCoreSalesItem = cmd.Parameters("@NIID").Value
        End If
    End Function

    Public Function spGet_FirstDayOfFinancialYear(ByVal OfDate As Date, ByVal Connection As SqlClient.SqlConnection) As Date
        Dim cnn As SqlClient.SqlConnection
        cnn = Connection
        Dim cmd As New SqlClient.SqlCommand("spGet_FirstDayOfFinancialYear", cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@Date", OfDate)
        cmd.Parameters.Add("@FirstDayOfFinancialYear", SqlDbType.DateTime).Direction = ParameterDirection.Output

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            spGet_FirstDayOfFinancialYear = cmd.Parameters("@FirstDayOfFinancialYear").Value
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            spGet_FirstDayOfFinancialYear = cmd.Parameters("@FirstDayOfFinancialYear").Value
        End If
    End Function

    Public Function spExecLockRequest(ByVal Command As String, ByVal BRID As Integer, ByVal ObjID As Long, ByVal Transaction As SqlClient.SqlTransaction) As Boolean
        Dim cnn As SqlClient.SqlConnection
        cnn = Transaction.Connection
        Dim cmd As New SqlClient.SqlCommand(Command, cnn)
        cmd.Transaction = Transaction
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@ObjID", ObjID)
        cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            spExecLockRequest = CInt(cmd.Parameters("@Result").Value) >= 0
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            spExecLockRequest = CInt(cmd.Parameters("@Result").Value) >= 0
        End If
    End Function

    Public Function spExecLockRequest(ByVal Command As String, ByVal BRID As Integer, ByVal ObjID As Long, ByVal Connection As SqlClient.SqlConnection) As Boolean
        Dim cnn As SqlClient.SqlConnection = Connection
        Dim cmd As New SqlClient.SqlCommand(Command, cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@ObjID", ObjID)
        cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            spExecLockRequest = CInt(cmd.Parameters("@Result").Value) >= 0
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            spExecLockRequest = CInt(cmd.Parameters("@Result").Value) >= 0
        End If
    End Function

    Public Function spExecLockRequest(ByVal Command As String, ByVal ObjID As Long, ByVal Transaction As SqlClient.SqlTransaction) As Boolean
        Dim cnn As SqlClient.SqlConnection
        cnn = Transaction.Connection
        Dim cmd As New SqlClient.SqlCommand(Command, cnn)
        cmd.Transaction = Transaction
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@ObjID", ObjID)
        cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            spExecLockRequest = CInt(cmd.Parameters("@Result").Value) >= 0
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            spExecLockRequest = CInt(cmd.Parameters("@Result").Value) >= 0
        End If
    End Function

    Public Function spExecLockRequest(ByVal Command As String, ByVal ObjID As Long, ByVal Connection As SqlClient.SqlConnection) As Boolean
        Dim cnn As SqlClient.SqlConnection = Connection
        Dim cmd As New SqlClient.SqlCommand(Command, cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@ObjID", ObjID)
        cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            spExecLockRequest = CInt(cmd.Parameters("@Result").Value) >= 0
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            spExecLockRequest = CInt(cmd.Parameters("@Result").Value) >= 0
        End If
    End Function

    Public Function spExecLockRequest_All(ByVal Command As String, ByVal BRID As Integer, ByVal Transaction As SqlClient.SqlTransaction) As Boolean
        Dim cnn As SqlClient.SqlConnection
        cnn = Transaction.Connection
        Dim cmd As New SqlClient.SqlCommand(Command, cnn)
        cmd.Transaction = Transaction
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            spExecLockRequest_All = CInt(cmd.Parameters("@Result").Value) >= 0
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            spExecLockRequest_All = CInt(cmd.Parameters("@Result").Value) >= 0
        End If
    End Function

    Public Function spExecLockRequest_All(ByVal Command As String, ByVal BRID As Integer, ByVal Connection As SqlClient.SqlConnection) As Boolean
        Dim cnn As SqlClient.SqlConnection = Connection
        Dim cmd As New SqlClient.SqlCommand(Command, cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            spExecLockRequest_All = CInt(cmd.Parameters("@Result").Value) >= 0
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            spExecLockRequest_All = CInt(cmd.Parameters("@Result").Value) >= 0
        End If
    End Function

    Public Function spExecLockRequest_Global(ByVal Command As String, ByVal Connection As SqlClient.SqlConnection) As Boolean
        Dim cnn As SqlClient.SqlConnection = Connection
        Dim cmd As New SqlClient.SqlCommand(Command, cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            cmd.ExecuteNonQuery()
            spExecLockRequest_Global = CInt(cmd.Parameters("@Result").Value) >= 0
            cnn.Close()
        Else
            cmd.ExecuteNonQuery()
            spExecLockRequest_Global = CInt(cmd.Parameters("@Result").Value) >= 0
        End If
    End Function

    Public Sub sp_InsertExpenseAllowance(ByVal BRID As Integer, ByVal EXID As Integer, ByVal ALFromDate As DateTime, ByVal ALAllowance As Double, ByVal ALHours As Object, ByVal Transaction As SqlClient.SqlTransaction)
        Dim cnn As SqlClient.SqlConnection
        cnn = Transaction.Connection
        Dim cmd As New SqlClient.SqlCommand("sp_InsertExpenseAllowance", cnn)
        cmd.Transaction = Transaction
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@EXID", EXID)
        cmd.Parameters.Add("@ALFromDate", ALFromDate)
        cmd.Parameters.Add("@ALAllowance", ALAllowance)
        cmd.Parameters.Add("@ALHours", ALHours)

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            Try
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                If InStr(ex.Message, "Voilation of PRIMARY KEY constraint") > 0 Then
                    Throw New ConstraintException
                Else
                    Throw ex
                End If
            End Try
            cnn.Close()
        Else
            Try
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                If InStr(ex.Message, "Voilation of PRIMARY KEY constraint") > 0 Then
                    Throw New ConstraintException
                Else
                    Throw ex
                End If
            End Try
        End If
    End Sub

    Public Sub sp_InsertExpenseAllowance(ByVal BRID As Integer, ByVal EXID As Integer, ByVal ALFromDate As DateTime, ByVal ALAllowance As Double, ByVal ALHours As Object, ByVal Connection As SqlClient.SqlConnection)
        Dim cnn As SqlClient.SqlConnection
        cnn = Connection
        Dim cmd As New SqlClient.SqlCommand("sp_InsertExpenseAllowance", cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@EXID", EXID)
        cmd.Parameters.Add("@ALFromDate", ALFromDate)
        cmd.Parameters.Add("@ALAllowance", ALAllowance)
        cmd.Parameters.Add("@ALHours", ALHours)

        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            Try
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                If InStr(ex.Message, "Voilation of PRIMARY KEY constraint") > 0 Then
                    Throw New ConstraintException
                Else
                    Throw ex
                End If
            End Try
            cnn.Close()
        Else
            Try
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                If InStr(ex.Message, "Voilation of PRIMARY KEY constraint") > 0 Then
                    Throw New ConstraintException
                Else
                    Throw ex
                End If
            End Try
        End If
    End Sub

    Public Sub sp_InsertMaterialCost(ByVal BRID As Integer, ByVal MTID As Integer, ByVal MCFromDate As DateTime, ByVal MCPurchaseCost As Double, ByVal Transaction As SqlClient.SqlTransaction)
        ExecuteNonQuery("sp_InsertMaterialCost", CommandType.StoredProcedure, New SqlClient.SqlParameter() _
            {New SqlClient.SqlParameter("@BRID", BRID), New SqlClient.SqlParameter("@MTID", MTID), _
            New SqlClient.SqlParameter("@MCFromDate", MCFromDate), New SqlClient.SqlParameter("@MCPurchaseCost", MCPurchaseCost)}, Transaction)
    End Sub

    Public Sub sp_InsertMaterialCost(ByVal BRID As Integer, ByVal MTID As Integer, ByVal MCFromDate As DateTime, ByVal MCPurchaseCost As Double, ByVal MCAdditionalCost As Double, ByVal Connection As SqlClient.SqlConnection)
        ExecuteNonQuery("sp_InsertMaterialCost", CommandType.StoredProcedure, New SqlClient.SqlParameter() _
            {New SqlClient.SqlParameter("@BRID", BRID), New SqlClient.SqlParameter("@MTID", MTID), _
            New SqlClient.SqlParameter("@MCFromDate", MCFromDate), New SqlClient.SqlParameter("@MCPurchaseCost", MCPurchaseCost), _
            New SqlClient.SqlParameter("@MCAdditionalCost", MCAdditionalCost)}, Connection)
    End Sub

    Public Function ExecuteNonQuery(ByVal commandText As String, ByVal commandType As System.Data.CommandType, _
    ByVal parameters As System.Data.SqlClient.SqlParameter(), ByVal transaction As SqlClient.SqlTransaction) As Object
        Dim cnn As SqlClient.SqlConnection
        cnn = transaction.Connection
        Dim cmd As New SqlClient.SqlCommand(commandText, cnn)
        cmd.Transaction = transaction
        cmd.CommandType = commandType
        For Each parameter As System.Data.SqlClient.SqlParameter In parameters
            cmd.Parameters.Add(parameter)
        Next
        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            ExecuteNonQuery = cmd.ExecuteScalar
            cnn.Close()
        Else
            ExecuteNonQuery = cmd.ExecuteScalar()
        End If
    End Function

    Public Function ExecuteNonQuery(ByVal commandText As String, ByVal commandType As System.Data.CommandType, _
    ByVal parameters As System.Data.SqlClient.SqlParameter(), ByVal connection As SqlClient.SqlConnection) As Object
        Dim cnn As SqlClient.SqlConnection
        cnn = connection
        Dim cmd As New SqlClient.SqlCommand(commandText, cnn)
        cmd.CommandType = commandType
        For Each parameter As System.Data.SqlClient.SqlParameter In parameters
            cmd.Parameters.Add(parameter)
        Next
        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            ExecuteNonQuery = cmd.ExecuteNonQuery
            cnn.Close()
        Else
            ExecuteNonQuery = cmd.ExecuteNonQuery()
        End If
    End Function

    Public Function ExecuteScalar(ByVal commandText As String, ByVal commandType As System.Data.CommandType, _
    ByVal parameters As System.Data.SqlClient.SqlParameter(), ByVal transaction As SqlClient.SqlTransaction) As Object
        Dim cnn As SqlClient.SqlConnection
        cnn = transaction.Connection
        Dim cmd As New SqlClient.SqlCommand(commandText, cnn)
        cmd.Transaction = transaction
        cmd.CommandType = commandType
        For Each parameter As System.Data.SqlClient.SqlParameter In parameters
            cmd.Parameters.Add(parameter)
        Next
        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            ExecuteScalar = cmd.ExecuteScalar
            cnn.Close()
        Else
            ExecuteScalar = cmd.ExecuteScalar()
        End If
    End Function

    Public Function ExecuteScalar(ByVal commandText As String, ByVal commandType As System.Data.CommandType, _
    ByVal parameters As System.Data.SqlClient.SqlParameter(), ByVal connection As SqlClient.SqlConnection) As Object
        Dim cnn As SqlClient.SqlConnection
        cnn = connection
        Dim cmd As New SqlClient.SqlCommand(commandText, cnn)
        cmd.CommandType = commandType
        For Each parameter As System.Data.SqlClient.SqlParameter In parameters
            cmd.Parameters.Add(parameter)
        Next
        If cnn.State = ConnectionState.Closed Then
            cnn.Open()
            ExecuteScalar = cmd.ExecuteScalar
            cnn.Close()
        Else
            ExecuteScalar = cmd.ExecuteScalar()
        End If
    End Function

End Module

Public Class CommandAccess
    Sub New(ByVal Command As String, ByVal Allowed As Boolean)
        hCommand = Command
        hAllowed = Allowed
    End Sub

    Private hCommand As String
    Public ReadOnly Property Command() As String
        Get
            Return hCommand
        End Get
    End Property

    Private hAllowed As Boolean
    Public ReadOnly Property Allowed() As Boolean
        Get
            Return hAllowed
        End Get
    End Property
End Class

Public Class ObjectLockedException
    Inherits Exception

End Class

Public Interface ISelectedItemReports

    ReadOnly Property SelectedItemReports() As DevExpress.XtraBars.BarItem

    ReadOnly Property SelectedItemForms() As DevExpress.XtraBars.BarItem

End Interface

