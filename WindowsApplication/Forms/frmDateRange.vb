Public Class frmDateRange
    Inherits DevExpress.XtraEditors.XtraForm

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Connection As System.Data.SqlClient.SqlConnection)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        dpFrom.EditValue = spGet_FirstDayOfFinancialYear(Today.Date, Connection)
        dpTo.EditValue = Today.Date
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dpFrom As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dpTo As DevExpress.XtraEditors.DateEdit
    Friend WithEvents btnHelp As DevExpress.XtraEditors.SimpleButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmDateRange))
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.dpFrom = New DevExpress.XtraEditors.DateEdit
        Me.dpTo = New DevExpress.XtraEditors.DateEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.btnHelp = New DevExpress.XtraEditors.SimpleButton
        CType(Me.dpFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dpTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(16, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(312, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Please select a date range on which to generate the report."
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(16, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 21)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "From:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dpFrom
        '
        Me.dpFrom.EditValue = New Date(2005, 8, 17, 0, 0, 0, 0)
        Me.dpFrom.Location = New System.Drawing.Point(64, 48)
        Me.dpFrom.Name = "dpFrom"
        '
        'dpFrom.Properties
        '
        Me.dpFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpFrom.Size = New System.Drawing.Size(136, 20)
        Me.dpFrom.TabIndex = 2
        '
        'dpTo
        '
        Me.dpTo.EditValue = New Date(2005, 8, 17, 0, 0, 0, 0)
        Me.dpTo.Location = New System.Drawing.Point(248, 48)
        Me.dpTo.Name = "dpTo"
        '
        'dpTo.Properties
        '
        Me.dpTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpTo.Size = New System.Drawing.Size(136, 20)
        Me.dpTo.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(216, 48)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(32, 21)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "To:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(312, 88)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 7
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.Location = New System.Drawing.Point(232, 88)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 6
        Me.btnOK.Text = "OK"
        '
        'btnHelp
        '
        Me.btnHelp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnHelp.Image = CType(resources.GetObject("btnHelp.Image"), System.Drawing.Image)
        Me.btnHelp.Location = New System.Drawing.Point(8, 88)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.TabIndex = 5
        Me.btnHelp.Text = "Help"
        '
        'frmDateRange
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(394, 120)
        Me.Controls.Add(Me.btnHelp)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dpTo)
        Me.Controls.Add(Me.dpFrom)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDateRange"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Select a Date Range"
        CType(Me.dpFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dpTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public Property FromDate() As Date
        Get
            Return dpFrom.EditValue
        End Get
        Set(ByVal Value As Date)
            dpFrom.EditValue = Value
        End Set
    End Property

    Public Property ToDate() As Date
        Get
            Return dpTo.EditValue
        End Get
        Set(ByVal Value As Date)
            dpTo.EditValue = Value
        End Set
    End Property

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If ValidateForm() Then
            Me.DialogResult = DialogResult.OK
            Me.Close()
        End If
    End Sub
    Private Function ValidateForm() As Boolean
        If dpFrom.EditValue Is Nothing Then
            Message.ShowMessage("You must enter a From Date.", MessageBoxIcon.Exclamation)
            dpFrom.Focus()
            Return False
        End If
        If dpTo.EditValue Is Nothing Then
            Message.ShowMessage("You must enter a To Date.", MessageBoxIcon.Exclamation)
            dpTo.Focus()
            Return False
        End If
        Return True
    End Function

    Private Sub DateEdit_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) _
    Handles dpFrom.Validating, dpTo.Validating
        Library.DateEdit_Validating(sender, e)
    End Sub

End Class

