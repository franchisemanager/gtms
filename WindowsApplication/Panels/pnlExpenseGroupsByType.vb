Imports Power.Forms

Public Class pnlExpenseGroupsByType
    Inherits System.Windows.Forms.UserControl
    Implements IListControl

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Connection As SqlClient.SqlConnection, ByVal BRID As Integer, ByVal EGType As String, ByVal EGTypeName As String)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.Connection = Connection
        Me.BRID = BRID
        Me.EGType = EGType
        Me.EGTypeName = EGTypeName
        FillDataSet()
        LoadMenu()
    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents hSqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DsGTMS As WindowsApplication.dsGTMS
    Friend WithEvents tvwMenu As System.Windows.Forms.TreeView
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents ImageList2 As System.Windows.Forms.ImageList
    Friend WithEvents Splitter1 As System.Windows.Forms.Splitter
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(pnlExpenseGroupsByType))
        Me.hSqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.DsGTMS = New WindowsApplication.dsGTMS
        Me.tvwMenu = New System.Windows.Forms.TreeView
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.ImageList2 = New System.Windows.Forms.ImageList(Me.components)
        Me.Splitter1 = New System.Windows.Forms.Splitter
        Me.pnlMain = New System.Windows.Forms.Panel
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'hSqlConnection
        '
        Me.hSqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;integrated security=SSPI;data source=SERVER;" & _
        "persist security info=False;initial catalog=GTMS_DEV"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "ExpenseGroups", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("EGID", "EGID"), New System.Data.Common.DataColumnMapping("EGName", "EGName"), New System.Data.Common.DataColumnMapping("EGType", "EGType")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM ExpenseGroups WHERE (EGID = @Original_EGID) AND (EGName = @Original_E" & _
        "GName OR @Original_EGName IS NULL AND EGName IS NULL) AND (EGType = @Original_EG" & _
        "Type OR @Original_EGType IS NULL AND EGType IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.hSqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGType", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO ExpenseGroups(EGName, EGType) VALUES (@EGName, @EGType); SELECT EGID," & _
        " EGName, EGType FROM ExpenseGroups WHERE (EGID = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.hSqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGName", System.Data.SqlDbType.VarChar, 50, "EGName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 2, "EGType"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT EGID, EGName, EGType FROM ExpenseGroups WHERE (EGType = @EGType)"
        Me.SqlSelectCommand1.Connection = Me.hSqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGType", System.Data.DataRowVersion.Current, "0"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE ExpenseGroups SET EGName = @EGName, EGType = @EGType WHERE (EGID = @Origin" & _
        "al_EGID) AND (EGName = @Original_EGName OR @Original_EGName IS NULL AND EGName I" & _
        "S NULL) AND (EGType = @Original_EGType OR @Original_EGType IS NULL AND EGType IS" & _
        " NULL); SELECT EGID, EGName, EGType FROM ExpenseGroups WHERE (EGID = @EGID)"
        Me.SqlUpdateCommand1.Connection = Me.hSqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGName", System.Data.SqlDbType.VarChar, 50, "EGName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 2, "EGType"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGID", System.Data.SqlDbType.SmallInt, 2, "EGID"))
        '
        'DsGTMS
        '
        Me.DsGTMS.DataSetName = "dsGTMS"
        Me.DsGTMS.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'tvwMenu
        '
        Me.tvwMenu.Dock = System.Windows.Forms.DockStyle.Left
        Me.tvwMenu.HideSelection = False
        Me.tvwMenu.ImageList = Me.ImageList1
        Me.tvwMenu.Location = New System.Drawing.Point(0, 0)
        Me.tvwMenu.Name = "tvwMenu"
        Me.tvwMenu.ShowRootLines = False
        Me.tvwMenu.Size = New System.Drawing.Size(248, 432)
        Me.tvwMenu.TabIndex = 0
        '
        'ImageList1
        '
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'ImageList2
        '
        Me.ImageList2.ImageSize = New System.Drawing.Size(32, 32)
        Me.ImageList2.ImageStream = CType(resources.GetObject("ImageList2.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList2.TransparentColor = System.Drawing.Color.Transparent
        '
        'Splitter1
        '
        Me.Splitter1.Location = New System.Drawing.Point(248, 0)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(4, 432)
        Me.Splitter1.TabIndex = 2
        Me.Splitter1.TabStop = False
        '
        'pnlMain
        '
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(252, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(572, 432)
        Me.pnlMain.TabIndex = 3
        '
        'pnlExpenseGroupsByType
        '
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.Splitter1)
        Me.Controls.Add(Me.tvwMenu)
        Me.Name = "pnlExpenseGroupsByType"
        Me.Size = New System.Drawing.Size(824, 432)
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return hSqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            hSqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private Sub pnlExpenseGroupsByType_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim w As Integer = tvwMenu.Width
        tvwMenu.Width = 0
        tvwMenu.Width = w
    End Sub

    Private Sub LoadMenu()
        Dim node As System.Windows.Forms.TreeNode
        Dim parentNode As System.Windows.Forms.TreeNode
        node = New TreeNode(EGTypeName, 0, 0)
        tvwMenu.Nodes.Add(node)
        parentNode = node
        Dim i As Integer
        For i = 0 To DsGTMS.ExpenseGroups.Count - 1
            ' Create Node
            node = New TreeNode(DsGTMS.ExpenseGroups(i).EGName, 1, 1)

            ' Create and Add PKey
            Dim PKey As PKey
            PKey = New PKey
            PKey.Fields.Add("EGID", DsGTMS.ExpenseGroups(i).EGID.ToString)
            node.Tag = PKey

            ' Add Node
            parentNode.Nodes.Add(node)
        Next
        tvwMenu.SelectedNode = parentNode
        Me.Select()
        tvwMenu.Select()
    End Sub

    Public Function FillDataSet() As Integer Implements IListControl.FillDataSet
        Return SqlDataAdapter.Fill(DsGTMS)
    End Function

    Private MainControl As Control
    Private Sub tvwMenu_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvwMenu.AfterSelect
        Dim NewMainControl As Control

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        ' If it has sub-nodes, expand
        tvwMenu.SelectedNode.Expand()

        ' Set up the title bar
        Try
            If Not tvwMenu.SelectedNode.ImageIndex = -1 Then
                ExplorerForm.PictureBox1.Image = ImageList2.Images(tvwMenu.SelectedNode.ImageIndex)
            Else
                ExplorerForm.PictureBox1.Image = ImageList2.Images(tvwMenu.SelectedImageIndex)
            End If
            ExplorerForm.lblSectionTitle.Text = tvwMenu.SelectedNode.Text
        Catch ex As Exception
            ' Do nothing
        End Try

        ' Add new control
        Select Case tvwMenu.SelectedNode.FullPath
            Case EGTypeName
                Dim lv As New lvNodeList(tvwMenu.SelectedNode, ImageList2, ImageList1, ExplorerForm)
                lv.ListView.View = View.LargeIcon
                NewMainControl = lv
            Case Else
                If Not tvwMenu.SelectedNode.Tag Is Nothing Then
                    Dim lv As New lvExpenses(Me.Connection, BRID, CType(tvwMenu.SelectedNode.Tag, PKey)("EGID"), ExplorerForm)
                    NewMainControl = lv
                End If
        End Select

        ' Swap MainControl
        If Not MainControl Is Nothing Then
            pnlMain.Controls.Remove(MainControl)
            MainControl = Nothing
        End If
        MainControl = NewMainControl
        If Not MainControl Is Nothing Then
            MainControl.Dock = DockStyle.Fill
            pnlMain.Controls.Add(MainControl)
        End If

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private hExplorerForm As Form
    Public Property ExplorerForm() As frmExplorer
        Get
            Return hExplorerForm
        End Get
        Set(ByVal Value As frmExplorer)
            hExplorerForm = Value
        End Set
    End Property

    Private hBRID As Integer
    Private Property BRID() As Integer
        Get
            Return hBRID
        End Get
        Set(ByVal Value As Integer)
            hBRID = Value
        End Set
    End Property

    Private hEGType As String
    Private Property EGType() As String
        Get
            Return hEGType
        End Get
        Set(ByVal Value As String)
            hEGType = Value
            SqlDataAdapter.SelectCommand.Parameters("@EGType").Value = Value
        End Set
    End Property

    Private hEGTypeName As String ' for example 'Direct Labour' or 'Overheads'
    Private Property EGTypeName() As String
        Get
            Return hEGTypeName
        End Get
        Set(ByVal Value As String)
            hEGTypeName = Value
        End Set
    End Property

    Public Property Icon1() As System.Drawing.Image
        Get
            Return ImageList2.Images(0)
        End Get
        Set(ByVal Value As System.Drawing.Image)
            ImageList1.Images(0) = Value
            ImageList2.Images(0) = Value
        End Set
    End Property

    Public Property Icon2() As System.Drawing.Image
        Get
            Return ImageList2.Images(1)
        End Get
        Set(ByVal Value As System.Drawing.Image)
            ImageList1.Images(1) = Value
            ImageList2.Images(1) = Value
        End Set
    End Property

    Public Sub List_Delete() Implements IListControl.List_Delete
        If Not MainControl Is Nothing Then
            CType(MainControl, IListControl).List_Delete()
        End If
    End Sub

    Public Sub List_Edit() Implements IListControl.List_Edit
        If Not MainControl Is Nothing Then
            CType(MainControl, IListControl).List_Edit()
        End If
    End Sub

    Public Sub List_New() Implements IListControl.List_New
        If Not MainControl Is Nothing Then
            CType(MainControl, IListControl).List_New()
        End If
    End Sub
End Class
