Imports Power.Forms

Public Class lvCalendar2
    Inherits System.Windows.Forms.UserControl
    Implements IListControl, IPrintableControl

    Private EGTypeFilter As String

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Connection As SqlClient.SqlConnection, ByVal BRID As Integer, ByVal ExplorerForm As frmExplorer, Optional ByVal EGTypeFilter As String = Nothing)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.Connection = Connection
        Me.BRID = BRID
        Me.EGTypeFilter = EGTypeFilter
        hExplorerForm = ExplorerForm
        Calendar.Calendar.Start = Today.Date
        If Not EGTypeFilter Is Nothing Then
            Calendar.ApplyEGTypeFilter(EGTypeFilter)
        Else
            Calendar.ApplyDefaultFilter()
        End If
        FillPreliminaryData()
        SetUpDateFilter()
        DateFilterIsListening = True
        FillDataSet()
    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents hSqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents daTasks As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daAppointments As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents daExpenses As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Calendar As WindowsApplication.CalendarControl
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents bStandard As DevExpress.XtraBars.Bar
    Friend WithEvents btnNew As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnEdit As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subReportsOnSelectedItem As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnQuickPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPrintPreview As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnHelp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarAndDockingController1 As DevExpress.XtraBars.BarAndDockingController
    Friend WithEvents ImageListToolBar As System.Windows.Forms.ImageList
    Friend WithEvents btnDelete As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subReportsOnSelectedItem2 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents beiFromDate As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents beiToDate As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents bAdvancedFilter As DevExpress.XtraBars.Bar
    Friend WithEvents btnShowAllDates As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(lvCalendar2))
        Me.hSqlConnection = New System.Data.SqlClient.SqlConnection
        Me.Calendar = New WindowsApplication.CalendarControl
        Me.daTasks = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.daAppointments = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.daExpenses = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.BarManager1 = New DevExpress.XtraBars.BarManager
        Me.bStandard = New DevExpress.XtraBars.Bar
        Me.btnNew = New DevExpress.XtraBars.BarButtonItem
        Me.btnEdit = New DevExpress.XtraBars.BarButtonItem
        Me.btnDelete = New DevExpress.XtraBars.BarButtonItem
        Me.subReportsOnSelectedItem = New DevExpress.XtraBars.BarSubItem
        Me.btnQuickPrint = New DevExpress.XtraBars.BarButtonItem
        Me.btnPrintPreview = New DevExpress.XtraBars.BarButtonItem
        Me.btnHelp = New DevExpress.XtraBars.BarButtonItem
        Me.bAdvancedFilter = New DevExpress.XtraBars.Bar
        Me.beiFromDate = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.beiToDate = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.btnShowAllDates = New DevExpress.XtraBars.BarButtonItem
        Me.BarAndDockingController1 = New DevExpress.XtraBars.BarAndDockingController(Me.components)
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
        Me.ImageListToolBar = New System.Windows.Forms.ImageList(Me.components)
        Me.btnPrint = New DevExpress.XtraBars.BarButtonItem
        Me.subReportsOnSelectedItem2 = New DevExpress.XtraBars.BarSubItem
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'hSqlConnection
        '
        Me.hSqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;user id=sa;integrated security=SSPI;data sou" & _
        "rce=""SERVER\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'Calendar
        '
        Me.Calendar.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Calendar.Location = New System.Drawing.Point(0, 50)
        Me.Calendar.Name = "Calendar"
        Me.Calendar.Size = New System.Drawing.Size(592, 310)
        Me.Calendar.TabIndex = 0
        '
        'daTasks
        '
        Me.daTasks.InsertCommand = Me.SqlInsertCommand1
        Me.daTasks.SelectCommand = Me.SqlSelectCommand1
        Me.daTasks.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Tasks", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("TAName", "TAName"), New System.Data.Common.DataColumnMapping("TAControlsJobDate", "TAControlsJobDate"), New System.Data.Common.DataColumnMapping("TAMenuCaption", "TAMenuCaption"), New System.Data.Common.DataColumnMapping("TAColourR", "TAColourR"), New System.Data.Common.DataColumnMapping("TAColourG", "TAColourG"), New System.Data.Common.DataColumnMapping("TAColourB", "TAColourB"), New System.Data.Common.DataColumnMapping("TAID", "TAID")})})
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Tasks(TAName, TAControlsJobDate, TAMenuCaption, TAColourR, TAColourG," & _
        " TAColourB, TAID) VALUES (@TAName, @TAControlsJobDate, @TAMenuCaption, @TAColour" & _
        "R, @TAColourG, @TAColourB, @TAID); SELECT TAName, TAControlsJobDate, TAMenuCapti" & _
        "on, TAColourR, TAColourG, TAColourB, TAID FROM Tasks ORDER BY TAID"
        Me.SqlInsertCommand1.Connection = Me.hSqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAName", System.Data.SqlDbType.VarChar, 50, "TAName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAControlsJobDate", System.Data.SqlDbType.Bit, 1, "TAControlsJobDate"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAMenuCaption", System.Data.SqlDbType.VarChar, 50, "TAMenuCaption"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourR", System.Data.SqlDbType.SmallInt, 2, "TAColourR"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourG", System.Data.SqlDbType.SmallInt, 2, "TAColourG"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourB", System.Data.SqlDbType.SmallInt, 2, "TAColourB"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAID", System.Data.SqlDbType.Int, 4, "TAID"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT TAName, TAControlsJobDate, TAMenuCaption, TAColourR, TAColourG, TAColourB," & _
        " TAID FROM Tasks ORDER BY TAID"
        Me.SqlSelectCommand1.Connection = Me.hSqlConnection
        '
        'daAppointments
        '
        Me.daAppointments.DeleteCommand = Me.SqlDeleteCommand2
        Me.daAppointments.InsertCommand = Me.SqlInsertCommand2
        Me.daAppointments.SelectCommand = Me.SqlSelectCommand2
        Me.daAppointments.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VAppointments", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("APID", "APID"), New System.Data.Common.DataColumnMapping("APBegin", "APBegin"), New System.Data.Common.DataColumnMapping("APEnd", "APEnd"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("APType", "APType"), New System.Data.Common.DataColumnMapping("APTypeID", "APTypeID"), New System.Data.Common.DataColumnMapping("APNotes", "APNotes"), New System.Data.Common.DataColumnMapping("APStatus", "APStatus"), New System.Data.Common.DataColumnMapping("APTask", "APTask"), New System.Data.Common.DataColumnMapping("APAllDay", "APAllDay")})})
        Me.daAppointments.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM Appointments WHERE (APID = @Original_APID) AND (BRID = @Original_BRID" &
        ");"



        'Me.SqlDeleteCommand2.CommandText = "UPDATE Jobs SET JBScheduledStartDate = NULL, JBScheduledFinishDate = NULL WHERE (JBID in " & " (select APTypeID from Appointments where (APID = @Original_APID)));DELETE FROM Appointments WHERE (APID = " & "@Original_APID) AND (BRID = @Original_BRID);"


        Me.SqlDeleteCommand2.Connection = Me.hSqlConnection
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO Appointments (BRID, APBegin, APEnd, EXID, APType, APTypeID, APNotes, " & _
        "APStatus, APTask, APAllDay) VALUES (@BRID, @APBegin, @APEnd, @EXID, @APType, @AP" & _
        "TypeID, @APNotes, @APStatus, @APTask, @APAllDay); SELECT BRID, APID, APBegin, AP" & _
        "End, EXID, APType, APTypeID, APNotes, APStatus, APTask, APDescription, APAddress" & _
        "Multiline, ID, APClientName, EXName, APJobDescription, APClientHomePhoneNumber, " & _
        "APClientWorkPhoneNumber, APClientMobileNumber, APSuburb, APAllDay, JBPriceQuoted" & _
        " FROM VAppointments WHERE (APID = @@IDENTITY) AND (BRID = @BRID)"
        Me.SqlInsertCommand2.Connection = Me.hSqlConnection
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APBegin", System.Data.SqlDbType.DateTime, 8, "APBegin"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APEnd", System.Data.SqlDbType.DateTime, 8, "APEnd"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APType", System.Data.SqlDbType.VarChar, 2, "APType"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTypeID", System.Data.SqlDbType.BigInt, 8, "APTypeID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APNotes", System.Data.SqlDbType.VarChar, 2000, "APNotes"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APStatus", System.Data.SqlDbType.Int, 4, "APStatus"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTask", System.Data.SqlDbType.Int, 4, "APTask"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APAllDay", System.Data.SqlDbType.Bit, 1, "APAllDay"))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT BRID, APID, APBegin, APEnd, EXID, APType, APTypeID, APNotes, APStatus, APT" & _
        "ask, APDescription, APAddressMultiline, ID, APClientName, EXName, APJobDescripti" & _
        "on, APClientHomePhoneNumber, APClientWorkPhoneNumber, APClientMobileNumber, APSu" & _
        "burb, APAllDay, JBPriceQuoted FROM VAppointments WHERE (BRID = @BRID) AND (APIsC" & _
        "ancelled = 0) AND (APEnd > @FROM_DATE) AND (APBegin < @TO_DATE) OR (BRID = @BRID" & _
        ") AND (APIsCancelled = 0) AND (APBegin < @TO_DATE) AND (@FROM_DATE IS NULL) OR (" & _
        "BRID = @BRID) AND (APIsCancelled = 0) AND (APEnd > @FROM_DATE) AND (@TO_DATE IS " & _
        "NULL) OR (BRID = @BRID) AND (APIsCancelled = 0) AND (@FROM_DATE IS NULL) AND (@T" & _
        "O_DATE IS NULL)"
        Me.SqlSelectCommand2.Connection = Me.hSqlConnection
        Me.SqlSelectCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FROM_DATE", System.Data.SqlDbType.DateTime, 8, "APEnd"))
        Me.SqlSelectCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TO_DATE", System.Data.SqlDbType.DateTime, 8, "APBegin"))
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE Appointments SET BRID = @BRID, APBegin = @APBegin, APEnd = @APEnd, EXID = " & _
        "@EXID, APType = @APType, APTypeID = @APTypeID, APNotes = @APNotes, APStatus = @A" & _
        "PStatus, APTask = @APTask, APAllDay = @APAllDay WHERE (APID = @Original_APID) AN" & _
        "D (BRID = @Original_BRID); SELECT BRID, APID, APBegin, APEnd, EXID, APType, APTy" & _
        "peID, APNotes, APStatus, APTask, APDescription, APAddressMultiline, ID, APClient" & _
        "Name, EXName, APJobDescription, APClientHomePhoneNumber, APClientWorkPhoneNumber" & _
        ", APClientMobileNumber, APSuburb, APAllDay, JBPriceQuoted FROM VAppointments WHE" & _
        "RE (APID = @APID) AND (BRID = @BRID)"
        Me.SqlUpdateCommand2.Connection = Me.hSqlConnection
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APBegin", System.Data.SqlDbType.DateTime, 8, "APBegin"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APEnd", System.Data.SqlDbType.DateTime, 8, "APEnd"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APType", System.Data.SqlDbType.VarChar, 2, "APType"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTypeID", System.Data.SqlDbType.BigInt, 8, "APTypeID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APNotes", System.Data.SqlDbType.VarChar, 2000, "APNotes"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APStatus", System.Data.SqlDbType.Int, 4, "APStatus"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTask", System.Data.SqlDbType.Int, 4, "APTask"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APAllDay", System.Data.SqlDbType.Bit, 1, "APAllDay"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APID", System.Data.SqlDbType.BigInt, 8, "APID"))
        '
        'daExpenses
        '
        Me.daExpenses.DeleteCommand = Me.SqlDeleteCommand1
        Me.daExpenses.InsertCommand = Me.SqlInsertCommand3
        Me.daExpenses.SelectCommand = Me.SqlSelectCommand3
        Me.daExpenses.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VExpenses", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("EXName", "EXName"), New System.Data.Common.DataColumnMapping("EGType", "EGType"), New System.Data.Common.DataColumnMapping("EXAppearInCalendarRC", "EXAppearInCalendarRC"), New System.Data.Common.DataColumnMapping("EXAppearInCalendarDL", "EXAppearInCalendarDL")})})
        Me.daExpenses.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Expenses WHERE (BRID = @Original_BRID) AND (EXID = @Original_EXID) AN" & _
        "D (EGType = @Original_EGType OR @Original_EGType IS NULL AND EGType IS NULL) AND" & _
        " (EXAppearInCalendarDL = @Original_EXAppearInCalendarDL OR @Original_EXAppearInC" & _
        "alendarDL IS NULL AND EXAppearInCalendarDL IS NULL) AND (EXAppearInCalendarRC = " & _
        "@Original_EXAppearInCalendarRC OR @Original_EXAppearInCalendarRC IS NULL AND EXA" & _
        "ppearInCalendarRC IS NULL) AND (EXName = @Original_EXName OR @Original_EXName IS" & _
        " NULL AND EXName IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.hSqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXAppearInCalendarDL", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXAppearInCalendarDL", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXAppearInCalendarRC", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXAppearInCalendarRC", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXName", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO Expenses(BRID, EXName, EXAppearInCalendarRC, EXAppearInCalendarDL) VA" & _
        "LUES (@BRID, @EXName, @EXAppearInCalendarRC, @EXAppearInCalendarDL); SELECT BRID" & _
        ", EXID, EXName, EGType, EXAppearInCalendarRC, EXAppearInCalendarDL FROM Expenses" & _
        " WHERE (BRID = @BRID) AND (EXID = @@IDENTITY)"
        Me.SqlInsertCommand3.Connection = Me.hSqlConnection
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAppearInCalendarRC", System.Data.SqlDbType.Bit, 1, "EXAppearInCalendarRC"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAppearInCalendarDL", System.Data.SqlDbType.Bit, 1, "EXAppearInCalendarDL"))
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT BRID, EXID, EXName, EGType, EXAppearInCalendarRC, EXAppearInCalendarDL, EX" & _
        "CalendarName, EGName FROM VExpenses WHERE (EXDiscontinued = 0) AND (BRID = @BRID" & _
        ")"
        Me.SqlSelectCommand3.Connection = Me.hSqlConnection
        Me.SqlSelectCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Expenses SET BRID = @BRID, EXName = @EXName, EXAppearInCalendarRC = @EXApp" & _
        "earInCalendarRC, EXAppearInCalendarDL = @EXAppearInCalendarDL WHERE (BRID = @Ori" & _
        "ginal_BRID) AND (EXID = @Original_EXID) AND (EXAppearInCalendarDL = @Original_EX" & _
        "AppearInCalendarDL OR @Original_EXAppearInCalendarDL IS NULL AND EXAppearInCalen" & _
        "darDL IS NULL) AND (EXAppearInCalendarRC = @Original_EXAppearInCalendarRC OR @Or" & _
        "iginal_EXAppearInCalendarRC IS NULL AND EXAppearInCalendarRC IS NULL) AND (EXNam" & _
        "e = @Original_EXName OR @Original_EXName IS NULL AND EXName IS NULL); SELECT BRI" & _
        "D, EXID, EXName, EGType, EXAppearInCalendarRC, EXAppearInCalendarDL FROM Expense" & _
        "s WHERE (BRID = @BRID) AND (EXID = @EXID)"
        Me.SqlUpdateCommand1.Connection = Me.hSqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAppearInCalendarRC", System.Data.SqlDbType.Bit, 1, "EXAppearInCalendarRC"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAppearInCalendarDL", System.Data.SqlDbType.Bit, 1, "EXAppearInCalendarDL"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXAppearInCalendarDL", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXAppearInCalendarDL", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXAppearInCalendarRC", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXAppearInCalendarRC", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.bStandard, Me.bAdvancedFilter})
        Me.BarManager1.Categories.AddRange(New DevExpress.XtraBars.BarManagerCategory() {New DevExpress.XtraBars.BarManagerCategory("Built-in Menus", New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd"), False), New DevExpress.XtraBars.BarManagerCategory("File", New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")), New DevExpress.XtraBars.BarManagerCategory("Reports", New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")), New DevExpress.XtraBars.BarManagerCategory("Forms", New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")), New DevExpress.XtraBars.BarManagerCategory("Help", New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")), New DevExpress.XtraBars.BarManagerCategory("Filters", New System.Guid("e6c9ece2-ecb3-403a-9dff-0660dbc3a261"))})
        Me.BarManager1.Controller = Me.BarAndDockingController1
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Images = Me.ImageListToolBar
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnNew, Me.btnEdit, Me.btnDelete, Me.subReportsOnSelectedItem, Me.btnHelp, Me.btnPrintPreview, Me.btnQuickPrint, Me.btnPrint, Me.subReportsOnSelectedItem2, Me.beiFromDate, Me.beiToDate, Me.btnShowAllDates})
        Me.BarManager1.MainMenu = Me.bStandard
        Me.BarManager1.MaxItemId = 96
        Me.BarManager1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1, Me.RepositoryItemDateEdit2})
        '
        'bStandard
        '
        Me.bStandard.BarName = "Standard Toolbar"
        Me.bStandard.DockCol = 0
        Me.bStandard.DockRow = 0
        Me.bStandard.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bStandard.FloatLocation = New System.Drawing.Point(44, 188)
        Me.bStandard.FloatSize = New System.Drawing.Size(659, 24)
        Me.bStandard.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnNew, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnEdit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnDelete, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(Me.subReportsOnSelectedItem, True), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnQuickPrint, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnPrintPreview, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnHelp, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.bStandard.OptionsBar.AllowQuickCustomization = False
        Me.bStandard.OptionsBar.DisableClose = True
        Me.bStandard.OptionsBar.DrawDragBorder = False
        Me.bStandard.OptionsBar.MultiLine = True
        Me.bStandard.OptionsBar.UseWholeRow = True
        Me.bStandard.Text = "Standard Toolbar"
        '
        'btnNew
        '
        Me.btnNew.Caption = "&New..."
        Me.btnNew.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnNew.Id = 19
        Me.btnNew.ImageIndex = 0
        Me.btnNew.ImageIndexDisabled = 0
        Me.btnNew.Name = "btnNew"
        '
        'btnEdit
        '
        Me.btnEdit.Caption = "&Edit..."
        Me.btnEdit.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnEdit.Id = 20
        Me.btnEdit.ImageIndex = 1
        Me.btnEdit.ImageIndexDisabled = 1
        Me.btnEdit.Name = "btnEdit"
        '
        'btnDelete
        '
        Me.btnDelete.Caption = "&Delete"
        Me.btnDelete.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnDelete.Id = 23
        Me.btnDelete.ImageIndex = 13
        Me.btnDelete.ImageIndexDisabled = 13
        Me.btnDelete.Name = "btnDelete"
        '
        'subReportsOnSelectedItem
        '
        Me.subReportsOnSelectedItem.Caption = "Reports on Selected Item"
        Me.subReportsOnSelectedItem.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.subReportsOnSelectedItem.Id = 78
        Me.subReportsOnSelectedItem.Name = "subReportsOnSelectedItem"
        Me.subReportsOnSelectedItem.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInCustomizing
        '
        'btnQuickPrint
        '
        Me.btnQuickPrint.Caption = "Print"
        Me.btnQuickPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnQuickPrint.Id = 46
        Me.btnQuickPrint.ImageIndex = 11
        Me.btnQuickPrint.ImageIndexDisabled = 11
        Me.btnQuickPrint.Name = "btnQuickPrint"
        '
        'btnPrintPreview
        '
        Me.btnPrintPreview.Caption = "Print Pre&view"
        Me.btnPrintPreview.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrintPreview.Id = 44
        Me.btnPrintPreview.ImageIndex = 10
        Me.btnPrintPreview.ImageIndexDisabled = 10
        Me.btnPrintPreview.Name = "btnPrintPreview"
        '
        'btnHelp
        '
        Me.btnHelp.Caption = "&Help"
        Me.btnHelp.CategoryGuid = New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")
        Me.btnHelp.Id = 68
        Me.btnHelp.ImageIndex = 4
        Me.btnHelp.ImageIndexDisabled = 4
        Me.btnHelp.Name = "btnHelp"
        '
        'bAdvancedFilter
        '
        Me.bAdvancedFilter.BarName = "Date Filter"
        Me.bAdvancedFilter.DockCol = 0
        Me.bAdvancedFilter.DockRow = 1
        Me.bAdvancedFilter.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bAdvancedFilter.FloatLocation = New System.Drawing.Point(-2, 188)
        Me.bAdvancedFilter.FloatSize = New System.Drawing.Size(638, 24)
        Me.bAdvancedFilter.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.beiFromDate, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.beiToDate, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnShowAllDates, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.bAdvancedFilter.OptionsBar.AllowQuickCustomization = False
        Me.bAdvancedFilter.OptionsBar.DisableClose = True
        Me.bAdvancedFilter.OptionsBar.MultiLine = True
        Me.bAdvancedFilter.Text = "Date Filter"
        '
        'beiFromDate
        '
        Me.beiFromDate.Caption = "Appointment Dates from:"
        Me.beiFromDate.CategoryGuid = New System.Guid("e6c9ece2-ecb3-403a-9dff-0660dbc3a261")
        Me.beiFromDate.Edit = Me.RepositoryItemDateEdit1
        Me.beiFromDate.Id = 93
        Me.beiFromDate.Name = "beiFromDate"
        Me.beiFromDate.Width = 120
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'beiToDate
        '
        Me.beiToDate.Caption = "Appointment Dates to:"
        Me.beiToDate.CategoryGuid = New System.Guid("e6c9ece2-ecb3-403a-9dff-0660dbc3a261")
        Me.beiToDate.Edit = Me.RepositoryItemDateEdit2
        Me.beiToDate.Id = 94
        Me.beiToDate.Name = "beiToDate"
        Me.beiToDate.Width = 120
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'btnShowAllDates
        '
        Me.btnShowAllDates.Caption = "Show A&ll"
        Me.btnShowAllDates.CategoryGuid = New System.Guid("e6c9ece2-ecb3-403a-9dff-0660dbc3a261")
        Me.btnShowAllDates.Id = 95
        Me.btnShowAllDates.ImageIndex = 14
        Me.btnShowAllDates.ImageIndexDisabled = 14
        Me.btnShowAllDates.Name = "btnShowAllDates"
        '
        'BarAndDockingController1
        '
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseForeColor = True
        Me.BarAndDockingController1.PaintStyleName = "OfficeXP"
        '
        'ImageListToolBar
        '
        Me.ImageListToolBar.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImageListToolBar.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageListToolBar.ImageStream = CType(resources.GetObject("ImageListToolBar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListToolBar.TransparentColor = System.Drawing.Color.Transparent
        '
        'btnPrint
        '
        Me.btnPrint.Caption = "&Print..."
        Me.btnPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrint.Id = 43
        Me.btnPrint.ImageIndex = 11
        Me.btnPrint.ImageIndexDisabled = 11
        Me.btnPrint.Name = "btnPrint"
        '
        'subReportsOnSelectedItem2
        '
        Me.subReportsOnSelectedItem2.Caption = "&Reports on Selected Item"
        Me.subReportsOnSelectedItem2.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subReportsOnSelectedItem2.Id = 87
        Me.subReportsOnSelectedItem2.Name = "subReportsOnSelectedItem2"
        '
        'lvCalendar2
        '
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.Controls.Add(Me.Calendar)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "lvCalendar2"
        Me.Size = New System.Drawing.Size(592, 360)
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return hSqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            hSqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private Sub FillPreliminaryData()
        Dim cursor As System.Windows.Forms.Cursor = ExplorerForm.Cursor
        ExplorerForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
        ' Open connection
        Dim trans As SqlClient.SqlTransaction = Connection.BeginTransaction(IsolationLevel.ReadUncommitted)
        Power.Library.Library.ApplyTransactionToAllDataAdapters(trans, Me)
        ' --- SALES REPS ---
        Calendar.DsCalendar.VExpenses.Clear()
        daExpenses.Fill(Calendar.DsCalendar.VExpenses)
        ' --- TASKS ---
        Calendar.DsCalendar.Tasks.Clear()
        daTasks.Fill(Calendar.DsCalendar)
        Calendar.RefreshTasks(Calendar.DsCalendar.Tasks)
        'Close transaction
        trans.Commit()
        ExplorerForm.Cursor = cursor
    End Sub

    Public Function FillDataSet() As Integer Implements IListControl.FillDataSet

        FillPreliminaryData()

        Dim cursor As System.Windows.Forms.Cursor = ExplorerForm.Cursor
        ExplorerForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
        ' Open connection
        Dim trans As SqlClient.SqlTransaction = Connection.BeginTransaction(IsolationLevel.ReadUncommitted)
        Power.Library.Library.ApplyTransactionToAllDataAdapters(trans, Me)
        ' --- APPOINTMENTS ---
        Calendar.DsCalendar.VAppointments.Clear()
        daAppointments.Fill(Calendar.DsCalendar.VAppointments)
        'Close transaction
        trans.Commit()
        Calendar.SetHighlight(BRID, "NO", 0)
        ExplorerForm.Cursor = cursor
    End Function

    Private Sub SetUpDateFilter()
        ' Set Up Default Advanced Filter
        beiFromDate.EditValue = Today.Date
        UpdateDateFilter()
    End Sub

    Private Sub DateFilter_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles beiFromDate.EditValueChanged, beiToDate.EditValueChanged
        UpdateDateFilter()
    End Sub

    Private DateFilterIsListening As Boolean = False ' This is so this doesn't trigger during load (this will happen in code)
    Private Sub UpdateDateFilter()
        Me.daAppointments.SelectCommand.Parameters("@FROM_DATE").Value = IsNull(beiFromDate.EditValue, DBNull.Value)
        Me.daAppointments.SelectCommand.Parameters("@TO_DATE").Value = IsNull(beiToDate.EditValue, DBNull.Value)
        If DateFilterIsListening Then
            RefreshAppointments()
        End If
    End Sub

    Private Sub btnShowAllDates_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnShowAllDates.ItemClick
        If (Not beiFromDate.EditValue Is Nothing) Or (Not beiToDate.EditValue Is Nothing) Then
            DateFilterIsListening = False
            beiFromDate.EditValue = Nothing
            beiToDate.EditValue = Nothing
            RefreshAppointments()
            DateFilterIsListening = True
        End If
    End Sub

    Public ReadOnly Property VisibleItems() As Integer Implements Power.Library.IListControl.VisibleItems
        Get
            Return Calendar.DsCalendar.VAppointments.Rows.Count
        End Get
    End Property

    Public ReadOnly Property NumItems() As Integer Implements Power.Library.IListControl.NumItems
        Get
            Return Calendar.DsCalendar.VAppointments.Rows.Count
        End Get
    End Property

    Private Sub RefreshAppointments()
        Dim cursor As System.Windows.Forms.Cursor = ExplorerForm.Cursor
        ExplorerForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
        ' Open connection
        Dim trans As SqlClient.SqlTransaction = Connection.BeginTransaction(IsolationLevel.ReadUncommitted)
        Power.Library.Library.ApplyTransactionToAllDataAdapters(trans, Me)
        ' --- APPOINTMENTS ---
        Calendar.DsCalendar.VAppointments.Clear()
        daAppointments.Fill(Calendar.DsCalendar.VAppointments)
        ExplorerForm.UpdateNumItems(NumItems)
        ExplorerForm.UpdateVisibleItems(VisibleItems)
        'Close transaction
        trans.Commit()
        Calendar.SetHighlight(BRID, "NO", 0)
        ExplorerForm.Cursor = cursor
    End Sub

    Private hBRID As Integer
    Private Property BRID() As Integer
        Get
            Return hBRID
        End Get
        Set(ByVal Value As Integer)
            hBRID = Value
            daExpenses.SelectCommand.Parameters("@BRID").Value = Value
            daAppointments.SelectCommand.Parameters("@BRID").Value = Value
        End Set
    End Property

    Private Sub SetUpCalendar()
    End Sub

    Private hExplorerForm As Form
    Public ReadOnly Property ExplorerForm() As frmExplorer
        Get
            Return hExplorerForm
        End Get
    End Property

#Region " Appointments "

    Private Sub Calendar_NewAllDayEvent(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar.NewAllDayEvent
        If HasRole("branch_read_only") Then Exit Sub
        If Not Calendar.SelectedResource Is Nothing Then
            Dim c As Cursor = Me.Cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            frmAppointment2.Add(Connection, Calendar.Storage, Calendar.dvAppointments.Table, BRID, "NO", 0, True, 1, Calendar.SelectedResource("EXID"), Calendar.Calendar.SelectedInterval.Start, Calendar.Calendar.SelectedInterval.End)
            daAppointments.Update(Calendar.DsCalendar.VAppointments)
            RefreshAppointments()
            Me.Cursor = c
        Else
            Message.ShowMessage("You must select a staff member on the right to add this appointment to.", MessageBoxIcon.Exclamation)
        End If
    End Sub

    Private Sub Calendar_NewAppointment(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar.NewAppointment
        If HasRole("branch_read_only") Then Exit Sub
        If Not Calendar.SelectedResource Is Nothing Then
            Dim c As Cursor = Me.Cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            frmAppointment2.Add(Connection, Calendar.Storage, Calendar.dvAppointments.Table, BRID, "NO", 0, False, 1, Calendar.SelectedResource("EXID"), Calendar.Calendar.SelectedInterval.Start, Calendar.Calendar.SelectedInterval.End)
            daAppointments.Update(Calendar.DsCalendar.VAppointments)
            RefreshAppointments()
            Me.Cursor = c
        Else
            Message.ShowMessage("You must select a staff member on the right to add this appointment to.", MessageBoxIcon.Exclamation)
        End If
    End Sub

    Private Sub btnNew_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnNew.ItemClick
        List_New()
    End Sub

    Public Sub List_New() Implements IListControl.List_New
        If Calendar.Calendar.SelectedInterval.Start.Hour = 0 And Calendar.Calendar.SelectedInterval.Start.Minute = 0 And _
                Calendar.Calendar.SelectedInterval.End.Hour = 0 And Calendar.Calendar.SelectedInterval.End.Minute = 0 And _
                Calendar.Calendar.SelectedInterval.Start <> Calendar.Calendar.SelectedInterval.End Then
            Calendar_NewAllDayEvent(Me, New EventArgs)
        Else
            Calendar_NewAppointment(Me, New EventArgs)
        End If
    End Sub

    Private Sub Calendar_EditAppointment(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar.EditAppointment
        If Not Calendar.SelectedAppointment Is Nothing Then
            'If Not (Calendar.SelectedAppointment.Item("BRID") = Me.BRID And Calendar.SelectedAppointment.Item("APType") = "NO" And _
            '        Calendar.SelectedAppointment.Item("APTypeID") = 0) Then
            '    If Message.AppointmentNotCurrentObject("job", Message.ObjectAction.Edit) = MsgBoxResult.No Then
            '        Exit Sub
            '    End If
            'End If
            Dim c As Cursor = Me.Cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If AppointmentExists(Calendar.SelectedAppointment.Item("BRID"), Calendar.SelectedAppointment.Item("APID")) Then
                Dim APID As Long = Calendar.SelectedAppointment.Item("APID")
                If DataAccess.spExecLockRequest("sp_GetAppointmentLock", BRID, APID, Connection) Then
                    frmAppointment2.Edit(Connection, Calendar.Storage, Calendar.SelectedAppointment, 1)
                    daAppointments.Update(Calendar.DsCalendar.VAppointments)
                    DataAccess.spExecLockRequest("sp_ReleaseAppointmentLock", BRID, APID, Connection)
                    RefreshAppointments()
                Else
                    Message.CurrentlyAccessed("appointment")
                End If
            Else
                Message.AlreadyDeleted("appointment", Message.ObjectAction.Edit)
                RefreshAppointments()
            End If
            Me.Cursor = c
        End If
    End Sub

    Private Sub btnEdit_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnEdit.ItemClick
        List_Edit()
    End Sub

    Public Sub List_Edit() Implements IListControl.List_Edit
        Calendar_EditAppointment(Me, New EventArgs)
    End Sub

    Public Sub List_Cancel() Implements Power.Library.IListControl.List_Cancel

    End Sub

    Public Sub List_Uncancel() Implements Power.Library.IListControl.List_Uncancel

    End Sub

    Private Sub btnDelete_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnDelete.ItemClick
        List_Delete()
    End Sub

    Public Sub List_Delete() Implements IListControl.List_Delete
        If Not Calendar.SelectedAppointment Is Nothing Then
            If Not (Calendar.SelectedAppointment.Item("BRID") = Me.BRID And Calendar.SelectedAppointment.Item("APType") = "NO" And Calendar.SelectedAppointment.Item("APTypeID") = 0) Then
                If Message.AppointmentNotCurrentObject("job", Message.ObjectAction.Delete) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If
            Dim APID As Long = Calendar.SelectedAppointment.Item("APID")
            If AppointmentExists(Calendar.SelectedAppointment.Item("BRID"), Calendar.SelectedAppointment.Item("APID")) Then
                If DataAccess.spExecLockRequest("sp_GetAppointmentLock", BRID, APID, Connection) Then
                    Calendar.SelectedAppointment.Delete()
                    daAppointments.Update(Calendar.DsCalendar.VAppointments)
                    DataAccess.spExecLockRequest("sp_ReleaseAppointmentLock", BRID, APID, Connection)
                    'RefreshAppointments() -- REMOVED
                Else
                    Message.CurrentlyAccessed("appointment")
                End If
            Else
                RefreshAppointments()
            End If
        End If
    End Sub

    Private Sub Calendar_AppointmentsChanged(ByVal sender As Object, ByVal e As DevExpress.XtraScheduler.PersistentObjectsEventArgs) Handles Calendar.AppointmentsChanged
        If HasRole("branch_read_only") Then Exit Sub
        For Each obj As Object In e.Objects
            'If Not (obj.GetRow(Calendar.Storage)("BRID") = BRID And obj.GetRow(Calendar.Storage)("APType") = APType And obj.GetRow(Calendar.Storage)("APTypeID") = APTypeID) Then
            '   If MsgBox("This timeslot belongs to another appointment.  Are you sure you want to move this timeslot?", MessageBoxIcon.Exclamation Or MsgBoxStyle.YesNo Or MsgBoxStyle.DefaultButton2) = MsgBoxResult.No Then
            '       Calendar.DsCalendar.RejectChanges()
            '    End If
            'End If
            Dim APID As Long = obj.GetRow(Calendar.Storage)("APID")
            If DataAccess.spExecLockRequest("sp_GetAppointmentLock", BRID, APID, Connection) Then
                DataAccess.spExecLockRequest("sp_ReleaseAppointmentLock", BRID, APID, Connection)
            Else
                Message.CurrentlyAccessed("appointment")
                Calendar.DsCalendar.RejectChanges()
            End If
        Next
        daAppointments.Update(Calendar.DsCalendar.VAppointments)
        'RefreshAppointments() -- REMOVED
    End Sub

    Private Sub Calendar_AppointmentsDeleted(ByVal sender As Object, ByVal e As DevExpress.XtraScheduler.PersistentObjectsEventArgs) Handles Calendar.AppointmentsDeleted
        If HasRole("branch_read_only") Then Exit Sub
        Dim DeletedRows As New ArrayList
        For Each obj As System.Data.DataRow In Calendar.DsCalendar.VAppointments.Rows
            If obj.RowState = DataRowState.Deleted Then
                DeletedRows.Add(obj)
            End If
        Next
        For Each obj As System.Data.DataRow In DeletedRows
            obj.RejectChanges()
            Dim APID As Long = obj("APID")
            'If Not (obj("BRID") = BRID And obj("APType") = "NO" And obj("APTypeID") = 0) Then
            '    If MsgBox("This timeslot belongs to another appointment.  Are you sure you delete to move this timeslot?", MessageBoxIcon.Exclamation Or MsgBoxStyle.YesNo Or MsgBoxStyle.DefaultButton2) = MsgBoxResult.No Then
            '        'obj.RejectChanges()
            '    End If
            'End If
            If DataAccess.spExecLockRequest("sp_GetAppointmentLock", BRID, APID, Connection) Then
                obj.Delete()
                DataAccess.spExecLockRequest("sp_ReleaseAppointmentLock", BRID, APID, Connection)
            Else
                Message.CurrentlyAccessed("appointment")
                'Calendar.DsCalendar.RejectChanges()
            End If
        Next
        daAppointments.Update(Calendar.DsCalendar.VAppointments)
        'RefreshAppointments() -- REMOVED
    End Sub

    Private Function AppointmentExists(ByVal BRID As Int32, ByVal APID As Int64) As Boolean
        Dim cnn As SqlClient.SqlConnection = Connection
        Dim cmd As New SqlClient.SqlCommand("SELECT Count(APID) FROM Appointments WHERE BRID = @BRID AND APID = @APID", cnn)
        cmd.CommandType = CommandType.Text
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@APID", APID)
        Return cmd.ExecuteScalar > 0
    End Function

#End Region

#Region " Printing "

    Private Sub btnPrint_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrint.ItemClick
        Print(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub Print(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.Print
        Calendar.Print(PrintingSystem)
    End Sub

    Private Sub btnPrintPreview_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrintPreview.ItemClick
        PrintPreview(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub PrintPreview(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.PrintPreview
        Calendar.PrintPreview(PrintingSystem)
    End Sub

    Private Sub btnQuickPrint_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnQuickPrint.ItemClick
        QuickPrint(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub QuickPrint(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.QuickPrint
        Calendar.QuickPrint(PrintingSystem)
    End Sub

#End Region

    Public ReadOnly Property AllowDelete() As Boolean Implements Power.Library.IListControl.AllowDelete
        Get
            Return Not HasRole("branch_read_only")
        End Get
    End Property

    Public ReadOnly Property AllowEdit() As Boolean Implements Power.Library.IListControl.AllowEdit
        Get
            Return True
        End Get
    End Property

    Public ReadOnly Property AllowNew() As Boolean Implements Power.Library.IListControl.AllowNew
        Get
            Return Not HasRole("branch_read_only")
        End Get
    End Property

    Public ReadOnly Property AllowCancel() As Boolean Implements Power.Library.IListControl.AllowCancel
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property AllowUncancel() As Boolean Implements Power.Library.IListControl.AllowUncancel
        Get
            Return False
        End Get
    End Property

    Private Sub btnHelp_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHelp.ItemClick
        ShowHelpTopic(ExplorerForm, "UsingTheSalespersonCalendar.htm")
    End Sub
End Class
