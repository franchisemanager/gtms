Public Class frmBooking
    Inherits DevExpress.XtraEditors.XtraForm

    Private DataRow As DataRow
    Public BRID As Int32
    Public JBID As Int64

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return SqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            SqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private hTransaction As SqlClient.SqlTransaction
    Private Property Transaction() As SqlClient.SqlTransaction
        Get
            Return hTransaction
        End Get
        Set(ByVal Value As SqlClient.SqlTransaction)
            hTransaction = Value
            Power.Library.Library.ApplyTransactionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Public Shared Function Add(ByVal BRID As Int32) As frmBooking
        Dim gui As New frmBooking

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()
            Dim tempTrans As SqlClient.SqlTransaction = .SqlConnection.BeginTransaction(IsolationLevel.ReadUncommitted)
            Dim NextID As Long = sp_GetNextID(BRID, tempTrans)
            tempTrans.Commit()

            .BRID = BRID
            .LoadTreeViews()

            .Transaction = .SqlConnection.BeginTransaction(IsolationLevel.ReadUncommitted)
            .FillPreliminaryData()
            .JBID = spNew_Job(.BRID, NextID, .Transaction)

            If DataAccess.spExecLockRequest("sp_GetJobIDLock", .BRID, .JBID, .Transaction) Then

                .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = .BRID
                .SqlDataAdapter.SelectCommand.Parameters("@JBID").Value = .JBID
                .SqlDataAdapter.Fill(.DsGTMS)
                .DataRow = .DsGTMS.Jobs(0)

                .FillData()

            Else
                Throw New ObjectLockedException
            End If
        End With

        Return gui
    End Function

    Public Shared Function Edit(ByVal BRID As Int32, ByRef JBID As Int64) As frmBooking
        Dim gui As New frmBooking

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            .BRID = BRID
            .JBID = JBID
            .LoadTreeViews()

            .Transaction = .SqlConnection.BeginTransaction(IsolationLevel.ReadUncommitted)
            .FillPreliminaryData()

            If DataAccess.spExecLockRequest("sp_GetJobIDLock", .BRID, .JBID, .Transaction) Then

                .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = .BRID
                .SqlDataAdapter.SelectCommand.Parameters("@JBID").Value = .JBID
                .SqlDataAdapter.Fill(.DsGTMS)
                .DataRow = .DsGTMS.Jobs(0)

                .FillData()

            Else
                Throw New ObjectLockedException
            End If
        End With

        Return gui
    End Function

    Public Shared Function CopyFromLead(ByVal BRID As Int32, ByVal LDID As Int64) As frmBooking
        Dim gui As New frmBooking

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            .BRID = BRID
            .LoadTreeViews()

            .Transaction = .SqlConnection.BeginTransaction(IsolationLevel.ReadUncommitted)
            .FillPreliminaryData()

            ' Copy Lead to Job and find JBID
            Dim JBID As Int64
            Dim AlreadyConverted As Boolean = False
            If Not sp_CopyLeadToJob(BRID, LDID, JBID, .Transaction) Then
                Message.ShowMessage("This quote request has already been converted to a job.", MessageBoxIcon.Exclamation)
                AlreadyConverted = True
            End If
            .JBID = JBID

            If DataAccess.spExecLockRequest("sp_GetJobIDLock", .BRID, .JBID, .Transaction) Then

                .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = BRID
                .SqlDataAdapter.SelectCommand.Parameters("@JBID").Value = JBID
                .SqlDataAdapter.Fill(.DsGTMS)
                .DataRow = .DsGTMS.Jobs(0)
                If .DataRow("JBIsCancelled") = True Then
                    ' This must be an existing job which has been cancelled. Set it back to uncancelled, but leave existing data
                    .DataRow("JBIsCancelled") = False
                End If

                If Not AlreadyConverted Then
                    sp_AddCoreMaterialsToJob(.BRID, .JBID, .Transaction)
                End If

                .FillData()

            Else
                Throw New ObjectLockedException
            End If
        End With

        Return gui
    End Function

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        ReadMRU(txtJBJobDescription, UserAppDataPath)
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupLine4 As Power.Forms.GroupLine
    Friend WithEvents GroupLine3 As Power.Forms.GroupLine
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupLine1 As Power.Forms.GroupLine
    Friend WithEvents GroupLine2 As Power.Forms.GroupLine
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents DsGTMS As WindowsApplication.dsGTMS
    Friend WithEvents colNIName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIUserType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents GroupLine18 As Power.Forms.GroupLine
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents GroupLine20 As Power.Forms.GroupLine
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents GroupLine21 As Power.Forms.GroupLine
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents tvGranite As Power.Forms.TreeView
    Friend WithEvents ListBox As Power.Forms.ListBox
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents txtEXIDBookingMeasurer As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents rgJBJobAddressAsAbove As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents txtJBJobStreetAddress02 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBJobSuburb As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBJobState As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBJobPostCode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBJobStreetAddress01 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientSurname As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientFirstName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientReferenceName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientStreetAddress02 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientStreetAddress01 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientSuburb As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientState As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientPostCode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents colNIPaidByClient1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIOrderedByClient1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIDesc1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIName1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents RepositoryItemLookUpEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents dvAppliances As System.Data.DataView
    Friend WithEvents dgAppliances As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvAppliances As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colNIName2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIUserType1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIOrderedBy As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIPaidBy As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnAddAppliance As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEditAppliance As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveAppliance As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colNIBrand As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daJobNonCoreSalesItems As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents btnAddOtherTrade As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gvOtherTrades As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btnEditOtherTrade As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveOtherTrade As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dvOtherTrades As System.Data.DataView
    Friend WithEvents dgOtherTrades As DevExpress.XtraGrid.GridControl
    Friend WithEvents colNIOrderedBy1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIPaidBy1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnAddAppointment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEditAppointment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveAppointment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents dgGranite As DevExpress.XtraGrid.GridControl
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daJobs_Materials As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents colMTName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colJMPrimaryQuoted As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gvGranite As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btnRemoveGranite As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAddGranite As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblJobState As System.Windows.Forms.Label
    Friend WithEvents lblJobStreetAddress As System.Windows.Forms.Label
    Friend WithEvents lblJobSuburb As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents RadioGroup1 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents chkJBIsAllOrderingComplete As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents PnlJobInformation As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlJobAddress As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlGranite As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlScheduling As DevExpress.XtraEditors.GroupControl
    Friend WithEvents PnlOtherTrades As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlAppliances As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlClientInformation As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtJBJobDescription As DevExpress.XtraEditors.MRUEdit
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtJBClientPhoneNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientFaxNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientMobileNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtJBClientEmail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents colAPBegin As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEXName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dgAppointments As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvAppointments As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btnHelp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents colMTPrimaryUOMShortName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents pnlMain As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnViewCalendar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupLine9 As Power.Forms.GroupLine
    Friend WithEvents GroupLine8 As Power.Forms.GroupLine
    Friend WithEvents txtJBPriceQuoted As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupLine5 As Power.Forms.GroupLine
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cmHistory As System.Windows.Forms.ContextMenu
    Friend WithEvents miClearHistory As System.Windows.Forms.MenuItem
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents dvAppointments As System.Data.DataView
    Friend WithEvents DsCalendar As WindowsApplication.dsCalendar
    Friend WithEvents daExpenses As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daAppointments As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents Storage As DevExpress.XtraScheduler.SchedulerStorage
    Friend WithEvents daTasks As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents colTAName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dvResources As System.Data.DataView
    Friend WithEvents dvDirectLabour As System.Data.DataView
    Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents dvSalesReps As System.Data.DataView
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents dgNotes As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvNotes As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colINUser As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colINDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colINNotes As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents daIDNotes As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents dvExpenses As System.Data.DataView
    Friend WithEvents pnlNotes As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtAmountGranite As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents txtUser As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents RepositoryItemMemoEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit
    Friend WithEvents txtINDate As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblJobPostCode As System.Windows.Forms.Label
    Friend WithEvents txtLDUser As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents dpLDDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmBooking))
        Me.RadioGroup1 = New DevExpress.XtraEditors.RadioGroup
        Me.DsGTMS = New WindowsApplication.dsGTMS
        Me.Label5 = New System.Windows.Forms.Label
        Me.chkJBIsAllOrderingComplete = New DevExpress.XtraEditors.CheckEdit
        Me.GroupLine1 = New Power.Forms.GroupLine
        Me.txtEXIDBookingMeasurer = New DevExpress.XtraEditors.LookUpEdit
        Me.dvDirectLabour = New System.Data.DataView
        Me.DsCalendar = New WindowsApplication.dsCalendar
        Me.dvResources = New System.Data.DataView
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.GroupLine2 = New Power.Forms.GroupLine
        Me.Label15 = New System.Windows.Forms.Label
        Me.GroupLine3 = New Power.Forms.GroupLine
        Me.GroupLine4 = New Power.Forms.GroupLine
        Me.btnAddOtherTrade = New DevExpress.XtraEditors.SimpleButton
        Me.dgOtherTrades = New DevExpress.XtraGrid.GridControl
        Me.dvOtherTrades = New System.Data.DataView
        Me.gvOtherTrades = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colNIName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIUserType = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIOrderedBy1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIPaidBy1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.btnEditOtherTrade = New DevExpress.XtraEditors.SimpleButton
        Me.btnRemoveOtherTrade = New DevExpress.XtraEditors.SimpleButton
        Me.dvAppliances = New System.Data.DataView
        Me.btnAddAppliance = New DevExpress.XtraEditors.SimpleButton
        Me.dgAppliances = New DevExpress.XtraGrid.GridControl
        Me.gvAppliances = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colNIUserType1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIBrand = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIName2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIOrderedBy = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIPaidBy = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.btnEditAppliance = New DevExpress.XtraEditors.SimpleButton
        Me.btnRemoveAppliance = New DevExpress.XtraEditors.SimpleButton
        Me.btnAddAppointment = New DevExpress.XtraEditors.SimpleButton
        Me.btnEditAppointment = New DevExpress.XtraEditors.SimpleButton
        Me.btnRemoveAppointment = New DevExpress.XtraEditors.SimpleButton
        Me.txtJBClientSurname = New DevExpress.XtraEditors.TextEdit
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.GroupLine18 = New Power.Forms.GroupLine
        Me.Label36 = New System.Windows.Forms.Label
        Me.GroupLine20 = New Power.Forms.GroupLine
        Me.Label37 = New System.Windows.Forms.Label
        Me.Label38 = New System.Windows.Forms.Label
        Me.Label39 = New System.Windows.Forms.Label
        Me.GroupLine21 = New Power.Forms.GroupLine
        Me.Label40 = New System.Windows.Forms.Label
        Me.Label41 = New System.Windows.Forms.Label
        Me.txtJBClientFirstName = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientReferenceName = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientStreetAddress02 = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientStreetAddress01 = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientSuburb = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientPhoneNumber = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientFaxNumber = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientMobileNumber = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientState = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientPostCode = New DevExpress.XtraEditors.TextEdit
        Me.pnlScheduling = New DevExpress.XtraEditors.GroupControl
        Me.btnViewCalendar = New DevExpress.XtraEditors.SimpleButton
        Me.GroupLine9 = New Power.Forms.GroupLine
        Me.GroupLine8 = New Power.Forms.GroupLine
        Me.Label7 = New System.Windows.Forms.Label
        Me.dgAppointments = New DevExpress.XtraGrid.GridControl
        Me.dvAppointments = New System.Data.DataView
        Me.gvAppointments = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colAPBegin = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colEXName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colTAName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.PnlOtherTrades = New DevExpress.XtraEditors.GroupControl
        Me.pnlGranite = New DevExpress.XtraEditors.GroupControl
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.btnRemoveGranite = New DevExpress.XtraEditors.SimpleButton
        Me.btnAddGranite = New DevExpress.XtraEditors.SimpleButton
        Me.tvGranite = New Power.Forms.TreeView
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.dgGranite = New DevExpress.XtraGrid.GridControl
        Me.gvGranite = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colMTName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colJMPrimaryQuoted = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtAmountGranite = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colMTPrimaryUOMShortName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.pnlAppliances = New DevExpress.XtraEditors.GroupControl
        Me.PnlJobInformation = New DevExpress.XtraEditors.GroupControl
        Me.dpLDDate = New DevExpress.XtraEditors.DateEdit
        Me.txtLDUser = New DevExpress.XtraEditors.LookUpEdit
        Me.dvExpenses = New System.Data.DataView
        Me.Label22 = New System.Windows.Forms.Label
        Me.LookUpEdit1 = New DevExpress.XtraEditors.LookUpEdit
        Me.dvSalesReps = New System.Data.DataView
        Me.Label21 = New System.Windows.Forms.Label
        Me.txtJBPriceQuoted = New DevExpress.XtraEditors.TextEdit
        Me.Label8 = New System.Windows.Forms.Label
        Me.GroupLine5 = New Power.Forms.GroupLine
        Me.txtJBJobDescription = New DevExpress.XtraEditors.MRUEdit
        Me.cmHistory = New System.Windows.Forms.ContextMenu
        Me.miClearHistory = New System.Windows.Forms.MenuItem
        Me.Label19 = New System.Windows.Forms.Label
        Me.pnlJobAddress = New DevExpress.XtraEditors.GroupControl
        Me.lblJobPostCode = New System.Windows.Forms.Label
        Me.txtJBJobStreetAddress01 = New DevExpress.XtraEditors.TextEdit
        Me.rgJBJobAddressAsAbove = New DevExpress.XtraEditors.RadioGroup
        Me.Label17 = New System.Windows.Forms.Label
        Me.lblJobState = New System.Windows.Forms.Label
        Me.lblJobStreetAddress = New System.Windows.Forms.Label
        Me.lblJobSuburb = New System.Windows.Forms.Label
        Me.txtJBJobStreetAddress02 = New DevExpress.XtraEditors.TextEdit
        Me.txtJBJobSuburb = New DevExpress.XtraEditors.TextEdit
        Me.txtJBJobState = New DevExpress.XtraEditors.TextEdit
        Me.txtJBJobPostCode = New DevExpress.XtraEditors.TextEdit
        Me.pnlClientInformation = New DevExpress.XtraEditors.GroupControl
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtJBClientEmail = New DevExpress.XtraEditors.TextEdit
        Me.Label20 = New System.Windows.Forms.Label
        Me.ListBox = New Power.Forms.ListBox
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.colNIPaidByClient1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIOrderedByClient1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIDesc1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIName1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemLookUpEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.daJobNonCoreSalesItems = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.daJobs_Materials = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand5 = New System.Data.SqlClient.SqlCommand
        Me.btnHelp = New DevExpress.XtraEditors.SimpleButton
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider
        Me.pnlMain = New DevExpress.XtraEditors.PanelControl
        Me.pnlNotes = New DevExpress.XtraEditors.GroupControl
        Me.dgNotes = New DevExpress.XtraGrid.GridControl
        Me.gvNotes = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colINUser = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtUser = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.colINDate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtINDate = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.colINNotes = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemMemoEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit
        Me.daExpenses = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.daAppointments = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand
        Me.Storage = New DevExpress.XtraScheduler.SchedulerStorage(Me.components)
        Me.daTasks = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand2 = New System.Data.SqlClient.SqlCommand
        Me.daIDNotes = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand6 = New System.Data.SqlClient.SqlCommand
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkJBIsAllOrderingComplete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEXIDBookingMeasurer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvDirectLabour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsCalendar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvResources, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgOtherTrades, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvOtherTrades, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvOtherTrades, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvAppliances, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgAppliances, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvAppliances, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientFirstName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientReferenceName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientSuburb.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientPhoneNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientFaxNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientMobileNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientState.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientPostCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlScheduling, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlScheduling.SuspendLayout()
        CType(Me.dgAppointments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvAppointments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvAppointments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PnlOtherTrades, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PnlOtherTrades.SuspendLayout()
        CType(Me.pnlGranite, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlGranite.SuspendLayout()
        CType(Me.dgGranite, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvGranite, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAmountGranite, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlAppliances, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlAppliances.SuspendLayout()
        CType(Me.PnlJobInformation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PnlJobInformation.SuspendLayout()
        CType(Me.dpLDDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDUser.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvExpenses, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvSalesReps, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBPriceQuoted.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBJobDescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlJobAddress, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlJobAddress.SuspendLayout()
        CType(Me.txtJBJobStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgJBJobAddressAsAbove.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBJobStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBJobSuburb.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBJobState.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBJobPostCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlClientInformation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlClientInformation.SuspendLayout()
        CType(Me.txtJBClientEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemLookUpEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMain.SuspendLayout()
        CType(Me.pnlNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlNotes.SuspendLayout()
        CType(Me.dgNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUser, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtINDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemMemoEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Storage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RadioGroup1
        '
        Me.RadioGroup1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBIsMeasureOnSite"))
        Me.RadioGroup1.Location = New System.Drawing.Point(352, 176)
        Me.RadioGroup1.Name = "RadioGroup1"
        '
        'RadioGroup1.Properties
        '
        Me.RadioGroup1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.RadioGroup1.Properties.Appearance.Options.UseBackColor = True
        Me.RadioGroup1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.RadioGroup1.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "On site"), New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "Office")})
        Me.RadioGroup1.Size = New System.Drawing.Size(64, 40)
        Me.RadioGroup1.TabIndex = 3
        '
        'DsGTMS
        '
        Me.DsGTMS.DataSetName = "dsGTMS"
        Me.DsGTMS.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(48, 328)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(464, 16)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Tick ""All ordering complete"" when all orders (both internal and external) have be" & _
        "en placed."
        '
        'chkJBIsAllOrderingComplete
        '
        Me.chkJBIsAllOrderingComplete.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBIsAllOrderingComplete"))
        Me.chkJBIsAllOrderingComplete.Location = New System.Drawing.Point(136, 352)
        Me.chkJBIsAllOrderingComplete.Name = "chkJBIsAllOrderingComplete"
        '
        'chkJBIsAllOrderingComplete.Properties
        '
        Me.chkJBIsAllOrderingComplete.Properties.Caption = "All ordering complete"
        Me.chkJBIsAllOrderingComplete.Size = New System.Drawing.Size(176, 19)
        Me.chkJBIsAllOrderingComplete.TabIndex = 5
        '
        'GroupLine1
        '
        Me.GroupLine1.Location = New System.Drawing.Point(24, 384)
        Me.GroupLine1.Name = "GroupLine1"
        Me.GroupLine1.Size = New System.Drawing.Size(480, 16)
        Me.GroupLine1.TabIndex = 4
        Me.GroupLine1.TextString = "Recorded By / Date"
        Me.GroupLine1.TextWidth = 110
        '
        'txtEXIDBookingMeasurer
        '
        Me.txtEXIDBookingMeasurer.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.EXIDBookingMeasurer"))
        Me.txtEXIDBookingMeasurer.Location = New System.Drawing.Point(200, 152)
        Me.txtEXIDBookingMeasurer.Name = "txtEXIDBookingMeasurer"
        '
        'txtEXIDBookingMeasurer.Properties
        '
        Me.txtEXIDBookingMeasurer.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True
        Me.txtEXIDBookingMeasurer.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtEXIDBookingMeasurer.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXName", "Name", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.txtEXIDBookingMeasurer.Properties.DataSource = Me.dvDirectLabour
        Me.txtEXIDBookingMeasurer.Properties.DisplayMember = "EXName"
        Me.txtEXIDBookingMeasurer.Properties.NullText = ""
        Me.txtEXIDBookingMeasurer.Properties.ShowFooter = False
        Me.txtEXIDBookingMeasurer.Properties.ShowHeader = False
        Me.txtEXIDBookingMeasurer.Properties.ShowLines = False
        Me.txtEXIDBookingMeasurer.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.txtEXIDBookingMeasurer.Properties.ValueMember = "EXID"
        Me.txtEXIDBookingMeasurer.Size = New System.Drawing.Size(176, 20)
        Me.txtEXIDBookingMeasurer.TabIndex = 2
        '
        'dvDirectLabour
        '
        Me.dvDirectLabour.RowFilter = "EGType = 'DL'"
        Me.dvDirectLabour.Sort = "EXName"
        Me.dvDirectLabour.Table = Me.DsCalendar.VExpenses
        '
        'DsCalendar
        '
        Me.DsCalendar.DataSetName = "dsCalendar"
        Me.DsCalendar.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'dvResources
        '
        Me.dvResources.RowFilter = "EXAppearInCalendar = 1"
        Me.dvResources.Sort = "EGName, EXName"
        Me.dvResources.Table = Me.DsCalendar.VExpenses
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(48, 152)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(136, 21)
        Me.Label12.TabIndex = 2
        Me.Label12.Text = "Templated/measured by:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(48, 72)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(96, 21)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Job description:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(312, 408)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 21)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Date:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(48, 184)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(304, 21)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Will the template/measure be done on site or in the office?"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine2
        '
        Me.GroupLine2.Location = New System.Drawing.Point(24, 24)
        Me.GroupLine2.Name = "GroupLine2"
        Me.GroupLine2.Size = New System.Drawing.Size(480, 16)
        Me.GroupLine2.TabIndex = 4
        Me.GroupLine2.TextString = "Job Description"
        Me.GroupLine2.TextWidth = 82
        '
        'Label15
        '
        Me.Label15.Location = New System.Drawing.Point(48, 48)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(432, 16)
        Me.Label15.TabIndex = 7
        Me.Label15.Text = "This is entered at the appointment stage, and may be changed in the booking stage" & _
        "."
        '
        'GroupLine3
        '
        Me.GroupLine3.Location = New System.Drawing.Point(24, 104)
        Me.GroupLine3.Name = "GroupLine3"
        Me.GroupLine3.Size = New System.Drawing.Size(480, 16)
        Me.GroupLine3.TabIndex = 4
        Me.GroupLine3.TextString = "Templater/Measure and Salesperson Information"
        Me.GroupLine3.TextWidth = 250
        '
        'GroupLine4
        '
        Me.GroupLine4.Location = New System.Drawing.Point(24, 304)
        Me.GroupLine4.Name = "GroupLine4"
        Me.GroupLine4.Size = New System.Drawing.Size(480, 16)
        Me.GroupLine4.TabIndex = 4
        Me.GroupLine4.TextString = "Ordering"
        Me.GroupLine4.TextWidth = 50
        '
        'btnAddOtherTrade
        '
        Me.btnAddOtherTrade.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddOtherTrade.Image = CType(resources.GetObject("btnAddOtherTrade.Image"), System.Drawing.Image)
        Me.btnAddOtherTrade.Location = New System.Drawing.Point(16, 352)
        Me.btnAddOtherTrade.Name = "btnAddOtherTrade"
        Me.btnAddOtherTrade.Size = New System.Drawing.Size(72, 23)
        Me.btnAddOtherTrade.TabIndex = 1
        Me.btnAddOtherTrade.Text = "Add..."
        '
        'dgOtherTrades
        '
        Me.dgOtherTrades.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgOtherTrades.DataSource = Me.dvOtherTrades
        '
        'dgOtherTrades.EmbeddedNavigator
        '
        Me.dgOtherTrades.EmbeddedNavigator.Name = ""
        Me.dgOtherTrades.Location = New System.Drawing.Point(16, 32)
        Me.dgOtherTrades.MainView = Me.gvOtherTrades
        Me.dgOtherTrades.Name = "dgOtherTrades"
        Me.dgOtherTrades.Size = New System.Drawing.Size(440, 312)
        Me.dgOtherTrades.TabIndex = 0
        Me.dgOtherTrades.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvOtherTrades})
        '
        'dvOtherTrades
        '
        Me.dvOtherTrades.RowFilter = "NIType = 'OT'"
        Me.dvOtherTrades.Table = Me.DsGTMS.VJobsNonCoreSalesItems
        '
        'gvOtherTrades
        '
        Me.gvOtherTrades.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.gvOtherTrades.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvOtherTrades.Appearance.FocusedCell.Options.UseBackColor = True
        Me.gvOtherTrades.Appearance.FocusedCell.Options.UseForeColor = True
        Me.gvOtherTrades.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.gvOtherTrades.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gvOtherTrades.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvOtherTrades.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvOtherTrades.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colNIName, Me.colNIUserType, Me.colNIOrderedBy1, Me.colNIPaidBy1})
        Me.gvOtherTrades.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.gvOtherTrades.GridControl = Me.dgOtherTrades
        Me.gvOtherTrades.Name = "gvOtherTrades"
        Me.gvOtherTrades.OptionsBehavior.Editable = False
        Me.gvOtherTrades.OptionsCustomization.AllowFilter = False
        Me.gvOtherTrades.OptionsNavigation.AutoFocusNewRow = True
        Me.gvOtherTrades.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.gvOtherTrades.OptionsView.ShowGroupPanel = False
        Me.gvOtherTrades.OptionsView.ShowHorzLines = False
        Me.gvOtherTrades.OptionsView.ShowIndicator = False
        Me.gvOtherTrades.OptionsView.ShowVertLines = False
        Me.gvOtherTrades.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colNIName, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colNIName
        '
        Me.colNIName.Caption = "Name"
        Me.colNIName.FieldName = "NIName"
        Me.colNIName.Name = "colNIName"
        Me.colNIName.Visible = True
        Me.colNIName.VisibleIndex = 0
        '
        'colNIUserType
        '
        Me.colNIUserType.Caption = "Trade Type"
        Me.colNIUserType.FieldName = "NIUserType"
        Me.colNIUserType.Name = "colNIUserType"
        Me.colNIUserType.Visible = True
        Me.colNIUserType.VisibleIndex = 1
        '
        'colNIOrderedBy1
        '
        Me.colNIOrderedBy1.Caption = "Ordered By"
        Me.colNIOrderedBy1.FieldName = "NIOrderedBy"
        Me.colNIOrderedBy1.Name = "colNIOrderedBy1"
        Me.colNIOrderedBy1.Visible = True
        Me.colNIOrderedBy1.VisibleIndex = 2
        '
        'colNIPaidBy1
        '
        Me.colNIPaidBy1.Caption = "Paid By"
        Me.colNIPaidBy1.FieldName = "NIPaidBy"
        Me.colNIPaidBy1.Name = "colNIPaidBy1"
        Me.colNIPaidBy1.Visible = True
        Me.colNIPaidBy1.VisibleIndex = 3
        '
        'btnEditOtherTrade
        '
        Me.btnEditOtherTrade.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEditOtherTrade.Image = CType(resources.GetObject("btnEditOtherTrade.Image"), System.Drawing.Image)
        Me.btnEditOtherTrade.Location = New System.Drawing.Point(96, 352)
        Me.btnEditOtherTrade.Name = "btnEditOtherTrade"
        Me.btnEditOtherTrade.Size = New System.Drawing.Size(72, 23)
        Me.btnEditOtherTrade.TabIndex = 2
        Me.btnEditOtherTrade.Text = "Edit..."
        '
        'btnRemoveOtherTrade
        '
        Me.btnRemoveOtherTrade.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveOtherTrade.Image = CType(resources.GetObject("btnRemoveOtherTrade.Image"), System.Drawing.Image)
        Me.btnRemoveOtherTrade.Location = New System.Drawing.Point(176, 352)
        Me.btnRemoveOtherTrade.Name = "btnRemoveOtherTrade"
        Me.btnRemoveOtherTrade.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveOtherTrade.TabIndex = 3
        Me.btnRemoveOtherTrade.Text = "Remove"
        '
        'dvAppliances
        '
        Me.dvAppliances.RowFilter = "NIType = 'AP'"
        Me.dvAppliances.Table = Me.DsGTMS.VJobsNonCoreSalesItems
        '
        'btnAddAppliance
        '
        Me.btnAddAppliance.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddAppliance.Image = CType(resources.GetObject("btnAddAppliance.Image"), System.Drawing.Image)
        Me.btnAddAppliance.Location = New System.Drawing.Point(16, 320)
        Me.btnAddAppliance.Name = "btnAddAppliance"
        Me.btnAddAppliance.Size = New System.Drawing.Size(72, 23)
        Me.btnAddAppliance.TabIndex = 1
        Me.btnAddAppliance.Text = "Add..."
        '
        'dgAppliances
        '
        Me.dgAppliances.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgAppliances.DataSource = Me.dvAppliances
        '
        'dgAppliances.EmbeddedNavigator
        '
        Me.dgAppliances.EmbeddedNavigator.Name = ""
        Me.dgAppliances.Location = New System.Drawing.Point(16, 32)
        Me.dgAppliances.MainView = Me.gvAppliances
        Me.dgAppliances.Name = "dgAppliances"
        Me.dgAppliances.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemComboBox1})
        Me.dgAppliances.Size = New System.Drawing.Size(464, 280)
        Me.dgAppliances.TabIndex = 0
        Me.dgAppliances.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvAppliances, Me.GridView2})
        '
        'gvAppliances
        '
        Me.gvAppliances.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.gvAppliances.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvAppliances.Appearance.FocusedCell.Options.UseBackColor = True
        Me.gvAppliances.Appearance.FocusedCell.Options.UseForeColor = True
        Me.gvAppliances.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.gvAppliances.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gvAppliances.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvAppliances.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvAppliances.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colNIUserType1, Me.colNIBrand, Me.colNIName2, Me.colNIOrderedBy, Me.colNIPaidBy})
        Me.gvAppliances.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.gvAppliances.GridControl = Me.dgAppliances
        Me.gvAppliances.Name = "gvAppliances"
        Me.gvAppliances.OptionsBehavior.Editable = False
        Me.gvAppliances.OptionsCustomization.AllowFilter = False
        Me.gvAppliances.OptionsNavigation.AutoFocusNewRow = True
        Me.gvAppliances.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.gvAppliances.OptionsView.ShowGroupPanel = False
        Me.gvAppliances.OptionsView.ShowHorzLines = False
        Me.gvAppliances.OptionsView.ShowIndicator = False
        Me.gvAppliances.OptionsView.ShowVertLines = False
        Me.gvAppliances.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colNIUserType1, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colNIUserType1
        '
        Me.colNIUserType1.Caption = "Appliance Type"
        Me.colNIUserType1.FieldName = "NIUserType"
        Me.colNIUserType1.Name = "colNIUserType1"
        Me.colNIUserType1.Visible = True
        Me.colNIUserType1.VisibleIndex = 0
        Me.colNIUserType1.Width = 176
        '
        'colNIBrand
        '
        Me.colNIBrand.Caption = "Brand"
        Me.colNIBrand.FieldName = "NIBrand"
        Me.colNIBrand.Name = "colNIBrand"
        Me.colNIBrand.Visible = True
        Me.colNIBrand.VisibleIndex = 1
        Me.colNIBrand.Width = 166
        '
        'colNIName2
        '
        Me.colNIName2.Caption = "Model Number"
        Me.colNIName2.FieldName = "NIName"
        Me.colNIName2.Name = "colNIName2"
        Me.colNIName2.Visible = True
        Me.colNIName2.VisibleIndex = 2
        Me.colNIName2.Width = 162
        '
        'colNIOrderedBy
        '
        Me.colNIOrderedBy.Caption = "Ordered By"
        Me.colNIOrderedBy.FieldName = "NIOrderedBy"
        Me.colNIOrderedBy.Name = "colNIOrderedBy"
        Me.colNIOrderedBy.Visible = True
        Me.colNIOrderedBy.VisibleIndex = 3
        Me.colNIOrderedBy.Width = 168
        '
        'colNIPaidBy
        '
        Me.colNIPaidBy.Caption = "Paid By"
        Me.colNIPaidBy.FieldName = "NIPaidBy"
        Me.colNIPaidBy.Name = "colNIPaidBy"
        Me.colNIPaidBy.Visible = True
        Me.colNIPaidBy.VisibleIndex = 4
        Me.colNIPaidBy.Width = 171
        '
        'RepositoryItemComboBox1
        '
        Me.RepositoryItemComboBox1.AutoHeight = False
        Me.RepositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox1.Items.AddRange(New Object() {"Granite Transformations", "Customer"})
        Me.RepositoryItemComboBox1.Name = "RepositoryItemComboBox1"
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.dgAppliances
        Me.GridView2.Name = "GridView2"
        '
        'btnEditAppliance
        '
        Me.btnEditAppliance.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEditAppliance.Image = CType(resources.GetObject("btnEditAppliance.Image"), System.Drawing.Image)
        Me.btnEditAppliance.Location = New System.Drawing.Point(96, 320)
        Me.btnEditAppliance.Name = "btnEditAppliance"
        Me.btnEditAppliance.Size = New System.Drawing.Size(72, 23)
        Me.btnEditAppliance.TabIndex = 2
        Me.btnEditAppliance.Text = "Edit..."
        '
        'btnRemoveAppliance
        '
        Me.btnRemoveAppliance.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveAppliance.Image = CType(resources.GetObject("btnRemoveAppliance.Image"), System.Drawing.Image)
        Me.btnRemoveAppliance.Location = New System.Drawing.Point(176, 320)
        Me.btnRemoveAppliance.Name = "btnRemoveAppliance"
        Me.btnRemoveAppliance.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveAppliance.TabIndex = 3
        Me.btnRemoveAppliance.Text = "Remove"
        '
        'btnAddAppointment
        '
        Me.btnAddAppointment.Image = CType(resources.GetObject("btnAddAppointment.Image"), System.Drawing.Image)
        Me.btnAddAppointment.Location = New System.Drawing.Point(48, 408)
        Me.btnAddAppointment.Name = "btnAddAppointment"
        Me.btnAddAppointment.Size = New System.Drawing.Size(72, 23)
        Me.btnAddAppointment.TabIndex = 2
        Me.btnAddAppointment.Text = "Add..."
        '
        'btnEditAppointment
        '
        Me.btnEditAppointment.Image = CType(resources.GetObject("btnEditAppointment.Image"), System.Drawing.Image)
        Me.btnEditAppointment.Location = New System.Drawing.Point(128, 408)
        Me.btnEditAppointment.Name = "btnEditAppointment"
        Me.btnEditAppointment.Size = New System.Drawing.Size(72, 23)
        Me.btnEditAppointment.TabIndex = 3
        Me.btnEditAppointment.Text = "Edit..."
        '
        'btnRemoveAppointment
        '
        Me.btnRemoveAppointment.Image = CType(resources.GetObject("btnRemoveAppointment.Image"), System.Drawing.Image)
        Me.btnRemoveAppointment.Location = New System.Drawing.Point(208, 408)
        Me.btnRemoveAppointment.Name = "btnRemoveAppointment"
        Me.btnRemoveAppointment.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveAppointment.TabIndex = 4
        Me.btnRemoveAppointment.Text = "Remove"
        '
        'txtJBClientSurname
        '
        Me.txtJBClientSurname.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBClientSurname"))
        Me.txtJBClientSurname.EditValue = ""
        Me.txtJBClientSurname.Location = New System.Drawing.Point(184, 48)
        Me.txtJBClientSurname.Name = "txtJBClientSurname"
        Me.txtJBClientSurname.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientSurname.TabIndex = 0
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(40, 336)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(80, 21)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Phone:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(40, 368)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(80, 21)
        Me.Label11.TabIndex = 19
        Me.Label11.Text = "Fax:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label13
        '
        Me.Label13.Location = New System.Drawing.Point(40, 400)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(80, 21)
        Me.Label13.TabIndex = 20
        Me.Label13.Text = "Mobile:"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine18
        '
        Me.GroupLine18.Location = New System.Drawing.Point(16, 312)
        Me.GroupLine18.Name = "GroupLine18"
        Me.GroupLine18.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine18.TabIndex = 26
        Me.GroupLine18.TextString = "Contact Details"
        Me.GroupLine18.TextWidth = 79
        '
        'Label36
        '
        Me.Label36.Location = New System.Drawing.Point(40, 112)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(96, 21)
        Me.Label36.TabIndex = 23
        Me.Label36.Text = "Reference name:"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine20
        '
        Me.GroupLine20.Location = New System.Drawing.Point(16, 24)
        Me.GroupLine20.Name = "GroupLine20"
        Me.GroupLine20.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine20.TabIndex = 25
        Me.GroupLine20.TextString = "Name"
        Me.GroupLine20.TextWidth = 35
        '
        'Label37
        '
        Me.Label37.Location = New System.Drawing.Point(40, 272)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(80, 21)
        Me.Label37.TabIndex = 21
        Me.Label37.Text = "State/region:"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label38
        '
        Me.Label38.Location = New System.Drawing.Point(40, 80)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(72, 21)
        Me.Label38.TabIndex = 15
        Me.Label38.Text = "First name:"
        Me.Label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label39
        '
        Me.Label39.Location = New System.Drawing.Point(40, 48)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(144, 21)
        Me.Label39.TabIndex = 17
        Me.Label39.Text = "Surname / business name:"
        Me.Label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine21
        '
        Me.GroupLine21.Location = New System.Drawing.Point(16, 152)
        Me.GroupLine21.Name = "GroupLine21"
        Me.GroupLine21.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine21.TabIndex = 27
        Me.GroupLine21.TextString = "Address"
        Me.GroupLine21.TextWidth = 50
        '
        'Label40
        '
        Me.Label40.Location = New System.Drawing.Point(40, 176)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(80, 21)
        Me.Label40.TabIndex = 16
        Me.Label40.Text = "Street address:"
        Me.Label40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label41
        '
        Me.Label41.Location = New System.Drawing.Point(40, 240)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(80, 21)
        Me.Label41.TabIndex = 22
        Me.Label41.Text = "Suburb/town:"
        Me.Label41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJBClientFirstName
        '
        Me.txtJBClientFirstName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBClientFirstName"))
        Me.txtJBClientFirstName.EditValue = ""
        Me.txtJBClientFirstName.Location = New System.Drawing.Point(184, 80)
        Me.txtJBClientFirstName.Name = "txtJBClientFirstName"
        Me.txtJBClientFirstName.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientFirstName.TabIndex = 1
        '
        'txtJBClientReferenceName
        '
        Me.txtJBClientReferenceName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBReferenceName"))
        Me.txtJBClientReferenceName.EditValue = ""
        Me.txtJBClientReferenceName.Location = New System.Drawing.Point(184, 112)
        Me.txtJBClientReferenceName.Name = "txtJBClientReferenceName"
        Me.txtJBClientReferenceName.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientReferenceName.TabIndex = 2
        '
        'txtJBClientStreetAddress02
        '
        Me.txtJBClientStreetAddress02.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBClientStreetAddress02"))
        Me.txtJBClientStreetAddress02.EditValue = ""
        Me.txtJBClientStreetAddress02.Location = New System.Drawing.Point(184, 208)
        Me.txtJBClientStreetAddress02.Name = "txtJBClientStreetAddress02"
        Me.txtJBClientStreetAddress02.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientStreetAddress02.TabIndex = 4
        '
        'txtJBClientStreetAddress01
        '
        Me.txtJBClientStreetAddress01.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBClientStreetAddress01"))
        Me.txtJBClientStreetAddress01.EditValue = ""
        Me.txtJBClientStreetAddress01.Location = New System.Drawing.Point(184, 176)
        Me.txtJBClientStreetAddress01.Name = "txtJBClientStreetAddress01"
        Me.txtJBClientStreetAddress01.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientStreetAddress01.TabIndex = 3
        '
        'txtJBClientSuburb
        '
        Me.txtJBClientSuburb.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBClientSuburb"))
        Me.txtJBClientSuburb.EditValue = ""
        Me.txtJBClientSuburb.Location = New System.Drawing.Point(184, 240)
        Me.txtJBClientSuburb.Name = "txtJBClientSuburb"
        Me.txtJBClientSuburb.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientSuburb.TabIndex = 5
        '
        'txtJBClientPhoneNumber
        '
        Me.txtJBClientPhoneNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBClientPhoneNumber"))
        Me.txtJBClientPhoneNumber.EditValue = ""
        Me.txtJBClientPhoneNumber.Location = New System.Drawing.Point(184, 336)
        Me.txtJBClientPhoneNumber.Name = "txtJBClientPhoneNumber"
        Me.txtJBClientPhoneNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientPhoneNumber.TabIndex = 8
        '
        'txtJBClientFaxNumber
        '
        Me.txtJBClientFaxNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBClientFaxNumber"))
        Me.txtJBClientFaxNumber.EditValue = ""
        Me.txtJBClientFaxNumber.Location = New System.Drawing.Point(184, 368)
        Me.txtJBClientFaxNumber.Name = "txtJBClientFaxNumber"
        Me.txtJBClientFaxNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientFaxNumber.TabIndex = 9
        '
        'txtJBClientMobileNumber
        '
        Me.txtJBClientMobileNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBClientMobileNumber"))
        Me.txtJBClientMobileNumber.EditValue = ""
        Me.txtJBClientMobileNumber.Location = New System.Drawing.Point(184, 400)
        Me.txtJBClientMobileNumber.Name = "txtJBClientMobileNumber"
        Me.txtJBClientMobileNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientMobileNumber.TabIndex = 10
        '
        'txtJBClientState
        '
        Me.txtJBClientState.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBClientState"))
        Me.txtJBClientState.EditValue = ""
        Me.txtJBClientState.Location = New System.Drawing.Point(184, 272)
        Me.txtJBClientState.Name = "txtJBClientState"
        Me.txtJBClientState.Size = New System.Drawing.Size(104, 20)
        Me.txtJBClientState.TabIndex = 6
        '
        'txtJBClientPostCode
        '
        Me.txtJBClientPostCode.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBClientPostCode"))
        Me.txtJBClientPostCode.EditValue = ""
        Me.txtJBClientPostCode.Location = New System.Drawing.Point(392, 272)
        Me.txtJBClientPostCode.Name = "txtJBClientPostCode"
        Me.txtJBClientPostCode.Size = New System.Drawing.Size(88, 20)
        Me.txtJBClientPostCode.TabIndex = 7
        '
        'pnlScheduling
        '
        Me.pnlScheduling.Controls.Add(Me.btnViewCalendar)
        Me.pnlScheduling.Controls.Add(Me.GroupLine9)
        Me.pnlScheduling.Controls.Add(Me.GroupLine8)
        Me.pnlScheduling.Controls.Add(Me.Label7)
        Me.pnlScheduling.Controls.Add(Me.btnAddAppointment)
        Me.pnlScheduling.Controls.Add(Me.btnEditAppointment)
        Me.pnlScheduling.Controls.Add(Me.btnRemoveAppointment)
        Me.pnlScheduling.Controls.Add(Me.dgAppointments)
        Me.pnlScheduling.Location = New System.Drawing.Point(24, 24)
        Me.pnlScheduling.Name = "pnlScheduling"
        Me.pnlScheduling.Size = New System.Drawing.Size(536, 448)
        Me.pnlScheduling.TabIndex = 15
        Me.pnlScheduling.Text = "Scheduling"
        '
        'btnViewCalendar
        '
        Me.btnViewCalendar.Location = New System.Drawing.Point(48, 88)
        Me.btnViewCalendar.Name = "btnViewCalendar"
        Me.btnViewCalendar.Size = New System.Drawing.Size(136, 23)
        Me.btnViewCalendar.TabIndex = 0
        Me.btnViewCalendar.Text = "View Calendar..."
        '
        'GroupLine9
        '
        Me.GroupLine9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupLine9.Location = New System.Drawing.Point(24, 128)
        Me.GroupLine9.Name = "GroupLine9"
        Me.GroupLine9.Size = New System.Drawing.Size(488, 16)
        Me.GroupLine9.TabIndex = 20
        Me.GroupLine9.TextString = "List View"
        Me.GroupLine9.TextWidth = 50
        '
        'GroupLine8
        '
        Me.GroupLine8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupLine8.Location = New System.Drawing.Point(24, 64)
        Me.GroupLine8.Name = "GroupLine8"
        Me.GroupLine8.Size = New System.Drawing.Size(488, 16)
        Me.GroupLine8.TabIndex = 19
        Me.GroupLine8.TextString = "Calendar View"
        Me.GroupLine8.TextWidth = 80
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(24, 32)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(488, 16)
        Me.Label7.TabIndex = 18
        Me.Label7.Text = "You may add appointments for direct labor using the list or calendar view."
        '
        'dgAppointments
        '
        Me.dgAppointments.DataSource = Me.dvAppointments
        '
        'dgAppointments.EmbeddedNavigator
        '
        Me.dgAppointments.EmbeddedNavigator.Name = ""
        Me.dgAppointments.Location = New System.Drawing.Point(48, 152)
        Me.dgAppointments.MainView = Me.gvAppointments
        Me.dgAppointments.Name = "dgAppointments"
        Me.dgAppointments.Size = New System.Drawing.Size(464, 248)
        Me.dgAppointments.TabIndex = 1
        Me.dgAppointments.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvAppointments})
        '
        'dvAppointments
        '
        Me.dvAppointments.Table = Me.DsCalendar.VAppointments
        '
        'gvAppointments
        '
        Me.gvAppointments.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.gvAppointments.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvAppointments.Appearance.FocusedCell.Options.UseBackColor = True
        Me.gvAppointments.Appearance.FocusedCell.Options.UseForeColor = True
        Me.gvAppointments.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.gvAppointments.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gvAppointments.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvAppointments.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvAppointments.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colAPBegin, Me.colEXName, Me.colTAName})
        Me.gvAppointments.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.gvAppointments.GridControl = Me.dgAppointments
        Me.gvAppointments.Name = "gvAppointments"
        Me.gvAppointments.OptionsBehavior.Editable = False
        Me.gvAppointments.OptionsCustomization.AllowFilter = False
        Me.gvAppointments.OptionsNavigation.AutoFocusNewRow = True
        Me.gvAppointments.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.gvAppointments.OptionsView.ShowGroupPanel = False
        Me.gvAppointments.OptionsView.ShowHorzLines = False
        Me.gvAppointments.OptionsView.ShowIndicator = False
        Me.gvAppointments.OptionsView.ShowVertLines = False
        '
        'colAPBegin
        '
        Me.colAPBegin.Caption = "Date"
        Me.colAPBegin.DisplayFormat.FormatString = "d"
        Me.colAPBegin.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colAPBegin.FieldName = "APBegin"
        Me.colAPBegin.Name = "colAPBegin"
        Me.colAPBegin.Visible = True
        Me.colAPBegin.VisibleIndex = 0
        '
        'colEXName
        '
        Me.colEXName.Caption = "Staff Member"
        Me.colEXName.FieldName = "EXName"
        Me.colEXName.Name = "colEXName"
        Me.colEXName.Visible = True
        Me.colEXName.VisibleIndex = 1
        '
        'colTAName
        '
        Me.colTAName.Caption = "Task"
        Me.colTAName.FieldName = "TAName"
        Me.colTAName.Name = "colTAName"
        Me.colTAName.Visible = True
        Me.colTAName.VisibleIndex = 2
        '
        'PnlOtherTrades
        '
        Me.PnlOtherTrades.Controls.Add(Me.btnAddOtherTrade)
        Me.PnlOtherTrades.Controls.Add(Me.dgOtherTrades)
        Me.PnlOtherTrades.Controls.Add(Me.btnEditOtherTrade)
        Me.PnlOtherTrades.Controls.Add(Me.btnRemoveOtherTrade)
        Me.PnlOtherTrades.Location = New System.Drawing.Point(80, 16)
        Me.PnlOtherTrades.Name = "PnlOtherTrades"
        Me.PnlOtherTrades.Size = New System.Drawing.Size(472, 384)
        Me.PnlOtherTrades.TabIndex = 16
        Me.PnlOtherTrades.Text = "Other Trades"
        '
        'pnlGranite
        '
        Me.pnlGranite.Controls.Add(Me.Label24)
        Me.pnlGranite.Controls.Add(Me.Label25)
        Me.pnlGranite.Controls.Add(Me.btnRemoveGranite)
        Me.pnlGranite.Controls.Add(Me.btnAddGranite)
        Me.pnlGranite.Controls.Add(Me.tvGranite)
        Me.pnlGranite.Controls.Add(Me.dgGranite)
        Me.pnlGranite.Location = New System.Drawing.Point(32, 16)
        Me.pnlGranite.Name = "pnlGranite"
        Me.pnlGranite.Size = New System.Drawing.Size(528, 400)
        Me.pnlGranite.TabIndex = 14
        Me.pnlGranite.Text = "Tracked Material"
        '
        'Label24
        '
        Me.Label24.Location = New System.Drawing.Point(16, 24)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(184, 21)
        Me.Label24.TabIndex = 15
        Me.Label24.Text = "Available tracked material:"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label25
        '
        Me.Label25.Location = New System.Drawing.Point(288, 24)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(224, 21)
        Me.Label25.TabIndex = 14
        Me.Label25.Text = "Extimated tracked material sold in job:"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnRemoveGranite
        '
        Me.btnRemoveGranite.Image = CType(resources.GetObject("btnRemoveGranite.Image"), System.Drawing.Image)
        Me.btnRemoveGranite.Location = New System.Drawing.Point(208, 200)
        Me.btnRemoveGranite.Name = "btnRemoveGranite"
        Me.btnRemoveGranite.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveGranite.TabIndex = 2
        Me.btnRemoveGranite.Text = "Remove"
        '
        'btnAddGranite
        '
        Me.btnAddGranite.Image = CType(resources.GetObject("btnAddGranite.Image"), System.Drawing.Image)
        Me.btnAddGranite.ImageAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnAddGranite.Location = New System.Drawing.Point(208, 168)
        Me.btnAddGranite.Name = "btnAddGranite"
        Me.btnAddGranite.Size = New System.Drawing.Size(72, 23)
        Me.btnAddGranite.TabIndex = 1
        Me.btnAddGranite.Text = "Add"
        '
        'tvGranite
        '
        Me.tvGranite.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.tvGranite.Connection = Me.SqlConnection
        Me.tvGranite.HideSelection = False
        Me.tvGranite.ImageIndex = -1
        Me.tvGranite.Location = New System.Drawing.Point(16, 48)
        Me.tvGranite.Name = "tvGranite"
        Me.tvGranite.SelectedImageIndex = -1
        Me.tvGranite.Size = New System.Drawing.Size(184, 336)
        Me.tvGranite.TabIndex = 0
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;integrated security=SSPI;data source=""SERVER" & _
        "\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'dgGranite
        '
        Me.dgGranite.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgGranite.DataSource = Me.DsGTMS.VJobs_Materials
        '
        'dgGranite.EmbeddedNavigator
        '
        Me.dgGranite.EmbeddedNavigator.Name = ""
        Me.dgGranite.Location = New System.Drawing.Point(288, 48)
        Me.dgGranite.MainView = Me.gvGranite
        Me.dgGranite.Name = "dgGranite"
        Me.dgGranite.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtAmountGranite})
        Me.dgGranite.Size = New System.Drawing.Size(224, 336)
        Me.dgGranite.TabIndex = 3
        Me.dgGranite.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvGranite})
        '
        'gvGranite
        '
        Me.gvGranite.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvGranite.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvGranite.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvGranite.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvGranite.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvGranite.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvGranite.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvGranite.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvGranite.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvGranite.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvGranite.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvGranite.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvGranite.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvGranite.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvGranite.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvGranite.Appearance.VertLine.Options.UseBackColor = True
        Me.gvGranite.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colMTName, Me.colJMPrimaryQuoted, Me.colMTPrimaryUOMShortName})
        Me.gvGranite.GridControl = Me.dgGranite
        Me.gvGranite.Name = "gvGranite"
        Me.gvGranite.OptionsCustomization.AllowFilter = False
        Me.gvGranite.OptionsNavigation.AutoFocusNewRow = True
        Me.gvGranite.OptionsView.ShowGroupPanel = False
        Me.gvGranite.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colMTName, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colMTName
        '
        Me.colMTName.Caption = "Material"
        Me.colMTName.FieldName = "MTName"
        Me.colMTName.Name = "colMTName"
        Me.colMTName.OptionsColumn.AllowFocus = False
        Me.colMTName.Visible = True
        Me.colMTName.VisibleIndex = 0
        Me.colMTName.Width = 102
        '
        'colJMPrimaryQuoted
        '
        Me.colJMPrimaryQuoted.Caption = "Estimated Amount"
        Me.colJMPrimaryQuoted.ColumnEdit = Me.txtAmountGranite
        Me.colJMPrimaryQuoted.FieldName = "JMPrimaryQuoted"
        Me.colJMPrimaryQuoted.Name = "colJMPrimaryQuoted"
        Me.colJMPrimaryQuoted.Visible = True
        Me.colJMPrimaryQuoted.VisibleIndex = 1
        Me.colJMPrimaryQuoted.Width = 97
        '
        'txtAmountGranite
        '
        Me.txtAmountGranite.AutoHeight = False
        Me.txtAmountGranite.DisplayFormat.FormatString = "#0.000"
        Me.txtAmountGranite.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtAmountGranite.EditFormat.FormatString = "#0.000"
        Me.txtAmountGranite.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtAmountGranite.Name = "txtAmountGranite"
        '
        'colMTPrimaryUOMShortName
        '
        Me.colMTPrimaryUOMShortName.FieldName = "MTPrimaryUOMShortName"
        Me.colMTPrimaryUOMShortName.Name = "colMTPrimaryUOMShortName"
        Me.colMTPrimaryUOMShortName.OptionsColumn.AllowEdit = False
        Me.colMTPrimaryUOMShortName.Visible = True
        Me.colMTPrimaryUOMShortName.VisibleIndex = 2
        Me.colMTPrimaryUOMShortName.Width = 43
        '
        'pnlAppliances
        '
        Me.pnlAppliances.Controls.Add(Me.btnRemoveAppliance)
        Me.pnlAppliances.Controls.Add(Me.btnAddAppliance)
        Me.pnlAppliances.Controls.Add(Me.dgAppliances)
        Me.pnlAppliances.Controls.Add(Me.btnEditAppliance)
        Me.pnlAppliances.Location = New System.Drawing.Point(48, 24)
        Me.pnlAppliances.Name = "pnlAppliances"
        Me.pnlAppliances.Size = New System.Drawing.Size(496, 352)
        Me.pnlAppliances.TabIndex = 17
        Me.pnlAppliances.Text = "Appliances"
        '
        'PnlJobInformation
        '
        Me.PnlJobInformation.Controls.Add(Me.dpLDDate)
        Me.PnlJobInformation.Controls.Add(Me.txtLDUser)
        Me.PnlJobInformation.Controls.Add(Me.Label22)
        Me.PnlJobInformation.Controls.Add(Me.LookUpEdit1)
        Me.PnlJobInformation.Controls.Add(Me.Label21)
        Me.PnlJobInformation.Controls.Add(Me.txtJBPriceQuoted)
        Me.PnlJobInformation.Controls.Add(Me.Label8)
        Me.PnlJobInformation.Controls.Add(Me.GroupLine5)
        Me.PnlJobInformation.Controls.Add(Me.txtJBJobDescription)
        Me.PnlJobInformation.Controls.Add(Me.txtEXIDBookingMeasurer)
        Me.PnlJobInformation.Controls.Add(Me.Label12)
        Me.PnlJobInformation.Controls.Add(Me.Label1)
        Me.PnlJobInformation.Controls.Add(Me.Label6)
        Me.PnlJobInformation.Controls.Add(Me.GroupLine2)
        Me.PnlJobInformation.Controls.Add(Me.Label15)
        Me.PnlJobInformation.Controls.Add(Me.RadioGroup1)
        Me.PnlJobInformation.Controls.Add(Me.GroupLine3)
        Me.PnlJobInformation.Controls.Add(Me.GroupLine4)
        Me.PnlJobInformation.Controls.Add(Me.Label5)
        Me.PnlJobInformation.Controls.Add(Me.chkJBIsAllOrderingComplete)
        Me.PnlJobInformation.Controls.Add(Me.GroupLine1)
        Me.PnlJobInformation.Controls.Add(Me.Label4)
        Me.PnlJobInformation.Controls.Add(Me.Label19)
        Me.PnlJobInformation.Location = New System.Drawing.Point(48, 0)
        Me.PnlJobInformation.Name = "PnlJobInformation"
        Me.PnlJobInformation.Size = New System.Drawing.Size(536, 480)
        Me.PnlJobInformation.TabIndex = 11
        Me.PnlJobInformation.Text = "Job Information"
        '
        'dpLDDate
        '
        Me.dpLDDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBBookingDate"))
        Me.dpLDDate.EditValue = New Date(2005, 7, 21, 0, 0, 0, 0)
        Me.dpLDDate.Location = New System.Drawing.Point(352, 408)
        Me.dpLDDate.Name = "dpLDDate"
        '
        'dpLDDate.Properties
        '
        Me.dpLDDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False
        Me.dpLDDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpLDDate.Size = New System.Drawing.Size(152, 20)
        Me.dpLDDate.TabIndex = 7
        '
        'txtLDUser
        '
        Me.txtLDUser.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBUser"))
        Me.txtLDUser.Location = New System.Drawing.Point(128, 408)
        Me.txtLDUser.Name = "txtLDUser"
        '
        'txtLDUser.Properties
        '
        Me.txtLDUser.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtLDUser.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXName", "", 45, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.txtLDUser.Properties.DataSource = Me.dvExpenses
        Me.txtLDUser.Properties.DisplayMember = "EXName"
        Me.txtLDUser.Properties.NullText = ""
        Me.txtLDUser.Properties.ShowFooter = False
        Me.txtLDUser.Properties.ShowHeader = False
        Me.txtLDUser.Properties.ShowLines = False
        Me.txtLDUser.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.txtLDUser.Properties.ValueMember = "EXName"
        Me.txtLDUser.Size = New System.Drawing.Size(176, 20)
        Me.txtLDUser.TabIndex = 6
        '
        'dvExpenses
        '
        Me.dvExpenses.RowFilter = "EGType <> 'OE'"
        Me.dvExpenses.Sort = "EXName"
        Me.dvExpenses.Table = Me.DsGTMS.Expenses
        '
        'Label22
        '
        Me.Label22.Location = New System.Drawing.Point(48, 128)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(136, 21)
        Me.Label22.TabIndex = 33
        Me.Label22.Text = "Salesperson:"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LookUpEdit1
        '
        Me.LookUpEdit1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.EXIDRep"))
        Me.LookUpEdit1.Location = New System.Drawing.Point(200, 128)
        Me.LookUpEdit1.Name = "LookUpEdit1"
        '
        'LookUpEdit1.Properties
        '
        Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookUpEdit1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXName")})
        Me.LookUpEdit1.Properties.DataSource = Me.dvSalesReps
        Me.LookUpEdit1.Properties.DisplayMember = "EXName"
        Me.LookUpEdit1.Properties.NullText = ""
        Me.LookUpEdit1.Properties.ShowFooter = False
        Me.LookUpEdit1.Properties.ShowHeader = False
        Me.LookUpEdit1.Properties.ShowLines = False
        Me.LookUpEdit1.Properties.ValueMember = "EXID"
        Me.LookUpEdit1.Size = New System.Drawing.Size(176, 20)
        Me.LookUpEdit1.TabIndex = 1
        '
        'dvSalesReps
        '
        Me.dvSalesReps.RowFilter = "EGType = 'RC'"
        Me.dvSalesReps.Sort = "EXName"
        Me.dvSalesReps.Table = Me.DsGTMS.Expenses
        '
        'Label21
        '
        Me.Label21.Location = New System.Drawing.Point(48, 248)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(432, 16)
        Me.Label21.TabIndex = 31
        Me.Label21.Text = "This figure is used to predict sales. In most cases, this will be the quoted pric" & _
        "e."
        '
        'txtJBPriceQuoted
        '
        Me.txtJBPriceQuoted.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBPriceQuoted"))
        Me.txtJBPriceQuoted.EditValue = ""
        Me.txtJBPriceQuoted.Location = New System.Drawing.Point(200, 272)
        Me.txtJBPriceQuoted.Name = "txtJBPriceQuoted"
        '
        'txtJBPriceQuoted.Properties
        '
        Me.txtJBPriceQuoted.Properties.Appearance.Options.UseTextOptions = True
        Me.txtJBPriceQuoted.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtJBPriceQuoted.Properties.DisplayFormat.FormatString = "c"
        Me.txtJBPriceQuoted.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtJBPriceQuoted.Properties.EditFormat.FormatString = "c"
        Me.txtJBPriceQuoted.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtJBPriceQuoted.Size = New System.Drawing.Size(176, 20)
        Me.txtJBPriceQuoted.TabIndex = 4
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(48, 272)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(152, 21)
        Me.Label8.TabIndex = 30
        Me.Label8.Text = "Estimated job price (BAOT):"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine5
        '
        Me.GroupLine5.Location = New System.Drawing.Point(24, 224)
        Me.GroupLine5.Name = "GroupLine5"
        Me.GroupLine5.Size = New System.Drawing.Size(480, 16)
        Me.GroupLine5.TabIndex = 27
        Me.GroupLine5.TextString = "Estimated Job Price"
        Me.GroupLine5.TextWidth = 110
        '
        'txtJBJobDescription
        '
        Me.txtJBJobDescription.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBJobDescription"))
        Me.txtJBJobDescription.EditValue = ""
        Me.txtJBJobDescription.Location = New System.Drawing.Point(200, 72)
        Me.txtJBJobDescription.Name = "txtJBJobDescription"
        '
        'txtJBJobDescription.Properties
        '
        Me.txtJBJobDescription.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtJBJobDescription.Properties.ContextMenu = Me.cmHistory
        Me.txtJBJobDescription.Properties.MaxItemCount = 15
        Me.txtJBJobDescription.Size = New System.Drawing.Size(176, 20)
        Me.txtJBJobDescription.TabIndex = 0
        '
        'cmHistory
        '
        Me.cmHistory.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.miClearHistory})
        '
        'miClearHistory
        '
        Me.miClearHistory.Index = 0
        Me.miClearHistory.Text = "Clear History"
        '
        'Label19
        '
        Me.Label19.Location = New System.Drawing.Point(48, 408)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(80, 21)
        Me.Label19.TabIndex = 14
        Me.Label19.Text = "Booked in by:"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlJobAddress
        '
        Me.pnlJobAddress.Controls.Add(Me.lblJobPostCode)
        Me.pnlJobAddress.Controls.Add(Me.txtJBJobStreetAddress01)
        Me.pnlJobAddress.Controls.Add(Me.rgJBJobAddressAsAbove)
        Me.pnlJobAddress.Controls.Add(Me.Label17)
        Me.pnlJobAddress.Controls.Add(Me.lblJobState)
        Me.pnlJobAddress.Controls.Add(Me.lblJobStreetAddress)
        Me.pnlJobAddress.Controls.Add(Me.lblJobSuburb)
        Me.pnlJobAddress.Controls.Add(Me.txtJBJobStreetAddress02)
        Me.pnlJobAddress.Controls.Add(Me.txtJBJobSuburb)
        Me.pnlJobAddress.Controls.Add(Me.txtJBJobState)
        Me.pnlJobAddress.Controls.Add(Me.txtJBJobPostCode)
        Me.pnlJobAddress.Location = New System.Drawing.Point(40, 16)
        Me.pnlJobAddress.Name = "pnlJobAddress"
        Me.pnlJobAddress.Size = New System.Drawing.Size(504, 400)
        Me.pnlJobAddress.TabIndex = 12
        Me.pnlJobAddress.Text = "Job Address"
        '
        'lblJobPostCode
        '
        Me.lblJobPostCode.Location = New System.Drawing.Point(288, 192)
        Me.lblJobPostCode.Name = "lblJobPostCode"
        Me.lblJobPostCode.Size = New System.Drawing.Size(96, 21)
        Me.lblJobPostCode.TabIndex = 23
        Me.lblJobPostCode.Text = "ZIP/postal code:"
        Me.lblJobPostCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJBJobStreetAddress01
        '
        Me.txtJBJobStreetAddress01.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBJobStreetAddress01"))
        Me.txtJBJobStreetAddress01.EditValue = ""
        Me.txtJBJobStreetAddress01.Location = New System.Drawing.Point(96, 96)
        Me.txtJBJobStreetAddress01.Name = "txtJBJobStreetAddress01"
        Me.txtJBJobStreetAddress01.Size = New System.Drawing.Size(368, 20)
        Me.txtJBJobStreetAddress01.TabIndex = 1
        '
        'rgJBJobAddressAsAbove
        '
        Me.rgJBJobAddressAsAbove.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBJobAddressAsAbove"))
        Me.rgJBJobAddressAsAbove.Location = New System.Drawing.Point(312, 8)
        Me.rgJBJobAddressAsAbove.Name = "rgJBJobAddressAsAbove"
        '
        'rgJBJobAddressAsAbove.Properties
        '
        Me.rgJBJobAddressAsAbove.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.rgJBJobAddressAsAbove.Properties.Appearance.Options.UseBackColor = True
        Me.rgJBJobAddressAsAbove.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.rgJBJobAddressAsAbove.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "Yes"), New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "No")})
        Me.rgJBJobAddressAsAbove.Size = New System.Drawing.Size(56, 56)
        Me.rgJBJobAddressAsAbove.TabIndex = 0
        '
        'Label17
        '
        Me.Label17.Location = New System.Drawing.Point(8, 24)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(304, 21)
        Me.Label17.TabIndex = 16
        Me.Label17.Text = "Is this job at the customer's address (same as previous)?"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobState
        '
        Me.lblJobState.Location = New System.Drawing.Point(8, 192)
        Me.lblJobState.Name = "lblJobState"
        Me.lblJobState.Size = New System.Drawing.Size(80, 21)
        Me.lblJobState.TabIndex = 11
        Me.lblJobState.Text = "State/region:"
        Me.lblJobState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobStreetAddress
        '
        Me.lblJobStreetAddress.Location = New System.Drawing.Point(8, 96)
        Me.lblJobStreetAddress.Name = "lblJobStreetAddress"
        Me.lblJobStreetAddress.Size = New System.Drawing.Size(88, 21)
        Me.lblJobStreetAddress.TabIndex = 13
        Me.lblJobStreetAddress.Text = "Street address:"
        Me.lblJobStreetAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobSuburb
        '
        Me.lblJobSuburb.Location = New System.Drawing.Point(8, 160)
        Me.lblJobSuburb.Name = "lblJobSuburb"
        Me.lblJobSuburb.Size = New System.Drawing.Size(80, 21)
        Me.lblJobSuburb.TabIndex = 10
        Me.lblJobSuburb.Text = "Suburb/town:"
        Me.lblJobSuburb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJBJobStreetAddress02
        '
        Me.txtJBJobStreetAddress02.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBJobStreetAddress02"))
        Me.txtJBJobStreetAddress02.EditValue = ""
        Me.txtJBJobStreetAddress02.Location = New System.Drawing.Point(96, 128)
        Me.txtJBJobStreetAddress02.Name = "txtJBJobStreetAddress02"
        Me.txtJBJobStreetAddress02.Size = New System.Drawing.Size(368, 20)
        Me.txtJBJobStreetAddress02.TabIndex = 2
        '
        'txtJBJobSuburb
        '
        Me.txtJBJobSuburb.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBJobSuburb"))
        Me.txtJBJobSuburb.EditValue = ""
        Me.txtJBJobSuburb.Location = New System.Drawing.Point(96, 160)
        Me.txtJBJobSuburb.Name = "txtJBJobSuburb"
        Me.txtJBJobSuburb.Size = New System.Drawing.Size(368, 20)
        Me.txtJBJobSuburb.TabIndex = 3
        '
        'txtJBJobState
        '
        Me.txtJBJobState.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBJobState"))
        Me.txtJBJobState.EditValue = ""
        Me.txtJBJobState.Location = New System.Drawing.Point(96, 192)
        Me.txtJBJobState.Name = "txtJBJobState"
        Me.txtJBJobState.Size = New System.Drawing.Size(112, 20)
        Me.txtJBJobState.TabIndex = 4
        '
        'txtJBJobPostCode
        '
        Me.txtJBJobPostCode.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBJobPostCode"))
        Me.txtJBJobPostCode.EditValue = ""
        Me.txtJBJobPostCode.Location = New System.Drawing.Point(384, 192)
        Me.txtJBJobPostCode.Name = "txtJBJobPostCode"
        Me.txtJBJobPostCode.Size = New System.Drawing.Size(80, 20)
        Me.txtJBJobPostCode.TabIndex = 5
        '
        'pnlClientInformation
        '
        Me.pnlClientInformation.Controls.Add(Me.Label9)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientEmail)
        Me.pnlClientInformation.Controls.Add(Me.Label20)
        Me.pnlClientInformation.Controls.Add(Me.Label39)
        Me.pnlClientInformation.Controls.Add(Me.GroupLine21)
        Me.pnlClientInformation.Controls.Add(Me.Label40)
        Me.pnlClientInformation.Controls.Add(Me.Label41)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientFirstName)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientStreetAddress02)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientStreetAddress01)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientSuburb)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientPhoneNumber)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientFaxNumber)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientMobileNumber)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientState)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientPostCode)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientReferenceName)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientSurname)
        Me.pnlClientInformation.Controls.Add(Me.Label10)
        Me.pnlClientInformation.Controls.Add(Me.Label11)
        Me.pnlClientInformation.Controls.Add(Me.Label13)
        Me.pnlClientInformation.Controls.Add(Me.GroupLine18)
        Me.pnlClientInformation.Controls.Add(Me.Label36)
        Me.pnlClientInformation.Controls.Add(Me.GroupLine20)
        Me.pnlClientInformation.Controls.Add(Me.Label37)
        Me.pnlClientInformation.Controls.Add(Me.Label38)
        Me.pnlClientInformation.Location = New System.Drawing.Point(16, 16)
        Me.pnlClientInformation.Name = "pnlClientInformation"
        Me.pnlClientInformation.Size = New System.Drawing.Size(520, 456)
        Me.pnlClientInformation.TabIndex = 18
        Me.pnlClientInformation.Text = "Customer Information"
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(296, 272)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(96, 21)
        Me.Label9.TabIndex = 31
        Me.Label9.Text = "ZIP/postal code:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJBClientEmail
        '
        Me.txtJBClientEmail.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBClientEmail"))
        Me.txtJBClientEmail.EditValue = ""
        Me.txtJBClientEmail.Location = New System.Drawing.Point(184, 432)
        Me.txtJBClientEmail.Name = "txtJBClientEmail"
        Me.txtJBClientEmail.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientEmail.TabIndex = 11
        '
        'Label20
        '
        Me.Label20.Location = New System.Drawing.Point(40, 432)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(80, 21)
        Me.Label20.TabIndex = 29
        Me.Label20.Text = "Email:"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ListBox
        '
        Me.ListBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ListBox.IntegralHeight = False
        Me.ListBox.ItemHeight = 25
        Me.ListBox.Items.AddRange(New Object() {"Customer Information", "Job Address", "Job Information", "Tracked Material", "Appliances", "Other Trades", "Scheduling", "Notes"})
        Me.ListBox.Location = New System.Drawing.Point(8, 8)
        Me.ListBox.Name = "ListBox"
        Me.ListBox.Size = New System.Drawing.Size(136, 480)
        Me.ListBox.TabIndex = 0
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(648, 496)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(568, 496)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 3
        Me.btnOK.Text = "OK"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Jobs", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("JBID", "JBID"), New System.Data.Common.DataColumnMapping("ID", "ID"), New System.Data.Common.DataColumnMapping("JBClientFirstName", "JBClientFirstName"), New System.Data.Common.DataColumnMapping("JBClientSurname", "JBClientSurname"), New System.Data.Common.DataColumnMapping("JBClientName", "JBClientName"), New System.Data.Common.DataColumnMapping("JBClientStreetAddress01", "JBClientStreetAddress01"), New System.Data.Common.DataColumnMapping("JBClientStreetAddress02", "JBClientStreetAddress02"), New System.Data.Common.DataColumnMapping("JBClientSuburb", "JBClientSuburb"), New System.Data.Common.DataColumnMapping("JBClientState", "JBClientState"), New System.Data.Common.DataColumnMapping("JBClientPostCode", "JBClientPostCode"), New System.Data.Common.DataColumnMapping("JBClientAddress", "JBClientAddress"), New System.Data.Common.DataColumnMapping("JBClientPhoneNumber", "JBClientPhoneNumber"), New System.Data.Common.DataColumnMapping("JBClientFaxNumber", "JBClientFaxNumber"), New System.Data.Common.DataColumnMapping("JBClientMobileNumber", "JBClientMobileNumber"), New System.Data.Common.DataColumnMapping("JBJobAddressAsAbove", "JBJobAddressAsAbove"), New System.Data.Common.DataColumnMapping("JBJobStreetAddress01", "JBJobStreetAddress01"), New System.Data.Common.DataColumnMapping("JBJobStreetAddress02", "JBJobStreetAddress02"), New System.Data.Common.DataColumnMapping("JBJobSuburb", "JBJobSuburb"), New System.Data.Common.DataColumnMapping("JBJobState", "JBJobState"), New System.Data.Common.DataColumnMapping("JBJobPostCode", "JBJobPostCode"), New System.Data.Common.DataColumnMapping("JBJobAddress", "JBJobAddress"), New System.Data.Common.DataColumnMapping("JBReferenceName", "JBReferenceName"), New System.Data.Common.DataColumnMapping("EXIDBookingMeasurer", "EXIDBookingMeasurer"), New System.Data.Common.DataColumnMapping("JBJobDescription", "JBJobDescription"), New System.Data.Common.DataColumnMapping("JBDoorsType", "JBDoorsType"), New System.Data.Common.DataColumnMapping("JBDoorsStyle", "JBDoorsStyle"), New System.Data.Common.DataColumnMapping("JBDoorsColour", "JBDoorsColour"), New System.Data.Common.DataColumnMapping("JBIsMeasureOnSite", "JBIsMeasureOnSite"), New System.Data.Common.DataColumnMapping("JBIsAllOrderingComplete", "JBIsAllOrderingComplete"), New System.Data.Common.DataColumnMapping("JBDoorsHandles", "JBDoorsHandles"), New System.Data.Common.DataColumnMapping("JBDoorsTexture", "JBDoorsTexture"), New System.Data.Common.DataColumnMapping("JBIsCancelled", "JBIsCancelled"), New System.Data.Common.DataColumnMapping("JBDoorsSupplier", "JBDoorsSupplier"), New System.Data.Common.DataColumnMapping("JBClientEmail", "JBClientEmail"), New System.Data.Common.DataColumnMapping("JBPriceQuoted", "JBPriceQuoted"), New System.Data.Common.DataColumnMapping("JBGraniteBAOTPriceQuoted", "JBGraniteBAOTPriceQuoted"), New System.Data.Common.DataColumnMapping("JBCabinetryBAOTPriceQuoted", "JBCabinetryBAOTPriceQuoted"), New System.Data.Common.DataColumnMapping("EXIDRep", "EXIDRep"), New System.Data.Common.DataColumnMapping("JBBookingDate", "JBBookingDate"), New System.Data.Common.DataColumnMapping("JBUser", "JBUser")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Jobs WHERE (BRID = @Original_BRID) AND (JBID = @Original_JBID) AND (E" & _
        "XIDBookingMeasurer = @Original_EXIDBookingMeasurer OR @Original_EXIDBookingMeasu" & _
        "rer IS NULL AND EXIDBookingMeasurer IS NULL) AND (EXIDRep = @Original_EXIDRep OR" & _
        " @Original_EXIDRep IS NULL AND EXIDRep IS NULL) AND (ID = @Original_ID) AND (JBB" & _
        "ookingDate = @Original_JBBookingDate OR @Original_JBBookingDate IS NULL AND JBBo" & _
        "okingDate IS NULL) AND (JBCabinetryBAOTPriceQuoted = @Original_JBCabinetryBAOTPr" & _
        "iceQuoted OR @Original_JBCabinetryBAOTPriceQuoted IS NULL AND JBCabinetryBAOTPri" & _
        "ceQuoted IS NULL) AND (JBClientAddress = @Original_JBClientAddress OR @Original_" & _
        "JBClientAddress IS NULL AND JBClientAddress IS NULL) AND (JBClientEmail = @Origi" & _
        "nal_JBClientEmail OR @Original_JBClientEmail IS NULL AND JBClientEmail IS NULL) " & _
        "AND (JBClientFaxNumber = @Original_JBClientFaxNumber OR @Original_JBClientFaxNum" & _
        "ber IS NULL AND JBClientFaxNumber IS NULL) AND (JBClientFirstName = @Original_JB" & _
        "ClientFirstName OR @Original_JBClientFirstName IS NULL AND JBClientFirstName IS " & _
        "NULL) AND (JBClientMobileNumber = @Original_JBClientMobileNumber OR @Original_JB" & _
        "ClientMobileNumber IS NULL AND JBClientMobileNumber IS NULL) AND (JBClientName =" & _
        " @Original_JBClientName OR @Original_JBClientName IS NULL AND JBClientName IS NU" & _
        "LL) AND (JBClientPhoneNumber = @Original_JBClientPhoneNumber OR @Original_JBClie" & _
        "ntPhoneNumber IS NULL AND JBClientPhoneNumber IS NULL) AND (JBClientPostCode = @" & _
        "Original_JBClientPostCode OR @Original_JBClientPostCode IS NULL AND JBClientPost" & _
        "Code IS NULL) AND (JBClientState = @Original_JBClientState OR @Original_JBClient" & _
        "State IS NULL AND JBClientState IS NULL) AND (JBClientStreetAddress01 = @Origina" & _
        "l_JBClientStreetAddress01 OR @Original_JBClientStreetAddress01 IS NULL AND JBCli" & _
        "entStreetAddress01 IS NULL) AND (JBClientStreetAddress02 = @Original_JBClientStr" & _
        "eetAddress02 OR @Original_JBClientStreetAddress02 IS NULL AND JBClientStreetAddr" & _
        "ess02 IS NULL) AND (JBClientSuburb = @Original_JBClientSuburb OR @Original_JBCli" & _
        "entSuburb IS NULL AND JBClientSuburb IS NULL) AND (JBClientSurname = @Original_J" & _
        "BClientSurname OR @Original_JBClientSurname IS NULL AND JBClientSurname IS NULL)" & _
        " AND (JBDoorsColour = @Original_JBDoorsColour OR @Original_JBDoorsColour IS NULL" & _
        " AND JBDoorsColour IS NULL) AND (JBDoorsHandles = @Original_JBDoorsHandles OR @O" & _
        "riginal_JBDoorsHandles IS NULL AND JBDoorsHandles IS NULL) AND (JBDoorsStyle = @" & _
        "Original_JBDoorsStyle OR @Original_JBDoorsStyle IS NULL AND JBDoorsStyle IS NULL" & _
        ") AND (JBDoorsSupplier = @Original_JBDoorsSupplier OR @Original_JBDoorsSupplier " & _
        "IS NULL AND JBDoorsSupplier IS NULL) AND (JBDoorsTexture = @Original_JBDoorsText" & _
        "ure OR @Original_JBDoorsTexture IS NULL AND JBDoorsTexture IS NULL) AND (JBDoors" & _
        "Type = @Original_JBDoorsType OR @Original_JBDoorsType IS NULL AND JBDoorsType IS" & _
        " NULL) AND (JBGraniteBAOTPriceQuoted = @Original_JBGraniteBAOTPriceQuoted OR @Or" & _
        "iginal_JBGraniteBAOTPriceQuoted IS NULL AND JBGraniteBAOTPriceQuoted IS NULL) AN" & _
        "D (JBIsAllOrderingComplete = @Original_JBIsAllOrderingComplete OR @Original_JBIs" & _
        "AllOrderingComplete IS NULL AND JBIsAllOrderingComplete IS NULL) AND (JBIsCancel" & _
        "led = @Original_JBIsCancelled OR @Original_JBIsCancelled IS NULL AND JBIsCancell" & _
        "ed IS NULL) AND (JBIsMeasureOnSite = @Original_JBIsMeasureOnSite OR @Original_JB" & _
        "IsMeasureOnSite IS NULL AND JBIsMeasureOnSite IS NULL) AND (JBJobAddress = @Orig" & _
        "inal_JBJobAddress OR @Original_JBJobAddress IS NULL AND JBJobAddress IS NULL) AN" & _
        "D (JBJobAddressAsAbove = @Original_JBJobAddressAsAbove OR @Original_JBJobAddress" & _
        "AsAbove IS NULL AND JBJobAddressAsAbove IS NULL) AND (JBJobDescription = @Origin" & _
        "al_JBJobDescription OR @Original_JBJobDescription IS NULL AND JBJobDescription I" & _
        "S NULL) AND (JBJobPostCode = @Original_JBJobPostCode OR @Original_JBJobPostCode " & _
        "IS NULL AND JBJobPostCode IS NULL) AND (JBJobState = @Original_JBJobState OR @Or" & _
        "iginal_JBJobState IS NULL AND JBJobState IS NULL) AND (JBJobStreetAddress01 = @O" & _
        "riginal_JBJobStreetAddress01 OR @Original_JBJobStreetAddress01 IS NULL AND JBJob" & _
        "StreetAddress01 IS NULL) AND (JBJobStreetAddress02 = @Original_JBJobStreetAddres" & _
        "s02 OR @Original_JBJobStreetAddress02 IS NULL AND JBJobStreetAddress02 IS NULL) " & _
        "AND (JBJobSuburb = @Original_JBJobSuburb OR @Original_JBJobSuburb IS NULL AND JB" & _
        "JobSuburb IS NULL) AND (JBPriceQuoted = @Original_JBPriceQuoted OR @Original_JBP" & _
        "riceQuoted IS NULL AND JBPriceQuoted IS NULL) AND (JBReferenceName = @Original_J" & _
        "BReferenceName OR @Original_JBReferenceName IS NULL AND JBReferenceName IS NULL)" & _
        " AND (JBUser = @Original_JBUser OR @Original_JBUser IS NULL AND JBUser IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDBookingMeasurer", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDBookingMeasurer", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDRep", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDRep", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBBookingDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBBookingDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBCabinetryBAOTPriceQuoted", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBCabinetryBAOTPriceQuoted", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientFirstName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientFirstName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientName", System.Data.SqlDbType.VarChar, 102, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientSurname", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsColour", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsColour", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsHandles", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsHandles", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsStyle", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsStyle", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsSupplier", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsSupplier", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsTexture", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsTexture", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsType", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBGraniteBAOTPriceQuoted", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBGraniteBAOTPriceQuoted", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBIsAllOrderingComplete", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBIsAllOrderingComplete", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBIsCancelled", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBIsCancelled", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBIsMeasureOnSite", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBIsMeasureOnSite", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobAddressAsAbove", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobDescription", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBPriceQuoted", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBPriceQuoted", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBReferenceName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBReferenceName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBUser", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBUser", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Jobs(BRID, ID, JBClientFirstName, JBClientSurname, JBClientName, JBCl" & _
        "ientStreetAddress01, JBClientStreetAddress02, JBClientSuburb, JBClientState, JBC" & _
        "lientPostCode, JBClientAddress, JBClientPhoneNumber, JBClientFaxNumber, JBClient" & _
        "MobileNumber, JBJobAddressAsAbove, JBJobStreetAddress01, JBJobStreetAddress02, J" & _
        "BJobSuburb, JBJobState, JBJobPostCode, JBJobAddress, JBReferenceName, EXIDBookin" & _
        "gMeasurer, JBJobDescription, JBDoorsType, JBDoorsStyle, JBDoorsColour, JBIsMeasu" & _
        "reOnSite, JBIsAllOrderingComplete, JBDoorsHandles, JBDoorsTexture, JBIsCancelled" & _
        ", JBDoorsSupplier, JBClientEmail, JBPriceQuoted, JBGraniteBAOTPriceQuoted, JBCab" & _
        "inetryBAOTPriceQuoted, EXIDRep, JBBookingDate, JBUser) VALUES (@BRID, @ID, @JBCl" & _
        "ientFirstName, @JBClientSurname, @JBClientName, @JBClientStreetAddress01, @JBCli" & _
        "entStreetAddress02, @JBClientSuburb, @JBClientState, @JBClientPostCode, @JBClien" & _
        "tAddress, @JBClientPhoneNumber, @JBClientFaxNumber, @JBClientMobileNumber, @JBJo" & _
        "bAddressAsAbove, @JBJobStreetAddress01, @JBJobStreetAddress02, @JBJobSuburb, @JB" & _
        "JobState, @JBJobPostCode, @JBJobAddress, @JBReferenceName, @EXIDBookingMeasurer," & _
        " @JBJobDescription, @JBDoorsType, @JBDoorsStyle, @JBDoorsColour, @JBIsMeasureOnS" & _
        "ite, @JBIsAllOrderingComplete, @JBDoorsHandles, @JBDoorsTexture, @JBIsCancelled," & _
        " @JBDoorsSupplier, @JBClientEmail, @JBPriceQuoted, @JBGraniteBAOTPriceQuoted, @J" & _
        "BCabinetryBAOTPriceQuoted, @EXIDRep, @JBBookingDate, @JBUser); SELECT BRID, JBID" & _
        ", ID, JBClientFirstName, JBClientSurname, JBClientName, JBClientStreetAddress01," & _
        " JBClientStreetAddress02, JBClientSuburb, JBClientState, JBClientPostCode, JBCli" & _
        "entAddress, JBClientPhoneNumber, JBClientFaxNumber, JBClientMobileNumber, JBJobA" & _
        "ddressAsAbove, JBJobStreetAddress01, JBJobStreetAddress02, JBJobSuburb, JBJobSta" & _
        "te, JBJobPostCode, JBJobAddress, JBReferenceName, EXIDBookingMeasurer, JBJobDesc" & _
        "ription, JBDoorsType, JBDoorsStyle, JBDoorsColour, JBIsMeasureOnSite, JBIsAllOrd" & _
        "eringComplete, JBDoorsHandles, JBDoorsTexture, JBIsCancelled, JBDoorsSupplier, J" & _
        "BClientEmail, JBPriceQuoted, JBGraniteBAOTPriceQuoted, JBCabinetryBAOTPriceQuote" & _
        "d, EXIDRep, JBBookingDate, JBUser FROM Jobs WHERE (BRID = @BRID) AND (JBID = @@I" & _
        "DENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientFirstName", System.Data.SqlDbType.VarChar, 50, "JBClientFirstName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientSurname", System.Data.SqlDbType.VarChar, 50, "JBClientSurname"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientName", System.Data.SqlDbType.VarChar, 102, "JBClientName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, "JBClientStreetAddress01"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, "JBClientStreetAddress02"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientSuburb", System.Data.SqlDbType.VarChar, 50, "JBClientSuburb"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientState", System.Data.SqlDbType.VarChar, 3, "JBClientState"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientPostCode", System.Data.SqlDbType.VarChar, 20, "JBClientPostCode"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientAddress", System.Data.SqlDbType.VarChar, 259, "JBClientAddress"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientPhoneNumber", System.Data.SqlDbType.VarChar, 20, "JBClientPhoneNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientFaxNumber", System.Data.SqlDbType.VarChar, 20, "JBClientFaxNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientMobileNumber", System.Data.SqlDbType.VarChar, 20, "JBClientMobileNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, "JBJobAddressAsAbove"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, "JBJobStreetAddress01"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, "JBJobStreetAddress02"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobSuburb", System.Data.SqlDbType.VarChar, 50, "JBJobSuburb"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobState", System.Data.SqlDbType.VarChar, 3, "JBJobState"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobPostCode", System.Data.SqlDbType.VarChar, 20, "JBJobPostCode"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobAddress", System.Data.SqlDbType.VarChar, 259, "JBJobAddress"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBReferenceName", System.Data.SqlDbType.VarChar, 100, "JBReferenceName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDBookingMeasurer", System.Data.SqlDbType.Int, 4, "EXIDBookingMeasurer"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobDescription", System.Data.SqlDbType.VarChar, 50, "JBJobDescription"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsType", System.Data.SqlDbType.VarChar, 50, "JBDoorsType"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsStyle", System.Data.SqlDbType.VarChar, 50, "JBDoorsStyle"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsColour", System.Data.SqlDbType.VarChar, 50, "JBDoorsColour"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBIsMeasureOnSite", System.Data.SqlDbType.Bit, 1, "JBIsMeasureOnSite"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBIsAllOrderingComplete", System.Data.SqlDbType.Bit, 1, "JBIsAllOrderingComplete"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsHandles", System.Data.SqlDbType.VarChar, 50, "JBDoorsHandles"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsTexture", System.Data.SqlDbType.VarChar, 50, "JBDoorsTexture"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBIsCancelled", System.Data.SqlDbType.Bit, 1, "JBIsCancelled"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsSupplier", System.Data.SqlDbType.VarChar, 50, "JBDoorsSupplier"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientEmail", System.Data.SqlDbType.VarChar, 50, "JBClientEmail"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBPriceQuoted", System.Data.SqlDbType.Money, 8, "JBPriceQuoted"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBGraniteBAOTPriceQuoted", System.Data.SqlDbType.Money, 8, "JBGraniteBAOTPriceQuoted"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBCabinetryBAOTPriceQuoted", System.Data.SqlDbType.Money, 8, "JBCabinetryBAOTPriceQuoted"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDRep", System.Data.SqlDbType.Int, 4, "EXIDRep"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBBookingDate", System.Data.SqlDbType.DateTime, 8, "JBBookingDate"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBUser", System.Data.SqlDbType.VarChar, 50, "JBUser"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, JBID, ID, JBClientFirstName, JBClientSurname, JBClientName, JBClient" & _
        "StreetAddress01, JBClientStreetAddress02, JBClientSuburb, JBClientState, JBClien" & _
        "tPostCode, JBClientAddress, JBClientPhoneNumber, JBClientFaxNumber, JBClientMobi" & _
        "leNumber, JBJobAddressAsAbove, JBJobStreetAddress01, JBJobStreetAddress02, JBJob" & _
        "Suburb, JBJobState, JBJobPostCode, JBJobAddress, JBReferenceName, EXIDBookingMea" & _
        "surer, JBJobDescription, JBDoorsType, JBDoorsStyle, JBDoorsColour, JBIsMeasureOn" & _
        "Site, JBIsAllOrderingComplete, JBDoorsHandles, JBDoorsTexture, JBIsCancelled, JB" & _
        "DoorsSupplier, JBClientEmail, JBPriceQuoted, JBGraniteBAOTPriceQuoted, JBCabinet" & _
        "ryBAOTPriceQuoted, EXIDRep, JBBookingDate, JBUser FROM Jobs WHERE (BRID = @BRID)" & _
        " AND (JBID = @JBID)"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Jobs SET BRID = @BRID, ID = @ID, JBClientFirstName = @JBClientFirstName, J" & _
        "BClientSurname = @JBClientSurname, JBClientName = @JBClientName, JBClientStreetA" & _
        "ddress01 = @JBClientStreetAddress01, JBClientStreetAddress02 = @JBClientStreetAd" & _
        "dress02, JBClientSuburb = @JBClientSuburb, JBClientState = @JBClientState, JBCli" & _
        "entPostCode = @JBClientPostCode, JBClientAddress = @JBClientAddress, JBClientPho" & _
        "neNumber = @JBClientPhoneNumber, JBClientFaxNumber = @JBClientFaxNumber, JBClien" & _
        "tMobileNumber = @JBClientMobileNumber, JBJobAddressAsAbove = @JBJobAddressAsAbov" & _
        "e, JBJobStreetAddress01 = @JBJobStreetAddress01, JBJobStreetAddress02 = @JBJobSt" & _
        "reetAddress02, JBJobSuburb = @JBJobSuburb, JBJobState = @JBJobState, JBJobPostCo" & _
        "de = @JBJobPostCode, JBJobAddress = @JBJobAddress, JBReferenceName = @JBReferenc" & _
        "eName, EXIDBookingMeasurer = @EXIDBookingMeasurer, JBJobDescription = @JBJobDesc" & _
        "ription, JBDoorsType = @JBDoorsType, JBDoorsStyle = @JBDoorsStyle, JBDoorsColour" & _
        " = @JBDoorsColour, JBIsMeasureOnSite = @JBIsMeasureOnSite, JBIsAllOrderingComple" & _
        "te = @JBIsAllOrderingComplete, JBDoorsHandles = @JBDoorsHandles, JBDoorsTexture " & _
        "= @JBDoorsTexture, JBIsCancelled = @JBIsCancelled, JBDoorsSupplier = @JBDoorsSup" & _
        "plier, JBClientEmail = @JBClientEmail, JBPriceQuoted = @JBPriceQuoted, JBGranite" & _
        "BAOTPriceQuoted = @JBGraniteBAOTPriceQuoted, JBCabinetryBAOTPriceQuoted = @JBCab" & _
        "inetryBAOTPriceQuoted, EXIDRep = @EXIDRep, JBBookingDate = @JBBookingDate, JBUse" & _
        "r = @JBUser WHERE (BRID = @Original_BRID) AND (JBID = @Original_JBID) AND (EXIDB" & _
        "ookingMeasurer = @Original_EXIDBookingMeasurer OR @Original_EXIDBookingMeasurer " & _
        "IS NULL AND EXIDBookingMeasurer IS NULL) AND (EXIDRep = @Original_EXIDRep OR @Or" & _
        "iginal_EXIDRep IS NULL AND EXIDRep IS NULL) AND (ID = @Original_ID) AND (JBBooki" & _
        "ngDate = @Original_JBBookingDate OR @Original_JBBookingDate IS NULL AND JBBookin" & _
        "gDate IS NULL) AND (JBCabinetryBAOTPriceQuoted = @Original_JBCabinetryBAOTPriceQ" & _
        "uoted OR @Original_JBCabinetryBAOTPriceQuoted IS NULL AND JBCabinetryBAOTPriceQu" & _
        "oted IS NULL) AND (JBClientAddress = @Original_JBClientAddress OR @Original_JBCl" & _
        "ientAddress IS NULL AND JBClientAddress IS NULL) AND (JBClientEmail = @Original_" & _
        "JBClientEmail OR @Original_JBClientEmail IS NULL AND JBClientEmail IS NULL) AND " & _
        "(JBClientFaxNumber = @Original_JBClientFaxNumber OR @Original_JBClientFaxNumber " & _
        "IS NULL AND JBClientFaxNumber IS NULL) AND (JBClientFirstName = @Original_JBClie" & _
        "ntFirstName OR @Original_JBClientFirstName IS NULL AND JBClientFirstName IS NULL" & _
        ") AND (JBClientMobileNumber = @Original_JBClientMobileNumber OR @Original_JBClie" & _
        "ntMobileNumber IS NULL AND JBClientMobileNumber IS NULL) AND (JBClientName = @Or" & _
        "iginal_JBClientName OR @Original_JBClientName IS NULL AND JBClientName IS NULL) " & _
        "AND (JBClientPhoneNumber = @Original_JBClientPhoneNumber OR @Original_JBClientPh" & _
        "oneNumber IS NULL AND JBClientPhoneNumber IS NULL) AND (JBClientPostCode = @Orig" & _
        "inal_JBClientPostCode OR @Original_JBClientPostCode IS NULL AND JBClientPostCode" & _
        " IS NULL) AND (JBClientState = @Original_JBClientState OR @Original_JBClientStat" & _
        "e IS NULL AND JBClientState IS NULL) AND (JBClientStreetAddress01 = @Original_JB" & _
        "ClientStreetAddress01 OR @Original_JBClientStreetAddress01 IS NULL AND JBClientS" & _
        "treetAddress01 IS NULL) AND (JBClientStreetAddress02 = @Original_JBClientStreetA" & _
        "ddress02 OR @Original_JBClientStreetAddress02 IS NULL AND JBClientStreetAddress0" & _
        "2 IS NULL) AND (JBClientSuburb = @Original_JBClientSuburb OR @Original_JBClientS" & _
        "uburb IS NULL AND JBClientSuburb IS NULL) AND (JBClientSurname = @Original_JBCli" & _
        "entSurname OR @Original_JBClientSurname IS NULL AND JBClientSurname IS NULL) AND" & _
        " (JBDoorsColour = @Original_JBDoorsColour OR @Original_JBDoorsColour IS NULL AND" & _
        " JBDoorsColour IS NULL) AND (JBDoorsHandles = @Original_JBDoorsHandles OR @Origi" & _
        "nal_JBDoorsHandles IS NULL AND JBDoorsHandles IS NULL) AND (JBDoorsStyle = @Orig" & _
        "inal_JBDoorsStyle OR @Original_JBDoorsStyle IS NULL AND JBDoorsStyle IS NULL) AN" & _
        "D (JBDoorsSupplier = @Original_JBDoorsSupplier OR @Original_JBDoorsSupplier IS N" & _
        "ULL AND JBDoorsSupplier IS NULL) AND (JBDoorsTexture = @Original_JBDoorsTexture " & _
        "OR @Original_JBDoorsTexture IS NULL AND JBDoorsTexture IS NULL) AND (JBDoorsType" & _
        " = @Original_JBDoorsType OR @Original_JBDoorsType IS NULL AND JBDoorsType IS NUL" & _
        "L) AND (JBGraniteBAOTPriceQuoted = @Original_JBGraniteBAOTPriceQuoted OR @Origin" & _
        "al_JBGraniteBAOTPriceQuoted IS NULL AND JBGraniteBAOTPriceQuoted IS NULL) AND (J" & _
        "BIsAllOrderingComplete = @Original_JBIsAllOrderingComplete OR @Original_JBIsAllO" & _
        "rderingComplete IS NULL AND JBIsAllOrderingComplete IS NULL) AND (JBIsCancelled " & _
        "= @Original_JBIsCancelled OR @Original_JBIsCancelled IS NULL AND JBIsCancelled I" & _
        "S NULL) AND (JBIsMeasureOnSite = @Original_JBIsMeasureOnSite OR @Original_JBIsMe" & _
        "asureOnSite IS NULL AND JBIsMeasureOnSite IS NULL) AND (JBJobAddress = @Original" & _
        "_JBJobAddress OR @Original_JBJobAddress IS NULL AND JBJobAddress IS NULL) AND (J" & _
        "BJobAddressAsAbove = @Original_JBJobAddressAsAbove OR @Original_JBJobAddressAsAb" & _
        "ove IS NULL AND JBJobAddressAsAbove IS NULL) AND (JBJobDescription = @Original_J" & _
        "BJobDescription OR @Original_JBJobDescription IS NULL AND JBJobDescription IS NU" & _
        "LL) AND (JBJobPostCode = @Original_JBJobPostCode OR @Original_JBJobPostCode IS N" & _
        "ULL AND JBJobPostCode IS NULL) AND (JBJobState = @Original_JBJobState OR @Origin" & _
        "al_JBJobState IS NULL AND JBJobState IS NULL) AND (JBJobStreetAddress01 = @Origi" & _
        "nal_JBJobStreetAddress01 OR @Original_JBJobStreetAddress01 IS NULL AND JBJobStre" & _
        "etAddress01 IS NULL) AND (JBJobStreetAddress02 = @Original_JBJobStreetAddress02 " & _
        "OR @Original_JBJobStreetAddress02 IS NULL AND JBJobStreetAddress02 IS NULL) AND " & _
        "(JBJobSuburb = @Original_JBJobSuburb OR @Original_JBJobSuburb IS NULL AND JBJobS" & _
        "uburb IS NULL) AND (JBPriceQuoted = @Original_JBPriceQuoted OR @Original_JBPrice" & _
        "Quoted IS NULL AND JBPriceQuoted IS NULL) AND (JBReferenceName = @Original_JBRef" & _
        "erenceName OR @Original_JBReferenceName IS NULL AND JBReferenceName IS NULL) AND" & _
        " (JBUser = @Original_JBUser OR @Original_JBUser IS NULL AND JBUser IS NULL); SEL" & _
        "ECT BRID, JBID, ID, JBClientFirstName, JBClientSurname, JBClientName, JBClientSt" & _
        "reetAddress01, JBClientStreetAddress02, JBClientSuburb, JBClientState, JBClientP" & _
        "ostCode, JBClientAddress, JBClientPhoneNumber, JBClientFaxNumber, JBClientMobile" & _
        "Number, JBJobAddressAsAbove, JBJobStreetAddress01, JBJobStreetAddress02, JBJobSu" & _
        "burb, JBJobState, JBJobPostCode, JBJobAddress, JBReferenceName, EXIDBookingMeasu" & _
        "rer, JBJobDescription, JBDoorsType, JBDoorsStyle, JBDoorsColour, JBIsMeasureOnSi" & _
        "te, JBIsAllOrderingComplete, JBDoorsHandles, JBDoorsTexture, JBIsCancelled, JBDo" & _
        "orsSupplier, JBClientEmail, JBPriceQuoted, JBGraniteBAOTPriceQuoted, JBCabinetry" & _
        "BAOTPriceQuoted, EXIDRep, JBBookingDate, JBUser FROM Jobs WHERE (BRID = @BRID) A" & _
        "ND (JBID = @JBID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientFirstName", System.Data.SqlDbType.VarChar, 50, "JBClientFirstName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientSurname", System.Data.SqlDbType.VarChar, 50, "JBClientSurname"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientName", System.Data.SqlDbType.VarChar, 102, "JBClientName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, "JBClientStreetAddress01"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, "JBClientStreetAddress02"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientSuburb", System.Data.SqlDbType.VarChar, 50, "JBClientSuburb"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientState", System.Data.SqlDbType.VarChar, 3, "JBClientState"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientPostCode", System.Data.SqlDbType.VarChar, 20, "JBClientPostCode"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientAddress", System.Data.SqlDbType.VarChar, 259, "JBClientAddress"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientPhoneNumber", System.Data.SqlDbType.VarChar, 20, "JBClientPhoneNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientFaxNumber", System.Data.SqlDbType.VarChar, 20, "JBClientFaxNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientMobileNumber", System.Data.SqlDbType.VarChar, 20, "JBClientMobileNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, "JBJobAddressAsAbove"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, "JBJobStreetAddress01"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, "JBJobStreetAddress02"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobSuburb", System.Data.SqlDbType.VarChar, 50, "JBJobSuburb"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobState", System.Data.SqlDbType.VarChar, 3, "JBJobState"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobPostCode", System.Data.SqlDbType.VarChar, 20, "JBJobPostCode"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobAddress", System.Data.SqlDbType.VarChar, 259, "JBJobAddress"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBReferenceName", System.Data.SqlDbType.VarChar, 100, "JBReferenceName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDBookingMeasurer", System.Data.SqlDbType.Int, 4, "EXIDBookingMeasurer"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobDescription", System.Data.SqlDbType.VarChar, 50, "JBJobDescription"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsType", System.Data.SqlDbType.VarChar, 50, "JBDoorsType"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsStyle", System.Data.SqlDbType.VarChar, 50, "JBDoorsStyle"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsColour", System.Data.SqlDbType.VarChar, 50, "JBDoorsColour"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBIsMeasureOnSite", System.Data.SqlDbType.Bit, 1, "JBIsMeasureOnSite"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBIsAllOrderingComplete", System.Data.SqlDbType.Bit, 1, "JBIsAllOrderingComplete"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsHandles", System.Data.SqlDbType.VarChar, 50, "JBDoorsHandles"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsTexture", System.Data.SqlDbType.VarChar, 50, "JBDoorsTexture"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBIsCancelled", System.Data.SqlDbType.Bit, 1, "JBIsCancelled"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBDoorsSupplier", System.Data.SqlDbType.VarChar, 50, "JBDoorsSupplier"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientEmail", System.Data.SqlDbType.VarChar, 50, "JBClientEmail"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBPriceQuoted", System.Data.SqlDbType.Money, 8, "JBPriceQuoted"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBGraniteBAOTPriceQuoted", System.Data.SqlDbType.Money, 8, "JBGraniteBAOTPriceQuoted"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBCabinetryBAOTPriceQuoted", System.Data.SqlDbType.Money, 8, "JBCabinetryBAOTPriceQuoted"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDRep", System.Data.SqlDbType.Int, 4, "EXIDRep"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBBookingDate", System.Data.SqlDbType.DateTime, 8, "JBBookingDate"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBUser", System.Data.SqlDbType.VarChar, 50, "JBUser"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDBookingMeasurer", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDBookingMeasurer", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDRep", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDRep", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBBookingDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBBookingDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBCabinetryBAOTPriceQuoted", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBCabinetryBAOTPriceQuoted", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientFirstName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientFirstName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientName", System.Data.SqlDbType.VarChar, 102, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientSurname", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsColour", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsColour", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsHandles", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsHandles", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsStyle", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsStyle", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsSupplier", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsSupplier", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsTexture", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsTexture", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBDoorsType", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBDoorsType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBGraniteBAOTPriceQuoted", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBGraniteBAOTPriceQuoted", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBIsAllOrderingComplete", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBIsAllOrderingComplete", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBIsCancelled", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBIsCancelled", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBIsMeasureOnSite", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBIsMeasureOnSite", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobAddressAsAbove", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobDescription", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBPriceQuoted", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBPriceQuoted", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBReferenceName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBReferenceName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBUser", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBUser", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'colNIPaidByClient1
        '
        Me.colNIPaidByClient1.Caption = "NIPaidByClient"
        Me.colNIPaidByClient1.FieldName = "NIPaidByClient"
        Me.colNIPaidByClient1.Name = "colNIPaidByClient1"
        Me.colNIPaidByClient1.Visible = True
        Me.colNIPaidByClient1.VisibleIndex = 0
        '
        'colNIOrderedByClient1
        '
        Me.colNIOrderedByClient1.Caption = "NIOrderedByClient"
        Me.colNIOrderedByClient1.FieldName = "NIOrderedByClient"
        Me.colNIOrderedByClient1.Name = "colNIOrderedByClient1"
        Me.colNIOrderedByClient1.Visible = True
        Me.colNIOrderedByClient1.VisibleIndex = 0
        '
        'colNIDesc1
        '
        Me.colNIDesc1.Caption = "NIDesc"
        Me.colNIDesc1.FieldName = "NIDesc"
        Me.colNIDesc1.Name = "colNIDesc1"
        Me.colNIDesc1.Visible = True
        Me.colNIDesc1.VisibleIndex = 0
        '
        'colNIName1
        '
        Me.colNIName1.Caption = "NIName"
        Me.colNIName1.FieldName = "NIName"
        Me.colNIName1.Name = "colNIName1"
        Me.colNIName1.Visible = True
        Me.colNIName1.VisibleIndex = 0
        '
        'RepositoryItemLookUpEdit1
        '
        Me.RepositoryItemLookUpEdit1.AutoHeight = False
        Me.RepositoryItemLookUpEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemLookUpEdit1.Name = "RepositoryItemLookUpEdit1"
        '
        'daJobNonCoreSalesItems
        '
        Me.daJobNonCoreSalesItems.DeleteCommand = Me.SqlDeleteCommand2
        Me.daJobNonCoreSalesItems.InsertCommand = Me.SqlInsertCommand2
        Me.daJobNonCoreSalesItems.SelectCommand = Me.SqlSelectCommand2
        Me.daJobNonCoreSalesItems.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VJobsNonCoreSalesItems", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("NIID", "NIID")})})
        Me.daJobNonCoreSalesItems.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM JobsNonCoreSalesItems WHERE (BRID = @Original_BRID) AND (NIID = @Orig" & _
        "inal_NIID)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO JobsNonCoreSalesItems(BRID) VALUES (@BRID); SELECT BRID, NIID FROM Jo" & _
        "bsNonCoreSalesItems WHERE (BRID = @BRID) AND (NIID = @@IDENTITY)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT BRID, NIID, JBID, NIName, NIDesc, NICost, NIPrice, EXName, NIType, NIOrder" & _
        "edBy, NIPaidBy, NIUserType, NIBrand, NISupplier FROM VJobsNonCoreSalesItems WHER" & _
        "E (BRID = @BRID) AND (JBID = @JBID)"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection
        Me.SqlSelectCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE JobsNonCoreSalesItems SET BRID = @BRID WHERE (BRID = @Original_BRID) AND (" & _
        "NIID = @Original_NIID); SELECT BRID, NIID FROM JobsNonCoreSalesItems WHERE (BRID" & _
        " = @BRID) AND (NIID = @NIID)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIID", System.Data.SqlDbType.BigInt, 8, "NIID"))
        '
        'daJobs_Materials
        '
        Me.daJobs_Materials.DeleteCommand = Me.SqlDeleteCommand5
        Me.daJobs_Materials.InsertCommand = Me.SqlInsertCommand5
        Me.daJobs_Materials.SelectCommand = Me.SqlSelectCommand5
        Me.daJobs_Materials.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VJobs_Materials", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("JMID", "JMID"), New System.Data.Common.DataColumnMapping("JBID", "JBID"), New System.Data.Common.DataColumnMapping("MTID", "MTID"), New System.Data.Common.DataColumnMapping("JMPrimaryQuoted", "JMPrimaryQuoted"), New System.Data.Common.DataColumnMapping("JMPrimarySold", "JMPrimarySold")})})
        Me.daJobs_Materials.UpdateCommand = Me.SqlUpdateCommand5
        '
        'SqlDeleteCommand5
        '
        Me.SqlDeleteCommand5.CommandText = "DELETE FROM Jobs_Materials WHERE (BRID = @Original_BRID) AND (JMID = @Original_JM" & _
        "ID) AND (JBID = @Original_JBID) AND (JMPrimaryQuoted = @Original_JMPrimaryQuoted" & _
        " OR @Original_JMPrimaryQuoted IS NULL AND JMPrimaryQuoted IS NULL) AND (JMPrimar" & _
        "ySold = @Original_JMPrimarySold OR @Original_JMPrimarySold IS NULL AND JMPrimary" & _
        "Sold IS NULL) AND (MTID = @Original_MTID)"
        Me.SqlDeleteCommand5.Connection = Me.SqlConnection
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JMID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JMID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JMPrimaryQuoted", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "JMPrimaryQuoted", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JMPrimarySold", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "JMPrimarySold", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand5
        '
        Me.SqlInsertCommand5.CommandText = "INSERT INTO Jobs_Materials (BRID, JBID, MTID, JMPrimaryQuoted, JMPrimarySold) VAL" & _
        "UES (@BRID, @JBID, @MTID, @JMPrimaryQuoted, @JMPrimarySold); SELECT BRID, JMID, " & _
        "JBID, MTID, JMPrimaryQuoted, JMPrimarySold, MTStocked, MTPrimaryUOM, MTName, MTP" & _
        "rimaryUOMShortName FROM VJobs_Materials WHERE (BRID = @BRID) AND (JMID = @@IDENT" & _
        "ITY)"
        Me.SqlInsertCommand5.Connection = Me.SqlConnection
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JMPrimaryQuoted", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "JMPrimaryQuoted", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JMPrimarySold", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "JMPrimarySold", System.Data.DataRowVersion.Current, Nothing))
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT BRID, JMID, JBID, MTID, JMPrimaryQuoted, JMPrimarySold, MTStocked, MTPrima" & _
        "ryUOM, MTName, MTPrimaryUOMShortName FROM VJobs_Materials WHERE (BRID = @BRID) A" & _
        "ND (JBID = @JBID) AND (MTStocked = 1)"
        Me.SqlSelectCommand5.Connection = Me.SqlConnection
        Me.SqlSelectCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlUpdateCommand5
        '
        Me.SqlUpdateCommand5.CommandText = "UPDATE Jobs_Materials SET BRID = @BRID, JBID = @JBID, MTID = @MTID, JMPrimaryQuot" & _
        "ed = @JMPrimaryQuoted, JMPrimarySold = @JMPrimarySold WHERE (BRID = @Original_BR" & _
        "ID) AND (JMID = @Original_JMID) AND (JBID = @Original_JBID) AND (JMPrimaryQuoted" & _
        " = @Original_JMPrimaryQuoted OR @Original_JMPrimaryQuoted IS NULL AND JMPrimaryQ" & _
        "uoted IS NULL) AND (JMPrimarySold = @Original_JMPrimarySold OR @Original_JMPrima" & _
        "rySold IS NULL AND JMPrimarySold IS NULL) AND (MTID = @Original_MTID); SELECT BR" & _
        "ID, JMID, JBID, MTID, JMPrimaryQuoted, JMPrimarySold, MTStocked, MTPrimaryUOM, M" & _
        "TName, MTPrimaryUOMShortName FROM VJobs_Materials WHERE (BRID = @BRID) AND (JMID" & _
        " = @JMID)"
        Me.SqlUpdateCommand5.Connection = Me.SqlConnection
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JMPrimaryQuoted", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "JMPrimaryQuoted", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JMPrimarySold", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "JMPrimarySold", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JMID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JMID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JMPrimaryQuoted", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "JMPrimaryQuoted", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JMPrimarySold", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "JMPrimarySold", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JMID", System.Data.SqlDbType.BigInt, 8, "JMID"))
        '
        'btnHelp
        '
        Me.btnHelp.Image = CType(resources.GetObject("btnHelp.Image"), System.Drawing.Image)
        Me.btnHelp.Location = New System.Drawing.Point(8, 496)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.TabIndex = 2
        Me.btnHelp.Text = "Help"
        '
        'HelpProvider1
        '
        Me.HelpProvider1.HelpNamespace = "C:\Documents and Settings\djpower\My Documents\My Projects\GTMS\Version1\Help\Hel" & _
        "pProject.chm"
        '
        'pnlMain
        '
        Me.pnlMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlMain.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.pnlMain.Controls.Add(Me.pnlScheduling)
        Me.pnlMain.Controls.Add(Me.pnlJobAddress)
        Me.pnlMain.Controls.Add(Me.pnlClientInformation)
        Me.pnlMain.Controls.Add(Me.pnlGranite)
        Me.pnlMain.Controls.Add(Me.PnlOtherTrades)
        Me.pnlMain.Controls.Add(Me.pnlNotes)
        Me.pnlMain.Controls.Add(Me.PnlJobInformation)
        Me.pnlMain.Controls.Add(Me.pnlAppliances)
        Me.pnlMain.Location = New System.Drawing.Point(152, 8)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(568, 480)
        Me.pnlMain.TabIndex = 1
        '
        'pnlNotes
        '
        Me.pnlNotes.Controls.Add(Me.dgNotes)
        Me.pnlNotes.Location = New System.Drawing.Point(48, 32)
        Me.pnlNotes.Name = "pnlNotes"
        Me.pnlNotes.Size = New System.Drawing.Size(496, 408)
        Me.pnlNotes.TabIndex = 19
        Me.pnlNotes.Text = "Notes"
        '
        'dgNotes
        '
        Me.dgNotes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgNotes.DataSource = Me.DsGTMS.IDNotes
        '
        'dgNotes.EmbeddedNavigator
        '
        Me.dgNotes.EmbeddedNavigator.Name = ""
        Me.dgNotes.Location = New System.Drawing.Point(16, 32)
        Me.dgNotes.MainView = Me.gvNotes
        Me.dgNotes.Name = "dgNotes"
        Me.dgNotes.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtUser, Me.RepositoryItemMemoEdit1, Me.txtINDate})
        Me.dgNotes.Size = New System.Drawing.Size(464, 360)
        Me.dgNotes.TabIndex = 0
        Me.dgNotes.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvNotes})
        '
        'gvNotes
        '
        Me.gvNotes.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvNotes.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvNotes.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvNotes.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvNotes.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvNotes.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvNotes.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvNotes.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvNotes.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvNotes.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvNotes.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvNotes.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvNotes.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvNotes.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvNotes.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvNotes.Appearance.VertLine.Options.UseBackColor = True
        Me.gvNotes.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colINUser, Me.colINDate, Me.colINNotes})
        Me.gvNotes.GridControl = Me.dgNotes
        Me.gvNotes.Name = "gvNotes"
        Me.gvNotes.NewItemRowText = "Type here to add a new row"
        Me.gvNotes.OptionsCustomization.AllowFilter = False
        Me.gvNotes.OptionsCustomization.AllowRowSizing = True
        Me.gvNotes.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvNotes.OptionsView.RowAutoHeight = True
        Me.gvNotes.OptionsView.ShowGroupPanel = False
        Me.gvNotes.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colINDate, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colINUser
        '
        Me.colINUser.Caption = "User"
        Me.colINUser.ColumnEdit = Me.txtUser
        Me.colINUser.FieldName = "INUser"
        Me.colINUser.Name = "colINUser"
        Me.colINUser.Visible = True
        Me.colINUser.VisibleIndex = 1
        Me.colINUser.Width = 516
        '
        'txtUser
        '
        Me.txtUser.AutoHeight = False
        Me.txtUser.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXName")})
        Me.txtUser.DataSource = Me.dvExpenses
        Me.txtUser.DisplayMember = "EXName"
        Me.txtUser.Name = "txtUser"
        Me.txtUser.NullText = ""
        Me.txtUser.ShowFooter = False
        Me.txtUser.ShowHeader = False
        Me.txtUser.ShowLines = False
        Me.txtUser.ValueMember = "EXName"
        '
        'colINDate
        '
        Me.colINDate.Caption = "Date"
        Me.colINDate.ColumnEdit = Me.txtINDate
        Me.colINDate.FieldName = "INDate"
        Me.colINDate.Name = "colINDate"
        Me.colINDate.Visible = True
        Me.colINDate.VisibleIndex = 0
        Me.colINDate.Width = 381
        '
        'txtINDate
        '
        Me.txtINDate.AutoHeight = False
        Me.txtINDate.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtINDate.Name = "txtINDate"
        '
        'colINNotes
        '
        Me.colINNotes.Caption = "Notes"
        Me.colINNotes.ColumnEdit = Me.RepositoryItemMemoEdit1
        Me.colINNotes.FieldName = "INNotes"
        Me.colINNotes.Name = "colINNotes"
        Me.colINNotes.Visible = True
        Me.colINNotes.VisibleIndex = 2
        Me.colINNotes.Width = 897
        '
        'RepositoryItemMemoEdit1
        '
        Me.RepositoryItemMemoEdit1.Name = "RepositoryItemMemoEdit1"
        '
        'daExpenses
        '
        Me.daExpenses.DeleteCommand = Me.SqlDeleteCommand3
        Me.daExpenses.InsertCommand = Me.SqlInsertCommand3
        Me.daExpenses.SelectCommand = Me.SqlSelectCommand3
        Me.daExpenses.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Expenses", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("EXName", "EXName")})})
        Me.daExpenses.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM Expenses WHERE (BRID = @Original_BRID) AND (EXID = @Original_EXID) AN" & _
        "D (EXName = @Original_EXName OR @Original_EXName IS NULL AND EXName IS NULL)"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXName", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO Expenses (BRID, EXName) VALUES (@BRID, @EXName); SELECT BRID, EXID, E" & _
        "XName, EGType, EXAppearInCalendar, EXCalendarName, EGName FROM VExpenses WHERE (" & _
        "BRID = @BRID) AND (EXID = @@IDENTITY)"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT BRID, EXID, EXName, EGType, EXAppearInCalendar, EXCalendarName, EGName FRO" & _
        "M VExpenses WHERE (EXDiscontinued = 0) AND (BRID = @BRID)"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection
        Me.SqlSelectCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE Expenses SET BRID = @BRID, EXName = @EXName WHERE (BRID = @Original_BRID) " & _
        "AND (EXID = @Original_EXID) AND (EXName = @Original_EXName OR @Original_EXName I" & _
        "S NULL AND EXName IS NULL); SELECT BRID, EXID, EXName, EGType, EXAppearInCalenda" & _
        "r, EXCalendarName, EGName FROM VExpenses WHERE (BRID = @BRID) AND (EXID = @EXID)" & _
        ""
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        '
        'daAppointments
        '
        Me.daAppointments.DeleteCommand = Me.SqlDeleteCommand4
        Me.daAppointments.InsertCommand = Me.SqlInsertCommand4
        Me.daAppointments.SelectCommand = Me.SqlSelectCommand4
        Me.daAppointments.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VAppointments", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("APID", "APID"), New System.Data.Common.DataColumnMapping("APBegin", "APBegin"), New System.Data.Common.DataColumnMapping("APEnd", "APEnd"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("APType", "APType"), New System.Data.Common.DataColumnMapping("APTypeID", "APTypeID"), New System.Data.Common.DataColumnMapping("APNotes", "APNotes"), New System.Data.Common.DataColumnMapping("APStatus", "APStatus"), New System.Data.Common.DataColumnMapping("APTask", "APTask")})})
        Me.daAppointments.UpdateCommand = Me.SqlUpdateCommand4
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = "DELETE FROM Appointments WHERE (APID = @Original_APID) AND (BRID = @Original_BRID" & _
        ") AND (APBegin = @Original_APBegin OR @Original_APBegin IS NULL AND APBegin IS N" & _
        "ULL) AND (APEnd = @Original_APEnd OR @Original_APEnd IS NULL AND APEnd IS NULL) " & _
        "AND (APNotes = @Original_APNotes OR @Original_APNotes IS NULL AND APNotes IS NUL" & _
        "L) AND (APStatus = @Original_APStatus OR @Original_APStatus IS NULL AND APStatus" & _
        " IS NULL) AND (APTask = @Original_APTask OR @Original_APTask IS NULL AND APTask " & _
        "IS NULL) AND (APType = @Original_APType OR @Original_APType IS NULL AND APType I" & _
        "S NULL) AND (APTypeID = @Original_APTypeID OR @Original_APTypeID IS NULL AND APT" & _
        "ypeID IS NULL) AND (EXID = @Original_EXID OR @Original_EXID IS NULL AND EXID IS " & _
        "NULL)"
        Me.SqlDeleteCommand4.Connection = Me.SqlConnection
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APBegin", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APBegin", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APEnd", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APEnd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APNotes", System.Data.SqlDbType.VarChar, 2000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APStatus", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APStatus", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APTask", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APTask", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APTypeID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APTypeID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = "INSERT INTO Appointments (BRID, APBegin, APEnd, EXID, APType, APTypeID, APNotes, " & _
        "APStatus, APTask) VALUES (@BRID, @APBegin, @APEnd, @EXID, @APType, @APTypeID, @A" & _
        "PNotes, @APStatus, @APTask); SELECT BRID, APID, APBegin, APEnd, EXID, APType, AP" & _
        "TypeID, APNotes, APStatus, APTask, APDescription, APAddressMultiline, ID, APClie" & _
        "ntName, EXName, APJobDescription, APClientPhoneNumber, APClientMobileNumber, APS" & _
        "uburb, TAName FROM VAppointments WHERE (APID = @@IDENTITY) AND (BRID = @BRID)"
        Me.SqlInsertCommand4.Connection = Me.SqlConnection
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APBegin", System.Data.SqlDbType.DateTime, 8, "APBegin"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APEnd", System.Data.SqlDbType.DateTime, 8, "APEnd"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APType", System.Data.SqlDbType.VarChar, 2, "APType"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTypeID", System.Data.SqlDbType.BigInt, 8, "APTypeID"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APNotes", System.Data.SqlDbType.VarChar, 2000, "APNotes"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APStatus", System.Data.SqlDbType.Int, 4, "APStatus"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTask", System.Data.SqlDbType.Int, 4, "APTask"))
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT BRID, APID, APBegin, APEnd, EXID, APType, APTypeID, APNotes, APStatus, APT" & _
        "ask, APDescription, APAddressMultiline, ID, APClientName, EXName, APJobDescripti" & _
        "on, APClientPhoneNumber, APClientMobileNumber, APSuburb, TAName FROM VAppointmen" & _
        "ts WHERE (BRID = @BRID) AND (APIsCancelled = 0)"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection
        Me.SqlSelectCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = "UPDATE Appointments SET BRID = @BRID, APBegin = @APBegin, APEnd = @APEnd, EXID = " & _
        "@EXID, APType = @APType, APTypeID = @APTypeID, APNotes = @APNotes, APStatus = @A" & _
        "PStatus, APTask = @APTask WHERE (APID = @Original_APID) AND (BRID = @Original_BR" & _
        "ID) AND (APBegin = @Original_APBegin OR @Original_APBegin IS NULL AND APBegin IS" & _
        " NULL) AND (APEnd = @Original_APEnd OR @Original_APEnd IS NULL AND APEnd IS NULL" & _
        ") AND (APNotes = @Original_APNotes OR @Original_APNotes IS NULL AND APNotes IS N" & _
        "ULL) AND (APStatus = @Original_APStatus OR @Original_APStatus IS NULL AND APStat" & _
        "us IS NULL) AND (APTask = @Original_APTask OR @Original_APTask IS NULL AND APTas" & _
        "k IS NULL) AND (APType = @Original_APType OR @Original_APType IS NULL AND APType" & _
        " IS NULL) AND (APTypeID = @Original_APTypeID OR @Original_APTypeID IS NULL AND A" & _
        "PTypeID IS NULL) AND (EXID = @Original_EXID OR @Original_EXID IS NULL AND EXID I" & _
        "S NULL); SELECT BRID, APID, APBegin, APEnd, EXID, APType, APTypeID, APNotes, APS" & _
        "tatus, APTask, APDescription, APAddressMultiline, ID, APClientName, EXName, APJo" & _
        "bDescription, APClientPhoneNumber, APClientMobileNumber, APSuburb, TAName FROM V" & _
        "Appointments WHERE (APID = @APID) AND (BRID = @BRID)"
        Me.SqlUpdateCommand4.Connection = Me.SqlConnection
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APBegin", System.Data.SqlDbType.DateTime, 8, "APBegin"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APEnd", System.Data.SqlDbType.DateTime, 8, "APEnd"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APType", System.Data.SqlDbType.VarChar, 2, "APType"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTypeID", System.Data.SqlDbType.BigInt, 8, "APTypeID"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APNotes", System.Data.SqlDbType.VarChar, 2000, "APNotes"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APStatus", System.Data.SqlDbType.Int, 4, "APStatus"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTask", System.Data.SqlDbType.Int, 4, "APTask"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APBegin", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APBegin", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APEnd", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APEnd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APNotes", System.Data.SqlDbType.VarChar, 2000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APStatus", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APStatus", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APTask", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APTask", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APTypeID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APTypeID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APID", System.Data.SqlDbType.BigInt, 8, "APID"))
        '
        'Storage
        '
        Me.Storage.Appointments.DataSource = Me.dvAppointments
        Me.Storage.Appointments.Mappings.AllDay = "APAllDay"
        Me.Storage.Appointments.Mappings.End = "APEnd"
        Me.Storage.Appointments.Mappings.Label = "APTask"
        Me.Storage.Appointments.Mappings.Location = "APSuburb"
        Me.Storage.Appointments.Mappings.ResourceId = "EXID"
        Me.Storage.Appointments.Mappings.Start = "APBegin"
        Me.Storage.Appointments.Mappings.Status = "APStatus"
        Me.Storage.Appointments.Mappings.Subject = "APDescription"
        Me.Storage.Appointments.Statuses.Add(New DevExpress.XtraScheduler.AppointmentStatus(DevExpress.XtraScheduler.AppointmentStatusType.Free, "Free", "&Free"))
        Me.Storage.Appointments.Statuses.Add(New DevExpress.XtraScheduler.AppointmentStatus(DevExpress.XtraScheduler.AppointmentStatusType.Tentative, System.Drawing.Color.FromArgb(CType(74, Byte), CType(135, Byte), CType(226, Byte)), "Tentative", "&Tentative"))
        Me.Storage.Appointments.Statuses.Add(New DevExpress.XtraScheduler.AppointmentStatus(DevExpress.XtraScheduler.AppointmentStatusType.Busy, System.Drawing.Color.FromArgb(CType(74, Byte), CType(135, Byte), CType(226, Byte)), "Busy", "&Busy"))
        Me.Storage.Appointments.Statuses.Add(New DevExpress.XtraScheduler.AppointmentStatus(DevExpress.XtraScheduler.AppointmentStatusType.OutOfOffice, System.Drawing.Color.FromArgb(CType(217, Byte), CType(83, Byte), CType(83, Byte)), "Off Work", "&Off Work"))
        Me.Storage.Resources.DataSource = Me.dvResources
        Me.Storage.Resources.Mappings.Caption = "EXCalendarName"
        Me.Storage.Resources.Mappings.Id = "EXID"
        '
        'daTasks
        '
        Me.daTasks.InsertCommand = Me.SqlCommand1
        Me.daTasks.SelectCommand = Me.SqlCommand2
        Me.daTasks.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Tasks", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("TAName", "TAName"), New System.Data.Common.DataColumnMapping("TAControlsJobDate", "TAControlsJobDate"), New System.Data.Common.DataColumnMapping("TAMenuCaption", "TAMenuCaption"), New System.Data.Common.DataColumnMapping("TAColourR", "TAColourR"), New System.Data.Common.DataColumnMapping("TAColourG", "TAColourG"), New System.Data.Common.DataColumnMapping("TAColourB", "TAColourB"), New System.Data.Common.DataColumnMapping("TAID", "TAID")})})
        '
        'SqlCommand1
        '
        Me.SqlCommand1.CommandText = "INSERT INTO Tasks(TAName, TAControlsJobDate, TAMenuCaption, TAColourR, TAColourG," & _
        " TAColourB, TAID) VALUES (@TAName, @TAControlsJobDate, @TAMenuCaption, @TAColour" & _
        "R, @TAColourG, @TAColourB, @TAID); SELECT TAName, TAControlsJobDate, TAMenuCapti" & _
        "on, TAColourR, TAColourG, TAColourB, TAID FROM Tasks ORDER BY TAID"
        Me.SqlCommand1.Connection = Me.SqlConnection
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAName", System.Data.SqlDbType.VarChar, 50, "TAName"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAControlsJobDate", System.Data.SqlDbType.Bit, 1, "TAControlsJobDate"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAMenuCaption", System.Data.SqlDbType.VarChar, 50, "TAMenuCaption"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourR", System.Data.SqlDbType.SmallInt, 2, "TAColourR"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourG", System.Data.SqlDbType.SmallInt, 2, "TAColourG"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourB", System.Data.SqlDbType.SmallInt, 2, "TAColourB"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAID", System.Data.SqlDbType.Int, 4, "TAID"))
        '
        'SqlCommand2
        '
        Me.SqlCommand2.CommandText = "SELECT TAName, TAControlsJobDate, TAMenuCaption, TAColourR, TAColourG, TAColourB," & _
        " TAID FROM Tasks ORDER BY TAID"
        Me.SqlCommand2.Connection = Me.SqlConnection
        '
        'daIDNotes
        '
        Me.daIDNotes.DeleteCommand = Me.SqlDeleteCommand6
        Me.daIDNotes.InsertCommand = Me.SqlInsertCommand6
        Me.daIDNotes.SelectCommand = Me.SqlSelectCommand6
        Me.daIDNotes.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "IDNotes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("INID", "INID"), New System.Data.Common.DataColumnMapping("ID", "ID"), New System.Data.Common.DataColumnMapping("INUser", "INUser"), New System.Data.Common.DataColumnMapping("INDate", "INDate"), New System.Data.Common.DataColumnMapping("INNotes", "INNotes")})})
        Me.daIDNotes.UpdateCommand = Me.SqlUpdateCommand6
        '
        'SqlDeleteCommand6
        '
        Me.SqlDeleteCommand6.CommandText = "DELETE FROM IDNotes WHERE (BRID = @Original_BRID) AND (INID = @Original_INID) AND" & _
        " (ID = @Original_ID) AND (INDate = @Original_INDate OR @Original_INDate IS NULL " & _
        "AND INDate IS NULL) AND (INNotes = @Original_INNotes OR @Original_INNotes IS NUL" & _
        "L AND INNotes IS NULL) AND (INUser = @Original_INUser OR @Original_INUser IS NUL" & _
        "L AND INUser IS NULL)"
        Me.SqlDeleteCommand6.Connection = Me.SqlConnection
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INNotes", System.Data.SqlDbType.VarChar, 1000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INUser", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INUser", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand6
        '
        Me.SqlInsertCommand6.CommandText = "INSERT INTO IDNotes(BRID, ID, INUser, INDate, INNotes) VALUES (@BRID, @ID, @INUse" & _
        "r, @INDate, @INNotes); SELECT BRID, INID, ID, INUser, INDate, INNotes FROM IDNot" & _
        "es WHERE (BRID = @BRID) AND (INID = @@IDENTITY)"
        Me.SqlInsertCommand6.Connection = Me.SqlConnection
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INUser", System.Data.SqlDbType.VarChar, 50, "INUser"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INDate", System.Data.SqlDbType.DateTime, 8, "INDate"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INNotes", System.Data.SqlDbType.VarChar, 1000, "INNotes"))
        '
        'SqlSelectCommand6
        '
        Me.SqlSelectCommand6.CommandText = "SELECT BRID, INID, ID, INUser, INDate, INNotes FROM IDNotes WHERE (BRID = @BRID) " & _
        "AND (ID = @ID)"
        Me.SqlSelectCommand6.Connection = Me.SqlConnection
        Me.SqlSelectCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        '
        'SqlUpdateCommand6
        '
        Me.SqlUpdateCommand6.CommandText = "UPDATE IDNotes SET BRID = @BRID, ID = @ID, INUser = @INUser, INDate = @INDate, IN" & _
        "Notes = @INNotes WHERE (BRID = @Original_BRID) AND (INID = @Original_INID) AND (" & _
        "ID = @Original_ID) AND (INDate = @Original_INDate OR @Original_INDate IS NULL AN" & _
        "D INDate IS NULL) AND (INNotes = @Original_INNotes OR @Original_INNotes IS NULL " & _
        "AND INNotes IS NULL) AND (INUser = @Original_INUser OR @Original_INUser IS NULL " & _
        "AND INUser IS NULL); SELECT BRID, INID, ID, INUser, INDate, INNotes FROM IDNotes" & _
        " WHERE (BRID = @BRID) AND (INID = @INID)"
        Me.SqlUpdateCommand6.Connection = Me.SqlConnection
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INUser", System.Data.SqlDbType.VarChar, 50, "INUser"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INDate", System.Data.SqlDbType.DateTime, 8, "INDate"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INNotes", System.Data.SqlDbType.VarChar, 1000, "INNotes"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INNotes", System.Data.SqlDbType.VarChar, 1000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INUser", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INUser", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INID", System.Data.SqlDbType.BigInt, 8, "INID"))
        '
        'frmBooking
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(730, 528)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.btnHelp)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.ListBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBooking"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Booking"
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkJBIsAllOrderingComplete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEXIDBookingMeasurer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvDirectLabour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsCalendar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvResources, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgOtherTrades, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvOtherTrades, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvOtherTrades, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvAppliances, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgAppliances, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvAppliances, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientFirstName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientReferenceName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientSuburb.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientPhoneNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientFaxNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientMobileNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientState.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientPostCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlScheduling, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlScheduling.ResumeLayout(False)
        CType(Me.dgAppointments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvAppointments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvAppointments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PnlOtherTrades, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PnlOtherTrades.ResumeLayout(False)
        CType(Me.pnlGranite, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlGranite.ResumeLayout(False)
        CType(Me.dgGranite, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvGranite, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAmountGranite, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlAppliances, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlAppliances.ResumeLayout(False)
        CType(Me.PnlJobInformation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PnlJobInformation.ResumeLayout(False)
        CType(Me.dpLDDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDUser.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvExpenses, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvSalesReps, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBPriceQuoted.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBJobDescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlJobAddress, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlJobAddress.ResumeLayout(False)
        CType(Me.txtJBJobStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgJBJobAddressAsAbove.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBJobStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBJobSuburb.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBJobState.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBJobPostCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlClientInformation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlClientInformation.ResumeLayout(False)
        CType(Me.txtJBClientEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemLookUpEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMain.ResumeLayout(False)
        CType(Me.pnlNotes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlNotes.ResumeLayout(False)
        CType(Me.dgNotes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvNotes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUser, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtINDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemMemoEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Storage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnOK.Enabled = Not HasRole("branch_read_only")
        ListBox.SelectedIndex = 0
    End Sub

    Private Sub LoadTreeViews()
        ' --- MATERIALS ---
        tvGranite.Filter.SetOrAddField("BRID", BRID)
        tvGranite.LoadFromStream(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream("WindowsApplication.xmlJobsGraniteTree.xml"))
        tvGranite.ExpandAll()
    End Sub

    Private Sub FillPreliminaryData()
        ' --- EXPENSES ---
        daExpenses.SelectCommand.Parameters("@BRID").Value = BRID
        daExpenses.Fill(DsGTMS)
        daExpenses.Fill(DsCalendar.VExpenses)
    End Sub

    Private Sub FillData()
        ' --- ID NOTES ---
        daIDNotes.SelectCommand.Parameters("@BRID").Value = BRID
        daIDNotes.SelectCommand.Parameters("@ID").Value = DataRow("ID")
        daIDNotes.Fill(DsGTMS)
        ' --- MATERIALS ---
        daJobs_Materials.SelectCommand.Parameters("@BRID").Value = BRID
        daJobs_Materials.SelectCommand.Parameters("@JBID").Value = JBID
        daJobs_Materials.Fill(DsGTMS)
        ' --- APPLIANCES AND OTHER TRADES ---
        daJobNonCoreSalesItems.SelectCommand.Parameters("@BRID").Value = BRID
        daJobNonCoreSalesItems.SelectCommand.Parameters("@JBID").Value = JBID
        daJobNonCoreSalesItems.Fill(DsGTMS)
        ' --- APPOINTMENTS ---
        daAppointments.SelectCommand.Parameters("@BRID").Value = BRID
        daAppointments.Fill(DsCalendar.VAppointments)
        dvAppointments.RowFilter = "(BRID = " & BRID & ") AND (APType = 'JB') AND (APTypeID = " & JBID & ")"
        ' --- TASKS ---
        daTasks.Fill(DsCalendar)
        ' --- Calendar ---
        SetUpCalendar()
    End Sub

    Private Sub EnableDisable()
        lblJobStreetAddress.Enabled = Not rgJBJobAddressAsAbove.EditValue
        lblJobSuburb.Enabled = Not rgJBJobAddressAsAbove.EditValue
        lblJobState.Enabled = Not rgJBJobAddressAsAbove.EditValue
        lblJobPostCode.Enabled = Not rgJBJobAddressAsAbove.EditValue
        txtJBJobStreetAddress01.Enabled = Not rgJBJobAddressAsAbove.EditValue
        txtJBJobStreetAddress02.Enabled = Not rgJBJobAddressAsAbove.EditValue
        txtJBJobSuburb.Enabled = Not rgJBJobAddressAsAbove.EditValue
        txtJBJobState.Enabled = Not rgJBJobAddressAsAbove.EditValue
        txtJBJobPostCode.Enabled = Not rgJBJobAddressAsAbove.EditValue
    End Sub

    Private Const DEFAULT_FILTER As String = "EXAppearInCalendar = 1"
    Private Const DEFAULT_SORT As String = "EGName, EXName"
    Private Sub SetUpCalendar()
        Storage.Appointments.Labels.Clear()
        For Each task As DataRow In DsCalendar.Tasks.Rows
            Storage.Appointments.Labels.Add(System.Drawing.Color.FromArgb(task("TAColourR"), task("TAColourG"), task("TAColourB")), _
                task("TAName"), task("TAMenuCaption"))
        Next
        ' --- SET UP FILTER ---
        dvResources.RowFilter = "(" & DEFAULT_FILTER & ") AND (EGType = 'DL')"
        dvResources.Sort = DEFAULT_SORT
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        ' EndEdit() to end editing the dataset record so that we can update
        DataRow.EndEdit()

        If ValidateForm() Then
            SqlDataAdapter.Update(DsGTMS)
            daJobs_Materials.Update(DsGTMS)
            daIDNotes.Update(DsGTMS)
            WriteMRU(txtJBJobDescription, UserAppDataPath)

            ReleaseAppointmentLocks()
            DataAccess.spExecLockRequest("sp_ReleaseJobIDLock", BRID, JBID, Transaction)
            Transaction.Commit()
            SqlConnection.Close()

            OK = True
            Me.Close()
        End If
    End Sub

    Private Function ValidateForm() As Boolean
        Return True
    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private HasChanges As Boolean = False
    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            DataRow.EndEdit()
            If HasChanges Or DsGTMS.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If response = MsgBoxResult.Yes Then
                ReleaseAppointmentLocks()
                DataAccess.spExecLockRequest("sp_ReleaseJobIDLock", BRID, JBID, Transaction)
                Transaction.Rollback()
                SqlConnection.Close()
            Else
                e.Cancel = True
            End If
        End If
    End Sub

    Private HelpTopic As String = Nothing
    Private Sub ListBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox.SelectedIndexChanged
        Dim pnl As DevExpress.XtraEditors.GroupControl

        For Each pnl In pnlMain.Controls
            pnl.Enabled = False
        Next
        pnl = Nothing
        Select Case ListBox.SelectedIndex
            Case 0
                pnl = pnlClientInformation
                HelpTopic = "ClientInformation"
            Case 1
                pnl = pnlJobAddress
                HelpTopic = "JobAddress"
            Case 2
                pnl = PnlJobInformation
                HelpTopic = "JobInformation"
            Case 3
                pnl = pnlGranite
                HelpTopic = "StockedMaterial"
            Case 4
                pnl = pnlAppliances
                HelpTopic = "Appliances"
            Case 5
                pnl = PnlOtherTrades
                HelpTopic = "OtherTrades"
            Case 6
                pnl = pnlScheduling
                DataRow.EndEdit()
                SqlDataAdapter.Update(DsGTMS)
                daAppointments.Fill(DsCalendar.VAppointments)
                'Calendar.RefreshCalendar()
                HelpTopic = "Scheduling"
            Case 7
                pnl = pnlNotes
                HelpTopic = "Notes"
        End Select
        If Not pnl Is Nothing Then
            pnl.Enabled = True
            pnl.Dock = DockStyle.Fill
            Power.Library.Library.CenterControls(pnl)
            pnl.BringToFront()
        End If

    End Sub

#Region " Granite "

    Public ReadOnly Property SelectedGranite() As DataRow
        Get
            If Not gvGranite.GetSelectedRows Is Nothing Then
                Return gvGranite.GetDataRow(gvGranite.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub btnAddGranite_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddGranite.Click
        Dim row As dsGTMS.VJobs_MaterialsRow = DsGTMS.VJobs_Materials.NewVJobs_MaterialsRow
        row.BRID = BRID
        row.JBID = JBID
        If CType(tvGranite.SelectedNode, Power.Forms.TreeNode).Action = "ADD" Then
            row.MTID = CType(tvGranite.SelectedNode, Power.Forms.TreeNode).PKey("MTID")
        ElseIf CType(tvGranite.SelectedNode, Power.Forms.TreeNode).Action = "LIST" Then
            Message.SelectFromList(tvGranite.SelectedNode.Text)
            Exit Sub
        End If
        Try
            DsGTMS.VJobs_Materials.AddVJobs_MaterialsRow(row)
            daJobs_Materials.Update(DsGTMS)
            HasChanges = True
        Catch ex As System.Data.ConstraintException
            row.Delete()
            Message.AlreadyExistsInJob(tvGranite.SelectedNode.Text)
        End Try
    End Sub

    Private Sub btnRemoveStockedItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveGranite.Click
        If Not SelectedGranite Is Nothing Then
            SelectedGranite.Delete()
        End If
    End Sub

#End Region

#Region " Appliances "

    Public ReadOnly Property SelectedAppliance() As DataRow
        Get
            If Not gvAppliances.GetSelectedRows Is Nothing Then
                Return gvAppliances.GetDataRow(gvAppliances.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub dgAppliances_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgAppliances.DoubleClick
        btnEditAppliance_Click(sender, e)
    End Sub

    Private Sub btnAddAppliance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAppliance.Click
        Dim c As Cursor = Me.Cursor
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim gui As frmBookingAppliance = frmBookingAppliance.Add(BRID, JBID, Transaction)
        gui.ShowDialog(Me)
        daJobNonCoreSalesItems.Fill(DsGTMS)
        HasChanges = True
        Me.Cursor = c
    End Sub

    Private Sub btnEditAppliance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditAppliance.Click
        If Not SelectedAppliance Is Nothing Then
            Dim c As Cursor = Me.Cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Dim gui As frmBookingAppliance = frmBookingAppliance.Edit(SelectedAppliance.Item("BRID"), SelectedAppliance.Item("NIID"), Transaction)
            gui.ShowDialog(Me)
            daJobNonCoreSalesItems.Fill(DsGTMS)
            HasChanges = True
            Me.Cursor = c
        End If
    End Sub

    Private Sub btnRemoveAppliance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveAppliance.Click
        If Not SelectedAppliance Is Nothing Then
            SelectedAppliance.Delete()
            HasChanges = True
            daJobNonCoreSalesItems.Update(DsGTMS)
        End If
    End Sub

#End Region

#Region " Other Trades "

    Public ReadOnly Property SelectedOtherTrade() As DataRow
        Get
            If Not gvOtherTrades.GetSelectedRows Is Nothing Then
                Return gvOtherTrades.GetDataRow(gvOtherTrades.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub dgOtherTrades_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgOtherTrades.DoubleClick
        btnEditOtherTrade_Click(sender, e)
    End Sub

    Private Sub btnAddOtherTrade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddOtherTrade.Click
        Dim c As Cursor = Me.Cursor
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim gui As frmBookingOtherTrade = frmBookingOtherTrade.Add(BRID, JBID, Transaction)
        gui.ShowDialog(Me)
        daJobNonCoreSalesItems.Fill(DsGTMS)
        HasChanges = True
        Me.Cursor = c
    End Sub

    Private Sub btnEditOtherTrade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditOtherTrade.Click
        If Not SelectedOtherTrade Is Nothing Then
            Dim c As Cursor = Me.Cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Dim gui As frmBookingOtherTrade = frmBookingOtherTrade.Edit(SelectedOtherTrade.Item("BRID"), SelectedOtherTrade.Item("NIID"), Transaction)
            gui.ShowDialog(Me)
            daJobNonCoreSalesItems.Fill(DsGTMS)
            HasChanges = True
            Me.Cursor = c
        End If
    End Sub

    Private Sub btnRemoveOtherTrade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveOtherTrade.Click
        If Not SelectedOtherTrade Is Nothing Then
            SelectedOtherTrade.Delete()
            HasChanges = True
            daJobNonCoreSalesItems.Update(DsGTMS)
        End If
    End Sub

#End Region

#Region " Appointments "

    Public ReadOnly Property SelectedAppointment() As DataRow
        Get
            If Not gvAppointments.GetSelectedRows Is Nothing Then
                Return gvAppointments.GetDataRow(gvAppointments.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub Calendar_NewAllDayEvent(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim c As Cursor = Me.Cursor
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim gui As frmAppointment = frmAppointment.Add(Storage, BRID, "JB", JBID, Transaction, 1)
        AddAppointmentLock(gui.APID)
        gui.APBegin = Today.Date
        gui.APEnd = DateAdd(DateInterval.Day, 1, Today.Date)
        gui.APAllDay = True
        gui.APTask = 1
        gui.ShowDialog(Me)
        HasChanges = True
        daAppointments.Fill(DsCalendar.VAppointments)
        Me.Cursor = c
    End Sub

    Private Sub Calendar_NewAppointment(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim c As Cursor = Me.Cursor
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim gui As frmAppointment = frmAppointment.Add(Storage, BRID, "JB", JBID, Transaction, 1)
        AddAppointmentLock(gui.APID)
        gui.APBegin = Today.Date
        gui.APEnd = DateAdd(DateInterval.Hour, 1, Today.Date)
        gui.APTask = 1
        gui.ShowDialog(Me)
        HasChanges = True
        daAppointments.Fill(DsCalendar.VAppointments)
        Me.Cursor = c
    End Sub

    Private Sub btnAddAppointment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAppointment.Click
        Calendar_NewAllDayEvent(sender, e)
    End Sub

    Private Sub Calendar_EditAppointment(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not SelectedAppointment Is Nothing Then
            If Not (SelectedAppointment.Item("BRID") = Me.BRID And SelectedAppointment.Item("APType") = "JB" And SelectedAppointment.Item("APTypeID") = JBID) Then
                If Message.AppointmentNotCurrentObject("job", Message.ObjectAction.Edit) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If
            Dim c As Cursor = Me.Cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If AddAppointmentLock(SelectedAppointment.Item("APID")) Then
                Dim gui As frmAppointment = frmAppointment.Edit(Storage, SelectedAppointment.Item("BRID"), SelectedAppointment.Item("APID"), Transaction, 1)
                gui.ShowDialog(Me)
                HasChanges = True
                daAppointments.Fill(DsCalendar.VAppointments)
            Else
                Message.CurrentlyAccessed("appointment")
            End If
            Me.Cursor = c
        End If
    End Sub

    Private Sub dgAppointments_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgAppointments.DoubleClick
        Calendar_EditAppointment(sender, e)
    End Sub

    Private Sub btnEditAppointment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditAppointment.Click
        Calendar_EditAppointment(sender, e)
    End Sub

    Private Sub btnRemoveAppointment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveAppointment.Click
        If Not SelectedAppointment Is Nothing Then
            If Not (SelectedAppointment.Item("BRID") = Me.BRID And SelectedAppointment.Item("APType") = "JB" And SelectedAppointment.Item("APTypeID") = JBID) Then
                If Message.AppointmentNotCurrentObject("job", Message.ObjectAction.Delete) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If
            If AddAppointmentLock(SelectedAppointment.Item("APID")) Then
                SelectedAppointment.Delete()
                HasChanges = True
                daAppointments.Update(DsCalendar.VAppointments)
            Else
                Message.CurrentlyAccessed("appointment")
            End If
        End If
    End Sub

    Private Sub btnViewCalendar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewCalendar.Click
        Dim gui As New frmCalendar(BRID, "JB", JBID, Transaction)
        gui.EditedAppointmentList = EditedAppointmentList
        gui.Calendar.Calendar.SelectedInterval.Start = Today.Date
        gui.Calendar.Calendar.SelectedInterval.End = Today.Date
        gui.Calendar.Calendar.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Week
        gui.ShowDialog(Me)
        HasChanges = True
        EditedAppointmentList = gui.EditedAppointmentList
        DsCalendar.VAppointments.Clear()
        daAppointments.Fill(DsCalendar.VAppointments)
    End Sub

    Private EditedAppointmentList As New ArrayList

    Private Function AddAppointmentLock(ByVal APID As Long) As Boolean
        If Not IsInEditList(APID) Then
            If DataAccess.spExecLockRequest("sp_GetAppointmentLock", BRID, APID, Transaction) Then
                EditedAppointmentList.Add(APID)
                Return True
            Else
                Return False
            End If
        Else
            Return True
        End If
    End Function

    Private Function IsInEditList(ByVal APID As Long) As Boolean
        For Each lng As Long In EditedAppointmentList
            If lng = APID Then
                Return True
            End If
        Next
        Return False
    End Function

    Private Sub ReleaseAppointmentLocks()
        For Each lng As Long In EditedAppointmentList
            DataAccess.spExecLockRequest("sp_ReleaseAppointmentLock", BRID, lng, Transaction)
        Next
    End Sub

#End Region

#Region " Notes "

    Private Sub gvNotes_InitNewRow(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gvNotes.InitNewRow
        gvNotes.GetDataRow(e.RowHandle)("BRID") = BRID
        gvNotes.GetDataRow(e.RowHandle)("ID") = DataRow("ID")
    End Sub

    Private Sub gvNotes_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gvNotes.KeyDown
        If e.KeyCode = Keys.Delete Then
            If gvNotes.SelectedRowsCount > 0 Then
                gvNotes.GetDataRow(gvNotes.GetSelectedRows(0)).Delete()
            End If
        End If
    End Sub

    Private Sub gvNotes_InvalidRowException(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gvNotes.InvalidRowException
        GridView_InvalidRowException(sender, e)
    End Sub

#End Region

    Private Sub Decimal_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) _
    Handles txtJBPriceQuoted.ParseEditValue, txtAmountGranite.ParseEditValue
        Format.Decimal_ParseEditValue(sender, e)
    End Sub

    Private Sub Date_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) _
    Handles txtINDate.Validating
        Library.DateEdit_Validating(sender, e)
    End Sub

    Private Sub rgJBJobAddressAsAbove_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgJBJobAddressAsAbove.SelectedIndexChanged
        EnableDisable()
    End Sub

    Private Sub btnHelp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHelp.Click
        If HelpTopic Is Nothing Then
            ShowHelpTopic(Me, "JobBookingsTerms.html")
        Else
            ShowHelpTopic(Me, "JobBookingsTerms.html#" & HelpTopic)
        End If
    End Sub

    Private Sub txtJBClientFirstName_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtJBClientFirstName.EditValueChanged, txtJBClientSurname.EditValueChanged, txtJBClientFirstName.Validated, txtJBClientSurname.Validated
        If Not DataRow Is Nothing Then
            Me.Text = "Job Booking " & DataRow("ID") & " - " & FormatName(DataRow("JBClientFirstName"), DataRow("JBClientSurname"))
        Else
            Me.Text = "Job Booking"
        End If
    End Sub

    Private Sub miClearHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miClearHistory.Click
        ClearHistory(sender, e)
    End Sub

End Class

