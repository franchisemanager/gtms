Imports Power.Forms

Public Class lvBranches
    Inherits System.Windows.Forms.UserControl
    Implements IListControl

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Connection As SqlClient.SqlConnection, ByVal ExplorerForm As frmExplorerHeadOffice)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.Connection = Connection
        hExplorerForm = ExplorerForm
        FillDataSet()
    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents hSqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents cmListView As System.Windows.Forms.ContextMenu
    Friend WithEvents miUncancelJob As System.Windows.Forms.MenuItem
    Friend WithEvents DsHeadOffice As WindowsApplication.dsHeadOffice
    Friend WithEvents colBRAddress As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBRBusinessName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim StyleFormatCondition1 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition
        Me.hSqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.cmListView = New System.Windows.Forms.ContextMenu
        Me.miUncancelJob = New System.Windows.Forms.MenuItem
        Me.DsHeadOffice = New WindowsApplication.dsHeadOffice
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colBRBusinessName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colBRAddress = New DevExpress.XtraGrid.Columns.GridColumn
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsHeadOffice, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'hSqlConnection
        '
        Me.hSqlConnection.ConnectionString = "packet size=4096;integrated security=SSPI;data source=SERVER;persist security inf" & _
        "o=False;initial catalog=GTMS_DEV"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VBranches", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("BRShortName", "BRShortName"), New System.Data.Common.DataColumnMapping("BRBusinessName", "BRBusinessName")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Branches WHERE (BRID = @Original_BRID) AND (BRBusinessName = @Origina" & _
        "l_BRBusinessName OR @Original_BRBusinessName IS NULL AND BRBusinessName IS NULL)" & _
        " AND (BRShortName = @Original_BRShortName OR @Original_BRShortName IS NULL AND B" & _
        "RShortName IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.hSqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRBusinessName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRBusinessName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRShortName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRShortName", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Branches (BRShortName, BRBusinessName) VALUES (@BRShortName, @BRBusin" & _
        "essName); SELECT BRID, BRShortName, BRAddress, BRBusinessName FROM VBranches WHE" & _
        "RE (BRID = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.hSqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRShortName", System.Data.SqlDbType.VarChar, 50, "BRShortName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRBusinessName", System.Data.SqlDbType.VarChar, 100, "BRBusinessName"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, BRShortName, BRAddress, BRBusinessName FROM VBranches"
        Me.SqlSelectCommand1.Connection = Me.hSqlConnection
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Branches SET BRShortName = @BRShortName, BRBusinessName = @BRBusinessName " & _
        "WHERE (BRID = @Original_BRID) AND (BRBusinessName = @Original_BRBusinessName OR " & _
        "@Original_BRBusinessName IS NULL AND BRBusinessName IS NULL) AND (BRShortName = " & _
        "@Original_BRShortName OR @Original_BRShortName IS NULL AND BRShortName IS NULL);" & _
        " SELECT BRID, BRShortName, BRAddress, BRBusinessName FROM VBranches WHERE (BRID " & _
        "= @BRID)"
        Me.SqlUpdateCommand1.Connection = Me.hSqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRShortName", System.Data.SqlDbType.VarChar, 50, "BRShortName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRBusinessName", System.Data.SqlDbType.VarChar, 100, "BRBusinessName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRBusinessName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRBusinessName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRShortName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRShortName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'GridControl1
        '
        Me.GridControl1.ContextMenu = Me.cmListView
        Me.GridControl1.DataMember = "VBranches"
        Me.GridControl1.DataSource = Me.DsHeadOffice
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'GridControl1.EmbeddedNavigator
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(632, 392)
        Me.GridControl1.Styles.AddReplace("CardBorder", New DevExpress.Utils.ViewStyleEx("CardBorder", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.InactiveBorder, System.Drawing.SystemColors.WindowFrame, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("BandPanelBackground", New DevExpress.Utils.ViewStyleEx("BandPanelBackground", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ControlDark, System.Drawing.Color.DarkSalmon, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("EmptySpace", New DevExpress.Utils.ViewStyleEx("EmptySpace", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("FieldValue", New DevExpress.Utils.ViewStyleEx("FieldValue", "CardView", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("BandPanel", New DevExpress.Utils.ViewStyleEx("BandPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("CardButton", New DevExpress.Utils.ViewStyleEx("CardButton", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("FocusedCardCaption", New DevExpress.Utils.ViewStyleEx("FocusedCardCaption", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ActiveCaption, System.Drawing.SystemColors.ActiveCaptionText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("CardCaption", New DevExpress.Utils.ViewStyleEx("CardCaption", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.InactiveCaption, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("HeaderPanelBackground", New DevExpress.Utils.ViewStyleEx("HeaderPanelBackground", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ControlDark, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("SeparatorLine", New DevExpress.Utils.ViewStyleEx("SeparatorLine", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ActiveBorder, System.Drawing.SystemColors.ActiveBorder, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("FieldCaption", New DevExpress.Utils.ViewStyleEx("FieldCaption", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Top, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'cmListView
        '
        Me.cmListView.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.miUncancelJob})
        '
        'miUncancelJob
        '
        Me.miUncancelJob.Index = 0
        Me.miUncancelJob.Text = "Uncancel Job"
        '
        'DsHeadOffice
        '
        Me.DsHeadOffice.DataSetName = "dsHeadOffice"
        Me.DsHeadOffice.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'GridView1
        '
        Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
        Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
        Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colBRBusinessName, Me.colBRAddress})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        StyleFormatCondition1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Strikeout)
        StyleFormatCondition1.Appearance.ForeColor = System.Drawing.SystemColors.GrayText
        StyleFormatCondition1.Appearance.Options.UseFont = True
        StyleFormatCondition1.Appearance.Options.UseForeColor = True
        StyleFormatCondition1.ApplyToRow = True
        StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal
        StyleFormatCondition1.Value1 = "Cancelled"
        Me.GridView1.FormatConditions.AddRange(New DevExpress.XtraGrid.StyleFormatCondition() {StyleFormatCondition1})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsNavigation.AutoFocusNewRow = True
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.OptionsView.ShowHorzLines = False
        Me.GridView1.OptionsView.ShowIndicator = False
        Me.GridView1.OptionsView.ShowVertLines = False
        Me.GridView1.RowHeight = 8
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colBRBusinessName, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colBRBusinessName
        '
        Me.colBRBusinessName.Caption = "Branch Name"
        Me.colBRBusinessName.FieldName = "BRShortName"
        Me.colBRBusinessName.Name = "colBRBusinessName"
        Me.colBRBusinessName.Visible = True
        Me.colBRBusinessName.VisibleIndex = 0
        Me.colBRBusinessName.Width = 340
        '
        'colBRAddress
        '
        Me.colBRAddress.Caption = "Address"
        Me.colBRAddress.FieldName = "BRAddress"
        Me.colBRAddress.Name = "colBRAddress"
        Me.colBRAddress.Visible = True
        Me.colBRAddress.VisibleIndex = 1
        Me.colBRAddress.Width = 767
        '
        'lvBranches
        '
        Me.Controls.Add(Me.GridControl1)
        Me.Name = "lvBranches"
        Me.Size = New System.Drawing.Size(632, 392)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsHeadOffice, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return hSqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            hSqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Public Function FillDataSet() As Integer Implements IListControl.FillDataSet
        ' Open connection
        Dim trans As SqlClient.SqlTransaction = Connection.BeginTransaction(IsolationLevel.ReadUncommitted)
        Power.Library.Library.ApplyTransactionToAllDataAdapters(trans, Me)
        'Me.GridControl1.DataSource.Clear()
        FillDataSet = SqlDataAdapter.Fill(DsHeadOffice)
        'Close connecton
        trans.Commit()
    End Function

    Private Sub GridView1_RowCountChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.RowCountChanged
        ExplorerForm.UpdateNumItems(NumItems)
        ExplorerForm.UpdateVisibleItems(VisibleItems)
    End Sub

    Public ReadOnly Property VisibleItems() As Integer Implements Power.Library.IListControl.VisibleItems
        Get
            Return GridView1.RowCount
        End Get
    End Property

    Public ReadOnly Property NumItems() As Integer Implements Power.Library.IListControl.NumItems
        Get
            Return DsHeadOffice.Tables(SqlDataAdapter.TableMappings(0).DataSetTable).Rows.Count
        End Get
    End Property

    Public ReadOnly Property SelectedRow() As DataRow
        Get
            If Not GridView1.GetSelectedRows Is Nothing Then
                Return GridView1.GetDataRow(GridView1.GetSelectedRows(0))
            End If
        End Get
    End Property

    Public ReadOnly Property SelectedRowField(ByVal Field As String) As Object
        Get
            If Not SelectedRow Is Nothing Then
                Return SelectedRow.Item(Field)
            Else
                Return DBNull.Value
            End If
        End Get
    End Property

    Public Function SelectRow(ByVal BRID As Int32) As Boolean
        Dim i As Integer = 0
        Do Until False
            Try
                If GridView1.GetDataRow(GridView1.GetVisibleRowHandle(i))("BRID") = BRID Then
                    GridView1.ClearSelection()
                    GridView1.SelectRow(GridView1.GetVisibleRowHandle(i))
                    Return True
                End If
                i += 1
            Catch ex As NullReferenceException
                Return False
            End Try
        Loop
        Return False
    End Function

#Region " New, Open and Delete "

    Public Event ShowBranch(ByVal sender As Object, ByVal e As System.EventArgs, ByVal BRID As Integer)
    Private Sub GridControl1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridControl1.DoubleClick
        RaiseEvent ShowBranch(sender, e, SelectedRow.Item("BRID"))
    End Sub

    Public Sub List_New() Implements IListControl.List_New
        Dim c As Cursor = ParentForm.Cursor
        ParentForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            Dim gui As frmBranch = frmBranch.Add(Roles)
            gui.ShowDialog(Me)
            FillDataSet()
            SelectRow(gui.BRID)
        Catch ex As ObjectLockedException
            Message.CurrentlyAccessed("branch")
        End Try
        ParentForm.Cursor = c
    End Sub

    Public Sub List_Edit() Implements IListControl.List_Edit
        If Not SelectedRow Is Nothing Then
            Dim c As Cursor = ParentForm.Cursor
            ParentForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If BranchExists(SelectedRowField("BRID")) Then
                Try
                    Dim gui As frmBranch = frmBranch.Edit(SelectedRowField("BRID"), Roles)
                    gui.ShowDialog(Me)
                    FillDataSet()
                Catch ex As ObjectLockedException
                    Message.CurrentlyAccessed("branch")
                End Try
            Else
                Message.AlreadyDeleted("branch", Message.ObjectAction.Edit)
                Me.GridControl1.DataSource.Clear()
                FillDataSet()
            End If
            ParentForm.Cursor = c
        End If
    End Sub

    Public Sub List_Delete() Implements IListControl.List_Delete
        Exit Sub
        If Not SelectedRow Is Nothing Then
            If DevExpress.XtraEditors.XtraMessageBox.Show("This command will delete all business setup and company data for this branch.  " & _
                    "This command cannot be undone." & vbNewLine & vbNewLine & _
                    "Are you sure you want to delete this branch?", "Franchise Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) = MsgBoxResult.Yes Then
                If BranchExists(SelectedRowField("BRID")) Then
                    Dim BRID As Integer = SelectedRowField("BRID")
                    If DataAccess.spExecLockRequest_All("sp_GetBranchLock", BRID, Connection) Then
                        SelectedRow.Delete()
                        Try
                            SqlDataAdapter.Update(DsHeadOffice)
                        Catch ex As Exception
                            Me.GridControl1.DataSource.Clear()
                            FillDataSet()
                        End Try
                        DataAccess.spExecLockRequest_All("sp_ReleaseBranchLock", BRID, Connection)
                    Else
                        Message.CurrentlyAccessed("branch")
                    End If
                Else
                    Message.AlreadyDeleted("branch", Message.ObjectAction.Cancel)
                    Me.GridControl1.DataSource.Clear()
                    FillDataSet()
                End If
            End If
        End If
    End Sub

    Public Sub List_Undelete() Implements Power.Library.IListControl.List_Undelete
        Exit Sub
    End Sub

    Private Function BranchExists(ByVal BRID As Int32) As Boolean
        Dim cnn As SqlClient.SqlConnection = Connection
        Dim cmd As New SqlClient.SqlCommand("SELECT Count(BRID) FROM Branches WHERE BRID = @BRID", cnn)
        cmd.CommandType = CommandType.Text
        cmd.Parameters.Add("@BRID", BRID)
        Return cmd.ExecuteScalar > 0
    End Function

#End Region

    Private hExplorerForm As Form
    Public ReadOnly Property ExplorerForm() As frmExplorerHeadOffice
        Get
            Return hExplorerForm
        End Get
    End Property

    Public ReadOnly Property AllowDelete() As Boolean Implements Power.Library.IListControl.AllowDelete
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property AllowEdit() As Boolean Implements Power.Library.IListControl.AllowEdit
        Get
            Return True
        End Get
    End Property

    Public ReadOnly Property AllowNew() As Boolean Implements Power.Library.IListControl.AllowNew
        Get
            Return True
        End Get
    End Property

    Public ReadOnly Property AllowUndelete() As Boolean Implements Power.Library.IListControl.AllowUndelete
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property DeleteCaption() As String Implements Power.Library.IListControl.DeleteCaption
        Get
            Return "&Delete"
        End Get
    End Property

    Public ReadOnly Property UndeleteCaption() As String Implements Power.Library.IListControl.UndeleteCaption
        Get
            Return Nothing
        End Get
    End Property

End Class
