Public Class frmExpenseWizard
    Inherits DevExpress.XtraEditors.XtraForm

    Private DataRow As DataRow
    Private ExpenseAllowanceDataRow As DataRow

    Public BRID As Int32
    Public EXID As Int32
    Public EGType As String

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return SqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            SqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private hTransaction As SqlClient.SqlTransaction
    Private Property Transaction() As SqlClient.SqlTransaction
        Get
            Return hTransaction
        End Get
        Set(ByVal Value As SqlClient.SqlTransaction)
            hTransaction = Value
            Power.Library.Library.ApplyTransactionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Public Shared Function Add(ByVal BRID As Int32, ByVal EGID As Int32) As frmExpenseWizard
        Dim gui As New frmExpenseWizard

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            .BRID = BRID

            .Transaction = .Connection.BeginTransaction(IsolationLevel.ReadUncommitted)
            .FillPreliminaryData()
            .EXID = spNew_Expense(BRID, EGID, .Transaction)
            .EGType = DataAccess.EGType(.BRID, .EXID, .Transaction)
            .FillEGTypeDependantData()

            If DataAccess.spExecLockRequest("sp_GetExpenseLock", .BRID, .EXID, .Transaction) Then

                .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = .BRID
                .SqlDataAdapter.SelectCommand.Parameters("@EXID").Value = .EXID
                .SqlDataAdapter.Fill(.DsGTMS)
                .DataRow = .DsGTMS.Expenses(0)

                .FillData()

            Else
                Throw New ObjectLockedException
            End If
        End With

        Return gui
    End Function

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage4 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage5 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage6 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage7 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage8 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtEXName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents DsGTMS As WindowsApplication.dsGTMS
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daEXTypes As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daEXAssignToPortions As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daEXPaymentMethods As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents PanelControl5 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtEXType As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents PanelControl7 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents PanelControl8 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents PanelControl9 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents PanelControl10 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents PanelControl11 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl12 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtEXPaymentMethod As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtEXAssignToPortion As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents chkEXAppearInCalendar As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents btnNext As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnBack As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents HorizonalRuleLine3D1 As Power.Forms.HorizonalRuleLine3D
    Friend WithEvents HorizonalRuleLine3D2 As Power.Forms.HorizonalRuleLine3D
    Friend WithEvents HorizonalRuleLine3D3 As Power.Forms.HorizonalRuleLine3D
    Friend WithEvents HorizonalRuleLine3D4 As Power.Forms.HorizonalRuleLine3D
    Friend WithEvents HorizonalRuleLine3D5 As Power.Forms.HorizonalRuleLine3D
    Friend WithEvents HorizonalRuleLine3D6 As Power.Forms.HorizonalRuleLine3D
    Friend WithEvents HorizonalRuleLine3D7 As Power.Forms.HorizonalRuleLine3D
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txtALAllowance As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtALFromDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daEXPaymentMethod_Rules As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DsWizards As WindowsApplication.dsWizards
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents lblIntroTitle As System.Windows.Forms.Label
    Friend WithEvents lblIntroDesc As System.Windows.Forms.Label
    Friend WithEvents lblFinishDesc1 As System.Windows.Forms.Label
    Friend WithEvents lblFinishDesc2 As System.Windows.Forms.Label
    Friend WithEvents HorizonalRuleLine3D8 As Power.Forms.HorizonalRuleLine3D
    Friend WithEvents HorizonalRuleLine3D9 As Power.Forms.HorizonalRuleLine3D
    Friend WithEvents HorizonalRuleLine3D10 As Power.Forms.HorizonalRuleLine3D
    Friend WithEvents HorizonalRuleLine3D11 As Power.Forms.HorizonalRuleLine3D
    Friend WithEvents HorizonalRuleLine3D12 As Power.Forms.HorizonalRuleLine3D
    Friend WithEvents HorizonalRuleLine3D13 As Power.Forms.HorizonalRuleLine3D
    Friend WithEvents HorizonalRuleLine3D14 As Power.Forms.HorizonalRuleLine3D
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl
        Me.lblIntroTitle = New System.Windows.Forms.Label
        Me.lblIntroDesc = New System.Windows.Forms.Label
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage
        Me.HorizonalRuleLine3D9 = New Power.Forms.HorizonalRuleLine3D
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtEXName = New DevExpress.XtraEditors.TextEdit
        Me.DsGTMS = New WindowsApplication.dsGTMS
        Me.Label4 = New System.Windows.Forms.Label
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage
        Me.HorizonalRuleLine3D10 = New Power.Forms.HorizonalRuleLine3D
        Me.PanelControl5 = New DevExpress.XtraEditors.PanelControl
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtEXType = New DevExpress.XtraEditors.LookUpEdit
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.XtraTabPage4 = New DevExpress.XtraTab.XtraTabPage
        Me.HorizonalRuleLine3D11 = New Power.Forms.HorizonalRuleLine3D
        Me.txtEXPaymentMethod = New DevExpress.XtraEditors.LookUpEdit
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.PanelControl7 = New DevExpress.XtraEditors.PanelControl
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.XtraTabPage6 = New DevExpress.XtraTab.XtraTabPage
        Me.HorizonalRuleLine3D12 = New Power.Forms.HorizonalRuleLine3D
        Me.Label28 = New System.Windows.Forms.Label
        Me.txtALAllowance = New DevExpress.XtraEditors.TextEdit
        Me.DsWizards = New WindowsApplication.dsWizards
        Me.Label27 = New System.Windows.Forms.Label
        Me.txtALFromDate = New DevExpress.XtraEditors.DateEdit
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.PanelControl9 = New DevExpress.XtraEditors.PanelControl
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.XtraTabPage5 = New DevExpress.XtraTab.XtraTabPage
        Me.HorizonalRuleLine3D13 = New Power.Forms.HorizonalRuleLine3D
        Me.Label30 = New System.Windows.Forms.Label
        Me.txtEXAssignToPortion = New DevExpress.XtraEditors.LookUpEdit
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.PanelControl8 = New DevExpress.XtraEditors.PanelControl
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.XtraTabPage7 = New DevExpress.XtraTab.XtraTabPage
        Me.HorizonalRuleLine3D14 = New Power.Forms.HorizonalRuleLine3D
        Me.Label29 = New System.Windows.Forms.Label
        Me.chkEXAppearInCalendar = New DevExpress.XtraEditors.CheckEdit
        Me.PanelControl10 = New DevExpress.XtraEditors.PanelControl
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.XtraTabPage8 = New DevExpress.XtraTab.XtraTabPage
        Me.PanelControl11 = New DevExpress.XtraEditors.PanelControl
        Me.lblFinishDesc2 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.lblFinishDesc1 = New System.Windows.Forms.Label
        Me.PanelControl12 = New DevExpress.XtraEditors.PanelControl
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.btnNext = New DevExpress.XtraEditors.SimpleButton
        Me.btnBack = New DevExpress.XtraEditors.SimpleButton
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.daEXTypes = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.daEXAssignToPortions = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.daEXPaymentMethods = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand5 = New System.Data.SqlClient.SqlCommand
        Me.daEXPaymentMethod_Rules = New System.Data.SqlClient.SqlDataAdapter
        Me.HorizonalRuleLine3D8 = New Power.Forms.HorizonalRuleLine3D
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.txtEXName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage3.SuspendLayout()
        CType(Me.PanelControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl5.SuspendLayout()
        CType(Me.txtEXType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage4.SuspendLayout()
        CType(Me.txtEXPaymentMethod.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl7.SuspendLayout()
        Me.XtraTabPage6.SuspendLayout()
        CType(Me.txtALAllowance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsWizards, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtALFromDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl9.SuspendLayout()
        Me.XtraTabPage5.SuspendLayout()
        CType(Me.txtEXAssignToPortion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl8.SuspendLayout()
        Me.XtraTabPage7.SuspendLayout()
        CType(Me.chkEXAppearInCalendar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl10.SuspendLayout()
        Me.XtraTabPage8.SuspendLayout()
        CType(Me.PanelControl11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl11.SuspendLayout()
        CType(Me.PanelControl12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.PaintStyleName = "Flat"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(490, 352)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.XtraTabPage3, Me.XtraTabPage4, Me.XtraTabPage6, Me.XtraTabPage5, Me.XtraTabPage7, Me.XtraTabPage8})
        Me.XtraTabControl1.TabStop = False
        Me.XtraTabControl1.Text = "XtraTabControl1"
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.PanelControl2)
        Me.XtraTabPage1.Controls.Add(Me.PanelControl1)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(490, 330)
        Me.XtraTabPage1.Text = "Intro"
        '
        'PanelControl2
        '
        Me.PanelControl2.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.PanelControl2.Appearance.Options.UseBackColor = True
        Me.PanelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl2.Controls.Add(Me.lblIntroTitle)
        Me.PanelControl2.Controls.Add(Me.lblIntroDesc)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl2.Location = New System.Drawing.Point(120, 0)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(370, 330)
        Me.PanelControl2.TabIndex = 1
        Me.PanelControl2.Text = "PanelControl2"
        '
        'lblIntroTitle
        '
        Me.lblIntroTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblIntroTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblIntroTitle.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIntroTitle.Location = New System.Drawing.Point(16, 24)
        Me.lblIntroTitle.Name = "lblIntroTitle"
        Me.lblIntroTitle.Size = New System.Drawing.Size(338, 48)
        Me.lblIntroTitle.TabIndex = 0
        Me.lblIntroTitle.Text = "Welcome to the Add New Expense Wizard"
        '
        'lblIntroDesc
        '
        Me.lblIntroDesc.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblIntroDesc.BackColor = System.Drawing.Color.Transparent
        Me.lblIntroDesc.Location = New System.Drawing.Point(16, 80)
        Me.lblIntroDesc.Name = "lblIntroDesc"
        Me.lblIntroDesc.Size = New System.Drawing.Size(338, 56)
        Me.lblIntroDesc.TabIndex = 1
        Me.lblIntroDesc.Text = "This wizard guides you through adding a new expense to your business setup."
        '
        'PanelControl1
        '
        Me.PanelControl1.Appearance.BackColor = System.Drawing.Color.MidnightBlue
        Me.PanelControl1.Appearance.Options.UseBackColor = True
        Me.PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(120, 330)
        Me.PanelControl1.TabIndex = 0
        Me.PanelControl1.Text = "PanelControl1"
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.HorizonalRuleLine3D9)
        Me.XtraTabPage2.Controls.Add(Me.PanelControl3)
        Me.XtraTabPage2.Controls.Add(Me.txtEXName)
        Me.XtraTabPage2.Controls.Add(Me.Label4)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(490, 330)
        Me.XtraTabPage2.Text = "Name"
        '
        'HorizonalRuleLine3D9
        '
        Me.HorizonalRuleLine3D9.BackColor = System.Drawing.SystemColors.Control
        Me.HorizonalRuleLine3D9.Dock = System.Windows.Forms.DockStyle.Top
        Me.HorizonalRuleLine3D9.Location = New System.Drawing.Point(0, 56)
        Me.HorizonalRuleLine3D9.Name = "HorizonalRuleLine3D9"
        Me.HorizonalRuleLine3D9.Size = New System.Drawing.Size(490, 2)
        Me.HorizonalRuleLine3D9.TabIndex = 1
        '
        'PanelControl3
        '
        Me.PanelControl3.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.PanelControl3.Appearance.Options.UseBackColor = True
        Me.PanelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl3.Controls.Add(Me.Label3)
        Me.PanelControl3.Controls.Add(Me.Label2)
        Me.PanelControl3.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl3.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(490, 56)
        Me.PanelControl3.TabIndex = 0
        Me.PanelControl3.Text = "PanelControl3"
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Location = New System.Drawing.Point(24, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(450, 16)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Enter the name of the expense or person."
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(112, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Name"
        '
        'txtEXName
        '
        Me.txtEXName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Expenses.EXName"))
        Me.txtEXName.EditValue = ""
        Me.txtEXName.Location = New System.Drawing.Point(120, 160)
        Me.txtEXName.Name = "txtEXName"
        Me.txtEXName.Size = New System.Drawing.Size(288, 20)
        Me.txtEXName.TabIndex = 3
        '
        'DsGTMS
        '
        Me.DsGTMS.DataSetName = "dsGTMS"
        Me.DsGTMS.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(72, 160)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 21)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Name:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.HorizonalRuleLine3D10)
        Me.XtraTabPage3.Controls.Add(Me.PanelControl5)
        Me.XtraTabPage3.Controls.Add(Me.txtEXType)
        Me.XtraTabPage3.Controls.Add(Me.Label7)
        Me.XtraTabPage3.Controls.Add(Me.Label8)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(490, 330)
        Me.XtraTabPage3.Text = "Expense Type"
        '
        'HorizonalRuleLine3D10
        '
        Me.HorizonalRuleLine3D10.BackColor = System.Drawing.SystemColors.Control
        Me.HorizonalRuleLine3D10.Dock = System.Windows.Forms.DockStyle.Top
        Me.HorizonalRuleLine3D10.Location = New System.Drawing.Point(0, 56)
        Me.HorizonalRuleLine3D10.Name = "HorizonalRuleLine3D10"
        Me.HorizonalRuleLine3D10.Size = New System.Drawing.Size(490, 2)
        Me.HorizonalRuleLine3D10.TabIndex = 1
        '
        'PanelControl5
        '
        Me.PanelControl5.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.PanelControl5.Appearance.Options.UseBackColor = True
        Me.PanelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl5.Controls.Add(Me.Label5)
        Me.PanelControl5.Controls.Add(Me.Label6)
        Me.PanelControl5.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl5.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl5.Name = "PanelControl5"
        Me.PanelControl5.Size = New System.Drawing.Size(490, 56)
        Me.PanelControl5.TabIndex = 0
        Me.PanelControl5.Text = "PanelControl5"
        '
        'Label5
        '
        Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Location = New System.Drawing.Point(24, 32)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(450, 16)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Select the expense type."
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(8, 8)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(112, 16)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Expense Type"
        '
        'txtEXType
        '
        Me.txtEXType.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Expenses.EXType"))
        Me.txtEXType.Location = New System.Drawing.Point(176, 176)
        Me.txtEXType.Name = "txtEXType"
        '
        'txtEXType.Properties
        '
        Me.txtEXType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtEXType.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXTypeDisplay")})
        Me.txtEXType.Properties.DataSource = Me.DsGTMS.EXTypes
        Me.txtEXType.Properties.DisplayMember = "EXTypeDisplay"
        Me.txtEXType.Properties.NullText = ""
        Me.txtEXType.Properties.ShowFooter = False
        Me.txtEXType.Properties.ShowHeader = False
        Me.txtEXType.Properties.ValueMember = "EXType"
        Me.txtEXType.Size = New System.Drawing.Size(224, 20)
        Me.txtEXType.TabIndex = 4
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(80, 136)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(320, 32)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "Expense type defines the way item or person will be classified in calculating reg" & _
        "arding indirect tax and labor on-costs."
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(80, 176)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(96, 21)
        Me.Label8.TabIndex = 3
        Me.Label8.Text = "Expense type:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'XtraTabPage4
        '
        Me.XtraTabPage4.Controls.Add(Me.HorizonalRuleLine3D11)
        Me.XtraTabPage4.Controls.Add(Me.txtEXPaymentMethod)
        Me.XtraTabPage4.Controls.Add(Me.Label20)
        Me.XtraTabPage4.Controls.Add(Me.Label21)
        Me.XtraTabPage4.Controls.Add(Me.PanelControl7)
        Me.XtraTabPage4.Name = "XtraTabPage4"
        Me.XtraTabPage4.Size = New System.Drawing.Size(490, 330)
        Me.XtraTabPage4.Text = "Payment Method"
        '
        'HorizonalRuleLine3D11
        '
        Me.HorizonalRuleLine3D11.BackColor = System.Drawing.SystemColors.Control
        Me.HorizonalRuleLine3D11.Dock = System.Windows.Forms.DockStyle.Top
        Me.HorizonalRuleLine3D11.Location = New System.Drawing.Point(0, 56)
        Me.HorizonalRuleLine3D11.Name = "HorizonalRuleLine3D11"
        Me.HorizonalRuleLine3D11.Size = New System.Drawing.Size(490, 2)
        Me.HorizonalRuleLine3D11.TabIndex = 1
        '
        'txtEXPaymentMethod
        '
        Me.txtEXPaymentMethod.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Expenses.EXPaymentMethod"))
        Me.txtEXPaymentMethod.Location = New System.Drawing.Point(176, 176)
        Me.txtEXPaymentMethod.Name = "txtEXPaymentMethod"
        '
        'txtEXPaymentMethod.Properties
        '
        Me.txtEXPaymentMethod.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtEXPaymentMethod.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXPaymentMethodDisplay", "", 100, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None)})
        Me.txtEXPaymentMethod.Properties.DataSource = Me.DsGTMS.EXPaymentMethods
        Me.txtEXPaymentMethod.Properties.DisplayMember = "EXPaymentMethodDisplay"
        Me.txtEXPaymentMethod.Properties.NullText = ""
        Me.txtEXPaymentMethod.Properties.ShowFooter = False
        Me.txtEXPaymentMethod.Properties.ShowHeader = False
        Me.txtEXPaymentMethod.Properties.ValueMember = "EXPaymentMethod"
        Me.txtEXPaymentMethod.Size = New System.Drawing.Size(224, 20)
        Me.txtEXPaymentMethod.TabIndex = 4
        '
        'Label20
        '
        Me.Label20.Location = New System.Drawing.Point(80, 176)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(96, 21)
        Me.Label20.TabIndex = 3
        Me.Label20.Text = "Payment method:"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label21
        '
        Me.Label21.Location = New System.Drawing.Point(80, 136)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(320, 32)
        Me.Label21.TabIndex = 2
        Me.Label21.Text = "The payment method specifies how this item or person will be paid.  Select a paym" & _
        "ent method from the list."
        '
        'PanelControl7
        '
        Me.PanelControl7.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.PanelControl7.Appearance.Options.UseBackColor = True
        Me.PanelControl7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl7.Controls.Add(Me.Label9)
        Me.PanelControl7.Controls.Add(Me.Label10)
        Me.PanelControl7.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl7.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl7.Name = "PanelControl7"
        Me.PanelControl7.Size = New System.Drawing.Size(490, 56)
        Me.PanelControl7.TabIndex = 0
        Me.PanelControl7.Text = "PanelControl7"
        '
        'Label9
        '
        Me.Label9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Location = New System.Drawing.Point(24, 32)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(450, 16)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "Select the payment type."
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(8, 8)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(112, 16)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Payment Type"
        '
        'XtraTabPage6
        '
        Me.XtraTabPage6.Controls.Add(Me.HorizonalRuleLine3D12)
        Me.XtraTabPage6.Controls.Add(Me.Label28)
        Me.XtraTabPage6.Controls.Add(Me.txtALAllowance)
        Me.XtraTabPage6.Controls.Add(Me.Label27)
        Me.XtraTabPage6.Controls.Add(Me.txtALFromDate)
        Me.XtraTabPage6.Controls.Add(Me.Label26)
        Me.XtraTabPage6.Controls.Add(Me.Label25)
        Me.XtraTabPage6.Controls.Add(Me.Label24)
        Me.XtraTabPage6.Controls.Add(Me.PanelControl9)
        Me.XtraTabPage6.Name = "XtraTabPage6"
        Me.XtraTabPage6.Size = New System.Drawing.Size(490, 330)
        Me.XtraTabPage6.Text = "Annual Amount"
        '
        'HorizonalRuleLine3D12
        '
        Me.HorizonalRuleLine3D12.BackColor = System.Drawing.SystemColors.Control
        Me.HorizonalRuleLine3D12.Dock = System.Windows.Forms.DockStyle.Top
        Me.HorizonalRuleLine3D12.Location = New System.Drawing.Point(0, 56)
        Me.HorizonalRuleLine3D12.Name = "HorizonalRuleLine3D12"
        Me.HorizonalRuleLine3D12.Size = New System.Drawing.Size(490, 2)
        Me.HorizonalRuleLine3D12.TabIndex = 1
        '
        'Label28
        '
        Me.Label28.Location = New System.Drawing.Point(96, 272)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(96, 21)
        Me.Label28.TabIndex = 7
        Me.Label28.Text = "Annual amount:"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtALAllowance
        '
        Me.txtALAllowance.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsWizards, "ExpenseAllowances.ALAllowance"))
        Me.txtALAllowance.Location = New System.Drawing.Point(192, 272)
        Me.txtALAllowance.Name = "txtALAllowance"
        '
        'txtALAllowance.Properties
        '
        Me.txtALAllowance.Properties.Appearance.Options.UseTextOptions = True
        Me.txtALAllowance.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtALAllowance.Properties.DisplayFormat.FormatString = "c"
        Me.txtALAllowance.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtALAllowance.Properties.EditFormat.FormatString = "c"
        Me.txtALAllowance.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtALAllowance.Size = New System.Drawing.Size(176, 20)
        Me.txtALAllowance.TabIndex = 8
        '
        'DsWizards
        '
        Me.DsWizards.DataSetName = "dsWizards"
        Me.DsWizards.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'Label27
        '
        Me.Label27.Location = New System.Drawing.Point(96, 240)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(96, 21)
        Me.Label27.TabIndex = 5
        Me.Label27.Text = "Start date:"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtALFromDate
        '
        Me.txtALFromDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsWizards, "ExpenseAllowances.ALFromDate"))
        Me.txtALFromDate.EditValue = Nothing
        Me.txtALFromDate.Location = New System.Drawing.Point(192, 240)
        Me.txtALFromDate.Name = "txtALFromDate"
        '
        'txtALFromDate.Properties
        '
        Me.txtALFromDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtALFromDate.Size = New System.Drawing.Size(176, 20)
        Me.txtALFromDate.TabIndex = 6
        '
        'Label26
        '
        Me.Label26.Location = New System.Drawing.Point(24, 184)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(448, 32)
        Me.Label26.TabIndex = 4
        Me.Label26.Text = "If the annual amount changes, this can be altered in the Amount Specified Annuall" & _
        "y list in the accounting section of the program."
        '
        'Label25
        '
        Me.Label25.Location = New System.Drawing.Point(24, 112)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(448, 64)
        Me.Label25.TabIndex = 3
        Me.Label25.Text = "Note: The annual amount must be the amount the expense WOULD BE PAID if the expen" & _
        "se spanned the entire finiancial year.  If the expense was $40 000 over a full f" & _
        "inancial year, but the start date is half way through the financial year, the pr" & _
        "ogram will only assign $20 000 for this finacial year."
        '
        'Label24
        '
        Me.Label24.Location = New System.Drawing.Point(24, 72)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(448, 32)
        Me.Label24.TabIndex = 2
        Me.Label24.Text = "You must set up an annual amount for this expense.  Select the date the expense b" & _
        "egan and a total annual amount."
        '
        'PanelControl9
        '
        Me.PanelControl9.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.PanelControl9.Appearance.Options.UseBackColor = True
        Me.PanelControl9.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl9.Controls.Add(Me.Label13)
        Me.PanelControl9.Controls.Add(Me.Label14)
        Me.PanelControl9.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl9.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl9.Name = "PanelControl9"
        Me.PanelControl9.Size = New System.Drawing.Size(490, 56)
        Me.PanelControl9.TabIndex = 0
        Me.PanelControl9.Text = "PanelControl9"
        '
        'Label13
        '
        Me.Label13.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Location = New System.Drawing.Point(24, 32)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(450, 16)
        Me.Label13.TabIndex = 1
        Me.Label13.Text = "Set up the annual amount for this expense."
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(8, 8)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(112, 16)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Annual Amount"
        '
        'XtraTabPage5
        '
        Me.XtraTabPage5.Controls.Add(Me.HorizonalRuleLine3D13)
        Me.XtraTabPage5.Controls.Add(Me.Label30)
        Me.XtraTabPage5.Controls.Add(Me.txtEXAssignToPortion)
        Me.XtraTabPage5.Controls.Add(Me.Label22)
        Me.XtraTabPage5.Controls.Add(Me.Label23)
        Me.XtraTabPage5.Controls.Add(Me.PanelControl8)
        Me.XtraTabPage5.Name = "XtraTabPage5"
        Me.XtraTabPage5.Size = New System.Drawing.Size(490, 330)
        Me.XtraTabPage5.Text = "Category"
        '
        'HorizonalRuleLine3D13
        '
        Me.HorizonalRuleLine3D13.BackColor = System.Drawing.SystemColors.Control
        Me.HorizonalRuleLine3D13.Dock = System.Windows.Forms.DockStyle.Top
        Me.HorizonalRuleLine3D13.Location = New System.Drawing.Point(0, 56)
        Me.HorizonalRuleLine3D13.Name = "HorizonalRuleLine3D13"
        Me.HorizonalRuleLine3D13.Size = New System.Drawing.Size(490, 2)
        Me.HorizonalRuleLine3D13.TabIndex = 1
        '
        'Label30
        '
        Me.Label30.Location = New System.Drawing.Point(80, 168)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(320, 48)
        Me.Label30.TabIndex = 3
        Me.Label30.Text = "By selecting both, the cost of this item will be split across both in porportion " & _
        "to the Trend and Non-Trend portion of the job price."
        '
        'txtEXAssignToPortion
        '
        Me.txtEXAssignToPortion.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Expenses.EXAssignToPortion"))
        Me.txtEXAssignToPortion.Location = New System.Drawing.Point(176, 216)
        Me.txtEXAssignToPortion.Name = "txtEXAssignToPortion"
        '
        'txtEXAssignToPortion.Properties
        '
        Me.txtEXAssignToPortion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtEXAssignToPortion.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXAssignToPortionDisplay")})
        Me.txtEXAssignToPortion.Properties.DataSource = Me.DsGTMS.EXAssignToPortions
        Me.txtEXAssignToPortion.Properties.DisplayMember = "EXAssignToPortionDisplay"
        Me.txtEXAssignToPortion.Properties.NullText = ""
        Me.txtEXAssignToPortion.Properties.ShowFooter = False
        Me.txtEXAssignToPortion.Properties.ShowHeader = False
        Me.txtEXAssignToPortion.Properties.ValueMember = "EXAssignToPortion"
        Me.txtEXAssignToPortion.Size = New System.Drawing.Size(224, 20)
        Me.txtEXAssignToPortion.TabIndex = 5
        '
        'Label22
        '
        Me.Label22.Location = New System.Drawing.Point(80, 104)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(320, 56)
        Me.Label22.TabIndex = 2
        Me.Label22.Text = "You may select a category, either Trend portion, Non-Trend portion or Both, to wh" & _
        "ich this item will be assigned.  This will apportion the expense to the relevant" & _
        " portion of the costs for reporting purposes."
        '
        'Label23
        '
        Me.Label23.Location = New System.Drawing.Point(80, 216)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(96, 21)
        Me.Label23.TabIndex = 4
        Me.Label23.Text = "Category:"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'PanelControl8
        '
        Me.PanelControl8.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.PanelControl8.Appearance.Options.UseBackColor = True
        Me.PanelControl8.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl8.Controls.Add(Me.Label11)
        Me.PanelControl8.Controls.Add(Me.Label12)
        Me.PanelControl8.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl8.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl8.Name = "PanelControl8"
        Me.PanelControl8.Size = New System.Drawing.Size(490, 56)
        Me.PanelControl8.TabIndex = 0
        Me.PanelControl8.Text = "PanelControl8"
        '
        'Label11
        '
        Me.Label11.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Location = New System.Drawing.Point(24, 32)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(450, 16)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "Select the expense category for this expense."
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(8, 8)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(112, 16)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Category"
        '
        'XtraTabPage7
        '
        Me.XtraTabPage7.Controls.Add(Me.HorizonalRuleLine3D14)
        Me.XtraTabPage7.Controls.Add(Me.Label29)
        Me.XtraTabPage7.Controls.Add(Me.chkEXAppearInCalendar)
        Me.XtraTabPage7.Controls.Add(Me.PanelControl10)
        Me.XtraTabPage7.Name = "XtraTabPage7"
        Me.XtraTabPage7.Size = New System.Drawing.Size(490, 330)
        Me.XtraTabPage7.Text = "Show In Calendar"
        '
        'HorizonalRuleLine3D14
        '
        Me.HorizonalRuleLine3D14.BackColor = System.Drawing.SystemColors.Control
        Me.HorizonalRuleLine3D14.Dock = System.Windows.Forms.DockStyle.Top
        Me.HorizonalRuleLine3D14.Location = New System.Drawing.Point(0, 56)
        Me.HorizonalRuleLine3D14.Name = "HorizonalRuleLine3D14"
        Me.HorizonalRuleLine3D14.Size = New System.Drawing.Size(490, 2)
        Me.HorizonalRuleLine3D14.TabIndex = 1
        '
        'Label29
        '
        Me.Label29.Location = New System.Drawing.Point(80, 128)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(320, 56)
        Me.Label29.TabIndex = 2
        Me.Label29.Text = "This item or person may be shown in the calendar.  This will allow the user to sc" & _
        "hedule it for appointments.  The appointments which you may schedule this item f" & _
        "or will depend on the type of item."
        '
        'chkEXAppearInCalendar
        '
        Me.chkEXAppearInCalendar.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Expenses.EXAppearInCalendar"))
        Me.chkEXAppearInCalendar.Location = New System.Drawing.Point(128, 192)
        Me.chkEXAppearInCalendar.Name = "chkEXAppearInCalendar"
        '
        'chkEXAppearInCalendar.Properties
        '
        Me.chkEXAppearInCalendar.Properties.Caption = "Show this expense in the calendar"
        Me.chkEXAppearInCalendar.Size = New System.Drawing.Size(200, 18)
        Me.chkEXAppearInCalendar.TabIndex = 3
        '
        'PanelControl10
        '
        Me.PanelControl10.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.PanelControl10.Appearance.Options.UseBackColor = True
        Me.PanelControl10.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl10.Controls.Add(Me.Label15)
        Me.PanelControl10.Controls.Add(Me.Label16)
        Me.PanelControl10.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl10.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl10.Name = "PanelControl10"
        Me.PanelControl10.Size = New System.Drawing.Size(490, 56)
        Me.PanelControl10.TabIndex = 0
        Me.PanelControl10.Text = "PanelControl10"
        '
        'Label15
        '
        Me.Label15.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Location = New System.Drawing.Point(24, 32)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(450, 16)
        Me.Label15.TabIndex = 1
        Me.Label15.Text = "Is this expense shown in the calendar?"
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(8, 8)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(112, 16)
        Me.Label16.TabIndex = 0
        Me.Label16.Text = "Show In Calendar"
        '
        'XtraTabPage8
        '
        Me.XtraTabPage8.Controls.Add(Me.PanelControl11)
        Me.XtraTabPage8.Controls.Add(Me.PanelControl12)
        Me.XtraTabPage8.Name = "XtraTabPage8"
        Me.XtraTabPage8.Size = New System.Drawing.Size(490, 330)
        Me.XtraTabPage8.Text = "Finish"
        '
        'PanelControl11
        '
        Me.PanelControl11.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.PanelControl11.Appearance.Options.UseBackColor = True
        Me.PanelControl11.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl11.Controls.Add(Me.lblFinishDesc2)
        Me.PanelControl11.Controls.Add(Me.Label17)
        Me.PanelControl11.Controls.Add(Me.lblFinishDesc1)
        Me.PanelControl11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl11.Location = New System.Drawing.Point(120, 0)
        Me.PanelControl11.Name = "PanelControl11"
        Me.PanelControl11.Size = New System.Drawing.Size(370, 330)
        Me.PanelControl11.TabIndex = 1
        Me.PanelControl11.Text = "PanelControl11"
        '
        'lblFinishDesc2
        '
        Me.lblFinishDesc2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblFinishDesc2.BackColor = System.Drawing.Color.Transparent
        Me.lblFinishDesc2.Location = New System.Drawing.Point(16, 120)
        Me.lblFinishDesc2.Name = "lblFinishDesc2"
        Me.lblFinishDesc2.Size = New System.Drawing.Size(338, 40)
        Me.lblFinishDesc2.TabIndex = 2
        Me.lblFinishDesc2.Text = "Click 'Finish' to complete the wizard and add the new expense to your business se" & _
        "tup."
        '
        'Label17
        '
        Me.Label17.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(16, 24)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(338, 48)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Wizard Completed Successfully"
        '
        'lblFinishDesc1
        '
        Me.lblFinishDesc1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblFinishDesc1.BackColor = System.Drawing.Color.Transparent
        Me.lblFinishDesc1.Location = New System.Drawing.Point(16, 80)
        Me.lblFinishDesc1.Name = "lblFinishDesc1"
        Me.lblFinishDesc1.Size = New System.Drawing.Size(338, 32)
        Me.lblFinishDesc1.TabIndex = 1
        Me.lblFinishDesc1.Text = "The wizard now has enough information to add the new expense."
        '
        'PanelControl12
        '
        Me.PanelControl12.Appearance.BackColor = System.Drawing.Color.MidnightBlue
        Me.PanelControl12.Appearance.Options.UseBackColor = True
        Me.PanelControl12.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl12.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelControl12.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl12.Name = "PanelControl12"
        Me.PanelControl12.Size = New System.Drawing.Size(120, 330)
        Me.PanelControl12.TabIndex = 0
        Me.PanelControl12.Text = "PanelControl12"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(408, 360)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        '
        'btnNext
        '
        Me.btnNext.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNext.Location = New System.Drawing.Point(328, 360)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(72, 23)
        Me.btnNext.TabIndex = 3
        Me.btnNext.Text = "&Next >"
        '
        'btnBack
        '
        Me.btnBack.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBack.Enabled = False
        Me.btnBack.Location = New System.Drawing.Point(256, 360)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(72, 23)
        Me.btnBack.TabIndex = 2
        Me.btnBack.Text = "< &Back"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand2
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand2
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand2
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Expenses", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("EXName", "EXName"), New System.Data.Common.DataColumnMapping("EXType", "EXType"), New System.Data.Common.DataColumnMapping("EGID", "EGID"), New System.Data.Common.DataColumnMapping("EXDiscontinued", "EXDiscontinued"), New System.Data.Common.DataColumnMapping("EXPaymentMethod", "EXPaymentMethod"), New System.Data.Common.DataColumnMapping("EGType", "EGType"), New System.Data.Common.DataColumnMapping("EXAssignToPortion", "EXAssignToPortion"), New System.Data.Common.DataColumnMapping("EXAppearInCalendar", "EXAppearInCalendar")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM Expenses WHERE (BRID = @Original_BRID) AND (EXID = @Original_EXID) AN" & _
        "D (EGID = @Original_EGID OR @Original_EGID IS NULL AND EGID IS NULL) AND (EGType" & _
        " = @Original_EGType OR @Original_EGType IS NULL AND EGType IS NULL) AND (EXAppea" & _
        "rInCalendar = @Original_EXAppearInCalendar OR @Original_EXAppearInCalendar IS NU" & _
        "LL AND EXAppearInCalendar IS NULL) AND (EXAssignToPortion = @Original_EXAssignTo" & _
        "Portion OR @Original_EXAssignToPortion IS NULL AND EXAssignToPortion IS NULL) AN" & _
        "D (EXDiscontinued = @Original_EXDiscontinued OR @Original_EXDiscontinued IS NULL" & _
        " AND EXDiscontinued IS NULL) AND (EXName = @Original_EXName OR @Original_EXName " & _
        "IS NULL AND EXName IS NULL) AND (EXPaymentMethod = @Original_EXPaymentMethod OR " & _
        "@Original_EXPaymentMethod IS NULL AND EXPaymentMethod IS NULL) AND (EXType = @Or" & _
        "iginal_EXType OR @Original_EXType IS NULL AND EXType IS NULL)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXAppearInCalendar", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXAppearInCalendar", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXAssignToPortion", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXAssignToPortion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXDiscontinued", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXDiscontinued", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXPaymentMethod", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXType", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;integrated security=SSPI;data source=""SERVER" & _
        "\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO Expenses(BRID, EXName, EXType, EGID, EXDiscontinued, EXPaymentMethod," & _
        " EXAssignToPortion, EXAppearInCalendar) VALUES (@BRID, @EXName, @EXType, @EGID, " & _
        "@EXDiscontinued, @EXPaymentMethod, @EXAssignToPortion, @EXAppearInCalendar); SEL" & _
        "ECT BRID, EXID, EXName, EXType, EGID, EXDiscontinued, EXPaymentMethod, EGType, E" & _
        "XAssignToPortion, EXAppearInCalendar FROM Expenses WHERE (BRID = @BRID) AND (EXI" & _
        "D = @@IDENTITY)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXType", System.Data.SqlDbType.VarChar, 2, "EXType"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGID", System.Data.SqlDbType.SmallInt, 2, "EGID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXDiscontinued", System.Data.SqlDbType.Bit, 1, "EXDiscontinued"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, "EXPaymentMethod"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAssignToPortion", System.Data.SqlDbType.VarChar, 2, "EXAssignToPortion"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAppearInCalendar", System.Data.SqlDbType.Bit, 1, "EXAppearInCalendar"))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT BRID, EXID, EXName, EXType, EGID, EXDiscontinued, EXPaymentMethod, EGType," & _
        " EXAssignToPortion, EXAppearInCalendar FROM Expenses WHERE (BRID = @BRID) AND (E" & _
        "XID = @EXID)"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection
        Me.SqlSelectCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE Expenses SET BRID = @BRID, EXName = @EXName, EXType = @EXType, EGID = @EGI" & _
        "D, EXDiscontinued = @EXDiscontinued, EXPaymentMethod = @EXPaymentMethod, EXAssig" & _
        "nToPortion = @EXAssignToPortion, EXAppearInCalendar = @EXAppearInCalendar WHERE " & _
        "(BRID = @Original_BRID) AND (EXID = @Original_EXID) AND (EGID = @Original_EGID O" & _
        "R @Original_EGID IS NULL AND EGID IS NULL) AND (EXAppearInCalendar = @Original_E" & _
        "XAppearInCalendar OR @Original_EXAppearInCalendar IS NULL AND EXAppearInCalendar" & _
        " IS NULL) AND (EXAssignToPortion = @Original_EXAssignToPortion OR @Original_EXAs" & _
        "signToPortion IS NULL AND EXAssignToPortion IS NULL) AND (EXDiscontinued = @Orig" & _
        "inal_EXDiscontinued OR @Original_EXDiscontinued IS NULL AND EXDiscontinued IS NU" & _
        "LL) AND (EXName = @Original_EXName OR @Original_EXName IS NULL AND EXName IS NUL" & _
        "L) AND (EXPaymentMethod = @Original_EXPaymentMethod OR @Original_EXPaymentMethod" & _
        " IS NULL AND EXPaymentMethod IS NULL) AND (EXType = @Original_EXType OR @Origina" & _
        "l_EXType IS NULL AND EXType IS NULL); SELECT BRID, EXID, EXName, EXType, EGID, E" & _
        "XDiscontinued, EXPaymentMethod, EGType, EXAssignToPortion, EXAppearInCalendar FR" & _
        "OM Expenses WHERE (BRID = @BRID) AND (EXID = @EXID)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXType", System.Data.SqlDbType.VarChar, 2, "EXType"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGID", System.Data.SqlDbType.SmallInt, 2, "EGID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXDiscontinued", System.Data.SqlDbType.Bit, 1, "EXDiscontinued"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, "EXPaymentMethod"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAssignToPortion", System.Data.SqlDbType.VarChar, 2, "EXAssignToPortion"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAppearInCalendar", System.Data.SqlDbType.Bit, 1, "EXAppearInCalendar"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXAppearInCalendar", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXAppearInCalendar", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXAssignToPortion", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXAssignToPortion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXDiscontinued", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXDiscontinued", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXPaymentMethod", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        '
        'daEXTypes
        '
        Me.daEXTypes.DeleteCommand = Me.SqlDeleteCommand1
        Me.daEXTypes.InsertCommand = Me.SqlInsertCommand1
        Me.daEXTypes.SelectCommand = Me.SqlSelectCommand1
        Me.daEXTypes.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "EXTypes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("EXType", "EXType"), New System.Data.Common.DataColumnMapping("EXTypeDisplay", "EXTypeDisplay"), New System.Data.Common.DataColumnMapping("EXTypeDescription", "EXTypeDescription"), New System.Data.Common.DataColumnMapping("EGType", "EGType")})})
        Me.daEXTypes.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM EXTypes WHERE (EGType = @Original_EGType) AND (EXType = @Original_EXT" & _
        "ype) AND (EXTypeDescription = @Original_EXTypeDescription OR @Original_EXTypeDes" & _
        "cription IS NULL AND EXTypeDescription IS NULL) AND (EXTypeDisplay = @Original_E" & _
        "XTypeDisplay OR @Original_EXTypeDisplay IS NULL AND EXTypeDisplay IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGType", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXTypeDescription", System.Data.SqlDbType.VarChar, 200, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXTypeDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXTypeDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXTypeDisplay", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO EXTypes(EXType, EXTypeDisplay, EXTypeDescription, EGType) VALUES (@EX" & _
        "Type, @EXTypeDisplay, @EXTypeDescription, @EGType); SELECT EXType, EXTypeDisplay" & _
        ", EXTypeDescription, EGType FROM EXTypes WHERE (EGType = @EGType) AND (EXType = " & _
        "@EXType)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXType", System.Data.SqlDbType.VarChar, 2, "EXType"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXTypeDisplay", System.Data.SqlDbType.VarChar, 50, "EXTypeDisplay"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXTypeDescription", System.Data.SqlDbType.VarChar, 200, "EXTypeDescription"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 50, "EGType"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT EXType, EXTypeDisplay, EXTypeDescription, EGType FROM EXTypes WHERE (EGTyp" & _
        "e = @EGType)"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 50, "EGType"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE EXTypes SET EXType = @EXType, EXTypeDisplay = @EXTypeDisplay, EXTypeDescri" & _
        "ption = @EXTypeDescription, EGType = @EGType WHERE (EGType = @Original_EGType) A" & _
        "ND (EXType = @Original_EXType) AND (EXTypeDescription = @Original_EXTypeDescript" & _
        "ion OR @Original_EXTypeDescription IS NULL AND EXTypeDescription IS NULL) AND (E" & _
        "XTypeDisplay = @Original_EXTypeDisplay OR @Original_EXTypeDisplay IS NULL AND EX" & _
        "TypeDisplay IS NULL); SELECT EXType, EXTypeDisplay, EXTypeDescription, EGType FR" & _
        "OM EXTypes WHERE (EGType = @EGType) AND (EXType = @EXType)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXType", System.Data.SqlDbType.VarChar, 2, "EXType"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXTypeDisplay", System.Data.SqlDbType.VarChar, 50, "EXTypeDisplay"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXTypeDescription", System.Data.SqlDbType.VarChar, 200, "EXTypeDescription"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 50, "EGType"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGType", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXTypeDescription", System.Data.SqlDbType.VarChar, 200, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXTypeDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXTypeDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXTypeDisplay", System.Data.DataRowVersion.Original, Nothing))
        '
        'daEXAssignToPortions
        '
        Me.daEXAssignToPortions.DeleteCommand = Me.SqlDeleteCommand3
        Me.daEXAssignToPortions.InsertCommand = Me.SqlInsertCommand3
        Me.daEXAssignToPortions.SelectCommand = Me.SqlSelectCommand3
        Me.daEXAssignToPortions.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "EXAssignToPortions", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("EGType", "EGType"), New System.Data.Common.DataColumnMapping("EXAssignToPortion", "EXAssignToPortion"), New System.Data.Common.DataColumnMapping("EXAssignToPortionDisplay", "EXAssignToPortionDisplay")})})
        Me.daEXAssignToPortions.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM EXAssignToPortions WHERE (EGType = @Original_EGType) AND (EXAssignToP" & _
        "ortion = @Original_EXAssignToPortion) AND (EXAssignToPortionDisplay = @Original_" & _
        "EXAssignToPortionDisplay OR @Original_EXAssignToPortionDisplay IS NULL AND EXAss" & _
        "ignToPortionDisplay IS NULL)"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXAssignToPortion", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXAssignToPortion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXAssignToPortionDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXAssignToPortionDisplay", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO EXAssignToPortions(EGType, EXAssignToPortion, EXAssignToPortionDispla" & _
        "y) VALUES (@EGType, @EXAssignToPortion, @EXAssignToPortionDisplay); SELECT EGTyp" & _
        "e, EXAssignToPortion, EXAssignToPortionDisplay FROM EXAssignToPortions WHERE (EG" & _
        "Type = @EGType) AND (EXAssignToPortion = @EXAssignToPortion)"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 2, "EGType"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAssignToPortion", System.Data.SqlDbType.VarChar, 2, "EXAssignToPortion"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAssignToPortionDisplay", System.Data.SqlDbType.VarChar, 50, "EXAssignToPortionDisplay"))
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT EXAssignToPortion, EXAssignToPortionDisplay, EGType FROM EXAssignToPortion" & _
        "s WHERE (EGType = @EGType)"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection
        Me.SqlSelectCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 2, "EGType"))
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE EXAssignToPortions SET EGType = @EGType, EXAssignToPortion = @EXAssignToPo" & _
        "rtion, EXAssignToPortionDisplay = @EXAssignToPortionDisplay WHERE (EGType = @Ori" & _
        "ginal_EGType) AND (EXAssignToPortion = @Original_EXAssignToPortion) AND (EXAssig" & _
        "nToPortionDisplay = @Original_EXAssignToPortionDisplay OR @Original_EXAssignToPo" & _
        "rtionDisplay IS NULL AND EXAssignToPortionDisplay IS NULL); SELECT EGType, EXAss" & _
        "ignToPortion, EXAssignToPortionDisplay FROM EXAssignToPortions WHERE (EGType = @" & _
        "EGType) AND (EXAssignToPortion = @EXAssignToPortion)"
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 2, "EGType"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAssignToPortion", System.Data.SqlDbType.VarChar, 2, "EXAssignToPortion"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAssignToPortionDisplay", System.Data.SqlDbType.VarChar, 50, "EXAssignToPortionDisplay"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXAssignToPortion", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXAssignToPortion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXAssignToPortionDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXAssignToPortionDisplay", System.Data.DataRowVersion.Original, Nothing))
        '
        'daEXPaymentMethods
        '
        Me.daEXPaymentMethods.DeleteCommand = Me.SqlDeleteCommand4
        Me.daEXPaymentMethods.InsertCommand = Me.SqlInsertCommand4
        Me.daEXPaymentMethods.SelectCommand = Me.SqlSelectCommand4
        Me.daEXPaymentMethods.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "EXPaymentMethods", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("EGType", "EGType"), New System.Data.Common.DataColumnMapping("EXPaymentMethod", "EXPaymentMethod"), New System.Data.Common.DataColumnMapping("EXPaymentMethodDisplay", "EXPaymentMethodDisplay"), New System.Data.Common.DataColumnMapping("EXPaymentMethodDescription", "EXPaymentMethodDescription")})})
        Me.daEXPaymentMethods.UpdateCommand = Me.SqlUpdateCommand4
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = "DELETE FROM EXPaymentMethods WHERE (EGType = @Original_EGType) AND (EXPaymentMeth" & _
        "od = @Original_EXPaymentMethod) AND (EXPaymentMethodDescription = @Original_EXPa" & _
        "ymentMethodDescription OR @Original_EXPaymentMethodDescription IS NULL AND EXPay" & _
        "mentMethodDescription IS NULL) AND (EXPaymentMethodDisplay = @Original_EXPayment" & _
        "MethodDisplay OR @Original_EXPaymentMethodDisplay IS NULL AND EXPaymentMethodDis" & _
        "play IS NULL)"
        Me.SqlDeleteCommand4.Connection = Me.SqlConnection
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXPaymentMethod", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXPaymentMethodDescription", System.Data.SqlDbType.VarChar, 300, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXPaymentMethodDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXPaymentMethodDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXPaymentMethodDisplay", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = "INSERT INTO EXPaymentMethods (EGType, EXPaymentMethod, EXPaymentMethodDisplay, EX" & _
        "PaymentMethodDescription) VALUES (@EGType, @EXPaymentMethod, @EXPaymentMethodDis" & _
        "play, @EXPaymentMethodDescription); SELECT EGType, EXPaymentMethod, EXPaymentMet" & _
        "hodDisplay, EXPaymentMethodDescription FROM EXPaymentMethods WHERE (EGType = @EG" & _
        "Type) AND (EXPaymentMethod = @EXPaymentMethod)"
        Me.SqlInsertCommand4.Connection = Me.SqlConnection
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 2, "EGType"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, "EXPaymentMethod"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXPaymentMethodDisplay", System.Data.SqlDbType.VarChar, 50, "EXPaymentMethodDisplay"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXPaymentMethodDescription", System.Data.SqlDbType.VarChar, 300, "EXPaymentMethodDescription"))
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT EGType, EXPaymentMethod, EXPaymentMethodDisplay, EXPaymentMethodDescriptio" & _
        "n FROM EXPaymentMethods WHERE (EGType = @EGType) ORDER BY EXPaymentMethodSeq"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection
        Me.SqlSelectCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 2, "EGType"))
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = "UPDATE EXPaymentMethods SET EGType = @EGType, EXPaymentMethod = @EXPaymentMethod," & _
        " EXPaymentMethodDisplay = @EXPaymentMethodDisplay, EXPaymentMethodDescription = " & _
        "@EXPaymentMethodDescription WHERE (EGType = @Original_EGType) AND (EXPaymentMeth" & _
        "od = @Original_EXPaymentMethod) AND (EXPaymentMethodDescription = @Original_EXPa" & _
        "ymentMethodDescription OR @Original_EXPaymentMethodDescription IS NULL AND EXPay" & _
        "mentMethodDescription IS NULL) AND (EXPaymentMethodDisplay = @Original_EXPayment" & _
        "MethodDisplay OR @Original_EXPaymentMethodDisplay IS NULL AND EXPaymentMethodDis" & _
        "play IS NULL); SELECT EGType, EXPaymentMethod, EXPaymentMethodDisplay, EXPayment" & _
        "MethodDescription FROM EXPaymentMethods WHERE (EGType = @EGType) AND (EXPaymentM" & _
        "ethod = @EXPaymentMethod)"
        Me.SqlUpdateCommand4.Connection = Me.SqlConnection
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 2, "EGType"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, "EXPaymentMethod"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXPaymentMethodDisplay", System.Data.SqlDbType.VarChar, 50, "EXPaymentMethodDisplay"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXPaymentMethodDescription", System.Data.SqlDbType.VarChar, 300, "EXPaymentMethodDescription"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXPaymentMethod", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXPaymentMethodDescription", System.Data.SqlDbType.VarChar, 300, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXPaymentMethodDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXPaymentMethodDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXPaymentMethodDisplay", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT EXPaymentMethod, PMShowInJobScreen, PMShowInReceipts, PMShowInAllowances, " & _
        "PMShowInBonuses, PMReceiptsPaymentTypeDisplay, PMAllowancesPaymentTypeDisplay, P" & _
        "MBonusesPaymentTypeDisplay, PMAllowValueInJobScreen FROM EXPaymentMethod_Rules"
        Me.SqlSelectCommand5.Connection = Me.SqlConnection
        '
        'SqlInsertCommand5
        '
        Me.SqlInsertCommand5.CommandText = "INSERT INTO EXPaymentMethod_Rules(EXPaymentMethod, PMShowInJobScreen, PMShowInRec" & _
        "eipts, PMShowInAllowances, PMShowInBonuses, PMReceiptsPaymentTypeDisplay, PMAllo" & _
        "wancesPaymentTypeDisplay, PMBonusesPaymentTypeDisplay, PMAllowValueInJobScreen) " & _
        "VALUES (@EXPaymentMethod, @PMShowInJobScreen, @PMShowInReceipts, @PMShowInAllowa" & _
        "nces, @PMShowInBonuses, @PMReceiptsPaymentTypeDisplay, @PMAllowancesPaymentTypeD" & _
        "isplay, @PMBonusesPaymentTypeDisplay, @PMAllowValueInJobScreen); SELECT EXPaymen" & _
        "tMethod, PMShowInJobScreen, PMShowInReceipts, PMShowInAllowances, PMShowInBonuse" & _
        "s, PMReceiptsPaymentTypeDisplay, PMAllowancesPaymentTypeDisplay, PMBonusesPaymen" & _
        "tTypeDisplay, PMAllowValueInJobScreen FROM EXPaymentMethod_Rules WHERE (EXPaymen" & _
        "tMethod = @EXPaymentMethod)"
        Me.SqlInsertCommand5.Connection = Me.SqlConnection
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, "EXPaymentMethod"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMShowInJobScreen", System.Data.SqlDbType.Bit, 1, "PMShowInJobScreen"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMShowInReceipts", System.Data.SqlDbType.Bit, 1, "PMShowInReceipts"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMShowInAllowances", System.Data.SqlDbType.Bit, 1, "PMShowInAllowances"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMShowInBonuses", System.Data.SqlDbType.Bit, 1, "PMShowInBonuses"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMReceiptsPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, "PMReceiptsPaymentTypeDisplay"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMAllowancesPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, "PMAllowancesPaymentTypeDisplay"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMBonusesPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, "PMBonusesPaymentTypeDisplay"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMAllowValueInJobScreen", System.Data.SqlDbType.Bit, 1, "PMAllowValueInJobScreen"))
        '
        'SqlUpdateCommand5
        '
        Me.SqlUpdateCommand5.CommandText = "UPDATE EXPaymentMethod_Rules SET EXPaymentMethod = @EXPaymentMethod, PMShowInJobS" & _
        "creen = @PMShowInJobScreen, PMShowInReceipts = @PMShowInReceipts, PMShowInAllowa" & _
        "nces = @PMShowInAllowances, PMShowInBonuses = @PMShowInBonuses, PMReceiptsPaymen" & _
        "tTypeDisplay = @PMReceiptsPaymentTypeDisplay, PMAllowancesPaymentTypeDisplay = @" & _
        "PMAllowancesPaymentTypeDisplay, PMBonusesPaymentTypeDisplay = @PMBonusesPaymentT" & _
        "ypeDisplay, PMAllowValueInJobScreen = @PMAllowValueInJobScreen WHERE (EXPaymentM" & _
        "ethod = @Original_EXPaymentMethod) AND (PMAllowValueInJobScreen = @Original_PMAl" & _
        "lowValueInJobScreen) AND (PMAllowancesPaymentTypeDisplay = @Original_PMAllowance" & _
        "sPaymentTypeDisplay OR @Original_PMAllowancesPaymentTypeDisplay IS NULL AND PMAl" & _
        "lowancesPaymentTypeDisplay IS NULL) AND (PMBonusesPaymentTypeDisplay = @Original" & _
        "_PMBonusesPaymentTypeDisplay OR @Original_PMBonusesPaymentTypeDisplay IS NULL AN" & _
        "D PMBonusesPaymentTypeDisplay IS NULL) AND (PMReceiptsPaymentTypeDisplay = @Orig" & _
        "inal_PMReceiptsPaymentTypeDisplay OR @Original_PMReceiptsPaymentTypeDisplay IS N" & _
        "ULL AND PMReceiptsPaymentTypeDisplay IS NULL) AND (PMShowInAllowances = @Origina" & _
        "l_PMShowInAllowances) AND (PMShowInBonuses = @Original_PMShowInBonuses) AND (PMS" & _
        "howInJobScreen = @Original_PMShowInJobScreen) AND (PMShowInReceipts = @Original_" & _
        "PMShowInReceipts); SELECT EXPaymentMethod, PMShowInJobScreen, PMShowInReceipts, " & _
        "PMShowInAllowances, PMShowInBonuses, PMReceiptsPaymentTypeDisplay, PMAllowancesP" & _
        "aymentTypeDisplay, PMBonusesPaymentTypeDisplay, PMAllowValueInJobScreen FROM EXP" & _
        "aymentMethod_Rules WHERE (EXPaymentMethod = @EXPaymentMethod)"
        Me.SqlUpdateCommand5.Connection = Me.SqlConnection
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, "EXPaymentMethod"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMShowInJobScreen", System.Data.SqlDbType.Bit, 1, "PMShowInJobScreen"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMShowInReceipts", System.Data.SqlDbType.Bit, 1, "PMShowInReceipts"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMShowInAllowances", System.Data.SqlDbType.Bit, 1, "PMShowInAllowances"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMShowInBonuses", System.Data.SqlDbType.Bit, 1, "PMShowInBonuses"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMReceiptsPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, "PMReceiptsPaymentTypeDisplay"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMAllowancesPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, "PMAllowancesPaymentTypeDisplay"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMBonusesPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, "PMBonusesPaymentTypeDisplay"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMAllowValueInJobScreen", System.Data.SqlDbType.Bit, 1, "PMAllowValueInJobScreen"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXPaymentMethod", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMAllowValueInJobScreen", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMAllowValueInJobScreen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMAllowancesPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMAllowancesPaymentTypeDisplay", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMBonusesPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMBonusesPaymentTypeDisplay", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMReceiptsPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMReceiptsPaymentTypeDisplay", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMShowInAllowances", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMShowInAllowances", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMShowInBonuses", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMShowInBonuses", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMShowInJobScreen", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMShowInJobScreen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMShowInReceipts", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMShowInReceipts", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlDeleteCommand5
        '
        Me.SqlDeleteCommand5.CommandText = "DELETE FROM EXPaymentMethod_Rules WHERE (EXPaymentMethod = @Original_EXPaymentMet" & _
        "hod) AND (PMAllowValueInJobScreen = @Original_PMAllowValueInJobScreen) AND (PMAl" & _
        "lowancesPaymentTypeDisplay = @Original_PMAllowancesPaymentTypeDisplay OR @Origin" & _
        "al_PMAllowancesPaymentTypeDisplay IS NULL AND PMAllowancesPaymentTypeDisplay IS " & _
        "NULL) AND (PMBonusesPaymentTypeDisplay = @Original_PMBonusesPaymentTypeDisplay O" & _
        "R @Original_PMBonusesPaymentTypeDisplay IS NULL AND PMBonusesPaymentTypeDisplay " & _
        "IS NULL) AND (PMReceiptsPaymentTypeDisplay = @Original_PMReceiptsPaymentTypeDisp" & _
        "lay OR @Original_PMReceiptsPaymentTypeDisplay IS NULL AND PMReceiptsPaymentTypeD" & _
        "isplay IS NULL) AND (PMShowInAllowances = @Original_PMShowInAllowances) AND (PMS" & _
        "howInBonuses = @Original_PMShowInBonuses) AND (PMShowInJobScreen = @Original_PMS" & _
        "howInJobScreen) AND (PMShowInReceipts = @Original_PMShowInReceipts)"
        Me.SqlDeleteCommand5.Connection = Me.SqlConnection
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXPaymentMethod", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMAllowValueInJobScreen", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMAllowValueInJobScreen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMAllowancesPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMAllowancesPaymentTypeDisplay", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMBonusesPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMBonusesPaymentTypeDisplay", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMReceiptsPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMReceiptsPaymentTypeDisplay", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMShowInAllowances", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMShowInAllowances", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMShowInBonuses", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMShowInBonuses", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMShowInJobScreen", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMShowInJobScreen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMShowInReceipts", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMShowInReceipts", System.Data.DataRowVersion.Original, Nothing))
        '
        'daEXPaymentMethod_Rules
        '
        Me.daEXPaymentMethod_Rules.DeleteCommand = Me.SqlDeleteCommand5
        Me.daEXPaymentMethod_Rules.InsertCommand = Me.SqlInsertCommand5
        Me.daEXPaymentMethod_Rules.SelectCommand = Me.SqlSelectCommand5
        Me.daEXPaymentMethod_Rules.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "EXPaymentMethod_Rules", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("EXPaymentMethod", "EXPaymentMethod"), New System.Data.Common.DataColumnMapping("PMShowInJobScreen", "PMShowInJobScreen"), New System.Data.Common.DataColumnMapping("PMShowInReceipts", "PMShowInReceipts"), New System.Data.Common.DataColumnMapping("PMShowInAllowances", "PMShowInAllowances"), New System.Data.Common.DataColumnMapping("PMShowInBonuses", "PMShowInBonuses"), New System.Data.Common.DataColumnMapping("PMReceiptsPaymentTypeDisplay", "PMReceiptsPaymentTypeDisplay"), New System.Data.Common.DataColumnMapping("PMAllowancesPaymentTypeDisplay", "PMAllowancesPaymentTypeDisplay"), New System.Data.Common.DataColumnMapping("PMBonusesPaymentTypeDisplay", "PMBonusesPaymentTypeDisplay"), New System.Data.Common.DataColumnMapping("PMAllowValueInJobScreen", "PMAllowValueInJobScreen")})})
        Me.daEXPaymentMethod_Rules.UpdateCommand = Me.SqlUpdateCommand5
        '
        'HorizonalRuleLine3D8
        '
        Me.HorizonalRuleLine3D8.BackColor = System.Drawing.SystemColors.Control
        Me.HorizonalRuleLine3D8.Dock = System.Windows.Forms.DockStyle.Top
        Me.HorizonalRuleLine3D8.Location = New System.Drawing.Point(0, 352)
        Me.HorizonalRuleLine3D8.Name = "HorizonalRuleLine3D8"
        Me.HorizonalRuleLine3D8.Size = New System.Drawing.Size(490, 2)
        Me.HorizonalRuleLine3D8.TabIndex = 1
        '
        'frmExpenseWizard
        '
        Me.AcceptButton = Me.btnNext
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(490, 392)
        Me.ControlBox = False
        Me.Controls.Add(Me.HorizonalRuleLine3D8)
        Me.Controls.Add(Me.btnBack)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnNext)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExpenseWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add a New Expense Wizard"
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        CType(Me.txtEXName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage3.ResumeLayout(False)
        CType(Me.PanelControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl5.ResumeLayout(False)
        CType(Me.txtEXType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage4.ResumeLayout(False)
        CType(Me.txtEXPaymentMethod.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl7.ResumeLayout(False)
        Me.XtraTabPage6.ResumeLayout(False)
        CType(Me.txtALAllowance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsWizards, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtALFromDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl9.ResumeLayout(False)
        Me.XtraTabPage5.ResumeLayout(False)
        CType(Me.txtEXAssignToPortion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl8.ResumeLayout(False)
        Me.XtraTabPage7.ResumeLayout(False)
        CType(Me.chkEXAppearInCalendar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl10.ResumeLayout(False)
        Me.XtraTabPage8.ResumeLayout(False)
        CType(Me.PanelControl11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl11.ResumeLayout(False)
        CType(Me.PanelControl12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmExpenseWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.XtraTabControl1.SelectedTabPageIndex = 0
        Me.XtraTabControl1.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False
    End Sub

    ' This data must be loaded BEFORE the DataRow is loaded (eg lookups etc)
    Private Sub FillPreliminaryData()
        daEXPaymentMethod_Rules.Fill(DsWizards)
    End Sub

    Private Sub FillEGTypeDependantData()
        ' EXTypes
        daEXTypes.SelectCommand.Parameters("@EGType").Value = EGType
        daEXTypes.Fill(DsGTMS)

        ' EXAssignToPortions
        daEXAssignToPortions.SelectCommand.Parameters("@EGType").Value = EGType
        daEXAssignToPortions.Fill(DsGTMS)

        ' EXPaymentMethods
        daEXPaymentMethods.SelectCommand.Parameters("@EGType").Value = EGType
        daEXPaymentMethods.Fill(DsGTMS)
    End Sub

    ' This data must be loaded AFTER the DataRow is loaded (eg record related data)
    Private Sub FillData()
        ExpenseAllowanceDataRow = DsWizards.ExpenseAllowances.NewExpenseAllowancesRow
        ExpenseAllowanceDataRow("BRID") = BRID
        ExpenseAllowanceDataRow("EXID") = EXID
        DsWizards.ExpenseAllowances.AddExpenseAllowancesRow(ExpenseAllowanceDataRow)
        CustomizeScreen()
    End Sub

    Dim OK As Boolean = False
    Private Sub RunOK()
        ' EndEdit() to end editing the dataset record so that we can update
        DataRow.EndEdit()
        SqlDataAdapter.Update(DsGTMS)
        If ShowAnnualAmount() Then
            sp_InsertExpenseAllowance(BRID, EXID, ExpenseAllowanceDataRow("ALFromDate"), ExpenseAllowanceDataRow("ALAllowance"), ExpenseAllowanceDataRow("ALHours"), Transaction)
        End If
        DataAccess.spExecLockRequest("sp_ReleaseExpenseLock", BRID, EXID, Transaction)
        Transaction.Commit()
        SqlConnection.Close()

        OK = True
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private HasChanges As Boolean = False
    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            DataRow.EndEdit()
            If HasChanges Or DsGTMS.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If response = MsgBoxResult.Yes Then
                DataAccess.spExecLockRequest("sp_ReleaseExpenseLock", BRID, EXID, Transaction)
                Transaction.Rollback()
                SqlConnection.Close()
            Else
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        btnBack.Enabled = True
        If ValidateTab(XtraTabControl1.SelectedTabPageIndex) Then
            If XtraTabControl1.SelectedTabPageIndex = XtraTabControl1.TabPages.Count - 1 Then
                RunOK()
            Else
                XtraTabControl1.SelectedTabPageIndex = NextTabIndex(XtraTabControl1.SelectedTabPageIndex)
            End If
        End If
    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        XtraTabControl1.SelectedTabPageIndex = PreviousTabIndex(XtraTabControl1.SelectedTabPageIndex)
    End Sub

    Private Function NextTabIndex(ByVal CurrentTabIndex As Integer) As Integer
        If CurrentTabIndex = XtraTabControl1.TabPages.Count - 1 Then
            Throw New ArgumentOutOfRangeException("CurrentTabIndex", CurrentTabIndex)
        End If
        NextTabIndex = CurrentTabIndex + 1
        Select Case NextTabIndex
            Case 4 ' Annual Amount
                If Not ShowAnnualAmount() Then
                    Return NextTabIndex(NextTabIndex) ' This tab is not valid - get next
                End If
            Case 5 ' Category
                If Not (DataRow("EGType") = "DL") Then
                    Return NextTabIndex(NextTabIndex) ' This tab is not valid - get next
                End If
            Case 6 ' Show in calendar?
                If Not (DataRow("EGType") = "DL" Or DataRow("EGType") = "RC") Then
                    Return NextTabIndex(NextTabIndex) ' This tab is not valid - get next
                End If
            Case XtraTabControl1.TabPages.Count - 1
                btnNext.Text = "&Finish"
                btnNext.DialogResult = DialogResult.OK
        End Select
        Return NextTabIndex ' Found the correct value
    End Function

    Private Function PreviousTabIndex(ByVal CurrentTabIndex As Integer) As Integer
        If CurrentTabIndex = XtraTabControl1.TabPages.Count - 1 Then
            btnNext.Text = "&Next >"
            btnNext.DialogResult = DialogResult.None
        End If
        PreviousTabIndex = CurrentTabIndex - 1
        Select Case PreviousTabIndex
            Case 0
                btnBack.Enabled = False
            Case 4 ' Annual Amount
                If Not ShowAnnualAmount() Then
                    Return PreviousTabIndex(PreviousTabIndex) ' This tab is not valid - get next
                End If
            Case 5 ' Category
                If Not (DataRow("EGType") = "DL") Then
                    Return PreviousTabIndex(PreviousTabIndex) ' This tab is not valid - get next
                End If
            Case 6 ' Show in calendar?
                If Not (DataRow("EGType") = "DL" Or DataRow("EGType") = "RC") Then
                    Return PreviousTabIndex(PreviousTabIndex) ' This tab is not valid - get next
                End If
        End Select
        Return PreviousTabIndex ' Found the correct value
    End Function

    Private Function ShowAnnualAmount() As Boolean
        Return DsWizards.EXPaymentMethod_Rules.FindByEXPaymentMethod(txtEXPaymentMethod.EditValue).PMShowInAllowances
    End Function

    Private Function ValidateTab(ByVal Tab As Integer) As Boolean
        Select Case Tab
            Case 1
                If DataRow("EXName") Is DBNull.Value Then
                    Message.ShowMessage("You must enter a name.", MessageBoxIcon.Exclamation)
                    Return False
                End If
            Case 2
                If DataRow("EXType") Is DBNull.Value Then
                    Message.ShowMessage("You must enter an expense type.", MessageBoxIcon.Exclamation)
                    Return False
                End If
            Case 3
                If DataRow("EXPaymentMethod") Is DBNull.Value Then
                    Message.ShowMessage("You must enter a payment method.", MessageBoxIcon.Exclamation)
                    Return False
                End If
            Case 4
                If ExpenseAllowanceDataRow("ALFromDate") Is DBNull.Value Then
                    Message.ShowMessage("You must enter a start date.", MessageBoxIcon.Exclamation)
                    Return False
                End If
                If ExpenseAllowanceDataRow("ALAllowance") Is DBNull.Value Then
                    Message.ShowMessage("You must enter an annual amount.", MessageBoxIcon.Exclamation)
                    Return False
                End If
            Case 5
                If DataRow("EXAssignToPortion") Is DBNull.Value Then
                    Message.ShowMessage("You must enter a category.", MessageBoxIcon.Exclamation)
                    Return False
                End If
        End Select
        Return True
    End Function

    Private Sub CustomizeScreen()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmExpense))
        Select Case DataRow("EGType")
            Case "DL"
                Me.Text = "New Direct Labor Wizard"
                Me.lblIntroTitle.Text = "Welcome to the Add New Direct Labor Wizard"
                Me.lblIntroDesc.Text = "This wizard guides you through adding a new direct labor person to your business setup."
                Me.lblFinishDesc1.Text = "The wizard now has enough information to add the new direct labor person."
                Me.lblFinishDesc2.Text = "Click 'Finish' to complete the wizard and add the new direct labor person to your business setup."
                Me.Icon = New Icon(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream("WindowsApplication.ExpenseDL.ico"))
            Case "RC"
                Me.Text = "New Salesperson Wizard"
                Me.lblIntroTitle.Text = "Welcome to the Add New Salesperson Wizard"
                Me.lblIntroDesc.Text = "This wizard guides you through adding a new salesperson to your business setup."
                Me.lblFinishDesc1.Text = "The wizard now has enough information to add the new salesperson."
                Me.lblFinishDesc2.Text = "Click 'Finish' to complete the wizard and add the new salesperson to your business setup."
                Me.Icon = New Icon(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream("WindowsApplication.ExpenseRC.ico"))
            Case "OE"
                chkEXAppearInCalendar.Enabled = False
                Me.Text = "New Non-Wage Expense Wizard"
                Me.lblIntroTitle.Text = "Welcome to the Add New Non-Wage Expense Wizard"
                Me.lblIntroDesc.Text = "This wizard guides you through adding a new non-wage expense to your business setup."
                Me.lblFinishDesc1.Text = "The wizard now has enough information to add the new non-wage expense."
                Me.lblFinishDesc2.Text = "Click 'Finish' to complete the wizard and add the new non-wage expense to your business setup."
                Me.Icon = New Icon(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream("WindowsApplication.ExpenseOE.ico"))
            Case "OW"
                chkEXAppearInCalendar.Enabled = False
                Me.Text = "New Non-Direct Labor Wizard"
                Me.lblIntroTitle.Text = "Welcome to the Add New Non-Direct Labor Wizard"
                Me.lblIntroDesc.Text = "This wizard guides you through adding a new non-direct labor person to your business setup."
                Me.lblFinishDesc1.Text = "The wizard now has enough information to add the new non-direct labor person."
                Me.lblFinishDesc2.Text = "Click 'Finish' to complete the wizard and add the new non-direct labor person to your business setup."
                Me.Icon = New Icon(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream("WindowsApplication.ExpenseOW.ico"))
            Case "OH"
                chkEXAppearInCalendar.Enabled = False
                Me.Text = "New Overhead Wizard"
                Me.lblIntroTitle.Text = "Welcome to the Add New Overhead Wizard"
                Me.lblIntroDesc.Text = "This wizard guides you through adding a new overhead to your business setup."
                Me.lblFinishDesc1.Text = "The wizard now has enough information to add the new overhead."
                Me.lblFinishDesc2.Text = "Click 'Finish' to complete the wizard and add the new overhead to your business setup."
        End Select
    End Sub

    Private Sub txtEXName_ParseEditValue(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) Handles txtEXName.ParseEditValue
        Format.Text_ParseEditValue(sender, e)
    End Sub

    Private Sub txtCurrency_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) Handles txtALAllowance.ParseEditValue
        Format.Decimal_ParseEditValue(sender, e)
    End Sub

    Private Sub Date_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtALFromDate.Validating
        Library.DateEdit_Validating(sender, e)
    End Sub

End Class

