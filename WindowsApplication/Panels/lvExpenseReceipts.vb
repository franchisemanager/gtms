Imports Power.Forms

Public Class lvExpenseReceipts
    Inherits System.Windows.Forms.UserControl
    Implements IListControl

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Connection As SqlClient.SqlConnection, ByVal BRID As Integer, ByVal ExplorerForm As frmExplorer)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.Connection = Connection
        Me.BRID = BRID
        hExplorerForm = ExplorerForm
        FillDataSet()
    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents DsGTMS As WindowsApplication.dsGTMS
    Friend WithEvents hSqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colEXName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEXPaymentMethodDisplay As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEGName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEXTypeDisplay As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents colPMReceiptsPaymentTypeDisplay As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.hSqlConnection = New System.Data.SqlClient.SqlConnection
        Me.DsGTMS = New WindowsApplication.dsGTMS
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colEXName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colEXTypeDisplay = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colEXPaymentMethodDisplay = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colEGName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colPMReceiptsPaymentTypeDisplay = New DevExpress.XtraGrid.Columns.GridColumn
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'hSqlConnection
        '
        Me.hSqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;integrated security=SSPI;data source=""SERVER" & _
        "\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'DsGTMS
        '
        Me.DsGTMS.DataSetName = "dsGTMS"
        Me.DsGTMS.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VExpenses_Display", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("EGID", "EGID"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("EXName", "EXName"), New System.Data.Common.DataColumnMapping("EXDiscontinued", "EXDiscontinued"), New System.Data.Common.DataColumnMapping("EXAssignToPortionDisplay", "EXAssignToPortionDisplay"), New System.Data.Common.DataColumnMapping("EXPaymentMethodDisplay", "EXPaymentMethodDisplay"), New System.Data.Common.DataColumnMapping("EXTypeDisplay", "EXTypeDisplay"), New System.Data.Common.DataColumnMapping("EGName", "EGName")})})
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO VExpenses_Display(BRID, EGID, EXName, EXDiscontinued, EXAssignToPorti" & _
        "onDisplay, EXPaymentMethodDisplay, EXTypeDisplay, EGName) VALUES (@BRID, @EGID, " & _
        "@EXName, @EXDiscontinued, @EXAssignToPortionDisplay, @EXPaymentMethodDisplay, @E" & _
        "XTypeDisplay, @EGName); SELECT BRID, EGID, EXID, EXName, EXDiscontinued, EXAssig" & _
        "nToPortionDisplay, EXPaymentMethodDisplay, EXTypeDisplay, EGName FROM VExpenses_" & _
        "Display"
        Me.SqlInsertCommand1.Connection = Me.hSqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGID", System.Data.SqlDbType.SmallInt, 2, "EGID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXDiscontinued", System.Data.SqlDbType.VarChar, 12, "EXDiscontinued"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAssignToPortionDisplay", System.Data.SqlDbType.VarChar, 50, "EXAssignToPortionDisplay"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXPaymentMethodDisplay", System.Data.SqlDbType.VarChar, 50, "EXPaymentMethodDisplay"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXTypeDisplay", System.Data.SqlDbType.VarChar, 50, "EXTypeDisplay"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGName", System.Data.SqlDbType.VarChar, 50, "EGName"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, EGID, EXID, EXName, EXDiscontinued, EXAssignToPortionDisplay, EXPaym" & _
        "entMethodDisplay, EXTypeDisplay, EGName, PMReceiptsPaymentTypeDisplay FROM VExpe" & _
        "nses_Display WHERE (BRID = @BRID) AND (PMShowInReceipts = 1) AND (EXDiscontinued" & _
        " = 0)"
        Me.SqlSelectCommand1.Connection = Me.hSqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.DsGTMS.VExpenses_Display
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'GridControl1.EmbeddedNavigator
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(632, 392)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
        Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colEXName, Me.colEXTypeDisplay, Me.colEXPaymentMethodDisplay, Me.colEGName, Me.colPMReceiptsPaymentTypeDisplay})
        Me.GridView1.CustomizationFormBounds = New System.Drawing.Rectangle(802, 474, 208, 156)
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.GroupCount = 1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsNavigation.AutoFocusNewRow = True
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsView.ShowGroupedColumns = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.OptionsView.ShowHorzLines = False
        Me.GridView1.OptionsView.ShowIndicator = False
        Me.GridView1.OptionsView.ShowVertLines = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colEGName, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colEXName, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colEXName
        '
        Me.colEXName.Caption = "Name"
        Me.colEXName.FieldName = "EXName"
        Me.colEXName.Name = "colEXName"
        Me.colEXName.Visible = True
        Me.colEXName.VisibleIndex = 0
        Me.colEXName.Width = 163
        '
        'colEXTypeDisplay
        '
        Me.colEXTypeDisplay.Caption = "Expense Type"
        Me.colEXTypeDisplay.FieldName = "EXTypeDisplay"
        Me.colEXTypeDisplay.Name = "colEXTypeDisplay"
        Me.colEXTypeDisplay.Visible = True
        Me.colEXTypeDisplay.VisibleIndex = 3
        Me.colEXTypeDisplay.Width = 157
        '
        'colEXPaymentMethodDisplay
        '
        Me.colEXPaymentMethodDisplay.Caption = "Payment Method"
        Me.colEXPaymentMethodDisplay.FieldName = "EXPaymentMethodDisplay"
        Me.colEXPaymentMethodDisplay.Name = "colEXPaymentMethodDisplay"
        Me.colEXPaymentMethodDisplay.Width = 144
        '
        'colEGName
        '
        Me.colEGName.Caption = "Expense Group"
        Me.colEGName.FieldName = "EGName"
        Me.colEGName.Name = "colEGName"
        Me.colEGName.Visible = True
        Me.colEGName.VisibleIndex = 1
        Me.colEGName.Width = 131
        '
        'colPMReceiptsPaymentTypeDisplay
        '
        Me.colPMReceiptsPaymentTypeDisplay.Caption = "Payment Type"
        Me.colPMReceiptsPaymentTypeDisplay.FieldName = "PMReceiptsPaymentTypeDisplay"
        Me.colPMReceiptsPaymentTypeDisplay.Name = "colPMReceiptsPaymentTypeDisplay"
        Me.colPMReceiptsPaymentTypeDisplay.Visible = True
        Me.colPMReceiptsPaymentTypeDisplay.VisibleIndex = 2
        Me.colPMReceiptsPaymentTypeDisplay.Width = 179
        '
        'lvExpenseReceipts
        '
        Me.Controls.Add(Me.GridControl1)
        Me.Name = "lvExpenseReceipts"
        Me.Size = New System.Drawing.Size(632, 392)
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public Property Connection() As SqlClient.SqlConnection
        Get
            Return hSqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            hSqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Public Function FillDataSet() As Integer Implements IListControl.FillDataSet
        ' Open connection
        Dim trans As SqlClient.SqlTransaction = Connection.BeginTransaction(IsolationLevel.ReadUncommitted)
        Power.Library.Library.ApplyTransactionToAllDataAdapters(trans, Me)
        Me.GridControl1.DataSource.Clear()
        FillDataSet = SqlDataAdapter.Fill(DsGTMS)
        GridView1.ExpandAllGroups()
        'Close connecton
        trans.Commit()
    End Function

    Private Sub GridView1_RowCountChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.RowCountChanged
        ExplorerForm.UpdateNumItems(NumItems)
        ExplorerForm.UpdateVisibleItems(VisibleItems)
    End Sub

    Public ReadOnly Property VisibleItems() As Integer Implements Power.Library.IListControl.VisibleItems
        Get
            Return GridView1.RowCount
        End Get
    End Property

    Public ReadOnly Property NumItems() As Integer Implements Power.Library.IListControl.NumItems
        Get
            Return DsGTMS.Tables(SqlDataAdapter.TableMappings(0).DataSetTable).Rows.Count
        End Get
    End Property

    Private hBRID As Integer
    Public Property BRID() As Integer
        Get
            Return hBRID
        End Get
        Set(ByVal Value As Integer)
            hBRID = Value
            SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = Value
        End Set
    End Property

    Private hBAType As String
    Public Property BAType() As String
        Get
            Return hBAType
        End Get
        Set(ByVal Value As String)
            hBAType = Value
            SqlDataAdapter.SelectCommand.Parameters("@BAType").Value = Value
        End Set
    End Property

    Public ReadOnly Property SelectedRow() As DataRow
        Get
            If Not GridView1.GetSelectedRows Is Nothing Then
                Return GridView1.GetDataRow(GridView1.GetSelectedRows(0))
            End If
        End Get
    End Property

    Public ReadOnly Property SelectedRowField(ByVal Field As String) As Object
        Get
            If Not SelectedRow Is Nothing Then
                Return SelectedRow.Item(Field)
            Else
                Return DBNull.Value
            End If
        End Get
    End Property

    Public Function SelectRow(ByVal BRID As Int32, ByVal EXID As Int64) As Boolean
        Dim i As Integer = 0
        Do Until False
            Try
                If GridView1.GetDataRow(GridView1.GetVisibleRowHandle(i))("BRID") = BRID And _
                        GridView1.GetDataRow(GridView1.GetVisibleRowHandle(i))("EXID") = EXID Then
                    GridView1.ClearSelection()
                    GridView1.SelectRow(GridView1.GetVisibleRowHandle(i))
                    Return True
                End If
                i += 1
            Catch ex As NullReferenceException
                Return False
            End Try
        Loop
        Return False
    End Function

#Region " New, Open and Delete "

    Private Sub GridControl1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridControl1.DoubleClick
        List_Edit()
    End Sub

    Public Sub List_New() Implements IListControl.List_New

    End Sub

    Public Sub List_Edit() Implements IListControl.List_Edit
        If Not SelectedRow Is Nothing Then
            Dim c As Cursor = ParentForm.Cursor
            ParentForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If ExpenseExists(SelectedRowField("BRID"), SelectedRowField("EXID")) Then
                Try
                    Dim gui As frmExpenseAdjustments = frmExpenseAdjustments.Edit(SelectedRowField("BRID"), SelectedRowField("EXID"), "PR", SelectedRowField("EXName"), SelectedRowField("PMReceiptsPaymentTypeDisplay"), "AmountsSpecifiedIncrementallyTerms.html")
                    gui.ShowDialog(Me)
                    FillDataSet()
                Catch ex As ObjectLockedException
                    Message.CurrentlyAccessed("item")
                End Try
            Else
                Message.AlreadyDeleted("item", Message.ObjectAction.Edit)
                Me.GridControl1.DataSource.Clear()
                FillDataSet()
            End If
            ParentForm.Cursor = c
        End If
    End Sub

    Public Sub List_Delete() Implements IListControl.List_Delete

    End Sub

    Private Function ExpenseExists(ByVal BRID As Int32, ByVal EXID As Int64) As Boolean
        Dim cnn As SqlClient.SqlConnection = Connection
        Dim cmd As New SqlClient.SqlCommand("SELECT Count(EXID) FROM Expenses WHERE BRID = @BRID AND EXID = @EXID", cnn)
        cmd.CommandType = CommandType.Text
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@EXID", EXID)
        Return cmd.ExecuteScalar > 0
    End Function

#End Region

    Private hExplorerForm As Form
    Public ReadOnly Property ExplorerForm() As frmExplorer
        Get
            Return hExplorerForm
        End Get
    End Property

    Public ReadOnly Property AllowDelete() As Boolean Implements Power.Library.IListControl.AllowDelete
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property AllowEdit() As Boolean Implements Power.Library.IListControl.AllowEdit
        Get
            Return True
        End Get
    End Property

    Public ReadOnly Property AllowNew() As Boolean Implements Power.Library.IListControl.AllowNew
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property AllowUndelete() As Boolean Implements Power.Library.IListControl.AllowUndelete
        Get
            Return False
        End Get
    End Property

    Public Sub List_Undelete() Implements Power.Library.IListControl.List_Undelete

    End Sub

    Public ReadOnly Property DeleteCaption() As String Implements Power.Library.IListControl.DeleteCaption
        Get
            Return Nothing
        End Get
    End Property

    Public ReadOnly Property UndeleteCaption() As String Implements Power.Library.IListControl.UndeleteCaption
        Get
            Return Nothing
        End Get
    End Property

End Class
