Public Class frmLogin
    Inherits DevExpress.XtraEditors.XtraForm

    Private SplashScreen As frmSplash
    Private DataRow As DataRow

#Region " Windows Form Designer generated code "

    Public Sub New(ByRef ConnectionSuccessful As Boolean, Optional ByVal SplashScreen As frmSplash = Nothing)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        SqlConnection.ConnectionString = ConnectionString
        Try
            SqlConnection.Open()
            ConnectionSuccessful = True
        Catch ex As SqlClient.SqlException
            ConnectionSuccessful = False
            Return
        End Try
        Dim trans As SqlClient.SqlTransaction = SqlConnection.BeginTransaction(IsolationLevel.ReadUncommitted)
        Power.Library.ApplyTransactionToAllDataAdapters(trans, Me)
        daBranches.Fill(DsHeadOffice)
        trans.Commit()
        SqlConnection.Close()

        Me.SplashScreen = SplashScreen
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtUsername As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtPassword As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents chkUSIsHeadOffice As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents daBranches As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents DsHeadOffice As WindowsApplication.dsHeadOffice
    Friend WithEvents txtBRID As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmLogin))
        Dim configurationAppSettings As System.Configuration.AppSettingsReader = New System.Configuration.AppSettingsReader
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtUsername = New DevExpress.XtraEditors.TextEdit
        Me.txtPassword = New DevExpress.XtraEditors.TextEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.Label4 = New System.Windows.Forms.Label
        Me.chkUSIsHeadOffice = New DevExpress.XtraEditors.CheckEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.daBranches = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.DsHeadOffice = New WindowsApplication.dsHeadOffice
        Me.txtBRID = New DevExpress.XtraEditors.LookUpEdit
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        CType(Me.txtUsername.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPassword.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkUSIsHeadOffice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsHeadOffice, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBRID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(176, 216)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 7
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(256, 216)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 8
        Me.btnCancel.Text = "Cancel"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 64)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 21)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Username:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtUsername
        '
        Me.txtUsername.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtUsername.EditValue = ""
        Me.txtUsername.Location = New System.Drawing.Point(72, 64)
        Me.txtUsername.Name = "txtUsername"
        Me.txtUsername.Size = New System.Drawing.Size(256, 20)
        Me.txtUsername.TabIndex = 10
        '
        'txtPassword
        '
        Me.txtPassword.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPassword.EditValue = ""
        Me.txtPassword.Location = New System.Drawing.Point(72, 96)
        Me.txtPassword.Name = "txtPassword"
        '
        'txtPassword.Properties
        '
        Me.txtPassword.Properties.PasswordChar = Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(256, 20)
        Me.txtPassword.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(8, 96)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 21)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Password:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton1.Image = CType(resources.GetObject("SimpleButton1.Image"), System.Drawing.Image)
        Me.SimpleButton1.Location = New System.Drawing.Point(8, 216)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(72, 23)
        Me.SimpleButton1.TabIndex = 6
        Me.SimpleButton1.Text = "Help"
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = CType(configurationAppSettings.GetValue("SqlConnection.ConnectionString", GetType(System.String)), String)
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.Location = New System.Drawing.Point(8, 8)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(328, 48)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Welcome to the Franchise Manager for Align Kitchens.  You must log in wi" &
        "th your username and password to begin using the software."
        '
        'chkUSIsHeadOffice
        '
        Me.chkUSIsHeadOffice.Location = New System.Drawing.Point(8, 136)
        Me.chkUSIsHeadOffice.Name = "chkUSIsHeadOffice"
        '
        'chkUSIsHeadOffice.Properties
        '
        Me.chkUSIsHeadOffice.Properties.Caption = "Log in as head office user"
        Me.chkUSIsHeadOffice.Size = New System.Drawing.Size(152, 18)
        Me.chkUSIsHeadOffice.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(8, 168)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(64, 21)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Branch:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'daBranches
        '
        Me.daBranches.DeleteCommand = Me.SqlDeleteCommand3
        Me.daBranches.InsertCommand = Me.SqlInsertCommand3
        Me.daBranches.SelectCommand = Me.SqlSelectCommand3
        Me.daBranches.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Branches", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("NextID", "NextID"), New System.Data.Common.DataColumnMapping("BRStreetAddress01", "BRStreetAddress01"), New System.Data.Common.DataColumnMapping("BRStreetAddress02", "BRStreetAddress02"), New System.Data.Common.DataColumnMapping("BRSuburb", "BRSuburb"), New System.Data.Common.DataColumnMapping("BRState", "BRState"), New System.Data.Common.DataColumnMapping("BRPostCode", "BRPostCode"), New System.Data.Common.DataColumnMapping("BRPhoneNumber", "BRPhoneNumber"), New System.Data.Common.DataColumnMapping("BRFaxNumber", "BRFaxNumber"), New System.Data.Common.DataColumnMapping("BRBusinessName", "BRBusinessName"), New System.Data.Common.DataColumnMapping("BRMobileNumber", "BRMobileNumber"), New System.Data.Common.DataColumnMapping("BRShortName", "BRShortName"), New System.Data.Common.DataColumnMapping("BREmail", "BREmail"), New System.Data.Common.DataColumnMapping("BRGST", "BRGST"), New System.Data.Common.DataColumnMapping("BRSuperannuation", "BRSuperannuation"), New System.Data.Common.DataColumnMapping("BRIncludeInReporting", "BRIncludeInReporting")})})
        Me.daBranches.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM Branches WHERE (BRID = @Original_BRID) AND (BRBusinessName = @Origina" & _
        "l_BRBusinessName OR @Original_BRBusinessName IS NULL AND BRBusinessName IS NULL)" & _
        " AND (BREmail = @Original_BREmail OR @Original_BREmail IS NULL AND BREmail IS NU" & _
        "LL) AND (BRFaxNumber = @Original_BRFaxNumber OR @Original_BRFaxNumber IS NULL AN" & _
        "D BRFaxNumber IS NULL) AND (BRGST = @Original_BRGST OR @Original_BRGST IS NULL A" & _
        "ND BRGST IS NULL) AND (BRIncludeInReporting = @Original_BRIncludeInReporting OR " & _
        "@Original_BRIncludeInReporting IS NULL AND BRIncludeInReporting IS NULL) AND (BR" & _
        "MobileNumber = @Original_BRMobileNumber OR @Original_BRMobileNumber IS NULL AND " & _
        "BRMobileNumber IS NULL) AND (BRPhoneNumber = @Original_BRPhoneNumber OR @Origina" & _
        "l_BRPhoneNumber IS NULL AND BRPhoneNumber IS NULL) AND (BRPostCode = @Original_B" & _
        "RPostCode OR @Original_BRPostCode IS NULL AND BRPostCode IS NULL) AND (BRShortNa" & _
        "me = @Original_BRShortName OR @Original_BRShortName IS NULL AND BRShortName IS N" & _
        "ULL) AND (BRState = @Original_BRState OR @Original_BRState IS NULL AND BRState I" & _
        "S NULL) AND (BRStreetAddress01 = @Original_BRStreetAddress01 OR @Original_BRStre" & _
        "etAddress01 IS NULL AND BRStreetAddress01 IS NULL) AND (BRStreetAddress02 = @Ori" & _
        "ginal_BRStreetAddress02 OR @Original_BRStreetAddress02 IS NULL AND BRStreetAddre" & _
        "ss02 IS NULL) AND (BRSuburb = @Original_BRSuburb OR @Original_BRSuburb IS NULL A" & _
        "ND BRSuburb IS NULL) AND (BRSuperannuation = @Original_BRSuperannuation OR @Orig" & _
        "inal_BRSuperannuation IS NULL AND BRSuperannuation IS NULL) AND (NextID = @Origi" & _
        "nal_NextID)"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRBusinessName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRBusinessName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BREmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BREmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRGST", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRGST", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRIncludeInReporting", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRIncludeInReporting", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRShortName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRShortName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRSuperannuation", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRSuperannuation", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NextID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NextID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO Branches(NextID, BRStreetAddress01, BRStreetAddress02, BRSuburb, BRSt" & _
        "ate, BRPostCode, BRPhoneNumber, BRFaxNumber, BRBusinessName, BRMobileNumber, BRS" & _
        "hortName, BREmail, BRGST, BRSuperannuation, BRIncludeInReporting) VALUES (@NextI" & _
        "D, @BRStreetAddress01, @BRStreetAddress02, @BRSuburb, @BRState, @BRPostCode, @BR" & _
        "PhoneNumber, @BRFaxNumber, @BRBusinessName, @BRMobileNumber, @BRShortName, @BREm" & _
        "ail, @BRGST, @BRSuperannuation, @BRIncludeInReporting); SELECT BRID, NextID, BRS" & _
        "treetAddress01, BRStreetAddress02, BRSuburb, BRState, BRPostCode, BRPhoneNumber," & _
        " BRFaxNumber, BRBusinessName, BRMobileNumber, BRShortName, BREmail, BRGST, BRSup" & _
        "erannuation, BRIncludeInReporting FROM Branches WHERE (BRID = @@IDENTITY)"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NextID", System.Data.SqlDbType.BigInt, 8, "NextID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRStreetAddress01", System.Data.SqlDbType.VarChar, 100, "BRStreetAddress01"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRStreetAddress02", System.Data.SqlDbType.VarChar, 100, "BRStreetAddress02"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRSuburb", System.Data.SqlDbType.VarChar, 50, "BRSuburb"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRState", System.Data.SqlDbType.VarChar, 3, "BRState"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRPostCode", System.Data.SqlDbType.VarChar, 20, "BRPostCode"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRPhoneNumber", System.Data.SqlDbType.VarChar, 20, "BRPhoneNumber"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRFaxNumber", System.Data.SqlDbType.VarChar, 20, "BRFaxNumber"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRBusinessName", System.Data.SqlDbType.VarChar, 100, "BRBusinessName"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRMobileNumber", System.Data.SqlDbType.VarChar, 20, "BRMobileNumber"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRShortName", System.Data.SqlDbType.VarChar, 50, "BRShortName"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BREmail", System.Data.SqlDbType.VarChar, 50, "BREmail"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRGST", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRGST", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRSuperannuation", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRSuperannuation", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRIncludeInReporting", System.Data.SqlDbType.Bit, 1, "BRIncludeInReporting"))
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT BRID, NextID, BRStreetAddress01, BRStreetAddress02, BRSuburb, BRState, BRP" & _
        "ostCode, BRPhoneNumber, BRFaxNumber, BRBusinessName, BRMobileNumber, BRShortName" & _
        ", BREmail, BRGST, BRSuperannuation, BRIncludeInReporting FROM Branches"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE Branches SET NextID = @NextID, BRStreetAddress01 = @BRStreetAddress01, BRS" & _
        "treetAddress02 = @BRStreetAddress02, BRSuburb = @BRSuburb, BRState = @BRState, B" & _
        "RPostCode = @BRPostCode, BRPhoneNumber = @BRPhoneNumber, BRFaxNumber = @BRFaxNum" & _
        "ber, BRBusinessName = @BRBusinessName, BRMobileNumber = @BRMobileNumber, BRShort" & _
        "Name = @BRShortName, BREmail = @BREmail, BRGST = @BRGST, BRSuperannuation = @BRS" & _
        "uperannuation, BRIncludeInReporting = @BRIncludeInReporting WHERE (BRID = @Origi" & _
        "nal_BRID) AND (BRBusinessName = @Original_BRBusinessName OR @Original_BRBusiness" & _
        "Name IS NULL AND BRBusinessName IS NULL) AND (BREmail = @Original_BREmail OR @Or" & _
        "iginal_BREmail IS NULL AND BREmail IS NULL) AND (BRFaxNumber = @Original_BRFaxNu" & _
        "mber OR @Original_BRFaxNumber IS NULL AND BRFaxNumber IS NULL) AND (BRGST = @Ori" & _
        "ginal_BRGST OR @Original_BRGST IS NULL AND BRGST IS NULL) AND (BRIncludeInReport" & _
        "ing = @Original_BRIncludeInReporting OR @Original_BRIncludeInReporting IS NULL A" & _
        "ND BRIncludeInReporting IS NULL) AND (BRMobileNumber = @Original_BRMobileNumber " & _
        "OR @Original_BRMobileNumber IS NULL AND BRMobileNumber IS NULL) AND (BRPhoneNumb" & _
        "er = @Original_BRPhoneNumber OR @Original_BRPhoneNumber IS NULL AND BRPhoneNumbe" & _
        "r IS NULL) AND (BRPostCode = @Original_BRPostCode OR @Original_BRPostCode IS NUL" & _
        "L AND BRPostCode IS NULL) AND (BRShortName = @Original_BRShortName OR @Original_" & _
        "BRShortName IS NULL AND BRShortName IS NULL) AND (BRState = @Original_BRState OR" & _
        " @Original_BRState IS NULL AND BRState IS NULL) AND (BRStreetAddress01 = @Origin" & _
        "al_BRStreetAddress01 OR @Original_BRStreetAddress01 IS NULL AND BRStreetAddress0" & _
        "1 IS NULL) AND (BRStreetAddress02 = @Original_BRStreetAddress02 OR @Original_BRS" & _
        "treetAddress02 IS NULL AND BRStreetAddress02 IS NULL) AND (BRSuburb = @Original_" & _
        "BRSuburb OR @Original_BRSuburb IS NULL AND BRSuburb IS NULL) AND (BRSuperannuati" & _
        "on = @Original_BRSuperannuation OR @Original_BRSuperannuation IS NULL AND BRSupe" & _
        "rannuation IS NULL) AND (NextID = @Original_NextID); SELECT BRID, NextID, BRStre" & _
        "etAddress01, BRStreetAddress02, BRSuburb, BRState, BRPostCode, BRPhoneNumber, BR" & _
        "FaxNumber, BRBusinessName, BRMobileNumber, BRShortName, BREmail, BRGST, BRSupera" & _
        "nnuation, BRIncludeInReporting FROM Branches WHERE (BRID = @BRID)"
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NextID", System.Data.SqlDbType.BigInt, 8, "NextID"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRStreetAddress01", System.Data.SqlDbType.VarChar, 100, "BRStreetAddress01"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRStreetAddress02", System.Data.SqlDbType.VarChar, 100, "BRStreetAddress02"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRSuburb", System.Data.SqlDbType.VarChar, 50, "BRSuburb"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRState", System.Data.SqlDbType.VarChar, 3, "BRState"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRPostCode", System.Data.SqlDbType.VarChar, 20, "BRPostCode"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRPhoneNumber", System.Data.SqlDbType.VarChar, 20, "BRPhoneNumber"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRFaxNumber", System.Data.SqlDbType.VarChar, 20, "BRFaxNumber"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRBusinessName", System.Data.SqlDbType.VarChar, 100, "BRBusinessName"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRMobileNumber", System.Data.SqlDbType.VarChar, 20, "BRMobileNumber"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRShortName", System.Data.SqlDbType.VarChar, 50, "BRShortName"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BREmail", System.Data.SqlDbType.VarChar, 50, "BREmail"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRGST", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRGST", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRSuperannuation", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRSuperannuation", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRIncludeInReporting", System.Data.SqlDbType.Bit, 1, "BRIncludeInReporting"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRBusinessName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRBusinessName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BREmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BREmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRGST", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRGST", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRIncludeInReporting", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRIncludeInReporting", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRShortName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRShortName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRSuperannuation", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRSuperannuation", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NextID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NextID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'DsHeadOffice
        '
        Me.DsHeadOffice.DataSetName = "dsHeadOffice"
        Me.DsHeadOffice.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'txtBRID
        '
        Me.txtBRID.Location = New System.Drawing.Point(72, 168)
        Me.txtBRID.Name = "txtBRID"
        '
        'txtBRID.Properties
        '
        Me.txtBRID.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtBRID.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("BRShortName", "", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.txtBRID.Properties.DataSource = Me.DsHeadOffice.Branches
        Me.txtBRID.Properties.DisplayMember = "BRShortName"
        Me.txtBRID.Properties.NullText = "--- No branch selected ---"
        Me.txtBRID.Properties.ShowFooter = False
        Me.txtBRID.Properties.ShowHeader = False
        Me.txtBRID.Properties.ShowLines = False
        Me.txtBRID.Properties.ValueMember = "BRID"
        Me.txtBRID.Size = New System.Drawing.Size(256, 20)
        Me.txtBRID.TabIndex = 5
        '
        'DefaultLookAndFeel1
        '
        Me.DefaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D
        Me.DefaultLookAndFeel1.LookAndFeel.UseWindowsXPTheme = True
        '
        'frmLogin
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(338, 248)
        Me.Controls.Add(Me.txtBRID)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.chkUSIsHeadOffice)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.txtUsername)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.SimpleButton1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLogin"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Login"
        CType(Me.txtUsername.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPassword.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkUSIsHeadOffice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsHeadOffice, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBRID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If Not SplashScreen Is Nothing Then
            SplashScreen.Show()
        End If
        If CBool(chkUSIsHeadOffice.EditValue) And Not (txtBRID.EditValue Is DBNull.Value) Then
            txtBRID.EditValue = DBNull.Value
        End If

        SaveSetting(Application.ProductName, "Settings", "Username", txtUsername.EditValue)
        SaveSetting(Application.ProductName, "Settings", "IsHeadOffice", CBool(chkUSIsHeadOffice.EditValue))
        If txtBRID.EditValue Is DBNull.Value Then
            SaveSetting(Application.ProductName, "Settings", "BRID", 0)
        Else
            SaveSetting(Application.ProductName, "Settings", "BRID", txtBRID.EditValue)
        End If
    End Sub

    Private Sub frmLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtUsername.EditValue = GetSetting(Application.ProductName, "Settings", "Username", "")
        chkUSIsHeadOffice.EditValue = CBool(GetSetting(Application.ProductName, "Settings", "IsHeadOffice", "False"))
        Dim TempBRID As Integer = GetSetting(Application.ProductName, "Settings", "BRID", 0)
        If TempBRID = 0 Then
            txtBRID.EditValue = DBNull.Value
        Else
            txtBRID.EditValue = TempBRID
        End If
        If txtUsername.EditValue = "" Then
            txtUsername.TabIndex = 0
        End If
    End Sub

    Private Sub chkUSIsHeadOffice_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUSIsHeadOffice.CheckedChanged
        If CBool(chkUSIsHeadOffice.EditValue) Then
            txtBRID.Enabled = False
        Else
            txtBRID.Enabled = True
        End If
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        ShowHelp(Me)
    End Sub

End Class

