Public Class frmExpenseAllowances
    Inherits DevExpress.XtraEditors.XtraForm

    Public BRID As Int32
    Public EXID As Int32
    Public EAType As String
    Private ShowHours As Boolean

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return SqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            SqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private hTransaction As SqlClient.SqlTransaction
    Private Property Transaction() As SqlClient.SqlTransaction
        Get
            Return hTransaction
        End Get
        Set(ByVal Value As SqlClient.SqlTransaction)
            hTransaction = Value
            Power.Library.Library.ApplyTransactionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Public Shared Function Edit(ByVal BRID As Int32, ByRef EXID As Int64, ByVal EAType As String, _
        ByVal EXName As String, ByVal PaymentTypeDisplay As String) As frmExpenseAllowances
        Dim gui As New frmExpenseAllowances

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            .BRID = BRID
            .EXID = EXID
            .EAType = EAType

            .Transaction = .SqlConnection.BeginTransaction(IsolationLevel.ReadUncommitted)
            .FillPreliminaryData(BRID)
            .CustomiseScreen()

            If DataAccess.spExecLockRequest("sp_GetExpenseLock", .BRID, .EXID, .Transaction) Then

                .FillData()

                .Text = PaymentTypeDisplay & " for " & EXName
                .tpExpenseProperties.Text = PaymentTypeDisplay

            Else
                Throw New ObjectLockedException
            End If
        End With

        Return gui
    End Function

#Region " Windows Form Designer generated code "

    ' FOR EDIT
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents DsGTMS As WindowsApplication.dsGTMS
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents daAllowances As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents dvAllowances As System.Data.DataView
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents dgAllowances As DevExpress.XtraGrid.GridControl
    Friend WithEvents tpExpenseProperties As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents gvExpenseAllowance As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colALFromDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colALAllowance As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnRemoveAllowance As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtCurrencyGrid As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents txtDate As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents btnHelp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colALHours As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents txtALHours As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmExpenseAllowances))
        Me.dvAllowances = New System.Data.DataView
        Me.DsGTMS = New WindowsApplication.dsGTMS
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.daAllowances = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.tpExpenseProperties = New DevExpress.XtraTab.XtraTabControl
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage
        Me.dgAllowances = New DevExpress.XtraGrid.GridControl
        Me.gvExpenseAllowance = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colALFromDate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtDate = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.colALAllowance = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtCurrencyGrid = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colALHours = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtALHours = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.btnRemoveAllowance = New DevExpress.XtraEditors.SimpleButton
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.btnHelp = New DevExpress.XtraEditors.SimpleButton
        CType(Me.dvAllowances, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tpExpenseProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpExpenseProperties.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.dgAllowances, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvExpenseAllowance, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCurrencyGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtALHours, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dvAllowances
        '
        Me.dvAllowances.Table = Me.DsGTMS.ExpenseAllowances
        '
        'DsGTMS
        '
        Me.DsGTMS.DataSetName = "dsGTMS"
        Me.DsGTMS.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;user id=sa;data source=""SERVER\DEV"";persist " & _
        "security info=False;initial catalog=GTMS_DEV"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand2
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand2
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VExpenses_Display", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("EXName", "EXName"), New System.Data.Common.DataColumnMapping("PMAllowancesPaymentTypeDisplay", "PMAllowancesPaymentTypeDisplay")})})
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO VExpenses_Display(BRID, EXName, PMAllowancesPaymentTypeDisplay) VALUE" & _
        "S (@BRID, @EXName, @PMAllowancesPaymentTypeDisplay); SELECT BRID, EXID, EXName, " & _
        "PMAllowancesPaymentTypeDisplay FROM VExpenses_Display"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMAllowancesPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, "PMAllowancesPaymentTypeDisplay"))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT BRID, EXID, EXName, PMAllowancesPaymentTypeDisplay FROM VExpenses_Display " & _
        "WHERE (BRID = @BRID) AND (EXID = @EXID)"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection
        Me.SqlSelectCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        '
        'daAllowances
        '
        Me.daAllowances.DeleteCommand = Me.SqlDeleteCommand1
        Me.daAllowances.InsertCommand = Me.SqlInsertCommand1
        Me.daAllowances.SelectCommand = Me.SqlSelectCommand1
        Me.daAllowances.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "ExpenseAllowances", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("ALFromDate", "ALFromDate"), New System.Data.Common.DataColumnMapping("ALAllowance", "ALAllowance"), New System.Data.Common.DataColumnMapping("ALHours", "ALHours")})})
        Me.daAllowances.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM ExpenseAllowances WHERE (ALFromDate = @Original_ALFromDate) AND (BRID" & _
        " = @Original_BRID) AND (EXID = @Original_EXID) AND (ALAllowance = @Original_ALAl" & _
        "lowance) AND (ALHours = @Original_ALHours OR @Original_ALHours IS NULL AND ALHou" & _
        "rs IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ALFromDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ALFromDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ALAllowance", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ALAllowance", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ALHours", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(10, Byte), CType(2, Byte), "ALHours", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO ExpenseAllowances(BRID, EXID, ALFromDate, ALAllowance, ALHours) VALUE" & _
        "S (@BRID, @EXID, @ALFromDate, @ALAllowance, @ALHours); SELECT BRID, EXID, ALFrom" & _
        "Date, ALAllowance, ALHours FROM ExpenseAllowances WHERE (ALFromDate = @ALFromDat" & _
        "e) AND (BRID = @BRID) AND (EXID = @EXID)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ALFromDate", System.Data.SqlDbType.DateTime, 8, "ALFromDate"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ALAllowance", System.Data.SqlDbType.Money, 8, "ALAllowance"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ALHours", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(10, Byte), CType(2, Byte), "ALHours", System.Data.DataRowVersion.Current, Nothing))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, EXID, ALFromDate, ALAllowance, ALHours FROM ExpenseAllowances WHERE " & _
        "(BRID = @BRID) AND (EXID = @EXID)"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE ExpenseAllowances SET BRID = @BRID, EXID = @EXID, ALFromDate = @ALFromDate" & _
        ", ALAllowance = @ALAllowance, ALHours = @ALHours WHERE (ALFromDate = @Original_A" & _
        "LFromDate) AND (BRID = @Original_BRID) AND (EXID = @Original_EXID) AND (ALAllowa" & _
        "nce = @Original_ALAllowance) AND (ALHours = @Original_ALHours OR @Original_ALHou" & _
        "rs IS NULL AND ALHours IS NULL); SELECT BRID, EXID, ALFromDate, ALAllowance, ALH" & _
        "ours FROM ExpenseAllowances WHERE (ALFromDate = @ALFromDate) AND (BRID = @BRID) " & _
        "AND (EXID = @EXID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ALFromDate", System.Data.SqlDbType.DateTime, 8, "ALFromDate"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ALAllowance", System.Data.SqlDbType.Money, 8, "ALAllowance"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ALHours", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(10, Byte), CType(2, Byte), "ALHours", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ALFromDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ALFromDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ALAllowance", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ALAllowance", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ALHours", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(10, Byte), CType(2, Byte), "ALHours", System.Data.DataRowVersion.Original, Nothing))
        '
        'tpExpenseProperties
        '
        Me.tpExpenseProperties.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tpExpenseProperties.Location = New System.Drawing.Point(8, 8)
        Me.tpExpenseProperties.Name = "tpExpenseProperties"
        Me.tpExpenseProperties.SelectedTabPage = Me.XtraTabPage1
        Me.tpExpenseProperties.Size = New System.Drawing.Size(416, 400)
        Me.tpExpenseProperties.TabIndex = 0
        Me.tpExpenseProperties.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1})
        Me.tpExpenseProperties.Text = "XtraTabControl1"
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.dgAllowances)
        Me.XtraTabPage1.Controls.Add(Me.btnRemoveAllowance)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(407, 370)
        Me.XtraTabPage1.Text = "Expense Properties"
        '
        'dgAllowances
        '
        Me.dgAllowances.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgAllowances.DataSource = Me.DsGTMS.ExpenseAllowances
        '
        'dgAllowances.EmbeddedNavigator
        '
        Me.dgAllowances.EmbeddedNavigator.Name = ""
        Me.dgAllowances.Location = New System.Drawing.Point(8, 16)
        Me.dgAllowances.MainView = Me.gvExpenseAllowance
        Me.dgAllowances.Name = "dgAllowances"
        Me.dgAllowances.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtCurrencyGrid, Me.txtDate, Me.txtALHours})
        Me.dgAllowances.Size = New System.Drawing.Size(389, 316)
        Me.dgAllowances.TabIndex = 0
        Me.dgAllowances.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvExpenseAllowance})
        '
        'gvExpenseAllowance
        '
        Me.gvExpenseAllowance.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvExpenseAllowance.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvExpenseAllowance.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvExpenseAllowance.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvExpenseAllowance.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvExpenseAllowance.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvExpenseAllowance.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvExpenseAllowance.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvExpenseAllowance.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvExpenseAllowance.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvExpenseAllowance.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvExpenseAllowance.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvExpenseAllowance.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvExpenseAllowance.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvExpenseAllowance.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvExpenseAllowance.Appearance.VertLine.Options.UseBackColor = True
        Me.gvExpenseAllowance.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colALFromDate, Me.colALAllowance, Me.colALHours})
        Me.gvExpenseAllowance.GridControl = Me.dgAllowances
        Me.gvExpenseAllowance.Name = "gvExpenseAllowance"
        Me.gvExpenseAllowance.OptionsCustomization.AllowFilter = False
        Me.gvExpenseAllowance.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvExpenseAllowance.OptionsView.ShowGroupPanel = False
        Me.gvExpenseAllowance.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colALFromDate, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colALFromDate
        '
        Me.colALFromDate.Caption = "From Date"
        Me.colALFromDate.ColumnEdit = Me.txtDate
        Me.colALFromDate.FieldName = "ALFromDate"
        Me.colALFromDate.Name = "colALFromDate"
        Me.colALFromDate.Visible = True
        Me.colALFromDate.VisibleIndex = 0
        Me.colALFromDate.Width = 104
        '
        'txtDate
        '
        Me.txtDate.AutoHeight = False
        Me.txtDate.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtDate.Name = "txtDate"
        '
        'colALAllowance
        '
        Me.colALAllowance.Caption = "Amount Per Annum"
        Me.colALAllowance.ColumnEdit = Me.txtCurrencyGrid
        Me.colALAllowance.DisplayFormat.FormatString = "c"
        Me.colALAllowance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colALAllowance.FieldName = "ALAllowance"
        Me.colALAllowance.Name = "colALAllowance"
        Me.colALAllowance.Visible = True
        Me.colALAllowance.VisibleIndex = 1
        Me.colALAllowance.Width = 98
        '
        'txtCurrencyGrid
        '
        Me.txtCurrencyGrid.AutoHeight = False
        Me.txtCurrencyGrid.DisplayFormat.FormatString = "c"
        Me.txtCurrencyGrid.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCurrencyGrid.EditFormat.FormatString = "c"
        Me.txtCurrencyGrid.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCurrencyGrid.Name = "txtCurrencyGrid"
        '
        'colALHours
        '
        Me.colALHours.Caption = "Hours Per Annum"
        Me.colALHours.ColumnEdit = Me.txtALHours
        Me.colALHours.DisplayFormat.FormatString = "#.00"
        Me.colALHours.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colALHours.FieldName = "ALHours"
        Me.colALHours.Name = "colALHours"
        Me.colALHours.Visible = True
        Me.colALHours.VisibleIndex = 2
        '
        'txtALHours
        '
        Me.txtALHours.AutoHeight = False
        Me.txtALHours.DisplayFormat.FormatString = "#.00"
        Me.txtALHours.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtALHours.EditFormat.FormatString = "#.00"
        Me.txtALHours.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtALHours.Name = "txtALHours"
        '
        'btnRemoveAllowance
        '
        Me.btnRemoveAllowance.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveAllowance.Image = CType(resources.GetObject("btnRemoveAllowance.Image"), System.Drawing.Image)
        Me.btnRemoveAllowance.Location = New System.Drawing.Point(8, 340)
        Me.btnRemoveAllowance.Name = "btnRemoveAllowance"
        Me.btnRemoveAllowance.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveAllowance.TabIndex = 1
        Me.btnRemoveAllowance.Text = "Remove"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(272, 416)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(352, 416)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'btnHelp
        '
        Me.btnHelp.Image = CType(resources.GetObject("btnHelp.Image"), System.Drawing.Image)
        Me.btnHelp.Location = New System.Drawing.Point(8, 416)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(72, 23)
        Me.btnHelp.TabIndex = 1
        Me.btnHelp.Text = "Help"
        '
        'frmExpenseAllowances
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(434, 448)
        Me.Controls.Add(Me.btnHelp)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.tpExpenseProperties)
        Me.Controls.Add(Me.btnCancel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExpenseAllowances"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Expense"
        CType(Me.dvAllowances, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tpExpenseProperties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpExpenseProperties.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.dgAllowances, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvExpenseAllowance, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCurrencyGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtALHours, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnOK.Enabled = Not HasRole("branch_read_only")
    End Sub

    Public ReadOnly Property SelectedExpenseAllowance() As DataRow
        Get
            If Not gvExpenseAllowance.GetSelectedRows Is Nothing Then
                Return gvExpenseAllowance.GetDataRow(gvExpenseAllowance.GetSelectedRows(0))
            End If
        End Get
    End Property
    ' This data must be loaded BEFORE the DataRow is loaded (eg lookups etc)
    Private Sub FillPreliminaryData(ByVal BRID As Integer)
        ' --- SHOW HOURS ---
        ShowHours = DataAccess.ShowHours(BRID, Transaction)
    End Sub

    ' This data must be loaded AFTER the DataRow is loaded (eg record related data)
    Private Sub FillData()
        ' Allowances
        daAllowances.SelectCommand.Parameters("@BRID").Value = BRID
        daAllowances.SelectCommand.Parameters("@EXID").Value = EXID
        DsGTMS.ExpenseAllowances.BRIDColumn.DefaultValue = BRID
        DsGTMS.ExpenseAllowances.EXIDColumn.DefaultValue = EXID
        daAllowances.Fill(DsGTMS)
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        daAllowances.Update(DsGTMS)

        DataAccess.spExecLockRequest("sp_ReleaseExpenseLock", BRID, EXID, Transaction)
        Transaction.Commit()
        SqlConnection.Close()

        OK = True
        Me.Close()
    End Sub

    Private HasChanges As Boolean = False
    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            If HasChanges Or DsGTMS.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If response = MsgBoxResult.Yes Then
                DataAccess.spExecLockRequest("sp_ReleaseExpenseLock", BRID, EXID, Transaction)
                Transaction.Rollback()
                SqlConnection.Close()
            Else
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub CustomiseScreen()
        colALHours.Visible = ShowHours
    End Sub

    Private Sub btnRemoveAllowance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveAllowance.Click
        If Not SelectedExpenseAllowance Is Nothing Then
            SelectedExpenseAllowance.Delete()
            daAllowances.Update(DsGTMS)
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub gvClientPayments_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gvExpenseAllowance.InvalidRowException
        GridView_InvalidRowException(sender, e)
    End Sub

    Private Sub txtCurrency_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) _
    Handles txtCurrencyGrid.ParseEditValue, txtALHours.ParseEditValue
        Format.Decimal_ParseEditValue(sender, e)
    End Sub

    Private Sub Date_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtDate.Validating
        Library.DateEdit_Validating(sender, e)
    End Sub

    Private Sub btnHelp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHelp.Click
        ShowHelpTopic(Me, "AmountsSpecifiedAnnuallyTerms.html")
    End Sub

End Class
