Public Class frmBranch2
    Inherits DevExpress.XtraEditors.XtraForm

    Private DataRow As DataRow

    Public Shared Sub Add(ByVal Roles As ArrayList)
        Dim gui As New frmBranch2(Roles)

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            .FillPreliminaryData()

            .DataRow = .dataSet.Branches.NewRow()
            .dataSet.Branches.Rows.Add(.DataRow)

            .FillInitData()

            gui.ShowDialog()

            .SqlConnection.Close()
        End With
    End Sub

    Public Shared Sub Edit(ByVal BRID As Int32, ByVal Roles As ArrayList)
        Dim gui As New frmBranch2(Roles)

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            If DataAccess.spExecLockRequest_All("sp_GetBranchLock", BRID, .SqlConnection) Then

                .FillPreliminaryData()

                .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = BRID
                .SqlDataAdapter.Fill(.dataSet)
                .DataRow = .dataSet.Branches(0)

                .FillData()

                gui.ShowDialog()

                DataAccess.spExecLockRequest_All("sp_ReleaseBranchLock", BRID, .SqlConnection)
            Else
                Message.CurrentlyAccessed("branch")
            End If

            .SqlConnection.Close()
        End With
    End Sub

#Region " Windows Form Designer generated code "

    ' FOR NEW
    Public Sub New(ByVal Roles As ArrayList)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        chkBRIncludeInReporting.Enabled = HasRole("branches")
        txtBRShortName.Enabled = HasRole("branches")
        If Not HasRole("management") Then
            Me.XtraTabControl1.TabPages.Remove(tpFinancialSettings)
        End If
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtLDClientStreetAddress01 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientStreetAddress02 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientSuburb As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientState As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientPostCode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtLDClientEmail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtLDClientPhoneNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientFaxNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientMobileNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtBRGST As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtBRSuperannuation As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tpBranchProperties As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tpFinancialSettings As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents lblBRShortName As System.Windows.Forms.Label
    Friend WithEvents txtBRShortName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tpBranchSettings As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents chkBRIncludeInReporting As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupControl6 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents dataSet As WindowsApplication.dsBranches
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmBranch2))
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl
        Me.tpBranchProperties = New DevExpress.XtraTab.XtraTabPage
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl
        Me.txtLDClientPhoneNumber = New DevExpress.XtraEditors.TextEdit
        Me.dataSet = New WindowsApplication.dsBranches
        Me.txtLDClientFaxNumber = New DevExpress.XtraEditors.TextEdit
        Me.Label17 = New System.Windows.Forms.Label
        Me.txtLDClientEmail = New DevExpress.XtraEditors.TextEdit
        Me.txtLDClientMobileNumber = New DevExpress.XtraEditors.TextEdit
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl
        Me.txtLDClientStreetAddress02 = New DevExpress.XtraEditors.TextEdit
        Me.txtLDClientStreetAddress01 = New DevExpress.XtraEditors.TextEdit
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtLDClientPostCode = New DevExpress.XtraEditors.TextEdit
        Me.txtLDClientState = New DevExpress.XtraEditors.TextEdit
        Me.txtLDClientSuburb = New DevExpress.XtraEditors.TextEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit
        Me.lblBRShortName = New System.Windows.Forms.Label
        Me.txtBRShortName = New DevExpress.XtraEditors.TextEdit
        Me.tpBranchSettings = New DevExpress.XtraTab.XtraTabPage
        Me.GroupControl6 = New DevExpress.XtraEditors.GroupControl
        Me.Label7 = New System.Windows.Forms.Label
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl
        Me.Label1 = New System.Windows.Forms.Label
        Me.chkBRIncludeInReporting = New DevExpress.XtraEditors.CheckEdit
        Me.tpFinancialSettings = New DevExpress.XtraTab.XtraTabPage
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtBRSuperannuation = New DevExpress.XtraEditors.TextEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl
        Me.txtBRGST = New DevExpress.XtraEditors.TextEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.tpBranchProperties.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.txtLDClientPhoneNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientFaxNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientMobileNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtLDClientStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientPostCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientState.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientSuburb.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBRShortName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpBranchSettings.SuspendLayout()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.chkBRIncludeInReporting.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpFinancialSettings.SuspendLayout()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.txtBRSuperannuation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.txtBRGST.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;user id=sa;data source=""SERVER\DEV"";persist " & _
        "security info=False;initial catalog=GTMS_DEV"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Branches", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("BRStreetAddress01", "BRStreetAddress01"), New System.Data.Common.DataColumnMapping("BRStreetAddress02", "BRStreetAddress02"), New System.Data.Common.DataColumnMapping("BRSuburb", "BRSuburb"), New System.Data.Common.DataColumnMapping("BRState", "BRState"), New System.Data.Common.DataColumnMapping("BRPostCode", "BRPostCode"), New System.Data.Common.DataColumnMapping("BRPhoneNumber", "BRPhoneNumber"), New System.Data.Common.DataColumnMapping("BRFaxNumber", "BRFaxNumber"), New System.Data.Common.DataColumnMapping("BRMobileNumber", "BRMobileNumber"), New System.Data.Common.DataColumnMapping("BRBusinessName", "BRBusinessName"), New System.Data.Common.DataColumnMapping("BRShortName", "BRShortName"), New System.Data.Common.DataColumnMapping("BREmail", "BREmail"), New System.Data.Common.DataColumnMapping("BRGST", "BRGST"), New System.Data.Common.DataColumnMapping("BRSuperannuation", "BRSuperannuation"), New System.Data.Common.DataColumnMapping("BRIncludeInReporting", "BRIncludeInReporting"), New System.Data.Common.DataColumnMapping("BRShowHours", "BRShowHours")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Branches WHERE (BRID = @Original_BRID) AND (BRBusinessName = @Origina" & _
        "l_BRBusinessName OR @Original_BRBusinessName IS NULL AND BRBusinessName IS NULL)" & _
        " AND (BREmail = @Original_BREmail OR @Original_BREmail IS NULL AND BREmail IS NU" & _
        "LL) AND (BRFaxNumber = @Original_BRFaxNumber OR @Original_BRFaxNumber IS NULL AN" & _
        "D BRFaxNumber IS NULL) AND (BRGST = @Original_BRGST OR @Original_BRGST IS NULL A" & _
        "ND BRGST IS NULL) AND (BRIncludeInReporting = @Original_BRIncludeInReporting OR " & _
        "@Original_BRIncludeInReporting IS NULL AND BRIncludeInReporting IS NULL) AND (BR" & _
        "MobileNumber = @Original_BRMobileNumber OR @Original_BRMobileNumber IS NULL AND " & _
        "BRMobileNumber IS NULL) AND (BRPhoneNumber = @Original_BRPhoneNumber OR @Origina" & _
        "l_BRPhoneNumber IS NULL AND BRPhoneNumber IS NULL) AND (BRPostCode = @Original_B" & _
        "RPostCode OR @Original_BRPostCode IS NULL AND BRPostCode IS NULL) AND (BRShortNa" & _
        "me = @Original_BRShortName OR @Original_BRShortName IS NULL AND BRShortName IS N" & _
        "ULL) AND (BRShowHours = @Original_BRShowHours OR @Original_BRShowHours IS NULL A" & _
        "ND BRShowHours IS NULL) AND (BRState = @Original_BRState OR @Original_BRState IS" & _
        " NULL AND BRState IS NULL) AND (BRStreetAddress01 = @Original_BRStreetAddress01 " & _
        "OR @Original_BRStreetAddress01 IS NULL AND BRStreetAddress01 IS NULL) AND (BRStr" & _
        "eetAddress02 = @Original_BRStreetAddress02 OR @Original_BRStreetAddress02 IS NUL" & _
        "L AND BRStreetAddress02 IS NULL) AND (BRSuburb = @Original_BRSuburb OR @Original" & _
        "_BRSuburb IS NULL AND BRSuburb IS NULL) AND (BRSuperannuation = @Original_BRSupe" & _
        "rannuation OR @Original_BRSuperannuation IS NULL AND BRSuperannuation IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRBusinessName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRBusinessName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BREmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BREmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRGST", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRGST", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRIncludeInReporting", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRIncludeInReporting", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRShortName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRShortName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRShowHours", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRShowHours", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRSuperannuation", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRSuperannuation", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Branches(BRStreetAddress01, BRStreetAddress02, BRSuburb, BRState, BRP" & _
        "ostCode, BRPhoneNumber, BRFaxNumber, BRMobileNumber, BRBusinessName, BRShortName" & _
        ", BREmail, BRGST, BRSuperannuation, BRIncludeInReporting, BRShowHours) VALUES (@" & _
        "BRStreetAddress01, @BRStreetAddress02, @BRSuburb, @BRState, @BRPostCode, @BRPhon" & _
        "eNumber, @BRFaxNumber, @BRMobileNumber, @BRBusinessName, @BRShortName, @BREmail," & _
        " @BRGST, @BRSuperannuation, @BRIncludeInReporting, @BRShowHours); SELECT BRID, B" & _
        "RStreetAddress01, BRStreetAddress02, BRSuburb, BRState, BRPostCode, BRPhoneNumbe" & _
        "r, BRFaxNumber, BRMobileNumber, BRBusinessName, BRShortName, BREmail, BRGST, BRS" & _
        "uperannuation, BRIncludeInReporting, BRShowHours FROM Branches WHERE (BRID = @@I" & _
        "DENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRStreetAddress01", System.Data.SqlDbType.VarChar, 100, "BRStreetAddress01"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRStreetAddress02", System.Data.SqlDbType.VarChar, 100, "BRStreetAddress02"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRSuburb", System.Data.SqlDbType.VarChar, 50, "BRSuburb"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRState", System.Data.SqlDbType.VarChar, 3, "BRState"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRPostCode", System.Data.SqlDbType.VarChar, 20, "BRPostCode"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRPhoneNumber", System.Data.SqlDbType.VarChar, 20, "BRPhoneNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRFaxNumber", System.Data.SqlDbType.VarChar, 20, "BRFaxNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRMobileNumber", System.Data.SqlDbType.VarChar, 20, "BRMobileNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRBusinessName", System.Data.SqlDbType.VarChar, 100, "BRBusinessName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRShortName", System.Data.SqlDbType.VarChar, 50, "BRShortName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BREmail", System.Data.SqlDbType.VarChar, 50, "BREmail"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRGST", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRGST", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRSuperannuation", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRSuperannuation", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRIncludeInReporting", System.Data.SqlDbType.Bit, 1, "BRIncludeInReporting"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRShowHours", System.Data.SqlDbType.Bit, 1, "BRShowHours"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, BRStreetAddress01, BRStreetAddress02, BRSuburb, BRState, BRPostCode," & _
        " BRPhoneNumber, BRFaxNumber, BRMobileNumber, BRBusinessName, BRShortName, BREmai" & _
        "l, BRGST, BRSuperannuation, BRIncludeInReporting, BRShowHours FROM Branches WHER" & _
        "E (BRID = @BRID)"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Branches SET BRStreetAddress01 = @BRStreetAddress01, BRStreetAddress02 = @" & _
        "BRStreetAddress02, BRSuburb = @BRSuburb, BRState = @BRState, BRPostCode = @BRPos" & _
        "tCode, BRPhoneNumber = @BRPhoneNumber, BRFaxNumber = @BRFaxNumber, BRMobileNumbe" & _
        "r = @BRMobileNumber, BRBusinessName = @BRBusinessName, BRShortName = @BRShortNam" & _
        "e, BREmail = @BREmail, BRGST = @BRGST, BRSuperannuation = @BRSuperannuation, BRI" & _
        "ncludeInReporting = @BRIncludeInReporting, BRShowHours = @BRShowHours WHERE (BRI" & _
        "D = @Original_BRID) AND (BRBusinessName = @Original_BRBusinessName OR @Original_" & _
        "BRBusinessName IS NULL AND BRBusinessName IS NULL) AND (BREmail = @Original_BREm" & _
        "ail OR @Original_BREmail IS NULL AND BREmail IS NULL) AND (BRFaxNumber = @Origin" & _
        "al_BRFaxNumber OR @Original_BRFaxNumber IS NULL AND BRFaxNumber IS NULL) AND (BR" & _
        "GST = @Original_BRGST OR @Original_BRGST IS NULL AND BRGST IS NULL) AND (BRInclu" & _
        "deInReporting = @Original_BRIncludeInReporting OR @Original_BRIncludeInReporting" & _
        " IS NULL AND BRIncludeInReporting IS NULL) AND (BRMobileNumber = @Original_BRMob" & _
        "ileNumber OR @Original_BRMobileNumber IS NULL AND BRMobileNumber IS NULL) AND (B" & _
        "RPhoneNumber = @Original_BRPhoneNumber OR @Original_BRPhoneNumber IS NULL AND BR" & _
        "PhoneNumber IS NULL) AND (BRPostCode = @Original_BRPostCode OR @Original_BRPostC" & _
        "ode IS NULL AND BRPostCode IS NULL) AND (BRShortName = @Original_BRShortName OR " & _
        "@Original_BRShortName IS NULL AND BRShortName IS NULL) AND (BRShowHours = @Origi" & _
        "nal_BRShowHours OR @Original_BRShowHours IS NULL AND BRShowHours IS NULL) AND (B" & _
        "RState = @Original_BRState OR @Original_BRState IS NULL AND BRState IS NULL) AND" & _
        " (BRStreetAddress01 = @Original_BRStreetAddress01 OR @Original_BRStreetAddress01" & _
        " IS NULL AND BRStreetAddress01 IS NULL) AND (BRStreetAddress02 = @Original_BRStr" & _
        "eetAddress02 OR @Original_BRStreetAddress02 IS NULL AND BRStreetAddress02 IS NUL" & _
        "L) AND (BRSuburb = @Original_BRSuburb OR @Original_BRSuburb IS NULL AND BRSuburb" & _
        " IS NULL) AND (BRSuperannuation = @Original_BRSuperannuation OR @Original_BRSupe" & _
        "rannuation IS NULL AND BRSuperannuation IS NULL); SELECT BRID, BRStreetAddress01" & _
        ", BRStreetAddress02, BRSuburb, BRState, BRPostCode, BRPhoneNumber, BRFaxNumber, " & _
        "BRMobileNumber, BRBusinessName, BRShortName, BREmail, BRGST, BRSuperannuation, B" & _
        "RIncludeInReporting, BRShowHours FROM Branches WHERE (BRID = @BRID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRStreetAddress01", System.Data.SqlDbType.VarChar, 100, "BRStreetAddress01"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRStreetAddress02", System.Data.SqlDbType.VarChar, 100, "BRStreetAddress02"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRSuburb", System.Data.SqlDbType.VarChar, 50, "BRSuburb"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRState", System.Data.SqlDbType.VarChar, 3, "BRState"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRPostCode", System.Data.SqlDbType.VarChar, 20, "BRPostCode"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRPhoneNumber", System.Data.SqlDbType.VarChar, 20, "BRPhoneNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRFaxNumber", System.Data.SqlDbType.VarChar, 20, "BRFaxNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRMobileNumber", System.Data.SqlDbType.VarChar, 20, "BRMobileNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRBusinessName", System.Data.SqlDbType.VarChar, 100, "BRBusinessName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRShortName", System.Data.SqlDbType.VarChar, 50, "BRShortName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BREmail", System.Data.SqlDbType.VarChar, 50, "BREmail"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRGST", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRGST", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRSuperannuation", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRSuperannuation", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRIncludeInReporting", System.Data.SqlDbType.Bit, 1, "BRIncludeInReporting"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRShowHours", System.Data.SqlDbType.Bit, 1, "BRShowHours"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRBusinessName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRBusinessName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BREmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BREmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRGST", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRGST", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRIncludeInReporting", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRIncludeInReporting", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRShortName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRShortName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRShowHours", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRShowHours", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRSuperannuation", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRSuperannuation", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.XtraTabControl1.Location = New System.Drawing.Point(8, 8)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.tpBranchProperties
        Me.XtraTabControl1.Size = New System.Drawing.Size(496, 440)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tpBranchProperties, Me.tpBranchSettings, Me.tpFinancialSettings})
        Me.XtraTabControl1.Text = "XtraTabControl1"
        '
        'tpBranchProperties
        '
        Me.tpBranchProperties.Controls.Add(Me.GroupControl2)
        Me.tpBranchProperties.Controls.Add(Me.GroupControl1)
        Me.tpBranchProperties.Controls.Add(Me.Label2)
        Me.tpBranchProperties.Controls.Add(Me.TextEdit2)
        Me.tpBranchProperties.Controls.Add(Me.lblBRShortName)
        Me.tpBranchProperties.Controls.Add(Me.txtBRShortName)
        Me.tpBranchProperties.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tpBranchProperties.Name = "tpBranchProperties"
        Me.tpBranchProperties.Size = New System.Drawing.Size(487, 410)
        Me.tpBranchProperties.Text = "Branch Details"
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.txtLDClientPhoneNumber)
        Me.GroupControl2.Controls.Add(Me.txtLDClientFaxNumber)
        Me.GroupControl2.Controls.Add(Me.Label17)
        Me.GroupControl2.Controls.Add(Me.txtLDClientEmail)
        Me.GroupControl2.Controls.Add(Me.txtLDClientMobileNumber)
        Me.GroupControl2.Controls.Add(Me.Label13)
        Me.GroupControl2.Controls.Add(Me.Label14)
        Me.GroupControl2.Controls.Add(Me.Label15)
        Me.GroupControl2.Location = New System.Drawing.Point(8, 248)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(472, 152)
        Me.GroupControl2.TabIndex = 5
        Me.GroupControl2.Text = "Contact Details"
        '
        'txtLDClientPhoneNumber
        '
        Me.txtLDClientPhoneNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Branches.BRPhoneNumber"))
        Me.txtLDClientPhoneNumber.EditValue = ""
        Me.txtLDClientPhoneNumber.Location = New System.Drawing.Point(160, 24)
        Me.txtLDClientPhoneNumber.Name = "txtLDClientPhoneNumber"
        Me.txtLDClientPhoneNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientPhoneNumber.TabIndex = 1
        '
        'dataSet
        '
        Me.dataSet.DataSetName = "dsBranches"
        Me.dataSet.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'txtLDClientFaxNumber
        '
        Me.txtLDClientFaxNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Branches.BRFaxNumber"))
        Me.txtLDClientFaxNumber.EditValue = ""
        Me.txtLDClientFaxNumber.Location = New System.Drawing.Point(160, 56)
        Me.txtLDClientFaxNumber.Name = "txtLDClientFaxNumber"
        Me.txtLDClientFaxNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientFaxNumber.TabIndex = 3
        '
        'Label17
        '
        Me.Label17.Location = New System.Drawing.Point(16, 120)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(80, 21)
        Me.Label17.TabIndex = 6
        Me.Label17.Text = "Email:"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLDClientEmail
        '
        Me.txtLDClientEmail.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Branches.BREmail"))
        Me.txtLDClientEmail.EditValue = ""
        Me.txtLDClientEmail.Location = New System.Drawing.Point(160, 120)
        Me.txtLDClientEmail.Name = "txtLDClientEmail"
        Me.txtLDClientEmail.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientEmail.TabIndex = 7
        '
        'txtLDClientMobileNumber
        '
        Me.txtLDClientMobileNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Branches.BRMobileNumber"))
        Me.txtLDClientMobileNumber.EditValue = ""
        Me.txtLDClientMobileNumber.Location = New System.Drawing.Point(160, 88)
        Me.txtLDClientMobileNumber.Name = "txtLDClientMobileNumber"
        Me.txtLDClientMobileNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientMobileNumber.TabIndex = 5
        '
        'Label13
        '
        Me.Label13.Location = New System.Drawing.Point(16, 24)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(80, 21)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "Phone:"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.Location = New System.Drawing.Point(16, 56)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(80, 21)
        Me.Label14.TabIndex = 2
        Me.Label14.Text = "Fax:"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label15
        '
        Me.Label15.Location = New System.Drawing.Point(16, 88)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(80, 21)
        Me.Label15.TabIndex = 4
        Me.Label15.Text = "Mobile:"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupControl1
        '
        Me.GroupControl1.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.GroupControl1.Appearance.Options.UseBackColor = True
        Me.GroupControl1.Controls.Add(Me.txtLDClientStreetAddress02)
        Me.GroupControl1.Controls.Add(Me.txtLDClientStreetAddress01)
        Me.GroupControl1.Controls.Add(Me.Label12)
        Me.GroupControl1.Controls.Add(Me.Label11)
        Me.GroupControl1.Controls.Add(Me.Label10)
        Me.GroupControl1.Controls.Add(Me.Label9)
        Me.GroupControl1.Controls.Add(Me.txtLDClientPostCode)
        Me.GroupControl1.Controls.Add(Me.txtLDClientState)
        Me.GroupControl1.Controls.Add(Me.txtLDClientSuburb)
        Me.GroupControl1.Location = New System.Drawing.Point(8, 88)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(472, 152)
        Me.GroupControl1.TabIndex = 4
        Me.GroupControl1.Text = "Address"
        '
        'txtLDClientStreetAddress02
        '
        Me.txtLDClientStreetAddress02.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Branches.BRStreetAddress02"))
        Me.txtLDClientStreetAddress02.EditValue = ""
        Me.txtLDClientStreetAddress02.Location = New System.Drawing.Point(160, 56)
        Me.txtLDClientStreetAddress02.Name = "txtLDClientStreetAddress02"
        Me.txtLDClientStreetAddress02.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientStreetAddress02.TabIndex = 2
        '
        'txtLDClientStreetAddress01
        '
        Me.txtLDClientStreetAddress01.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Branches.BRStreetAddress01"))
        Me.txtLDClientStreetAddress01.EditValue = ""
        Me.txtLDClientStreetAddress01.Location = New System.Drawing.Point(160, 24)
        Me.txtLDClientStreetAddress01.Name = "txtLDClientStreetAddress01"
        Me.txtLDClientStreetAddress01.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientStreetAddress01.TabIndex = 1
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(16, 88)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(80, 21)
        Me.Label12.TabIndex = 3
        Me.Label12.Text = "Suburb/town:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(16, 24)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(80, 21)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Street address:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(272, 120)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(96, 21)
        Me.Label10.TabIndex = 7
        Me.Label10.Text = "ZIP/postal code:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(16, 120)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(80, 21)
        Me.Label9.TabIndex = 5
        Me.Label9.Text = "State/region:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLDClientPostCode
        '
        Me.txtLDClientPostCode.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Branches.BRPostCode"))
        Me.txtLDClientPostCode.EditValue = ""
        Me.txtLDClientPostCode.Location = New System.Drawing.Point(368, 120)
        Me.txtLDClientPostCode.Name = "txtLDClientPostCode"
        Me.txtLDClientPostCode.Size = New System.Drawing.Size(88, 20)
        Me.txtLDClientPostCode.TabIndex = 8
        '
        'txtLDClientState
        '
        Me.txtLDClientState.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Branches.BRState"))
        Me.txtLDClientState.EditValue = ""
        Me.txtLDClientState.Location = New System.Drawing.Point(160, 120)
        Me.txtLDClientState.Name = "txtLDClientState"
        Me.txtLDClientState.Size = New System.Drawing.Size(88, 20)
        Me.txtLDClientState.TabIndex = 6
        '
        'txtLDClientSuburb
        '
        Me.txtLDClientSuburb.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Branches.BRSuburb"))
        Me.txtLDClientSuburb.EditValue = ""
        Me.txtLDClientSuburb.Location = New System.Drawing.Point(160, 88)
        Me.txtLDClientSuburb.Name = "txtLDClientSuburb"
        Me.txtLDClientSuburb.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientSuburb.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(8, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(120, 20)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Full business name:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextEdit2
        '
        Me.TextEdit2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextEdit2.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Branches.BRBusinessName"))
        Me.TextEdit2.EditValue = ""
        Me.TextEdit2.Location = New System.Drawing.Point(128, 48)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Size = New System.Drawing.Size(349, 20)
        Me.TextEdit2.TabIndex = 3
        '
        'lblBRShortName
        '
        Me.lblBRShortName.Location = New System.Drawing.Point(8, 16)
        Me.lblBRShortName.Name = "lblBRShortName"
        Me.lblBRShortName.Size = New System.Drawing.Size(120, 20)
        Me.lblBRShortName.TabIndex = 0
        Me.lblBRShortName.Text = "Branch name (short):"
        Me.lblBRShortName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBRShortName
        '
        Me.txtBRShortName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtBRShortName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Branches.BRShortName"))
        Me.txtBRShortName.EditValue = ""
        Me.txtBRShortName.Location = New System.Drawing.Point(128, 16)
        Me.txtBRShortName.Name = "txtBRShortName"
        Me.txtBRShortName.Size = New System.Drawing.Size(349, 20)
        Me.txtBRShortName.TabIndex = 1
        '
        'tpBranchSettings
        '
        Me.tpBranchSettings.Controls.Add(Me.GroupControl6)
        Me.tpBranchSettings.Controls.Add(Me.GroupControl5)
        Me.tpBranchSettings.Name = "tpBranchSettings"
        Me.tpBranchSettings.Size = New System.Drawing.Size(487, 410)
        Me.tpBranchSettings.Text = "Branch Settings"
        '
        'GroupControl6
        '
        Me.GroupControl6.Controls.Add(Me.Label7)
        Me.GroupControl6.Controls.Add(Me.CheckEdit1)
        Me.GroupControl6.Location = New System.Drawing.Point(8, 128)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.Size = New System.Drawing.Size(472, 88)
        Me.GroupControl6.TabIndex = 1
        Me.GroupControl6.Text = "Measuring Productivity"
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(8, 24)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(456, 32)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "You can measure productivity of your staff by measuring the hours they work.  If " & _
        "you tick this box, the program will allow you to enter the hours each staff memb" & _
        "er works."
        '
        'CheckEdit1
        '
        Me.CheckEdit1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Branches.BRShowHours"))
        Me.CheckEdit1.Location = New System.Drawing.Point(8, 64)
        Me.CheckEdit1.Name = "CheckEdit1"
        '
        'CheckEdit1.Properties
        '
        Me.CheckEdit1.Properties.Caption = "Measure productivity by measuring hours worked"
        Me.CheckEdit1.Size = New System.Drawing.Size(264, 19)
        Me.CheckEdit1.TabIndex = 0
        '
        'GroupControl5
        '
        Me.GroupControl5.Controls.Add(Me.Label1)
        Me.GroupControl5.Controls.Add(Me.chkBRIncludeInReporting)
        Me.GroupControl5.Location = New System.Drawing.Point(8, 8)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(472, 112)
        Me.GroupControl5.TabIndex = 0
        Me.GroupControl5.Text = "Head Office Reporting"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(456, 64)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "This option controls whether this branch is included in head office reporting.  I" & _
        "f this branch has misleading data, this box can be unticked to prevent the group" & _
        " data from being 'contaminated'.  If the data in the branch is correct, you shou" & _
        "ld tick this box to include this branch in group reporting."
        '
        'chkBRIncludeInReporting
        '
        Me.chkBRIncludeInReporting.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Branches.BRIncludeInReporting"))
        Me.chkBRIncludeInReporting.Location = New System.Drawing.Point(8, 88)
        Me.chkBRIncludeInReporting.Name = "chkBRIncludeInReporting"
        '
        'chkBRIncludeInReporting.Properties
        '
        Me.chkBRIncludeInReporting.Properties.Caption = "Include this branch in head office reporting"
        Me.chkBRIncludeInReporting.Size = New System.Drawing.Size(232, 19)
        Me.chkBRIncludeInReporting.TabIndex = 7
        '
        'tpFinancialSettings
        '
        Me.tpFinancialSettings.Controls.Add(Me.GroupControl4)
        Me.tpFinancialSettings.Controls.Add(Me.GroupControl3)
        Me.tpFinancialSettings.Name = "tpFinancialSettings"
        Me.tpFinancialSettings.Size = New System.Drawing.Size(487, 410)
        Me.tpFinancialSettings.Text = "Financial Settings"
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.Label6)
        Me.GroupControl4.Controls.Add(Me.txtBRSuperannuation)
        Me.GroupControl4.Controls.Add(Me.Label5)
        Me.GroupControl4.Location = New System.Drawing.Point(8, 176)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(472, 120)
        Me.GroupControl4.TabIndex = 1
        Me.GroupControl4.Text = "Labor on costs"
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(8, 88)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(144, 20)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Labor on cost percentage:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBRSuperannuation
        '
        Me.txtBRSuperannuation.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtBRSuperannuation.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Branches.BRSuperannuation"))
        Me.txtBRSuperannuation.EditValue = ""
        Me.txtBRSuperannuation.Location = New System.Drawing.Point(152, 88)
        Me.txtBRSuperannuation.Name = "txtBRSuperannuation"
        '
        'txtBRSuperannuation.Properties
        '
        Me.txtBRSuperannuation.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBRSuperannuation.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtBRSuperannuation.Properties.DisplayFormat.FormatString = "p"
        Me.txtBRSuperannuation.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtBRSuperannuation.Properties.EditFormat.FormatString = "p"
        Me.txtBRSuperannuation.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtBRSuperannuation.Size = New System.Drawing.Size(136, 20)
        Me.txtBRSuperannuation.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(8, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(456, 56)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Labor on costs are automatically added to wages and salaries. Generally these on " & _
        "costs are compulsory. Examples of labor on costs paid by the employer would be C" & _
        "ompulsory Superannuation or National Health Contributions. These are expressed a" & _
        "s a percentage of wages or salaries."
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.txtBRGST)
        Me.GroupControl3.Controls.Add(Me.Label4)
        Me.GroupControl3.Controls.Add(Me.Label3)
        Me.GroupControl3.Location = New System.Drawing.Point(8, 8)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(472, 160)
        Me.GroupControl3.TabIndex = 0
        Me.GroupControl3.Text = "Indirect Tax"
        '
        'txtBRGST
        '
        Me.txtBRGST.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtBRGST.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Branches.BRGST"))
        Me.txtBRGST.EditValue = ""
        Me.txtBRGST.Location = New System.Drawing.Point(152, 128)
        Me.txtBRGST.Name = "txtBRGST"
        '
        'txtBRGST.Properties
        '
        Me.txtBRGST.Properties.Appearance.Options.UseTextOptions = True
        Me.txtBRGST.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtBRGST.Properties.DisplayFormat.FormatString = "p"
        Me.txtBRGST.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtBRGST.Properties.EditFormat.FormatString = "p"
        Me.txtBRGST.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtBRGST.Size = New System.Drawing.Size(136, 20)
        Me.txtBRGST.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(8, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(456, 96)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "This is a tax on transactions. It is a tax that is charged on sales and a tax tha" & _
        "t is charged by suppliers when materials and services are purchased. Generally t" & _
        "he tax charged on materials and services can be claimed back and the tax paid on" & _
        " sales is paid to the government. In some countries this called VAT(Value Added " & _
        "Tax) in others it is called GST (Goods and Services Tax). In the Franchise Manag" & _
        "er all such taxes will be called Indirect Taxes and these taxes are expressed as" & _
        " a percentage.  In certain reports amounts are calculated net of Indirect Tax. "
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(8, 128)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(136, 20)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Indirect tax percentage:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(352, 456)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "OK"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton1.Image = CType(resources.GetObject("SimpleButton1.Image"), System.Drawing.Image)
        Me.SimpleButton1.Location = New System.Drawing.Point(8, 456)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(72, 23)
        Me.SimpleButton1.TabIndex = 1
        Me.SimpleButton1.Text = "Help"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(432, 456)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'frmBranch2
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(514, 488)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBranch2"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Branch"
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.tpBranchProperties.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.txtLDClientPhoneNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientFaxNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientMobileNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.txtLDClientStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientPostCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientState.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientSuburb.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBRShortName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpBranchSettings.ResumeLayout(False)
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        CType(Me.chkBRIncludeInReporting.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpFinancialSettings.ResumeLayout(False)
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.txtBRSuperannuation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.txtBRGST.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnOK.Enabled = Not HasRole("branch_read_only")
    End Sub

    ' This data must be loaded BEFORE the DataRow is loaded (eg lookups etc)
    Private Sub FillPreliminaryData()
    End Sub

    ' This data must be loaded AFTER the DataRow is loaded (eg record related data)
    Private Sub FillData()
    End Sub

    Private Sub FillInitData()
        DataRow("BRGST") = ExecuteScalar("SELECT GSGST FROM GlobalSettings WHERE GSID = 1", CommandType.Text, New SqlClient.SqlParameter() {}, SqlConnection)
        DataRow("BRSuperannuation") = ExecuteScalar("SELECT GSSuperannuation FROM GlobalSettings WHERE GSID = 1", CommandType.Text, New SqlClient.SqlParameter() {}, SqlConnection)
    End Sub

    Private Sub UpdateData()
        Dim c As Cursor = Me.Cursor
        c = Windows.Forms.Cursors.WaitCursor
        Application.DoEvents()
        SqlDataAdapter.Update(dataSet)
        Me.Cursor = c
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        ' EndEdit() to end editing the dataset record so that we can update
        DataRow.EndEdit()

        If ValidateForm() Then
            UpdateData()
            OK = True
            Me.Close()
        End If
    End Sub

    Private Function ValidateForm() As Boolean
        If DataRow("BRBusinessName") Is DBNull.Value Then
            Message.ShowMessage("You must enter a branch name.", MessageBoxIcon.Exclamation)
            Return False
        End If
        If DataRow("BRShortName") Is DBNull.Value Then
            Message.ShowMessage("You must enter a full business name.", MessageBoxIcon.Exclamation)
            Return False
        End If
        If DataRow("BRGST") Is DBNull.Value Then
            Message.ShowMessage("You must enter indirect tax percentage.  For none, enter zero.", MessageBoxIcon.Exclamation)
            Return False
        End If
        If DataRow("BRSuperannuation") Is DBNull.Value Then
            Message.ShowMessage("You must enter a labor on costs percentage.  For none, enter zero.", MessageBoxIcon.Exclamation)
            Return False
        End If
        Dim proceed As Boolean = True
        If DataRow.RowState = DataRowState.Modified Then
            If DataRow.Item("BRShowHours", DataRowVersion.Original) <> DataRow.Item("BRShowHours", DataRowVersion.Current) Then
                If Message.AskQuestion("Because you have changed the Measuring Productivity setting, the program will need to recalculate the jobs at this branch.  This may take some time." & _
                vbNewLine & vbNewLine & "Do you wish to proceed?") = DialogResult.Yes Then
                    SqlDataAdapter.UpdateCommand.CommandTimeout = 0
                Else
                    Return False
                End If
            End If
        End If
        Return True
    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            DataRow.EndEdit()
            If dataSet.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If response = MsgBoxResult.No Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub EnableDisable()
    End Sub

    Private Sub txtPercentage_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) _
    Handles txtBRGST.ParseEditValue, txtBRSuperannuation.ParseEditValue
        If TypeOf e.Value Is String Then
            If Trim(e.Value) = "" Then
                e.Value = DBNull.Value
            Else
                Try
                    e.Value = e.Value / 100
                Catch ex As Exception
                End Try
            End If
        End If
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        ShowHelpTopic(Me, "BranchPropertiesTerms.html")
    End Sub

End Class
