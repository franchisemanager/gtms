Imports Power.Forms
Imports Power.Library

Public Class frmExplorerHeadOffice
    Inherits DevExpress.XtraEditors.XtraForm

    Private IsInitializing As Boolean = True

    Public BranchForms As ArrayList = New ArrayList

#Region " Windows Form Designer generated code "

    Public Sub New() 'ByVal Roles As ArrayList, ByVal Commands As ArrayList)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        IsInitializing = False

        SqlConnection.ConnectionString = ConnectionString
        SqlConnection.Open()
        NodeCollection = New ArrayList
        NodeCollectionCursor = -1
        'btnBack.Enabled = False
        'btnForward.Enabled = False
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents pnlMain As DevExpress.XtraEditors.PanelControl
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents lblSectionTitle As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlOutside As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Splitter1 As System.Windows.Forms.Splitter
    Friend WithEvents pnlHeader As DevExpress.XtraEditors.PanelControl
    Friend WithEvents ImageListSmall As System.Windows.Forms.ImageList
    Friend WithEvents ImageListToolBar As System.Windows.Forms.ImageList
    Friend WithEvents ImageListLarge As System.Windows.Forms.ImageList
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents BarAndDockingController1 As DevExpress.XtraBars.BarAndDockingController
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents subFile As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnExit As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnAboutFranchiseManager As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bStatus As DevExpress.XtraBars.Bar
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents subReports As DevExpress.XtraBars.BarSubItem
    Friend WithEvents bMainMenu As DevExpress.XtraBars.Bar
    Friend WithEvents subHOOperations As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnHOJobsFinishedOnTimeReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnHODebtorStatisticsReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnHOJobIssuesReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subHOProfit As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnHOAllJobsReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subHOMaterials As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnHOStockReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnHOWasteReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnHOStockRequirementsReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subHOMarketing As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnHOPrimaryEnquirySourcesReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subHelp As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnHOAboutFranchiseManager1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnNew As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnEdit As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnDelete As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnShowBranch As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subHOReports As DevExpress.XtraBars.BarSubItem
    Friend WithEvents DockManager1 As DevExpress.XtraBars.Docking.DockManager
    Friend WithEvents DockPanel1_Container As DevExpress.XtraBars.Docking.ControlContainer
    Friend WithEvents dpNavigation As DevExpress.XtraBars.Docking.DockPanel
    Friend WithEvents tvwMenu As Power.Forms.TreeView
    Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarToolbarsListItem As DevExpress.XtraBars.BarToolbarsListItem
    Friend WithEvents btnAutoHideNavigationPanel As DevExpress.XtraBars.BarCheckItem
    Friend WithEvents ControlContainer1 As DevExpress.XtraBars.Docking.ControlContainer
    Friend WithEvents pnlHelp As DevExpress.XtraEditors.PanelControl
    Friend WithEvents rtbHelp As Power.Forms.RichTextBox
    Friend WithEvents dpHelp As DevExpress.XtraBars.Docking.DockPanel
    Friend WithEvents btnAutoHideHelpPanel As DevExpress.XtraBars.BarCheckItem
    Friend WithEvents ImageListPanels As System.Windows.Forms.ImageList
    Friend WithEvents hideContainerRight As DevExpress.XtraBars.Docking.AutoHideContainer
    Friend WithEvents btnShowHelp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnHelp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bsiVisibleItems As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents bsiNumItems As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents subHODirectLabourandSalespeople As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnHoursCostsAndVolumeReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnCancel As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnHODataEntryStatisticsReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnHOBranchBenchmarksReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnHOPredictedSalesReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnHOQuoteStatisticsReport As DevExpress.XtraBars.BarButtonItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmExplorerHeadOffice))
        Dim configurationAppSettings As System.Configuration.AppSettingsReader = New System.Configuration.AppSettingsReader
        Me.ImageListToolBar = New System.Windows.Forms.ImageList(Me.components)
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.pnlMain = New DevExpress.XtraEditors.PanelControl
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.pnlHeader = New DevExpress.XtraEditors.PanelControl
        Me.lblSectionTitle = New System.Windows.Forms.Label
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl
        Me.pnlOutside = New DevExpress.XtraEditors.PanelControl
        Me.Splitter1 = New System.Windows.Forms.Splitter
        Me.ImageListSmall = New System.Windows.Forms.ImageList(Me.components)
        Me.ImageListLarge = New System.Windows.Forms.ImageList(Me.components)
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider
        Me.BarAndDockingController1 = New DevExpress.XtraBars.BarAndDockingController(Me.components)
        Me.BarManager1 = New DevExpress.XtraBars.BarManager
        Me.bStatus = New DevExpress.XtraBars.Bar
        Me.bsiNumItems = New DevExpress.XtraBars.BarStaticItem
        Me.bsiVisibleItems = New DevExpress.XtraBars.BarStaticItem
        Me.bMainMenu = New DevExpress.XtraBars.Bar
        Me.subFile = New DevExpress.XtraBars.BarSubItem
        Me.btnNew = New DevExpress.XtraBars.BarButtonItem
        Me.btnEdit = New DevExpress.XtraBars.BarButtonItem
        Me.btnCancel = New DevExpress.XtraBars.BarButtonItem
        Me.btnDelete = New DevExpress.XtraBars.BarButtonItem
        Me.btnShowBranch = New DevExpress.XtraBars.BarButtonItem
        Me.btnExit = New DevExpress.XtraBars.BarButtonItem
        Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem
        Me.btnAutoHideNavigationPanel = New DevExpress.XtraBars.BarCheckItem
        Me.btnAutoHideHelpPanel = New DevExpress.XtraBars.BarCheckItem
        Me.subHOReports = New DevExpress.XtraBars.BarSubItem
        Me.subHOOperations = New DevExpress.XtraBars.BarSubItem
        Me.btnHOJobsFinishedOnTimeReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnHODebtorStatisticsReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnHOJobIssuesReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnHODataEntryStatisticsReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnHOPredictedSalesReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnHOQuoteStatisticsReport = New DevExpress.XtraBars.BarButtonItem
        Me.subHOMarketing = New DevExpress.XtraBars.BarSubItem
        Me.btnHOPrimaryEnquirySourcesReport = New DevExpress.XtraBars.BarButtonItem
        Me.subHOMaterials = New DevExpress.XtraBars.BarSubItem
        Me.btnHOStockReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnHOWasteReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnHOStockRequirementsReport = New DevExpress.XtraBars.BarButtonItem
        Me.subHODirectLabourandSalespeople = New DevExpress.XtraBars.BarSubItem
        Me.btnHoursCostsAndVolumeReport = New DevExpress.XtraBars.BarButtonItem
        Me.subHOProfit = New DevExpress.XtraBars.BarSubItem
        Me.btnHOAllJobsReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnHOBranchBenchmarksReport = New DevExpress.XtraBars.BarButtonItem
        Me.subHelp = New DevExpress.XtraBars.BarSubItem
        Me.btnHelp = New DevExpress.XtraBars.BarButtonItem
        Me.btnHOAboutFranchiseManager1 = New DevExpress.XtraBars.BarButtonItem
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
        Me.DockManager1 = New DevExpress.XtraBars.Docking.DockManager
        Me.hideContainerRight = New DevExpress.XtraBars.Docking.AutoHideContainer
        Me.dpHelp = New DevExpress.XtraBars.Docking.DockPanel
        Me.ControlContainer1 = New DevExpress.XtraBars.Docking.ControlContainer
        Me.pnlHelp = New DevExpress.XtraEditors.PanelControl
        Me.rtbHelp = New Power.Forms.RichTextBox
        Me.ImageListPanels = New System.Windows.Forms.ImageList(Me.components)
        Me.dpNavigation = New DevExpress.XtraBars.Docking.DockPanel
        Me.DockPanel1_Container = New DevExpress.XtraBars.Docking.ControlContainer
        Me.tvwMenu = New Power.Forms.TreeView
        Me.btnShowHelp = New DevExpress.XtraBars.BarButtonItem
        Me.btnAboutFranchiseManager = New DevExpress.XtraBars.BarButtonItem
        Me.BarToolbarsListItem = New DevExpress.XtraBars.BarToolbarsListItem
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem
        Me.subReports = New DevExpress.XtraBars.BarSubItem
        CType(Me.pnlMain, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlHeader.SuspendLayout()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlOutside, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlOutside.SuspendLayout()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hideContainerRight.SuspendLayout()
        Me.dpHelp.SuspendLayout()
        Me.ControlContainer1.SuspendLayout()
        CType(Me.pnlHelp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlHelp.SuspendLayout()
        Me.dpNavigation.SuspendLayout()
        Me.DockPanel1_Container.SuspendLayout()
        Me.SuspendLayout()
        '
        'ImageListToolBar
        '
        Me.ImageListToolBar.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImageListToolBar.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageListToolBar.ImageStream = CType(resources.GetObject("ImageListToolBar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListToolBar.TransparentColor = System.Drawing.Color.Transparent
        '
        'pnlMain
        '
        Me.pnlMain.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.pnlMain.Appearance.Options.UseBackColor = True
        Me.pnlMain.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(1, 40)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(668, 479)
        Me.pnlMain.TabIndex = 4
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = CType(configurationAppSettings.GetValue("SqlConnection.ConnectionString", GetType(System.String)), String)
        '
        'pnlHeader
        '
        Me.pnlHeader.Appearance.BackColor = System.Drawing.SystemColors.ControlDark
        Me.pnlHeader.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.pnlHeader.Appearance.Options.UseBackColor = True
        Me.pnlHeader.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.pnlHeader.Controls.Add(Me.lblSectionTitle)
        Me.pnlHeader.Controls.Add(Me.PanelControl3)
        Me.pnlHeader.Controls.Add(Me.PictureBox1)
        Me.pnlHeader.Controls.Add(Me.PanelControl2)
        Me.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeader.DockPadding.All = 2
        Me.pnlHeader.Location = New System.Drawing.Point(1, 1)
        Me.pnlHeader.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat
        Me.pnlHeader.LookAndFeel.UseDefaultLookAndFeel = False
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Size = New System.Drawing.Size(668, 36)
        Me.pnlHeader.TabIndex = 5
        '
        'lblSectionTitle
        '
        Me.lblSectionTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblSectionTitle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblSectionTitle.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSectionTitle.ForeColor = System.Drawing.Color.White
        Me.lblSectionTitle.Location = New System.Drawing.Point(54, 2)
        Me.lblSectionTitle.Name = "lblSectionTitle"
        Me.lblSectionTitle.Size = New System.Drawing.Size(612, 32)
        Me.lblSectionTitle.TabIndex = 0
        Me.lblSectionTitle.Text = "Franchise Manager for Align Kitchens"
        Me.lblSectionTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'PanelControl3
        '
        Me.PanelControl3.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PanelControl3.Appearance.Options.UseBackColor = True
        Me.PanelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl3.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelControl3.Location = New System.Drawing.Point(42, 2)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(12, 32)
        Me.PanelControl3.TabIndex = 1
        Me.PanelControl3.Text = "PanelControl3"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PictureBox1.Location = New System.Drawing.Point(10, 2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(32, 32)
        Me.PictureBox1.TabIndex = 3
        Me.PictureBox1.TabStop = False
        '
        'PanelControl2
        '
        Me.PanelControl2.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PanelControl2.Appearance.Options.UseBackColor = True
        Me.PanelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelControl2.Location = New System.Drawing.Point(2, 2)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(8, 32)
        Me.PanelControl2.TabIndex = 0
        Me.PanelControl2.Text = "PanelControl2"
        '
        'pnlOutside
        '
        Me.pnlOutside.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.pnlOutside.Controls.Add(Me.pnlMain)
        Me.pnlOutside.Controls.Add(Me.Splitter1)
        Me.pnlOutside.Controls.Add(Me.pnlHeader)
        Me.pnlOutside.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlOutside.DockPadding.All = 1
        Me.pnlOutside.Location = New System.Drawing.Point(276, 20)
        Me.pnlOutside.Name = "pnlOutside"
        Me.pnlOutside.Size = New System.Drawing.Size(670, 520)
        Me.pnlOutside.TabIndex = 6
        '
        'Splitter1
        '
        Me.Splitter1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Splitter1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Splitter1.Enabled = False
        Me.Splitter1.Location = New System.Drawing.Point(1, 37)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(668, 3)
        Me.Splitter1.TabIndex = 6
        Me.Splitter1.TabStop = False
        '
        'ImageListSmall
        '
        Me.ImageListSmall.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImageListSmall.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageListSmall.ImageStream = CType(resources.GetObject("ImageListSmall.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListSmall.TransparentColor = System.Drawing.Color.Transparent
        '
        'ImageListLarge
        '
        Me.ImageListLarge.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImageListLarge.ImageSize = New System.Drawing.Size(32, 32)
        Me.ImageListLarge.ImageStream = CType(resources.GetObject("ImageListLarge.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListLarge.TransparentColor = System.Drawing.Color.Transparent
        '
        'HelpProvider1
        '
        Me.HelpProvider1.HelpNamespace = "C:\Documents and Settings\cjrada\My Documents\HelpProject.chm"
        '
        'BarAndDockingController1
        '
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseForeColor = True
        Me.BarAndDockingController1.PaintStyleName = "OfficeXP"
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.bStatus, Me.bMainMenu})
        Me.BarManager1.Categories.AddRange(New DevExpress.XtraBars.BarManagerCategory() {New DevExpress.XtraBars.BarManagerCategory("Built-in Menus", New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd"), False), New DevExpress.XtraBars.BarManagerCategory("File", New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")), New DevExpress.XtraBars.BarManagerCategory("Reports", New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")), New DevExpress.XtraBars.BarManagerCategory("Help", New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")), New DevExpress.XtraBars.BarManagerCategory("View", New System.Guid("5e30dc5b-f3bb-4739-a282-c2a49802c68d"))})
        Me.BarManager1.Controller = Me.BarAndDockingController1
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.DockManager = Me.DockManager1
        Me.BarManager1.Form = Me
        Me.BarManager1.Images = Me.ImageListToolBar
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnNew, Me.btnEdit, Me.subFile, Me.btnCancel, Me.btnDelete, Me.btnShowBranch, Me.btnExit, Me.btnShowHelp, Me.btnAboutFranchiseManager, Me.subHOReports, Me.subHOOperations, Me.btnHOJobsFinishedOnTimeReport, Me.btnHODebtorStatisticsReport, Me.btnHOJobIssuesReport, Me.subHOProfit, Me.btnHOAllJobsReport, Me.subHOMaterials, Me.btnHOStockReport, Me.btnHOWasteReport, Me.btnHOStockRequirementsReport, Me.subHOMarketing, Me.btnHOPrimaryEnquirySourcesReport, Me.subHelp, Me.btnHOAboutFranchiseManager1, Me.BarSubItem1, Me.BarToolbarsListItem, Me.btnAutoHideNavigationPanel, Me.btnAutoHideHelpPanel, Me.btnHelp, Me.BarButtonItem1, Me.bsiVisibleItems, Me.bsiNumItems, Me.subHODirectLabourandSalespeople, Me.btnHoursCostsAndVolumeReport, Me.btnHODataEntryStatisticsReport, Me.btnHOBranchBenchmarksReport, Me.btnHOPredictedSalesReport, Me.btnHOQuoteStatisticsReport})
        Me.BarManager1.MainMenu = Me.bMainMenu
        Me.BarManager1.MaxItemId = 66
        Me.BarManager1.StatusBar = Me.bStatus
        '
        'bStatus
        '
        Me.bStatus.BarName = "Status Bar"
        Me.bStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
        Me.bStatus.DockCol = 0
        Me.bStatus.DockRow = 0
        Me.bStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.bStatus.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.bsiNumItems), New DevExpress.XtraBars.LinkPersistInfo(Me.bsiVisibleItems)})
        Me.bStatus.OptionsBar.AllowQuickCustomization = False
        Me.bStatus.OptionsBar.DisableClose = True
        Me.bStatus.OptionsBar.DisableCustomization = True
        Me.bStatus.OptionsBar.DrawDragBorder = False
        Me.bStatus.OptionsBar.DrawSizeGrip = True
        Me.bStatus.OptionsBar.UseWholeRow = True
        Me.bStatus.Text = "Status Bar"
        '
        'bsiNumItems
        '
        Me.bsiNumItems.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None
        Me.bsiNumItems.Border = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.bsiNumItems.Caption = "0 Items"
        Me.bsiNumItems.Id = 57
        Me.bsiNumItems.Name = "bsiNumItems"
        Me.bsiNumItems.TextAlignment = System.Drawing.StringAlignment.Near
        Me.bsiNumItems.Width = 100
        '
        'bsiVisibleItems
        '
        Me.bsiVisibleItems.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring
        Me.bsiVisibleItems.Border = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.bsiVisibleItems.Caption = "0 Visible Items"
        Me.bsiVisibleItems.Id = 56
        Me.bsiVisibleItems.Name = "bsiVisibleItems"
        Me.bsiVisibleItems.TextAlignment = System.Drawing.StringAlignment.Near
        Me.bsiVisibleItems.Width = 32
        '
        'bMainMenu
        '
        Me.bMainMenu.BarName = "Main Menu"
        Me.bMainMenu.DockCol = 0
        Me.bMainMenu.DockRow = 0
        Me.bMainMenu.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bMainMenu.FloatLocation = New System.Drawing.Point(18, 186)
        Me.bMainMenu.FloatSize = New System.Drawing.Size(237, 20)
        Me.bMainMenu.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.subFile), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.subHOReports), New DevExpress.XtraBars.LinkPersistInfo(Me.subHelp)})
        Me.bMainMenu.OptionsBar.AllowQuickCustomization = False
        Me.bMainMenu.OptionsBar.MultiLine = True
        Me.bMainMenu.OptionsBar.UseWholeRow = True
        Me.bMainMenu.Text = "Main Menu"
        '
        'subFile
        '
        Me.subFile.Caption = "&File"
        Me.subFile.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subFile.Id = 21
        Me.subFile.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnNew), New DevExpress.XtraBars.LinkPersistInfo(Me.btnEdit), New DevExpress.XtraBars.LinkPersistInfo(Me.btnCancel), New DevExpress.XtraBars.LinkPersistInfo(Me.btnDelete), New DevExpress.XtraBars.LinkPersistInfo(Me.btnShowBranch, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnExit, True)})
        Me.subFile.Name = "subFile"
        '
        'btnNew
        '
        Me.btnNew.Caption = "&New..."
        Me.btnNew.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnNew.Id = 19
        Me.btnNew.ImageIndex = 0
        Me.btnNew.ImageIndexDisabled = 0
        Me.btnNew.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N))
        Me.btnNew.Name = "btnNew"
        '
        'btnEdit
        '
        Me.btnEdit.Caption = "&Edit..."
        Me.btnEdit.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnEdit.Id = 20
        Me.btnEdit.ImageIndex = 1
        Me.btnEdit.ImageIndexDisabled = 1
        Me.btnEdit.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.E))
        Me.btnEdit.Name = "btnEdit"
        '
        'btnCancel
        '
        Me.btnCancel.Caption = "&Cancel"
        Me.btnCancel.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnCancel.Id = 61
        Me.btnCancel.ImageIndex = 2
        Me.btnCancel.ImageIndexDisabled = 2
        Me.btnCancel.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.D))
        Me.btnCancel.Name = "btnCancel"
        '
        'btnDelete
        '
        Me.btnDelete.Caption = "&Delete"
        Me.btnDelete.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnDelete.Id = 23
        Me.btnDelete.ImageIndex = 6
        Me.btnDelete.ImageIndexDisabled = 6
        Me.btnDelete.Name = "btnDelete"
        '
        'btnShowBranch
        '
        Me.btnShowBranch.Caption = "Show &Branch..."
        Me.btnShowBranch.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnShowBranch.Id = 24
        Me.btnShowBranch.ImageIndex = 5
        Me.btnShowBranch.ImageIndexDisabled = 5
        Me.btnShowBranch.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.B))
        Me.btnShowBranch.Name = "btnShowBranch"
        '
        'btnExit
        '
        Me.btnExit.Caption = "E&xit"
        Me.btnExit.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnExit.Id = 25
        Me.btnExit.Name = "btnExit"
        '
        'BarSubItem1
        '
        Me.BarSubItem1.Caption = "&View"
        Me.BarSubItem1.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.BarSubItem1.Id = 48
        Me.BarSubItem1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnAutoHideNavigationPanel), New DevExpress.XtraBars.LinkPersistInfo(Me.btnAutoHideHelpPanel)})
        Me.BarSubItem1.Name = "BarSubItem1"
        '
        'btnAutoHideNavigationPanel
        '
        Me.btnAutoHideNavigationPanel.Caption = "Hide &Navigation Panel"
        Me.btnAutoHideNavigationPanel.CategoryGuid = New System.Guid("5e30dc5b-f3bb-4739-a282-c2a49802c68d")
        Me.btnAutoHideNavigationPanel.Id = 52
        Me.btnAutoHideNavigationPanel.ItemShortcut = New DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F6)
        Me.btnAutoHideNavigationPanel.Name = "btnAutoHideNavigationPanel"
        '
        'btnAutoHideHelpPanel
        '
        Me.btnAutoHideHelpPanel.Caption = "Hide &Help Panel"
        Me.btnAutoHideHelpPanel.CategoryGuid = New System.Guid("5e30dc5b-f3bb-4739-a282-c2a49802c68d")
        Me.btnAutoHideHelpPanel.Checked = True
        Me.btnAutoHideHelpPanel.Id = 53
        Me.btnAutoHideHelpPanel.ItemShortcut = New DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F7)
        Me.btnAutoHideHelpPanel.Name = "btnAutoHideHelpPanel"
        '
        'subHOReports
        '
        Me.subHOReports.Caption = "&Reports"
        Me.subHOReports.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subHOReports.Id = 30
        Me.subHOReports.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.subHOOperations), New DevExpress.XtraBars.LinkPersistInfo(Me.subHOMarketing, True), New DevExpress.XtraBars.LinkPersistInfo(Me.subHOMaterials), New DevExpress.XtraBars.LinkPersistInfo(Me.subHODirectLabourandSalespeople), New DevExpress.XtraBars.LinkPersistInfo(Me.subHOProfit)})
        Me.subHOReports.Name = "subHOReports"
        '
        'subHOOperations
        '
        Me.subHOOperations.Caption = "&Operations"
        Me.subHOOperations.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subHOOperations.Id = 31
        Me.subHOOperations.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnHOJobsFinishedOnTimeReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnHODebtorStatisticsReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnHOJobIssuesReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnHODataEntryStatisticsReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnHOPredictedSalesReport, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnHOQuoteStatisticsReport)})
        Me.subHOOperations.Name = "subHOOperations"
        '
        'btnHOJobsFinishedOnTimeReport
        '
        Me.btnHOJobsFinishedOnTimeReport.Caption = "Jobs Finished On Time Report"
        Me.btnHOJobsFinishedOnTimeReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnHOJobsFinishedOnTimeReport.Id = 32
        Me.btnHOJobsFinishedOnTimeReport.Name = "btnHOJobsFinishedOnTimeReport"
        '
        'btnHODebtorStatisticsReport
        '
        Me.btnHODebtorStatisticsReport.Caption = "Accounts Receivable Statistics Report"
        Me.btnHODebtorStatisticsReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnHODebtorStatisticsReport.Id = 33
        Me.btnHODebtorStatisticsReport.Name = "btnHODebtorStatisticsReport"
        '
        'btnHOJobIssuesReport
        '
        Me.btnHOJobIssuesReport.Caption = "Job Issues Report"
        Me.btnHOJobIssuesReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnHOJobIssuesReport.Id = 34
        Me.btnHOJobIssuesReport.Name = "btnHOJobIssuesReport"
        '
        'btnHODataEntryStatisticsReport
        '
        Me.btnHODataEntryStatisticsReport.Caption = "Data Entry Statistics Report"
        Me.btnHODataEntryStatisticsReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnHODataEntryStatisticsReport.Id = 62
        Me.btnHODataEntryStatisticsReport.Name = "btnHODataEntryStatisticsReport"
        '
        'btnHOPredictedSalesReport
        '
        Me.btnHOPredictedSalesReport.Caption = "Predicted Sales Report"
        Me.btnHOPredictedSalesReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnHOPredictedSalesReport.Id = 64
        Me.btnHOPredictedSalesReport.Name = "btnHOPredictedSalesReport"
        '
        'btnHOQuoteStatisticsReport
        '
        Me.btnHOQuoteStatisticsReport.Caption = "Quote Statistics Report"
        Me.btnHOQuoteStatisticsReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnHOQuoteStatisticsReport.Id = 65
        Me.btnHOQuoteStatisticsReport.Name = "btnHOQuoteStatisticsReport"
        '
        'subHOMarketing
        '
        Me.subHOMarketing.Caption = "Ma&rketing"
        Me.subHOMarketing.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subHOMarketing.Id = 43
        Me.subHOMarketing.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnHOPrimaryEnquirySourcesReport)})
        Me.subHOMarketing.Name = "subHOMarketing"
        '
        'btnHOPrimaryEnquirySourcesReport
        '
        Me.btnHOPrimaryEnquirySourcesReport.Caption = "Primary Enquiry Sources Report"
        Me.btnHOPrimaryEnquirySourcesReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnHOPrimaryEnquirySourcesReport.Id = 44
        Me.btnHOPrimaryEnquirySourcesReport.Name = "btnHOPrimaryEnquirySourcesReport"
        '
        'subHOMaterials
        '
        Me.subHOMaterials.Caption = "&Materials"
        Me.subHOMaterials.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subHOMaterials.Id = 38
        Me.subHOMaterials.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnHOStockReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnHOWasteReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnHOStockRequirementsReport)})
        Me.subHOMaterials.Name = "subHOMaterials"
        '
        'btnHOStockReport
        '
        Me.btnHOStockReport.Caption = "Inventory Report"
        Me.btnHOStockReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnHOStockReport.Id = 39
        Me.btnHOStockReport.Name = "btnHOStockReport"
        '
        'btnHOWasteReport
        '
        Me.btnHOWasteReport.Caption = "Waste Report"
        Me.btnHOWasteReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnHOWasteReport.Id = 40
        Me.btnHOWasteReport.Name = "btnHOWasteReport"
        '
        'btnHOStockRequirementsReport
        '
        Me.btnHOStockRequirementsReport.Caption = "Tracked Material Requirements Report"
        Me.btnHOStockRequirementsReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnHOStockRequirementsReport.Id = 41
        Me.btnHOStockRequirementsReport.Name = "btnHOStockRequirementsReport"
        '
        'subHODirectLabourandSalespeople
        '
        Me.subHODirectLabourandSalespeople.Caption = "&Direct Labor and Salespeople"
        Me.subHODirectLabourandSalespeople.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subHODirectLabourandSalespeople.Id = 58
        Me.subHODirectLabourandSalespeople.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnHoursCostsAndVolumeReport)})
        Me.subHODirectLabourandSalespeople.Name = "subHODirectLabourandSalespeople"
        '
        'btnHoursCostsAndVolumeReport
        '
        Me.btnHoursCostsAndVolumeReport.Caption = "Hours, Cost and Volume Report"
        Me.btnHoursCostsAndVolumeReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnHoursCostsAndVolumeReport.Id = 59
        Me.btnHoursCostsAndVolumeReport.Name = "btnHoursCostsAndVolumeReport"
        '
        'subHOProfit
        '
        Me.subHOProfit.Caption = "&Profit"
        Me.subHOProfit.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subHOProfit.Id = 36
        Me.subHOProfit.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnHOAllJobsReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnHOBranchBenchmarksReport)})
        Me.subHOProfit.Name = "subHOProfit"
        '
        'btnHOAllJobsReport
        '
        Me.btnHOAllJobsReport.Caption = "All Jobs Report"
        Me.btnHOAllJobsReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnHOAllJobsReport.Id = 37
        Me.btnHOAllJobsReport.Name = "btnHOAllJobsReport"
        '
        'btnHOBranchBenchmarksReport
        '
        Me.btnHOBranchBenchmarksReport.Caption = "Branch Benchmarks Report"
        Me.btnHOBranchBenchmarksReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnHOBranchBenchmarksReport.Id = 63
        Me.btnHOBranchBenchmarksReport.Name = "btnHOBranchBenchmarksReport"
        '
        'subHelp
        '
        Me.subHelp.Caption = "&Help"
        Me.subHelp.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subHelp.Id = 45
        Me.subHelp.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnHelp, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnHOAboutFranchiseManager1, True)})
        Me.subHelp.Name = "subHelp"
        '
        'btnHelp
        '
        Me.btnHelp.Caption = "Franchise Manager &Help..."
        Me.btnHelp.CategoryGuid = New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")
        Me.btnHelp.Id = 54
        Me.btnHelp.ImageIndex = 4
        Me.btnHelp.ImageIndexDisabled = 4
        Me.btnHelp.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F1))
        Me.btnHelp.Name = "btnHelp"
        '
        'btnHOAboutFranchiseManager1
        '
        Me.btnHOAboutFranchiseManager1.Caption = "&About Franchise Manager..."
        Me.btnHOAboutFranchiseManager1.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnHOAboutFranchiseManager1.Id = 46
        Me.btnHOAboutFranchiseManager1.Name = "btnHOAboutFranchiseManager1"
        '
        'DockManager1
        '
        Me.DockManager1.AutoHideContainers.AddRange(New DevExpress.XtraBars.Docking.AutoHideContainer() {Me.hideContainerRight})
        Me.DockManager1.Controller = Me.BarAndDockingController1
        Me.DockManager1.Form = Me
        Me.DockManager1.Images = Me.ImageListPanels
        Me.DockManager1.RootPanels.AddRange(New DevExpress.XtraBars.Docking.DockPanel() {Me.dpNavigation})
        Me.DockManager1.TopZIndexControls.AddRange(New String() {"DevExpress.XtraBars.BarDockControl", "System.Windows.Forms.StatusBar"})
        '
        'hideContainerRight
        '
        Me.hideContainerRight.Controls.Add(Me.dpHelp)
        Me.hideContainerRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.hideContainerRight.Location = New System.Drawing.Point(946, 20)
        Me.hideContainerRight.Name = "hideContainerRight"
        Me.hideContainerRight.Size = New System.Drawing.Size(22, 520)
        '
        'dpHelp
        '
        Me.dpHelp.Controls.Add(Me.ControlContainer1)
        Me.dpHelp.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right
        Me.dpHelp.ID = New System.Guid("c7d688d4-44c4-4a6f-a735-7eba6f64fec6")
        Me.dpHelp.ImageIndex = 0
        Me.dpHelp.Location = New System.Drawing.Point(0, 0)
        Me.dpHelp.Name = "dpHelp"
        Me.dpHelp.Options.ShowCloseButton = False
        Me.dpHelp.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Right
        Me.dpHelp.SavedIndex = 0
        Me.dpHelp.Size = New System.Drawing.Size(265, 495)
        Me.dpHelp.Text = "Help"
        Me.dpHelp.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide
        '
        'ControlContainer1
        '
        Me.ControlContainer1.Controls.Add(Me.pnlHelp)
        Me.ControlContainer1.Location = New System.Drawing.Point(4, 21)
        Me.ControlContainer1.Name = "ControlContainer1"
        Me.ControlContainer1.Size = New System.Drawing.Size(257, 470)
        Me.ControlContainer1.TabIndex = 0
        '
        'pnlHelp
        '
        Me.pnlHelp.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.pnlHelp.Appearance.Options.UseBackColor = True
        Me.pnlHelp.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.pnlHelp.Controls.Add(Me.rtbHelp)
        Me.pnlHelp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlHelp.DockPadding.Bottom = 4
        Me.pnlHelp.DockPadding.Left = 4
        Me.pnlHelp.DockPadding.Right = 4
        Me.pnlHelp.Location = New System.Drawing.Point(0, 0)
        Me.pnlHelp.Name = "pnlHelp"
        Me.pnlHelp.Size = New System.Drawing.Size(257, 470)
        Me.pnlHelp.TabIndex = 1
        Me.pnlHelp.Text = "PanelControl1"
        '
        'rtbHelp
        '
        Me.rtbHelp.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.rtbHelp.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.rtbHelp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rtbHelp.Location = New System.Drawing.Point(6, 2)
        Me.rtbHelp.Name = "rtbHelp"
        Me.rtbHelp.ReadOnly = True
        Me.rtbHelp.Size = New System.Drawing.Size(245, 462)
        Me.rtbHelp.TabIndex = 0
        Me.rtbHelp.Text = "RichTextBox1"
        '
        'ImageListPanels
        '
        Me.ImageListPanels.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageListPanels.ImageStream = CType(resources.GetObject("ImageListPanels.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListPanels.TransparentColor = System.Drawing.Color.Transparent
        '
        'dpNavigation
        '
        Me.dpNavigation.Controls.Add(Me.DockPanel1_Container)
        Me.dpNavigation.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left
        Me.dpNavigation.FloatSize = New System.Drawing.Size(271, 200)
        Me.dpNavigation.ID = New System.Guid("22e7b859-8558-41a2-b49b-aa1bf3978aed")
        Me.dpNavigation.Location = New System.Drawing.Point(0, 20)
        Me.dpNavigation.Name = "dpNavigation"
        Me.dpNavigation.Options.ShowCloseButton = False
        Me.dpNavigation.Size = New System.Drawing.Size(276, 520)
        Me.dpNavigation.Text = "Navigation Panel"
        '
        'DockPanel1_Container
        '
        Me.DockPanel1_Container.Controls.Add(Me.tvwMenu)
        Me.DockPanel1_Container.Location = New System.Drawing.Point(4, 21)
        Me.DockPanel1_Container.Name = "DockPanel1_Container"
        Me.DockPanel1_Container.Size = New System.Drawing.Size(268, 495)
        Me.DockPanel1_Container.TabIndex = 0
        '
        'tvwMenu
        '
        Me.tvwMenu.BackColor = System.Drawing.Color.White
        Me.tvwMenu.Connection = Me.SqlConnection
        Me.tvwMenu.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvwMenu.HideSelection = False
        Me.tvwMenu.ImageList = Me.ImageListSmall
        Me.tvwMenu.Location = New System.Drawing.Point(0, 0)
        Me.tvwMenu.Name = "tvwMenu"
        Me.tvwMenu.Size = New System.Drawing.Size(268, 495)
        Me.tvwMenu.TabIndex = 10
        '
        'btnShowHelp
        '
        Me.btnShowHelp.Caption = "&Help"
        Me.btnShowHelp.CategoryGuid = New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")
        Me.btnShowHelp.Id = 68
        Me.btnShowHelp.ImageIndex = 4
        Me.btnShowHelp.ImageIndexDisabled = 4
        Me.btnShowHelp.Name = "btnShowHelp"
        '
        'btnAboutFranchiseManager
        '
        Me.btnAboutFranchiseManager.Caption = "About Franchise Manager..."
        Me.btnAboutFranchiseManager.CategoryGuid = New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")
        Me.btnAboutFranchiseManager.Id = 69
        Me.btnAboutFranchiseManager.Name = "btnAboutFranchiseManager"
        '
        'BarToolbarsListItem
        '
        Me.BarToolbarsListItem.Caption = "Toolbar List"
        Me.BarToolbarsListItem.CategoryGuid = New System.Guid("5e30dc5b-f3bb-4739-a282-c2a49802c68d")
        Me.BarToolbarsListItem.Id = 51
        Me.BarToolbarsListItem.Name = "BarToolbarsListItem"
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "0 Items"
        Me.BarButtonItem1.Id = 55
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'subReports
        '
        Me.subReports.Caption = "&Reports"
        Me.subReports.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subReports.Id = 26
        Me.subReports.Name = "subReports"
        '
        'frmExplorerHeadOffice
        '
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(968, 561)
        Me.Controls.Add(Me.pnlOutside)
        Me.Controls.Add(Me.dpNavigation)
        Me.Controls.Add(Me.hideContainerRight)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Menu = Me.MainMenu1
        Me.Name = "frmExplorerHeadOffice"
        Me.Text = "Franchise Manager - Enterprise Control"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.pnlMain, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlHeader.ResumeLayout(False)
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlOutside, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlOutside.ResumeLayout(False)
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hideContainerRight.ResumeLayout(False)
        Me.dpHelp.ResumeLayout(False)
        Me.ControlContainer1.ResumeLayout(False)
        CType(Me.pnlHelp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlHelp.ResumeLayout(False)
        Me.dpNavigation.ResumeLayout(False)
        Me.DockPanel1_Container.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmExplorer_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim w As Integer = tvwMenu.Width
        tvwMenu.Width = 0
        tvwMenu.Width = w
    End Sub

    Private Sub frmExplorer_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Power.Library.SaveBarManagerToXML(BarManager1, UserAppDataPathWithVerson)
        'Power.Library.SaveDockManagerToXML(DockManager1, UserAppDataPathWithVerson)
        Do Until BranchForms.Count = 0
            CType(BranchForms(0), frmExplorer).Close()
        Loop
        If SqlConnection.State = ConnectionState.Open Then
            SqlConnection.Close()
        End If
        Application.Exit()
    End Sub

    Public Sub ShowBranch(ByVal sender As Object, ByVal e As System.EventArgs, ByVal BRID As Integer)
        If Not CType(MainControl, lvBranches2).SelectedRow Is Nothing Then
            Dim c As Cursor = Me.Cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmExplorer(BRID, Me)
            gui.WindowState = FormWindowState.Normal
            AddBranchForm(gui)
            gui.Show()
            gui.SetUpScreen()
            Me.Cursor = c
        End If
    End Sub

    Private Sub AddBranchForm(ByVal BranchForm As frmExplorer)
        BranchForms.Add(BranchForm)
    End Sub

    Public Sub RemoveBranchForm(ByVal BranchForm As frmExplorer)
        BranchForms.Remove(BranchForm)
    End Sub

#Region " Permissions "

    Public Function SetUpScreen() As Boolean
        For Each cmd As CommandAccess In Commands
            If Not Me.BarManager1.Items(cmd.Command) Is Nothing Then  ' This will not fail if the cmd.Command is not found
                Me.BarManager1.Items(cmd.Command).Tag = cmd
                Me.BarManager1.Items(cmd.Command).Enabled = cmd.Allowed
            End If
        Next

        ' Load the treeview based on this user's permissions
        tvwMenu.LoadFromStream(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream("WindowsApplication.xmlHeadOfficeTree.xml"))
        If Not (HasRole("branches")) Then
            HideNode("BRANCHES", tvwMenu.Nodes)
        End If
        If Not HasRole("head_office_security") Then
            HideNode("USERS", tvwMenu.Nodes)
        End If
        If Not HasRole("global_settings") Then
            HideNode("GLOBAL SETTINGS", tvwMenu.Nodes)
        End If
        'Power.Library.RestoreDockManagerFromXML(DockManager1, UserAppDataPathWithVerson)
        'Power.Library.RestoreBarManagerFromXML(BarManager1, UserAppDataPathWithVerson)
    End Function

    Private Sub HideNode(ByVal Action As String, ByVal tnc As TreeNodeCollection)
        For Each node As Power.Forms.TreeNode In tnc
            If node.Action = Action Then
                node.Remove()
            Else
                HideNode(Action, node.Nodes)
            End If
        Next
    End Sub
#End Region

#Region " Navigation "

#Region " Menu Select "

    Private MainControl As Control
    Private NodeCollection As ArrayList
    Private NodeCollectionCursor As Int32
    Private Sub tvwMenu_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvwMenu.AfterSelect
        Dim OldMainControl As Control

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        ' If it has sub-nodes, expand
        tvwMenu.SelectedNode.Expand()
        ' Set up the title bar
        If Not tvwMenu.SelectedNode.ImageIndex = -1 Then
            Me.PictureBox1.Image = ImageListLarge.Images(tvwMenu.SelectedNode.ImageIndex)
        Else
            Me.PictureBox1.Image = ImageListLarge.Images(tvwMenu.SelectedImageIndex)
        End If
        lblSectionTitle.Text = tvwMenu.SelectedNode.Text

        Dim newScreenAction As String = CType(tvwMenu.SelectedNode, Power.Forms.TreeNode).Action

        If Not tvwMenu.SelectedNode.Tag Is Nothing Then
            If TypeOf tvwMenu.SelectedNode.Tag Is Control Then
                If TypeOf tvwMenu.SelectedNode.Tag Is IListControl Then
                    CType(tvwMenu.SelectedNode.Tag, IListControl).FillDataSet()
                End If
                CType(tvwMenu.SelectedNode.Tag, Control).Enabled = True
                CType(tvwMenu.SelectedNode.Tag, Control).BringToFront()
                MainControl.Enabled = False
                MainControl = CType(tvwMenu.SelectedNode.Tag, Control)
            Else
                Throw New InvalidCastException("Invalid tag object: " & tvwMenu.SelectedNode.Tag.GetType.ToString & _
                    ". Expected: System.Windows.Forms.Control.")
            End If
        Else
            ' Add new control
            Dim NewControl As Control
            If tvwMenu.SelectedNode.GetType Is (New Power.Forms.TreeNode).GetType Then
                Select Case newScreenAction
                    Case "WELCOME"
                        Dim pnl As New pnlRichText(Me, "WindowsApplication.HeadOfficeIntro.rtf", newScreenAction)
                        NewControl = pnl
                    Case "BRANCHES"
                        Dim lv As New lvBranches2(SqlConnection, Me)
                        AddHandler lv.ShowBranch, AddressOf ShowBranch
                        NewControl = lv
                    Case "USERS"
                        Dim lv As New lvUsers(SqlConnection, Me)
                        NewControl = lv
                    Case "MATERIALS"
                        Dim lv As New lvMasterMaterials2(SqlConnection, CType(tvwMenu.SelectedNode, Power.Forms.TreeNode).Filter("MGID"), Me)
                        NewControl = lv
                    Case "PRIMARYLEADSOURCES"
                        Dim lv As New lvPrimaryEnquirySources(SqlConnection, Me)
                        NewControl = lv
                    Case "JOBISSUES"
                        Dim lv As New lvJobIssues(SqlConnection, Me)
                        NewControl = lv
                    Case "JOBTYPES"
                        Dim lv As New lvJobTypes(SqlConnection, Me)
                        NewControl = lv
                    Case Else
                        Dim lv As New lvNodeList(tvwMenu.SelectedNode, ImageListLarge, ImageListSmall, Me)
                        lv.ListView.View = View.LargeIcon
                        NewControl = lv
                End Select
            Else
                Dim lv As New lvNodeList(tvwMenu.SelectedNode, ImageListLarge, ImageListSmall, Me)
                lv.ListView.View = View.LargeIcon
                NewControl = lv

                btnNew.Enabled = False
                btnEdit.Enabled = False
                btnDelete.Enabled = False
            End If
            If Not NewControl Is Nothing Then
                NewControl.Dock = DockStyle.Fill
                NewControl.Visible = False
                pnlMain.Controls.Add(NewControl)
                NewControl.SendToBack()
                NewControl.Visible = True
                NewControl.BringToFront()
                If Not MainControl Is Nothing Then
                    MainControl.Enabled = False
                End If
                tvwMenu.SelectedNode.Tag = NewControl
                MainControl = NewControl
            End If
        End If
        If Not BackForwarding Then
            Do Until NodeCollection.Count <= NodeCollectionCursor + 1
                NodeCollection.RemoveAt(NodeCollectionCursor + 1)
            Loop
            NodeCollection.Insert(NodeCollectionCursor + 1, tvwMenu.SelectedNode)
            NodeCollectionCursor = NodeCollectionCursor + 1
            'btnForward.Enabled = False
            If NodeCollectionCursor > 0 Then
                'btnBack.Enabled = True
            End If
        End If
        EnableDisable(MainControl)
        SetUpHelp(CType(tvwMenu.SelectedNode, Power.Forms.TreeNode).HelpScreen)

        ' Setting up the status bar
        If TypeOf MainControl Is IListControl Then
            UpdateNumItems(CType(MainControl, IListControl).NumItems)
            UpdateVisibleItems(CType(MainControl, IListControl).VisibleItems)
        Else
            UpdateNumItems(0)
            UpdateVisibleItems(0)
        End If


        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Public Sub UpdateNumItems(ByVal items As Integer)
        If items = 1 Then
            bsiNumItems.Caption = "1 Item"
        Else
            bsiNumItems.Caption = items & " Items"
        End If
    End Sub

    Public Sub UpdateVisibleItems(ByVal items As Integer)
        If items = 1 Then
            bsiVisibleItems.Caption = "1 Visible Item"
        Else
            bsiVisibleItems.Caption = items & " Visible Items"
        End If
    End Sub

#End Region

#Region " EnableDisable"

    Private Sub CheckEnabled(ByVal btn As DevExpress.XtraBars.BarButtonItem, ByVal enabled As Boolean)
        If btn.Tag Is Nothing Then
            btn.Enabled = enabled
        Else
            If TypeOf btn.Tag Is CommandAccess Then
                btn.Enabled = CType(btn.Tag, CommandAccess).Allowed And enabled
            Else
                Throw New Exception("The tag against " & btn.Name & " should be of type CommandAccess")
            End If
        End If
    End Sub

    Private Sub EnableDisable(ByVal MainControl As Control)
        If TypeOf MainControl Is IListControl Then
            Dim ctrl As IListControl = CType(MainControl, IListControl)
            CheckEnabled(btnNew, ctrl.AllowNew)
            CheckEnabled(btnEdit, ctrl.AllowEdit)
            CheckEnabled(btnDelete, ctrl.AllowDelete)
            CheckEnabled(btnCancel, ctrl.AllowCancel)
        Else
            CheckEnabled(btnNew, False)
            CheckEnabled(btnEdit, False)
            CheckEnabled(btnDelete, False)
            CheckEnabled(btnCancel, False)
        End If
        CheckEnabled(btnShowBranch, TypeOf MainControl Is lvBranches2)
    End Sub

#End Region

#Region " Side Panel Help "

    Private Sub SetUpHelp(ByVal richTextStreamPath As String)
        If richTextStreamPath Is Nothing Then
            LoadRichText("WindowsApplication.NoHelpAvailable.rtf")
        Else
            LoadRichText(richTextStreamPath)
        End If
    End Sub

    Private Sub LoadRichText(ByVal richTextStreamPath As String)
        rtbHelp.Font = New Font(Me.Font, FontStyle.Regular)

        rtbHelp.Visible = False ' Hide while loading
        Me.rtbHelp.LoadFile(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream(richTextStreamPath), RichTextBoxStreamType.RichText)

        Dim cursor As Integer = 1
        Do Until InStr(cursor, rtbHelp.Text, "<a") = 0
            ConvertToLink(rtbHelp, InStr(cursor, rtbHelp.Text, "<a"))
            cursor += 1
        Loop
        rtbHelp.Visible = True ' Show
    End Sub

    Public Shared Sub ConvertToLink(ByVal rtb As Power.Forms.RichTextBox, ByVal startPosition As Integer)
        Dim address, text As String

        Dim startOpenTag As Integer = startPosition
        Dim endOpenTag As Integer = InStr(startPosition, rtb.Text, ">")
        If endOpenTag = 0 Then Exit Sub ' Could not find ending '>'

        Dim nextOpenTag As Integer = InStr(startOpenTag + 1, rtb.Text, "<")
        If 0 <> nextOpenTag And nextOpenTag < endOpenTag Then Exit Sub ' Found extra '<' within tag

        Dim startAddress As Integer = InStr(startOpenTag + 1, rtb.Text, "{")
        Dim endAddress As Integer = InStr(startAddress + 1, rtb.Text, "}")
        If startAddress = 0 Or startAddress > endOpenTag Or endAddress = 0 Or endAddress > endOpenTag Then Exit Sub ' Could not find two '"'

        address = Mid(rtb.Text, startAddress + 1, endAddress - startAddress - 1)

        Dim startCloseTag As Integer = InStr(endOpenTag, rtb.Text, "</a>")
        If startCloseTag = 0 Then Exit Sub ' could not find closing tag

        text = Mid(rtb.Text, endOpenTag + 1, startCloseTag - endOpenTag - 1)

        rtb.Select(startOpenTag - 1, (startCloseTag + 3) - startOpenTag + 1) ' Don't know why we need the '- 1' in both cases, but whatever works :)

        rtb.InsertLink(text, address)
    End Sub

    Private Sub RichTextBox1_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkClickedEventArgs) Handles rtbHelp.LinkClicked
        DoHelpAction(CType(tvwMenu.SelectedNode, Power.Forms.TreeNode).Action, Mid(e.LinkText, InStr(e.LinkText, "#") + 1))
    End Sub

#Region " Help Actions "

    Private Sub DoHelpAction(ByVal screen As String, ByVal helpAction As String)
        helpAction = Trim(helpAction)
        If InStr(helpAction, "/") > 0 Then
            ShowHelpTopic(Me, Mid(helpAction, InStr(helpAction, "/") + 1))
        Else
            Select Case screen
                Case "BRANCHES"
                    Select Case helpAction
                        ' List help actions
                    End Select
            End Select
        End If
    End Sub

#End Region

#End Region

#Region " Back and Forward "

    Private BackForwarding As Boolean = False ' If this is true, then we are calling the below back forward methods
    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        NodeCollectionCursor = NodeCollectionCursor - 1
        BackForwarding = True
        tvwMenu.SelectedNode = CType(NodeCollection(NodeCollectionCursor), TreeNode)
        BackForwarding = False
        If NodeCollectionCursor <= 0 Then
            'btnBack.Enabled = False
        End If
        'btnForward.Enabled = True
    End Sub

    Private Sub btnForward_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        NodeCollectionCursor = NodeCollectionCursor + 1
        BackForwarding = True
        tvwMenu.SelectedNode = CType(NodeCollection(NodeCollectionCursor), TreeNode)
        BackForwarding = False
        If NodeCollectionCursor >= NodeCollection.Count - 1 Then
            'btnForward.Enabled = False
        End If
        'btnBack.Enabled = True
    End Sub

#End Region

#End Region

#Region " Commands "

#Region " File "

    Private Sub btnNew_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnNew.ItemClick
        CType(MainControl, IListControl).List_New()
    End Sub

    Private Sub btnEdit_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnEdit.ItemClick
        CType(MainControl, IListControl).List_Edit()
    End Sub

    Private Sub btnDelete_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnDelete.ItemClick
        CType(MainControl, IListControl).List_Delete()
    End Sub

    Private Sub btnShowBranch_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnShowBranch.ItemClick
        ShowBranch(sender, e, CType(MainControl, lvBranches2).SelectedRow.Item("BRID"))
    End Sub

    Private Sub btnExit_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnExit.ItemClick
        Me.Close()
    End Sub

#End Region

#Region " View "

    Private Sub btnAutoHideNavigationPanel_CheckedChanged(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnAutoHideNavigationPanel.CheckedChanged
        If Not IsInitializing Then
            If btnAutoHideNavigationPanel.Checked Then
                Me.dpNavigation.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide
            Else
                Me.dpNavigation.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible
            End If
        End If
    End Sub

    Private Sub dpNavigation_VisibilityChanged(ByVal sender As Object, ByVal e As DevExpress.XtraBars.Docking.VisibilityChangedEventArgs) Handles dpNavigation.VisibilityChanged
        If Not IsInitializing Then
            btnAutoHideNavigationPanel.Checked = (dpNavigation.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide)
        End If
    End Sub

    Private Sub btnAutoHideHelpPanel_CheckedChanged(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnAutoHideHelpPanel.CheckedChanged
        If Not IsInitializing Then
            If btnAutoHideHelpPanel.Checked Then
                Me.dpHelp.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide
            Else
                Me.dpHelp.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible
            End If
        End If
    End Sub

    Private Sub dpHelp_VisibilityChanged(ByVal sender As Object, ByVal e As DevExpress.XtraBars.Docking.VisibilityChangedEventArgs) Handles dpHelp.VisibilityChanged
        If Not IsInitializing Then
            btnAutoHideHelpPanel.Checked = (dpHelp.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide)
        End If
    End Sub

#End Region

#Region " Reports "

#Region " Operations "

    Private Sub btnHOJobsFinishedOnTimeReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHOJobsFinishedOnTimeReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repHOFinishedOnTime
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repFinishedOnTime](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", DBNull.Value)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repFinishedOnTime)
            cmd.CommandText = "SELECT * FROM Branches WHERE BRIncludeInReporting = 1"
            ReadUncommittedFill(da, ds.Branches)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Jobs Finished On Time Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnHODebtorStatisticsReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHODebtorStatisticsReport.ItemClick
        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        Dim gui As New frmReport(Nothing)
        Dim rep As New repHODebtorStatisticsReport
        rep.Load()
        Dim ds As New dsReports
        Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repDebtorStatisticsReport](@BRID, @GETDATE)", SqlConnection)
        cmd.Parameters.Add("@BRID", DBNull.Value)
        cmd.Parameters.Add("@GETDATE", Today.Date)
        Dim da As New SqlClient.SqlDataAdapter(cmd)
        ReadUncommittedFill(da, ds.repDebtorStatisticsReport)
        cmd.CommandText = "SELECT * FROM Branches WHERE BRIncludeInReporting = 1"
        ReadUncommittedFill(da, ds.Branches)
        rep.SetDataSource(ds)
        rep.SetParameterValue("AsOfDate", CDate(cmd.Parameters("@GETDATE").Value))
        gui.Display(rep)
        Me.Cursor = Windows.Forms.Cursors.Default
        gui.Text = "Accounts Receivable Statistics Report"
        gui.Show()
    End Sub

    Private Sub btnHOJobIssuesReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHOJobIssuesReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repHOJobIssues
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repJobIssues](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", DBNull.Value)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repJobIssues)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Job Issues Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnHODataEntryStatisticsReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHODataEntryStatisticsReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repHODataEntryStatistics
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repDataEntryStatistics](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", DBNull.Value)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repDataEntryStatistics)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Data Entry Statistics Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnHOPredictedSalesReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHOPredictedSalesReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        DateRangeGui.dpFrom.EditValue = Today.Date
        DateRangeGui.dpTo.EditValue = Nothing
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("PredictedSalesReport.html")
            Dim rep As New repHOPredictedSales
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repPredictedSales](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", DBNull.Value)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repPredictedSales)
            cmd.CommandText = "SELECT * FROM Branches WHERE BRIncludeInReporting = 1"
            ReadUncommittedFill(da, ds.Branches)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Predicted Sales Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnHOQuoteStatisticsReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHOQuoteStatisticsReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repHOAppointmentStatistics
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repAppointmentStatistics](@BRID, @FROM_DATE, @TO_DATE, @AsOf)", SqlConnection)
            cmd.Parameters.Add("@BRID", DBNull.Value)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            cmd.Parameters.Add("@AsOf", DateTime.Now)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repAppointmentStatistics)
            cmd.CommandText = "SELECT * FROM Branches WHERE BRIncludeInReporting = 1"
            ReadUncommittedFill(da, ds.Branches)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Quote Statistics Report"
            gui.Show()
        End If
    End Sub

#End Region

#Region " Profit "

    Private Sub btnHOAllJobsReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHOAllJobsReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repHOAllJobsReport
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repAllJobsReport](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", DBNull.Value)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repAllJobsReport)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "All Jobs Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnHOBranchBenchmarksReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHOBranchBenchmarksReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repHOBranchBenchmarks
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repAllJobsReport](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", DBNull.Value)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            cmd.CommandTimeout = 50
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repAllJobsReport)
            cmd.CommandText = "SELECT * FROM dbo.[repProfitReport](@BRID, @FROM_DATE, @TO_DATE)"
            ReadUncommittedFill(da, ds.repProfitReport)
            cmd.CommandText = "SELECT * FROM dbo.[repWasteReportByBranch2](@BRID, @FROM_DATE, @TO_DATE)"
            ReadUncommittedFill(da, ds.repWasteReportByBranch)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Branch Benchmarks Report"
            gui.Show()
        End If
    End Sub

#End Region

#Region " Materials "

    Private Sub btnHOStockReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHOStockReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repHOStockReport
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repStockReport2](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", DBNull.Value)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repStockReport)
            cmd.CommandText = "SELECT * FROM Branches WHERE BRIncludeInReporting = 1"
            ReadUncommittedFill(da, ds.Branches)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Inventory Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnHOWasteReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHOWasteReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repHOWasteReport
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repWasteReport2](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", DBNull.Value)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            cmd.CommandTimeout = 50
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repWasteReport)
            cmd.CommandText = "SELECT * FROM dbo.[repWasteReportByBranch2](@BRID, @FROM_DATE, @TO_DATE)"
            ReadUncommittedFill(da, ds.repWasteReportByBranch)
            cmd.CommandText = "SELECT * FROM Branches WHERE BRIncludeInReporting = 1"
            ReadUncommittedFill(da, ds.Branches)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Waste Report"
            gui.Show()
        End If

    End Sub

    Private Sub btnHOStockRequirementsReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHOStockRequirementsReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        DateRangeGui.dpFrom.EditValue = Today.Date
        DateRangeGui.dpTo.EditValue = Nothing
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repHOProductRequirements
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repProductRequirements2](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", DBNull.Value)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repProductRequirements)
            cmd.CommandText = "SELECT * FROM Branches WHERE BRIncludeInReporting = 1"
            ReadUncommittedFill(da, ds.Branches)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Tracked Material Requirements Report"
            gui.Show()
        End If
    End Sub

#End Region

#Region " Direct Labor and Salespeople "

    Private Sub btnHoursCostsAndVolumeReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHoursCostsAndVolumeReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repHoursCostAndVolumeReport
            rep.Load()
            Dim ds As New dsReportsHeadOfficeDirectLabour
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[Hours Cost and Volume Report](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", DBNull.Value)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.Hours_Cost_and_Volume_Report)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Hours, Cost and Volume Report"
            gui.Show()
        End If
    End Sub

#End Region

#Region " Marketing "

    Private Sub btnHOPrimaryEnquirySourcesReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHOPrimaryEnquirySourcesReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repHOLeadSources
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repLeadSources](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", DBNull.Value)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repLeadSources)
            cmd.CommandText = "SELECT * FROM Branches WHERE BRIncludeInReporting = 1"
            ReadUncommittedFill(da, ds.Branches)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Primary Enquiry Sources Report"
            gui.Show()
        End If
    End Sub

#End Region

#Region " Other Reports "

#End Region

#End Region

#Region " Help "

    Private Sub btnHOAboutFranchiseManager1_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHOAboutFranchiseManager1.ItemClick
        Dim gui As New frmAbout
        gui.ShowDialog(Me)
    End Sub

    Private Sub btnShowHelp_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnShowHelp.ItemClick
        Me.dpHelp.Show()
    End Sub

    Private Sub btnHelp_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHelp.ItemClick
        ShowHelp(Me)
    End Sub

#End Region

#End Region

End Class
