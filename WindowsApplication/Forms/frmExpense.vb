Public Class frmExpense
    Inherits DevExpress.XtraEditors.XtraForm

    Private DataRow As DataRow

    Public BRID As Int32
    Public EXID As Int32
    Public EGType As String

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return SqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            SqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private hTransaction As SqlClient.SqlTransaction
    Private Property Transaction() As SqlClient.SqlTransaction
        Get
            Return hTransaction
        End Get
        Set(ByVal Value As SqlClient.SqlTransaction)
            hTransaction = Value
            Power.Library.Library.ApplyTransactionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Public Shared Function Add(ByVal BRID As Int32, ByVal EGID As Int32) As frmExpense
        Dim gui As New frmExpense

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            .BRID = BRID

            .Transaction = .Connection.BeginTransaction(IsolationLevel.ReadUncommitted)
            .FillPreliminaryData()
            .EXID = spNew_Expense(BRID, EGID, .Transaction)
            .EGType = DataAccess.EGType(.BRID, .EXID, .Transaction)
            .FillEGTypeDependantData()

            If DataAccess.spExecLockRequest("sp_GetExpenseLock", .BRID, .EXID, .Transaction) Then

                .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = .BRID
                .SqlDataAdapter.SelectCommand.Parameters("@EXID").Value = .EXID
                .SqlDataAdapter.Fill(.DsGTMS)
                .DataRow = .DsGTMS.Expenses(0)

                .FillData()

            Else
                Throw New ObjectLockedException
            End If
        End With

        Return gui
    End Function

    Public Shared Function Edit(ByVal BRID As Int32, ByRef EXID As Int32) As frmExpense
        Dim gui As New frmExpense

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            .BRID = BRID
            .EXID = EXID

            .Transaction = .SqlConnection.BeginTransaction(IsolationLevel.ReadUncommitted)
            .FillPreliminaryData()
            .EGType = DataAccess.EGType(.BRID, .EXID, .Transaction)
            .FillEGTypeDependantData()

            If DataAccess.spExecLockRequest("sp_GetExpenseLock", .BRID, .EXID, .Transaction) Then

                .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = .BRID
                .SqlDataAdapter.SelectCommand.Parameters("@EXID").Value = .EXID
                .SqlDataAdapter.Fill(.DsGTMS)
                .DataRow = .DsGTMS.Expenses(0)

                .FillData()

            Else
                Throw New ObjectLockedException
            End If
        End With

        Return gui
    End Function

#Region " Windows Form Designer generated code "

    ' FOR NEW
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DsGTMS As WindowsApplication.dsGTMS
    Friend WithEvents daEXTypes As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daEXPaymentMethods As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daEXAssignToPortions As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkEXAppearInCalendar As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents txtEXName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents rgEXDiscontinued As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents tpProperties As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents txtEXPaymentMethod As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents txtEXType As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents txtEXAssignToPortion As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lblEXAssignToPortion As System.Windows.Forms.Label
    Friend WithEvents lblEXAssignToPortionDesc As System.Windows.Forms.Label
    Friend WithEvents daEXPaymentMethod_Rules As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents DsWizards As WindowsApplication.dsWizards
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim configurationAppSettings As System.Configuration.AppSettingsReader = New System.Configuration.AppSettingsReader
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmExpense))
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl
        Me.Label4 = New System.Windows.Forms.Label
        Me.rgEXDiscontinued = New DevExpress.XtraEditors.RadioGroup
        Me.DsGTMS = New WindowsApplication.dsGTMS
        Me.chkEXAppearInCalendar = New DevExpress.XtraEditors.CheckEdit
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.lblEXAssignToPortion = New System.Windows.Forms.Label
        Me.lblEXAssignToPortionDesc = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.daEXTypes = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.daEXPaymentMethods = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.daEXAssignToPortions = New System.Data.SqlClient.SqlDataAdapter
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl
        Me.tpProperties = New DevExpress.XtraTab.XtraTabPage
        Me.txtEXName = New DevExpress.XtraEditors.TextEdit
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage
        Me.txtEXAssignToPortion = New DevExpress.XtraEditors.LookUpEdit
        Me.txtEXType = New DevExpress.XtraEditors.LookUpEdit
        Me.txtEXPaymentMethod = New DevExpress.XtraEditors.LookUpEdit
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.daEXPaymentMethod_Rules = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand5 = New System.Data.SqlClient.SqlCommand
        Me.DsWizards = New WindowsApplication.dsWizards
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.rgEXDiscontinued.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkEXAppearInCalendar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.tpProperties.SuspendLayout()
        CType(Me.txtEXName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.txtEXAssignToPortion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEXType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEXPaymentMethod.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsWizards, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.GroupControl1.Appearance.Options.UseBackColor = True
        Me.GroupControl1.Controls.Add(Me.Label4)
        Me.GroupControl1.Controls.Add(Me.rgEXDiscontinued)
        Me.GroupControl1.Location = New System.Drawing.Point(8, 80)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(344, 128)
        Me.GroupControl1.TabIndex = 2
        Me.GroupControl1.Text = "Status"
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label4.Location = New System.Drawing.Point(12, 20)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(320, 48)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Setting an item as discontinued allows you to remove it from the list of availabl" & _
        "e expenses without destroying existing data, including jobs that have used this " & _
        "expense."
        '
        'rgEXDiscontinued
        '
        Me.rgEXDiscontinued.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Expenses.EXDiscontinued"))
        Me.rgEXDiscontinued.Location = New System.Drawing.Point(16, 80)
        Me.rgEXDiscontinued.Name = "rgEXDiscontinued"
        '
        'rgEXDiscontinued.Properties
        '
        Me.rgEXDiscontinued.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.rgEXDiscontinued.Properties.Appearance.Options.UseBackColor = True
        Me.rgEXDiscontinued.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.rgEXDiscontinued.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "Make this expense available to use (current)"), New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "Discontinue this expense (non current)")})
        Me.rgEXDiscontinued.Size = New System.Drawing.Size(312, 40)
        Me.rgEXDiscontinued.TabIndex = 1
        '
        'DsGTMS
        '
        Me.DsGTMS.DataSetName = "dsGTMS"
        Me.DsGTMS.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'chkEXAppearInCalendar
        '
        Me.chkEXAppearInCalendar.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Expenses.EXAppearInCalendar"))
        Me.chkEXAppearInCalendar.Location = New System.Drawing.Point(64, 48)
        Me.chkEXAppearInCalendar.Name = "chkEXAppearInCalendar"
        '
        'chkEXAppearInCalendar.Properties
        '
        Me.chkEXAppearInCalendar.Properties.Caption = "Show this expense in the calendar"
        Me.chkEXAppearInCalendar.Size = New System.Drawing.Size(200, 18)
        Me.chkEXAppearInCalendar.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 21)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Name:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(8, 136)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(96, 21)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Payment method:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(8, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(318, 32)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Expense type defines the way this expense will be classified in calculating indir" & _
        "ect tax, and labor on-costs."
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(8, 56)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(96, 21)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Expense type:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEXAssignToPortion
        '
        Me.lblEXAssignToPortion.Location = New System.Drawing.Point(8, 216)
        Me.lblEXAssignToPortion.Name = "lblEXAssignToPortion"
        Me.lblEXAssignToPortion.Size = New System.Drawing.Size(96, 21)
        Me.lblEXAssignToPortion.TabIndex = 7
        Me.lblEXAssignToPortion.Text = "Category:"
        Me.lblEXAssignToPortion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEXAssignToPortionDesc
        '
        Me.lblEXAssignToPortionDesc.Location = New System.Drawing.Point(8, 176)
        Me.lblEXAssignToPortionDesc.Name = "lblEXAssignToPortionDesc"
        Me.lblEXAssignToPortionDesc.Size = New System.Drawing.Size(320, 32)
        Me.lblEXAssignToPortionDesc.TabIndex = 6
        Me.lblEXAssignToPortionDesc.Text = "You may select a category, either Trend, Non-Trend or Both to which this expense " & _
        "will be assigned."
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(8, 96)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(318, 32)
        Me.Label8.TabIndex = 3
        Me.Label8.Text = "Select a payment method. This will determine the payment timing of the expense"
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = CType(configurationAppSettings.GetValue("SqlConnection.ConnectionString", GetType(System.String)), String)
        '
        'daEXTypes
        '
        Me.daEXTypes.DeleteCommand = Me.SqlDeleteCommand1
        Me.daEXTypes.InsertCommand = Me.SqlInsertCommand1
        Me.daEXTypes.SelectCommand = Me.SqlSelectCommand1
        Me.daEXTypes.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "EXTypes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("EXType", "EXType"), New System.Data.Common.DataColumnMapping("EXTypeDisplay", "EXTypeDisplay"), New System.Data.Common.DataColumnMapping("EXTypeDescription", "EXTypeDescription"), New System.Data.Common.DataColumnMapping("EGType", "EGType")})})
        Me.daEXTypes.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM EXTypes WHERE (EGType = @Original_EGType) AND (EXType = @Original_EXT" & _
        "ype) AND (EXTypeDescription = @Original_EXTypeDescription OR @Original_EXTypeDes" & _
        "cription IS NULL AND EXTypeDescription IS NULL) AND (EXTypeDisplay = @Original_E" & _
        "XTypeDisplay OR @Original_EXTypeDisplay IS NULL AND EXTypeDisplay IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGType", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXTypeDescription", System.Data.SqlDbType.VarChar, 200, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXTypeDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXTypeDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXTypeDisplay", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO EXTypes(EXType, EXTypeDisplay, EXTypeDescription, EGType) VALUES (@EX" & _
        "Type, @EXTypeDisplay, @EXTypeDescription, @EGType); SELECT EXType, EXTypeDisplay" & _
        ", EXTypeDescription, EGType FROM EXTypes WHERE (EGType = @EGType) AND (EXType = " & _
        "@EXType)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXType", System.Data.SqlDbType.VarChar, 2, "EXType"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXTypeDisplay", System.Data.SqlDbType.VarChar, 50, "EXTypeDisplay"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXTypeDescription", System.Data.SqlDbType.VarChar, 200, "EXTypeDescription"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 50, "EGType"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT EXType, EXTypeDisplay, EXTypeDescription, EGType FROM EXTypes WHERE (EGTyp" & _
        "e = @EGType)"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 50, "EGType"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE EXTypes SET EXType = @EXType, EXTypeDisplay = @EXTypeDisplay, EXTypeDescri" & _
        "ption = @EXTypeDescription, EGType = @EGType WHERE (EGType = @Original_EGType) A" & _
        "ND (EXType = @Original_EXType) AND (EXTypeDescription = @Original_EXTypeDescript" & _
        "ion OR @Original_EXTypeDescription IS NULL AND EXTypeDescription IS NULL) AND (E" & _
        "XTypeDisplay = @Original_EXTypeDisplay OR @Original_EXTypeDisplay IS NULL AND EX" & _
        "TypeDisplay IS NULL); SELECT EXType, EXTypeDisplay, EXTypeDescription, EGType FR" & _
        "OM EXTypes WHERE (EGType = @EGType) AND (EXType = @EXType)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXType", System.Data.SqlDbType.VarChar, 2, "EXType"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXTypeDisplay", System.Data.SqlDbType.VarChar, 50, "EXTypeDisplay"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXTypeDescription", System.Data.SqlDbType.VarChar, 200, "EXTypeDescription"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 50, "EGType"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGType", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXTypeDescription", System.Data.SqlDbType.VarChar, 200, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXTypeDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXTypeDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXTypeDisplay", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand2
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand2
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand2
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Expenses", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("EXName", "EXName"), New System.Data.Common.DataColumnMapping("EXType", "EXType"), New System.Data.Common.DataColumnMapping("EGID", "EGID"), New System.Data.Common.DataColumnMapping("EXDiscontinued", "EXDiscontinued"), New System.Data.Common.DataColumnMapping("EXPaymentMethod", "EXPaymentMethod"), New System.Data.Common.DataColumnMapping("EGType", "EGType"), New System.Data.Common.DataColumnMapping("EXAssignToPortion", "EXAssignToPortion"), New System.Data.Common.DataColumnMapping("EXAppearInCalendar", "EXAppearInCalendar")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM Expenses WHERE (BRID = @Original_BRID) AND (EXID = @Original_EXID) AN" & _
        "D (EGID = @Original_EGID OR @Original_EGID IS NULL AND EGID IS NULL) AND (EGType" & _
        " = @Original_EGType OR @Original_EGType IS NULL AND EGType IS NULL) AND (EXAppea" & _
        "rInCalendar = @Original_EXAppearInCalendar OR @Original_EXAppearInCalendar IS NU" & _
        "LL AND EXAppearInCalendar IS NULL) AND (EXAssignToPortion = @Original_EXAssignTo" & _
        "Portion OR @Original_EXAssignToPortion IS NULL AND EXAssignToPortion IS NULL) AN" & _
        "D (EXDiscontinued = @Original_EXDiscontinued OR @Original_EXDiscontinued IS NULL" & _
        " AND EXDiscontinued IS NULL) AND (EXName = @Original_EXName OR @Original_EXName " & _
        "IS NULL AND EXName IS NULL) AND (EXPaymentMethod = @Original_EXPaymentMethod OR " & _
        "@Original_EXPaymentMethod IS NULL AND EXPaymentMethod IS NULL) AND (EXType = @Or" & _
        "iginal_EXType OR @Original_EXType IS NULL AND EXType IS NULL)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXAppearInCalendar", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXAppearInCalendar", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXAssignToPortion", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXAssignToPortion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXDiscontinued", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXDiscontinued", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXPaymentMethod", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXType", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO Expenses(BRID, EXName, EXType, EGID, EXDiscontinued, EXPaymentMethod," & _
        " EXAssignToPortion, EXAppearInCalendar) VALUES (@BRID, @EXName, @EXType, @EGID, " & _
        "@EXDiscontinued, @EXPaymentMethod, @EXAssignToPortion, @EXAppearInCalendar); SEL" & _
        "ECT BRID, EXID, EXName, EXType, EGID, EXDiscontinued, EXPaymentMethod, EGType, E" & _
        "XAssignToPortion, EXAppearInCalendar FROM Expenses WHERE (BRID = @BRID) AND (EXI" & _
        "D = @@IDENTITY)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXType", System.Data.SqlDbType.VarChar, 2, "EXType"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGID", System.Data.SqlDbType.SmallInt, 2, "EGID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXDiscontinued", System.Data.SqlDbType.Bit, 1, "EXDiscontinued"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, "EXPaymentMethod"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAssignToPortion", System.Data.SqlDbType.VarChar, 2, "EXAssignToPortion"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAppearInCalendar", System.Data.SqlDbType.Bit, 1, "EXAppearInCalendar"))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT BRID, EXID, EXName, EXType, EGID, EXDiscontinued, EXPaymentMethod, EGType," & _
        " EXAssignToPortion, EXAppearInCalendar FROM Expenses WHERE (BRID = @BRID) AND (E" & _
        "XID = @EXID)"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection
        Me.SqlSelectCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE Expenses SET BRID = @BRID, EXName = @EXName, EXType = @EXType, EGID = @EGI" & _
        "D, EXDiscontinued = @EXDiscontinued, EXPaymentMethod = @EXPaymentMethod, EXAssig" & _
        "nToPortion = @EXAssignToPortion, EXAppearInCalendar = @EXAppearInCalendar WHERE " & _
        "(BRID = @Original_BRID) AND (EXID = @Original_EXID) AND (EGID = @Original_EGID O" & _
        "R @Original_EGID IS NULL AND EGID IS NULL) AND (EXAppearInCalendar = @Original_E" & _
        "XAppearInCalendar OR @Original_EXAppearInCalendar IS NULL AND EXAppearInCalendar" & _
        " IS NULL) AND (EXAssignToPortion = @Original_EXAssignToPortion OR @Original_EXAs" & _
        "signToPortion IS NULL AND EXAssignToPortion IS NULL) AND (EXDiscontinued = @Orig" & _
        "inal_EXDiscontinued OR @Original_EXDiscontinued IS NULL AND EXDiscontinued IS NU" & _
        "LL) AND (EXName = @Original_EXName OR @Original_EXName IS NULL AND EXName IS NUL" & _
        "L) AND (EXPaymentMethod = @Original_EXPaymentMethod OR @Original_EXPaymentMethod" & _
        " IS NULL AND EXPaymentMethod IS NULL) AND (EXType = @Original_EXType OR @Origina" & _
        "l_EXType IS NULL AND EXType IS NULL); SELECT BRID, EXID, EXName, EXType, EGID, E" & _
        "XDiscontinued, EXPaymentMethod, EGType, EXAssignToPortion, EXAppearInCalendar FR" & _
        "OM Expenses WHERE (BRID = @BRID) AND (EXID = @EXID)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXType", System.Data.SqlDbType.VarChar, 2, "EXType"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGID", System.Data.SqlDbType.SmallInt, 2, "EGID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXDiscontinued", System.Data.SqlDbType.Bit, 1, "EXDiscontinued"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, "EXPaymentMethod"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAssignToPortion", System.Data.SqlDbType.VarChar, 2, "EXAssignToPortion"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAppearInCalendar", System.Data.SqlDbType.Bit, 1, "EXAppearInCalendar"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXAppearInCalendar", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXAppearInCalendar", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXAssignToPortion", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXAssignToPortion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXDiscontinued", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXDiscontinued", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXPaymentMethod", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        '
        'daEXPaymentMethods
        '
        Me.daEXPaymentMethods.DeleteCommand = Me.SqlDeleteCommand4
        Me.daEXPaymentMethods.InsertCommand = Me.SqlInsertCommand4
        Me.daEXPaymentMethods.SelectCommand = Me.SqlSelectCommand4
        Me.daEXPaymentMethods.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "EXPaymentMethods", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("EGType", "EGType"), New System.Data.Common.DataColumnMapping("EXPaymentMethod", "EXPaymentMethod"), New System.Data.Common.DataColumnMapping("EXPaymentMethodDisplay", "EXPaymentMethodDisplay"), New System.Data.Common.DataColumnMapping("EXPaymentMethodDescription", "EXPaymentMethodDescription")})})
        Me.daEXPaymentMethods.UpdateCommand = Me.SqlUpdateCommand4
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = "DELETE FROM EXPaymentMethods WHERE (EGType = @Original_EGType) AND (EXPaymentMeth" & _
        "od = @Original_EXPaymentMethod) AND (EXPaymentMethodDescription = @Original_EXPa" & _
        "ymentMethodDescription OR @Original_EXPaymentMethodDescription IS NULL AND EXPay" & _
        "mentMethodDescription IS NULL) AND (EXPaymentMethodDisplay = @Original_EXPayment" & _
        "MethodDisplay OR @Original_EXPaymentMethodDisplay IS NULL AND EXPaymentMethodDis" & _
        "play IS NULL)"
        Me.SqlDeleteCommand4.Connection = Me.SqlConnection
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXPaymentMethod", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXPaymentMethodDescription", System.Data.SqlDbType.VarChar, 300, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXPaymentMethodDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXPaymentMethodDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXPaymentMethodDisplay", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = "INSERT INTO EXPaymentMethods (EGType, EXPaymentMethod, EXPaymentMethodDisplay, EX" & _
        "PaymentMethodDescription) VALUES (@EGType, @EXPaymentMethod, @EXPaymentMethodDis" & _
        "play, @EXPaymentMethodDescription); SELECT EGType, EXPaymentMethod, EXPaymentMet" & _
        "hodDisplay, EXPaymentMethodDescription FROM EXPaymentMethods WHERE (EGType = @EG" & _
        "Type) AND (EXPaymentMethod = @EXPaymentMethod)"
        Me.SqlInsertCommand4.Connection = Me.SqlConnection
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 2, "EGType"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, "EXPaymentMethod"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXPaymentMethodDisplay", System.Data.SqlDbType.VarChar, 50, "EXPaymentMethodDisplay"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXPaymentMethodDescription", System.Data.SqlDbType.VarChar, 300, "EXPaymentMethodDescription"))
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT EGType, EXPaymentMethod, EXPaymentMethodDisplay, EXPaymentMethodDescriptio" & _
        "n FROM EXPaymentMethods WHERE (EGType = @EGType) ORDER BY EXPaymentMethodSeq"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection
        Me.SqlSelectCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 2, "EGType"))
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = "UPDATE EXPaymentMethods SET EGType = @EGType, EXPaymentMethod = @EXPaymentMethod," & _
        " EXPaymentMethodDisplay = @EXPaymentMethodDisplay, EXPaymentMethodDescription = " & _
        "@EXPaymentMethodDescription WHERE (EGType = @Original_EGType) AND (EXPaymentMeth" & _
        "od = @Original_EXPaymentMethod) AND (EXPaymentMethodDescription = @Original_EXPa" & _
        "ymentMethodDescription OR @Original_EXPaymentMethodDescription IS NULL AND EXPay" & _
        "mentMethodDescription IS NULL) AND (EXPaymentMethodDisplay = @Original_EXPayment" & _
        "MethodDisplay OR @Original_EXPaymentMethodDisplay IS NULL AND EXPaymentMethodDis" & _
        "play IS NULL); SELECT EGType, EXPaymentMethod, EXPaymentMethodDisplay, EXPayment" & _
        "MethodDescription FROM EXPaymentMethods WHERE (EGType = @EGType) AND (EXPaymentM" & _
        "ethod = @EXPaymentMethod)"
        Me.SqlUpdateCommand4.Connection = Me.SqlConnection
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 2, "EGType"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, "EXPaymentMethod"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXPaymentMethodDisplay", System.Data.SqlDbType.VarChar, 50, "EXPaymentMethodDisplay"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXPaymentMethodDescription", System.Data.SqlDbType.VarChar, 300, "EXPaymentMethodDescription"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXPaymentMethod", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXPaymentMethodDescription", System.Data.SqlDbType.VarChar, 300, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXPaymentMethodDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXPaymentMethodDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXPaymentMethodDisplay", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT EXAssignToPortion, EXAssignToPortionDisplay, EGType FROM EXAssignToPortion" & _
        "s WHERE (EGType = @EGType)"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection
        Me.SqlSelectCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 2, "EGType"))
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO EXAssignToPortions(EGType, EXAssignToPortion, EXAssignToPortionDispla" & _
        "y) VALUES (@EGType, @EXAssignToPortion, @EXAssignToPortionDisplay); SELECT EGTyp" & _
        "e, EXAssignToPortion, EXAssignToPortionDisplay FROM EXAssignToPortions WHERE (EG" & _
        "Type = @EGType) AND (EXAssignToPortion = @EXAssignToPortion)"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 2, "EGType"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAssignToPortion", System.Data.SqlDbType.VarChar, 2, "EXAssignToPortion"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAssignToPortionDisplay", System.Data.SqlDbType.VarChar, 50, "EXAssignToPortionDisplay"))
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE EXAssignToPortions SET EGType = @EGType, EXAssignToPortion = @EXAssignToPo" & _
        "rtion, EXAssignToPortionDisplay = @EXAssignToPortionDisplay WHERE (EGType = @Ori" & _
        "ginal_EGType) AND (EXAssignToPortion = @Original_EXAssignToPortion) AND (EXAssig" & _
        "nToPortionDisplay = @Original_EXAssignToPortionDisplay OR @Original_EXAssignToPo" & _
        "rtionDisplay IS NULL AND EXAssignToPortionDisplay IS NULL); SELECT EGType, EXAss" & _
        "ignToPortion, EXAssignToPortionDisplay FROM EXAssignToPortions WHERE (EGType = @" & _
        "EGType) AND (EXAssignToPortion = @EXAssignToPortion)"
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 2, "EGType"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAssignToPortion", System.Data.SqlDbType.VarChar, 2, "EXAssignToPortion"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAssignToPortionDisplay", System.Data.SqlDbType.VarChar, 50, "EXAssignToPortionDisplay"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXAssignToPortion", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXAssignToPortion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXAssignToPortionDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXAssignToPortionDisplay", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM EXAssignToPortions WHERE (EGType = @Original_EGType) AND (EXAssignToP" & _
        "ortion = @Original_EXAssignToPortion) AND (EXAssignToPortionDisplay = @Original_" & _
        "EXAssignToPortionDisplay OR @Original_EXAssignToPortionDisplay IS NULL AND EXAss" & _
        "ignToPortionDisplay IS NULL)"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EGType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EGType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXAssignToPortion", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXAssignToPortion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXAssignToPortionDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXAssignToPortionDisplay", System.Data.DataRowVersion.Original, Nothing))
        '
        'daEXAssignToPortions
        '
        Me.daEXAssignToPortions.DeleteCommand = Me.SqlDeleteCommand3
        Me.daEXAssignToPortions.InsertCommand = Me.SqlInsertCommand3
        Me.daEXAssignToPortions.SelectCommand = Me.SqlSelectCommand3
        Me.daEXAssignToPortions.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "EXAssignToPortions", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("EGType", "EGType"), New System.Data.Common.DataColumnMapping("EXAssignToPortion", "EXAssignToPortion"), New System.Data.Common.DataColumnMapping("EXAssignToPortionDisplay", "EXAssignToPortionDisplay")})})
        Me.daEXAssignToPortions.UpdateCommand = Me.SqlUpdateCommand3
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Location = New System.Drawing.Point(8, 8)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.tpProperties
        Me.XtraTabControl1.Size = New System.Drawing.Size(368, 280)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tpProperties, Me.XtraTabPage2})
        Me.XtraTabControl1.Text = "XtraTabControl1"
        '
        'tpProperties
        '
        Me.tpProperties.Controls.Add(Me.txtEXName)
        Me.tpProperties.Controls.Add(Me.Label1)
        Me.tpProperties.Controls.Add(Me.chkEXAppearInCalendar)
        Me.tpProperties.Controls.Add(Me.GroupControl1)
        Me.tpProperties.Name = "tpProperties"
        Me.tpProperties.Size = New System.Drawing.Size(362, 254)
        Me.tpProperties.Text = "Expense Properties"
        '
        'txtEXName
        '
        Me.txtEXName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Expenses.EXName"))
        Me.txtEXName.EditValue = ""
        Me.txtEXName.Location = New System.Drawing.Point(64, 16)
        Me.txtEXName.Name = "txtEXName"
        Me.txtEXName.Size = New System.Drawing.Size(280, 20)
        Me.txtEXName.TabIndex = 0
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.txtEXAssignToPortion)
        Me.XtraTabPage2.Controls.Add(Me.txtEXType)
        Me.XtraTabPage2.Controls.Add(Me.txtEXPaymentMethod)
        Me.XtraTabPage2.Controls.Add(Me.lblEXAssignToPortionDesc)
        Me.XtraTabPage2.Controls.Add(Me.Label2)
        Me.XtraTabPage2.Controls.Add(Me.Label5)
        Me.XtraTabPage2.Controls.Add(Me.Label3)
        Me.XtraTabPage2.Controls.Add(Me.lblEXAssignToPortion)
        Me.XtraTabPage2.Controls.Add(Me.Label8)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(362, 254)
        Me.XtraTabPage2.Text = "Costing"
        '
        'txtEXAssignToPortion
        '
        Me.txtEXAssignToPortion.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Expenses.EXAssignToPortion"))
        Me.txtEXAssignToPortion.Location = New System.Drawing.Point(96, 216)
        Me.txtEXAssignToPortion.Name = "txtEXAssignToPortion"
        '
        'txtEXAssignToPortion.Properties
        '
        Me.txtEXAssignToPortion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtEXAssignToPortion.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXAssignToPortionDisplay")})
        Me.txtEXAssignToPortion.Properties.DataSource = Me.DsGTMS.EXAssignToPortions
        Me.txtEXAssignToPortion.Properties.DisplayMember = "EXAssignToPortionDisplay"
        Me.txtEXAssignToPortion.Properties.NullText = ""
        Me.txtEXAssignToPortion.Properties.ShowFooter = False
        Me.txtEXAssignToPortion.Properties.ShowHeader = False
        Me.txtEXAssignToPortion.Properties.ValueMember = "EXAssignToPortion"
        Me.txtEXAssignToPortion.Size = New System.Drawing.Size(256, 20)
        Me.txtEXAssignToPortion.TabIndex = 8
        '
        'txtEXType
        '
        Me.txtEXType.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Expenses.EXType"))
        Me.txtEXType.Location = New System.Drawing.Point(96, 56)
        Me.txtEXType.Name = "txtEXType"
        '
        'txtEXType.Properties
        '
        Me.txtEXType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtEXType.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXTypeDisplay")})
        Me.txtEXType.Properties.DataSource = Me.DsGTMS.EXTypes
        Me.txtEXType.Properties.DisplayMember = "EXTypeDisplay"
        Me.txtEXType.Properties.NullText = ""
        Me.txtEXType.Properties.ShowFooter = False
        Me.txtEXType.Properties.ShowHeader = False
        Me.txtEXType.Properties.ValueMember = "EXType"
        Me.txtEXType.Size = New System.Drawing.Size(256, 20)
        Me.txtEXType.TabIndex = 2
        '
        'txtEXPaymentMethod
        '
        Me.txtEXPaymentMethod.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Expenses.EXPaymentMethod"))
        Me.txtEXPaymentMethod.Location = New System.Drawing.Point(96, 136)
        Me.txtEXPaymentMethod.Name = "txtEXPaymentMethod"
        '
        'txtEXPaymentMethod.Properties
        '
        Me.txtEXPaymentMethod.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtEXPaymentMethod.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXPaymentMethodDisplay", "", 100, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None)})
        Me.txtEXPaymentMethod.Properties.DataSource = Me.DsGTMS.EXPaymentMethods
        Me.txtEXPaymentMethod.Properties.DisplayMember = "EXPaymentMethodDisplay"
        Me.txtEXPaymentMethod.Properties.NullText = ""
        Me.txtEXPaymentMethod.Properties.ShowFooter = False
        Me.txtEXPaymentMethod.Properties.ShowHeader = False
        Me.txtEXPaymentMethod.Properties.ValueMember = "EXPaymentMethod"
        Me.txtEXPaymentMethod.Size = New System.Drawing.Size(256, 20)
        Me.txtEXPaymentMethod.TabIndex = 5
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(224, 296)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "OK"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Image = CType(resources.GetObject("SimpleButton1.Image"), System.Drawing.Image)
        Me.SimpleButton1.Location = New System.Drawing.Point(8, 296)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(72, 23)
        Me.SimpleButton1.TabIndex = 1
        Me.SimpleButton1.Text = "Help"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(304, 296)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'ImageList1
        '
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'daEXPaymentMethod_Rules
        '
        Me.daEXPaymentMethod_Rules.DeleteCommand = Me.SqlDeleteCommand5
        Me.daEXPaymentMethod_Rules.InsertCommand = Me.SqlInsertCommand5
        Me.daEXPaymentMethod_Rules.SelectCommand = Me.SqlSelectCommand5
        Me.daEXPaymentMethod_Rules.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "EXPaymentMethod_Rules", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("EXPaymentMethod", "EXPaymentMethod"), New System.Data.Common.DataColumnMapping("PMShowInJobScreen", "PMShowInJobScreen"), New System.Data.Common.DataColumnMapping("PMShowInReceipts", "PMShowInReceipts"), New System.Data.Common.DataColumnMapping("PMShowInAllowances", "PMShowInAllowances"), New System.Data.Common.DataColumnMapping("PMShowInBonuses", "PMShowInBonuses"), New System.Data.Common.DataColumnMapping("PMReceiptsPaymentTypeDisplay", "PMReceiptsPaymentTypeDisplay"), New System.Data.Common.DataColumnMapping("PMAllowancesPaymentTypeDisplay", "PMAllowancesPaymentTypeDisplay"), New System.Data.Common.DataColumnMapping("PMBonusesPaymentTypeDisplay", "PMBonusesPaymentTypeDisplay"), New System.Data.Common.DataColumnMapping("PMAllowValueInJobScreen", "PMAllowValueInJobScreen")})})
        Me.daEXPaymentMethod_Rules.UpdateCommand = Me.SqlUpdateCommand5
        '
        'SqlDeleteCommand5
        '
        Me.SqlDeleteCommand5.CommandText = "DELETE FROM EXPaymentMethod_Rules WHERE (EXPaymentMethod = @Original_EXPaymentMet" & _
        "hod) AND (PMAllowValueInJobScreen = @Original_PMAllowValueInJobScreen) AND (PMAl" & _
        "lowancesPaymentTypeDisplay = @Original_PMAllowancesPaymentTypeDisplay OR @Origin" & _
        "al_PMAllowancesPaymentTypeDisplay IS NULL AND PMAllowancesPaymentTypeDisplay IS " & _
        "NULL) AND (PMBonusesPaymentTypeDisplay = @Original_PMBonusesPaymentTypeDisplay O" & _
        "R @Original_PMBonusesPaymentTypeDisplay IS NULL AND PMBonusesPaymentTypeDisplay " & _
        "IS NULL) AND (PMReceiptsPaymentTypeDisplay = @Original_PMReceiptsPaymentTypeDisp" & _
        "lay OR @Original_PMReceiptsPaymentTypeDisplay IS NULL AND PMReceiptsPaymentTypeD" & _
        "isplay IS NULL) AND (PMShowInAllowances = @Original_PMShowInAllowances) AND (PMS" & _
        "howInBonuses = @Original_PMShowInBonuses) AND (PMShowInJobScreen = @Original_PMS" & _
        "howInJobScreen) AND (PMShowInReceipts = @Original_PMShowInReceipts)"
        Me.SqlDeleteCommand5.Connection = Me.SqlConnection
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXPaymentMethod", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMAllowValueInJobScreen", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMAllowValueInJobScreen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMAllowancesPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMAllowancesPaymentTypeDisplay", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMBonusesPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMBonusesPaymentTypeDisplay", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMReceiptsPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMReceiptsPaymentTypeDisplay", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMShowInAllowances", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMShowInAllowances", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMShowInBonuses", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMShowInBonuses", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMShowInJobScreen", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMShowInJobScreen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMShowInReceipts", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMShowInReceipts", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand5
        '
        Me.SqlInsertCommand5.CommandText = "INSERT INTO EXPaymentMethod_Rules(EXPaymentMethod, PMShowInJobScreen, PMShowInRec" & _
        "eipts, PMShowInAllowances, PMShowInBonuses, PMReceiptsPaymentTypeDisplay, PMAllo" & _
        "wancesPaymentTypeDisplay, PMBonusesPaymentTypeDisplay, PMAllowValueInJobScreen) " & _
        "VALUES (@EXPaymentMethod, @PMShowInJobScreen, @PMShowInReceipts, @PMShowInAllowa" & _
        "nces, @PMShowInBonuses, @PMReceiptsPaymentTypeDisplay, @PMAllowancesPaymentTypeD" & _
        "isplay, @PMBonusesPaymentTypeDisplay, @PMAllowValueInJobScreen); SELECT EXPaymen" & _
        "tMethod, PMShowInJobScreen, PMShowInReceipts, PMShowInAllowances, PMShowInBonuse" & _
        "s, PMReceiptsPaymentTypeDisplay, PMAllowancesPaymentTypeDisplay, PMBonusesPaymen" & _
        "tTypeDisplay, PMAllowValueInJobScreen FROM EXPaymentMethod_Rules WHERE (EXPaymen" & _
        "tMethod = @EXPaymentMethod)"
        Me.SqlInsertCommand5.Connection = Me.SqlConnection
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, "EXPaymentMethod"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMShowInJobScreen", System.Data.SqlDbType.Bit, 1, "PMShowInJobScreen"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMShowInReceipts", System.Data.SqlDbType.Bit, 1, "PMShowInReceipts"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMShowInAllowances", System.Data.SqlDbType.Bit, 1, "PMShowInAllowances"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMShowInBonuses", System.Data.SqlDbType.Bit, 1, "PMShowInBonuses"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMReceiptsPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, "PMReceiptsPaymentTypeDisplay"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMAllowancesPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, "PMAllowancesPaymentTypeDisplay"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMBonusesPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, "PMBonusesPaymentTypeDisplay"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMAllowValueInJobScreen", System.Data.SqlDbType.Bit, 1, "PMAllowValueInJobScreen"))
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT EXPaymentMethod, PMShowInJobScreen, PMShowInReceipts, PMShowInAllowances, " & _
        "PMShowInBonuses, PMReceiptsPaymentTypeDisplay, PMAllowancesPaymentTypeDisplay, P" & _
        "MBonusesPaymentTypeDisplay, PMAllowValueInJobScreen FROM EXPaymentMethod_Rules"
        Me.SqlSelectCommand5.Connection = Me.SqlConnection
        '
        'SqlUpdateCommand5
        '
        Me.SqlUpdateCommand5.CommandText = "UPDATE EXPaymentMethod_Rules SET EXPaymentMethod = @EXPaymentMethod, PMShowInJobS" & _
        "creen = @PMShowInJobScreen, PMShowInReceipts = @PMShowInReceipts, PMShowInAllowa" & _
        "nces = @PMShowInAllowances, PMShowInBonuses = @PMShowInBonuses, PMReceiptsPaymen" & _
        "tTypeDisplay = @PMReceiptsPaymentTypeDisplay, PMAllowancesPaymentTypeDisplay = @" & _
        "PMAllowancesPaymentTypeDisplay, PMBonusesPaymentTypeDisplay = @PMBonusesPaymentT" & _
        "ypeDisplay, PMAllowValueInJobScreen = @PMAllowValueInJobScreen WHERE (EXPaymentM" & _
        "ethod = @Original_EXPaymentMethod) AND (PMAllowValueInJobScreen = @Original_PMAl" & _
        "lowValueInJobScreen) AND (PMAllowancesPaymentTypeDisplay = @Original_PMAllowance" & _
        "sPaymentTypeDisplay OR @Original_PMAllowancesPaymentTypeDisplay IS NULL AND PMAl" & _
        "lowancesPaymentTypeDisplay IS NULL) AND (PMBonusesPaymentTypeDisplay = @Original" & _
        "_PMBonusesPaymentTypeDisplay OR @Original_PMBonusesPaymentTypeDisplay IS NULL AN" & _
        "D PMBonusesPaymentTypeDisplay IS NULL) AND (PMReceiptsPaymentTypeDisplay = @Orig" & _
        "inal_PMReceiptsPaymentTypeDisplay OR @Original_PMReceiptsPaymentTypeDisplay IS N" & _
        "ULL AND PMReceiptsPaymentTypeDisplay IS NULL) AND (PMShowInAllowances = @Origina" & _
        "l_PMShowInAllowances) AND (PMShowInBonuses = @Original_PMShowInBonuses) AND (PMS" & _
        "howInJobScreen = @Original_PMShowInJobScreen) AND (PMShowInReceipts = @Original_" & _
        "PMShowInReceipts); SELECT EXPaymentMethod, PMShowInJobScreen, PMShowInReceipts, " & _
        "PMShowInAllowances, PMShowInBonuses, PMReceiptsPaymentTypeDisplay, PMAllowancesP" & _
        "aymentTypeDisplay, PMBonusesPaymentTypeDisplay, PMAllowValueInJobScreen FROM EXP" & _
        "aymentMethod_Rules WHERE (EXPaymentMethod = @EXPaymentMethod)"
        Me.SqlUpdateCommand5.Connection = Me.SqlConnection
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, "EXPaymentMethod"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMShowInJobScreen", System.Data.SqlDbType.Bit, 1, "PMShowInJobScreen"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMShowInReceipts", System.Data.SqlDbType.Bit, 1, "PMShowInReceipts"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMShowInAllowances", System.Data.SqlDbType.Bit, 1, "PMShowInAllowances"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMShowInBonuses", System.Data.SqlDbType.Bit, 1, "PMShowInBonuses"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMReceiptsPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, "PMReceiptsPaymentTypeDisplay"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMAllowancesPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, "PMAllowancesPaymentTypeDisplay"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMBonusesPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, "PMBonusesPaymentTypeDisplay"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMAllowValueInJobScreen", System.Data.SqlDbType.Bit, 1, "PMAllowValueInJobScreen"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXPaymentMethod", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMAllowValueInJobScreen", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMAllowValueInJobScreen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMAllowancesPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMAllowancesPaymentTypeDisplay", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMBonusesPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMBonusesPaymentTypeDisplay", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMReceiptsPaymentTypeDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMReceiptsPaymentTypeDisplay", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMShowInAllowances", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMShowInAllowances", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMShowInBonuses", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMShowInBonuses", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMShowInJobScreen", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMShowInJobScreen", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PMShowInReceipts", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PMShowInReceipts", System.Data.DataRowVersion.Original, Nothing))
        '
        'DsWizards
        '
        Me.DsWizards.DataSetName = "dsWizards"
        Me.DsWizards.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'frmExpense
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(386, 328)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExpense"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Expense"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.rgEXDiscontinued.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkEXAppearInCalendar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.tpProperties.ResumeLayout(False)
        CType(Me.txtEXName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.txtEXAssignToPortion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEXType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEXPaymentMethod.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsWizards, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnOK.Enabled = Not HasRole("branch_read_only")
    End Sub

    ' This data must be loaded BEFORE the DataRow is loaded (eg lookups etc)
    Private Sub FillPreliminaryData()
        daEXPaymentMethod_Rules.Fill(DsWizards)
    End Sub

    Private Sub FillEGTypeDependantData()
        ' EXTypes
        daEXTypes.SelectCommand.Parameters("@EGType").Value = EGType
        daEXTypes.Fill(DsGTMS)

        ' EXAssignToPortions
        daEXAssignToPortions.SelectCommand.Parameters("@EGType").Value = EGType
        daEXAssignToPortions.Fill(DsGTMS)

        ' EXPaymentMethods
        daEXPaymentMethods.SelectCommand.Parameters("@EGType").Value = EGType
        daEXPaymentMethods.Fill(DsGTMS)
    End Sub

    ' This data must be loaded AFTER the DataRow is loaded (eg record related data)
    Private Sub FillData()
        CustomizeScreen()
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        ' EndEdit() to end editing the dataset record so that we can update
        DataRow.EndEdit()
        If ValidateForm() Then
            SqlDataAdapter.Update(DsGTMS)
            DataAccess.spExecLockRequest("sp_ReleaseExpenseLock", BRID, EXID, Transaction)
            Transaction.Commit()
            SqlConnection.Close()

            OK = True
            Me.Close()
        End If
    End Sub

    Private Function ValidateForm() As Boolean
        If DataRow("EXName") Is DBNull.Value Then
            Message.ShowMessage("You must enter a Name.", MessageBoxIcon.Exclamation)
            Return False
        End If
        If DataRow("EXType") Is DBNull.Value Then
            Message.ShowMessage("You must enter an expense type.", MessageBoxIcon.Exclamation)
            Return False
        End If
        If DataRow("EXPaymentMethod") Is DBNull.Value Then
            Message.ShowMessage("You must enter a payment method.", MessageBoxIcon.Exclamation)
            Return False
        End If
        If DataRow("EXAssignToPortion") Is DBNull.Value Then
            Message.ShowMessage("You must enter a category.", MessageBoxIcon.Exclamation)
            Return False
        End If
        Dim view As New System.Data.DataView(DataRow.Table)
        view.RowStateFilter = DataViewRowState.OriginalRows
        Dim OldDataRow = view(0)
        If ShowAnnualAmount(OldDataRow("EXPaymentMethod")) And Not ShowAnnualAmount(DataRow("EXPaymentMethod")) Or _
        Not OldDataRow("EXDiscontinued") And DataRow("EXDiscontinued") Then
            Dim gui As New frmExpenseEndAnnualAmount(BRID, EXID, Transaction)
            If gui.ShowDialog = DialogResult.Cancel Then
                Return False
            End If
        End If
        If Not ShowAnnualAmount(OldDataRow("EXPaymentMethod")) And ShowAnnualAmount(DataRow("EXPaymentMethod")) Or _
        OldDataRow("EXDiscontinued") And Not DataRow("EXDiscontinued") Then
            Dim gui As New frmExpenseEnterAnnualAmount(BRID, EXID, Transaction)
            If gui.ShowDialog = DialogResult.Cancel Then
                Return False
            End If
        End If
        Return True
    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private HasChanges As Boolean = False
    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            DataRow.EndEdit()
            If HasChanges Or DsGTMS.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If response = MsgBoxResult.Yes Then
                DataAccess.spExecLockRequest("sp_ReleaseExpenseLock", BRID, EXID, Transaction)
                Transaction.Rollback()
                SqlConnection.Close()
            Else
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub EnableDisable()
    End Sub

    Private Sub CustomizeScreen()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmExpense))
        Select Case DataRow("EGType")
            Case "DL"
                Me.Text = "Direct Labor"
                tpProperties.Text = "Direct Labor Properties"
                Me.Icon = New Icon(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream("WindowsApplication.ExpenseDL.ico"))
            Case "RC"
                Me.Text = "Salesperson"
                tpProperties.Text = "Salesperson Properties"
                Me.Icon = New Icon(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream("WindowsApplication.ExpenseRC.ico"))
            Case "OE"
                chkEXAppearInCalendar.Enabled = False
                Me.Text = "Non-Wage Expense"
                tpProperties.Text = "Non-Wage Expense Properties"
                Me.Icon = New Icon(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream("WindowsApplication.ExpenseOE.ico"))
            Case "OW"
                chkEXAppearInCalendar.Enabled = False
                Me.Text = "Non-Direct Labor"
                tpProperties.Text = "Non-Direct Labor Properties"
                Me.Icon = New Icon(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream("WindowsApplication.ExpenseOW.ico"))
            Case "OH"
                chkEXAppearInCalendar.Enabled = False
                Me.Text = "Overhead"
                tpProperties.Text = "Overhead Properties"
        End Select
    End Sub

    Private Sub txtEXName_ParseEditValue(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) Handles txtEXName.ParseEditValue
        Format.Text_ParseEditValue(sender, e)
    End Sub

    Private Function ShowAnnualAmount(ByVal EXPaymentMethod As String) As Boolean
        Return DsWizards.EXPaymentMethod_Rules.FindByEXPaymentMethod(EXPaymentMethod).PMShowInAllowances
    End Function

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        Select Case DataRow("EGType")
            Case "DL"
                ShowHelpTopic(Me, "DirectLaborTerms.html")
            Case "RC"
                ShowHelpTopic(Me, "SalespeopleTerms.html")
            Case "OW"
                ShowHelpTopic(Me, "OverheadsTerms.html")
            Case "OE"
                ShowHelpTopic(Me, "OverheadsTerms.html")
            Case "OH"
                ShowHelpTopic(Me, "OverheadsTerms.html")
        End Select
    End Sub

End Class
