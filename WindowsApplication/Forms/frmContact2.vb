Public Class frmContact2
    Inherits DevExpress.XtraEditors.XtraForm

    Private DataRow As DataRow

    Public Shared Sub Add(ByVal BRID As Int32)
        Dim gui As New frmContact2

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            .FillPreliminaryData(BRID)

            .DataRow = .dataSet.Contacts.NewRow()
            .DataRow("BRID") = BRID
            .dataSet.Contacts.Rows.Add(.DataRow)

            gui.ShowDialog()

            .SqlConnection.Close()
        End With
    End Sub

    Public Shared Sub Edit(ByVal BRID As Int32, ByRef CTID As Int64)
        Dim gui As New frmContact2

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            If DataAccess.spExecLockRequest("sp_GetContactLock", BRID, CTID, .SqlConnection) Then

                .FillPreliminaryData(BRID)

                .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = BRID
                .SqlDataAdapter.SelectCommand.Parameters("@CTID").Value = CTID
                .SqlDataAdapter.Fill(.dataSet)
                .DataRow = .dataSet.Contacts(0)

                .FillData()

                gui.ShowDialog()

                DataAccess.spExecLockRequest("sp_ReleaseContactLock", BRID, CTID, .SqlConnection)
            Else
                Message.CurrentlyAccessed("contact")
            End If

            .SqlConnection.Close()
        End With
    End Sub

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnHelp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents txtJBClientEmail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientFirstName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientSurname As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents txtJBReferenceName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientStreetAddress01 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientStreetAddress02 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientSuburb As DevExpress.XtraEditors.TextEdit
    Friend WithEvents JBClientState As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientPostCode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientPhoneNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientFaxNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtBClientMobileNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents dataSet As WindowsApplication.dsContacts
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmContact2))
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.btnHelp = New DevExpress.XtraEditors.SimpleButton
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label45 = New System.Windows.Forms.Label
        Me.txtJBClientEmail = New DevExpress.XtraEditors.TextEdit
        Me.dataSet = New WindowsApplication.dsContacts
        Me.txtJBClientFirstName = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientSurname = New DevExpress.XtraEditors.TextEdit
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label30 = New System.Windows.Forms.Label
        Me.txtJBReferenceName = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientStreetAddress01 = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientStreetAddress02 = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientSuburb = New DevExpress.XtraEditors.TextEdit
        Me.JBClientState = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientPostCode = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientPhoneNumber = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientFaxNumber = New DevExpress.XtraEditors.TextEdit
        Me.txtBClientMobileNumber = New DevExpress.XtraEditors.TextEdit
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl
        Me.Label7 = New System.Windows.Forms.Label
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        CType(Me.txtJBClientEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientFirstName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBReferenceName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientSuburb.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JBClientState.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientPostCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientPhoneNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientFaxNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBClientMobileNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(416, 536)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(336, 536)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "OK"
        '
        'btnHelp
        '
        Me.btnHelp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnHelp.Image = CType(resources.GetObject("btnHelp.Image"), System.Drawing.Image)
        Me.btnHelp.Location = New System.Drawing.Point(8, 536)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(72, 23)
        Me.btnHelp.TabIndex = 1
        Me.btnHelp.Text = "Help"
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(264, 120)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(96, 21)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "ZIP/postal code:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label45
        '
        Me.Label45.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.Location = New System.Drawing.Point(8, 152)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(80, 21)
        Me.Label45.TabIndex = 8
        Me.Label45.Text = "Email:"
        Me.Label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJBClientEmail
        '
        Me.txtJBClientEmail.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Contacts.CTEmail"))
        Me.txtJBClientEmail.EditValue = ""
        Me.txtJBClientEmail.Location = New System.Drawing.Point(152, 152)
        Me.txtJBClientEmail.Name = "txtJBClientEmail"
        Me.txtJBClientEmail.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientEmail.TabIndex = 9
        '
        'dataSet
        '
        Me.dataSet.DataSetName = "dsContacts"
        Me.dataSet.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'txtJBClientFirstName
        '
        Me.txtJBClientFirstName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Contacts.CTFirstName"))
        Me.txtJBClientFirstName.EditValue = ""
        Me.txtJBClientFirstName.Location = New System.Drawing.Point(152, 56)
        Me.txtJBClientFirstName.Name = "txtJBClientFirstName"
        Me.txtJBClientFirstName.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientFirstName.TabIndex = 3
        '
        'txtJBClientSurname
        '
        Me.txtJBClientSurname.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Contacts.CTSurname"))
        Me.txtJBClientSurname.EditValue = ""
        Me.txtJBClientSurname.Location = New System.Drawing.Point(152, 24)
        Me.txtJBClientSurname.Name = "txtJBClientSurname"
        Me.txtJBClientSurname.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientSurname.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 56)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 21)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "First name:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(144, 21)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Surname / business name:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(8, 120)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 21)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "State/region:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(8, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 21)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Street address:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(8, 88)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 21)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Suburb/town:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(8, 24)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(80, 21)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Home Phone:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(8, 88)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(80, 21)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "Fax:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(8, 120)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(80, 21)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "Mobile:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label30
        '
        Me.Label30.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(8, 88)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(96, 21)
        Me.Label30.TabIndex = 4
        Me.Label30.Text = "Reference name:"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJBReferenceName
        '
        Me.txtJBReferenceName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Contacts.CTReferenceName"))
        Me.txtJBReferenceName.EditValue = ""
        Me.txtJBReferenceName.Location = New System.Drawing.Point(152, 88)
        Me.txtJBReferenceName.Name = "txtJBReferenceName"
        Me.txtJBReferenceName.Size = New System.Drawing.Size(296, 20)
        Me.txtJBReferenceName.TabIndex = 5
        '
        'txtJBClientStreetAddress01
        '
        Me.txtJBClientStreetAddress01.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Contacts.CTStreetAddress01"))
        Me.txtJBClientStreetAddress01.EditValue = ""
        Me.txtJBClientStreetAddress01.Location = New System.Drawing.Point(152, 24)
        Me.txtJBClientStreetAddress01.Name = "txtJBClientStreetAddress01"
        Me.txtJBClientStreetAddress01.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientStreetAddress01.TabIndex = 1
        '
        'txtJBClientStreetAddress02
        '
        Me.txtJBClientStreetAddress02.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Contacts.CTStreetAddress02"))
        Me.txtJBClientStreetAddress02.EditValue = ""
        Me.txtJBClientStreetAddress02.Location = New System.Drawing.Point(152, 56)
        Me.txtJBClientStreetAddress02.Name = "txtJBClientStreetAddress02"
        Me.txtJBClientStreetAddress02.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientStreetAddress02.TabIndex = 2
        '
        'txtJBClientSuburb
        '
        Me.txtJBClientSuburb.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Contacts.CTSuburb"))
        Me.txtJBClientSuburb.EditValue = ""
        Me.txtJBClientSuburb.Location = New System.Drawing.Point(152, 88)
        Me.txtJBClientSuburb.Name = "txtJBClientSuburb"
        Me.txtJBClientSuburb.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientSuburb.TabIndex = 4
        '
        'JBClientState
        '
        Me.JBClientState.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Contacts.CTState"))
        Me.JBClientState.EditValue = ""
        Me.JBClientState.Location = New System.Drawing.Point(152, 120)
        Me.JBClientState.Name = "JBClientState"
        Me.JBClientState.Size = New System.Drawing.Size(88, 20)
        Me.JBClientState.TabIndex = 6
        '
        'txtJBClientPostCode
        '
        Me.txtJBClientPostCode.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Contacts.CTPostCode"))
        Me.txtJBClientPostCode.EditValue = ""
        Me.txtJBClientPostCode.Location = New System.Drawing.Point(360, 120)
        Me.txtJBClientPostCode.Name = "txtJBClientPostCode"
        Me.txtJBClientPostCode.Size = New System.Drawing.Size(88, 20)
        Me.txtJBClientPostCode.TabIndex = 8
        '
        'txtJBClientPhoneNumber
        '
        Me.txtJBClientPhoneNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Contacts.CTHomePhoneNumber"))
        Me.txtJBClientPhoneNumber.EditValue = ""
        Me.txtJBClientPhoneNumber.Location = New System.Drawing.Point(152, 24)
        Me.txtJBClientPhoneNumber.Name = "txtJBClientPhoneNumber"
        Me.txtJBClientPhoneNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientPhoneNumber.TabIndex = 1
        '
        'txtJBClientFaxNumber
        '
        Me.txtJBClientFaxNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Contacts.CTFaxNumber"))
        Me.txtJBClientFaxNumber.EditValue = ""
        Me.txtJBClientFaxNumber.Location = New System.Drawing.Point(152, 88)
        Me.txtJBClientFaxNumber.Name = "txtJBClientFaxNumber"
        Me.txtJBClientFaxNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientFaxNumber.TabIndex = 5
        '
        'txtBClientMobileNumber
        '
        Me.txtBClientMobileNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Contacts.CTMobileNumber"))
        Me.txtBClientMobileNumber.EditValue = ""
        Me.txtBClientMobileNumber.Location = New System.Drawing.Point(152, 120)
        Me.txtBClientMobileNumber.Name = "txtBClientMobileNumber"
        Me.txtBClientMobileNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtBClientMobileNumber.TabIndex = 7
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.XtraTabControl1.Location = New System.Drawing.Point(8, 8)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(480, 520)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1})
        Me.XtraTabControl1.Text = "XtraTabControl1"
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.GroupControl3)
        Me.XtraTabPage1.Controls.Add(Me.GroupControl2)
        Me.XtraTabPage1.Controls.Add(Me.GroupControl1)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(471, 490)
        Me.XtraTabPage1.Text = "Contact Properties"
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.txtBClientMobileNumber)
        Me.GroupControl3.Controls.Add(Me.Label7)
        Me.GroupControl3.Controls.Add(Me.TextEdit1)
        Me.GroupControl3.Controls.Add(Me.txtJBClientPhoneNumber)
        Me.GroupControl3.Controls.Add(Me.txtJBClientFaxNumber)
        Me.GroupControl3.Controls.Add(Me.Label8)
        Me.GroupControl3.Controls.Add(Me.Label9)
        Me.GroupControl3.Controls.Add(Me.Label10)
        Me.GroupControl3.Controls.Add(Me.Label45)
        Me.GroupControl3.Controls.Add(Me.txtJBClientEmail)
        Me.GroupControl3.Location = New System.Drawing.Point(8, 296)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(456, 184)
        Me.GroupControl3.TabIndex = 2
        Me.GroupControl3.Text = "Contact Details"
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(8, 56)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(80, 21)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "Work Phone:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextEdit1
        '
        Me.TextEdit1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Contacts.CTWorkPhoneNumber"))
        Me.TextEdit1.EditValue = ""
        Me.TextEdit1.Location = New System.Drawing.Point(152, 56)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Size = New System.Drawing.Size(296, 20)
        Me.TextEdit1.TabIndex = 3
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.txtJBClientStreetAddress02)
        Me.GroupControl2.Controls.Add(Me.txtJBClientSuburb)
        Me.GroupControl2.Controls.Add(Me.JBClientState)
        Me.GroupControl2.Controls.Add(Me.txtJBClientPostCode)
        Me.GroupControl2.Controls.Add(Me.Label4)
        Me.GroupControl2.Controls.Add(Me.Label6)
        Me.GroupControl2.Controls.Add(Me.Label3)
        Me.GroupControl2.Controls.Add(Me.Label5)
        Me.GroupControl2.Controls.Add(Me.txtJBClientStreetAddress01)
        Me.GroupControl2.Location = New System.Drawing.Point(8, 136)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(456, 152)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "Address"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.Label30)
        Me.GroupControl1.Controls.Add(Me.txtJBReferenceName)
        Me.GroupControl1.Controls.Add(Me.Label2)
        Me.GroupControl1.Controls.Add(Me.txtJBClientSurname)
        Me.GroupControl1.Controls.Add(Me.txtJBClientFirstName)
        Me.GroupControl1.Controls.Add(Me.Label1)
        Me.GroupControl1.Location = New System.Drawing.Point(8, 8)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(456, 120)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Name"
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;user id=sa;integrated security=SSPI;data sou" & _
        "rce=""SERVER\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Contacts", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("CTID", "CTID"), New System.Data.Common.DataColumnMapping("CTSurname", "CTSurname"), New System.Data.Common.DataColumnMapping("CTFirstName", "CTFirstName"), New System.Data.Common.DataColumnMapping("CTName", "CTName"), New System.Data.Common.DataColumnMapping("CTStreetAddress01", "CTStreetAddress01"), New System.Data.Common.DataColumnMapping("CTStreetAddress02", "CTStreetAddress02"), New System.Data.Common.DataColumnMapping("CTSuburb", "CTSuburb"), New System.Data.Common.DataColumnMapping("CTState", "CTState"), New System.Data.Common.DataColumnMapping("CTPostCode", "CTPostCode"), New System.Data.Common.DataColumnMapping("CTHomePhoneNumber", "CTHomePhoneNumber"), New System.Data.Common.DataColumnMapping("CTWorkPhoneNumber", "CTWorkPhoneNumber"), New System.Data.Common.DataColumnMapping("CTMobileNumber", "CTMobileNumber"), New System.Data.Common.DataColumnMapping("CTFaxNumber", "CTFaxNumber"), New System.Data.Common.DataColumnMapping("CTEmail", "CTEmail"), New System.Data.Common.DataColumnMapping("CTReferenceName", "CTReferenceName")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Contacts WHERE (BRID = @Original_BRID) AND (CTID = @Original_CTID) AN" & _
        "D (CTEmail = @Original_CTEmail OR @Original_CTEmail IS NULL AND CTEmail IS NULL)" & _
        " AND (CTFaxNumber = @Original_CTFaxNumber OR @Original_CTFaxNumber IS NULL AND C" & _
        "TFaxNumber IS NULL) AND (CTFirstName = @Original_CTFirstName OR @Original_CTFirs" & _
        "tName IS NULL AND CTFirstName IS NULL) AND (CTHomePhoneNumber = @Original_CTHome" & _
        "PhoneNumber OR @Original_CTHomePhoneNumber IS NULL AND CTHomePhoneNumber IS NULL" & _
        ") AND (CTMobileNumber = @Original_CTMobileNumber OR @Original_CTMobileNumber IS " & _
        "NULL AND CTMobileNumber IS NULL) AND (CTPostCode = @Original_CTPostCode OR @Orig" & _
        "inal_CTPostCode IS NULL AND CTPostCode IS NULL) AND (CTReferenceName = @Original" & _
        "_CTReferenceName OR @Original_CTReferenceName IS NULL AND CTReferenceName IS NUL" & _
        "L) AND (CTState = @Original_CTState OR @Original_CTState IS NULL AND CTState IS " & _
        "NULL) AND (CTStreetAddress01 = @Original_CTStreetAddress01 OR @Original_CTStreet" & _
        "Address01 IS NULL AND CTStreetAddress01 IS NULL) AND (CTStreetAddress02 = @Origi" & _
        "nal_CTStreetAddress02 OR @Original_CTStreetAddress02 IS NULL AND CTStreetAddress" & _
        "02 IS NULL) AND (CTSuburb = @Original_CTSuburb OR @Original_CTSuburb IS NULL AND" & _
        " CTSuburb IS NULL) AND (CTSurname = @Original_CTSurname OR @Original_CTSurname I" & _
        "S NULL AND CTSurname IS NULL) AND (CTWorkPhoneNumber = @Original_CTWorkPhoneNumb" & _
        "er OR @Original_CTWorkPhoneNumber IS NULL AND CTWorkPhoneNumber IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTFirstName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTFirstName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTHomePhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTReferenceName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTReferenceName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTSurname", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTWorkPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Contacts(BRID, CTSurname, CTFirstName, CTName, CTStreetAddress01, CTStreetAdd" &
        "ress02, CTSuburb, CTState, CTPostCode, CTHomePhoneNumber, CTWorkPhoneNumber, CTM" &
        "obileNumber, CTFaxNumber, CTEmail, CTAddress, CTAddressMultiline, CTReferenceName) VALUES (@BRID, @CTSurname, @" &
        "CTFirstName, @CTName, @CTStreetAddress01, @CTStreetAddress02, @CTSuburb, @CTState, @CTPos" &
        "tCode, @CTHomePhoneNumber, @CTWorkPhoneNumber, @CTMobileNumber, @CTFaxNumber, @C" &
        "TEmail,  @CTAddress, @CTAddressMultiline, @CTReferenceName); SELECT BRID, CTID, CTSurname, CTName, CTFirstName, CTStreetAd" &
        "dress01, CTStreetAddress02, CTSuburb, CTState, CTPostCode, CTHomePhoneNumber, CT" &
        "WorkPhoneNumber, CTMobileNumber, CTFaxNumber, CTEmail, CTReferenceName FROM Cont" &
        "acts WHERE (BRID = @BRID) AND (CTID = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTSurname", System.Data.SqlDbType.VarChar, 50, "CTSurname"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTFirstName", System.Data.SqlDbType.VarChar, 50, "CTFirstName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTName", System.Data.SqlDbType.VarChar, 102, "CTName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTStreetAddress01", System.Data.SqlDbType.VarChar, 100, "CTStreetAddress01"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTStreetAddress02", System.Data.SqlDbType.VarChar, 100, "CTStreetAddress02"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTSuburb", System.Data.SqlDbType.VarChar, 50, "CTSuburb"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTState", System.Data.SqlDbType.VarChar, 3, "CTState"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTPostCode", System.Data.SqlDbType.VarChar, 20, "CTPostCode"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, "CTHomePhoneNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, "CTWorkPhoneNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTMobileNumber", System.Data.SqlDbType.VarChar, 20, "CTMobileNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTFaxNumber", System.Data.SqlDbType.VarChar, 20, "CTFaxNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTEmail", System.Data.SqlDbType.VarChar, 50, "CTEmail"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTAddress", System.Data.SqlDbType.VarChar, 259, "CTAddress"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTAddressMultiline", System.Data.SqlDbType.VarChar, 261, "CTAddressMultiline"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTReferenceName", System.Data.SqlDbType.VarChar, 100, "CTReferenceName"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, CTID, CTSurname, CTFirstName,CTName, CTStreetAddress01, CTStreetAddress02, " &
        "CTSuburb, CTState, CTPostCode, CTHomePhoneNumber, CTWorkPhoneNumber, CTMobileNum" &
        "ber, CTFaxNumber, CTEmail,CTAddress, CTAddressMultiline, CTReferenceName FROM Contacts WHERE (BRID = @BRID) AN" &
        "D (CTID = @CTID)"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTID", System.Data.SqlDbType.BigInt, 8, "CTID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Contacts SET BRID = @BRID, CTSurname = @CTSurname, CTFirstName = @CTFirstN" &
        "ame, CTName = @CTName, CTStreetAddress01 = @CTStreetAddress01, CTStreetAddress02 = @CTStreetAddres" &
        "s02, CTSuburb = @CTSuburb, CTState = @CTState, CTPostCode = @CTPostCode, CTHomeP" &
        "honeNumber = @CTHomePhoneNumber, CTWorkPhoneNumber = @CTWorkPhoneNumber, CTMobil" &
        "eNumber = @CTMobileNumber, CTFaxNumber = @CTFaxNumber, CTEmail = @CTEmail, CTAddress = @CTAddress, CTAddressMultiline = @CTAddressMultiline, CTRef" &
        "erenceName = @CTReferenceName WHERE (BRID = @Original_BRID) AND (CTID = @Origina" &
        "l_CTID) AND (CTEmail = @Original_CTEmail OR @Original_CTEmail IS NULL AND CTEmai" &
        "l IS NULL) AND (CTFaxNumber = @Original_CTFaxNumber OR @Original_CTFaxNumber IS " &
        "NULL AND CTFaxNumber IS NULL) AND (CTFirstName = @Original_CTFirstName OR @Origi" &
        "nal_CTFirstName IS NULL AND CTFirstName IS NULL) AND (CTHomePhoneNumber = @Origi" &
        "nal_CTHomePhoneNumber OR @Original_CTHomePhoneNumber IS NULL AND CTHomePhoneNumb" &
        "er IS NULL) AND (CTMobileNumber = @Original_CTMobileNumber OR @Original_CTMobile" &
        "Number IS NULL AND CTMobileNumber IS NULL) AND (CTPostCode = @Original_CTPostCod" &
        "e OR @Original_CTPostCode IS NULL AND CTPostCode IS NULL) AND (CTReferenceName =" &
        " @Original_CTReferenceName OR @Original_CTReferenceName IS NULL AND CTReferenceN" &
        "ame IS NULL) AND (CTState = @Original_CTState OR @Original_CTState IS NULL AND C" &
        "TState IS NULL) AND (CTStreetAddress01 = @Original_CTStreetAddress01 OR @Origina" &
        "l_CTStreetAddress01 IS NULL AND CTStreetAddress01 IS NULL) AND (CTStreetAddress0" &
        "2 = @Original_CTStreetAddress02 OR @Original_CTStreetAddress02 IS NULL AND CTStr" &
        "eetAddress02 IS NULL) AND (CTSuburb = @Original_CTSuburb OR @Original_CTSuburb I" &
        "S NULL AND CTSuburb IS NULL) AND (CTSurname = @Original_CTSurname OR @Original_C" &
        "TSurname IS NULL AND CTSurname IS NULL) AND (CTWorkPhoneNumber = @Original_CTWor" &
        "kPhoneNumber OR @Original_CTWorkPhoneNumber IS NULL AND CTWorkPhoneNumber IS NUL" &
        "L); SELECT BRID, CTID, CTSurname, CTFirstName, CTStreetAddress01, CTStreetAddres" &
        "s02, CTSuburb, CTState, CTPostCode, CTHomePhoneNumber, CTWorkPhoneNumber, CTMobi" &
        "leNumber, CTFaxNumber, CTEmail, CTReferenceName FROM Contacts WHERE (BRID = @BRI" &
        "D) AND (CTID = @CTID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTSurname", System.Data.SqlDbType.VarChar, 50, "CTSurname"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTFirstName", System.Data.SqlDbType.VarChar, 50, "CTFirstName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTName", System.Data.SqlDbType.VarChar, 102, "CTName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTStreetAddress01", System.Data.SqlDbType.VarChar, 100, "CTStreetAddress01"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTStreetAddress02", System.Data.SqlDbType.VarChar, 100, "CTStreetAddress02"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTSuburb", System.Data.SqlDbType.VarChar, 50, "CTSuburb"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTState", System.Data.SqlDbType.VarChar, 3, "CTState"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTPostCode", System.Data.SqlDbType.VarChar, 20, "CTPostCode"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, "CTHomePhoneNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, "CTWorkPhoneNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTMobileNumber", System.Data.SqlDbType.VarChar, 20, "CTMobileNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTFaxNumber", System.Data.SqlDbType.VarChar, 20, "CTFaxNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTEmail", System.Data.SqlDbType.VarChar, 50, "CTEmail"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTAddress", System.Data.SqlDbType.VarChar, 259, "CTAddress"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTAddressMultiline", System.Data.SqlDbType.VarChar, 261, "CTAddressMultiline"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTReferenceName", System.Data.SqlDbType.VarChar, 100, "CTReferenceName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTFirstName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTFirstName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTHomePhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTReferenceName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTReferenceName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTSurname", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTWorkPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTID", System.Data.SqlDbType.BigInt, 8, "CTID"))
        '
        'frmContact2
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(498, 568)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Controls.Add(Me.btnHelp)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmContact2"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Contact"
        CType(Me.txtJBClientEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientFirstName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBReferenceName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientSuburb.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JBClientState.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientPostCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientPhoneNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientFaxNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBClientMobileNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnOK.Enabled = Not HasRole("branch_read_only")
    End Sub

    Private Sub FillPreliminaryData(ByVal BRID As Integer)
    End Sub

    Private Sub FillData()
    End Sub

    Private Sub UpdateData()
        SqlDataAdapter.Update(dataSet)
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        ' EndEdit() to end editing the dataset record so that we can update
        DataRow.EndEdit()

        If ValidateForm() Then
            UpdateData()
            OK = True
            Me.Close()
        End If
    End Sub
    Private Function ValidateForm() As Boolean
        If DataRow("CTSurname") Is DBNull.Value Then
            Message.ShowMessage("You must enter a Surname / business name.", MessageBoxIcon.Exclamation)
            Return False
        End If
        Return True
    End Function
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            DataRow.EndEdit()
            If dataSet.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If response = MsgBoxResult.No Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub Text_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) _
    Handles txtJBClientSurname.ParseEditValue
        Format.Text_ParseEditValue(sender, e)
    End Sub

    Private Sub btnHelp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHelp.Click
        ShowHelpTopic(Me, "UsingTheContactsSection.htm")
    End Sub
End Class

