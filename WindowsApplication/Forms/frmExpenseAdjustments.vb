Public Class frmExpenseAdjustments
    Inherits DevExpress.XtraEditors.XtraForm

    Public BRID As Int32
    Public EXID As Int32
    Public EAType As String
    Private HelpTopic As String = Nothing

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return SqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            SqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private hTransaction As SqlClient.SqlTransaction
    Private Property Transaction() As SqlClient.SqlTransaction
        Get
            Return hTransaction
        End Get
        Set(ByVal Value As SqlClient.SqlTransaction)
            hTransaction = Value
            Power.Library.Library.ApplyTransactionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Public Shared Function Edit(ByVal BRID As Int32, ByRef EXID As Int64, ByVal EAType As String, _
            ByVal EXName As String, ByVal PaymentTypeDisplay As String, ByVal HelpTopic As String) As frmExpenseAdjustments
        Dim gui As New frmExpenseAdjustments

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            .BRID = BRID
            .EXID = EXID
            .EAType = EAType

            .Transaction = .SqlConnection.BeginTransaction(IsolationLevel.ReadUncommitted)
            .FillPreliminaryData()

            If DataAccess.spExecLockRequest("sp_GetExpenseLock", .BRID, .EXID, .Transaction) Then

                .FillData()

                .Text = PaymentTypeDisplay & " for " & EXName
                .tpExpenseProperties.Text = PaymentTypeDisplay
                .HelpTopic = HelpTopic
                Select Case EAType
                    Case "PR"
                        .Icon = New Icon(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream("WindowsApplication.ExpenseAdjustmentsPR.ico"))
                    Case "BO"
                        .Icon = New Icon(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream("WindowsApplication.ExpenseAdjustmentsBO.ico"))
                End Select

            Else
                Throw New ObjectLockedException
            End If
        End With

        Return gui
    End Function

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents DsGTMS As WindowsApplication.dsGTMS
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents dvAdjustments As System.Data.DataView
    Friend WithEvents daAdjustments As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colEADate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEAAmount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEADesc As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dgAdjustments As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvExpenseAdjustment As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tpExpenseProperties As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents btnRemoveAdjustment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtCurrencyGrid As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents txtDate As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents btnHelp As DevExpress.XtraEditors.SimpleButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim configurationAppSettings As System.Configuration.AppSettingsReader = New System.Configuration.AppSettingsReader
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmExpenseAdjustments))
        Me.dgAdjustments = New DevExpress.XtraGrid.GridControl
        Me.DsGTMS = New WindowsApplication.dsGTMS
        Me.gvExpenseAdjustment = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colEADate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtDate = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.colEAAmount = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtCurrencyGrid = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colEADesc = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.dvAdjustments = New System.Data.DataView
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.daAdjustments = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.tpExpenseProperties = New DevExpress.XtraTab.XtraTabControl
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage
        Me.btnRemoveAdjustment = New DevExpress.XtraEditors.SimpleButton
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.btnHelp = New DevExpress.XtraEditors.SimpleButton
        CType(Me.dgAdjustments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvExpenseAdjustment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCurrencyGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvAdjustments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tpExpenseProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpExpenseProperties.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgAdjustments
        '
        Me.dgAdjustments.DataSource = Me.DsGTMS.ExpenseAdjustments
        '
        'dgAdjustments.EmbeddedNavigator
        '
        Me.dgAdjustments.EmbeddedNavigator.Name = ""
        Me.dgAdjustments.Location = New System.Drawing.Point(8, 16)
        Me.dgAdjustments.MainView = Me.gvExpenseAdjustment
        Me.dgAdjustments.Name = "dgAdjustments"
        Me.dgAdjustments.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtCurrencyGrid, Me.txtDate})
        Me.dgAdjustments.Size = New System.Drawing.Size(472, 320)
        Me.dgAdjustments.TabIndex = 0
        Me.dgAdjustments.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvExpenseAdjustment, Me.GridView1})
        '
        'DsGTMS
        '
        Me.DsGTMS.DataSetName = "dsGTMS"
        Me.DsGTMS.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'gvExpenseAdjustment
        '
        Me.gvExpenseAdjustment.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvExpenseAdjustment.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvExpenseAdjustment.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvExpenseAdjustment.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvExpenseAdjustment.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvExpenseAdjustment.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvExpenseAdjustment.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvExpenseAdjustment.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvExpenseAdjustment.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvExpenseAdjustment.Appearance.VertLine.Options.UseBackColor = True
        Me.gvExpenseAdjustment.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colEADate, Me.colEAAmount, Me.colEADesc})
        Me.gvExpenseAdjustment.GridControl = Me.dgAdjustments
        Me.gvExpenseAdjustment.Name = "gvExpenseAdjustment"
        Me.gvExpenseAdjustment.OptionsCustomization.AllowFilter = False
        Me.gvExpenseAdjustment.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvExpenseAdjustment.OptionsView.ShowGroupPanel = False
        Me.gvExpenseAdjustment.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colEADate, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colEADate
        '
        Me.colEADate.Caption = "Date"
        Me.colEADate.ColumnEdit = Me.txtDate
        Me.colEADate.FieldName = "EADate"
        Me.colEADate.Name = "colEADate"
        Me.colEADate.Visible = True
        Me.colEADate.VisibleIndex = 0
        Me.colEADate.Width = 107
        '
        'txtDate
        '
        Me.txtDate.AutoHeight = False
        Me.txtDate.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtDate.Name = "txtDate"
        '
        'colEAAmount
        '
        Me.colEAAmount.Caption = "Amount"
        Me.colEAAmount.ColumnEdit = Me.txtCurrencyGrid
        Me.colEAAmount.DisplayFormat.FormatString = "c"
        Me.colEAAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colEAAmount.FieldName = "EAAmount"
        Me.colEAAmount.Name = "colEAAmount"
        Me.colEAAmount.Visible = True
        Me.colEAAmount.VisibleIndex = 1
        Me.colEAAmount.Width = 95
        '
        'txtCurrencyGrid
        '
        Me.txtCurrencyGrid.AutoHeight = False
        Me.txtCurrencyGrid.DisplayFormat.FormatString = "c"
        Me.txtCurrencyGrid.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCurrencyGrid.EditFormat.FormatString = "c"
        Me.txtCurrencyGrid.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCurrencyGrid.Name = "txtCurrencyGrid"
        '
        'colEADesc
        '
        Me.colEADesc.Caption = "Description"
        Me.colEADesc.FieldName = "EADesc"
        Me.colEADesc.Name = "colEADesc"
        Me.colEADesc.Visible = True
        Me.colEADesc.VisibleIndex = 2
        Me.colEADesc.Width = 240
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.dgAdjustments
        Me.GridView1.Name = "GridView1"
        '
        'dvAdjustments
        '
        Me.dvAdjustments.Table = Me.DsGTMS.ExpenseAdjustments
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = CType(configurationAppSettings.GetValue("SqlConnection.ConnectionString", GetType(System.String)), String)
        '
        'daAdjustments
        '
        Me.daAdjustments.DeleteCommand = Me.SqlDeleteCommand1
        Me.daAdjustments.InsertCommand = Me.SqlInsertCommand1
        Me.daAdjustments.SelectCommand = Me.SqlSelectCommand1
        Me.daAdjustments.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "ExpenseAdjustments", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("EAID", "EAID"), New System.Data.Common.DataColumnMapping("EADate", "EADate"), New System.Data.Common.DataColumnMapping("EAAmount", "EAAmount"), New System.Data.Common.DataColumnMapping("EADesc", "EADesc"), New System.Data.Common.DataColumnMapping("EAType", "EAType")})})
        Me.daAdjustments.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM ExpenseAdjustments WHERE (BRID = @Original_BRID) AND (EAID = @Origina" & _
        "l_EAID) AND (EAAmount = @Original_EAAmount OR @Original_EAAmount IS NULL AND EAA" & _
        "mount IS NULL) AND (EADate = @Original_EADate OR @Original_EADate IS NULL AND EA" & _
        "Date IS NULL) AND (EADesc = @Original_EADesc OR @Original_EADesc IS NULL AND EAD" & _
        "esc IS NULL) AND (EAType = @Original_EAType OR @Original_EAType IS NULL AND EATy" & _
        "pe IS NULL) AND (EXID = @Original_EXID)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EAID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EAID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EAAmount", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EAAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EADate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EADate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EADesc", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EADesc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EAType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EAType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO ExpenseAdjustments(BRID, EXID, EADate, EAAmount, EADesc, EAType) VALU" & _
        "ES (@BRID, @EXID, @EADate, @EAAmount, @EADesc, @EAType); SELECT BRID, EXID, EAID" & _
        ", EADate, EAAmount, EADesc, EAType FROM ExpenseAdjustments WHERE (BRID = @BRID) " & _
        "AND (EAID = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EADate", System.Data.SqlDbType.DateTime, 8, "EADate"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EAAmount", System.Data.SqlDbType.Money, 8, "EAAmount"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EADesc", System.Data.SqlDbType.VarChar, 100, "EADesc"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EAType", System.Data.SqlDbType.VarChar, 2, "EAType"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, EXID, EAID, EADate, EAAmount, EADesc, EAType FROM ExpenseAdjustments" & _
        " WHERE (BRID = @BRID) AND (EXID = @EXID) AND (EAType = @EAType)"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EAType", System.Data.SqlDbType.VarChar, 2, "EAType"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE ExpenseAdjustments SET BRID = @BRID, EXID = @EXID, EADate = @EADate, EAAmo" & _
        "unt = @EAAmount, EADesc = @EADesc, EAType = @EAType WHERE (BRID = @Original_BRID" & _
        ") AND (EAID = @Original_EAID) AND (EAAmount = @Original_EAAmount OR @Original_EA" & _
        "Amount IS NULL AND EAAmount IS NULL) AND (EADate = @Original_EADate OR @Original" & _
        "_EADate IS NULL AND EADate IS NULL) AND (EADesc = @Original_EADesc OR @Original_" & _
        "EADesc IS NULL AND EADesc IS NULL) AND (EAType = @Original_EAType OR @Original_E" & _
        "AType IS NULL AND EAType IS NULL) AND (EXID = @Original_EXID); SELECT BRID, EXID" & _
        ", EAID, EADate, EAAmount, EADesc, EAType FROM ExpenseAdjustments WHERE (BRID = @" & _
        "BRID) AND (EAID = @EAID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EADate", System.Data.SqlDbType.DateTime, 8, "EADate"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EAAmount", System.Data.SqlDbType.Money, 8, "EAAmount"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EADesc", System.Data.SqlDbType.VarChar, 100, "EADesc"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EAType", System.Data.SqlDbType.VarChar, 2, "EAType"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EAID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EAID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EAAmount", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EAAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EADate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EADate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EADesc", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EADesc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EAType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EAType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EAID", System.Data.SqlDbType.BigInt, 8, "EAID"))
        '
        'tpExpenseProperties
        '
        Me.tpExpenseProperties.Location = New System.Drawing.Point(8, 8)
        Me.tpExpenseProperties.Name = "tpExpenseProperties"
        Me.tpExpenseProperties.SelectedTabPage = Me.XtraTabPage1
        Me.tpExpenseProperties.Size = New System.Drawing.Size(496, 400)
        Me.tpExpenseProperties.TabIndex = 0
        Me.tpExpenseProperties.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1})
        Me.tpExpenseProperties.Text = "XtraTabControl1"
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.btnRemoveAdjustment)
        Me.XtraTabPage1.Controls.Add(Me.dgAdjustments)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(490, 374)
        Me.XtraTabPage1.Text = "Expense Properties"
        '
        'btnRemoveAdjustment
        '
        Me.btnRemoveAdjustment.Image = CType(resources.GetObject("btnRemoveAdjustment.Image"), System.Drawing.Image)
        Me.btnRemoveAdjustment.Location = New System.Drawing.Point(8, 344)
        Me.btnRemoveAdjustment.Name = "btnRemoveAdjustment"
        Me.btnRemoveAdjustment.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveAdjustment.TabIndex = 1
        Me.btnRemoveAdjustment.Text = "Remove"
        '
        'btnOK
        '
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(352, 416)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(432, 416)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'btnHelp
        '
        Me.btnHelp.Image = CType(resources.GetObject("btnHelp.Image"), System.Drawing.Image)
        Me.btnHelp.Location = New System.Drawing.Point(8, 416)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(72, 23)
        Me.btnHelp.TabIndex = 1
        Me.btnHelp.Text = "Help"
        '
        'frmExpenseAdjustments
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(514, 448)
        Me.Controls.Add(Me.btnHelp)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.tpExpenseProperties)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExpenseAdjustments"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Expense"
        CType(Me.dgAdjustments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvExpenseAdjustment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCurrencyGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvAdjustments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tpExpenseProperties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpExpenseProperties.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnOK.Enabled = Not HasRole("branch_read_only")
    End Sub

    Public ReadOnly Property SelectedExpenseAdjustment() As DataRow
        Get
            If Not gvExpenseAdjustment.GetSelectedRows Is Nothing Then
                Return gvExpenseAdjustment.GetDataRow(gvExpenseAdjustment.GetSelectedRows(0))
            End If
        End Get
    End Property
    ' This data must be loaded BEFORE the DataRow is loaded (eg lookups etc)
    Private Sub FillPreliminaryData()

    End Sub

    ' This data must be loaded AFTER the DataRow is loaded (eg record related data)
    Private Sub FillData()
        ' Allowances
        daAdjustments.SelectCommand.Parameters("@BRID").Value = BRID
        daAdjustments.SelectCommand.Parameters("@EXID").Value = EXID
        daAdjustments.SelectCommand.Parameters("@EAType").Value = EAType
        DsGTMS.ExpenseAdjustments.BRIDColumn.DefaultValue = BRID
        DsGTMS.ExpenseAdjustments.EXIDColumn.DefaultValue = EXID
        DsGTMS.ExpenseAdjustments.EATypeColumn.DefaultValue = EAType
        daAdjustments.Fill(DsGTMS)
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        daAdjustments.Update(DsGTMS)

        DataAccess.spExecLockRequest("sp_ReleaseExpenseLock", BRID, EXID, Transaction)
        Transaction.Commit()
        SqlConnection.Close()

        OK = True
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private HasChanges As Boolean = False
    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            If HasChanges Or DsGTMS.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If response = MsgBoxResult.Yes Then
                DataAccess.spExecLockRequest("sp_ReleaseExpenseLock", BRID, EXID, Transaction)
                Transaction.Rollback()
                SqlConnection.Close()
            Else
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub btnRemoveAdjustment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveAdjustment.Click
        If Not SelectedExpenseAdjustment Is Nothing Then
            SelectedExpenseAdjustment.Delete()
            daAdjustments.Update(DsGTMS)
        End If
    End Sub

    Private Sub gvClientPayments_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gvExpenseAdjustment.InvalidRowException
        GridView_InvalidRowException(sender, e)
    End Sub

    Private Sub txtCurrency_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) Handles txtCurrencyGrid.ParseEditValue
        Decimal_ParseEditValue(sender, e)
    End Sub

    Private Sub Date_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtDate.Validating
        Library.DateEdit_Validating(sender, e)
    End Sub

    Private Sub btnHelp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHelp.Click
        If HelpTopic Is Nothing Then
            ShowHelp(Me)
        Else
            ShowHelpTopic(Me, HelpTopic)
        End If
    End Sub

End Class