Public Class frmStockAdjustmentWizard
    Inherits DevExpress.XtraEditors.XtraForm

    Private DataRow As DataRow
    Public BRID As Int32
    Public SAID As Int64

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return SqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            SqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private hTransaction As SqlClient.SqlTransaction
    Private Property Transaction() As SqlClient.SqlTransaction
        Get
            Return hTransaction
        End Get
        Set(ByVal Value As SqlClient.SqlTransaction)
            hTransaction = Value
            Power.Library.Library.ApplyTransactionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Public Shared Function Add(ByVal BRID As Int32) As frmStockAdjustmentWizard
        Dim gui As New frmStockAdjustmentWizard

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            .BRID = BRID
            .LoadTreeViews()

            .Transaction = .SqlConnection.BeginTransaction(IsolationLevel.ReadUncommitted)
            .FillPreliminaryData()
            .SAID = spNew_StockAdjustment(.BRID, .Transaction)

            If DataAccess.spExecLockRequest("sp_GetStockAdjustmentLock", .BRID, .SAID, .Transaction) Then

                .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = .BRID
                .SqlDataAdapter.SelectCommand.Parameters("@SAID").Value = .SAID
                .SqlDataAdapter.Fill(.DsGTMS)
                .DataRow = .DsGTMS.StockAdjustments(0)

                .FillData()

            Else
                Throw New ObjectLockedException
            End If
        End With

        Return gui
    End Function

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents HorizonalRuleLine3D8 As Power.Forms.HorizonalRuleLine3D
    Friend WithEvents btnBack As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnNext As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage4 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage5 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents lblIntroTitle As System.Windows.Forms.Label
    Friend WithEvents lblIntroDesc As System.Windows.Forms.Label
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents HorizonalRuleLine3D1 As Power.Forms.HorizonalRuleLine3D
    Friend WithEvents HorizonalRuleLine3D2 As Power.Forms.HorizonalRuleLine3D
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents HorizonalRuleLine3D3 As Power.Forms.HorizonalRuleLine3D
    Friend WithEvents PanelControl5 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents PanelControl11 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents lblFinishDesc2 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents lblFinishDesc1 As System.Windows.Forms.Label
    Friend WithEvents PanelControl12 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnRemoveMaterial As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAddMaterial As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents XtraTabControl2 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents tpDateStockIncoming As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tpDateStockOutgoing As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tpDateStocktake As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents DsGTMS As WindowsApplication.dsGTMS
    Friend WithEvents daSATypes As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daStockedItems_StockAdjustments As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents txtSAType As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents txtSADate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents tvStockedItems As Power.Forms.TreeView
    Friend WithEvents dgMaterials_StockAdjustments As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvMaterials_StockAdjustments As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colSSInventoryAmount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtSSInventoryAmount As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents colSIInventoryUOMShortName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSIName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnHelp As DevExpress.XtraEditors.SimpleButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim configurationAppSettings As System.Configuration.AppSettingsReader = New System.Configuration.AppSettingsReader
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmStockAdjustmentWizard))
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl
        Me.lblIntroTitle = New System.Windows.Forms.Label
        Me.lblIntroDesc = New System.Windows.Forms.Label
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage
        Me.txtSAType = New DevExpress.XtraEditors.LookUpEdit
        Me.DsGTMS = New WindowsApplication.dsGTMS
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.HorizonalRuleLine3D1 = New Power.Forms.HorizonalRuleLine3D
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage
        Me.txtSADate = New DevExpress.XtraEditors.DateEdit
        Me.XtraTabControl2 = New DevExpress.XtraTab.XtraTabControl
        Me.tpDateStockIncoming = New DevExpress.XtraTab.XtraTabPage
        Me.Label9 = New System.Windows.Forms.Label
        Me.tpDateStockOutgoing = New DevExpress.XtraTab.XtraTabPage
        Me.Label11 = New System.Windows.Forms.Label
        Me.tpDateStocktake = New DevExpress.XtraTab.XtraTabPage
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.HorizonalRuleLine3D2 = New Power.Forms.HorizonalRuleLine3D
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.XtraTabPage4 = New DevExpress.XtraTab.XtraTabPage
        Me.dgMaterials_StockAdjustments = New DevExpress.XtraGrid.GridControl
        Me.gvMaterials_StockAdjustments = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colSSInventoryAmount = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtSSInventoryAmount = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colSIInventoryUOMShortName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSIName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tvStockedItems = New Power.Forms.TreeView
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.btnRemoveMaterial = New DevExpress.XtraEditors.SimpleButton
        Me.btnAddMaterial = New DevExpress.XtraEditors.SimpleButton
        Me.HorizonalRuleLine3D3 = New Power.Forms.HorizonalRuleLine3D
        Me.PanelControl5 = New DevExpress.XtraEditors.PanelControl
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.XtraTabPage5 = New DevExpress.XtraTab.XtraTabPage
        Me.PanelControl11 = New DevExpress.XtraEditors.PanelControl
        Me.lblFinishDesc2 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.lblFinishDesc1 = New System.Windows.Forms.Label
        Me.PanelControl12 = New DevExpress.XtraEditors.PanelControl
        Me.HorizonalRuleLine3D8 = New Power.Forms.HorizonalRuleLine3D
        Me.btnBack = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.btnNext = New DevExpress.XtraEditors.SimpleButton
        Me.daSATypes = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.daStockedItems_StockAdjustments = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand5 = New System.Data.SqlClient.SqlCommand
        Me.btnHelp = New DevExpress.XtraEditors.SimpleButton
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.txtSAType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        Me.XtraTabPage3.SuspendLayout()
        CType(Me.txtSADate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl2.SuspendLayout()
        Me.tpDateStockIncoming.SuspendLayout()
        Me.tpDateStockOutgoing.SuspendLayout()
        Me.tpDateStocktake.SuspendLayout()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        Me.XtraTabPage4.SuspendLayout()
        CType(Me.dgMaterials_StockAdjustments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvMaterials_StockAdjustments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSSInventoryAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl5.SuspendLayout()
        Me.XtraTabPage5.SuspendLayout()
        CType(Me.PanelControl11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl11.SuspendLayout()
        CType(Me.PanelControl12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.PaintStyleName = "Flat"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(714, 416)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.XtraTabPage3, Me.XtraTabPage4, Me.XtraTabPage5})
        Me.XtraTabControl1.TabStop = False
        Me.XtraTabControl1.Text = "XtraTabControl1"
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.PanelControl2)
        Me.XtraTabPage1.Controls.Add(Me.PanelControl1)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(714, 394)
        Me.XtraTabPage1.Text = "Intro"
        '
        'PanelControl2
        '
        Me.PanelControl2.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.PanelControl2.Appearance.Options.UseBackColor = True
        Me.PanelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl2.Controls.Add(Me.lblIntroTitle)
        Me.PanelControl2.Controls.Add(Me.lblIntroDesc)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl2.Location = New System.Drawing.Point(120, 0)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(594, 394)
        Me.PanelControl2.TabIndex = 7
        Me.PanelControl2.Text = "PanelControl2"
        '
        'lblIntroTitle
        '
        Me.lblIntroTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblIntroTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblIntroTitle.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIntroTitle.Location = New System.Drawing.Point(16, 24)
        Me.lblIntroTitle.Name = "lblIntroTitle"
        Me.lblIntroTitle.Size = New System.Drawing.Size(562, 48)
        Me.lblIntroTitle.TabIndex = 3
        Me.lblIntroTitle.Text = "Welcome to the Add New Stock Adjustment Wizard"
        '
        'lblIntroDesc
        '
        Me.lblIntroDesc.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblIntroDesc.BackColor = System.Drawing.Color.Transparent
        Me.lblIntroDesc.Location = New System.Drawing.Point(16, 80)
        Me.lblIntroDesc.Name = "lblIntroDesc"
        Me.lblIntroDesc.Size = New System.Drawing.Size(562, 56)
        Me.lblIntroDesc.TabIndex = 0
        Me.lblIntroDesc.Text = "This wizard guides you through adding a new stock adjustment."
        '
        'PanelControl1
        '
        Me.PanelControl1.Appearance.BackColor = System.Drawing.Color.MidnightBlue
        Me.PanelControl1.Appearance.Options.UseBackColor = True
        Me.PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(120, 394)
        Me.PanelControl1.TabIndex = 6
        Me.PanelControl1.Text = "PanelControl1"
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.txtSAType)
        Me.XtraTabPage2.Controls.Add(Me.Label8)
        Me.XtraTabPage2.Controls.Add(Me.Label7)
        Me.XtraTabPage2.Controls.Add(Me.HorizonalRuleLine3D1)
        Me.XtraTabPage2.Controls.Add(Me.PanelControl3)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(714, 394)
        Me.XtraTabPage2.Text = "Adjustment Type"
        '
        'txtSAType
        '
        Me.txtSAType.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "StockAdjustments.SAType"))
        Me.txtSAType.Location = New System.Drawing.Point(368, 240)
        Me.txtSAType.Name = "txtSAType"
        '
        'txtSAType.Properties
        '
        Me.txtSAType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtSAType.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("SATypeDisplay")})
        Me.txtSAType.Properties.DataSource = Me.DsGTMS.SATypes
        Me.txtSAType.Properties.DisplayMember = "SATypeDisplay"
        Me.txtSAType.Properties.NullText = ""
        Me.txtSAType.Properties.ShowFooter = False
        Me.txtSAType.Properties.ShowHeader = False
        Me.txtSAType.Properties.ValueMember = "SAType"
        Me.txtSAType.Size = New System.Drawing.Size(184, 20)
        Me.txtSAType.TabIndex = 17
        '
        'DsGTMS
        '
        Me.DsGTMS.DataSetName = "dsGTMS"
        Me.DsGTMS.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(176, 176)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(360, 32)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Select from adjustment type from the list."
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(176, 240)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(128, 21)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Stock Adjustment Type:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'HorizonalRuleLine3D1
        '
        Me.HorizonalRuleLine3D1.BackColor = System.Drawing.SystemColors.Control
        Me.HorizonalRuleLine3D1.Dock = System.Windows.Forms.DockStyle.Top
        Me.HorizonalRuleLine3D1.Location = New System.Drawing.Point(0, 56)
        Me.HorizonalRuleLine3D1.Name = "HorizonalRuleLine3D1"
        Me.HorizonalRuleLine3D1.Size = New System.Drawing.Size(714, 2)
        Me.HorizonalRuleLine3D1.TabIndex = 13
        '
        'PanelControl3
        '
        Me.PanelControl3.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.PanelControl3.Appearance.Options.UseBackColor = True
        Me.PanelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl3.Controls.Add(Me.Label3)
        Me.PanelControl3.Controls.Add(Me.Label2)
        Me.PanelControl3.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl3.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(714, 56)
        Me.PanelControl3.TabIndex = 12
        Me.PanelControl3.Text = "PanelControl3"
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Location = New System.Drawing.Point(24, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(674, 16)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Select the adjustment type from the list."
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(112, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Adjustment Type"
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.txtSADate)
        Me.XtraTabPage3.Controls.Add(Me.XtraTabControl2)
        Me.XtraTabPage3.Controls.Add(Me.Label10)
        Me.XtraTabPage3.Controls.Add(Me.HorizonalRuleLine3D2)
        Me.XtraTabPage3.Controls.Add(Me.PanelControl4)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(714, 394)
        Me.XtraTabPage3.Text = "Date"
        '
        'txtSADate
        '
        Me.txtSADate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "StockAdjustments.SADate"))
        Me.txtSADate.EditValue = New Date(2005, 8, 2, 0, 0, 0, 0)
        Me.txtSADate.Location = New System.Drawing.Point(408, 240)
        Me.txtSADate.Name = "txtSADate"
        '
        'txtSADate.Properties
        '
        Me.txtSADate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtSADate.Size = New System.Drawing.Size(144, 20)
        Me.txtSADate.TabIndex = 21
        '
        'XtraTabControl2
        '
        Me.XtraTabControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.XtraTabControl2.Location = New System.Drawing.Point(136, 88)
        Me.XtraTabControl2.Name = "XtraTabControl2"
        Me.XtraTabControl2.PaintStyleName = "Flat"
        Me.XtraTabControl2.SelectedTabPage = Me.tpDateStockIncoming
        Me.XtraTabControl2.Size = New System.Drawing.Size(464, 144)
        Me.XtraTabControl2.TabIndex = 20
        Me.XtraTabControl2.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tpDateStockIncoming, Me.tpDateStockOutgoing, Me.tpDateStocktake})
        Me.XtraTabControl2.TabStop = False
        Me.XtraTabControl2.Text = "XtraTabControl2"
        '
        'tpDateStockIncoming
        '
        Me.tpDateStockIncoming.Controls.Add(Me.Label9)
        Me.tpDateStockIncoming.Name = "tpDateStockIncoming"
        Me.tpDateStockIncoming.Size = New System.Drawing.Size(464, 122)
        Me.tpDateStockIncoming.Text = "Stock Incoming Desc"
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label9.Location = New System.Drawing.Point(40, 64)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(240, 32)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "Select the date the incoming stock arrived."
        '
        'tpDateStockOutgoing
        '
        Me.tpDateStockOutgoing.Controls.Add(Me.Label11)
        Me.tpDateStockOutgoing.Name = "tpDateStockOutgoing"
        Me.tpDateStockOutgoing.Size = New System.Drawing.Size(464, 122)
        Me.tpDateStockOutgoing.Text = "Stock Outgoing Desc"
        '
        'Label11
        '
        Me.Label11.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label11.Location = New System.Drawing.Point(40, 64)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(240, 32)
        Me.Label11.TabIndex = 18
        Me.Label11.Text = "Select the date the stock left inventory."
        '
        'tpDateStocktake
        '
        Me.tpDateStocktake.Controls.Add(Me.Label13)
        Me.tpDateStocktake.Controls.Add(Me.Label12)
        Me.tpDateStocktake.Name = "tpDateStocktake"
        Me.tpDateStocktake.Size = New System.Drawing.Size(464, 122)
        Me.tpDateStocktake.Text = "Stocktake Desc"
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label13.Location = New System.Drawing.Point(0, 32)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(464, 88)
        Me.Label13.TabIndex = 19
        Me.Label13.Text = "The stocktake should always be entered as the closing stock of a given date.  For" & _
        " example, if a stocktake took place on a Tuesday evening (after all of the jobs " & _
        "have been cut for the day), then Tuesday's date should be entered here, however," & _
        " if the stocktake was done on Tuesday morning (before all of the jobs are cut fo" & _
        "r the day), then Monday's date should be entered here, because you are entering " & _
        "the closing stock on Monday, not Tuesday."
        '
        'Label12
        '
        Me.Label12.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label12.Location = New System.Drawing.Point(0, 8)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(376, 24)
        Me.Label12.TabIndex = 18
        Me.Label12.Text = "Select the date that the stocktake was counted (at the close of business)."
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(176, 240)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(168, 21)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Date of the stock adjustment:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'HorizonalRuleLine3D2
        '
        Me.HorizonalRuleLine3D2.BackColor = System.Drawing.SystemColors.Control
        Me.HorizonalRuleLine3D2.Dock = System.Windows.Forms.DockStyle.Top
        Me.HorizonalRuleLine3D2.Location = New System.Drawing.Point(0, 56)
        Me.HorizonalRuleLine3D2.Name = "HorizonalRuleLine3D2"
        Me.HorizonalRuleLine3D2.Size = New System.Drawing.Size(714, 2)
        Me.HorizonalRuleLine3D2.TabIndex = 15
        '
        'PanelControl4
        '
        Me.PanelControl4.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.PanelControl4.Appearance.Options.UseBackColor = True
        Me.PanelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl4.Controls.Add(Me.Label1)
        Me.PanelControl4.Controls.Add(Me.Label4)
        Me.PanelControl4.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl4.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(714, 56)
        Me.PanelControl4.TabIndex = 14
        Me.PanelControl4.Text = "PanelControl4"
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Location = New System.Drawing.Point(24, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(674, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Enter the date the adjustment took place."
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(8, 8)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(192, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Stock Adjustment Date"
        '
        'XtraTabPage4
        '
        Me.XtraTabPage4.Controls.Add(Me.dgMaterials_StockAdjustments)
        Me.XtraTabPage4.Controls.Add(Me.tvStockedItems)
        Me.XtraTabPage4.Controls.Add(Me.Label22)
        Me.XtraTabPage4.Controls.Add(Me.Label23)
        Me.XtraTabPage4.Controls.Add(Me.btnRemoveMaterial)
        Me.XtraTabPage4.Controls.Add(Me.btnAddMaterial)
        Me.XtraTabPage4.Controls.Add(Me.HorizonalRuleLine3D3)
        Me.XtraTabPage4.Controls.Add(Me.PanelControl5)
        Me.XtraTabPage4.Name = "XtraTabPage4"
        Me.XtraTabPage4.Size = New System.Drawing.Size(714, 394)
        Me.XtraTabPage4.Text = "Stocked Items"
        '
        'dgMaterials_StockAdjustments
        '
        Me.dgMaterials_StockAdjustments.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgMaterials_StockAdjustments.DataSource = Me.DsGTMS.VStockAdjustments_StockedItems
        '
        'dgMaterials_StockAdjustments.EmbeddedNavigator
        '
        Me.dgMaterials_StockAdjustments.EmbeddedNavigator.Name = ""
        Me.dgMaterials_StockAdjustments.Location = New System.Drawing.Point(352, 88)
        Me.dgMaterials_StockAdjustments.MainView = Me.gvMaterials_StockAdjustments
        Me.dgMaterials_StockAdjustments.Name = "dgMaterials_StockAdjustments"
        Me.dgMaterials_StockAdjustments.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtSSInventoryAmount})
        Me.dgMaterials_StockAdjustments.Size = New System.Drawing.Size(352, 296)
        Me.dgMaterials_StockAdjustments.TabIndex = 5
        Me.dgMaterials_StockAdjustments.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvMaterials_StockAdjustments})
        '
        'gvMaterials_StockAdjustments
        '
        Me.gvMaterials_StockAdjustments.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvMaterials_StockAdjustments.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvMaterials_StockAdjustments.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvMaterials_StockAdjustments.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvMaterials_StockAdjustments.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvMaterials_StockAdjustments.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvMaterials_StockAdjustments.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvMaterials_StockAdjustments.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvMaterials_StockAdjustments.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvMaterials_StockAdjustments.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvMaterials_StockAdjustments.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvMaterials_StockAdjustments.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvMaterials_StockAdjustments.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvMaterials_StockAdjustments.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvMaterials_StockAdjustments.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvMaterials_StockAdjustments.Appearance.VertLine.Options.UseBackColor = True
        Me.gvMaterials_StockAdjustments.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSSInventoryAmount, Me.colSIInventoryUOMShortName, Me.colSIName})
        Me.gvMaterials_StockAdjustments.GridControl = Me.dgMaterials_StockAdjustments
        Me.gvMaterials_StockAdjustments.Name = "gvMaterials_StockAdjustments"
        Me.gvMaterials_StockAdjustments.OptionsCustomization.AllowFilter = False
        Me.gvMaterials_StockAdjustments.OptionsView.ShowGroupPanel = False
        Me.gvMaterials_StockAdjustments.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colSIName, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colSSInventoryAmount
        '
        Me.colSSInventoryAmount.Caption = "Amount"
        Me.colSSInventoryAmount.ColumnEdit = Me.txtSSInventoryAmount
        Me.colSSInventoryAmount.FieldName = "SSInventoryAmount"
        Me.colSSInventoryAmount.Name = "colSSInventoryAmount"
        Me.colSSInventoryAmount.Visible = True
        Me.colSSInventoryAmount.VisibleIndex = 1
        Me.colSSInventoryAmount.Width = 64
        '
        'txtSSInventoryAmount
        '
        Me.txtSSInventoryAmount.AutoHeight = False
        Me.txtSSInventoryAmount.DisplayFormat.FormatString = "#0"
        Me.txtSSInventoryAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtSSInventoryAmount.EditFormat.FormatString = "#0"
        Me.txtSSInventoryAmount.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtSSInventoryAmount.Name = "txtSSInventoryAmount"
        '
        'colSIInventoryUOMShortName
        '
        Me.colSIInventoryUOMShortName.FieldName = "SIInventoryUOMShortName"
        Me.colSIInventoryUOMShortName.Name = "colSIInventoryUOMShortName"
        Me.colSIInventoryUOMShortName.OptionsColumn.AllowEdit = False
        Me.colSIInventoryUOMShortName.OptionsColumn.AllowFocus = False
        Me.colSIInventoryUOMShortName.Visible = True
        Me.colSIInventoryUOMShortName.VisibleIndex = 2
        Me.colSIInventoryUOMShortName.Width = 69
        '
        'colSIName
        '
        Me.colSIName.Caption = "Stocked item"
        Me.colSIName.FieldName = "SIName"
        Me.colSIName.Name = "colSIName"
        Me.colSIName.OptionsColumn.AllowEdit = False
        Me.colSIName.OptionsColumn.AllowFocus = False
        Me.colSIName.Visible = True
        Me.colSIName.VisibleIndex = 0
        Me.colSIName.Width = 165
        '
        'tvStockedItems
        '
        Me.tvStockedItems.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.tvStockedItems.Connection = Me.SqlConnection
        Me.tvStockedItems.HideSelection = False
        Me.tvStockedItems.ImageIndex = -1
        Me.tvStockedItems.Location = New System.Drawing.Point(8, 88)
        Me.tvStockedItems.Name = "tvStockedItems"
        Me.tvStockedItems.SelectedImageIndex = -1
        Me.tvStockedItems.Size = New System.Drawing.Size(240, 296)
        Me.tvStockedItems.TabIndex = 1
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = CType(configurationAppSettings.GetValue("SqlConnection.ConnectionString", GetType(System.String)), String)
        '
        'Label22
        '
        Me.Label22.Location = New System.Drawing.Point(8, 64)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(232, 21)
        Me.Label22.TabIndex = 0
        Me.Label22.Text = "Available stocked items:"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label23
        '
        Me.Label23.Location = New System.Drawing.Point(352, 64)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(240, 21)
        Me.Label23.TabIndex = 4
        Me.Label23.Text = "Stocked items in adjustment:"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnRemoveMaterial
        '
        Me.btnRemoveMaterial.Image = CType(resources.GetObject("btnRemoveMaterial.Image"), System.Drawing.Image)
        Me.btnRemoveMaterial.Location = New System.Drawing.Point(264, 184)
        Me.btnRemoveMaterial.Name = "btnRemoveMaterial"
        Me.btnRemoveMaterial.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveMaterial.TabIndex = 3
        Me.btnRemoveMaterial.Text = "Remove"
        '
        'btnAddMaterial
        '
        Me.btnAddMaterial.Image = CType(resources.GetObject("btnAddMaterial.Image"), System.Drawing.Image)
        Me.btnAddMaterial.ImageAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnAddMaterial.Location = New System.Drawing.Point(264, 152)
        Me.btnAddMaterial.Name = "btnAddMaterial"
        Me.btnAddMaterial.Size = New System.Drawing.Size(72, 23)
        Me.btnAddMaterial.TabIndex = 2
        Me.btnAddMaterial.Text = "Add"
        '
        'HorizonalRuleLine3D3
        '
        Me.HorizonalRuleLine3D3.BackColor = System.Drawing.SystemColors.Control
        Me.HorizonalRuleLine3D3.Dock = System.Windows.Forms.DockStyle.Top
        Me.HorizonalRuleLine3D3.Location = New System.Drawing.Point(0, 56)
        Me.HorizonalRuleLine3D3.Name = "HorizonalRuleLine3D3"
        Me.HorizonalRuleLine3D3.Size = New System.Drawing.Size(714, 2)
        Me.HorizonalRuleLine3D3.TabIndex = 15
        '
        'PanelControl5
        '
        Me.PanelControl5.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.PanelControl5.Appearance.Options.UseBackColor = True
        Me.PanelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl5.Controls.Add(Me.Label5)
        Me.PanelControl5.Controls.Add(Me.Label6)
        Me.PanelControl5.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl5.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl5.Name = "PanelControl5"
        Me.PanelControl5.Size = New System.Drawing.Size(714, 56)
        Me.PanelControl5.TabIndex = 14
        Me.PanelControl5.Text = "PanelControl5"
        '
        'Label5
        '
        Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Location = New System.Drawing.Point(24, 32)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(674, 16)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Add the stocked items and amounts to the stock adjustment."
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(8, 8)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(112, 16)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Stocked Items"
        '
        'XtraTabPage5
        '
        Me.XtraTabPage5.Controls.Add(Me.PanelControl11)
        Me.XtraTabPage5.Controls.Add(Me.PanelControl12)
        Me.XtraTabPage5.Name = "XtraTabPage5"
        Me.XtraTabPage5.Size = New System.Drawing.Size(714, 394)
        Me.XtraTabPage5.Text = "Finish"
        '
        'PanelControl11
        '
        Me.PanelControl11.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.PanelControl11.Appearance.Options.UseBackColor = True
        Me.PanelControl11.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl11.Controls.Add(Me.lblFinishDesc2)
        Me.PanelControl11.Controls.Add(Me.Label17)
        Me.PanelControl11.Controls.Add(Me.lblFinishDesc1)
        Me.PanelControl11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl11.Location = New System.Drawing.Point(120, 0)
        Me.PanelControl11.Name = "PanelControl11"
        Me.PanelControl11.Size = New System.Drawing.Size(594, 394)
        Me.PanelControl11.TabIndex = 9
        Me.PanelControl11.Text = "PanelControl11"
        '
        'lblFinishDesc2
        '
        Me.lblFinishDesc2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblFinishDesc2.BackColor = System.Drawing.Color.Transparent
        Me.lblFinishDesc2.Location = New System.Drawing.Point(16, 120)
        Me.lblFinishDesc2.Name = "lblFinishDesc2"
        Me.lblFinishDesc2.Size = New System.Drawing.Size(562, 40)
        Me.lblFinishDesc2.TabIndex = 4
        Me.lblFinishDesc2.Text = "Click 'Finish' to complete the wizard and add the new stock adjustment."
        '
        'Label17
        '
        Me.Label17.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(16, 24)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(562, 48)
        Me.Label17.TabIndex = 3
        Me.Label17.Text = "Wizard Completed Successfully"
        '
        'lblFinishDesc1
        '
        Me.lblFinishDesc1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblFinishDesc1.BackColor = System.Drawing.Color.Transparent
        Me.lblFinishDesc1.Location = New System.Drawing.Point(16, 80)
        Me.lblFinishDesc1.Name = "lblFinishDesc1"
        Me.lblFinishDesc1.Size = New System.Drawing.Size(562, 32)
        Me.lblFinishDesc1.TabIndex = 0
        Me.lblFinishDesc1.Text = "The wizard now has enough information to add the new stock adjustment."
        '
        'PanelControl12
        '
        Me.PanelControl12.Appearance.BackColor = System.Drawing.Color.MidnightBlue
        Me.PanelControl12.Appearance.Options.UseBackColor = True
        Me.PanelControl12.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl12.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelControl12.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl12.Name = "PanelControl12"
        Me.PanelControl12.Size = New System.Drawing.Size(120, 394)
        Me.PanelControl12.TabIndex = 8
        Me.PanelControl12.Text = "PanelControl12"
        '
        'HorizonalRuleLine3D8
        '
        Me.HorizonalRuleLine3D8.BackColor = System.Drawing.SystemColors.Control
        Me.HorizonalRuleLine3D8.Dock = System.Windows.Forms.DockStyle.Top
        Me.HorizonalRuleLine3D8.Location = New System.Drawing.Point(0, 416)
        Me.HorizonalRuleLine3D8.Name = "HorizonalRuleLine3D8"
        Me.HorizonalRuleLine3D8.Size = New System.Drawing.Size(714, 2)
        Me.HorizonalRuleLine3D8.TabIndex = 10
        '
        'btnBack
        '
        Me.btnBack.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBack.Enabled = False
        Me.btnBack.Location = New System.Drawing.Point(480, 424)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(72, 23)
        Me.btnBack.TabIndex = 2
        Me.btnBack.Text = "< &Back"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(632, 424)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        '
        'btnNext
        '
        Me.btnNext.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNext.Location = New System.Drawing.Point(552, 424)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(72, 23)
        Me.btnNext.TabIndex = 3
        Me.btnNext.Text = "&Next >"
        '
        'daSATypes
        '
        Me.daSATypes.DeleteCommand = Me.SqlDeleteCommand4
        Me.daSATypes.InsertCommand = Me.SqlInsertCommand4
        Me.daSATypes.SelectCommand = Me.SqlSelectCommand4
        Me.daSATypes.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "SATypes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("SAType", "SAType"), New System.Data.Common.DataColumnMapping("SATypeDisplay", "SATypeDisplay")})})
        Me.daSATypes.UpdateCommand = Me.SqlUpdateCommand4
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = "DELETE FROM SATypes WHERE (SAType = @Original_SAType) AND (SATypeDisplay = @Origi" & _
        "nal_SATypeDisplay OR @Original_SATypeDisplay IS NULL AND SATypeDisplay IS NULL)"
        Me.SqlDeleteCommand4.Connection = Me.SqlConnection
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SAType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SAType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SATypeDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SATypeDisplay", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = "INSERT INTO SATypes(SAType, SATypeDisplay) VALUES (@SAType, @SATypeDisplay); SELE" & _
        "CT SAType, SATypeDisplay FROM SATypes WHERE (SAType = @SAType)"
        Me.SqlInsertCommand4.Connection = Me.SqlConnection
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SAType", System.Data.SqlDbType.VarChar, 2, "SAType"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SATypeDisplay", System.Data.SqlDbType.VarChar, 50, "SATypeDisplay"))
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT SAType, SATypeDisplay FROM SATypes"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = "UPDATE SATypes SET SAType = @SAType, SATypeDisplay = @SATypeDisplay WHERE (SAType" & _
        " = @Original_SAType) AND (SATypeDisplay = @Original_SATypeDisplay OR @Original_S" & _
        "ATypeDisplay IS NULL AND SATypeDisplay IS NULL); SELECT SAType, SATypeDisplay FR" & _
        "OM SATypes WHERE (SAType = @SAType)"
        Me.SqlUpdateCommand4.Connection = Me.SqlConnection
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SAType", System.Data.SqlDbType.VarChar, 2, "SAType"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SATypeDisplay", System.Data.SqlDbType.VarChar, 50, "SATypeDisplay"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SAType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SAType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SATypeDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SATypeDisplay", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "StockAdjustments", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("SAID", "SAID"), New System.Data.Common.DataColumnMapping("SADate", "SADate"), New System.Data.Common.DataColumnMapping("SAType", "SAType")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM StockAdjustments WHERE (BRID = @Original_BRID) AND (SAID = @Original_" & _
        "SAID) AND (SADate = @Original_SADate OR @Original_SADate IS NULL AND SADate IS N" & _
        "ULL) AND (SAType = @Original_SAType OR @Original_SAType IS NULL AND SAType IS NU" & _
        "LL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SAID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SAID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SADate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SADate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SAType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SAType", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO StockAdjustments(BRID, SADate, SAType) VALUES (@BRID, @SADate, @SATyp" & _
        "e); SELECT BRID, SAID, SADate, SAType FROM StockAdjustments WHERE (BRID = @BRID)" & _
        " AND (SAID = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SADate", System.Data.SqlDbType.DateTime, 8, "SADate"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SAType", System.Data.SqlDbType.VarChar, 2, "SAType"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, SAID, SADate, SAType, rowguid FROM StockAdjustments WHERE (BRID = @B" & _
        "RID) AND (SAID = @SAID)"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SAID", System.Data.SqlDbType.BigInt, 8, "SAID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE StockAdjustments SET BRID = @BRID, SADate = @SADate, SAType = @SAType WHER" & _
        "E (BRID = @Original_BRID) AND (SAID = @Original_SAID) AND (SADate = @Original_SA" & _
        "Date OR @Original_SADate IS NULL AND SADate IS NULL) AND (SAType = @Original_SAT" & _
        "ype OR @Original_SAType IS NULL AND SAType IS NULL); SELECT BRID, SAID, SADate, " & _
        "SAType FROM StockAdjustments WHERE (BRID = @BRID) AND (SAID = @SAID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SADate", System.Data.SqlDbType.DateTime, 8, "SADate"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SAType", System.Data.SqlDbType.VarChar, 2, "SAType"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SAID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SAID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SADate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SADate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SAType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SAType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SAID", System.Data.SqlDbType.BigInt, 8, "SAID"))
        '
        'daStockedItems_StockAdjustments
        '
        Me.daStockedItems_StockAdjustments.DeleteCommand = Me.SqlDeleteCommand5
        Me.daStockedItems_StockAdjustments.InsertCommand = Me.SqlInsertCommand5
        Me.daStockedItems_StockAdjustments.SelectCommand = Me.SqlSelectCommand5
        Me.daStockedItems_StockAdjustments.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VStockAdjustments_StockedItems", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("SAID", "SAID"), New System.Data.Common.DataColumnMapping("SIID", "SIID"), New System.Data.Common.DataColumnMapping("SSInventoryAmount", "SSInventoryAmount")})})
        Me.daStockedItems_StockAdjustments.UpdateCommand = Me.SqlUpdateCommand5
        '
        'SqlDeleteCommand5
        '
        Me.SqlDeleteCommand5.CommandText = "DELETE FROM StockAdjustments_StockedItems WHERE (BRID = @Original_BRID) AND (SAID" & _
        " = @Original_SAID) AND (SIID = @Original_SIID) AND (SSInventoryAmount = @Origina" & _
        "l_SSInventoryAmount OR @Original_SSInventoryAmount IS NULL AND SSInventoryAmount" & _
        " IS NULL)"
        Me.SqlDeleteCommand5.Connection = Me.SqlConnection
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SAID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SAID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "SSInventoryAmount", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand5
        '
        Me.SqlInsertCommand5.CommandText = "INSERT INTO StockAdjustments_StockedItems (BRID, SAID, SIID, SSInventoryAmount) V" & _
        "ALUES (@BRID, @SAID, @SIID, @SSInventoryAmount); SELECT BRID, SAID, SIID, SSInve" & _
        "ntoryAmount, SIInventoryUOMShortName, SIName FROM VStockAdjustments_StockedItems" & _
        " WHERE (BRID = @BRID) AND (SAID = @SAID) AND (SIID = @SIID)"
        Me.SqlInsertCommand5.Connection = Me.SqlConnection
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SAID", System.Data.SqlDbType.BigInt, 8, "SAID"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIID", System.Data.SqlDbType.Int, 4, "SIID"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "SSInventoryAmount", System.Data.DataRowVersion.Current, Nothing))
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT BRID, SAID, SIID, SSInventoryAmount, SIInventoryUOMShortName, SIName FROM " & _
        "VStockAdjustments_StockedItems WHERE (BRID = @BRID) AND (SAID = @SAID)"
        Me.SqlSelectCommand5.Connection = Me.SqlConnection
        Me.SqlSelectCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SAID", System.Data.SqlDbType.BigInt, 8, "SAID"))
        '
        'SqlUpdateCommand5
        '
        Me.SqlUpdateCommand5.CommandText = "UPDATE StockAdjustments_StockedItems SET BRID = @BRID, SAID = @SAID, SIID = @SIID" & _
        ", SSInventoryAmount = @SSInventoryAmount WHERE (BRID = @Original_BRID) AND (SAID" & _
        " = @Original_SAID) AND (SIID = @Original_SIID) AND (SSInventoryAmount = @Origina" & _
        "l_SSInventoryAmount OR @Original_SSInventoryAmount IS NULL AND SSInventoryAmount" & _
        " IS NULL); SELECT BRID, SAID, SIID, SSInventoryAmount, SIInventoryUOMShortName, " & _
        "SIName FROM VStockAdjustments_StockedItems WHERE (BRID = @BRID) AND (SAID = @SAI" & _
        "D) AND (SIID = @SIID)"
        Me.SqlUpdateCommand5.Connection = Me.SqlConnection
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SAID", System.Data.SqlDbType.BigInt, 8, "SAID"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIID", System.Data.SqlDbType.Int, 4, "SIID"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "SSInventoryAmount", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SAID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SAID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "SSInventoryAmount", System.Data.DataRowVersion.Original, Nothing))
        '
        'btnHelp
        '
        Me.btnHelp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnHelp.Image = CType(resources.GetObject("btnHelp.Image"), System.Drawing.Image)
        Me.btnHelp.Location = New System.Drawing.Point(8, 424)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(72, 24)
        Me.btnHelp.TabIndex = 1
        Me.btnHelp.Text = "Help"
        '
        'frmStockAdjustmentWizard
        '
        Me.AcceptButton = Me.btnNext
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(714, 456)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnHelp)
        Me.Controls.Add(Me.btnBack)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnNext)
        Me.Controls.Add(Me.HorizonalRuleLine3D8)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmStockAdjustmentWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add New Stock Adjustment Wizard"
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.txtSAType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.XtraTabPage3.ResumeLayout(False)
        CType(Me.txtSADate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl2.ResumeLayout(False)
        Me.tpDateStockIncoming.ResumeLayout(False)
        Me.tpDateStockOutgoing.ResumeLayout(False)
        Me.tpDateStocktake.ResumeLayout(False)
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        Me.XtraTabPage4.ResumeLayout(False)
        CType(Me.dgMaterials_StockAdjustments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvMaterials_StockAdjustments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSSInventoryAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl5.ResumeLayout(False)
        Me.XtraTabPage5.ResumeLayout(False)
        CType(Me.PanelControl11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl11.ResumeLayout(False)
        CType(Me.PanelControl12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmStockAdjustmentWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.XtraTabControl1.SelectedTabPageIndex = 0
        Me.XtraTabControl1.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False
        Me.XtraTabControl2.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False
    End Sub

    Private Sub LoadTreeViews()
        ' --- STOCKED ITEMS ---
        tvStockedItems.Filter.SetOrAddField("BRID", BRID)
        tvStockedItems.LoadFromStream(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream("WindowsApplication.xmlStockedItemsTree.xml"))
        tvStockedItems.ExpandAll()
    End Sub

    Private Sub FillPreliminaryData()
        ' ---  UOMs ---
        daSATypes.Fill(DsGTMS)
    End Sub
    Private Sub FillData()
        ' --- MATERIALS ---
        daStockedItems_StockAdjustments.SelectCommand.Parameters("@BRID").Value = BRID
        daStockedItems_StockAdjustments.SelectCommand.Parameters("@SAID").Value = SAID
        daStockedItems_StockAdjustments.Fill(DsGTMS)
    End Sub

    Dim OK As Boolean = False
    Private Sub RunOK()
        ' EndEdit() to end editing the dataset record so that we can update
        DataRow.EndEdit()
        SqlDataAdapter.Update(DsGTMS)
        Me.daStockedItems_StockAdjustments.Update(DsGTMS)

        DataAccess.spExecLockRequest("sp_ReleaseStockAdjustmentLock", BRID, SAID, Transaction)
        Transaction.Commit()
        SqlConnection.Close()

        OK = True
        Me.Close()
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        btnBack.Enabled = True
        If ValidateTab(XtraTabControl1.SelectedTabPageIndex) Then
            If XtraTabControl1.SelectedTabPageIndex = XtraTabControl1.TabPages.Count - 1 Then
                RunOK()
            Else
                XtraTabControl1.SelectedTabPageIndex = NextTabIndex(XtraTabControl1.SelectedTabPageIndex)
            End If
        End If
    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        XtraTabControl1.SelectedTabPageIndex = PreviousTabIndex(XtraTabControl1.SelectedTabPageIndex)
    End Sub

    Private Function NextTabIndex(ByVal CurrentTabIndex As Integer) As Integer
        If CurrentTabIndex = XtraTabControl1.TabPages.Count - 1 Then
            Throw New ArgumentOutOfRangeException("CurrentTabIndex", CurrentTabIndex)
        End If
        NextTabIndex = CurrentTabIndex + 1
        Select Case NextTabIndex
            Case 2
                Select Case txtSAType.EditValue
                    Case "SI"
                        XtraTabControl2.SelectedTabPageIndex = 0
                    Case "SO"
                        XtraTabControl2.SelectedTabPageIndex = 1
                    Case "IA"
                        XtraTabControl2.SelectedTabPageIndex = 2
                End Select
            Case XtraTabControl1.TabPages.Count - 1
                btnNext.Text = "&Finish"
                btnNext.DialogResult = DialogResult.OK
        End Select
        Return NextTabIndex ' Found the correct value
    End Function

    Private Function PreviousTabIndex(ByVal CurrentTabIndex As Integer) As Integer
        If CurrentTabIndex = XtraTabControl1.TabPages.Count - 1 Then
            btnNext.Text = "&Next >"
            btnNext.DialogResult = DialogResult.None
        End If
        PreviousTabIndex = CurrentTabIndex - 1
        Select Case PreviousTabIndex
            Case 0
                btnBack.Enabled = False
        End Select
        Return PreviousTabIndex ' Found the correct value
    End Function

    Private Function ValidateTab(ByVal Tab As Integer) As Boolean
        Select Case Tab
            Case 1
                If DataRow("SAType") Is DBNull.Value Then
                    Message.ShowMessage("You must enter a Stock Adjustment Type.", MessageBoxIcon.Exclamation)
                    Return False
                End If
            Case 2
                If DataRow("SADate") Is DBNull.Value Then
                    Message.ShowMessage("You must enter a Date.", MessageBoxIcon.Exclamation)
                    Return False
                End If
        End Select
        Return True
    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private HasChanges As Boolean = False
    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            DataRow.EndEdit()
            If HasChanges Or DsGTMS.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If response = MsgBoxResult.Yes Then
                DataAccess.spExecLockRequest("sp_ReleaseStockAdjustmentLock", BRID, SAID, Transaction)
                Transaction.Rollback()
                SqlConnection.Close()
            Else
                e.Cancel = True
            End If
        End If
    End Sub

#Region " Materials "

    Public ReadOnly Property SelectedMaterial() As DataRow
        Get
            If Not gvMaterials_StockAdjustments.GetSelectedRows Is Nothing Then
                Return gvMaterials_StockAdjustments.GetDataRow(gvMaterials_StockAdjustments.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub btnAddMaterial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddMaterial.Click
        Dim row As dsGTMS.VStockAdjustments_StockedItemsRow = DsGTMS.VStockAdjustments_StockedItems.NewVStockAdjustments_StockedItemsRow
        row.BRID = BRID
        row.SAID = SAID
        If CType(tvStockedItems.SelectedNode, Power.Forms.TreeNode).Action = "ADD" Then
            row.SIID = CType(tvStockedItems.SelectedNode, Power.Forms.TreeNode).PKey("SIID")
        ElseIf CType(tvStockedItems.SelectedNode, Power.Forms.TreeNode).Action = "LIST" Then
            Message.ShowMessage("You cannot add this item. You may however select from the list of " & tvStockedItems.SelectedNode.Text & ".", MsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            DsGTMS.VStockAdjustments_StockedItems.AddVStockAdjustments_StockedItemsRow(row)
            daStockedItems_StockAdjustments.Update(DsGTMS)
            HasChanges = True
        Catch ex As System.Data.ConstraintException
            row.Delete()
            Message.ShowMessage(tvStockedItems.SelectedNode.Text & " could not be added, as it already exists in the stock adjustment.", MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub btnRemoveMaterial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveMaterial.Click
        If Not SelectedMaterial Is Nothing Then
            SelectedMaterial.Delete()
        End If
    End Sub

#End Region

    Private Sub Decimal_WholeNumbers_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) _
    Handles txtSSInventoryAmount.ParseEditValue
        Format.WholeNumber_ParseEditValue(sender, e)
    End Sub

    Private Sub Date_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtSADate.Validating
        Library.DateEdit_Validating(sender, e)
    End Sub

    Private Sub btnHelp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHelp.Click
        ShowHelpTopic(Me, "StockTrackingTerms.html")
    End Sub

End Class

