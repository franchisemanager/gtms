Imports Power.Forms

Public Class lvExpenseAllowances2
    Inherits System.Windows.Forms.UserControl
    Implements IListControl, IPrintableControl

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Connection As SqlClient.SqlConnection, ByVal BRID As Integer, ByVal ExplorerForm As frmExplorer)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.Connection = Connection
        Me.BRID = BRID
        hExplorerForm = ExplorerForm
        FillDataSet()
        EnableDisable()
    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents DsGTMS As WindowsApplication.dsGTMS
    Friend WithEvents hSqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colEXName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEXTypeDisplay As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEXPaymentMethodDisplay As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEGName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPMAllowancesPaymentTypeDisplay As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BarAndDockingController1 As DevExpress.XtraBars.BarAndDockingController
    Friend WithEvents PopupMenu1 As DevExpress.XtraBars.PopupMenu
    Friend WithEvents subReportsOnSelectedItem As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnEdit As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents bStandard As DevExpress.XtraBars.Bar
    Friend WithEvents btnPrintPreview As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnQuickPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnHelp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ImageListToolBar As System.Windows.Forms.ImageList
    Friend WithEvents btnPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subReportsOnSelectedItem2 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents colEXAllowancesDisplayAmount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEXDiscontinued_Display As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim StyleFormatCondition1 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(lvExpenseAllowances2))
        Me.colEXDiscontinued_Display = New DevExpress.XtraGrid.Columns.GridColumn
        Me.hSqlConnection = New System.Data.SqlClient.SqlConnection
        Me.DsGTMS = New WindowsApplication.dsGTMS
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colEXName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colEXTypeDisplay = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colEXPaymentMethodDisplay = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colEGName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colPMAllowancesPaymentTypeDisplay = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colEXAllowancesDisplayAmount = New DevExpress.XtraGrid.Columns.GridColumn
        Me.BarAndDockingController1 = New DevExpress.XtraBars.BarAndDockingController(Me.components)
        Me.PopupMenu1 = New DevExpress.XtraBars.PopupMenu
        Me.subReportsOnSelectedItem = New DevExpress.XtraBars.BarSubItem
        Me.btnEdit = New DevExpress.XtraBars.BarButtonItem
        Me.BarManager1 = New DevExpress.XtraBars.BarManager
        Me.bStandard = New DevExpress.XtraBars.Bar
        Me.btnQuickPrint = New DevExpress.XtraBars.BarButtonItem
        Me.btnPrintPreview = New DevExpress.XtraBars.BarButtonItem
        Me.btnHelp = New DevExpress.XtraBars.BarButtonItem
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
        Me.ImageListToolBar = New System.Windows.Forms.ImageList(Me.components)
        Me.btnPrint = New DevExpress.XtraBars.BarButtonItem
        Me.subReportsOnSelectedItem2 = New DevExpress.XtraBars.BarSubItem
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'colEXDiscontinued_Display
        '
        Me.colEXDiscontinued_Display.Caption = "Status"
        Me.colEXDiscontinued_Display.FieldName = "EXDiscontinued_Display"
        Me.colEXDiscontinued_Display.Name = "colEXDiscontinued_Display"
        Me.colEXDiscontinued_Display.Visible = True
        Me.colEXDiscontinued_Display.VisibleIndex = 5
        '
        'hSqlConnection
        '
        Me.hSqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;integrated security=SSPI;data source=""SERVER" & _
        "\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'DsGTMS
        '
        Me.DsGTMS.DataSetName = "dsGTMS"
        Me.DsGTMS.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VExpenses_Display", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("EGID", "EGID"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("EXName", "EXName"), New System.Data.Common.DataColumnMapping("EXDiscontinued", "EXDiscontinued"), New System.Data.Common.DataColumnMapping("EXAssignToPortionDisplay", "EXAssignToPortionDisplay"), New System.Data.Common.DataColumnMapping("EXPaymentMethodDisplay", "EXPaymentMethodDisplay"), New System.Data.Common.DataColumnMapping("EXTypeDisplay", "EXTypeDisplay"), New System.Data.Common.DataColumnMapping("EGName", "EGName")})})
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO VExpenses_Display(BRID, EGID, EXName, EXDiscontinued, EXAssignToPorti" & _
        "onDisplay, EXPaymentMethodDisplay, EXTypeDisplay, EGName) VALUES (@BRID, @EGID, " & _
        "@EXName, @EXDiscontinued, @EXAssignToPortionDisplay, @EXPaymentMethodDisplay, @E" & _
        "XTypeDisplay, @EGName); SELECT BRID, EGID, EXID, EXName, EXDiscontinued, EXAssig" & _
        "nToPortionDisplay, EXPaymentMethodDisplay, EXTypeDisplay, EGName FROM VExpenses_" & _
        "Display"
        Me.SqlInsertCommand1.Connection = Me.hSqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGID", System.Data.SqlDbType.SmallInt, 2, "EGID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXDiscontinued", System.Data.SqlDbType.VarChar, 12, "EXDiscontinued"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAssignToPortionDisplay", System.Data.SqlDbType.VarChar, 50, "EXAssignToPortionDisplay"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXPaymentMethodDisplay", System.Data.SqlDbType.VarChar, 50, "EXPaymentMethodDisplay"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXTypeDisplay", System.Data.SqlDbType.VarChar, 50, "EXTypeDisplay"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGName", System.Data.SqlDbType.VarChar, 50, "EGName"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, EGID, EXID, EXName, EXDiscontinued, EXAssignToPortionDisplay, EXPaym" & _
        "entMethodDisplay, EXTypeDisplay, EGName, PMAllowancesPaymentTypeDisplay, EXAllow" & _
        "ancesDisplayAmount, EXDiscontinued_Display FROM VExpenses_Display WHERE (BRID = " & _
        "@BRID) AND (PMShowInAllowances = 1)"
        Me.SqlSelectCommand1.Connection = Me.hSqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.DsGTMS.VExpenses_Display
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'GridControl1.EmbeddedNavigator
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.Location = New System.Drawing.Point(0, 24)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(632, 368)
        Me.GridControl1.TabIndex = 2
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
        Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colEXName, Me.colEXTypeDisplay, Me.colEXPaymentMethodDisplay, Me.colEGName, Me.colPMAllowancesPaymentTypeDisplay, Me.colEXAllowancesDisplayAmount, Me.colEXDiscontinued_Display})
        Me.GridView1.CustomizationFormBounds = New System.Drawing.Rectangle(802, 474, 208, 156)
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        StyleFormatCondition1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Strikeout, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        StyleFormatCondition1.Appearance.ForeColor = System.Drawing.SystemColors.GrayText
        StyleFormatCondition1.Appearance.Options.UseFont = True
        StyleFormatCondition1.Appearance.Options.UseForeColor = True
        StyleFormatCondition1.ApplyToRow = True
        StyleFormatCondition1.Column = Me.colEXDiscontinued_Display
        StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal
        StyleFormatCondition1.Value1 = "Discontinued"
        Me.GridView1.FormatConditions.AddRange(New DevExpress.XtraGrid.StyleFormatCondition() {StyleFormatCondition1})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.GroupCount = 1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsNavigation.AutoFocusNewRow = True
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsView.ShowGroupedColumns = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.OptionsView.ShowHorzLines = False
        Me.GridView1.OptionsView.ShowIndicator = False
        Me.GridView1.OptionsView.ShowVertLines = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colEGName, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colEXName, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colEXName
        '
        Me.colEXName.Caption = "Name"
        Me.colEXName.FieldName = "EXName"
        Me.colEXName.Name = "colEXName"
        Me.colEXName.Visible = True
        Me.colEXName.VisibleIndex = 0
        Me.colEXName.Width = 124
        '
        'colEXTypeDisplay
        '
        Me.colEXTypeDisplay.Caption = "Expense Type"
        Me.colEXTypeDisplay.FieldName = "EXTypeDisplay"
        Me.colEXTypeDisplay.Name = "colEXTypeDisplay"
        Me.colEXTypeDisplay.Visible = True
        Me.colEXTypeDisplay.VisibleIndex = 3
        Me.colEXTypeDisplay.Width = 132
        '
        'colEXPaymentMethodDisplay
        '
        Me.colEXPaymentMethodDisplay.Caption = "Payment Method"
        Me.colEXPaymentMethodDisplay.FieldName = "EXPaymentMethodDisplay"
        Me.colEXPaymentMethodDisplay.Name = "colEXPaymentMethodDisplay"
        Me.colEXPaymentMethodDisplay.Width = 144
        '
        'colEGName
        '
        Me.colEGName.Caption = "Expense Group"
        Me.colEGName.FieldName = "EGName"
        Me.colEGName.Name = "colEGName"
        Me.colEGName.Visible = True
        Me.colEGName.VisibleIndex = 1
        Me.colEGName.Width = 114
        '
        'colPMAllowancesPaymentTypeDisplay
        '
        Me.colPMAllowancesPaymentTypeDisplay.Caption = "Payment Type"
        Me.colPMAllowancesPaymentTypeDisplay.FieldName = "PMAllowancesPaymentTypeDisplay"
        Me.colPMAllowancesPaymentTypeDisplay.Name = "colPMAllowancesPaymentTypeDisplay"
        Me.colPMAllowancesPaymentTypeDisplay.Visible = True
        Me.colPMAllowancesPaymentTypeDisplay.VisibleIndex = 2
        Me.colPMAllowancesPaymentTypeDisplay.Width = 143
        '
        'colEXAllowancesDisplayAmount
        '
        Me.colEXAllowancesDisplayAmount.Caption = "Current Amount"
        Me.colEXAllowancesDisplayAmount.DisplayFormat.FormatString = "c"
        Me.colEXAllowancesDisplayAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colEXAllowancesDisplayAmount.FieldName = "EXAllowancesDisplayAmount"
        Me.colEXAllowancesDisplayAmount.Name = "colEXAllowancesDisplayAmount"
        Me.colEXAllowancesDisplayAmount.Visible = True
        Me.colEXAllowancesDisplayAmount.VisibleIndex = 4
        Me.colEXAllowancesDisplayAmount.Width = 115
        '
        'BarAndDockingController1
        '
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseForeColor = True
        Me.BarAndDockingController1.PaintStyleName = "OfficeXP"
        '
        'PopupMenu1
        '
        Me.PopupMenu1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.subReportsOnSelectedItem, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnEdit, True)})
        Me.PopupMenu1.Manager = Me.BarManager1
        Me.PopupMenu1.Name = "PopupMenu1"
        '
        'subReportsOnSelectedItem
        '
        Me.subReportsOnSelectedItem.Caption = "Reports on Selected Item"
        Me.subReportsOnSelectedItem.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.subReportsOnSelectedItem.Id = 78
        Me.subReportsOnSelectedItem.Name = "subReportsOnSelectedItem"
        Me.subReportsOnSelectedItem.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInCustomizing
        '
        'btnEdit
        '
        Me.btnEdit.Caption = "&Open..."
        Me.btnEdit.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnEdit.Id = 20
        Me.btnEdit.ImageIndex = 1
        Me.btnEdit.ImageIndexDisabled = 1
        Me.btnEdit.Name = "btnEdit"
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.bStandard})
        Me.BarManager1.Categories.AddRange(New DevExpress.XtraBars.BarManagerCategory() {New DevExpress.XtraBars.BarManagerCategory("Built-in Menus", New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd"), False), New DevExpress.XtraBars.BarManagerCategory("File", New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")), New DevExpress.XtraBars.BarManagerCategory("Reports", New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")), New DevExpress.XtraBars.BarManagerCategory("Forms", New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")), New DevExpress.XtraBars.BarManagerCategory("Help", New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785"))})
        Me.BarManager1.Controller = Me.BarAndDockingController1
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Images = Me.ImageListToolBar
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnEdit, Me.btnHelp, Me.btnPrintPreview, Me.btnQuickPrint, Me.btnPrint, Me.subReportsOnSelectedItem, Me.subReportsOnSelectedItem2})
        Me.BarManager1.MainMenu = Me.bStandard
        Me.BarManager1.MaxItemId = 92
        '
        'bStandard
        '
        Me.bStandard.BarName = "Standard Toolbar"
        Me.bStandard.DockCol = 0
        Me.bStandard.DockRow = 0
        Me.bStandard.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bStandard.FloatLocation = New System.Drawing.Point(44, 188)
        Me.bStandard.FloatSize = New System.Drawing.Size(659, 24)
        Me.bStandard.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnEdit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnQuickPrint, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnPrintPreview, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(Me.subReportsOnSelectedItem, True), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnHelp, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.bStandard.OptionsBar.AllowQuickCustomization = False
        Me.bStandard.OptionsBar.DisableClose = True
        Me.bStandard.OptionsBar.DrawDragBorder = False
        Me.bStandard.OptionsBar.MultiLine = True
        Me.bStandard.OptionsBar.UseWholeRow = True
        Me.bStandard.Text = "Standard Toolbar"
        '
        'btnQuickPrint
        '
        Me.btnQuickPrint.Caption = "Print"
        Me.btnQuickPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnQuickPrint.Id = 46
        Me.btnQuickPrint.ImageIndex = 11
        Me.btnQuickPrint.ImageIndexDisabled = 11
        Me.btnQuickPrint.Name = "btnQuickPrint"
        '
        'btnPrintPreview
        '
        Me.btnPrintPreview.Caption = "Print Pre&view"
        Me.btnPrintPreview.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrintPreview.Id = 44
        Me.btnPrintPreview.ImageIndex = 10
        Me.btnPrintPreview.ImageIndexDisabled = 10
        Me.btnPrintPreview.Name = "btnPrintPreview"
        '
        'btnHelp
        '
        Me.btnHelp.Caption = "&Help"
        Me.btnHelp.CategoryGuid = New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")
        Me.btnHelp.Id = 68
        Me.btnHelp.ImageIndex = 4
        Me.btnHelp.ImageIndexDisabled = 4
        Me.btnHelp.Name = "btnHelp"
        '
        'ImageListToolBar
        '
        Me.ImageListToolBar.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImageListToolBar.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageListToolBar.ImageStream = CType(resources.GetObject("ImageListToolBar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListToolBar.TransparentColor = System.Drawing.Color.Transparent
        '
        'btnPrint
        '
        Me.btnPrint.Caption = "&Print..."
        Me.btnPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrint.Id = 43
        Me.btnPrint.ImageIndex = 11
        Me.btnPrint.ImageIndexDisabled = 11
        Me.btnPrint.Name = "btnPrint"
        '
        'subReportsOnSelectedItem2
        '
        Me.subReportsOnSelectedItem2.Caption = "&Reports on Selected Item"
        Me.subReportsOnSelectedItem2.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subReportsOnSelectedItem2.Id = 87
        Me.subReportsOnSelectedItem2.Name = "subReportsOnSelectedItem2"
        Me.subReportsOnSelectedItem2.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInCustomizing
        '
        'lvExpenseAllowances2
        '
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "lvExpenseAllowances2"
        Me.Size = New System.Drawing.Size(632, 392)
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public Property Connection() As SqlClient.SqlConnection
        Get
            Return hSqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            hSqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private Sub EnableDisable()
        btnEdit.Enabled = AllowEdit
    End Sub

    Public Function FillDataSet() As Integer Implements IListControl.FillDataSet
        ' Open connection
        Dim trans As SqlClient.SqlTransaction = Connection.BeginTransaction(IsolationLevel.ReadUncommitted)
        Power.Library.Library.ApplyTransactionToAllDataAdapters(trans, Me)
        Me.GridControl1.DataSource.Clear()
        FillDataSet = SqlDataAdapter.Fill(DsGTMS)
        GridView1.ExpandAllGroups()
        'Close connecton
        trans.Commit()
    End Function

    Private Sub GridView1_RowCountChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.RowCountChanged
        ExplorerForm.UpdateNumItems(NumItems)
        ExplorerForm.UpdateVisibleItems(VisibleItems)
    End Sub

    Public ReadOnly Property VisibleItems() As Integer Implements Power.Library.IListControl.VisibleItems
        Get
            Return GridView1.RowCount
        End Get
    End Property

    Public ReadOnly Property NumItems() As Integer Implements Power.Library.IListControl.NumItems
        Get
            Return DsGTMS.Tables(SqlDataAdapter.TableMappings(0).DataSetTable).Rows.Count
        End Get
    End Property

    Private hBRID As Integer
    Public Property BRID() As Integer
        Get
            Return hBRID
        End Get
        Set(ByVal Value As Integer)
            hBRID = Value
            SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = Value
        End Set
    End Property

    Public ReadOnly Property SelectedRow() As DataRow
        Get
            If Not GridView1.GetSelectedRows Is Nothing Then
                Return GridView1.GetDataRow(GridView1.GetSelectedRows(0))
            End If
        End Get
    End Property

    Public ReadOnly Property SelectedRowField(ByVal Field As String) As Object
        Get
            If Not SelectedRow Is Nothing Then
                Return SelectedRow.Item(Field)
            Else
                Return DBNull.Value
            End If
        End Get
    End Property

    Public Function SelectRow(ByVal BRID As Int32, ByVal EXID As Int64) As Boolean
        Dim i As Integer = 0
        Do Until False
            Try
                If GridView1.GetDataRow(GridView1.GetVisibleRowHandle(i))("BRID") = BRID And _
                        GridView1.GetDataRow(GridView1.GetVisibleRowHandle(i))("EXID") = EXID Then
                    GridView1.ClearSelection()
                    GridView1.SelectRow(GridView1.GetVisibleRowHandle(i))
                    Return True
                End If
                i += 1
            Catch ex As NullReferenceException
                Return False
            End Try
        Loop
        Return False
    End Function

#Region " New, Open and Delete "

    Private Sub GridControl1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridControl1.DoubleClick
        List_Edit()
    End Sub

    Public Sub List_New() Implements IListControl.List_New

    End Sub

    Private Sub btnEdit_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnEdit.ItemClick
        List_Edit()
    End Sub

    Public Sub List_Edit() Implements IListControl.List_Edit
        If Not SelectedRow Is Nothing Then
            Dim c As Cursor = ParentForm.Cursor
            ParentForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If ExpenseExists(SelectedRowField("BRID"), SelectedRowField("EXID")) Then
                frmExpenseAllowances2.Edit(SelectedRowField("BRID"), SelectedRowField("EXID"), SelectedRowField("EXName"), SelectedRowField("PMAllowancesPaymentTypeDisplay"))
                FillDataSet()
                ExplorerForm.RefreshDynamicCommands()
            Else
                Message.AlreadyDeleted("item", Message.ObjectAction.Edit)
                Me.GridControl1.DataSource.Clear()
                FillDataSet()
            End If
            ParentForm.Cursor = c
        End If
    End Sub

    Public Sub List_Cancel() Implements Power.Library.IListControl.List_Cancel

    End Sub

    Public Sub List_Uncancel() Implements Power.Library.IListControl.List_Uncancel

    End Sub

    Public Sub List_Delete() Implements IListControl.List_Delete

    End Sub

    Private Function ExpenseExists(ByVal BRID As Int32, ByVal EXID As Int64) As Boolean
        Dim cnn As SqlClient.SqlConnection = Connection
        Dim cmd As New SqlClient.SqlCommand("SELECT Count(EXID) FROM Expenses WHERE BRID = @BRID AND EXID = @EXID", cnn)
        cmd.CommandType = CommandType.Text
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@EXID", EXID)
        Return cmd.ExecuteScalar > 0
    End Function

    Public ReadOnly Property AllowDelete() As Boolean Implements Power.Library.IListControl.AllowDelete
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property AllowEdit() As Boolean Implements Power.Library.IListControl.AllowEdit
        Get
            Return True
        End Get
    End Property

    Public ReadOnly Property AllowNew() As Boolean Implements Power.Library.IListControl.AllowNew
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property AllowCancel() As Boolean Implements Power.Library.IListControl.AllowCancel
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property AllowUncancel() As Boolean Implements Power.Library.IListControl.AllowUncancel
        Get
            Return False
        End Get
    End Property

#End Region

#Region " Printing "

    Private Sub btnPrint_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrint.ItemClick
        Print(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub Print(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.Print
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.PrintDlg()
    End Sub

    Private Sub btnPrintPreview_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrintPreview.ItemClick
        PrintPreview(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub PrintPreview(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.PrintPreview
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(ExplorerForm.PrintingSystem)
        link.Component = GridControl1
        link.ShowPreview()
    End Sub

    Private Sub btnQuickPrint_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnQuickPrint.ItemClick
        QuickPrint(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub QuickPrint(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.QuickPrint
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.Print(PrintingSystem.PageSettings.PrinterName)
    End Sub

#End Region

    Private hExplorerForm As Form
    Public ReadOnly Property ExplorerForm() As frmExplorer
        Get
            Return hExplorerForm
        End Get
    End Property

    Private Sub btnHelp_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHelp.ItemClick
        ShowHelpTopic(ExplorerForm, "AmountsSpecifiedAnnually.html")
    End Sub
End Class
