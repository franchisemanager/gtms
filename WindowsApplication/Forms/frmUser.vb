Public Class frmUser
    Inherits DevExpress.XtraEditors.XtraForm

    Private DataRow As DataRow

    Public Shared Sub Add(ByVal FromHeadOfficeWindow As Boolean, ByVal USIsHeadOffice As Boolean, Optional ByVal BRID As Integer = Nothing)
        Dim gui As New frmUser(FromHeadOfficeWindow)

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            .FillPreliminaryData()

            .DataRow = .dataSet.Users.NewRow()
            .DataRow("BRID") = BRID
            .DataRow("USIsHeadOffice") = USIsHeadOffice
            .DataRow("USDisabled") = False
            .dataSet.Users.Rows.Add(.DataRow)

            .FillInitData()

            gui.ShowDialog()

            .SqlConnection.Close()
        End With
    End Sub

    Public Shared Sub Edit(ByVal FromHeadOfficeWindow As Boolean, ByVal USID As String)
        Dim gui As New frmUser(FromHeadOfficeWindow)

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            If DataAccess.spExecLockRequest("sp_GetUserLock", USID, .SqlConnection) Then

                .FillPreliminaryData()

                .SqlDataAdapter.SelectCommand.Parameters("@USID").Value = USID
                .SqlDataAdapter.Fill(.dataSet)
                .DataRow = .dataSet.Users(0)

                .FillData()

                gui.ShowDialog()

                DataAccess.spExecLockRequest("sp_ReleaseUserLock", USID, .SqlConnection)
            Else
                Message.CurrentlyAccessed("user")
            End If

            .SqlConnection.Close()
        End With
    End Sub

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal FromHeadOfficeWindow As Boolean)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        If Not FromHeadOfficeWindow Then
            chkUSIsHeadOffice.Enabled = False
            txtBRID.Enabled = False
            lblBranch.Enabled = False
            XtraTabControl1.TabPages.Remove(tpHeadOfficeRoles)
        End If
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtUSUsername As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Button1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents colRLRoleName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colURUserSelected As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents daRoles As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents dvBranchRoles As System.Data.DataView
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents txtUSPassword As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtUSPassword2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents chkUSDisabled As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtUSName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents chkUSIsHeadOffice As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daBranches As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents txtBRID As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents dvHeadOfficeRoles As System.Data.DataView
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcBranchRoles As DevExpress.XtraGrid.GridControl
    Friend WithEvents gcHeadOfficeRoles As DevExpress.XtraGrid.GridControl
    Friend WithEvents tcUserProperties As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tpBranchRoles As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tpHeadOfficeRoles As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents dataSet As WindowsApplication.dsUsers
    Friend WithEvents cmdRoles_init As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmUser))
        Me.txtUSUsername = New DevExpress.XtraEditors.TextEdit
        Me.dataSet = New WindowsApplication.dsUsers
        Me.txtUSPassword = New DevExpress.XtraEditors.TextEdit
        Me.txtUSPassword2 = New DevExpress.XtraEditors.TextEdit
        Me.Label31 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.gcBranchRoles = New DevExpress.XtraGrid.GridControl
        Me.dvBranchRoles = New System.Data.DataView
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colRLRoleName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colURUserSelected = New DevExpress.XtraGrid.Columns.GridColumn
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.Button1 = New DevExpress.XtraEditors.SimpleButton
        Me.chkUSDisabled = New DevExpress.XtraEditors.CheckEdit
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl
        Me.tcUserProperties = New DevExpress.XtraTab.XtraTabPage
        Me.lblBranch = New System.Windows.Forms.Label
        Me.txtBRID = New DevExpress.XtraEditors.LookUpEdit
        Me.chkUSIsHeadOffice = New DevExpress.XtraEditors.CheckEdit
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtUSName = New DevExpress.XtraEditors.TextEdit
        Me.tpBranchRoles = New DevExpress.XtraTab.XtraTabPage
        Me.tpHeadOfficeRoles = New DevExpress.XtraTab.XtraTabPage
        Me.gcHeadOfficeRoles = New DevExpress.XtraGrid.GridControl
        Me.dvHeadOfficeRoles = New System.Data.DataView
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.daRoles = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.daBranches = New System.Data.SqlClient.SqlDataAdapter
        Me.cmdRoles_init = New System.Data.SqlClient.SqlCommand
        CType(Me.txtUSUsername.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUSPassword.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUSPassword2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcBranchRoles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvBranchRoles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkUSDisabled.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.tcUserProperties.SuspendLayout()
        CType(Me.txtBRID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkUSIsHeadOffice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUSName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpBranchRoles.SuspendLayout()
        Me.tpHeadOfficeRoles.SuspendLayout()
        CType(Me.gcHeadOfficeRoles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvHeadOfficeRoles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtUSUsername
        '
        Me.txtUSUsername.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtUSUsername.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Users.USUsername"))
        Me.txtUSUsername.EditValue = ""
        Me.txtUSUsername.Location = New System.Drawing.Point(104, 88)
        Me.txtUSUsername.Name = "txtUSUsername"
        Me.txtUSUsername.Size = New System.Drawing.Size(285, 20)
        Me.txtUSUsername.TabIndex = 4
        '
        'dataSet
        '
        Me.dataSet.DataSetName = "dsUsers"
        Me.dataSet.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'txtUSPassword
        '
        Me.txtUSPassword.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtUSPassword.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Users.USPassword"))
        Me.txtUSPassword.EditValue = ""
        Me.txtUSPassword.Location = New System.Drawing.Point(104, 128)
        Me.txtUSPassword.Name = "txtUSPassword"
        '
        'txtUSPassword.Properties
        '
        Me.txtUSPassword.Properties.PasswordChar = Microsoft.VisualBasic.ChrW(42)
        Me.txtUSPassword.Size = New System.Drawing.Size(285, 20)
        Me.txtUSPassword.TabIndex = 5
        '
        'txtUSPassword2
        '
        Me.txtUSPassword2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtUSPassword2.EditValue = ""
        Me.txtUSPassword2.Location = New System.Drawing.Point(104, 160)
        Me.txtUSPassword2.Name = "txtUSPassword2"
        '
        'txtUSPassword2.Properties
        '
        Me.txtUSPassword2.Properties.PasswordChar = Microsoft.VisualBasic.ChrW(42)
        Me.txtUSPassword2.Size = New System.Drawing.Size(285, 20)
        Me.txtUSPassword2.TabIndex = 6
        '
        'Label31
        '
        Me.Label31.Location = New System.Drawing.Point(8, 88)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(88, 21)
        Me.Label31.TabIndex = 3
        Me.Label31.Text = "Username:"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 128)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(88, 21)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Password:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(8, 160)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(96, 21)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Retype password:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gcBranchRoles
        '
        Me.gcBranchRoles.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gcBranchRoles.DataSource = Me.dvBranchRoles
        '
        'gcBranchRoles.EmbeddedNavigator
        '
        Me.gcBranchRoles.EmbeddedNavigator.Name = ""
        Me.gcBranchRoles.Location = New System.Drawing.Point(8, 16)
        Me.gcBranchRoles.MainView = Me.GridView1
        Me.gcBranchRoles.Name = "gcBranchRoles"
        Me.gcBranchRoles.Size = New System.Drawing.Size(381, 412)
        Me.gcBranchRoles.TabIndex = 0
        Me.gcBranchRoles.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'dvBranchRoles
        '
        Me.dvBranchRoles.RowFilter = "RLIsHeadOffice = 0"
        Me.dvBranchRoles.Table = Me.dataSet.VUsers_Roles
        '
        'GridView1
        '
        Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
        Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
        Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
        Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
        Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
        Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colRLRoleName, Me.colURUserSelected})
        Me.GridView1.GridControl = Me.gcBranchRoles
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsView.AutoCalcPreviewLineCount = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.OptionsView.ShowPreview = True
        Me.GridView1.PreviewFieldName = "RLRoleDesc"
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colRLRoleName, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colRLRoleName
        '
        Me.colRLRoleName.Caption = "Role"
        Me.colRLRoleName.FieldName = "RLRoleName"
        Me.colRLRoleName.Name = "colRLRoleName"
        Me.colRLRoleName.OptionsColumn.AllowEdit = False
        Me.colRLRoleName.OptionsColumn.AllowFocus = False
        Me.colRLRoleName.Visible = True
        Me.colRLRoleName.VisibleIndex = 1
        Me.colRLRoleName.Width = 433
        '
        'colURUserSelected
        '
        Me.colURUserSelected.FieldName = "URUserSelected"
        Me.colURUserSelected.Name = "colURUserSelected"
        Me.colURUserSelected.Visible = True
        Me.colURUserSelected.VisibleIndex = 0
        Me.colURUserSelected.Width = 33
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(344, 488)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(264, 488)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "OK"
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.Location = New System.Drawing.Point(8, 488)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(72, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Help"
        '
        'chkUSDisabled
        '
        Me.chkUSDisabled.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Users.USDisabled"))
        Me.chkUSDisabled.Location = New System.Drawing.Point(8, 416)
        Me.chkUSDisabled.Name = "chkUSDisabled"
        '
        'chkUSDisabled.Properties
        '
        Me.chkUSDisabled.Properties.Caption = "Disable this user account"
        Me.chkUSDisabled.Size = New System.Drawing.Size(144, 19)
        Me.chkUSDisabled.TabIndex = 9
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.XtraTabControl1.Location = New System.Drawing.Point(8, 8)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.tcUserProperties
        Me.XtraTabControl1.Size = New System.Drawing.Size(408, 472)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tcUserProperties, Me.tpBranchRoles, Me.tpHeadOfficeRoles})
        Me.XtraTabControl1.Text = "XtraTabControl1"
        '
        'tcUserProperties
        '
        Me.tcUserProperties.Controls.Add(Me.lblBranch)
        Me.tcUserProperties.Controls.Add(Me.txtBRID)
        Me.tcUserProperties.Controls.Add(Me.chkUSIsHeadOffice)
        Me.tcUserProperties.Controls.Add(Me.Label4)
        Me.tcUserProperties.Controls.Add(Me.Label3)
        Me.tcUserProperties.Controls.Add(Me.txtUSName)
        Me.tcUserProperties.Controls.Add(Me.txtUSPassword)
        Me.tcUserProperties.Controls.Add(Me.txtUSPassword2)
        Me.tcUserProperties.Controls.Add(Me.Label31)
        Me.tcUserProperties.Controls.Add(Me.chkUSDisabled)
        Me.tcUserProperties.Controls.Add(Me.Label2)
        Me.tcUserProperties.Controls.Add(Me.Label1)
        Me.tcUserProperties.Controls.Add(Me.txtUSUsername)
        Me.tcUserProperties.Name = "tcUserProperties"
        Me.tcUserProperties.Size = New System.Drawing.Size(399, 442)
        Me.tcUserProperties.Text = "User Properties"
        '
        'lblBranch
        '
        Me.lblBranch.Location = New System.Drawing.Point(8, 248)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(96, 21)
        Me.lblBranch.TabIndex = 10
        Me.lblBranch.Text = "Branch:"
        Me.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBRID
        '
        Me.txtBRID.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Users.BRID"))
        Me.txtBRID.Location = New System.Drawing.Point(104, 248)
        Me.txtBRID.Name = "txtBRID"
        '
        'txtBRID.Properties
        '
        Me.txtBRID.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtBRID.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("BRShortName", "", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.txtBRID.Properties.DataSource = Me.dataSet.Branches
        Me.txtBRID.Properties.DisplayMember = "BRShortName"
        Me.txtBRID.Properties.NullText = "--- No branch selected ---"
        Me.txtBRID.Properties.ShowFooter = False
        Me.txtBRID.Properties.ShowHeader = False
        Me.txtBRID.Properties.ShowLines = False
        Me.txtBRID.Properties.ValueMember = "BRID"
        Me.txtBRID.Size = New System.Drawing.Size(288, 20)
        Me.txtBRID.TabIndex = 8
        '
        'chkUSIsHeadOffice
        '
        Me.chkUSIsHeadOffice.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Users.USIsHeadOffice"))
        Me.chkUSIsHeadOffice.Location = New System.Drawing.Point(8, 216)
        Me.chkUSIsHeadOffice.Name = "chkUSIsHeadOffice"
        '
        'chkUSIsHeadOffice.Properties
        '
        Me.chkUSIsHeadOffice.Properties.Caption = "This is a head office user"
        Me.chkUSIsHeadOffice.Size = New System.Drawing.Size(152, 19)
        Me.chkUSIsHeadOffice.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.Location = New System.Drawing.Point(8, 56)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(381, 32)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "The Username is used in logging into the program.  This must be unique to this us" & _
        "er and cannot be repeated across the system."
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(8, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(96, 21)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "User's Name:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtUSName
        '
        Me.txtUSName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtUSName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Users.USName"))
        Me.txtUSName.EditValue = ""
        Me.txtUSName.Location = New System.Drawing.Point(104, 16)
        Me.txtUSName.Name = "txtUSName"
        Me.txtUSName.Size = New System.Drawing.Size(285, 20)
        Me.txtUSName.TabIndex = 1
        '
        'tpBranchRoles
        '
        Me.tpBranchRoles.Controls.Add(Me.gcBranchRoles)
        Me.tpBranchRoles.Name = "tpBranchRoles"
        Me.tpBranchRoles.Size = New System.Drawing.Size(399, 442)
        Me.tpBranchRoles.Text = "User Permissions"
        '
        'tpHeadOfficeRoles
        '
        Me.tpHeadOfficeRoles.Controls.Add(Me.gcHeadOfficeRoles)
        Me.tpHeadOfficeRoles.Name = "tpHeadOfficeRoles"
        Me.tpHeadOfficeRoles.Size = New System.Drawing.Size(399, 442)
        Me.tpHeadOfficeRoles.Text = "User Permissions at Head Office"
        '
        'gcHeadOfficeRoles
        '
        Me.gcHeadOfficeRoles.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gcHeadOfficeRoles.DataSource = Me.dvHeadOfficeRoles
        '
        'gcHeadOfficeRoles.EmbeddedNavigator
        '
        Me.gcHeadOfficeRoles.EmbeddedNavigator.Name = ""
        Me.gcHeadOfficeRoles.Location = New System.Drawing.Point(8, 16)
        Me.gcHeadOfficeRoles.MainView = Me.GridView2
        Me.gcHeadOfficeRoles.Name = "gcHeadOfficeRoles"
        Me.gcHeadOfficeRoles.Size = New System.Drawing.Size(381, 412)
        Me.gcHeadOfficeRoles.TabIndex = 0
        Me.gcHeadOfficeRoles.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'dvHeadOfficeRoles
        '
        Me.dvHeadOfficeRoles.RowFilter = "RLIsHeadOffice = 1"
        Me.dvHeadOfficeRoles.Table = Me.dataSet.VUsers_Roles
        '
        'GridView2
        '
        Me.GridView2.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView2.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView2.Appearance.FocusedRow.Options.UseBackColor = True
        Me.GridView2.Appearance.FocusedRow.Options.UseForeColor = True
        Me.GridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView2.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView2.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView2.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.GridView2.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.GridView2.Appearance.HorzLine.Options.UseBackColor = True
        Me.GridView2.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView2.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView2.Appearance.SelectedRow.Options.UseBackColor = True
        Me.GridView2.Appearance.SelectedRow.Options.UseForeColor = True
        Me.GridView2.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.GridView2.Appearance.VertLine.Options.UseBackColor = True
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.GridView2.GridControl = Me.gcHeadOfficeRoles
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsCustomization.AllowFilter = False
        Me.GridView2.OptionsView.AutoCalcPreviewLineCount = True
        Me.GridView2.OptionsView.ShowGroupPanel = False
        Me.GridView2.OptionsView.ShowPreview = True
        Me.GridView2.PreviewFieldName = "RLRoleDesc"
        Me.GridView2.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn1, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Role"
        Me.GridColumn1.FieldName = "RLRoleName"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.AllowEdit = False
        Me.GridColumn1.OptionsColumn.AllowFocus = False
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 1
        Me.GridColumn1.Width = 433
        '
        'GridColumn2
        '
        Me.GridColumn2.FieldName = "URUserSelected"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 0
        Me.GridColumn2.Width = 33
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;integrated security=SSPI;data source=""SERVER" & _
        "\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'daRoles
        '
        Me.daRoles.DeleteCommand = Me.SqlDeleteCommand1
        Me.daRoles.InsertCommand = Me.SqlInsertCommand1
        Me.daRoles.SelectCommand = Me.SqlSelectCommand1
        Me.daRoles.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VUsers_Roles", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RLRole", "RLRole"), New System.Data.Common.DataColumnMapping("URUserSelected", "URUserSelected"), New System.Data.Common.DataColumnMapping("USID", "USID")})})
        Me.daRoles.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Users_Roles WHERE (RLRole = @Original_RLRole) AND (USID = @Original_U" & _
        "SID) AND (URUserSelected = @Original_URUserSelected OR @Original_URUserSelected " & _
        "IS NULL AND URUserSelected IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RLRole", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RLRole", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_URUserSelected", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "URUserSelected", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Users_Roles (RLRole, URUserSelected, USID) VALUES (@RLRole, @URUserSe" & _
        "lected, @USID); SELECT RLRole, URUserSelected, USID, RLRoleName, RLRoleDesc, RLI" & _
        "sHeadOffice FROM VUsers_Roles WHERE (RLRole = @RLRole) AND (USID = @USID)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RLRole", System.Data.SqlDbType.VarChar, 50, "RLRole"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@URUserSelected", System.Data.SqlDbType.Bit, 1, "URUserSelected"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USID", System.Data.SqlDbType.Int, 4, "USID"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT RLRole, URUserSelected, USID, RLRoleName, RLRoleDesc, RLIsHeadOffice FROM " & _
        "VUsers_Roles WHERE (USID = @USID)"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USID", System.Data.SqlDbType.Int, 4, "USID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Users_Roles SET RLRole = @RLRole, URUserSelected = @URUserSelected, USID =" & _
        " @USID WHERE (RLRole = @Original_RLRole) AND (USID = @Original_USID) AND (URUser" & _
        "Selected = @Original_URUserSelected OR @Original_URUserSelected IS NULL AND URUs" & _
        "erSelected IS NULL); SELECT RLRole, URUserSelected, USID, RLRoleName, RLRoleDesc" & _
        ", RLIsHeadOffice FROM VUsers_Roles WHERE (RLRole = @RLRole) AND (USID = @USID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RLRole", System.Data.SqlDbType.VarChar, 50, "RLRole"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@URUserSelected", System.Data.SqlDbType.Bit, 1, "URUserSelected"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USID", System.Data.SqlDbType.Int, 4, "USID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_RLRole", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "RLRole", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_URUserSelected", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "URUserSelected", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand2
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand2
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand2
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Users", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("USUsername", "USUsername"), New System.Data.Common.DataColumnMapping("USPassword", "USPassword"), New System.Data.Common.DataColumnMapping("USIsHeadOffice", "USIsHeadOffice"), New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("USDisabled", "USDisabled"), New System.Data.Common.DataColumnMapping("USName", "USName"), New System.Data.Common.DataColumnMapping("USID", "USID")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM Users WHERE (USID = @Original_USID) AND (BRID = @Original_BRID OR @Or" & _
        "iginal_BRID IS NULL AND BRID IS NULL) AND (USDisabled = @Original_USDisabled) AN" & _
        "D (USIsHeadOffice = @Original_USIsHeadOffice) AND (USName = @Original_USName OR " & _
        "@Original_USName IS NULL AND USName IS NULL) AND (USPassword = @Original_USPassw" & _
        "ord OR @Original_USPassword IS NULL AND USPassword IS NULL) AND (USUsername = @O" & _
        "riginal_USUsername OR @Original_USUsername IS NULL AND USUsername IS NULL)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USDisabled", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USDisabled", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USIsHeadOffice", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USIsHeadOffice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USPassword", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USPassword", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USUsername", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USUsername", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO Users(USUsername, USPassword, USIsHeadOffice, BRID, USDisabled, USNam" & _
        "e) VALUES (@USUsername, @USPassword, @USIsHeadOffice, @BRID, @USDisabled, @USNam" & _
        "e); SELECT USUsername, USPassword, USIsHeadOffice, BRID, USDisabled, USName, USI" & _
        "D FROM Users WHERE (USID = @@IDENTITY)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USUsername", System.Data.SqlDbType.VarChar, 50, "USUsername"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USPassword", System.Data.SqlDbType.VarChar, 50, "USPassword"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USIsHeadOffice", System.Data.SqlDbType.Bit, 1, "USIsHeadOffice"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USDisabled", System.Data.SqlDbType.Bit, 1, "USDisabled"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USName", System.Data.SqlDbType.VarChar, 50, "USName"))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT USUsername, USPassword, USIsHeadOffice, BRID, USDisabled, USName, USID FRO" & _
        "M Users WHERE (USID = @USID)"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection
        Me.SqlSelectCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USID", System.Data.SqlDbType.Int, 4, "USID"))
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE Users SET USUsername = @USUsername, USPassword = @USPassword, USIsHeadOffi" & _
        "ce = @USIsHeadOffice, BRID = @BRID, USDisabled = @USDisabled, USName = @USName W" & _
        "HERE (USID = @Original_USID) AND (BRID = @Original_BRID OR @Original_BRID IS NUL" & _
        "L AND BRID IS NULL) AND (USDisabled = @Original_USDisabled) AND (USIsHeadOffice " & _
        "= @Original_USIsHeadOffice) AND (USName = @Original_USName OR @Original_USName I" & _
        "S NULL AND USName IS NULL) AND (USPassword = @Original_USPassword OR @Original_U" & _
        "SPassword IS NULL AND USPassword IS NULL) AND (USUsername = @Original_USUsername" & _
        " OR @Original_USUsername IS NULL AND USUsername IS NULL); SELECT USUsername, USP" & _
        "assword, USIsHeadOffice, BRID, USDisabled, USName, USID FROM Users WHERE (USID =" & _
        " @USID)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USUsername", System.Data.SqlDbType.VarChar, 50, "USUsername"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USPassword", System.Data.SqlDbType.VarChar, 50, "USPassword"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USIsHeadOffice", System.Data.SqlDbType.Bit, 1, "USIsHeadOffice"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USDisabled", System.Data.SqlDbType.Bit, 1, "USDisabled"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USName", System.Data.SqlDbType.VarChar, 50, "USName"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USDisabled", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USDisabled", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USIsHeadOffice", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USIsHeadOffice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USPassword", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USPassword", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_USUsername", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "USUsername", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USID", System.Data.SqlDbType.Int, 4, "USID"))
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT BRID, NextID, BRStreetAddress01, BRStreetAddress02, BRSuburb, BRState, BRP" & _
        "ostCode, BRPhoneNumber, BRFaxNumber, BRBusinessName, BRMobileNumber, BRShortName" & _
        ", BREmail, BRGST, BRSuperannuation, BRIncludeInReporting FROM Branches"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO Branches(NextID, BRStreetAddress01, BRStreetAddress02, BRSuburb, BRSt" & _
        "ate, BRPostCode, BRPhoneNumber, BRFaxNumber, BRBusinessName, BRMobileNumber, BRS" & _
        "hortName, BREmail, BRGST, BRSuperannuation, BRIncludeInReporting) VALUES (@NextI" & _
        "D, @BRStreetAddress01, @BRStreetAddress02, @BRSuburb, @BRState, @BRPostCode, @BR" & _
        "PhoneNumber, @BRFaxNumber, @BRBusinessName, @BRMobileNumber, @BRShortName, @BREm" & _
        "ail, @BRGST, @BRSuperannuation, @BRIncludeInReporting); SELECT BRID, NextID, BRS" & _
        "treetAddress01, BRStreetAddress02, BRSuburb, BRState, BRPostCode, BRPhoneNumber," & _
        " BRFaxNumber, BRBusinessName, BRMobileNumber, BRShortName, BREmail, BRGST, BRSup" & _
        "erannuation, BRIncludeInReporting FROM Branches WHERE (BRID = @@IDENTITY)"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NextID", System.Data.SqlDbType.BigInt, 8, "NextID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRStreetAddress01", System.Data.SqlDbType.VarChar, 100, "BRStreetAddress01"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRStreetAddress02", System.Data.SqlDbType.VarChar, 100, "BRStreetAddress02"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRSuburb", System.Data.SqlDbType.VarChar, 50, "BRSuburb"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRState", System.Data.SqlDbType.VarChar, 3, "BRState"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRPostCode", System.Data.SqlDbType.VarChar, 20, "BRPostCode"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRPhoneNumber", System.Data.SqlDbType.VarChar, 20, "BRPhoneNumber"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRFaxNumber", System.Data.SqlDbType.VarChar, 20, "BRFaxNumber"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRBusinessName", System.Data.SqlDbType.VarChar, 100, "BRBusinessName"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRMobileNumber", System.Data.SqlDbType.VarChar, 20, "BRMobileNumber"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRShortName", System.Data.SqlDbType.VarChar, 50, "BRShortName"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BREmail", System.Data.SqlDbType.VarChar, 50, "BREmail"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRGST", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRGST", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRSuperannuation", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRSuperannuation", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRIncludeInReporting", System.Data.SqlDbType.Bit, 1, "BRIncludeInReporting"))
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE Branches SET NextID = @NextID, BRStreetAddress01 = @BRStreetAddress01, BRS" & _
        "treetAddress02 = @BRStreetAddress02, BRSuburb = @BRSuburb, BRState = @BRState, B" & _
        "RPostCode = @BRPostCode, BRPhoneNumber = @BRPhoneNumber, BRFaxNumber = @BRFaxNum" & _
        "ber, BRBusinessName = @BRBusinessName, BRMobileNumber = @BRMobileNumber, BRShort" & _
        "Name = @BRShortName, BREmail = @BREmail, BRGST = @BRGST, BRSuperannuation = @BRS" & _
        "uperannuation, BRIncludeInReporting = @BRIncludeInReporting WHERE (BRID = @Origi" & _
        "nal_BRID) AND (BRBusinessName = @Original_BRBusinessName OR @Original_BRBusiness" & _
        "Name IS NULL AND BRBusinessName IS NULL) AND (BREmail = @Original_BREmail OR @Or" & _
        "iginal_BREmail IS NULL AND BREmail IS NULL) AND (BRFaxNumber = @Original_BRFaxNu" & _
        "mber OR @Original_BRFaxNumber IS NULL AND BRFaxNumber IS NULL) AND (BRGST = @Ori" & _
        "ginal_BRGST OR @Original_BRGST IS NULL AND BRGST IS NULL) AND (BRIncludeInReport" & _
        "ing = @Original_BRIncludeInReporting OR @Original_BRIncludeInReporting IS NULL A" & _
        "ND BRIncludeInReporting IS NULL) AND (BRMobileNumber = @Original_BRMobileNumber " & _
        "OR @Original_BRMobileNumber IS NULL AND BRMobileNumber IS NULL) AND (BRPhoneNumb" & _
        "er = @Original_BRPhoneNumber OR @Original_BRPhoneNumber IS NULL AND BRPhoneNumbe" & _
        "r IS NULL) AND (BRPostCode = @Original_BRPostCode OR @Original_BRPostCode IS NUL" & _
        "L AND BRPostCode IS NULL) AND (BRShortName = @Original_BRShortName OR @Original_" & _
        "BRShortName IS NULL AND BRShortName IS NULL) AND (BRState = @Original_BRState OR" & _
        " @Original_BRState IS NULL AND BRState IS NULL) AND (BRStreetAddress01 = @Origin" & _
        "al_BRStreetAddress01 OR @Original_BRStreetAddress01 IS NULL AND BRStreetAddress0" & _
        "1 IS NULL) AND (BRStreetAddress02 = @Original_BRStreetAddress02 OR @Original_BRS" & _
        "treetAddress02 IS NULL AND BRStreetAddress02 IS NULL) AND (BRSuburb = @Original_" & _
        "BRSuburb OR @Original_BRSuburb IS NULL AND BRSuburb IS NULL) AND (BRSuperannuati" & _
        "on = @Original_BRSuperannuation OR @Original_BRSuperannuation IS NULL AND BRSupe" & _
        "rannuation IS NULL) AND (NextID = @Original_NextID); SELECT BRID, NextID, BRStre" & _
        "etAddress01, BRStreetAddress02, BRSuburb, BRState, BRPostCode, BRPhoneNumber, BR" & _
        "FaxNumber, BRBusinessName, BRMobileNumber, BRShortName, BREmail, BRGST, BRSupera" & _
        "nnuation, BRIncludeInReporting FROM Branches WHERE (BRID = @BRID)"
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NextID", System.Data.SqlDbType.BigInt, 8, "NextID"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRStreetAddress01", System.Data.SqlDbType.VarChar, 100, "BRStreetAddress01"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRStreetAddress02", System.Data.SqlDbType.VarChar, 100, "BRStreetAddress02"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRSuburb", System.Data.SqlDbType.VarChar, 50, "BRSuburb"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRState", System.Data.SqlDbType.VarChar, 3, "BRState"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRPostCode", System.Data.SqlDbType.VarChar, 20, "BRPostCode"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRPhoneNumber", System.Data.SqlDbType.VarChar, 20, "BRPhoneNumber"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRFaxNumber", System.Data.SqlDbType.VarChar, 20, "BRFaxNumber"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRBusinessName", System.Data.SqlDbType.VarChar, 100, "BRBusinessName"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRMobileNumber", System.Data.SqlDbType.VarChar, 20, "BRMobileNumber"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRShortName", System.Data.SqlDbType.VarChar, 50, "BRShortName"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BREmail", System.Data.SqlDbType.VarChar, 50, "BREmail"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRGST", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRGST", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRSuperannuation", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRSuperannuation", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRIncludeInReporting", System.Data.SqlDbType.Bit, 1, "BRIncludeInReporting"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRBusinessName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRBusinessName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BREmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BREmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRGST", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRGST", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRIncludeInReporting", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRIncludeInReporting", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRShortName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRShortName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRSuperannuation", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRSuperannuation", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NextID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NextID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM Branches WHERE (BRID = @Original_BRID) AND (BRBusinessName = @Origina" & _
        "l_BRBusinessName OR @Original_BRBusinessName IS NULL AND BRBusinessName IS NULL)" & _
        " AND (BREmail = @Original_BREmail OR @Original_BREmail IS NULL AND BREmail IS NU" & _
        "LL) AND (BRFaxNumber = @Original_BRFaxNumber OR @Original_BRFaxNumber IS NULL AN" & _
        "D BRFaxNumber IS NULL) AND (BRGST = @Original_BRGST OR @Original_BRGST IS NULL A" & _
        "ND BRGST IS NULL) AND (BRIncludeInReporting = @Original_BRIncludeInReporting OR " & _
        "@Original_BRIncludeInReporting IS NULL AND BRIncludeInReporting IS NULL) AND (BR" & _
        "MobileNumber = @Original_BRMobileNumber OR @Original_BRMobileNumber IS NULL AND " & _
        "BRMobileNumber IS NULL) AND (BRPhoneNumber = @Original_BRPhoneNumber OR @Origina" & _
        "l_BRPhoneNumber IS NULL AND BRPhoneNumber IS NULL) AND (BRPostCode = @Original_B" & _
        "RPostCode OR @Original_BRPostCode IS NULL AND BRPostCode IS NULL) AND (BRShortNa" & _
        "me = @Original_BRShortName OR @Original_BRShortName IS NULL AND BRShortName IS N" & _
        "ULL) AND (BRState = @Original_BRState OR @Original_BRState IS NULL AND BRState I" & _
        "S NULL) AND (BRStreetAddress01 = @Original_BRStreetAddress01 OR @Original_BRStre" & _
        "etAddress01 IS NULL AND BRStreetAddress01 IS NULL) AND (BRStreetAddress02 = @Ori" & _
        "ginal_BRStreetAddress02 OR @Original_BRStreetAddress02 IS NULL AND BRStreetAddre" & _
        "ss02 IS NULL) AND (BRSuburb = @Original_BRSuburb OR @Original_BRSuburb IS NULL A" & _
        "ND BRSuburb IS NULL) AND (BRSuperannuation = @Original_BRSuperannuation OR @Orig" & _
        "inal_BRSuperannuation IS NULL AND BRSuperannuation IS NULL) AND (NextID = @Origi" & _
        "nal_NextID)"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRBusinessName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRBusinessName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BREmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BREmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRGST", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRGST", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRIncludeInReporting", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRIncludeInReporting", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRShortName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRShortName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRSuperannuation", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(10, Byte), "BRSuperannuation", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NextID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NextID", System.Data.DataRowVersion.Original, Nothing))
        '
        'daBranches
        '
        Me.daBranches.DeleteCommand = Me.SqlDeleteCommand3
        Me.daBranches.InsertCommand = Me.SqlInsertCommand3
        Me.daBranches.SelectCommand = Me.SqlSelectCommand3
        Me.daBranches.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Branches", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("NextID", "NextID"), New System.Data.Common.DataColumnMapping("BRStreetAddress01", "BRStreetAddress01"), New System.Data.Common.DataColumnMapping("BRStreetAddress02", "BRStreetAddress02"), New System.Data.Common.DataColumnMapping("BRSuburb", "BRSuburb"), New System.Data.Common.DataColumnMapping("BRState", "BRState"), New System.Data.Common.DataColumnMapping("BRPostCode", "BRPostCode"), New System.Data.Common.DataColumnMapping("BRPhoneNumber", "BRPhoneNumber"), New System.Data.Common.DataColumnMapping("BRFaxNumber", "BRFaxNumber"), New System.Data.Common.DataColumnMapping("BRBusinessName", "BRBusinessName"), New System.Data.Common.DataColumnMapping("BRMobileNumber", "BRMobileNumber"), New System.Data.Common.DataColumnMapping("BRShortName", "BRShortName"), New System.Data.Common.DataColumnMapping("BREmail", "BREmail"), New System.Data.Common.DataColumnMapping("BRGST", "BRGST"), New System.Data.Common.DataColumnMapping("BRSuperannuation", "BRSuperannuation"), New System.Data.Common.DataColumnMapping("BRIncludeInReporting", "BRIncludeInReporting")})})
        Me.daBranches.UpdateCommand = Me.SqlUpdateCommand3
        '
        'cmdRoles_init
        '
        Me.cmdRoles_init.CommandText = "SELECT RLRole, 0 AS URUserSelected, @USID AS USID, RLRoleName, RLRoleDesc, RLIsHe" & _
        "adOffice FROM Roles"
        Me.cmdRoles_init.Connection = Me.SqlConnection
        Me.cmdRoles_init.Parameters.Add(New System.Data.SqlClient.SqlParameter("@USID", System.Data.SqlDbType.Variant))
        '
        'frmUser
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(426, 520)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUser"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "User"
        CType(Me.txtUSUsername.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUSPassword.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUSPassword2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcBranchRoles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvBranchRoles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkUSDisabled.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.tcUserProperties.ResumeLayout(False)
        CType(Me.txtBRID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkUSIsHeadOffice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUSName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpBranchRoles.ResumeLayout(False)
        Me.tpHeadOfficeRoles.ResumeLayout(False)
        CType(Me.gcHeadOfficeRoles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvHeadOfficeRoles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnOK.Enabled = Not HasRole("branch_read_only") ' Disabled to allow head office users to administer security, but not change data
    End Sub

    ' This data must be loaded BEFORE the DataRow is loaded (eg lookups etc)
    Private Sub FillPreliminaryData()
        daBranches.Fill(dataSet)
    End Sub

    ' This data must be loaded AFTER the DataRow is loaded (eg record related data)
    Private Sub FillInitData()
        ' --- LEAD SUBSOURCES ---
        Dim reader As SqlClient.SqlDataReader
        cmdRoles_init.Parameters("@USID").Value = DataRow("USID")
        reader = cmdRoles_init.ExecuteReader
        If reader.HasRows Then
            While reader.Read
                dataSet.VUsers_Roles.AddVUsers_RolesRow(DataRow, reader("RLRole"), reader("URUserSelected"), _
                reader("RLRoleName"), reader("RLRoleDesc"), reader("RLIsHeadOffice"))
            End While
        End If
        If Not reader.IsClosed Then reader.Close()
    End Sub

    ' This data must be loaded AFTER the DataRow is loaded (eg record related data)
    Private Sub FillData()
        txtUSPassword2.EditValue = DataRow("USPassword")
        ' --- MATERIAL COSTS ---
        daRoles.SelectCommand.Parameters("@USID").Value = DataRow("USID")
        daRoles.Fill(dataSet)
    End Sub

    Private Function UpdateData() As Boolean
        Try
            SqlDataAdapter.Update(dataSet)
        Catch ex As SqlClient.SqlException
            If ex.Number = 2627 Then ' 2627 = UNIQUE KEY VIOLATION
                Message.ShowMessage("The username you entered already appears at this branch.  Please enter a new username.", MessageBoxIcon.Exclamation)
                Return False
            Else
                Throw ex
            End If
        End Try
        Try
            daRoles.Update(dataSet)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return True
    End Function

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        ' EndEdit() to end editing the dataset record so that we can update
        If CBool(DataRow("USIsHeadOffice")) And Not (DataRow("BRID") Is DBNull.Value) Then
            DataRow("BRID") = DBNull.Value
        End If
        DataRow.EndEdit()
        If ValidateForm() Then
            If Not UpdateData() Then Exit Sub
            OK = True
            Me.Close()
        End If
    End Sub

    Private Function ValidateForm() As Boolean
        If DataRow("USUsername") Is DBNull.Value Then
            Message.ShowMessage("You must enter a Username.", MessageBoxIcon.Exclamation)
            Return False
        End If
        If DataRow("USName") Is DBNull.Value Then
            Message.ShowMessage("You must enter a name for the user.", MessageBoxIcon.Exclamation)
            Return False
        End If
        If DataRow("USPassword") Is DBNull.Value Then
            Message.ShowMessage("You must enter a password for the user.", MessageBoxIcon.Exclamation)
            Return False
        End If
        If txtUSPassword2.EditValue Is DBNull.Value Then
            Message.ShowMessage("You must re-enter a password for the user.", MessageBoxIcon.Exclamation)
            Return False
        End If
        If Not txtUSPassword2.EditValue = txtUSPassword.EditValue Then
            Message.ShowMessage("The passwords do not match. Please re-enter your password.", MessageBoxIcon.Exclamation)
            txtUSPassword.EditValue = DBNull.Value
            txtUSPassword2.EditValue = DBNull.Value
            Return False
        End If
        If Len(txtUSPassword.EditValue) < 4 Then
            Message.ShowMessage("The password must be at least 4 letters or digits.", MessageBoxIcon.Exclamation)
            txtUSPassword.EditValue = DBNull.Value
            txtUSPassword2.EditValue = DBNull.Value
            Return False
        End If
        If txtBRID.Text = "" Then
            txtBRID.EditValue = DBNull.Value
        End If
        If Not CBool(chkUSIsHeadOffice.EditValue) And txtBRID.EditValue Is DBNull.Value Then
            Message.ShowMessage("You must either set this as a head office user or select a branch from the drop down list.", MessageBoxIcon.Exclamation)
            Return False
        End If
        Return True
    End Function
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private HasChanges As Boolean = False
    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            DataRow.EndEdit()
            If dataSet.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If response = MsgBoxResult.No Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub Text_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) _
    Handles txtUSName.ParseEditValue, txtUSUsername.ParseEditValue, txtUSPassword.ParseEditValue, txtUSPassword2.ParseEditValue
        Format.Text_ParseEditValue(sender, e)
    End Sub

    Private Sub chkUSIsHeadOffice_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUSIsHeadOffice.CheckedChanged
        txtBRID.Enabled = Not CBool(chkUSIsHeadOffice.EditValue)
        lblBranch.Enabled = Not CBool(chkUSIsHeadOffice.EditValue)
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ShowHelp(Me)
    End Sub

End Class

