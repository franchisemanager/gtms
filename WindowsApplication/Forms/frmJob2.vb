Public Class frmJob2
    Inherits DevExpress.XtraEditors.XtraForm

    Private DataRow As DataRow
    Private ShowHours As Boolean

    Public Shared Sub Edit(ByVal BRID As Int32, ByRef JBID As Int64)
        Dim gui As New frmJob2

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            If DataAccess.spExecLockRequest("sp_GetJobLock", BRID, JBID, .SqlConnection) Then

                .LoadTreeViews(BRID)
                .FillPreliminaryData(BRID)
                .CustomiseScreen()

                .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = BRID
                .SqlDataAdapter.SelectCommand.Parameters("@JBID").Value = JBID
                .SqlDataAdapter.Fill(.dataSet)
                .DataRow = .dataSet.Jobs(0)

                .FillData()

                gui.ShowDialog()

                gui.ReleaseOrderLocks()
                DataAccess.spExecLockRequest("sp_ReleaseJobLock", BRID, JBID, .SqlConnection)
            Else
                Message.CurrentlyAccessed("job")
            End If

            .SqlConnection.Close()
        End With
    End Sub

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents ListBox As Power.Forms.ListBox
    Friend WithEvents pnlMain As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents GroupLine5 As Power.Forms.GroupLine
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents GroupLine2 As Power.Forms.GroupLine
    Friend WithEvents GroupLine4 As Power.Forms.GroupLine
    Friend WithEvents GroupLine3 As Power.Forms.GroupLine
    Friend WithEvents GroupLine6 As Power.Forms.GroupLine
    Friend WithEvents GroupLine7 As Power.Forms.GroupLine
    Friend WithEvents GroupLine8 As Power.Forms.GroupLine
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daJobTypes As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents dvJobs_Materials As System.Data.DataView
    Friend WithEvents dvJobs_Granite As System.Data.DataView
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents dvJobs_DirectLabour As System.Data.DataView
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents dvJobs_SalesReps As System.Data.DataView
    Friend WithEvents pnlSalesReps As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlDirectLabour As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlJobInformation As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlJobAddress As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlClientPayments As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupLine1 As Power.Forms.GroupLine
    Friend WithEvents GroupLine9 As Power.Forms.GroupLine
    Friend WithEvents GroupLine10 As Power.Forms.GroupLine
    Friend WithEvents GroupLine11 As Power.Forms.GroupLine
    Friend WithEvents GroupLine12 As Power.Forms.GroupLine
    Friend WithEvents GroupLine13 As Power.Forms.GroupLine
    Friend WithEvents daClientPayments As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents dvClientPayments As System.Data.DataView
    Friend WithEvents pnlAppliances As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlOtherTrades As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupLine14 As Power.Forms.GroupLine
    Friend WithEvents GroupLine15 As Power.Forms.GroupLine
    Friend WithEvents GroupLine16 As Power.Forms.GroupLine
    Friend WithEvents tvDirectLabour As Power.Forms.TreeView
    Friend WithEvents tvSalesReps As Power.Forms.TreeView
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents dvBadDebts As System.Data.DataView
    Friend WithEvents dvDiscounts As System.Data.DataView
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents dgAppliances As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvAppliances As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colNIUserType1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIBrand As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIName2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIOrderedBy As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIPaidBy As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnAddAppliance As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEditAppliance As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveAppliance As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents daJobNonCoreSalesItems As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents dgOtherTrades As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvOtherTrades As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colNIName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIUserType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIOrderedBy1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIPaidBy1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnRemoveOtherTrade As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEditOtherTrade As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAddOtherTrade As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dvAppliances As System.Data.DataView
    Friend WithEvents dvOtherTrades As System.Data.DataView
    Friend WithEvents btnRemoveClientPayment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dgClientPayments As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvClientPayments As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colCPDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCPAmount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCPNotes As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtCurrencyClientPayments As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents btnRemoveDiscount As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dgBadDebts As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dgDiscounts As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvDiscounts As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colCPDate2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCPAmount2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtCurrencyDiscounts As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents colCPNotes2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gvBadDebts As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtCurrencyBadDebts As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents btnRemoveBadDebt As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtJBPrice As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBGraniteBAOTPrice As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBCabinetryBAOTPrice As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBJobStreetAddress01 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents rgJBJobAddressAsAbove As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents lblJobState As System.Windows.Forms.Label
    Friend WithEvents lblJobPostCode As System.Windows.Forms.Label
    Friend WithEvents lblJobStreetAddress As System.Windows.Forms.Label
    Friend WithEvents lblJobSuburb As System.Windows.Forms.Label
    Friend WithEvents txtJBJobStreetAddress02 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBJobSuburb As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBJobState As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBJobPostCode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents daJobs_Expenses As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents dgSalesReps As DevExpress.XtraGrid.GridControl
    Friend WithEvents pnlResponsibilities As DevExpress.XtraEditors.GroupControl
    Friend WithEvents dgResponsibilities As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvResponsibilities As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents BandedGridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BandedGridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BandedGridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dgDirectLabour As DevExpress.XtraGrid.GridControl
    Friend WithEvents txtJBJobType As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents pnlIntro As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtJBClientSurname As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupLine17 As Power.Forms.GroupLine
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents GroupLine18 As Power.Forms.GroupLine
    Friend WithEvents GroupLine19 As Power.Forms.GroupLine
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents pnlClientInformation As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtJBClientFirstName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBReferenceName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientStreetAddress01 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientStreetAddress02 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientPostCode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientFaxNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents dpJBActualFinishDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents PictureEdit4 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents txtPercentOfJob As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents txtJBClientEmail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents pnlJobIssues As DevExpress.XtraEditors.GroupControl
    Friend WithEvents daJobs_Jobissues As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colIfOccured1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIfOverSchedule1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDescription1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlSelectCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents GroupLine20 As Power.Forms.GroupLine
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents txtJBJobInsurance As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientSuburb As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Button1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents PictureEdit3 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents btnAddDirectLabour As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveDirectLabour As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAddSalesRep As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveSalesRep As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents PictureEdit1 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents dpJBScheduledFinishDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dpJBScheduledStartDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents txtPercentageDirectLabour As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents txtPercentageSalesReps As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents txtDollarDirectLabour As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents txtDollarSalesRep As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents pnlSummary As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents lblJBPrice As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblJBPriceAppliances As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblJBPriceOtherTrades As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblJBJobInsurance As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblJBPriceTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblJBClientPaid As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblJBBalance As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents lblJBOtherReduction As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DsReports As WindowsApplication.dsReports
    Friend WithEvents btnViewJobReport As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents pnlNotes As DevExpress.XtraEditors.GroupControl
    Friend WithEvents dvExpenses As System.Data.DataView
    Friend WithEvents daExpenses As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents dgJobIssues As DevExpress.XtraGrid.GridControl
    Friend WithEvents rgPayAsDirectLabour As DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup
    Friend WithEvents rgPayAsSalesReps As DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup
    Friend WithEvents PersistentRepository1 As DevExpress.XtraEditors.Repository.PersistentRepository
    Friend WithEvents txtDate As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dataSet As WindowsApplication.dsJobs
    Friend WithEvents RepositoryItemComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents dvExpenses2 As System.Data.DataView
    Friend WithEvents DsNotes As WindowsApplication.dsNotes
    Friend WithEvents daINFollowUpTypes As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daIDNotes As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents dgNotes As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvNotes As DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
    Friend WithEvents GridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents colINUser As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents txtEXID As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents colINDate As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents txtINDate As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents colINNotes As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents txtINNotes As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents colINFollowUpText As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents txtINFollowUpText As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colJEHours As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEXName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gvDirectLabour As DevExpress.XtraGrid.Views.Card.CardView
    Friend WithEvents SqlSelectCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents gvSalesReps As DevExpress.XtraGrid.Views.Card.CardView
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents rgLDUseContactFalse As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents pceContact As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents rgLDUseContactTrue As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents daContacts As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents pccContacts As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents txtJBClientWorkPhoneNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientHomePhoneNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientState As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientMobileNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents pnlInternalOrders As DevExpress.XtraEditors.GroupControl
    Friend WithEvents dgInternalOrders As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvInternalOrders As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btnRemoveInternalOrder As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEditInternalOrder As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAddInternalOrder As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dvInternalOrders As System.Data.DataView
    Friend WithEvents colOROrderNumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colORDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colORDescription As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents daOrders As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents daOrders_Materials As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents daOrders_Materials_SubItems As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daOrders_StockedItems As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents pnlJobPurchaseOrders As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnRemoveJobPurchaseOrder As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEditJobPurchaseOrder As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAddJobPurchaseOrder As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dgJobPurchaseOrders As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gvJobPurchaseOrders As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents dvJobPurchaseOrders As System.Data.DataView
    Friend WithEvents daMaterials As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand12 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daStockedItems As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommand13 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand14 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents colORInvoicedCostTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colORStatus As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colORStatus1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents DsOrders As WindowsApplication.dsOrders
    Friend WithEvents SqlSelectCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand9 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmJob2))
        Me.ListBox = New Power.Forms.ListBox
        Me.dataSet = New WindowsApplication.dsJobs
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.pnlMain = New DevExpress.XtraEditors.PanelControl
        Me.pnlInternalOrders = New DevExpress.XtraEditors.GroupControl
        Me.btnRemoveInternalOrder = New DevExpress.XtraEditors.SimpleButton
        Me.btnEditInternalOrder = New DevExpress.XtraEditors.SimpleButton
        Me.btnAddInternalOrder = New DevExpress.XtraEditors.SimpleButton
        Me.dgInternalOrders = New DevExpress.XtraGrid.GridControl
        Me.dvInternalOrders = New System.Data.DataView
        Me.DsOrders = New WindowsApplication.dsOrders
        Me.gvInternalOrders = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colOROrderNumber = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colORDate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colORDescription = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colORStatus1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.pnlJobPurchaseOrders = New DevExpress.XtraEditors.GroupControl
        Me.btnRemoveJobPurchaseOrder = New DevExpress.XtraEditors.SimpleButton
        Me.btnEditJobPurchaseOrder = New DevExpress.XtraEditors.SimpleButton
        Me.btnAddJobPurchaseOrder = New DevExpress.XtraEditors.SimpleButton
        Me.dgJobPurchaseOrders = New DevExpress.XtraGrid.GridControl
        Me.dvJobPurchaseOrders = New System.Data.DataView
        Me.gvJobPurchaseOrders = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colORInvoicedCostTotal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colORStatus = New DevExpress.XtraGrid.Columns.GridColumn
        Me.pnlClientInformation = New DevExpress.XtraEditors.GroupControl
        Me.rgLDUseContactFalse = New DevExpress.XtraEditors.RadioGroup
        Me.Label60 = New System.Windows.Forms.Label
        Me.pceContact = New DevExpress.XtraEditors.PopupContainerEdit
        Me.pccContacts = New DevExpress.XtraEditors.PopupContainerControl
        Me.rgLDUseContactTrue = New DevExpress.XtraEditors.RadioGroup
        Me.Label59 = New System.Windows.Forms.Label
        Me.txtJBClientWorkPhoneNumber = New DevExpress.XtraEditors.TextEdit
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label45 = New System.Windows.Forms.Label
        Me.txtJBClientEmail = New DevExpress.XtraEditors.TextEdit
        Me.Label13 = New System.Windows.Forms.Label
        Me.txtJBClientFirstName = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientSurname = New DevExpress.XtraEditors.TextEdit
        Me.GroupLine17 = New Power.Forms.GroupLine
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.GroupLine18 = New Power.Forms.GroupLine
        Me.GroupLine19 = New Power.Forms.GroupLine
        Me.Label30 = New System.Windows.Forms.Label
        Me.txtJBReferenceName = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientStreetAddress01 = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientStreetAddress02 = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientSuburb = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientState = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientPostCode = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientHomePhoneNumber = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientFaxNumber = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientMobileNumber = New DevExpress.XtraEditors.TextEdit
        Me.pnlJobInformation = New DevExpress.XtraEditors.GroupControl
        Me.dpJBScheduledStartDate = New DevExpress.XtraEditors.DateEdit
        Me.dpJBScheduledFinishDate = New DevExpress.XtraEditors.DateEdit
        Me.txtJBJobInsurance = New DevExpress.XtraEditors.TextEdit
        Me.Label46 = New System.Windows.Forms.Label
        Me.Label40 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtJBJobType = New DevExpress.XtraEditors.LookUpEdit
        Me.dpJBActualFinishDate = New DevExpress.XtraEditors.DateEdit
        Me.txtJBCabinetryBAOTPrice = New DevExpress.XtraEditors.TextEdit
        Me.txtJBGraniteBAOTPrice = New DevExpress.XtraEditors.TextEdit
        Me.txtJBPrice = New DevExpress.XtraEditors.TextEdit
        Me.GroupLine14 = New Power.Forms.GroupLine
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.GroupLine15 = New Power.Forms.GroupLine
        Me.GroupLine16 = New Power.Forms.GroupLine
        Me.Label31 = New System.Windows.Forms.Label
        Me.Label32 = New System.Windows.Forms.Label
        Me.Label33 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label36 = New System.Windows.Forms.Label
        Me.Label38 = New System.Windows.Forms.Label
        Me.Label39 = New System.Windows.Forms.Label
        Me.GroupLine20 = New Power.Forms.GroupLine
        Me.Label47 = New System.Windows.Forms.Label
        Me.pnlClientPayments = New DevExpress.XtraEditors.GroupControl
        Me.Label41 = New System.Windows.Forms.Label
        Me.dgClientPayments = New DevExpress.XtraGrid.GridControl
        Me.dvClientPayments = New System.Data.DataView
        Me.PersistentRepository1 = New DevExpress.XtraEditors.Repository.PersistentRepository(Me.components)
        Me.txtDate = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.RepositoryItemComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox
        Me.gvClientPayments = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colCPDate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colCPAmount = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtCurrencyClientPayments = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colCPNotes = New DevExpress.XtraGrid.Columns.GridColumn
        Me.btnRemoveClientPayment = New DevExpress.XtraEditors.SimpleButton
        Me.dgDiscounts = New DevExpress.XtraGrid.GridControl
        Me.dvDiscounts = New System.Data.DataView
        Me.gvDiscounts = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colCPDate2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colCPAmount2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtCurrencyDiscounts = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colCPNotes2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Label35 = New System.Windows.Forms.Label
        Me.btnRemoveBadDebt = New DevExpress.XtraEditors.SimpleButton
        Me.dgBadDebts = New DevExpress.XtraGrid.GridControl
        Me.dvBadDebts = New System.Data.DataView
        Me.gvBadDebts = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtCurrencyBadDebts = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.btnRemoveDiscount = New DevExpress.XtraEditors.SimpleButton
        Me.Label34 = New System.Windows.Forms.Label
        Me.pnlAppliances = New DevExpress.XtraEditors.GroupControl
        Me.btnRemoveAppliance = New DevExpress.XtraEditors.SimpleButton
        Me.btnEditAppliance = New DevExpress.XtraEditors.SimpleButton
        Me.btnAddAppliance = New DevExpress.XtraEditors.SimpleButton
        Me.dgAppliances = New DevExpress.XtraGrid.GridControl
        Me.dvAppliances = New System.Data.DataView
        Me.gvAppliances = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colNIUserType1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIBrand = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIName2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIOrderedBy = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIPaidBy = New DevExpress.XtraGrid.Columns.GridColumn
        Me.pnlDirectLabour = New DevExpress.XtraEditors.GroupControl
        Me.btnRemoveDirectLabour = New DevExpress.XtraEditors.SimpleButton
        Me.btnAddDirectLabour = New DevExpress.XtraEditors.SimpleButton
        Me.dgDirectLabour = New DevExpress.XtraGrid.GridControl
        Me.dvJobs_DirectLabour = New System.Data.DataView
        Me.gvDirectLabour = New DevExpress.XtraGrid.Views.Card.CardView
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.rgPayAsDirectLabour = New DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtDollarDirectLabour = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtPercentageDirectLabour = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colJEHours = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colEXName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tvDirectLabour = New Power.Forms.TreeView
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.pnlSummary = New DevExpress.XtraEditors.GroupControl
        Me.btnViewJobReport = New DevExpress.XtraEditors.SimpleButton
        Me.Label57 = New System.Windows.Forms.Label
        Me.lblJBOtherReduction = New DevExpress.XtraEditors.TextEdit
        Me.DsReports = New WindowsApplication.dsReports
        Me.lblJBBalance = New DevExpress.XtraEditors.TextEdit
        Me.lblJBClientPaid = New DevExpress.XtraEditors.TextEdit
        Me.lblJBPriceTotal = New DevExpress.XtraEditors.TextEdit
        Me.lblJBJobInsurance = New DevExpress.XtraEditors.TextEdit
        Me.lblJBPriceOtherTrades = New DevExpress.XtraEditors.TextEdit
        Me.lblJBPriceAppliances = New DevExpress.XtraEditors.TextEdit
        Me.lblJBPrice = New DevExpress.XtraEditors.TextEdit
        Me.Label56 = New System.Windows.Forms.Label
        Me.Label54 = New System.Windows.Forms.Label
        Me.Label53 = New System.Windows.Forms.Label
        Me.Label52 = New System.Windows.Forms.Label
        Me.Label51 = New System.Windows.Forms.Label
        Me.Label50 = New System.Windows.Forms.Label
        Me.Label49 = New System.Windows.Forms.Label
        Me.pnlSalesReps = New DevExpress.XtraEditors.GroupControl
        Me.btnRemoveSalesRep = New DevExpress.XtraEditors.SimpleButton
        Me.btnAddSalesRep = New DevExpress.XtraEditors.SimpleButton
        Me.dgSalesReps = New DevExpress.XtraGrid.GridControl
        Me.dvJobs_SalesReps = New System.Data.DataView
        Me.gvSalesReps = New DevExpress.XtraGrid.Views.Card.CardView
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.rgPayAsSalesReps = New DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtDollarSalesRep = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtPercentageSalesReps = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.tvSalesReps = New Power.Forms.TreeView
        Me.Label28 = New System.Windows.Forms.Label
        Me.Label29 = New System.Windows.Forms.Label
        Me.pnlNotes = New DevExpress.XtraEditors.GroupControl
        Me.dgNotes = New DevExpress.XtraGrid.GridControl
        Me.DsNotes = New WindowsApplication.dsNotes
        Me.gvNotes = New DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
        Me.GridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.colINUser = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.txtEXID = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.dvExpenses2 = New System.Data.DataView
        Me.colINDate = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.txtINDate = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.colINNotes = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.txtINNotes = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
        Me.colINFollowUpText = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.txtINFollowUpText = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
        Me.pnlJobAddress = New DevExpress.XtraEditors.GroupControl
        Me.txtJBJobStreetAddress01 = New DevExpress.XtraEditors.TextEdit
        Me.rgJBJobAddressAsAbove = New DevExpress.XtraEditors.RadioGroup
        Me.Label17 = New System.Windows.Forms.Label
        Me.lblJobState = New System.Windows.Forms.Label
        Me.lblJobPostCode = New System.Windows.Forms.Label
        Me.lblJobStreetAddress = New System.Windows.Forms.Label
        Me.lblJobSuburb = New System.Windows.Forms.Label
        Me.txtJBJobStreetAddress02 = New DevExpress.XtraEditors.TextEdit
        Me.txtJBJobSuburb = New DevExpress.XtraEditors.TextEdit
        Me.txtJBJobState = New DevExpress.XtraEditors.TextEdit
        Me.txtJBJobPostCode = New DevExpress.XtraEditors.TextEdit
        Me.pnlOtherTrades = New DevExpress.XtraEditors.GroupControl
        Me.btnRemoveOtherTrade = New DevExpress.XtraEditors.SimpleButton
        Me.btnEditOtherTrade = New DevExpress.XtraEditors.SimpleButton
        Me.btnAddOtherTrade = New DevExpress.XtraEditors.SimpleButton
        Me.dgOtherTrades = New DevExpress.XtraGrid.GridControl
        Me.dvOtherTrades = New System.Data.DataView
        Me.gvOtherTrades = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colNIName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIUserType = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIOrderedBy1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIPaidBy1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.pnlIntro = New DevExpress.XtraEditors.GroupControl
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Label44 = New System.Windows.Forms.Label
        Me.PictureEdit4 = New DevExpress.XtraEditors.PictureEdit
        Me.Label43 = New System.Windows.Forms.Label
        Me.Label42 = New System.Windows.Forms.Label
        Me.PictureEdit3 = New DevExpress.XtraEditors.PictureEdit
        Me.Label48 = New System.Windows.Forms.Label
        Me.PictureEdit1 = New DevExpress.XtraEditors.PictureEdit
        Me.pnlResponsibilities = New DevExpress.XtraEditors.GroupControl
        Me.dgResponsibilities = New DevExpress.XtraGrid.GridControl
        Me.gvResponsibilities = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.BandedGridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.BandedGridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtPercentOfJob = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.BandedGridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.pnlJobIssues = New DevExpress.XtraEditors.GroupControl
        Me.dgJobIssues = New DevExpress.XtraGrid.GridControl
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colIfOccured1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colIfOverSchedule1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colDescription1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.dvJobs_Materials = New System.Data.DataView
        Me.dvJobs_Granite = New System.Data.DataView
        Me.dvExpenses = New System.Data.DataView
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.daJobTypes = New System.Data.SqlClient.SqlDataAdapter
        Me.daJobs_Expenses = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand
        Me.daClientPayments = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand7 = New System.Data.SqlClient.SqlCommand
        Me.daJobNonCoreSalesItems = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand9 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand11 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand11 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand9 = New System.Data.SqlClient.SqlCommand
        Me.daJobs_Jobissues = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand5 = New System.Data.SqlClient.SqlCommand
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.Button1 = New DevExpress.XtraEditors.SimpleButton
        Me.daExpenses = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlInsertCommand9 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand9 = New System.Data.SqlClient.SqlCommand
        Me.daINFollowUpTypes = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand9 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand10 = New System.Data.SqlClient.SqlCommand
        Me.daIDNotes = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand6 = New System.Data.SqlClient.SqlCommand
        Me.daContacts = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand6 = New System.Data.SqlClient.SqlCommand
        Me.daOrders = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.daOrders_Materials = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand8 = New System.Data.SqlClient.SqlCommand
        Me.daOrders_Materials_SubItems = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand10 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand10 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand10 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand10 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand11 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand11 = New System.Data.SqlClient.SqlCommand
        Me.daOrders_StockedItems = New System.Data.SqlClient.SqlDataAdapter
        Me.daMaterials = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlCommand11 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand12 = New System.Data.SqlClient.SqlCommand
        Me.daStockedItems = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlCommand13 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand14 = New System.Data.SqlClient.SqlCommand
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMain.SuspendLayout()
        CType(Me.pnlInternalOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlInternalOrders.SuspendLayout()
        CType(Me.dgInternalOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvInternalOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvInternalOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlJobPurchaseOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlJobPurchaseOrders.SuspendLayout()
        CType(Me.dgJobPurchaseOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvJobPurchaseOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvJobPurchaseOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlClientInformation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlClientInformation.SuspendLayout()
        CType(Me.rgLDUseContactFalse.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pceContact.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pccContacts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgLDUseContactTrue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientWorkPhoneNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientFirstName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBReferenceName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientSuburb.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientState.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientPostCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientHomePhoneNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientFaxNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientMobileNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlJobInformation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlJobInformation.SuspendLayout()
        CType(Me.dpJBScheduledStartDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dpJBScheduledFinishDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBJobInsurance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBJobType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dpJBActualFinishDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBCabinetryBAOTPrice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBGraniteBAOTPrice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBPrice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlClientPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlClientPayments.SuspendLayout()
        CType(Me.dgClientPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvClientPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvClientPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCurrencyClientPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgDiscounts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvDiscounts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvDiscounts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCurrencyDiscounts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgBadDebts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvBadDebts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvBadDebts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCurrencyBadDebts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlAppliances, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlAppliances.SuspendLayout()
        CType(Me.dgAppliances, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvAppliances, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvAppliances, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlDirectLabour, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlDirectLabour.SuspendLayout()
        CType(Me.dgDirectLabour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvJobs_DirectLabour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvDirectLabour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgPayAsDirectLabour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDollarDirectLabour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPercentageDirectLabour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlSummary.SuspendLayout()
        CType(Me.lblJBOtherReduction.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsReports, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblJBBalance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblJBClientPaid.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblJBPriceTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblJBJobInsurance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblJBPriceOtherTrades.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblJBPriceAppliances.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblJBPrice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlSalesReps, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlSalesReps.SuspendLayout()
        CType(Me.dgSalesReps, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvJobs_SalesReps, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvSalesReps, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgPayAsSalesReps, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDollarSalesRep, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPercentageSalesReps, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlNotes.SuspendLayout()
        CType(Me.dgNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEXID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvExpenses2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtINDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtINNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtINFollowUpText, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlJobAddress, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlJobAddress.SuspendLayout()
        CType(Me.txtJBJobStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgJBJobAddressAsAbove.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBJobStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBJobSuburb.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBJobState.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBJobPostCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlOtherTrades, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlOtherTrades.SuspendLayout()
        CType(Me.dgOtherTrades, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvOtherTrades, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvOtherTrades, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlIntro, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlIntro.SuspendLayout()
        CType(Me.PictureEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlResponsibilities, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlResponsibilities.SuspendLayout()
        CType(Me.dgResponsibilities, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvResponsibilities, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPercentOfJob, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlJobIssues, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlJobIssues.SuspendLayout()
        CType(Me.dgJobIssues, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvJobs_Materials, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvJobs_Granite, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvExpenses, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ListBox
        '
        Me.ListBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ListBox.ColumnWidth = 20
        Me.ListBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ListBox.IntegralHeight = False
        Me.ListBox.ItemHeight = 25
        Me.ListBox.Items.AddRange(New Object() {"Introduction", "Customer Information", "Job Address", "Job Information", "Job Issues", "Job Purchase Orders", "Internal Orders", "Specified Direct Labor", "Specified Salespeople", "Responsibilities", "Appliances & Plumbing Items", "Other Trades", "Customer Payments", "Notes and Reminders", "Job Price Summary"})
        Me.ListBox.Location = New System.Drawing.Point(8, 8)
        Me.ListBox.Name = "ListBox"
        Me.ListBox.Size = New System.Drawing.Size(168, 520)
        Me.ListBox.TabIndex = 0
        '
        'dataSet
        '
        Me.dataSet.DataSetName = "dsJobs"
        Me.dataSet.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;user id=sa;integrated security=SSPI;data sou" & _
        "rce=""SERVER\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Jobs", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("JBID", "JBID"), New System.Data.Common.DataColumnMapping("ID", "ID"), New System.Data.Common.DataColumnMapping("JBScheduledStartDate", "JBScheduledStartDate"), New System.Data.Common.DataColumnMapping("JBScheduledFinishDate", "JBScheduledFinishDate"), New System.Data.Common.DataColumnMapping("JBActualFinishDate", "JBActualFinishDate"), New System.Data.Common.DataColumnMapping("JBClientFirstName", "JBClientFirstName"), New System.Data.Common.DataColumnMapping("JBClientSurname", "JBClientSurname"), New System.Data.Common.DataColumnMapping("JBClientName", "JBClientName"), New System.Data.Common.DataColumnMapping("JBClientStreetAddress01", "JBClientStreetAddress01"), New System.Data.Common.DataColumnMapping("JBClientStreetAddress02", "JBClientStreetAddress02"), New System.Data.Common.DataColumnMapping("JBClientSuburb", "JBClientSuburb"), New System.Data.Common.DataColumnMapping("JBClientState", "JBClientState"), New System.Data.Common.DataColumnMapping("JBClientPostCode", "JBClientPostCode"), New System.Data.Common.DataColumnMapping("JBClientAddress", "JBClientAddress"), New System.Data.Common.DataColumnMapping("JBClientHomePhoneNumber", "JBClientHomePhoneNumber"), New System.Data.Common.DataColumnMapping("JBClientWorkPhoneNumber", "JBClientWorkPhoneNumber"), New System.Data.Common.DataColumnMapping("JBClientFaxNumber", "JBClientFaxNumber"), New System.Data.Common.DataColumnMapping("JBClientMobileNumber", "JBClientMobileNumber"), New System.Data.Common.DataColumnMapping("JBJobAddressAsAbove", "JBJobAddressAsAbove"), New System.Data.Common.DataColumnMapping("JBJobStreetAddress01", "JBJobStreetAddress01"), New System.Data.Common.DataColumnMapping("JBJobStreetAddress02", "JBJobStreetAddress02"), New System.Data.Common.DataColumnMapping("JBJobSuburb", "JBJobSuburb"), New System.Data.Common.DataColumnMapping("JBJobState", "JBJobState"), New System.Data.Common.DataColumnMapping("JBJobPostCode", "JBJobPostCode"), New System.Data.Common.DataColumnMapping("JBJobAddress", "JBJobAddress"), New System.Data.Common.DataColumnMapping("JTID", "JTID"), New System.Data.Common.DataColumnMapping("JBGraniteBAOTPrice", "JBGraniteBAOTPrice"), New System.Data.Common.DataColumnMapping("JBCabinetryBAOTPrice", "JBCabinetryBAOTPrice"), New System.Data.Common.DataColumnMapping("JBPrice", "JBPrice"), New System.Data.Common.DataColumnMapping("JBReferenceName", "JBReferenceName"), New System.Data.Common.DataColumnMapping("JBJobInsurance", "JBJobInsurance"), New System.Data.Common.DataColumnMapping("JBIsCancelled", "JBIsCancelled"), New System.Data.Common.DataColumnMapping("JBClientEmail", "JBClientEmail"), New System.Data.Common.DataColumnMapping("JBInventoryDate", "JBInventoryDate"), New System.Data.Common.DataColumnMapping("CTID", "CTID"), New System.Data.Common.DataColumnMapping("JBUseContact", "JBUseContact")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Jobs WHERE (BRID = @Original_BRID) AND (JBID = @Original_JBID) AND (C" & _
        "TID = @Original_CTID OR @Original_CTID IS NULL AND CTID IS NULL) AND (ID = @Orig" & _
        "inal_ID) AND (JBActualFinishDate = @Original_JBActualFinishDate OR @Original_JBA" & _
        "ctualFinishDate IS NULL AND JBActualFinishDate IS NULL) AND (JBCabinetryBAOTPric" & _
        "e = @Original_JBCabinetryBAOTPrice OR @Original_JBCabinetryBAOTPrice IS NULL AND" & _
        " JBCabinetryBAOTPrice IS NULL) AND (JBClientAddress = @Original_JBClientAddress " & _
        "OR @Original_JBClientAddress IS NULL AND JBClientAddress IS NULL) AND (JBClientE" & _
        "mail = @Original_JBClientEmail OR @Original_JBClientEmail IS NULL AND JBClientEm" & _
        "ail IS NULL) AND (JBClientFaxNumber = @Original_JBClientFaxNumber OR @Original_J" & _
        "BClientFaxNumber IS NULL AND JBClientFaxNumber IS NULL) AND (JBClientFirstName =" & _
        " @Original_JBClientFirstName OR @Original_JBClientFirstName IS NULL AND JBClient" & _
        "FirstName IS NULL) AND (JBClientHomePhoneNumber = @Original_JBClientHomePhoneNum" & _
        "ber OR @Original_JBClientHomePhoneNumber IS NULL AND JBClientHomePhoneNumber IS " & _
        "NULL) AND (JBClientMobileNumber = @Original_JBClientMobileNumber OR @Original_JB" & _
        "ClientMobileNumber IS NULL AND JBClientMobileNumber IS NULL) AND (JBClientName =" & _
        " @Original_JBClientName OR @Original_JBClientName IS NULL AND JBClientName IS NU" & _
        "LL) AND (JBClientPostCode = @Original_JBClientPostCode OR @Original_JBClientPost" & _
        "Code IS NULL AND JBClientPostCode IS NULL) AND (JBClientState = @Original_JBClie" & _
        "ntState OR @Original_JBClientState IS NULL AND JBClientState IS NULL) AND (JBCli" & _
        "entStreetAddress01 = @Original_JBClientStreetAddress01 OR @Original_JBClientStre" & _
        "etAddress01 IS NULL AND JBClientStreetAddress01 IS NULL) AND (JBClientStreetAddr" & _
        "ess02 = @Original_JBClientStreetAddress02 OR @Original_JBClientStreetAddress02 I" & _
        "S NULL AND JBClientStreetAddress02 IS NULL) AND (JBClientSuburb = @Original_JBCl" & _
        "ientSuburb OR @Original_JBClientSuburb IS NULL AND JBClientSuburb IS NULL) AND (" & _
        "JBClientSurname = @Original_JBClientSurname OR @Original_JBClientSurname IS NULL" & _
        " AND JBClientSurname IS NULL) AND (JBClientWorkPhoneNumber = @Original_JBClientW" & _
        "orkPhoneNumber OR @Original_JBClientWorkPhoneNumber IS NULL AND JBClientWorkPhon" & _
        "eNumber IS NULL) AND (JBGraniteBAOTPrice = @Original_JBGraniteBAOTPrice OR @Orig" & _
        "inal_JBGraniteBAOTPrice IS NULL AND JBGraniteBAOTPrice IS NULL) AND (JBInventory" & _
        "Date = @Original_JBInventoryDate OR @Original_JBInventoryDate IS NULL AND JBInve" & _
        "ntoryDate IS NULL) AND (JBIsCancelled = @Original_JBIsCancelled OR @Original_JBI" & _
        "sCancelled IS NULL AND JBIsCancelled IS NULL) AND (JBJobAddress = @Original_JBJo" & _
        "bAddress OR @Original_JBJobAddress IS NULL AND JBJobAddress IS NULL) AND (JBJobA" & _
        "ddressAsAbove = @Original_JBJobAddressAsAbove OR @Original_JBJobAddressAsAbove I" & _
        "S NULL AND JBJobAddressAsAbove IS NULL) AND (JBJobInsurance = @Original_JBJobIns" & _
        "urance OR @Original_JBJobInsurance IS NULL AND JBJobInsurance IS NULL) AND (JBJo" & _
        "bPostCode = @Original_JBJobPostCode OR @Original_JBJobPostCode IS NULL AND JBJob" & _
        "PostCode IS NULL) AND (JBJobState = @Original_JBJobState OR @Original_JBJobState" & _
        " IS NULL AND JBJobState IS NULL) AND (JBJobStreetAddress01 = @Original_JBJobStre" & _
        "etAddress01 OR @Original_JBJobStreetAddress01 IS NULL AND JBJobStreetAddress01 I" & _
        "S NULL) AND (JBJobStreetAddress02 = @Original_JBJobStreetAddress02 OR @Original_" & _
        "JBJobStreetAddress02 IS NULL AND JBJobStreetAddress02 IS NULL) AND (JBJobSuburb " & _
        "= @Original_JBJobSuburb OR @Original_JBJobSuburb IS NULL AND JBJobSuburb IS NULL" & _
        ") AND (JBPrice = @Original_JBPrice OR @Original_JBPrice IS NULL AND JBPrice IS N" & _
        "ULL) AND (JBReferenceName = @Original_JBReferenceName OR @Original_JBReferenceNa" & _
        "me IS NULL AND JBReferenceName IS NULL) AND (JBScheduledFinishDate = @Original_J" & _
        "BScheduledFinishDate OR @Original_JBScheduledFinishDate IS NULL AND JBScheduledF" & _
        "inishDate IS NULL) AND (JBScheduledStartDate = @Original_JBScheduledStartDate OR" & _
        " @Original_JBScheduledStartDate IS NULL AND JBScheduledStartDate IS NULL) AND (J" & _
        "BUseContact = @Original_JBUseContact OR @Original_JBUseContact IS NULL AND JBUse" & _
        "Contact IS NULL) AND (JTID = @Original_JTID OR @Original_JTID IS NULL AND JTID I" & _
        "S NULL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBActualFinishDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBActualFinishDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBCabinetryBAOTPrice", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBCabinetryBAOTPrice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientFirstName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientFirstName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientHomePhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientName", System.Data.SqlDbType.VarChar, 102, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientSurname", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientWorkPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBGraniteBAOTPrice", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBGraniteBAOTPrice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBInventoryDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBInventoryDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBIsCancelled", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBIsCancelled", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobAddressAsAbove", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobInsurance", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobInsurance", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBPrice", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBPrice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBReferenceName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBReferenceName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBScheduledFinishDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBScheduledFinishDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBScheduledStartDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBScheduledStartDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBUseContact", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBUseContact", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JTID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Jobs(BRID, ID, JBScheduledStartDate, JBScheduledFinishDate, JBActualF" &
        "inishDate, JBClientFirstName, JBClientSurname, JBClientName, JBClientStreetAddre" &
        "ss01, JBClientStreetAddress02, JBClientSuburb, JBClientState, JBClientPostCode, " &
        "JBClientAddress, JBClientHomePhoneNumber, JBClientWorkPhoneNumber, JBClientFaxNu" &
        "mber, JBClientMobileNumber, JBJobAddressAsAbove, JBJobStreetAddress01, JBJobStre" &
        "etAddress02, JBJobSuburb, JBJobState, JBJobPostCode, JBJobAddress, JTID, JBGrani" &
        "teBAOTPrice, JBCabinetryBAOTPrice, JBPrice, JBReferenceName, JBJobInsurance, JBI" &
        "sCancelled, JBClientEmail, JBInventoryDate, CTID, JBUseContact) VALUES (@BRID, @" &
        "ID, @JBScheduledStartDate, @JBScheduledFinishDate, @JBActualFinishDate, @JBClien" &
        "tFirstName, @JBClientSurname, @JBClientName, @JBClientStreetAddress01, @JBClient" &
        "StreetAddress02, @JBClientSuburb, @JBClientState, @JBClientPostCode, @JBClientAd" &
        "dress, @JBClientHomePhoneNumber, @JBClientWorkPhoneNumber, @JBClientFaxNumber, @" &
        "JBClientMobileNumber, @JBJobAddressAsAbove, @JBJobStreetAddress01, @JBJobStreetA" &
        "ddress02, @JBJobSuburb, @JBJobState, @JBJobPostCode, @JBJobAddress, @JTID, @JBGr" &
        "aniteBAOTPrice, @JBCabinetryBAOTPrice, @JBPrice, @JBReferenceName, @JBJobInsuran" &
        "ce, @JBIsCancelled, @JBClientEmail, @JBInventoryDate, @CTID, @JBUseContact); SEL" &
        "ECT BRID, JBID, ID, JBScheduledStartDate, JBScheduledFinishDate, JBActualFinishD" &
        "ate, JBClientFirstName, " &
        " JBClientSurname, JBClientName, JBClientStreetAddress01, " &
        "JBClientStreetAddress02, JBClientSuburb, JBClientState, JBClientPostCode, JBClie" &
        "ntAddress, JBClientHomePhoneNumber, JBClientWorkPhoneNumber, JBClientFaxNumber, " &
        "JBClientMobileNumber, JBJobAddressAsAbove, JBJobStreetAddress01, JBJobStreetAddr" &
        "ess02, JBJobSuburb, JBJobState, JBJobPostCode, JBJobAddress, JTID, JBGraniteBAOT" &
        "Price, JBCabinetryBAOTPrice, JBPrice, JBReferenceName, JBJobInsurance, JBIsCance" &
        "lled, JBClientEmail, JBInventoryDate, CTID, JBUseContact FROM Jobs WHERE (BRID =" &
        " @BRID) And (JBID = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBScheduledStartDate", System.Data.SqlDbType.DateTime, 8, "JBScheduledStartDate"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBScheduledFinishDate", System.Data.SqlDbType.DateTime, 8, "JBScheduledFinishDate"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBActualFinishDate", System.Data.SqlDbType.DateTime, 8, "JBActualFinishDate"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientFirstName", System.Data.SqlDbType.VarChar, 50, "JBClientFirstName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientSurname", System.Data.SqlDbType.VarChar, 50, "JBClientSurname"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientName", System.Data.SqlDbType.VarChar, 102, "JBClientName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, "JBClientStreetAddress01"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, "JBClientStreetAddress02"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientSuburb", System.Data.SqlDbType.VarChar, 50, "JBClientSuburb"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientState", System.Data.SqlDbType.VarChar, 3, "JBClientState"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientPostCode", System.Data.SqlDbType.VarChar, 20, "JBClientPostCode"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientAddress", System.Data.SqlDbType.VarChar, 259, "JBClientAddress"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, "JBClientHomePhoneNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, "JBClientWorkPhoneNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientFaxNumber", System.Data.SqlDbType.VarChar, 20, "JBClientFaxNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientMobileNumber", System.Data.SqlDbType.VarChar, 20, "JBClientMobileNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, "JBJobAddressAsAbove"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, "JBJobStreetAddress01"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, "JBJobStreetAddress02"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobSuburb", System.Data.SqlDbType.VarChar, 50, "JBJobSuburb"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobState", System.Data.SqlDbType.VarChar, 3, "JBJobState"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobPostCode", System.Data.SqlDbType.VarChar, 20, "JBJobPostCode"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobAddress", System.Data.SqlDbType.VarChar, 259, "JBJobAddress"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JTID", System.Data.SqlDbType.Int, 4, "JTID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBGraniteBAOTPrice", System.Data.SqlDbType.Money, 8, "JBGraniteBAOTPrice"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBCabinetryBAOTPrice", System.Data.SqlDbType.Money, 8, "JBCabinetryBAOTPrice"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBPrice", System.Data.SqlDbType.Money, 8, "JBPrice"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBReferenceName", System.Data.SqlDbType.VarChar, 100, "JBReferenceName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobInsurance", System.Data.SqlDbType.Money, 8, "JBJobInsurance"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBIsCancelled", System.Data.SqlDbType.Bit, 1, "JBIsCancelled"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientEmail", System.Data.SqlDbType.VarChar, 50, "JBClientEmail"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBInventoryDate", System.Data.SqlDbType.DateTime, 8, "JBInventoryDate"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTID", System.Data.SqlDbType.BigInt, 8, "CTID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBUseContact", System.Data.SqlDbType.Bit, 1, "JBUseContact"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, JBID, ID, JBScheduledStartDate, JBScheduledFinishDate, JBActualFinis" &
        "hDate, JBClientFirstName," &
        " JBClientSurname, JBClientName, JBClientStreetAddress01" &
        ", JBClientStreetAddress02, JBClientSuburb, JBClientState, JBClientPostCode, JBCl" &
        "ientAddress, JBClientHomePhoneNumber, JBClientWorkPhoneNumber, JBClientFaxNumber" &
        ", JBClientMobileNumber, JBJobAddressAsAbove, JBJobStreetAddress01, JBJobStreetAd" &
        "dress02, JBJobSuburb, JBJobState, JBJobPostCode, JBJobAddress, JTID, JBGraniteBA" &
        "OTPrice, JBCabinetryBAOTPrice, JBPrice, JBReferenceName, JBJobInsurance, JBIsCan" &
        "celled, JBClientEmail, JBInventoryDate, CTID, JBUseContact FROM Jobs WHERE (BRID" &
        " = @BRID) And (JBID = @JBID)"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Jobs SET BRID = @BRID, ID = @ID, JBScheduledStartDate = @JBScheduledStartD" &
        "ate, JBScheduledFinishDate = @JBScheduledFinishDate, JBActualFinishDate = @JBAct" &
        "ualFinishDate, JBClientFirstName = @JBClientFirstName, JBClientSurname = @JBClie" &
        "ntSurname, JBClientName = @JBClientName, JBClientStreetAddress01 = @JBClientStre" &
        "etAddress01, JBClientStreetAddress02 = @JBClientStreetAddress02, JBClientSuburb " &
        "= @JBClientSuburb, JBClientState = @JBClientState, JBClientPostCode = @JBClientP" &
        "ostCode, JBClientAddress = @JBClientAddress, JBClientHomePhoneNumber = @JBClient" &
        "HomePhoneNumber, JBClientWorkPhoneNumber = @JBClientWorkPhoneNumber, JBClientFax" &
        "Number = @JBClientFaxNumber, JBClientMobileNumber = @JBClientMobileNumber, JBJob" &
        "AddressAsAbove = @JBJobAddressAsAbove, JBJobStreetAddress01 = @JBJobStreetAddres" &
        "s01, JBJobStreetAddress02 = @JBJobStreetAddress02, JBJobSuburb = @JBJobSuburb, J" &
        "BJobState = @JBJobState, JBJobPostCode = @JBJobPostCode, JBJobAddress = @JBJobAd" &
        "dress, JTID = @JTID, JBGraniteBAOTPrice = @JBGraniteBAOTPrice, JBCabinetryBAOTPr" &
        "ice = @JBCabinetryBAOTPrice, JBPrice = @JBPrice, JBReferenceName = @JBReferenceN" &
        "ame, JBJobInsurance = @JBJobInsurance, JBIsCancelled = @JBIsCancelled, JBClientE" &
        "mail = @JBClientEmail, JBInventoryDate = @JBInventoryDate, CTID = @CTID, JBUseCo" &
        "ntact = @JBUseContact WHERE (BRID = @Original_BRID) And (JBID = @Original_JBID) " &
        "And (CTID = @Original_CTID Or @Original_CTID Is NULL And CTID Is NULL) And (ID =" &
        " @Original_ID) And (JBActualFinishDate = @Original_JBActualFinishDate Or @Origin" &
        "al_JBActualFinishDate Is NULL And JBActualFinishDate Is NULL) And (JBCabinetryBA" &
        "OTPrice = @Original_JBCabinetryBAOTPrice Or @Original_JBCabinetryBAOTPrice Is NU" &
        "LL And JBCabinetryBAOTPrice Is NULL) And (JBClientAddress = @Original_JBClientAd" &
        "dress Or @Original_JBClientAddress Is NULL And JBClientAddress Is NULL) And (JBC" &
        "lientEmail = @Original_JBClientEmail Or @Original_JBClientEmail Is NULL And JBCl" &
        "ientEmail Is NULL) And (JBClientFaxNumber = @Original_JBClientFaxNumber Or @Orig" &
        "inal_JBClientFaxNumber Is NULL And JBClientFaxNumber Is NULL) And (JBClientFirst" &
        "Name = @Original_JBClientFirstName Or @Original_JBClientFirstName Is NULL And JB" &
        "ClientFirstName Is NULL) And (JBClientHomePhoneNumber = @Original_JBClientHomePh" &
        "oneNumber Or @Original_JBClientHomePhoneNumber Is NULL And JBClientHomePhoneNumb" &
        "er Is NULL) And (JBClientMobileNumber = @Original_JBClientMobileNumber Or @Origi" &
        "nal_JBClientMobileNumber Is NULL And JBClientMobileNumber Is NULL) And (JBClient" &
        "Name = @Original_JBClientName Or @Original_JBClientName Is NULL And JBClientName" &
        " Is NULL) And (JBClientPostCode = @Original_JBClientPostCode Or @Original_JBClie" &
        "ntPostCode Is NULL And JBClientPostCode Is NULL) And (JBClientState = @Original_" &
        "JBClientState Or @Original_JBClientState Is NULL And JBClientState Is NULL) And " &
        "(JBClientStreetAddress01 = @Original_JBClientStreetAddress01 Or @Original_JBClie" &
        "ntStreetAddress01 Is NULL And JBClientStreetAddress01 Is NULL) And (JBClientStre" &
        "etAddress02 = @Original_JBClientStreetAddress02 Or @Original_JBClientStreetAddre" &
        "ss02 Is NULL And JBClientStreetAddress02 Is NULL) And (JBClientSuburb = @Origina" &
        "l_JBClientSuburb Or @Original_JBClientSuburb Is NULL And JBClientSuburb Is NULL)" &
        " And (JBClientSurname = @Original_JBClientSurname Or @Original_JBClientSurname I" &
        "S NULL And JBClientSurname Is NULL) And (JBClientWorkPhoneNumber = @Original_JBC" &
        "lientWorkPhoneNumber Or @Original_JBClientWorkPhoneNumber Is NULL And JBClientWo" &
        "rkPhoneNumber Is NULL) And (JBGraniteBAOTPrice = @Original_JBGraniteBAOTPrice Or" &
        " @Original_JBGraniteBAOTPrice Is NULL And JBGraniteBAOTPrice Is NULL) And (JBInv" &
        "entoryDate = @Original_JBInventoryDate Or @Original_JBInventoryDate Is NULL And " &
        "JBInventoryDate Is NULL) And (JBIsCancelled = @Original_JBIsCancelled Or @Origin" &
        "al_JBIsCancelled Is NULL And JBIsCancelled Is NULL) And (JBJobAddress = @Origina" &
        "l_JBJobAddress Or @Original_JBJobAddress Is NULL And JBJobAddress Is NULL) And (" &
        "JBJobAddressAsAbove = @Original_JBJobAddressAsAbove Or @Original_JBJobAddressAsA" &
        "bove Is NULL And JBJobAddressAsAbove Is NULL) And (JBJobInsurance = @Original_JB" &
        "JobInsurance Or @Original_JBJobInsurance Is NULL And JBJobInsurance Is NULL) And" &
        " (JBJobPostCode = @Original_JBJobPostCode Or @Original_JBJobPostCode Is NULL And" &
        " JBJobPostCode Is NULL) And (JBJobState = @Original_JBJobState Or @Original_JBJo" &
        "bState Is NULL And JBJobState Is NULL) And (JBJobStreetAddress01 = @Original_JBJ" &
        "obStreetAddress01 Or @Original_JBJobStreetAddress01 Is NULL And JBJobStreetAddre" &
        "ss01 Is NULL) And (JBJobStreetAddress02 = @Original_JBJobStreetAddress02 Or @Ori" &
        "ginal_JBJobStreetAddress02 Is NULL And JBJobStreetAddress02 Is NULL) And (JBJobS" &
        "uburb = @Original_JBJobSuburb Or @Original_JBJobSuburb Is NULL And JBJobSuburb I" &
        "S NULL) And (JBPrice = @Original_JBPrice Or @Original_JBPrice Is NULL And JBPric" &
        "e Is NULL) And (JBReferenceName = @Original_JBReferenceName Or @Original_JBRefer" &
        "enceName Is NULL And JBReferenceName Is NULL) And (JBScheduledFinishDate = @Orig" &
        "inal_JBScheduledFinishDate Or @Original_JBScheduledFinishDate Is NULL And JBSche" &
        "duledFinishDate Is NULL) And (JBScheduledStartDate = @Original_JBScheduledStartD" &
        "ate Or @Original_JBScheduledStartDate Is NULL And JBScheduledStartDate Is NULL) " &
        "And (JBUseContact = @Original_JBUseContact Or @Original_JBUseContact Is NULL And" &
        " JBUseContact Is NULL) And (JTID = @Original_JTID Or @Original_JTID Is NULL And " &
        "JTID Is NULL); SELECT BRID, JBID, ID, JBScheduledStartDate, JBScheduledFinishDat" &
        "e, JBActualFinishDate, JBClientFirstName, JBClientSurname, JBClientName, JBClien" &
        "tStreetAddress01, JBClientStreetAddress02, JBClientSuburb, JBClientState, JBClie" &
        "ntPostCode, JBClientAddress, JBClientHomePhoneNumber, JBClientWorkPhoneNumber, J" &
        "BClientFaxNumber, JBClientMobileNumber, JBJobAddressAsAbove, JBJobStreetAddress0" &
        "1, JBJobStreetAddress02, JBJobSuburb, JBJobState, JBJobPostCode, JBJobAddress, J" &
        "TID, JBGraniteBAOTPrice, JBCabinetryBAOTPrice, JBPrice, JBReferenceName, JBJobIn" &
        "surance, JBIsCancelled, JBClientEmail, JBInventoryDate, CTID, JBUseContact FROM " &
        "Jobs WHERE (BRID = @BRID) And (JBID = @JBID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBScheduledStartDate", System.Data.SqlDbType.DateTime, 8, "JBScheduledStartDate"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBScheduledFinishDate", System.Data.SqlDbType.DateTime, 8, "JBScheduledFinishDate"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBActualFinishDate", System.Data.SqlDbType.DateTime, 8, "JBActualFinishDate"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientFirstName", System.Data.SqlDbType.VarChar, 50, "JBClientFirstName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientSurname", System.Data.SqlDbType.VarChar, 50, "JBClientSurname"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientName", System.Data.SqlDbType.VarChar, 102, "JBClientName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, "JBClientStreetAddress01"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, "JBClientStreetAddress02"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientSuburb", System.Data.SqlDbType.VarChar, 50, "JBClientSuburb"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientState", System.Data.SqlDbType.VarChar, 3, "JBClientState"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientPostCode", System.Data.SqlDbType.VarChar, 20, "JBClientPostCode"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientAddress", System.Data.SqlDbType.VarChar, 259, "JBClientAddress"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, "JBClientHomePhoneNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, "JBClientWorkPhoneNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientFaxNumber", System.Data.SqlDbType.VarChar, 20, "JBClientFaxNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientMobileNumber", System.Data.SqlDbType.VarChar, 20, "JBClientMobileNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, "JBJobAddressAsAbove"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, "JBJobStreetAddress01"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, "JBJobStreetAddress02"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobSuburb", System.Data.SqlDbType.VarChar, 50, "JBJobSuburb"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobState", System.Data.SqlDbType.VarChar, 3, "JBJobState"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobPostCode", System.Data.SqlDbType.VarChar, 20, "JBJobPostCode"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobAddress", System.Data.SqlDbType.VarChar, 259, "JBJobAddress"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JTID", System.Data.SqlDbType.Int, 4, "JTID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBGraniteBAOTPrice", System.Data.SqlDbType.Money, 8, "JBGraniteBAOTPrice"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBCabinetryBAOTPrice", System.Data.SqlDbType.Money, 8, "JBCabinetryBAOTPrice"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBPrice", System.Data.SqlDbType.Money, 8, "JBPrice"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBReferenceName", System.Data.SqlDbType.VarChar, 100, "JBReferenceName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobInsurance", System.Data.SqlDbType.Money, 8, "JBJobInsurance"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBIsCancelled", System.Data.SqlDbType.Bit, 1, "JBIsCancelled"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientEmail", System.Data.SqlDbType.VarChar, 50, "JBClientEmail"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBInventoryDate", System.Data.SqlDbType.DateTime, 8, "JBInventoryDate"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTID", System.Data.SqlDbType.BigInt, 8, "CTID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBUseContact", System.Data.SqlDbType.Bit, 1, "JBUseContact"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBActualFinishDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBActualFinishDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBCabinetryBAOTPrice", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBCabinetryBAOTPrice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientFirstName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientFirstName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientHomePhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientName", System.Data.SqlDbType.VarChar, 102, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientSurname", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientWorkPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBGraniteBAOTPrice", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBGraniteBAOTPrice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBInventoryDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBInventoryDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBIsCancelled", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBIsCancelled", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobAddressAsAbove", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobInsurance", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobInsurance", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBPrice", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBPrice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBReferenceName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBReferenceName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBScheduledFinishDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBScheduledFinishDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBScheduledStartDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBScheduledStartDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBUseContact", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBUseContact", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'pnlMain
        '
        Me.pnlMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlMain.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.pnlMain.Controls.Add(Me.pnlSummary)
        Me.pnlMain.Controls.Add(Me.pnlSalesReps)
        Me.pnlMain.Controls.Add(Me.pnlResponsibilities)
        Me.pnlMain.Controls.Add(Me.pnlOtherTrades)
        Me.pnlMain.Controls.Add(Me.pnlNotes)
        Me.pnlMain.Controls.Add(Me.pnlJobPurchaseOrders)
        Me.pnlMain.Controls.Add(Me.pnlJobIssues)
        Me.pnlMain.Controls.Add(Me.pnlJobInformation)
        Me.pnlMain.Controls.Add(Me.pnlJobAddress)
        Me.pnlMain.Controls.Add(Me.pnlIntro)
        Me.pnlMain.Controls.Add(Me.pnlInternalOrders)
        Me.pnlMain.Controls.Add(Me.pnlDirectLabour)
        Me.pnlMain.Controls.Add(Me.pnlClientPayments)
        Me.pnlMain.Controls.Add(Me.pnlClientInformation)
        Me.pnlMain.Controls.Add(Me.pnlAppliances)
        Me.pnlMain.Location = New System.Drawing.Point(184, 8)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(600, 520)
        Me.pnlMain.TabIndex = 1
        '
        'pnlInternalOrders
        '
        Me.pnlInternalOrders.Controls.Add(Me.btnRemoveInternalOrder)
        Me.pnlInternalOrders.Controls.Add(Me.btnEditInternalOrder)
        Me.pnlInternalOrders.Controls.Add(Me.btnAddInternalOrder)
        Me.pnlInternalOrders.Controls.Add(Me.dgInternalOrders)
        Me.pnlInternalOrders.Location = New System.Drawing.Point(8, 40)
        Me.pnlInternalOrders.Name = "pnlInternalOrders"
        Me.pnlInternalOrders.Size = New System.Drawing.Size(584, 408)
        Me.pnlInternalOrders.TabIndex = 4
        Me.pnlInternalOrders.Text = "Internal Orders"
        '
        'btnRemoveInternalOrder
        '
        Me.btnRemoveInternalOrder.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveInternalOrder.Image = CType(resources.GetObject("btnRemoveInternalOrder.Image"), System.Drawing.Image)
        Me.btnRemoveInternalOrder.Location = New System.Drawing.Point(176, 376)
        Me.btnRemoveInternalOrder.Name = "btnRemoveInternalOrder"
        Me.btnRemoveInternalOrder.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveInternalOrder.TabIndex = 3
        Me.btnRemoveInternalOrder.Text = "Remove"
        '
        'btnEditInternalOrder
        '
        Me.btnEditInternalOrder.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEditInternalOrder.Image = CType(resources.GetObject("btnEditInternalOrder.Image"), System.Drawing.Image)
        Me.btnEditInternalOrder.Location = New System.Drawing.Point(96, 376)
        Me.btnEditInternalOrder.Name = "btnEditInternalOrder"
        Me.btnEditInternalOrder.Size = New System.Drawing.Size(72, 23)
        Me.btnEditInternalOrder.TabIndex = 2
        Me.btnEditInternalOrder.Text = "Edit..."
        '
        'btnAddInternalOrder
        '
        Me.btnAddInternalOrder.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddInternalOrder.Image = CType(resources.GetObject("btnAddInternalOrder.Image"), System.Drawing.Image)
        Me.btnAddInternalOrder.Location = New System.Drawing.Point(16, 376)
        Me.btnAddInternalOrder.Name = "btnAddInternalOrder"
        Me.btnAddInternalOrder.Size = New System.Drawing.Size(72, 23)
        Me.btnAddInternalOrder.TabIndex = 1
        Me.btnAddInternalOrder.Text = "Add..."
        '
        'dgInternalOrders
        '
        Me.dgInternalOrders.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgInternalOrders.DataSource = Me.dvInternalOrders
        '
        'dgInternalOrders.EmbeddedNavigator
        '
        Me.dgInternalOrders.EmbeddedNavigator.Name = ""
        Me.dgInternalOrders.Location = New System.Drawing.Point(16, 32)
        Me.dgInternalOrders.MainView = Me.gvInternalOrders
        Me.dgInternalOrders.Name = "dgInternalOrders"
        Me.dgInternalOrders.ShowOnlyPredefinedDetails = True
        Me.dgInternalOrders.Size = New System.Drawing.Size(552, 336)
        Me.dgInternalOrders.TabIndex = 0
        Me.dgInternalOrders.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvInternalOrders})
        '
        'dvInternalOrders
        '
        Me.dvInternalOrders.RowFilter = "ORType = 'IO'"
        Me.dvInternalOrders.Table = Me.DsOrders.VOrders
        '
        'DsOrders
        '
        Me.DsOrders.DataSetName = "dsOrders"
        Me.DsOrders.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'gvInternalOrders
        '
        Me.gvInternalOrders.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.gvInternalOrders.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvInternalOrders.Appearance.FocusedCell.Options.UseBackColor = True
        Me.gvInternalOrders.Appearance.FocusedCell.Options.UseForeColor = True
        Me.gvInternalOrders.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.gvInternalOrders.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gvInternalOrders.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvInternalOrders.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvInternalOrders.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colOROrderNumber, Me.colORDate, Me.colORDescription, Me.colORStatus1})
        Me.gvInternalOrders.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.gvInternalOrders.GridControl = Me.dgInternalOrders
        Me.gvInternalOrders.Name = "gvInternalOrders"
        Me.gvInternalOrders.OptionsBehavior.Editable = False
        Me.gvInternalOrders.OptionsCustomization.AllowFilter = False
        Me.gvInternalOrders.OptionsNavigation.AutoFocusNewRow = True
        Me.gvInternalOrders.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.gvInternalOrders.OptionsView.ShowGroupPanel = False
        Me.gvInternalOrders.OptionsView.ShowHorzLines = False
        Me.gvInternalOrders.OptionsView.ShowIndicator = False
        Me.gvInternalOrders.OptionsView.ShowVertLines = False
        Me.gvInternalOrders.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colOROrderNumber, DevExpress.Data.ColumnSortOrder.Descending)})
        '
        'colOROrderNumber
        '
        Me.colOROrderNumber.Caption = "Order #"
        Me.colOROrderNumber.FieldName = "OROrderNumber"
        Me.colOROrderNumber.Name = "colOROrderNumber"
        Me.colOROrderNumber.Visible = True
        Me.colOROrderNumber.VisibleIndex = 0
        Me.colOROrderNumber.Width = 51
        '
        'colORDate
        '
        Me.colORDate.Caption = "Date"
        Me.colORDate.FieldName = "ORDate"
        Me.colORDate.Name = "colORDate"
        Me.colORDate.Visible = True
        Me.colORDate.VisibleIndex = 1
        Me.colORDate.Width = 143
        '
        'colORDescription
        '
        Me.colORDescription.Caption = "Description"
        Me.colORDescription.FieldName = "ORDescription"
        Me.colORDescription.Name = "colORDescription"
        Me.colORDescription.Visible = True
        Me.colORDescription.VisibleIndex = 2
        Me.colORDescription.Width = 273
        '
        'colORStatus1
        '
        Me.colORStatus1.Caption = "Status"
        Me.colORStatus1.FieldName = "ORStatus"
        Me.colORStatus1.Name = "colORStatus1"
        Me.colORStatus1.Visible = True
        Me.colORStatus1.VisibleIndex = 3
        Me.colORStatus1.Width = 81
        '
        'pnlJobPurchaseOrders
        '
        Me.pnlJobPurchaseOrders.Controls.Add(Me.btnRemoveJobPurchaseOrder)
        Me.pnlJobPurchaseOrders.Controls.Add(Me.btnEditJobPurchaseOrder)
        Me.pnlJobPurchaseOrders.Controls.Add(Me.btnAddJobPurchaseOrder)
        Me.pnlJobPurchaseOrders.Controls.Add(Me.dgJobPurchaseOrders)
        Me.pnlJobPurchaseOrders.Location = New System.Drawing.Point(96, 40)
        Me.pnlJobPurchaseOrders.Name = "pnlJobPurchaseOrders"
        Me.pnlJobPurchaseOrders.Size = New System.Drawing.Size(472, 408)
        Me.pnlJobPurchaseOrders.TabIndex = 3
        Me.pnlJobPurchaseOrders.Text = "Job Purchase Orders"
        '
        'btnRemoveJobPurchaseOrder
        '
        Me.btnRemoveJobPurchaseOrder.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveJobPurchaseOrder.Image = CType(resources.GetObject("btnRemoveJobPurchaseOrder.Image"), System.Drawing.Image)
        Me.btnRemoveJobPurchaseOrder.Location = New System.Drawing.Point(176, 376)
        Me.btnRemoveJobPurchaseOrder.Name = "btnRemoveJobPurchaseOrder"
        Me.btnRemoveJobPurchaseOrder.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveJobPurchaseOrder.TabIndex = 3
        Me.btnRemoveJobPurchaseOrder.Text = "Remove"
        '
        'btnEditJobPurchaseOrder
        '
        Me.btnEditJobPurchaseOrder.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEditJobPurchaseOrder.Image = CType(resources.GetObject("btnEditJobPurchaseOrder.Image"), System.Drawing.Image)
        Me.btnEditJobPurchaseOrder.Location = New System.Drawing.Point(96, 376)
        Me.btnEditJobPurchaseOrder.Name = "btnEditJobPurchaseOrder"
        Me.btnEditJobPurchaseOrder.Size = New System.Drawing.Size(72, 23)
        Me.btnEditJobPurchaseOrder.TabIndex = 2
        Me.btnEditJobPurchaseOrder.Text = "Edit..."
        '
        'btnAddJobPurchaseOrder
        '
        Me.btnAddJobPurchaseOrder.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddJobPurchaseOrder.Image = CType(resources.GetObject("btnAddJobPurchaseOrder.Image"), System.Drawing.Image)
        Me.btnAddJobPurchaseOrder.Location = New System.Drawing.Point(16, 376)
        Me.btnAddJobPurchaseOrder.Name = "btnAddJobPurchaseOrder"
        Me.btnAddJobPurchaseOrder.Size = New System.Drawing.Size(72, 23)
        Me.btnAddJobPurchaseOrder.TabIndex = 1
        Me.btnAddJobPurchaseOrder.Text = "Add..."
        '
        'dgJobPurchaseOrders
        '
        Me.dgJobPurchaseOrders.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgJobPurchaseOrders.DataSource = Me.dvJobPurchaseOrders
        '
        'dgJobPurchaseOrders.EmbeddedNavigator
        '
        Me.dgJobPurchaseOrders.EmbeddedNavigator.Name = ""
        Me.dgJobPurchaseOrders.Location = New System.Drawing.Point(16, 32)
        Me.dgJobPurchaseOrders.MainView = Me.gvJobPurchaseOrders
        Me.dgJobPurchaseOrders.Name = "dgJobPurchaseOrders"
        Me.dgJobPurchaseOrders.ShowOnlyPredefinedDetails = True
        Me.dgJobPurchaseOrders.Size = New System.Drawing.Size(440, 336)
        Me.dgJobPurchaseOrders.TabIndex = 0
        Me.dgJobPurchaseOrders.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvJobPurchaseOrders})
        '
        'dvJobPurchaseOrders
        '
        Me.dvJobPurchaseOrders.RowFilter = "ORType = 'JPO'"
        Me.dvJobPurchaseOrders.Table = Me.DsOrders.VOrders
        '
        'gvJobPurchaseOrders
        '
        Me.gvJobPurchaseOrders.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.gvJobPurchaseOrders.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvJobPurchaseOrders.Appearance.FocusedCell.Options.UseBackColor = True
        Me.gvJobPurchaseOrders.Appearance.FocusedCell.Options.UseForeColor = True
        Me.gvJobPurchaseOrders.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.gvJobPurchaseOrders.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gvJobPurchaseOrders.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvJobPurchaseOrders.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvJobPurchaseOrders.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn4, Me.GridColumn5, Me.GridColumn6, Me.colORInvoicedCostTotal, Me.colORStatus})
        Me.gvJobPurchaseOrders.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.gvJobPurchaseOrders.GridControl = Me.dgJobPurchaseOrders
        Me.gvJobPurchaseOrders.Name = "gvJobPurchaseOrders"
        Me.gvJobPurchaseOrders.OptionsBehavior.Editable = False
        Me.gvJobPurchaseOrders.OptionsCustomization.AllowFilter = False
        Me.gvJobPurchaseOrders.OptionsNavigation.AutoFocusNewRow = True
        Me.gvJobPurchaseOrders.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.gvJobPurchaseOrders.OptionsView.ShowGroupPanel = False
        Me.gvJobPurchaseOrders.OptionsView.ShowHorzLines = False
        Me.gvJobPurchaseOrders.OptionsView.ShowIndicator = False
        Me.gvJobPurchaseOrders.OptionsView.ShowVertLines = False
        Me.gvJobPurchaseOrders.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn4, DevExpress.Data.ColumnSortOrder.Descending)})
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Order #"
        Me.GridColumn4.FieldName = "OROrderNumber"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 0
        Me.GridColumn4.Width = 38
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Date"
        Me.GridColumn5.FieldName = "ORDate"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 1
        Me.GridColumn5.Width = 106
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Description"
        Me.GridColumn6.FieldName = "ORDescription"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 2
        Me.GridColumn6.Width = 146
        '
        'colORInvoicedCostTotal
        '
        Me.colORInvoicedCostTotal.Caption = "Invoiced Cost"
        Me.colORInvoicedCostTotal.DisplayFormat.FormatString = "c"
        Me.colORInvoicedCostTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colORInvoicedCostTotal.FieldName = "ORInvoicedCostTotal"
        Me.colORInvoicedCostTotal.Name = "colORInvoicedCostTotal"
        Me.colORInvoicedCostTotal.Visible = True
        Me.colORInvoicedCostTotal.VisibleIndex = 3
        Me.colORInvoicedCostTotal.Width = 69
        '
        'colORStatus
        '
        Me.colORStatus.Caption = "Status"
        Me.colORStatus.FieldName = "ORStatus"
        Me.colORStatus.Name = "colORStatus"
        Me.colORStatus.Visible = True
        Me.colORStatus.VisibleIndex = 4
        Me.colORStatus.Width = 77
        '
        'pnlClientInformation
        '
        Me.pnlClientInformation.Controls.Add(Me.rgLDUseContactFalse)
        Me.pnlClientInformation.Controls.Add(Me.Label60)
        Me.pnlClientInformation.Controls.Add(Me.pceContact)
        Me.pnlClientInformation.Controls.Add(Me.rgLDUseContactTrue)
        Me.pnlClientInformation.Controls.Add(Me.Label59)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientWorkPhoneNumber)
        Me.pnlClientInformation.Controls.Add(Me.Label5)
        Me.pnlClientInformation.Controls.Add(Me.Label45)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientEmail)
        Me.pnlClientInformation.Controls.Add(Me.Label13)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientFirstName)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientSurname)
        Me.pnlClientInformation.Controls.Add(Me.GroupLine17)
        Me.pnlClientInformation.Controls.Add(Me.Label1)
        Me.pnlClientInformation.Controls.Add(Me.Label2)
        Me.pnlClientInformation.Controls.Add(Me.Label4)
        Me.pnlClientInformation.Controls.Add(Me.Label6)
        Me.pnlClientInformation.Controls.Add(Me.Label3)
        Me.pnlClientInformation.Controls.Add(Me.Label8)
        Me.pnlClientInformation.Controls.Add(Me.Label9)
        Me.pnlClientInformation.Controls.Add(Me.Label10)
        Me.pnlClientInformation.Controls.Add(Me.GroupLine18)
        Me.pnlClientInformation.Controls.Add(Me.GroupLine19)
        Me.pnlClientInformation.Controls.Add(Me.Label30)
        Me.pnlClientInformation.Controls.Add(Me.txtJBReferenceName)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientStreetAddress01)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientStreetAddress02)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientSuburb)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientState)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientPostCode)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientHomePhoneNumber)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientFaxNumber)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientMobileNumber)
        Me.pnlClientInformation.Location = New System.Drawing.Point(16, 0)
        Me.pnlClientInformation.Name = "pnlClientInformation"
        Me.pnlClientInformation.Size = New System.Drawing.Size(600, 520)
        Me.pnlClientInformation.TabIndex = 0
        Me.pnlClientInformation.Text = "Customer Information"
        '
        'rgLDUseContactFalse
        '
        Me.rgLDUseContactFalse.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBUseContact"))
        Me.rgLDUseContactFalse.Location = New System.Drawing.Point(48, 24)
        Me.rgLDUseContactFalse.Name = "rgLDUseContactFalse"
        '
        'rgLDUseContactFalse.Properties
        '
        Me.rgLDUseContactFalse.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.rgLDUseContactFalse.Properties.Appearance.Options.UseBackColor = True
        Me.rgLDUseContactFalse.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.rgLDUseContactFalse.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "I don't want this customer to be in my list of contacts (eg. for one off customer" & _
        "s)")})
        Me.rgLDUseContactFalse.Size = New System.Drawing.Size(496, 24)
        Me.rgLDUseContactFalse.TabIndex = 0
        '
        'Label60
        '
        Me.Label60.Location = New System.Drawing.Point(80, 72)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(288, 21)
        Me.Label60.TabIndex = 27
        Me.Label60.Text = "Select an existing contact or create a new contact:"
        Me.Label60.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'pceContact
        '
        Me.pceContact.Location = New System.Drawing.Point(80, 96)
        Me.pceContact.Name = "pceContact"
        '
        'pceContact.Properties
        '
        Me.pceContact.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.pceContact.Properties.CloseOnLostFocus = False
        Me.pceContact.Properties.NullText = "<No contact is selected>"
        Me.pceContact.Properties.PopupControl = Me.pccContacts
        Me.pceContact.Properties.ShowPopupShadow = False
        Me.pceContact.Size = New System.Drawing.Size(464, 20)
        Me.pceContact.TabIndex = 2
        '
        'pccContacts
        '
        Me.pccContacts.Location = New System.Drawing.Point(168, 224)
        Me.pccContacts.Name = "pccContacts"
        Me.pccContacts.Size = New System.Drawing.Size(464, 336)
        Me.pccContacts.TabIndex = 7
        Me.pccContacts.Text = "PopupContainerControl1"
        '
        'rgLDUseContactTrue
        '
        Me.rgLDUseContactTrue.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBUseContact"))
        Me.rgLDUseContactTrue.Location = New System.Drawing.Point(48, 48)
        Me.rgLDUseContactTrue.Name = "rgLDUseContactTrue"
        '
        'rgLDUseContactTrue.Properties
        '
        Me.rgLDUseContactTrue.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.rgLDUseContactTrue.Properties.Appearance.Options.UseBackColor = True
        Me.rgLDUseContactTrue.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.rgLDUseContactTrue.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "I want this customer to be in my list of contacts (eg. for repeat customers)")})
        Me.rgLDUseContactTrue.Size = New System.Drawing.Size(496, 24)
        Me.rgLDUseContactTrue.TabIndex = 1
        '
        'Label59
        '
        Me.Label59.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.Location = New System.Drawing.Point(104, 416)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(80, 21)
        Me.Label59.TabIndex = 24
        Me.Label59.Text = "Work phone:"
        Me.Label59.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJBClientWorkPhoneNumber
        '
        Me.txtJBClientWorkPhoneNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientWorkPhoneNumber"))
        Me.txtJBClientWorkPhoneNumber.EditValue = ""
        Me.txtJBClientWorkPhoneNumber.Location = New System.Drawing.Point(248, 416)
        Me.txtJBClientWorkPhoneNumber.Name = "txtJBClientWorkPhoneNumber"
        Me.txtJBClientWorkPhoneNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientWorkPhoneNumber.TabIndex = 12
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(360, 336)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(96, 21)
        Me.Label5.TabIndex = 23
        Me.Label5.Text = "ZIP/postal code:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label45
        '
        Me.Label45.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.Location = New System.Drawing.Point(104, 488)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(80, 21)
        Me.Label45.TabIndex = 8
        Me.Label45.Text = "Email:"
        Me.Label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJBClientEmail
        '
        Me.txtJBClientEmail.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientEmail"))
        Me.txtJBClientEmail.EditValue = ""
        Me.txtJBClientEmail.Location = New System.Drawing.Point(248, 488)
        Me.txtJBClientEmail.Name = "txtJBClientEmail"
        Me.txtJBClientEmail.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientEmail.TabIndex = 15
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label13.Location = New System.Drawing.Point(544, 160)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(24, 16)
        Me.Label13.TabIndex = 7
        Me.Label13.Text = "*"
        '
        'txtJBClientFirstName
        '
        Me.txtJBClientFirstName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientFirstName"))
        Me.txtJBClientFirstName.EditValue = ""
        Me.txtJBClientFirstName.Location = New System.Drawing.Point(248, 184)
        Me.txtJBClientFirstName.Name = "txtJBClientFirstName"
        Me.txtJBClientFirstName.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientFirstName.TabIndex = 4
        '
        'txtJBClientSurname
        '
        Me.txtJBClientSurname.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientSurname"))
        Me.txtJBClientSurname.EditValue = ""
        Me.txtJBClientSurname.Location = New System.Drawing.Point(248, 160)
        Me.txtJBClientSurname.Name = "txtJBClientSurname"
        '
        'txtJBClientSurname.Properties
        '
        Me.txtJBClientSurname.Properties.NullText = "<Incomplete>"
        Me.txtJBClientSurname.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientSurname.TabIndex = 3
        '
        'GroupLine17
        '
        Me.GroupLine17.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupLine17.Location = New System.Drawing.Point(80, 240)
        Me.GroupLine17.Name = "GroupLine17"
        Me.GroupLine17.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine17.TabIndex = 3
        Me.GroupLine17.TextString = "Address"
        Me.GroupLine17.TextWidth = 50
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(104, 184)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 21)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "First name:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(104, 160)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(144, 21)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Surname / business name:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(104, 336)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 21)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "State/region:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(104, 264)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 21)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Street address:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(104, 312)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 21)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Suburb/town:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(104, 392)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(80, 21)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Home phone:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(104, 440)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(80, 21)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "Fax:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(104, 464)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(80, 21)
        Me.Label10.TabIndex = 1
        Me.Label10.Text = "Mobile:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine18
        '
        Me.GroupLine18.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupLine18.Location = New System.Drawing.Point(80, 368)
        Me.GroupLine18.Name = "GroupLine18"
        Me.GroupLine18.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine18.TabIndex = 3
        Me.GroupLine18.TextString = "Contact Details"
        Me.GroupLine18.TextWidth = 79
        '
        'GroupLine19
        '
        Me.GroupLine19.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupLine19.Location = New System.Drawing.Point(80, 136)
        Me.GroupLine19.Name = "GroupLine19"
        Me.GroupLine19.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine19.TabIndex = 3
        Me.GroupLine19.TextString = "Name"
        Me.GroupLine19.TextWidth = 35
        '
        'Label30
        '
        Me.Label30.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(104, 208)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(96, 21)
        Me.Label30.TabIndex = 1
        Me.Label30.Text = "Reference name:"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJBReferenceName
        '
        Me.txtJBReferenceName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBReferenceName"))
        Me.txtJBReferenceName.EditValue = ""
        Me.txtJBReferenceName.Location = New System.Drawing.Point(248, 208)
        Me.txtJBReferenceName.Name = "txtJBReferenceName"
        Me.txtJBReferenceName.Size = New System.Drawing.Size(296, 20)
        Me.txtJBReferenceName.TabIndex = 5
        '
        'txtJBClientStreetAddress01
        '
        Me.txtJBClientStreetAddress01.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientStreetAddress01"))
        Me.txtJBClientStreetAddress01.EditValue = ""
        Me.txtJBClientStreetAddress01.Location = New System.Drawing.Point(248, 264)
        Me.txtJBClientStreetAddress01.Name = "txtJBClientStreetAddress01"
        Me.txtJBClientStreetAddress01.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientStreetAddress01.TabIndex = 6
        '
        'txtJBClientStreetAddress02
        '
        Me.txtJBClientStreetAddress02.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientStreetAddress02"))
        Me.txtJBClientStreetAddress02.EditValue = ""
        Me.txtJBClientStreetAddress02.Location = New System.Drawing.Point(248, 288)
        Me.txtJBClientStreetAddress02.Name = "txtJBClientStreetAddress02"
        Me.txtJBClientStreetAddress02.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientStreetAddress02.TabIndex = 7
        '
        'txtJBClientSuburb
        '
        Me.txtJBClientSuburb.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientSuburb"))
        Me.txtJBClientSuburb.EditValue = ""
        Me.txtJBClientSuburb.Location = New System.Drawing.Point(248, 312)
        Me.txtJBClientSuburb.Name = "txtJBClientSuburb"
        Me.txtJBClientSuburb.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientSuburb.TabIndex = 8
        '
        'txtJBClientState
        '
        Me.txtJBClientState.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientState"))
        Me.txtJBClientState.EditValue = ""
        Me.txtJBClientState.Location = New System.Drawing.Point(248, 336)
        Me.txtJBClientState.Name = "txtJBClientState"
        Me.txtJBClientState.Size = New System.Drawing.Size(88, 20)
        Me.txtJBClientState.TabIndex = 9
        '
        'txtJBClientPostCode
        '
        Me.txtJBClientPostCode.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientPostCode"))
        Me.txtJBClientPostCode.EditValue = ""
        Me.txtJBClientPostCode.Location = New System.Drawing.Point(456, 336)
        Me.txtJBClientPostCode.Name = "txtJBClientPostCode"
        Me.txtJBClientPostCode.Size = New System.Drawing.Size(88, 20)
        Me.txtJBClientPostCode.TabIndex = 10
        '
        'txtJBClientHomePhoneNumber
        '
        Me.txtJBClientHomePhoneNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientHomePhoneNumber"))
        Me.txtJBClientHomePhoneNumber.EditValue = ""
        Me.txtJBClientHomePhoneNumber.Location = New System.Drawing.Point(248, 392)
        Me.txtJBClientHomePhoneNumber.Name = "txtJBClientHomePhoneNumber"
        Me.txtJBClientHomePhoneNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientHomePhoneNumber.TabIndex = 11
        '
        'txtJBClientFaxNumber
        '
        Me.txtJBClientFaxNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientFaxNumber"))
        Me.txtJBClientFaxNumber.EditValue = ""
        Me.txtJBClientFaxNumber.Location = New System.Drawing.Point(248, 440)
        Me.txtJBClientFaxNumber.Name = "txtJBClientFaxNumber"
        Me.txtJBClientFaxNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientFaxNumber.TabIndex = 13
        '
        'txtJBClientMobileNumber
        '
        Me.txtJBClientMobileNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBClientMobileNumber"))
        Me.txtJBClientMobileNumber.EditValue = ""
        Me.txtJBClientMobileNumber.Location = New System.Drawing.Point(248, 464)
        Me.txtJBClientMobileNumber.Name = "txtJBClientMobileNumber"
        Me.txtJBClientMobileNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientMobileNumber.TabIndex = 14
        '
        'pnlJobInformation
        '
        Me.pnlJobInformation.Controls.Add(Me.dpJBScheduledStartDate)
        Me.pnlJobInformation.Controls.Add(Me.dpJBScheduledFinishDate)
        Me.pnlJobInformation.Controls.Add(Me.txtJBJobInsurance)
        Me.pnlJobInformation.Controls.Add(Me.Label46)
        Me.pnlJobInformation.Controls.Add(Me.Label40)
        Me.pnlJobInformation.Controls.Add(Me.Label14)
        Me.pnlJobInformation.Controls.Add(Me.txtJBJobType)
        Me.pnlJobInformation.Controls.Add(Me.dpJBActualFinishDate)
        Me.pnlJobInformation.Controls.Add(Me.txtJBCabinetryBAOTPrice)
        Me.pnlJobInformation.Controls.Add(Me.txtJBGraniteBAOTPrice)
        Me.pnlJobInformation.Controls.Add(Me.txtJBPrice)
        Me.pnlJobInformation.Controls.Add(Me.GroupLine14)
        Me.pnlJobInformation.Controls.Add(Me.Label7)
        Me.pnlJobInformation.Controls.Add(Me.Label11)
        Me.pnlJobInformation.Controls.Add(Me.Label12)
        Me.pnlJobInformation.Controls.Add(Me.Label18)
        Me.pnlJobInformation.Controls.Add(Me.Label19)
        Me.pnlJobInformation.Controls.Add(Me.Label20)
        Me.pnlJobInformation.Controls.Add(Me.Label21)
        Me.pnlJobInformation.Controls.Add(Me.GroupLine15)
        Me.pnlJobInformation.Controls.Add(Me.GroupLine16)
        Me.pnlJobInformation.Controls.Add(Me.Label31)
        Me.pnlJobInformation.Controls.Add(Me.Label32)
        Me.pnlJobInformation.Controls.Add(Me.Label33)
        Me.pnlJobInformation.Controls.Add(Me.Label15)
        Me.pnlJobInformation.Controls.Add(Me.Label16)
        Me.pnlJobInformation.Controls.Add(Me.Label36)
        Me.pnlJobInformation.Controls.Add(Me.Label38)
        Me.pnlJobInformation.Controls.Add(Me.Label39)
        Me.pnlJobInformation.Controls.Add(Me.GroupLine20)
        Me.pnlJobInformation.Controls.Add(Me.Label47)
        Me.pnlJobInformation.Location = New System.Drawing.Point(32, 0)
        Me.pnlJobInformation.Name = "pnlJobInformation"
        Me.pnlJobInformation.Size = New System.Drawing.Size(600, 488)
        Me.pnlJobInformation.TabIndex = 1
        Me.pnlJobInformation.Text = "Job Information"
        '
        'dpJBScheduledStartDate
        '
        Me.dpJBScheduledStartDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBScheduledStartDate"))
        Me.dpJBScheduledStartDate.EditValue = New Date(2005, 8, 19, 0, 0, 0, 0)
        Me.dpJBScheduledStartDate.Enabled = False
        Me.dpJBScheduledStartDate.Location = New System.Drawing.Point(200, 88)
        Me.dpJBScheduledStartDate.Name = "dpJBScheduledStartDate"
        '
        'dpJBScheduledStartDate.Properties
        '
        Me.dpJBScheduledStartDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpJBScheduledStartDate.Properties.NullText = "<Incomplete>"
        Me.dpJBScheduledStartDate.Size = New System.Drawing.Size(184, 20)
        Me.dpJBScheduledStartDate.TabIndex = 0
        '
        'dpJBScheduledFinishDate
        '
        Me.dpJBScheduledFinishDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBScheduledFinishDate"))
        Me.dpJBScheduledFinishDate.EditValue = New Date(2005, 8, 19, 0, 0, 0, 0)
        Me.dpJBScheduledFinishDate.Enabled = False
        Me.dpJBScheduledFinishDate.Location = New System.Drawing.Point(200, 120)
        Me.dpJBScheduledFinishDate.Name = "dpJBScheduledFinishDate"
        '
        'dpJBScheduledFinishDate.Properties
        '
        Me.dpJBScheduledFinishDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpJBScheduledFinishDate.Properties.NullText = "<Incomplete>"
        Me.dpJBScheduledFinishDate.Size = New System.Drawing.Size(184, 20)
        Me.dpJBScheduledFinishDate.TabIndex = 1
        '
        'txtJBJobInsurance
        '
        Me.txtJBJobInsurance.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBJobInsurance"))
        Me.txtJBJobInsurance.EditValue = ""
        Me.txtJBJobInsurance.Location = New System.Drawing.Point(200, 272)
        Me.txtJBJobInsurance.Name = "txtJBJobInsurance"
        '
        'txtJBJobInsurance.Properties
        '
        Me.txtJBJobInsurance.Properties.Appearance.Options.UseTextOptions = True
        Me.txtJBJobInsurance.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtJBJobInsurance.Properties.DisplayFormat.FormatString = "c"
        Me.txtJBJobInsurance.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtJBJobInsurance.Properties.EditFormat.FormatString = "c"
        Me.txtJBJobInsurance.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtJBJobInsurance.Properties.NullText = "<Incomplete>"
        Me.txtJBJobInsurance.Size = New System.Drawing.Size(184, 20)
        Me.txtJBJobInsurance.TabIndex = 4
        '
        'Label46
        '
        Me.Label46.Location = New System.Drawing.Point(40, 272)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(112, 21)
        Me.Label46.TabIndex = 32
        Me.Label46.Text = "Job insurance:"
        Me.Label46.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label40
        '
        Me.Label40.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label40.Location = New System.Drawing.Point(384, 440)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(24, 16)
        Me.Label40.TabIndex = 31
        Me.Label40.Text = "*"
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label14.Location = New System.Drawing.Point(384, 88)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(24, 16)
        Me.Label14.TabIndex = 30
        Me.Label14.Text = "*"
        '
        'txtJBJobType
        '
        Me.txtJBJobType.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JTID"))
        Me.txtJBJobType.Location = New System.Drawing.Point(200, 224)
        Me.txtJBJobType.Name = "txtJBJobType"
        '
        'txtJBJobType.Properties
        '
        Me.txtJBJobType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtJBJobType.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("JTName")})
        Me.txtJBJobType.Properties.DataSource = Me.dataSet.JobTypes
        Me.txtJBJobType.Properties.DisplayMember = "JTName"
        Me.txtJBJobType.Properties.NullText = "<Incomplete>"
        Me.txtJBJobType.Properties.ShowFooter = False
        Me.txtJBJobType.Properties.ShowHeader = False
        Me.txtJBJobType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.txtJBJobType.Properties.ValueMember = "JTID"
        Me.txtJBJobType.Size = New System.Drawing.Size(184, 20)
        Me.txtJBJobType.TabIndex = 3
        '
        'dpJBActualFinishDate
        '
        Me.dpJBActualFinishDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBActualFinishDate"))
        Me.dpJBActualFinishDate.EditValue = New Date(2005, 8, 19, 0, 0, 0, 0)
        Me.dpJBActualFinishDate.Location = New System.Drawing.Point(200, 152)
        Me.dpJBActualFinishDate.Name = "dpJBActualFinishDate"
        '
        'dpJBActualFinishDate.Properties
        '
        Me.dpJBActualFinishDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpJBActualFinishDate.Properties.NullText = "<Incomplete>"
        Me.dpJBActualFinishDate.Size = New System.Drawing.Size(184, 20)
        Me.dpJBActualFinishDate.TabIndex = 2
        '
        'txtJBCabinetryBAOTPrice
        '
        Me.txtJBCabinetryBAOTPrice.EditValue = ""
        Me.txtJBCabinetryBAOTPrice.Location = New System.Drawing.Point(200, 440)
        Me.txtJBCabinetryBAOTPrice.Name = "txtJBCabinetryBAOTPrice"
        '
        'txtJBCabinetryBAOTPrice.Properties
        '
        Me.txtJBCabinetryBAOTPrice.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.txtJBCabinetryBAOTPrice.Properties.Appearance.Options.UseBackColor = True
        Me.txtJBCabinetryBAOTPrice.Properties.Appearance.Options.UseTextOptions = True
        Me.txtJBCabinetryBAOTPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtJBCabinetryBAOTPrice.Properties.DisplayFormat.FormatString = "c"
        Me.txtJBCabinetryBAOTPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtJBCabinetryBAOTPrice.Properties.EditFormat.FormatString = "c"
        Me.txtJBCabinetryBAOTPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtJBCabinetryBAOTPrice.Properties.NullText = "<Incomplete>"
        Me.txtJBCabinetryBAOTPrice.Properties.ReadOnly = True
        Me.txtJBCabinetryBAOTPrice.Size = New System.Drawing.Size(184, 20)
        Me.txtJBCabinetryBAOTPrice.TabIndex = 7
        '
        'txtJBGraniteBAOTPrice
        '
        Me.txtJBGraniteBAOTPrice.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBGraniteBAOTPrice"))
        Me.txtJBGraniteBAOTPrice.EditValue = ""
        Me.txtJBGraniteBAOTPrice.Location = New System.Drawing.Point(200, 408)
        Me.txtJBGraniteBAOTPrice.Name = "txtJBGraniteBAOTPrice"
        '
        'txtJBGraniteBAOTPrice.Properties
        '
        Me.txtJBGraniteBAOTPrice.Properties.Appearance.Options.UseTextOptions = True
        Me.txtJBGraniteBAOTPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtJBGraniteBAOTPrice.Properties.DisplayFormat.FormatString = "c"
        Me.txtJBGraniteBAOTPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtJBGraniteBAOTPrice.Properties.EditFormat.FormatString = "c"
        Me.txtJBGraniteBAOTPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtJBGraniteBAOTPrice.Properties.NullText = "<Incomplete>"
        Me.txtJBGraniteBAOTPrice.Size = New System.Drawing.Size(184, 20)
        Me.txtJBGraniteBAOTPrice.TabIndex = 6
        '
        'txtJBPrice
        '
        Me.txtJBPrice.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBPrice"))
        Me.txtJBPrice.EditValue = ""
        Me.txtJBPrice.Location = New System.Drawing.Point(200, 376)
        Me.txtJBPrice.Name = "txtJBPrice"
        '
        'txtJBPrice.Properties
        '
        Me.txtJBPrice.Properties.Appearance.Options.UseTextOptions = True
        Me.txtJBPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtJBPrice.Properties.DisplayFormat.FormatString = "c"
        Me.txtJBPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtJBPrice.Properties.EditFormat.FormatString = "c"
        Me.txtJBPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtJBPrice.Properties.NullText = "<Incomplete>"
        Me.txtJBPrice.Size = New System.Drawing.Size(184, 20)
        Me.txtJBPrice.TabIndex = 5
        '
        'GroupLine14
        '
        Me.GroupLine14.Location = New System.Drawing.Point(16, 24)
        Me.GroupLine14.Name = "GroupLine14"
        Me.GroupLine14.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine14.TabIndex = 19
        Me.GroupLine14.TextString = "Job Date"
        Me.GroupLine14.TextWidth = 50
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(40, 40)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(440, 48)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "The scheduled start and finish date are pre-determined in the scheduling section " & _
        "of the ""Bookings"" stage.  The actual finish date is the date that the job is fin" & _
        "ished and ready for final payment."
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(40, 376)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(104, 21)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "Job price (BAOT):"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(40, 408)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(136, 21)
        Me.Label12.TabIndex = 1
        Me.Label12.Text = "Trend portion of job price:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label18
        '
        Me.Label18.Location = New System.Drawing.Point(40, 200)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(440, 16)
        Me.Label18.TabIndex = 1
        Me.Label18.Text = "Job type is set by head office. It will be used to categorize jobs for reporting " & _
        "purposes. "
        '
        'Label19
        '
        Me.Label19.Location = New System.Drawing.Point(40, 224)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(112, 21)
        Me.Label19.TabIndex = 1
        Me.Label19.Text = "Select the job type:"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label20
        '
        Me.Label20.Location = New System.Drawing.Point(40, 88)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(152, 21)
        Me.Label20.TabIndex = 1
        Me.Label20.Text = "Scheduled install start date:"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label21
        '
        Me.Label21.Location = New System.Drawing.Point(40, 320)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(440, 48)
        Me.Label21.TabIndex = 1
        Me.Label21.Text = "BAOT is the job price Before Appliances and Other Trades. This price is broken do" & _
        "wn into the Trend portion and the non-Trend portion.  You need only enter the jo" & _
        "b price (BAOT) and the Trend portion."
        '
        'GroupLine15
        '
        Me.GroupLine15.Location = New System.Drawing.Point(16, 184)
        Me.GroupLine15.Name = "GroupLine15"
        Me.GroupLine15.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine15.TabIndex = 19
        Me.GroupLine15.TextString = "Job Type"
        Me.GroupLine15.TextWidth = 50
        '
        'GroupLine16
        '
        Me.GroupLine16.Location = New System.Drawing.Point(16, 304)
        Me.GroupLine16.Name = "GroupLine16"
        Me.GroupLine16.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine16.TabIndex = 19
        Me.GroupLine16.TextString = "Job Price (BAOT)"
        Me.GroupLine16.TextWidth = 100
        '
        'Label31
        '
        Me.Label31.Location = New System.Drawing.Point(40, 120)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(152, 21)
        Me.Label31.TabIndex = 1
        Me.Label31.Text = "Scheduled install finish date:"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label32
        '
        Me.Label32.Location = New System.Drawing.Point(40, 152)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(152, 21)
        Me.Label32.TabIndex = 1
        Me.Label32.Text = "Actual install finish date:"
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label33
        '
        Me.Label33.Location = New System.Drawing.Point(40, 440)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(168, 21)
        Me.Label33.TabIndex = 1
        Me.Label33.Text = "Non-Trend portion of job price:"
        Me.Label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label15
        '
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label15.Location = New System.Drawing.Point(384, 120)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(24, 16)
        Me.Label15.TabIndex = 30
        Me.Label15.Text = "*"
        '
        'Label16
        '
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label16.Location = New System.Drawing.Point(384, 152)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(24, 16)
        Me.Label16.TabIndex = 30
        Me.Label16.Text = "*"
        '
        'Label36
        '
        Me.Label36.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label36.Location = New System.Drawing.Point(384, 224)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(24, 16)
        Me.Label36.TabIndex = 30
        Me.Label36.Text = "*"
        '
        'Label38
        '
        Me.Label38.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label38.Location = New System.Drawing.Point(384, 376)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(24, 16)
        Me.Label38.TabIndex = 30
        Me.Label38.Text = "*"
        '
        'Label39
        '
        Me.Label39.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label39.Location = New System.Drawing.Point(384, 408)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(24, 16)
        Me.Label39.TabIndex = 30
        Me.Label39.Text = "*"
        '
        'GroupLine20
        '
        Me.GroupLine20.Location = New System.Drawing.Point(16, 256)
        Me.GroupLine20.Name = "GroupLine20"
        Me.GroupLine20.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine20.TabIndex = 19
        Me.GroupLine20.TextString = "Job Insurance"
        Me.GroupLine20.TextWidth = 80
        '
        'Label47
        '
        Me.Label47.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label47.Location = New System.Drawing.Point(384, 272)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(24, 16)
        Me.Label47.TabIndex = 30
        Me.Label47.Text = "*"
        '
        'pnlClientPayments
        '
        Me.pnlClientPayments.Controls.Add(Me.Label41)
        Me.pnlClientPayments.Controls.Add(Me.dgClientPayments)
        Me.pnlClientPayments.Controls.Add(Me.btnRemoveClientPayment)
        Me.pnlClientPayments.Controls.Add(Me.dgDiscounts)
        Me.pnlClientPayments.Controls.Add(Me.Label35)
        Me.pnlClientPayments.Controls.Add(Me.btnRemoveBadDebt)
        Me.pnlClientPayments.Controls.Add(Me.dgBadDebts)
        Me.pnlClientPayments.Controls.Add(Me.btnRemoveDiscount)
        Me.pnlClientPayments.Controls.Add(Me.Label34)
        Me.pnlClientPayments.Location = New System.Drawing.Point(40, 0)
        Me.pnlClientPayments.Name = "pnlClientPayments"
        Me.pnlClientPayments.Size = New System.Drawing.Size(560, 480)
        Me.pnlClientPayments.TabIndex = 7
        Me.pnlClientPayments.Text = "Customer Payments"
        '
        'Label41
        '
        Me.Label41.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.Location = New System.Drawing.Point(16, 24)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(240, 23)
        Me.Label41.TabIndex = 11
        Me.Label41.Text = "Payments made by customer:"
        Me.Label41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dgClientPayments
        '
        Me.dgClientPayments.DataSource = Me.dvClientPayments
        '
        'dgClientPayments.EmbeddedNavigator
        '
        Me.dgClientPayments.EmbeddedNavigator.Name = ""
        Me.dgClientPayments.ExternalRepository = Me.PersistentRepository1
        Me.dgClientPayments.Location = New System.Drawing.Point(16, 48)
        Me.dgClientPayments.MainView = Me.gvClientPayments
        Me.dgClientPayments.Name = "dgClientPayments"
        Me.dgClientPayments.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtCurrencyClientPayments})
        Me.dgClientPayments.Size = New System.Drawing.Size(528, 88)
        Me.dgClientPayments.TabIndex = 0
        Me.dgClientPayments.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvClientPayments})
        '
        'dvClientPayments
        '
        Me.dvClientPayments.RowFilter = "CPType = 'CP'"
        Me.dvClientPayments.Table = Me.dataSet.ClientPayments
        '
        'PersistentRepository1
        '
        Me.PersistentRepository1.Items.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtDate, Me.RepositoryItemComboBox1})
        '
        'txtDate
        '
        Me.txtDate.AutoHeight = False
        Me.txtDate.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtDate.Name = "txtDate"
        '
        'RepositoryItemComboBox1
        '
        Me.RepositoryItemComboBox1.AutoHeight = False
        Me.RepositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox1.Name = "RepositoryItemComboBox1"
        '
        'gvClientPayments
        '
        Me.gvClientPayments.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvClientPayments.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvClientPayments.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvClientPayments.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvClientPayments.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvClientPayments.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvClientPayments.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvClientPayments.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvClientPayments.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvClientPayments.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvClientPayments.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvClientPayments.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvClientPayments.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvClientPayments.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvClientPayments.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvClientPayments.Appearance.VertLine.Options.UseBackColor = True
        Me.gvClientPayments.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCPDate, Me.colCPAmount, Me.colCPNotes})
        Me.gvClientPayments.GridControl = Me.dgClientPayments
        Me.gvClientPayments.Name = "gvClientPayments"
        Me.gvClientPayments.OptionsCustomization.AllowFilter = False
        Me.gvClientPayments.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvClientPayments.OptionsView.ShowGroupPanel = False
        '
        'colCPDate
        '
        Me.colCPDate.Caption = "Date"
        Me.colCPDate.ColumnEdit = Me.txtDate
        Me.colCPDate.FieldName = "CPDate"
        Me.colCPDate.Name = "colCPDate"
        Me.colCPDate.Visible = True
        Me.colCPDate.VisibleIndex = 0
        Me.colCPDate.Width = 187
        '
        'colCPAmount
        '
        Me.colCPAmount.Caption = "Amount *"
        Me.colCPAmount.ColumnEdit = Me.txtCurrencyClientPayments
        Me.colCPAmount.FieldName = "CPAmount"
        Me.colCPAmount.Name = "colCPAmount"
        Me.colCPAmount.Visible = True
        Me.colCPAmount.VisibleIndex = 2
        Me.colCPAmount.Width = 161
        '
        'txtCurrencyClientPayments
        '
        Me.txtCurrencyClientPayments.AutoHeight = False
        Me.txtCurrencyClientPayments.DisplayFormat.FormatString = "c"
        Me.txtCurrencyClientPayments.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCurrencyClientPayments.EditFormat.FormatString = "c"
        Me.txtCurrencyClientPayments.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCurrencyClientPayments.Name = "txtCurrencyClientPayments"
        '
        'colCPNotes
        '
        Me.colCPNotes.Caption = "Notes"
        Me.colCPNotes.FieldName = "CPNotes"
        Me.colCPNotes.Name = "colCPNotes"
        Me.colCPNotes.Visible = True
        Me.colCPNotes.VisibleIndex = 1
        Me.colCPNotes.Width = 477
        '
        'btnRemoveClientPayment
        '
        Me.btnRemoveClientPayment.Image = CType(resources.GetObject("btnRemoveClientPayment.Image"), System.Drawing.Image)
        Me.btnRemoveClientPayment.Location = New System.Drawing.Point(16, 144)
        Me.btnRemoveClientPayment.Name = "btnRemoveClientPayment"
        Me.btnRemoveClientPayment.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveClientPayment.TabIndex = 1
        Me.btnRemoveClientPayment.Text = "Remove"
        '
        'dgDiscounts
        '
        Me.dgDiscounts.DataSource = Me.dvDiscounts
        '
        'dgDiscounts.EmbeddedNavigator
        '
        Me.dgDiscounts.EmbeddedNavigator.Name = ""
        Me.dgDiscounts.ExternalRepository = Me.PersistentRepository1
        Me.dgDiscounts.Location = New System.Drawing.Point(16, 200)
        Me.dgDiscounts.MainView = Me.gvDiscounts
        Me.dgDiscounts.Name = "dgDiscounts"
        Me.dgDiscounts.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtCurrencyDiscounts})
        Me.dgDiscounts.Size = New System.Drawing.Size(528, 88)
        Me.dgDiscounts.TabIndex = 2
        Me.dgDiscounts.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvDiscounts})
        '
        'dvDiscounts
        '
        Me.dvDiscounts.RowFilter = "CPType = 'DC'"
        Me.dvDiscounts.Table = Me.dataSet.ClientPayments
        '
        'gvDiscounts
        '
        Me.gvDiscounts.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvDiscounts.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvDiscounts.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvDiscounts.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvDiscounts.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvDiscounts.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvDiscounts.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvDiscounts.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvDiscounts.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvDiscounts.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvDiscounts.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvDiscounts.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvDiscounts.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvDiscounts.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvDiscounts.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvDiscounts.Appearance.VertLine.Options.UseBackColor = True
        Me.gvDiscounts.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCPDate2, Me.colCPAmount2, Me.colCPNotes2})
        Me.gvDiscounts.GridControl = Me.dgDiscounts
        Me.gvDiscounts.Name = "gvDiscounts"
        Me.gvDiscounts.OptionsCustomization.AllowFilter = False
        Me.gvDiscounts.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvDiscounts.OptionsView.ShowGroupPanel = False
        '
        'colCPDate2
        '
        Me.colCPDate2.Caption = "Date"
        Me.colCPDate2.ColumnEdit = Me.txtDate
        Me.colCPDate2.FieldName = "CPDate"
        Me.colCPDate2.Name = "colCPDate2"
        Me.colCPDate2.Visible = True
        Me.colCPDate2.VisibleIndex = 0
        Me.colCPDate2.Width = 187
        '
        'colCPAmount2
        '
        Me.colCPAmount2.Caption = "Amount *"
        Me.colCPAmount2.ColumnEdit = Me.txtCurrencyDiscounts
        Me.colCPAmount2.FieldName = "CPAmount"
        Me.colCPAmount2.Name = "colCPAmount2"
        Me.colCPAmount2.Visible = True
        Me.colCPAmount2.VisibleIndex = 2
        Me.colCPAmount2.Width = 161
        '
        'txtCurrencyDiscounts
        '
        Me.txtCurrencyDiscounts.AutoHeight = False
        Me.txtCurrencyDiscounts.DisplayFormat.FormatString = "c"
        Me.txtCurrencyDiscounts.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCurrencyDiscounts.EditFormat.FormatString = "c"
        Me.txtCurrencyDiscounts.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCurrencyDiscounts.Name = "txtCurrencyDiscounts"
        '
        'colCPNotes2
        '
        Me.colCPNotes2.Caption = "Notes"
        Me.colCPNotes2.FieldName = "CPNotes"
        Me.colCPNotes2.Name = "colCPNotes2"
        Me.colCPNotes2.Visible = True
        Me.colCPNotes2.VisibleIndex = 1
        Me.colCPNotes2.Width = 477
        '
        'Label35
        '
        Me.Label35.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Location = New System.Drawing.Point(16, 176)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(240, 23)
        Me.Label35.TabIndex = 5
        Me.Label35.Text = "Discounts given to customer:"
        Me.Label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnRemoveBadDebt
        '
        Me.btnRemoveBadDebt.Image = CType(resources.GetObject("btnRemoveBadDebt.Image"), System.Drawing.Image)
        Me.btnRemoveBadDebt.Location = New System.Drawing.Point(16, 448)
        Me.btnRemoveBadDebt.Name = "btnRemoveBadDebt"
        Me.btnRemoveBadDebt.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveBadDebt.TabIndex = 5
        Me.btnRemoveBadDebt.Text = "Remove"
        '
        'dgBadDebts
        '
        Me.dgBadDebts.DataSource = Me.dvBadDebts
        '
        'dgBadDebts.EmbeddedNavigator
        '
        Me.dgBadDebts.EmbeddedNavigator.Name = ""
        Me.dgBadDebts.ExternalRepository = Me.PersistentRepository1
        Me.dgBadDebts.Location = New System.Drawing.Point(16, 352)
        Me.dgBadDebts.MainView = Me.gvBadDebts
        Me.dgBadDebts.Name = "dgBadDebts"
        Me.dgBadDebts.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtCurrencyBadDebts})
        Me.dgBadDebts.Size = New System.Drawing.Size(528, 88)
        Me.dgBadDebts.TabIndex = 4
        Me.dgBadDebts.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvBadDebts})
        '
        'dvBadDebts
        '
        Me.dvBadDebts.RowFilter = "CPType = 'BD'"
        Me.dvBadDebts.Table = Me.dataSet.ClientPayments
        '
        'gvBadDebts
        '
        Me.gvBadDebts.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvBadDebts.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvBadDebts.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvBadDebts.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvBadDebts.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvBadDebts.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvBadDebts.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvBadDebts.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvBadDebts.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvBadDebts.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvBadDebts.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvBadDebts.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvBadDebts.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvBadDebts.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvBadDebts.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvBadDebts.Appearance.VertLine.Options.UseBackColor = True
        Me.gvBadDebts.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3})
        Me.gvBadDebts.GridControl = Me.dgBadDebts
        Me.gvBadDebts.Name = "gvBadDebts"
        Me.gvBadDebts.OptionsCustomization.AllowFilter = False
        Me.gvBadDebts.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvBadDebts.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Date"
        Me.GridColumn1.ColumnEdit = Me.txtDate
        Me.GridColumn1.FieldName = "CPDate"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 187
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Amount *"
        Me.GridColumn2.ColumnEdit = Me.txtCurrencyBadDebts
        Me.GridColumn2.FieldName = "CPAmount"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 2
        Me.GridColumn2.Width = 161
        '
        'txtCurrencyBadDebts
        '
        Me.txtCurrencyBadDebts.AutoHeight = False
        Me.txtCurrencyBadDebts.DisplayFormat.FormatString = "c"
        Me.txtCurrencyBadDebts.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCurrencyBadDebts.EditFormat.FormatString = "c"
        Me.txtCurrencyBadDebts.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCurrencyBadDebts.Name = "txtCurrencyBadDebts"
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Notes"
        Me.GridColumn3.FieldName = "CPNotes"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 1
        Me.GridColumn3.Width = 477
        '
        'btnRemoveDiscount
        '
        Me.btnRemoveDiscount.Image = CType(resources.GetObject("btnRemoveDiscount.Image"), System.Drawing.Image)
        Me.btnRemoveDiscount.Location = New System.Drawing.Point(16, 296)
        Me.btnRemoveDiscount.Name = "btnRemoveDiscount"
        Me.btnRemoveDiscount.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveDiscount.TabIndex = 3
        Me.btnRemoveDiscount.Text = "Remove"
        '
        'Label34
        '
        Me.Label34.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(16, 328)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(368, 23)
        Me.Label34.TabIndex = 5
        Me.Label34.Text = "Bad debts written off:"
        Me.Label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlAppliances
        '
        Me.pnlAppliances.Controls.Add(Me.btnRemoveAppliance)
        Me.pnlAppliances.Controls.Add(Me.btnEditAppliance)
        Me.pnlAppliances.Controls.Add(Me.btnAddAppliance)
        Me.pnlAppliances.Controls.Add(Me.dgAppliances)
        Me.pnlAppliances.Location = New System.Drawing.Point(16, 64)
        Me.pnlAppliances.Name = "pnlAppliances"
        Me.pnlAppliances.Size = New System.Drawing.Size(576, 400)
        Me.pnlAppliances.TabIndex = 8
        Me.pnlAppliances.Text = "Appliances and Plumbing Items"
        '
        'btnRemoveAppliance
        '
        Me.btnRemoveAppliance.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveAppliance.Image = CType(resources.GetObject("btnRemoveAppliance.Image"), System.Drawing.Image)
        Me.btnRemoveAppliance.Location = New System.Drawing.Point(176, 368)
        Me.btnRemoveAppliance.Name = "btnRemoveAppliance"
        Me.btnRemoveAppliance.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveAppliance.TabIndex = 3
        Me.btnRemoveAppliance.Text = "Remove"
        '
        'btnEditAppliance
        '
        Me.btnEditAppliance.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEditAppliance.Image = CType(resources.GetObject("btnEditAppliance.Image"), System.Drawing.Image)
        Me.btnEditAppliance.Location = New System.Drawing.Point(96, 368)
        Me.btnEditAppliance.Name = "btnEditAppliance"
        Me.btnEditAppliance.Size = New System.Drawing.Size(72, 23)
        Me.btnEditAppliance.TabIndex = 2
        Me.btnEditAppliance.Text = "Edit..."
        '
        'btnAddAppliance
        '
        Me.btnAddAppliance.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddAppliance.Image = CType(resources.GetObject("btnAddAppliance.Image"), System.Drawing.Image)
        Me.btnAddAppliance.Location = New System.Drawing.Point(16, 368)
        Me.btnAddAppliance.Name = "btnAddAppliance"
        Me.btnAddAppliance.Size = New System.Drawing.Size(72, 23)
        Me.btnAddAppliance.TabIndex = 1
        Me.btnAddAppliance.Text = "Add..."
        '
        'dgAppliances
        '
        Me.dgAppliances.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgAppliances.DataSource = Me.dvAppliances
        '
        'dgAppliances.EmbeddedNavigator
        '
        Me.dgAppliances.EmbeddedNavigator.Name = ""
        Me.dgAppliances.Location = New System.Drawing.Point(16, 32)
        Me.dgAppliances.MainView = Me.gvAppliances
        Me.dgAppliances.Name = "dgAppliances"
        Me.dgAppliances.Size = New System.Drawing.Size(544, 328)
        Me.dgAppliances.TabIndex = 0
        Me.dgAppliances.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvAppliances})
        '
        'dvAppliances
        '
        Me.dvAppliances.RowFilter = "NIType = 'AP'"
        Me.dvAppliances.Table = Me.dataSet.VJobsNonCoreSalesItems
        '
        'gvAppliances
        '
        Me.gvAppliances.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.gvAppliances.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvAppliances.Appearance.FocusedCell.Options.UseBackColor = True
        Me.gvAppliances.Appearance.FocusedCell.Options.UseForeColor = True
        Me.gvAppliances.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.gvAppliances.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gvAppliances.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvAppliances.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvAppliances.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colNIUserType1, Me.colNIBrand, Me.colNIName2, Me.colNIOrderedBy, Me.colNIPaidBy})
        Me.gvAppliances.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.gvAppliances.GridControl = Me.dgAppliances
        Me.gvAppliances.Name = "gvAppliances"
        Me.gvAppliances.OptionsBehavior.Editable = False
        Me.gvAppliances.OptionsCustomization.AllowFilter = False
        Me.gvAppliances.OptionsNavigation.AutoFocusNewRow = True
        Me.gvAppliances.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.gvAppliances.OptionsView.ShowGroupPanel = False
        Me.gvAppliances.OptionsView.ShowHorzLines = False
        Me.gvAppliances.OptionsView.ShowIndicator = False
        Me.gvAppliances.OptionsView.ShowVertLines = False
        Me.gvAppliances.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colNIUserType1, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colNIUserType1
        '
        Me.colNIUserType1.Caption = "Appliance Type"
        Me.colNIUserType1.FieldName = "NIUserType"
        Me.colNIUserType1.Name = "colNIUserType1"
        Me.colNIUserType1.Visible = True
        Me.colNIUserType1.VisibleIndex = 0
        Me.colNIUserType1.Width = 176
        '
        'colNIBrand
        '
        Me.colNIBrand.Caption = "Brand"
        Me.colNIBrand.FieldName = "NIBrand"
        Me.colNIBrand.Name = "colNIBrand"
        Me.colNIBrand.Visible = True
        Me.colNIBrand.VisibleIndex = 1
        Me.colNIBrand.Width = 166
        '
        'colNIName2
        '
        Me.colNIName2.Caption = "Model Number"
        Me.colNIName2.FieldName = "NIName"
        Me.colNIName2.Name = "colNIName2"
        Me.colNIName2.Visible = True
        Me.colNIName2.VisibleIndex = 2
        Me.colNIName2.Width = 162
        '
        'colNIOrderedBy
        '
        Me.colNIOrderedBy.Caption = "Ordered By"
        Me.colNIOrderedBy.FieldName = "NIOrderedBy"
        Me.colNIOrderedBy.Name = "colNIOrderedBy"
        Me.colNIOrderedBy.Visible = True
        Me.colNIOrderedBy.VisibleIndex = 3
        Me.colNIOrderedBy.Width = 168
        '
        'colNIPaidBy
        '
        Me.colNIPaidBy.Caption = "Paid By"
        Me.colNIPaidBy.FieldName = "NIPaidBy"
        Me.colNIPaidBy.Name = "colNIPaidBy"
        Me.colNIPaidBy.Visible = True
        Me.colNIPaidBy.VisibleIndex = 4
        Me.colNIPaidBy.Width = 171
        '
        'pnlDirectLabour
        '
        Me.pnlDirectLabour.Controls.Add(Me.btnRemoveDirectLabour)
        Me.pnlDirectLabour.Controls.Add(Me.btnAddDirectLabour)
        Me.pnlDirectLabour.Controls.Add(Me.dgDirectLabour)
        Me.pnlDirectLabour.Controls.Add(Me.tvDirectLabour)
        Me.pnlDirectLabour.Controls.Add(Me.Label26)
        Me.pnlDirectLabour.Controls.Add(Me.Label27)
        Me.pnlDirectLabour.Location = New System.Drawing.Point(48, 24)
        Me.pnlDirectLabour.Name = "pnlDirectLabour"
        Me.pnlDirectLabour.Size = New System.Drawing.Size(584, 352)
        Me.pnlDirectLabour.TabIndex = 5
        Me.pnlDirectLabour.Text = "Specified Direct Labor"
        '
        'btnRemoveDirectLabour
        '
        Me.btnRemoveDirectLabour.Image = CType(resources.GetObject("btnRemoveDirectLabour.Image"), System.Drawing.Image)
        Me.btnRemoveDirectLabour.Location = New System.Drawing.Point(208, 184)
        Me.btnRemoveDirectLabour.Name = "btnRemoveDirectLabour"
        Me.btnRemoveDirectLabour.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveDirectLabour.TabIndex = 2
        Me.btnRemoveDirectLabour.Text = "Remove"
        '
        'btnAddDirectLabour
        '
        Me.btnAddDirectLabour.Image = CType(resources.GetObject("btnAddDirectLabour.Image"), System.Drawing.Image)
        Me.btnAddDirectLabour.ImageAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnAddDirectLabour.Location = New System.Drawing.Point(208, 152)
        Me.btnAddDirectLabour.Name = "btnAddDirectLabour"
        Me.btnAddDirectLabour.Size = New System.Drawing.Size(72, 23)
        Me.btnAddDirectLabour.TabIndex = 1
        Me.btnAddDirectLabour.Text = "Add"
        '
        'dgDirectLabour
        '
        Me.dgDirectLabour.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgDirectLabour.DataSource = Me.dvJobs_DirectLabour
        '
        'dgDirectLabour.EmbeddedNavigator
        '
        Me.dgDirectLabour.EmbeddedNavigator.Name = ""
        Me.dgDirectLabour.Location = New System.Drawing.Point(288, 48)
        Me.dgDirectLabour.MainView = Me.gvDirectLabour
        Me.dgDirectLabour.Name = "dgDirectLabour"
        Me.dgDirectLabour.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtDollarDirectLabour, Me.txtPercentageDirectLabour, Me.rgPayAsDirectLabour})
        Me.dgDirectLabour.Size = New System.Drawing.Size(280, 288)
        Me.dgDirectLabour.TabIndex = 3
        Me.dgDirectLabour.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvDirectLabour})
        '
        'dvJobs_DirectLabour
        '
        Me.dvJobs_DirectLabour.AllowNew = False
        Me.dvJobs_DirectLabour.RowFilter = "EGType = 'DL'"
        Me.dvJobs_DirectLabour.Table = Me.dataSet.VJobs_Expenses
        '
        'gvDirectLabour
        '
        Me.gvDirectLabour.CardCaptionFormat = ""
        Me.gvDirectLabour.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn12, Me.GridColumn13, Me.GridColumn14, Me.colJEHours, Me.colEXName})
        Me.gvDirectLabour.FocusedCardTopFieldIndex = 0
        Me.gvDirectLabour.GridControl = Me.dgDirectLabour
        Me.gvDirectLabour.Name = "gvDirectLabour"
        Me.gvDirectLabour.OptionsBehavior.AutoHorzWidth = True
        Me.gvDirectLabour.OptionsBehavior.FieldAutoHeight = True
        Me.gvDirectLabour.OptionsView.ShowLines = False
        Me.gvDirectLabour.OptionsView.ShowQuickCustomizeButton = False
        Me.gvDirectLabour.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Auto
        '
        'GridColumn12
        '
        Me.GridColumn12.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn12.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.GridColumn12.Caption = "Pay As..."
        Me.GridColumn12.ColumnEdit = Me.rgPayAsDirectLabour
        Me.GridColumn12.FieldName = "JEAsPercent"
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.Visible = True
        Me.GridColumn12.VisibleIndex = 0
        Me.GridColumn12.Width = 141
        '
        'rgPayAsDirectLabour
        '
        Me.rgPayAsDirectLabour.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Amount"), New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Percent"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Both")})
        Me.rgPayAsDirectLabour.Name = "rgPayAsDirectLabour"
        '
        'GridColumn13
        '
        Me.GridColumn13.Caption = "Amount *"
        Me.GridColumn13.ColumnEdit = Me.txtDollarDirectLabour
        Me.GridColumn13.FieldName = "JECharge"
        Me.GridColumn13.Name = "GridColumn13"
        Me.GridColumn13.Visible = True
        Me.GridColumn13.VisibleIndex = 1
        Me.GridColumn13.Width = 76
        '
        'txtDollarDirectLabour
        '
        Me.txtDollarDirectLabour.AutoHeight = False
        Me.txtDollarDirectLabour.DisplayFormat.FormatString = "c"
        Me.txtDollarDirectLabour.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtDollarDirectLabour.EditFormat.FormatString = "c"
        Me.txtDollarDirectLabour.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtDollarDirectLabour.Name = "txtDollarDirectLabour"
        '
        'GridColumn14
        '
        Me.GridColumn14.Caption = "Percent *"
        Me.GridColumn14.ColumnEdit = Me.txtPercentageDirectLabour
        Me.GridColumn14.FieldName = "JEPercent"
        Me.GridColumn14.Name = "GridColumn14"
        Me.GridColumn14.Visible = True
        Me.GridColumn14.VisibleIndex = 2
        Me.GridColumn14.Width = 93
        '
        'txtPercentageDirectLabour
        '
        Me.txtPercentageDirectLabour.AutoHeight = False
        Me.txtPercentageDirectLabour.DisplayFormat.FormatString = "P"
        Me.txtPercentageDirectLabour.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtPercentageDirectLabour.EditFormat.FormatString = "p"
        Me.txtPercentageDirectLabour.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtPercentageDirectLabour.Name = "txtPercentageDirectLabour"
        '
        'colJEHours
        '
        Me.colJEHours.Caption = "Hours*"
        Me.colJEHours.DisplayFormat.FormatString = "#.00"
        Me.colJEHours.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colJEHours.FieldName = "JEHours"
        Me.colJEHours.Name = "colJEHours"
        Me.colJEHours.Visible = True
        Me.colJEHours.VisibleIndex = 3
        Me.colJEHours.Width = 90
        '
        'colEXName
        '
        Me.colEXName.Caption = "EXName"
        Me.colEXName.FieldName = "EXName"
        Me.colEXName.Name = "colEXName"
        '
        'tvDirectLabour
        '
        Me.tvDirectLabour.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.tvDirectLabour.Connection = Me.SqlConnection
        Me.tvDirectLabour.HideSelection = False
        Me.tvDirectLabour.ImageIndex = -1
        Me.tvDirectLabour.Location = New System.Drawing.Point(16, 48)
        Me.tvDirectLabour.Name = "tvDirectLabour"
        Me.tvDirectLabour.SelectedImageIndex = -1
        Me.tvDirectLabour.Size = New System.Drawing.Size(184, 288)
        Me.tvDirectLabour.TabIndex = 0
        '
        'Label26
        '
        Me.Label26.Location = New System.Drawing.Point(16, 24)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(120, 21)
        Me.Label26.TabIndex = 9
        Me.Label26.Text = "Available direct labor:"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label27
        '
        Me.Label27.Location = New System.Drawing.Point(288, 24)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(120, 21)
        Me.Label27.TabIndex = 9
        Me.Label27.Text = "Direct labor in job:"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlSummary
        '
        Me.pnlSummary.Controls.Add(Me.btnViewJobReport)
        Me.pnlSummary.Controls.Add(Me.Label57)
        Me.pnlSummary.Controls.Add(Me.lblJBOtherReduction)
        Me.pnlSummary.Controls.Add(Me.lblJBBalance)
        Me.pnlSummary.Controls.Add(Me.lblJBClientPaid)
        Me.pnlSummary.Controls.Add(Me.lblJBPriceTotal)
        Me.pnlSummary.Controls.Add(Me.lblJBJobInsurance)
        Me.pnlSummary.Controls.Add(Me.lblJBPriceOtherTrades)
        Me.pnlSummary.Controls.Add(Me.lblJBPriceAppliances)
        Me.pnlSummary.Controls.Add(Me.lblJBPrice)
        Me.pnlSummary.Controls.Add(Me.Label56)
        Me.pnlSummary.Controls.Add(Me.Label54)
        Me.pnlSummary.Controls.Add(Me.Label53)
        Me.pnlSummary.Controls.Add(Me.Label52)
        Me.pnlSummary.Controls.Add(Me.Label51)
        Me.pnlSummary.Controls.Add(Me.Label50)
        Me.pnlSummary.Controls.Add(Me.Label49)
        Me.pnlSummary.Location = New System.Drawing.Point(24, 40)
        Me.pnlSummary.Name = "pnlSummary"
        Me.pnlSummary.Size = New System.Drawing.Size(592, 424)
        Me.pnlSummary.TabIndex = 33
        Me.pnlSummary.Text = "Job Price Summary"
        '
        'btnViewJobReport
        '
        Me.btnViewJobReport.Location = New System.Drawing.Point(16, 256)
        Me.btnViewJobReport.Name = "btnViewJobReport"
        Me.btnViewJobReport.Size = New System.Drawing.Size(248, 23)
        Me.btnViewJobReport.TabIndex = 18
        Me.btnViewJobReport.Text = "View Interim Job Report..."
        '
        'Label57
        '
        Me.Label57.Location = New System.Drawing.Point(16, 192)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(136, 18)
        Me.Label57.TabIndex = 17
        Me.Label57.Text = "- Other reduction:"
        Me.Label57.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJBOtherReduction
        '
        Me.lblJBOtherReduction.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsReports, "repJobReport.JBOtherReduction"))
        Me.lblJBOtherReduction.EditValue = "$0.00"
        Me.lblJBOtherReduction.Location = New System.Drawing.Point(184, 192)
        Me.lblJBOtherReduction.Name = "lblJBOtherReduction"
        '
        'lblJBOtherReduction.Properties
        '
        Me.lblJBOtherReduction.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.lblJBOtherReduction.Properties.Appearance.Options.UseBackColor = True
        Me.lblJBOtherReduction.Properties.Appearance.Options.UseTextOptions = True
        Me.lblJBOtherReduction.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblJBOtherReduction.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.lblJBOtherReduction.Properties.DisplayFormat.FormatString = "c"
        Me.lblJBOtherReduction.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBOtherReduction.Properties.EditFormat.FormatString = "c"
        Me.lblJBOtherReduction.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBOtherReduction.Properties.ReadOnly = True
        Me.lblJBOtherReduction.Size = New System.Drawing.Size(112, 18)
        Me.lblJBOtherReduction.TabIndex = 16
        Me.lblJBOtherReduction.TabStop = False
        '
        'DsReports
        '
        Me.DsReports.DataSetName = "dsReports"
        Me.DsReports.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'lblJBBalance
        '
        Me.lblJBBalance.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsReports, "repJobReport.JBBalance"))
        Me.lblJBBalance.EditValue = "$0.00"
        Me.lblJBBalance.Location = New System.Drawing.Point(184, 224)
        Me.lblJBBalance.Name = "lblJBBalance"
        '
        'lblJBBalance.Properties
        '
        Me.lblJBBalance.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.lblJBBalance.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJBBalance.Properties.Appearance.Options.UseBackColor = True
        Me.lblJBBalance.Properties.Appearance.Options.UseFont = True
        Me.lblJBBalance.Properties.Appearance.Options.UseTextOptions = True
        Me.lblJBBalance.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblJBBalance.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.lblJBBalance.Properties.DisplayFormat.FormatString = "c"
        Me.lblJBBalance.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBBalance.Properties.EditFormat.FormatString = "c"
        Me.lblJBBalance.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBBalance.Properties.ReadOnly = True
        Me.lblJBBalance.Size = New System.Drawing.Size(112, 18)
        Me.lblJBBalance.TabIndex = 15
        Me.lblJBBalance.TabStop = False
        '
        'lblJBClientPaid
        '
        Me.lblJBClientPaid.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsReports, "repJobReport.JBClientPaid"))
        Me.lblJBClientPaid.EditValue = "$0.00"
        Me.lblJBClientPaid.Location = New System.Drawing.Point(184, 168)
        Me.lblJBClientPaid.Name = "lblJBClientPaid"
        '
        'lblJBClientPaid.Properties
        '
        Me.lblJBClientPaid.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.lblJBClientPaid.Properties.Appearance.Options.UseBackColor = True
        Me.lblJBClientPaid.Properties.Appearance.Options.UseTextOptions = True
        Me.lblJBClientPaid.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblJBClientPaid.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.lblJBClientPaid.Properties.DisplayFormat.FormatString = "c"
        Me.lblJBClientPaid.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBClientPaid.Properties.EditFormat.FormatString = "c"
        Me.lblJBClientPaid.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBClientPaid.Properties.ReadOnly = True
        Me.lblJBClientPaid.Size = New System.Drawing.Size(112, 18)
        Me.lblJBClientPaid.TabIndex = 13
        Me.lblJBClientPaid.TabStop = False
        '
        'lblJBPriceTotal
        '
        Me.lblJBPriceTotal.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsReports, "repJobReport.JBPriceTotal"))
        Me.lblJBPriceTotal.EditValue = "$0.00"
        Me.lblJBPriceTotal.Location = New System.Drawing.Point(184, 136)
        Me.lblJBPriceTotal.Name = "lblJBPriceTotal"
        '
        'lblJBPriceTotal.Properties
        '
        Me.lblJBPriceTotal.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.lblJBPriceTotal.Properties.Appearance.Options.UseBackColor = True
        Me.lblJBPriceTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.lblJBPriceTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblJBPriceTotal.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.lblJBPriceTotal.Properties.DisplayFormat.FormatString = "c"
        Me.lblJBPriceTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBPriceTotal.Properties.EditFormat.FormatString = "c"
        Me.lblJBPriceTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBPriceTotal.Properties.ReadOnly = True
        Me.lblJBPriceTotal.Size = New System.Drawing.Size(112, 18)
        Me.lblJBPriceTotal.TabIndex = 12
        Me.lblJBPriceTotal.TabStop = False
        '
        'lblJBJobInsurance
        '
        Me.lblJBJobInsurance.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsReports, "repJobReport.JBJobInsurance"))
        Me.lblJBJobInsurance.EditValue = "$0.00"
        Me.lblJBJobInsurance.Location = New System.Drawing.Point(184, 104)
        Me.lblJBJobInsurance.Name = "lblJBJobInsurance"
        '
        'lblJBJobInsurance.Properties
        '
        Me.lblJBJobInsurance.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.lblJBJobInsurance.Properties.Appearance.Options.UseBackColor = True
        Me.lblJBJobInsurance.Properties.Appearance.Options.UseTextOptions = True
        Me.lblJBJobInsurance.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblJBJobInsurance.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.lblJBJobInsurance.Properties.DisplayFormat.FormatString = "c"
        Me.lblJBJobInsurance.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBJobInsurance.Properties.EditFormat.FormatString = "c"
        Me.lblJBJobInsurance.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBJobInsurance.Properties.ReadOnly = True
        Me.lblJBJobInsurance.Size = New System.Drawing.Size(112, 18)
        Me.lblJBJobInsurance.TabIndex = 11
        Me.lblJBJobInsurance.TabStop = False
        '
        'lblJBPriceOtherTrades
        '
        Me.lblJBPriceOtherTrades.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsReports, "repJobReport.JBPriceOtherTrades"))
        Me.lblJBPriceOtherTrades.EditValue = "$0.00"
        Me.lblJBPriceOtherTrades.Location = New System.Drawing.Point(184, 80)
        Me.lblJBPriceOtherTrades.Name = "lblJBPriceOtherTrades"
        '
        'lblJBPriceOtherTrades.Properties
        '
        Me.lblJBPriceOtherTrades.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.lblJBPriceOtherTrades.Properties.Appearance.Options.UseBackColor = True
        Me.lblJBPriceOtherTrades.Properties.Appearance.Options.UseTextOptions = True
        Me.lblJBPriceOtherTrades.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblJBPriceOtherTrades.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.lblJBPriceOtherTrades.Properties.DisplayFormat.FormatString = "c"
        Me.lblJBPriceOtherTrades.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBPriceOtherTrades.Properties.EditFormat.FormatString = "c"
        Me.lblJBPriceOtherTrades.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBPriceOtherTrades.Properties.ReadOnly = True
        Me.lblJBPriceOtherTrades.Size = New System.Drawing.Size(112, 18)
        Me.lblJBPriceOtherTrades.TabIndex = 10
        Me.lblJBPriceOtherTrades.TabStop = False
        '
        'lblJBPriceAppliances
        '
        Me.lblJBPriceAppliances.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsReports, "repJobReport.JBPriceAppliances"))
        Me.lblJBPriceAppliances.EditValue = "$0.00"
        Me.lblJBPriceAppliances.Location = New System.Drawing.Point(184, 56)
        Me.lblJBPriceAppliances.Name = "lblJBPriceAppliances"
        '
        'lblJBPriceAppliances.Properties
        '
        Me.lblJBPriceAppliances.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.lblJBPriceAppliances.Properties.Appearance.Options.UseBackColor = True
        Me.lblJBPriceAppliances.Properties.Appearance.Options.UseTextOptions = True
        Me.lblJBPriceAppliances.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblJBPriceAppliances.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.lblJBPriceAppliances.Properties.DisplayFormat.FormatString = "c"
        Me.lblJBPriceAppliances.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBPriceAppliances.Properties.EditFormat.FormatString = "c"
        Me.lblJBPriceAppliances.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBPriceAppliances.Properties.ReadOnly = True
        Me.lblJBPriceAppliances.Size = New System.Drawing.Size(112, 18)
        Me.lblJBPriceAppliances.TabIndex = 9
        Me.lblJBPriceAppliances.TabStop = False
        '
        'lblJBPrice
        '
        Me.lblJBPrice.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsReports, "repJobReport.JBPrice"))
        Me.lblJBPrice.EditValue = "$0.00"
        Me.lblJBPrice.Location = New System.Drawing.Point(184, 32)
        Me.lblJBPrice.Name = "lblJBPrice"
        '
        'lblJBPrice.Properties
        '
        Me.lblJBPrice.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.lblJBPrice.Properties.Appearance.Options.UseBackColor = True
        Me.lblJBPrice.Properties.Appearance.Options.UseTextOptions = True
        Me.lblJBPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblJBPrice.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.lblJBPrice.Properties.DisplayFormat.FormatString = "c"
        Me.lblJBPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBPrice.Properties.EditFormat.FormatString = "c"
        Me.lblJBPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBPrice.Properties.ReadOnly = True
        Me.lblJBPrice.Size = New System.Drawing.Size(112, 18)
        Me.lblJBPrice.TabIndex = 8
        Me.lblJBPrice.TabStop = False
        '
        'Label56
        '
        Me.Label56.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label56.Location = New System.Drawing.Point(16, 224)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(136, 18)
        Me.Label56.TabIndex = 7
        Me.Label56.Text = "= Balance:"
        Me.Label56.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label54
        '
        Me.Label54.Location = New System.Drawing.Point(16, 168)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(136, 18)
        Me.Label54.TabIndex = 5
        Me.Label54.Text = "- Paid:"
        Me.Label54.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label53
        '
        Me.Label53.Location = New System.Drawing.Point(16, 136)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(136, 18)
        Me.Label53.TabIndex = 4
        Me.Label53.Text = "= Total:"
        Me.Label53.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label52
        '
        Me.Label52.Location = New System.Drawing.Point(16, 104)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(136, 18)
        Me.Label52.TabIndex = 3
        Me.Label52.Text = "+ Job insurance:"
        Me.Label52.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label51
        '
        Me.Label51.Location = New System.Drawing.Point(16, 80)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(136, 18)
        Me.Label51.TabIndex = 2
        Me.Label51.Text = "+ Other trades:"
        Me.Label51.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label50
        '
        Me.Label50.Location = New System.Drawing.Point(16, 56)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(176, 18)
        Me.Label50.TabIndex = 1
        Me.Label50.Text = "+ Appliances and Plumbing items:"
        Me.Label50.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label49
        '
        Me.Label49.Location = New System.Drawing.Point(16, 32)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(136, 18)
        Me.Label49.TabIndex = 0
        Me.Label49.Text = "Job price:"
        Me.Label49.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlSalesReps
        '
        Me.pnlSalesReps.Controls.Add(Me.btnRemoveSalesRep)
        Me.pnlSalesReps.Controls.Add(Me.btnAddSalesRep)
        Me.pnlSalesReps.Controls.Add(Me.dgSalesReps)
        Me.pnlSalesReps.Controls.Add(Me.tvSalesReps)
        Me.pnlSalesReps.Controls.Add(Me.Label28)
        Me.pnlSalesReps.Controls.Add(Me.Label29)
        Me.pnlSalesReps.Location = New System.Drawing.Point(48, 168)
        Me.pnlSalesReps.Name = "pnlSalesReps"
        Me.pnlSalesReps.Size = New System.Drawing.Size(584, 296)
        Me.pnlSalesReps.TabIndex = 6
        Me.pnlSalesReps.Text = "Specified Salespeople"
        '
        'btnRemoveSalesRep
        '
        Me.btnRemoveSalesRep.Image = CType(resources.GetObject("btnRemoveSalesRep.Image"), System.Drawing.Image)
        Me.btnRemoveSalesRep.Location = New System.Drawing.Point(208, 184)
        Me.btnRemoveSalesRep.Name = "btnRemoveSalesRep"
        Me.btnRemoveSalesRep.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveSalesRep.TabIndex = 2
        Me.btnRemoveSalesRep.Text = "Remove"
        '
        'btnAddSalesRep
        '
        Me.btnAddSalesRep.Image = CType(resources.GetObject("btnAddSalesRep.Image"), System.Drawing.Image)
        Me.btnAddSalesRep.ImageAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnAddSalesRep.Location = New System.Drawing.Point(208, 152)
        Me.btnAddSalesRep.Name = "btnAddSalesRep"
        Me.btnAddSalesRep.Size = New System.Drawing.Size(72, 23)
        Me.btnAddSalesRep.TabIndex = 1
        Me.btnAddSalesRep.Text = "Add"
        '
        'dgSalesReps
        '
        Me.dgSalesReps.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgSalesReps.DataSource = Me.dvJobs_SalesReps
        '
        'dgSalesReps.EmbeddedNavigator
        '
        Me.dgSalesReps.EmbeddedNavigator.Name = ""
        Me.dgSalesReps.Location = New System.Drawing.Point(288, 48)
        Me.dgSalesReps.MainView = Me.gvSalesReps
        Me.dgSalesReps.Name = "dgSalesReps"
        Me.dgSalesReps.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtDollarSalesRep, Me.txtPercentageSalesReps, Me.rgPayAsSalesReps})
        Me.dgSalesReps.Size = New System.Drawing.Size(280, 232)
        Me.dgSalesReps.TabIndex = 3
        Me.dgSalesReps.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvSalesReps})
        '
        'dvJobs_SalesReps
        '
        Me.dvJobs_SalesReps.AllowNew = False
        Me.dvJobs_SalesReps.RowFilter = "EGType = 'RC'"
        Me.dvJobs_SalesReps.Table = Me.dataSet.VJobs_Expenses
        '
        'gvSalesReps
        '
        Me.gvSalesReps.CardCaptionFormat = ""
        Me.gvSalesReps.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn7, Me.GridColumn8, Me.GridColumn9, Me.GridColumn10})
        Me.gvSalesReps.FocusedCardTopFieldIndex = 0
        Me.gvSalesReps.GridControl = Me.dgSalesReps
        Me.gvSalesReps.Name = "gvSalesReps"
        Me.gvSalesReps.OptionsBehavior.AutoHorzWidth = True
        Me.gvSalesReps.OptionsBehavior.FieldAutoHeight = True
        Me.gvSalesReps.OptionsView.ShowLines = False
        Me.gvSalesReps.OptionsView.ShowQuickCustomizeButton = False
        Me.gvSalesReps.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn7, DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.gvSalesReps.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Auto
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Name"
        Me.GridColumn7.FieldName = "EXName"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.OptionsColumn.AllowEdit = False
        Me.GridColumn7.OptionsColumn.AllowFocus = False
        Me.GridColumn7.Width = 69
        '
        'GridColumn8
        '
        Me.GridColumn8.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn8.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.GridColumn8.Caption = "Pay As..."
        Me.GridColumn8.ColumnEdit = Me.rgPayAsSalesReps
        Me.GridColumn8.FieldName = "JEAsPercent"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 0
        Me.GridColumn8.Width = 106
        '
        'rgPayAsSalesReps
        '
        Me.rgPayAsSalesReps.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Amount"), New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Percent"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Both")})
        Me.rgPayAsSalesReps.Name = "rgPayAsSalesReps"
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Amount *"
        Me.GridColumn9.ColumnEdit = Me.txtDollarSalesRep
        Me.GridColumn9.FieldName = "JECharge"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 1
        Me.GridColumn9.Width = 41
        '
        'txtDollarSalesRep
        '
        Me.txtDollarSalesRep.AutoHeight = False
        Me.txtDollarSalesRep.DisplayFormat.FormatString = "c"
        Me.txtDollarSalesRep.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtDollarSalesRep.EditFormat.FormatString = "c"
        Me.txtDollarSalesRep.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtDollarSalesRep.Name = "txtDollarSalesRep"
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Percent *"
        Me.GridColumn10.ColumnEdit = Me.txtPercentageSalesReps
        Me.GridColumn10.FieldName = "JEPercent"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 2
        Me.GridColumn10.Width = 43
        '
        'txtPercentageSalesReps
        '
        Me.txtPercentageSalesReps.AutoHeight = False
        Me.txtPercentageSalesReps.DisplayFormat.FormatString = "P"
        Me.txtPercentageSalesReps.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtPercentageSalesReps.EditFormat.FormatString = "p"
        Me.txtPercentageSalesReps.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtPercentageSalesReps.Name = "txtPercentageSalesReps"
        '
        'tvSalesReps
        '
        Me.tvSalesReps.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.tvSalesReps.Connection = Me.SqlConnection
        Me.tvSalesReps.HideSelection = False
        Me.tvSalesReps.ImageIndex = -1
        Me.tvSalesReps.Location = New System.Drawing.Point(16, 48)
        Me.tvSalesReps.Name = "tvSalesReps"
        Me.tvSalesReps.SelectedImageIndex = -1
        Me.tvSalesReps.Size = New System.Drawing.Size(184, 232)
        Me.tvSalesReps.TabIndex = 0
        '
        'Label28
        '
        Me.Label28.Location = New System.Drawing.Point(16, 24)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(120, 21)
        Me.Label28.TabIndex = 14
        Me.Label28.Text = "Available salespeople:"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label29
        '
        Me.Label29.Location = New System.Drawing.Point(288, 24)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(120, 21)
        Me.Label29.TabIndex = 13
        Me.Label29.Text = "Salespeople in job:"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlNotes
        '
        Me.pnlNotes.Controls.Add(Me.dgNotes)
        Me.pnlNotes.Location = New System.Drawing.Point(32, 40)
        Me.pnlNotes.Name = "pnlNotes"
        Me.pnlNotes.Size = New System.Drawing.Size(584, 408)
        Me.pnlNotes.TabIndex = 34
        Me.pnlNotes.Text = "Notes and Reminders"
        '
        'dgNotes
        '
        Me.dgNotes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgNotes.DataSource = Me.DsNotes.IDNotes
        '
        'dgNotes.EmbeddedNavigator
        '
        Me.dgNotes.EmbeddedNavigator.Name = ""
        Me.dgNotes.Location = New System.Drawing.Point(16, 32)
        Me.dgNotes.MainView = Me.gvNotes
        Me.dgNotes.Name = "dgNotes"
        Me.dgNotes.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtEXID, Me.txtINDate, Me.txtINFollowUpText, Me.txtINNotes})
        Me.dgNotes.Size = New System.Drawing.Size(552, 360)
        Me.dgNotes.TabIndex = 2
        Me.dgNotes.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvNotes})
        '
        'DsNotes
        '
        Me.DsNotes.DataSetName = "dsNotes"
        Me.DsNotes.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'gvNotes
        '
        Me.gvNotes.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvNotes.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvNotes.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvNotes.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvNotes.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvNotes.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvNotes.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvNotes.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvNotes.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvNotes.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvNotes.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvNotes.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvNotes.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvNotes.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvNotes.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvNotes.Appearance.VertLine.Options.UseBackColor = True
        Me.gvNotes.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand1})
        Me.gvNotes.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.colINUser, Me.colINDate, Me.colINNotes, Me.colINFollowUpText})
        Me.gvNotes.GridControl = Me.dgNotes
        Me.gvNotes.Name = "gvNotes"
        Me.gvNotes.NewItemRowText = "Type here to add a new row"
        Me.gvNotes.OptionsCustomization.AllowFilter = False
        Me.gvNotes.OptionsCustomization.AllowRowSizing = True
        Me.gvNotes.OptionsView.ColumnAutoWidth = True
        Me.gvNotes.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvNotes.OptionsView.ShowGroupPanel = False
        Me.gvNotes.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colINDate, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'GridBand1
        '
        Me.GridBand1.Caption = "GridBand1"
        Me.GridBand1.Columns.Add(Me.colINUser)
        Me.GridBand1.Columns.Add(Me.colINDate)
        Me.GridBand1.Columns.Add(Me.colINNotes)
        Me.GridBand1.Columns.Add(Me.colINFollowUpText)
        Me.GridBand1.Name = "GridBand1"
        Me.GridBand1.OptionsBand.ShowCaption = False
        Me.GridBand1.Width = 466
        '
        'colINUser
        '
        Me.colINUser.Caption = "User"
        Me.colINUser.ColumnEdit = Me.txtEXID
        Me.colINUser.FieldName = "INUser"
        Me.colINUser.Name = "colINUser"
        Me.colINUser.Visible = True
        Me.colINUser.Width = 120
        '
        'txtEXID
        '
        Me.txtEXID.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXName")})
        Me.txtEXID.DataSource = Me.dvExpenses2
        Me.txtEXID.DisplayMember = "EXName"
        Me.txtEXID.Name = "txtEXID"
        Me.txtEXID.NullText = ""
        Me.txtEXID.ShowFooter = False
        Me.txtEXID.ShowHeader = False
        Me.txtEXID.ShowLines = False
        Me.txtEXID.ValueMember = "EXName"
        '
        'dvExpenses2
        '
        Me.dvExpenses2.RowFilter = "EGType <> 'OE'"
        Me.dvExpenses2.Sort = "EXName"
        Me.dvExpenses2.Table = Me.DsNotes.VExpenses
        '
        'colINDate
        '
        Me.colINDate.Caption = "Date"
        Me.colINDate.ColumnEdit = Me.txtINDate
        Me.colINDate.FieldName = "INDate"
        Me.colINDate.Name = "colINDate"
        Me.colINDate.Visible = True
        Me.colINDate.Width = 96
        '
        'txtINDate
        '
        Me.txtINDate.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtINDate.Name = "txtINDate"
        '
        'colINNotes
        '
        Me.colINNotes.Caption = "Notes"
        Me.colINNotes.ColumnEdit = Me.txtINNotes
        Me.colINNotes.FieldName = "INNotes"
        Me.colINNotes.Name = "colINNotes"
        Me.colINNotes.Visible = True
        Me.colINNotes.Width = 250
        '
        'txtINNotes
        '
        Me.txtINNotes.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.txtINNotes.Name = "txtINNotes"
        Me.txtINNotes.ReadOnly = True
        Me.txtINNotes.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        '
        'colINFollowUpText
        '
        Me.colINFollowUpText.Caption = "Follow Up Information"
        Me.colINFollowUpText.ColumnEdit = Me.txtINFollowUpText
        Me.colINFollowUpText.FieldName = "INFollowUpText"
        Me.colINFollowUpText.Name = "colINFollowUpText"
        Me.colINFollowUpText.RowIndex = 1
        Me.colINFollowUpText.Visible = True
        Me.colINFollowUpText.Width = 466
        '
        'txtINFollowUpText
        '
        Me.txtINFollowUpText.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.txtINFollowUpText.Name = "txtINFollowUpText"
        Me.txtINFollowUpText.ReadOnly = True
        Me.txtINFollowUpText.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        '
        'pnlJobAddress
        '
        Me.pnlJobAddress.Controls.Add(Me.txtJBJobStreetAddress01)
        Me.pnlJobAddress.Controls.Add(Me.rgJBJobAddressAsAbove)
        Me.pnlJobAddress.Controls.Add(Me.Label17)
        Me.pnlJobAddress.Controls.Add(Me.lblJobState)
        Me.pnlJobAddress.Controls.Add(Me.lblJobPostCode)
        Me.pnlJobAddress.Controls.Add(Me.lblJobStreetAddress)
        Me.pnlJobAddress.Controls.Add(Me.lblJobSuburb)
        Me.pnlJobAddress.Controls.Add(Me.txtJBJobStreetAddress02)
        Me.pnlJobAddress.Controls.Add(Me.txtJBJobSuburb)
        Me.pnlJobAddress.Controls.Add(Me.txtJBJobState)
        Me.pnlJobAddress.Controls.Add(Me.txtJBJobPostCode)
        Me.pnlJobAddress.Location = New System.Drawing.Point(32, 48)
        Me.pnlJobAddress.Name = "pnlJobAddress"
        Me.pnlJobAddress.Size = New System.Drawing.Size(576, 304)
        Me.pnlJobAddress.TabIndex = 2
        Me.pnlJobAddress.Text = "Job Address"
        '
        'txtJBJobStreetAddress01
        '
        Me.txtJBJobStreetAddress01.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBJobStreetAddress01"))
        Me.txtJBJobStreetAddress01.EditValue = ""
        Me.txtJBJobStreetAddress01.Location = New System.Drawing.Point(104, 104)
        Me.txtJBJobStreetAddress01.Name = "txtJBJobStreetAddress01"
        Me.txtJBJobStreetAddress01.Size = New System.Drawing.Size(368, 20)
        Me.txtJBJobStreetAddress01.TabIndex = 1
        '
        'rgJBJobAddressAsAbove
        '
        Me.rgJBJobAddressAsAbove.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBJobAddressAsAbove"))
        Me.rgJBJobAddressAsAbove.Location = New System.Drawing.Point(320, 40)
        Me.rgJBJobAddressAsAbove.Name = "rgJBJobAddressAsAbove"
        '
        'rgJBJobAddressAsAbove.Properties
        '
        Me.rgJBJobAddressAsAbove.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.rgJBJobAddressAsAbove.Properties.Appearance.Options.UseBackColor = True
        Me.rgJBJobAddressAsAbove.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.rgJBJobAddressAsAbove.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "Yes"), New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "No")})
        Me.rgJBJobAddressAsAbove.Size = New System.Drawing.Size(56, 56)
        Me.rgJBJobAddressAsAbove.TabIndex = 0
        '
        'Label17
        '
        Me.Label17.Location = New System.Drawing.Point(16, 56)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(296, 21)
        Me.Label17.TabIndex = 24
        Me.Label17.Text = "Is this job at the customer's address (same as previous)?"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobState
        '
        Me.lblJobState.Location = New System.Drawing.Point(16, 200)
        Me.lblJobState.Name = "lblJobState"
        Me.lblJobState.Size = New System.Drawing.Size(80, 21)
        Me.lblJobState.TabIndex = 21
        Me.lblJobState.Text = "State/region:"
        Me.lblJobState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobPostCode
        '
        Me.lblJobPostCode.Location = New System.Drawing.Point(296, 200)
        Me.lblJobPostCode.Name = "lblJobPostCode"
        Me.lblJobPostCode.Size = New System.Drawing.Size(96, 21)
        Me.lblJobPostCode.TabIndex = 22
        Me.lblJobPostCode.Text = "ZIP/postal code:"
        Me.lblJobPostCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobStreetAddress
        '
        Me.lblJobStreetAddress.Location = New System.Drawing.Point(16, 104)
        Me.lblJobStreetAddress.Name = "lblJobStreetAddress"
        Me.lblJobStreetAddress.Size = New System.Drawing.Size(88, 21)
        Me.lblJobStreetAddress.TabIndex = 23
        Me.lblJobStreetAddress.Text = "Street address:"
        Me.lblJobStreetAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobSuburb
        '
        Me.lblJobSuburb.Location = New System.Drawing.Point(16, 168)
        Me.lblJobSuburb.Name = "lblJobSuburb"
        Me.lblJobSuburb.Size = New System.Drawing.Size(80, 21)
        Me.lblJobSuburb.TabIndex = 20
        Me.lblJobSuburb.Text = "Suburb/town:"
        Me.lblJobSuburb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJBJobStreetAddress02
        '
        Me.txtJBJobStreetAddress02.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBJobStreetAddress02"))
        Me.txtJBJobStreetAddress02.EditValue = ""
        Me.txtJBJobStreetAddress02.Location = New System.Drawing.Point(104, 136)
        Me.txtJBJobStreetAddress02.Name = "txtJBJobStreetAddress02"
        Me.txtJBJobStreetAddress02.Size = New System.Drawing.Size(368, 20)
        Me.txtJBJobStreetAddress02.TabIndex = 2
        '
        'txtJBJobSuburb
        '
        Me.txtJBJobSuburb.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBJobSuburb"))
        Me.txtJBJobSuburb.EditValue = ""
        Me.txtJBJobSuburb.Location = New System.Drawing.Point(104, 168)
        Me.txtJBJobSuburb.Name = "txtJBJobSuburb"
        Me.txtJBJobSuburb.Size = New System.Drawing.Size(368, 20)
        Me.txtJBJobSuburb.TabIndex = 3
        '
        'txtJBJobState
        '
        Me.txtJBJobState.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBJobState"))
        Me.txtJBJobState.EditValue = ""
        Me.txtJBJobState.Location = New System.Drawing.Point(104, 200)
        Me.txtJBJobState.Name = "txtJBJobState"
        Me.txtJBJobState.Size = New System.Drawing.Size(104, 20)
        Me.txtJBJobState.TabIndex = 4
        '
        'txtJBJobPostCode
        '
        Me.txtJBJobPostCode.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Jobs.JBJobPostCode"))
        Me.txtJBJobPostCode.EditValue = ""
        Me.txtJBJobPostCode.Location = New System.Drawing.Point(392, 200)
        Me.txtJBJobPostCode.Name = "txtJBJobPostCode"
        Me.txtJBJobPostCode.Size = New System.Drawing.Size(80, 20)
        Me.txtJBJobPostCode.TabIndex = 5
        '
        'pnlOtherTrades
        '
        Me.pnlOtherTrades.Controls.Add(Me.btnRemoveOtherTrade)
        Me.pnlOtherTrades.Controls.Add(Me.btnEditOtherTrade)
        Me.pnlOtherTrades.Controls.Add(Me.btnAddOtherTrade)
        Me.pnlOtherTrades.Controls.Add(Me.dgOtherTrades)
        Me.pnlOtherTrades.Location = New System.Drawing.Point(96, 16)
        Me.pnlOtherTrades.Name = "pnlOtherTrades"
        Me.pnlOtherTrades.Size = New System.Drawing.Size(504, 336)
        Me.pnlOtherTrades.TabIndex = 10
        Me.pnlOtherTrades.Text = "Other Trades"
        '
        'btnRemoveOtherTrade
        '
        Me.btnRemoveOtherTrade.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveOtherTrade.Image = CType(resources.GetObject("btnRemoveOtherTrade.Image"), System.Drawing.Image)
        Me.btnRemoveOtherTrade.Location = New System.Drawing.Point(176, 304)
        Me.btnRemoveOtherTrade.Name = "btnRemoveOtherTrade"
        Me.btnRemoveOtherTrade.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveOtherTrade.TabIndex = 3
        Me.btnRemoveOtherTrade.Text = "Remove"
        '
        'btnEditOtherTrade
        '
        Me.btnEditOtherTrade.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEditOtherTrade.Image = CType(resources.GetObject("btnEditOtherTrade.Image"), System.Drawing.Image)
        Me.btnEditOtherTrade.Location = New System.Drawing.Point(96, 304)
        Me.btnEditOtherTrade.Name = "btnEditOtherTrade"
        Me.btnEditOtherTrade.Size = New System.Drawing.Size(72, 23)
        Me.btnEditOtherTrade.TabIndex = 2
        Me.btnEditOtherTrade.Text = "Edit..."
        '
        'btnAddOtherTrade
        '
        Me.btnAddOtherTrade.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddOtherTrade.Image = CType(resources.GetObject("btnAddOtherTrade.Image"), System.Drawing.Image)
        Me.btnAddOtherTrade.Location = New System.Drawing.Point(16, 304)
        Me.btnAddOtherTrade.Name = "btnAddOtherTrade"
        Me.btnAddOtherTrade.Size = New System.Drawing.Size(72, 23)
        Me.btnAddOtherTrade.TabIndex = 1
        Me.btnAddOtherTrade.Text = "Add..."
        '
        'dgOtherTrades
        '
        Me.dgOtherTrades.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgOtherTrades.DataSource = Me.dvOtherTrades
        '
        'dgOtherTrades.EmbeddedNavigator
        '
        Me.dgOtherTrades.EmbeddedNavigator.Name = ""
        Me.dgOtherTrades.Location = New System.Drawing.Point(16, 32)
        Me.dgOtherTrades.MainView = Me.gvOtherTrades
        Me.dgOtherTrades.Name = "dgOtherTrades"
        Me.dgOtherTrades.Size = New System.Drawing.Size(472, 264)
        Me.dgOtherTrades.TabIndex = 0
        Me.dgOtherTrades.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvOtherTrades})
        '
        'dvOtherTrades
        '
        Me.dvOtherTrades.RowFilter = "NIType = 'OT'"
        Me.dvOtherTrades.Table = Me.dataSet.VJobsNonCoreSalesItems
        '
        'gvOtherTrades
        '
        Me.gvOtherTrades.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.gvOtherTrades.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvOtherTrades.Appearance.FocusedCell.Options.UseBackColor = True
        Me.gvOtherTrades.Appearance.FocusedCell.Options.UseForeColor = True
        Me.gvOtherTrades.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.gvOtherTrades.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gvOtherTrades.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvOtherTrades.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvOtherTrades.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colNIName, Me.colNIUserType, Me.colNIOrderedBy1, Me.colNIPaidBy1})
        Me.gvOtherTrades.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.gvOtherTrades.GridControl = Me.dgOtherTrades
        Me.gvOtherTrades.Name = "gvOtherTrades"
        Me.gvOtherTrades.OptionsBehavior.Editable = False
        Me.gvOtherTrades.OptionsCustomization.AllowFilter = False
        Me.gvOtherTrades.OptionsNavigation.AutoFocusNewRow = True
        Me.gvOtherTrades.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.gvOtherTrades.OptionsView.ShowGroupPanel = False
        Me.gvOtherTrades.OptionsView.ShowHorzLines = False
        Me.gvOtherTrades.OptionsView.ShowIndicator = False
        Me.gvOtherTrades.OptionsView.ShowVertLines = False
        Me.gvOtherTrades.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colNIName, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colNIName
        '
        Me.colNIName.Caption = "Name"
        Me.colNIName.FieldName = "NIName"
        Me.colNIName.Name = "colNIName"
        Me.colNIName.Visible = True
        Me.colNIName.VisibleIndex = 0
        '
        'colNIUserType
        '
        Me.colNIUserType.Caption = "Trade Type"
        Me.colNIUserType.FieldName = "NIUserType"
        Me.colNIUserType.Name = "colNIUserType"
        Me.colNIUserType.Visible = True
        Me.colNIUserType.VisibleIndex = 1
        '
        'colNIOrderedBy1
        '
        Me.colNIOrderedBy1.Caption = "Ordered By"
        Me.colNIOrderedBy1.FieldName = "NIOrderedBy"
        Me.colNIOrderedBy1.Name = "colNIOrderedBy1"
        Me.colNIOrderedBy1.Visible = True
        Me.colNIOrderedBy1.VisibleIndex = 2
        '
        'colNIPaidBy1
        '
        Me.colNIPaidBy1.Caption = "Paid By"
        Me.colNIPaidBy1.FieldName = "NIPaidBy"
        Me.colNIPaidBy1.Name = "colNIPaidBy1"
        Me.colNIPaidBy1.Visible = True
        Me.colNIPaidBy1.VisibleIndex = 3
        '
        'pnlIntro
        '
        Me.pnlIntro.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.pnlIntro.Appearance.Options.UseBackColor = True
        Me.pnlIntro.Controls.Add(Me.PictureBox1)
        Me.pnlIntro.Controls.Add(Me.Label44)
        Me.pnlIntro.Controls.Add(Me.PictureEdit4)
        Me.pnlIntro.Controls.Add(Me.Label43)
        Me.pnlIntro.Controls.Add(Me.Label42)
        Me.pnlIntro.Controls.Add(Me.PictureEdit3)
        Me.pnlIntro.Controls.Add(Me.Label48)
        Me.pnlIntro.Controls.Add(Me.PictureEdit1)
        Me.pnlIntro.Location = New System.Drawing.Point(48, 40)
        Me.pnlIntro.Name = "pnlIntro"
        Me.pnlIntro.Size = New System.Drawing.Size(600, 432)
        Me.pnlIntro.TabIndex = 14
        Me.pnlIntro.Text = "Introduction"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(96, 90)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(16, 16)
        Me.PictureBox1.TabIndex = 6
        Me.PictureBox1.TabStop = False
        '
        'Label44
        '
        Me.Label44.BackColor = System.Drawing.Color.Transparent
        Me.Label44.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.Location = New System.Drawing.Point(96, 264)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(472, 32)
        Me.Label44.TabIndex = 5
        Me.Label44.Text = "Where indirect tax applies, all information entered into the Job screen is includ" & _
        "ing indirect tax."
        '
        'PictureEdit4
        '
        Me.PictureEdit4.EditValue = CType(resources.GetObject("PictureEdit4.EditValue"), Object)
        Me.PictureEdit4.Location = New System.Drawing.Point(72, 264)
        Me.PictureEdit4.Name = "PictureEdit4"
        '
        'PictureEdit4.Properties
        '
        Me.PictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit4.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit4.Size = New System.Drawing.Size(16, 16)
        Me.PictureEdit4.TabIndex = 4
        '
        'Label43
        '
        Me.Label43.BackColor = System.Drawing.Color.Transparent
        Me.Label43.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.Location = New System.Drawing.Point(96, 192)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(472, 64)
        Me.Label43.TabIndex = 3
        Me.Label43.Text = "Any data or inputs that have a * must be entered to make the status of the job ""C" & _
        "omplete"". Any data or inputs with * that have not yet been entered will have <In" & _
        "complete> status.  These fields must be entered in order to make the job status " & _
        """Complete"". You may have to enter zero into a field in order to make the status " & _
        "of the job ""Complete"". "
        '
        'Label42
        '
        Me.Label42.BackColor = System.Drawing.Color.Transparent
        Me.Label42.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.Location = New System.Drawing.Point(120, 88)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(280, 23)
        Me.Label42.TabIndex = 2
        Me.Label42.Text = "Read below before you begin:"
        '
        'PictureEdit3
        '
        Me.PictureEdit3.EditValue = CType(resources.GetObject("PictureEdit3.EditValue"), Object)
        Me.PictureEdit3.Location = New System.Drawing.Point(72, 192)
        Me.PictureEdit3.Name = "PictureEdit3"
        '
        'PictureEdit3.Properties
        '
        Me.PictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit3.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit3.Size = New System.Drawing.Size(16, 16)
        Me.PictureEdit3.TabIndex = 0
        '
        'Label48
        '
        Me.Label48.BackColor = System.Drawing.Color.Transparent
        Me.Label48.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.Location = New System.Drawing.Point(96, 128)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(472, 56)
        Me.Label48.TabIndex = 5
        Me.Label48.Text = "A job may have a status of ""Complete"" or ""Incomplete"". This has nothing to do wit" & _
        "h whether a job has been finished or not.  It actually refers to whether or not " & _
        "all the required information has been entered for this job. "
        '
        'PictureEdit1
        '
        Me.PictureEdit1.EditValue = CType(resources.GetObject("PictureEdit1.EditValue"), Object)
        Me.PictureEdit1.Location = New System.Drawing.Point(72, 128)
        Me.PictureEdit1.Name = "PictureEdit1"
        '
        'PictureEdit1.Properties
        '
        Me.PictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit1.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit1.Size = New System.Drawing.Size(16, 16)
        Me.PictureEdit1.TabIndex = 4
        '
        'pnlResponsibilities
        '
        Me.pnlResponsibilities.Controls.Add(Me.dgResponsibilities)
        Me.pnlResponsibilities.Location = New System.Drawing.Point(32, 176)
        Me.pnlResponsibilities.Name = "pnlResponsibilities"
        Me.pnlResponsibilities.Size = New System.Drawing.Size(584, 216)
        Me.pnlResponsibilities.TabIndex = 13
        Me.pnlResponsibilities.Text = "Responsibilities"
        '
        'dgResponsibilities
        '
        Me.dgResponsibilities.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgResponsibilities.DataSource = Me.dataSet.VJobs_Expenses
        '
        'dgResponsibilities.EmbeddedNavigator
        '
        Me.dgResponsibilities.EmbeddedNavigator.Name = ""
        Me.dgResponsibilities.Location = New System.Drawing.Point(16, 32)
        Me.dgResponsibilities.MainView = Me.gvResponsibilities
        Me.dgResponsibilities.Name = "dgResponsibilities"
        Me.dgResponsibilities.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtPercentOfJob})
        Me.dgResponsibilities.Size = New System.Drawing.Size(552, 168)
        Me.dgResponsibilities.TabIndex = 21
        Me.dgResponsibilities.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvResponsibilities})
        '
        'gvResponsibilities
        '
        Me.gvResponsibilities.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvResponsibilities.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvResponsibilities.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvResponsibilities.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvResponsibilities.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvResponsibilities.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvResponsibilities.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvResponsibilities.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvResponsibilities.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvResponsibilities.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvResponsibilities.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvResponsibilities.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvResponsibilities.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvResponsibilities.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvResponsibilities.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvResponsibilities.Appearance.VertLine.Options.UseBackColor = True
        Me.gvResponsibilities.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.BandedGridColumn7, Me.BandedGridColumn12, Me.BandedGridColumn8})
        Me.gvResponsibilities.CustomizationFormBounds = New System.Drawing.Rectangle(592, 331, 208, 156)
        Me.gvResponsibilities.GridControl = Me.dgResponsibilities
        Me.gvResponsibilities.GroupCount = 1
        Me.gvResponsibilities.GroupSummary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "JEPercentageOfJob", Nothing, "(Total = {0:p})")})
        Me.gvResponsibilities.Name = "gvResponsibilities"
        Me.gvResponsibilities.OptionsCustomization.AllowFilter = False
        Me.gvResponsibilities.OptionsNavigation.AutoFocusNewRow = True
        Me.gvResponsibilities.OptionsView.ShowGroupPanel = False
        Me.gvResponsibilities.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.BandedGridColumn8, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.BandedGridColumn7, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'BandedGridColumn7
        '
        Me.BandedGridColumn7.Caption = "Name"
        Me.BandedGridColumn7.FieldName = "EXName"
        Me.BandedGridColumn7.Name = "BandedGridColumn7"
        Me.BandedGridColumn7.OptionsColumn.AllowEdit = False
        Me.BandedGridColumn7.OptionsColumn.AllowFocus = False
        Me.BandedGridColumn7.Visible = True
        Me.BandedGridColumn7.VisibleIndex = 0
        Me.BandedGridColumn7.Width = 490
        '
        'BandedGridColumn12
        '
        Me.BandedGridColumn12.Caption = "Percent of Job *"
        Me.BandedGridColumn12.ColumnEdit = Me.txtPercentOfJob
        Me.BandedGridColumn12.FieldName = "JEPercentageOfJob"
        Me.BandedGridColumn12.Name = "BandedGridColumn12"
        Me.BandedGridColumn12.Visible = True
        Me.BandedGridColumn12.VisibleIndex = 1
        Me.BandedGridColumn12.Width = 349
        '
        'txtPercentOfJob
        '
        Me.txtPercentOfJob.AutoHeight = False
        Me.txtPercentOfJob.DisplayFormat.FormatString = "p"
        Me.txtPercentOfJob.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtPercentOfJob.EditFormat.FormatString = "p"
        Me.txtPercentOfJob.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtPercentOfJob.Name = "txtPercentOfJob"
        Me.txtPercentOfJob.NullText = "<Incomplete>"
        '
        'BandedGridColumn8
        '
        Me.BandedGridColumn8.Caption = "Group"
        Me.BandedGridColumn8.FieldName = "EGName"
        Me.BandedGridColumn8.Name = "BandedGridColumn8"
        Me.BandedGridColumn8.OptionsColumn.AllowEdit = False
        Me.BandedGridColumn8.OptionsColumn.AllowFocus = False
        Me.BandedGridColumn8.Visible = True
        Me.BandedGridColumn8.VisibleIndex = 2
        Me.BandedGridColumn8.Width = 161
        '
        'pnlJobIssues
        '
        Me.pnlJobIssues.Controls.Add(Me.dgJobIssues)
        Me.HelpProvider1.SetHelpString(Me.pnlJobIssues, """HELPEP{TFUCKING OK BUTTON IS CLICKING! "" ")
        Me.pnlJobIssues.Location = New System.Drawing.Point(40, 8)
        Me.pnlJobIssues.Name = "pnlJobIssues"
        Me.HelpProvider1.SetShowHelp(Me.pnlJobIssues, True)
        Me.pnlJobIssues.Size = New System.Drawing.Size(600, 456)
        Me.pnlJobIssues.TabIndex = 32
        Me.pnlJobIssues.Text = "Job Issues"
        '
        'dgJobIssues
        '
        Me.dgJobIssues.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgJobIssues.DataSource = Me.dataSet.VJobs_JobIssues
        '
        'dgJobIssues.EmbeddedNavigator
        '
        Me.dgJobIssues.EmbeddedNavigator.Name = ""
        Me.dgJobIssues.Location = New System.Drawing.Point(16, 32)
        Me.dgJobIssues.MainView = Me.GridView3
        Me.dgJobIssues.Name = "dgJobIssues"
        Me.HelpProvider1.SetShowHelp(Me.dgJobIssues, True)
        Me.dgJobIssues.Size = New System.Drawing.Size(568, 408)
        Me.dgJobIssues.TabIndex = 2
        Me.dgJobIssues.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView3})
        '
        'GridView3
        '
        Me.GridView3.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView3.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView3.Appearance.FocusedRow.Options.UseBackColor = True
        Me.GridView3.Appearance.FocusedRow.Options.UseForeColor = True
        Me.GridView3.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView3.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView3.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView3.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.GridView3.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.GridView3.Appearance.HorzLine.Options.UseBackColor = True
        Me.GridView3.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView3.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView3.Appearance.SelectedRow.Options.UseBackColor = True
        Me.GridView3.Appearance.SelectedRow.Options.UseForeColor = True
        Me.GridView3.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.GridView3.Appearance.VertLine.Options.UseBackColor = True
        Me.GridView3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colIfOccured1, Me.colIfOverSchedule1, Me.colDescription1})
        Me.GridView3.GridControl = Me.dgJobIssues
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsCustomization.AllowFilter = False
        Me.GridView3.OptionsView.ShowGroupPanel = False
        Me.GridView3.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colDescription1, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colIfOccured1
        '
        Me.colIfOccured1.Caption = "Issue occured"
        Me.colIfOccured1.FieldName = "IfOccured"
        Me.colIfOccured1.Name = "colIfOccured1"
        Me.colIfOccured1.Visible = True
        Me.colIfOccured1.VisibleIndex = 1
        Me.colIfOccured1.Width = 73
        '
        'colIfOverSchedule1
        '
        Me.colIfOverSchedule1.Caption = "Issue pushed the job over schedule"
        Me.colIfOverSchedule1.FieldName = "IfOverSchedule"
        Me.colIfOverSchedule1.Name = "colIfOverSchedule1"
        Me.colIfOverSchedule1.Visible = True
        Me.colIfOverSchedule1.VisibleIndex = 2
        Me.colIfOverSchedule1.Width = 179
        '
        'colDescription1
        '
        Me.colDescription1.Caption = "Issue"
        Me.colDescription1.FieldName = "JIDescription"
        Me.colDescription1.Name = "colDescription1"
        Me.colDescription1.OptionsColumn.AllowEdit = False
        Me.colDescription1.OptionsColumn.AllowFocus = False
        Me.colDescription1.Visible = True
        Me.colDescription1.VisibleIndex = 0
        Me.colDescription1.Width = 302
        '
        'dvJobs_Materials
        '
        Me.dvJobs_Materials.AllowNew = False
        Me.dvJobs_Materials.RowFilter = "MTStocked = 0"
        '
        'dvJobs_Granite
        '
        Me.dvJobs_Granite.AllowNew = False
        Me.dvJobs_Granite.RowFilter = "MTStocked = 1"
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT JTID, JTName FROM JobTypes"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO JobTypes(JTName) VALUES (@JTName); SELECT JTID, JTName FROM JobTypes " & _
        "WHERE (JTID = @@IDENTITY)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JTName", System.Data.SqlDbType.VarChar, 50, "JTName"))
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE JobTypes SET JTName = @JTName WHERE (JTID = @Original_JTID) AND (JTName = " & _
        "@Original_JTName OR @Original_JTName IS NULL AND JTName IS NULL); SELECT JTID, J" & _
        "TName FROM JobTypes WHERE (JTID = @JTID)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JTName", System.Data.SqlDbType.VarChar, 50, "JTName"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JTName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JTName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JTID", System.Data.SqlDbType.Int, 4, "JTID"))
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM JobTypes WHERE (JTID = @Original_JTID) AND (JTName = @Original_JTName" & _
        " OR @Original_JTName IS NULL AND JTName IS NULL)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JTName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JTName", System.Data.DataRowVersion.Original, Nothing))
        '
        'daJobTypes
        '
        Me.daJobTypes.DeleteCommand = Me.SqlDeleteCommand2
        Me.daJobTypes.InsertCommand = Me.SqlInsertCommand2
        Me.daJobTypes.SelectCommand = Me.SqlSelectCommand2
        Me.daJobTypes.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "JobTypes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("JTID", "JTID"), New System.Data.Common.DataColumnMapping("JTName", "JTName")})})
        Me.daJobTypes.UpdateCommand = Me.SqlUpdateCommand2
        '
        'daJobs_Expenses
        '
        Me.daJobs_Expenses.DeleteCommand = Me.SqlDeleteCommand4
        Me.daJobs_Expenses.InsertCommand = Me.SqlInsertCommand4
        Me.daJobs_Expenses.SelectCommand = Me.SqlSelectCommand4
        Me.daJobs_Expenses.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VJobs_Expenses", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("JBID", "JBID"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("JEAsPercent", "JEAsPercent"), New System.Data.Common.DataColumnMapping("JECharge", "JECharge"), New System.Data.Common.DataColumnMapping("JEPercent", "JEPercent"), New System.Data.Common.DataColumnMapping("JEPercentageOfJob", "JEPercentageOfJob"), New System.Data.Common.DataColumnMapping("JEHours", "JEHours")})})
        Me.daJobs_Expenses.UpdateCommand = Me.SqlUpdateCommand4
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = "DELETE FROM Jobs_Expenses WHERE (BRID = @Original_BRID) AND (EXID = @Original_EXI" & _
        "D) AND (JBID = @Original_JBID) AND (JEAsPercent = @Original_JEAsPercent OR @Orig" & _
        "inal_JEAsPercent IS NULL AND JEAsPercent IS NULL) AND (JECharge = @Original_JECh" & _
        "arge OR @Original_JECharge IS NULL AND JECharge IS NULL) AND (JEHours = @Origina" & _
        "l_JEHours OR @Original_JEHours IS NULL AND JEHours IS NULL) AND (JEPercent = @Or" & _
        "iginal_JEPercent OR @Original_JEPercent IS NULL AND JEPercent IS NULL) AND (JEPe" & _
        "rcentageOfJob = @Original_JEPercentageOfJob OR @Original_JEPercentageOfJob IS NU" & _
        "LL AND JEPercentageOfJob IS NULL)"
        Me.SqlDeleteCommand4.Connection = Me.SqlConnection
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JEAsPercent", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JEAsPercent", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JECharge", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JECharge", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JEHours", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JEHours", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JEPercent", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(4, Byte), "JEPercent", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JEPercentageOfJob", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(4, Byte), "JEPercentageOfJob", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = "INSERT INTO Jobs_Expenses(BRID, JBID, EXID, JEAsPercent, JECharge, JEPercent, JEP" & _
        "ercentageOfJob, JEHours) VALUES (@BRID, @JBID, @EXID, @JEAsPercent, @JECharge, @" & _
        "JEPercent, @JEPercentageOfJob, @JEHours); SELECT BRID, JBID, EXID, JEAsPercent, " & _
        "JECharge, JEPercent, JEPercentageOfJob, JEHours FROM Jobs_Expenses WHERE (BRID =" & _
        " @BRID) AND (EXID = @EXID) AND (JBID = @JBID)"
        Me.SqlInsertCommand4.Connection = Me.SqlConnection
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JEAsPercent", System.Data.SqlDbType.Int, 4, "JEAsPercent"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JECharge", System.Data.SqlDbType.Money, 8, "JECharge"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JEPercent", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(4, Byte), "JEPercent", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JEPercentageOfJob", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(4, Byte), "JEPercentageOfJob", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JEHours", System.Data.SqlDbType.Int, 4, "JEHours"))
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT BRID, JBID, EXID, JEAsPercent, JECharge, JEPercent, JEPercentageOfJob, EXN" & _
        "ame, EGName, EGType, PMAllowValueInJobScreen, EGID, JEHours, PMAllowHoursInJobSc" & _
        "reen FROM VJobs_Expenses WHERE (BRID = @BRID) AND (JBID = @JBID)"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection
        Me.SqlSelectCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = "UPDATE Jobs_Expenses SET BRID = @BRID, JBID = @JBID, EXID = @EXID, JEAsPercent = " & _
        "@JEAsPercent, JECharge = @JECharge, JEPercent = @JEPercent, JEPercentageOfJob = " & _
        "@JEPercentageOfJob, JEHours = @JEHours WHERE (BRID = @Original_BRID) AND (EXID =" & _
        " @Original_EXID) AND (JBID = @Original_JBID) AND (JEAsPercent = @Original_JEAsPe" & _
        "rcent OR @Original_JEAsPercent IS NULL AND JEAsPercent IS NULL) AND (JECharge = " & _
        "@Original_JECharge OR @Original_JECharge IS NULL AND JECharge IS NULL) AND (JEHo" & _
        "urs = @Original_JEHours OR @Original_JEHours IS NULL AND JEHours IS NULL) AND (J" & _
        "EPercent = @Original_JEPercent OR @Original_JEPercent IS NULL AND JEPercent IS N" & _
        "ULL) AND (JEPercentageOfJob = @Original_JEPercentageOfJob OR @Original_JEPercent" & _
        "ageOfJob IS NULL AND JEPercentageOfJob IS NULL); SELECT BRID, JBID, EXID, JEAsPe" & _
        "rcent, JECharge, JEPercent, JEPercentageOfJob, JEHours FROM Jobs_Expenses WHERE " & _
        "(BRID = @BRID) AND (EXID = @EXID) AND (JBID = @JBID)"
        Me.SqlUpdateCommand4.Connection = Me.SqlConnection
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JEAsPercent", System.Data.SqlDbType.Int, 4, "JEAsPercent"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JECharge", System.Data.SqlDbType.Money, 8, "JECharge"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JEPercent", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(4, Byte), "JEPercent", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JEPercentageOfJob", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(4, Byte), "JEPercentageOfJob", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JEHours", System.Data.SqlDbType.Int, 4, "JEHours"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JEAsPercent", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JEAsPercent", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JECharge", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JECharge", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JEHours", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JEHours", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JEPercent", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(4, Byte), "JEPercent", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JEPercentageOfJob", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(4, Byte), "JEPercentageOfJob", System.Data.DataRowVersion.Original, Nothing))
        '
        'daClientPayments
        '
        Me.daClientPayments.DeleteCommand = Me.SqlDeleteCommand7
        Me.daClientPayments.InsertCommand = Me.SqlInsertCommand5
        Me.daClientPayments.SelectCommand = Me.SqlSelectCommand5
        Me.daClientPayments.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "ClientPayments", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("JBID", "JBID"), New System.Data.Common.DataColumnMapping("CPID", "CPID"), New System.Data.Common.DataColumnMapping("CPDate", "CPDate"), New System.Data.Common.DataColumnMapping("CPAmount", "CPAmount"), New System.Data.Common.DataColumnMapping("CPNotes", "CPNotes"), New System.Data.Common.DataColumnMapping("CPType", "CPType")})})
        Me.daClientPayments.UpdateCommand = Me.SqlUpdateCommand7
        '
        'SqlDeleteCommand7
        '
        Me.SqlDeleteCommand7.CommandText = "DELETE FROM ClientPayments WHERE (BRID = @Original_BRID) AND (CPID = @Original_CP" & _
        "ID) AND (JBID = @Original_JBID) AND (CPAmount = @Original_CPAmount OR @Original_" & _
        "CPAmount IS NULL AND CPAmount IS NULL) AND (CPDate = @Original_CPDate) AND (CPNo" & _
        "tes = @Original_CPNotes OR @Original_CPNotes IS NULL AND CPNotes IS NULL) AND (C" & _
        "PType = @Original_CPType OR @Original_CPType IS NULL AND CPType IS NULL)"
        Me.SqlDeleteCommand7.Connection = Me.SqlConnection
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CPID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CPID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CPAmount", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CPAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CPDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CPDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CPNotes", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CPNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CPType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CPType", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand5
        '
        Me.SqlInsertCommand5.CommandText = "INSERT INTO ClientPayments(BRID, JBID, CPDate, CPAmount, CPNotes, CPType) VALUES " & _
        "(@BRID, @JBID, @CPDate, @CPAmount, @CPNotes, @CPType); SELECT BRID, JBID, CPID, " & _
        "CPDate, CPAmount, CPNotes, CPType FROM ClientPayments WHERE (BRID = @BRID) AND (" & _
        "CPID = @@IDENTITY) AND (JBID = @JBID)"
        Me.SqlInsertCommand5.Connection = Me.SqlConnection
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CPDate", System.Data.SqlDbType.DateTime, 8, "CPDate"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CPAmount", System.Data.SqlDbType.Money, 8, "CPAmount"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CPNotes", System.Data.SqlDbType.VarChar, 100, "CPNotes"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CPType", System.Data.SqlDbType.VarChar, 2, "CPType"))
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT BRID, JBID, CPID, CPDate, CPAmount, CPNotes, CPType FROM ClientPayments WH" & _
        "ERE (BRID = @BRID) AND (JBID = @JBID)"
        Me.SqlSelectCommand5.Connection = Me.SqlConnection
        Me.SqlSelectCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlUpdateCommand7
        '
        Me.SqlUpdateCommand7.CommandText = "UPDATE ClientPayments SET BRID = @BRID, JBID = @JBID, CPDate = @CPDate, CPAmount " & _
        "= @CPAmount, CPNotes = @CPNotes, CPType = @CPType WHERE (BRID = @Original_BRID) " & _
        "AND (CPID = @Original_CPID) AND (JBID = @Original_JBID) AND (CPAmount = @Origina" & _
        "l_CPAmount OR @Original_CPAmount IS NULL AND CPAmount IS NULL) AND (CPDate = @Or" & _
        "iginal_CPDate) AND (CPNotes = @Original_CPNotes OR @Original_CPNotes IS NULL AND" & _
        " CPNotes IS NULL) AND (CPType = @Original_CPType OR @Original_CPType IS NULL AND" & _
        " CPType IS NULL); SELECT BRID, JBID, CPID, CPDate, CPAmount, CPNotes, CPType FRO" & _
        "M ClientPayments WHERE (BRID = @BRID) AND (CPID = @CPID) AND (JBID = @JBID)"
        Me.SqlUpdateCommand7.Connection = Me.SqlConnection
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CPDate", System.Data.SqlDbType.DateTime, 8, "CPDate"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CPAmount", System.Data.SqlDbType.Money, 8, "CPAmount"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CPNotes", System.Data.SqlDbType.VarChar, 100, "CPNotes"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CPType", System.Data.SqlDbType.VarChar, 2, "CPType"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CPID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CPID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CPAmount", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CPAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CPDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CPDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CPNotes", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CPNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CPType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CPType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CPID", System.Data.SqlDbType.BigInt, 8, "CPID"))
        '
        'daJobNonCoreSalesItems
        '
        Me.daJobNonCoreSalesItems.DeleteCommand = Me.SqlDeleteCommand9
        Me.daJobNonCoreSalesItems.InsertCommand = Me.SqlInsertCommand11
        Me.daJobNonCoreSalesItems.SelectCommand = Me.SqlSelectCommand11
        Me.daJobNonCoreSalesItems.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VJobsNonCoreSalesItems", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("NIID", "NIID"), New System.Data.Common.DataColumnMapping("JBID", "JBID"), New System.Data.Common.DataColumnMapping("NIName", "NIName"), New System.Data.Common.DataColumnMapping("NIDesc", "NIDesc"), New System.Data.Common.DataColumnMapping("NICost", "NICost"), New System.Data.Common.DataColumnMapping("NIPrice", "NIPrice"), New System.Data.Common.DataColumnMapping("NIType", "NIType"), New System.Data.Common.DataColumnMapping("NIUserType", "NIUserType"), New System.Data.Common.DataColumnMapping("NIBrand", "NIBrand"), New System.Data.Common.DataColumnMapping("NISupplier", "NISupplier"), New System.Data.Common.DataColumnMapping("EXIDRep", "EXIDRep"), New System.Data.Common.DataColumnMapping("NICommissionAsPercent", "NICommissionAsPercent"), New System.Data.Common.DataColumnMapping("NICommissionAmount", "NICommissionAmount"), New System.Data.Common.DataColumnMapping("NICommissionPercent", "NICommissionPercent"), New System.Data.Common.DataColumnMapping("NIOrderedByClient", "NIOrderedByClient"), New System.Data.Common.DataColumnMapping("NIPaidByClient", "NIPaidByClient"), New System.Data.Common.DataColumnMapping("NIDeliveredToClient", "NIDeliveredToClient"), New System.Data.Common.DataColumnMapping("NIDeliveryDate", "NIDeliveryDate"), New System.Data.Common.DataColumnMapping("NIPayCommission", "NIPayCommission"), New System.Data.Common.DataColumnMapping("NIReceived", "NIReceived")})})
        Me.daJobNonCoreSalesItems.UpdateCommand = Me.SqlUpdateCommand9
        '
        'SqlDeleteCommand9
        '
        Me.SqlDeleteCommand9.CommandText = "DELETE FROM JobsNonCoreSalesItems WHERE (BRID = @Original_BRID) AND (NIID = @Orig" & _
        "inal_NIID) AND (EXIDRep = @Original_EXIDRep OR @Original_EXIDRep IS NULL AND EXI" & _
        "DRep IS NULL) AND (JBID = @Original_JBID) AND (NIBrand = @Original_NIBrand OR @O" & _
        "riginal_NIBrand IS NULL AND NIBrand IS NULL) AND (NICommissionAmount = @Original" & _
        "_NICommissionAmount OR @Original_NICommissionAmount IS NULL AND NICommissionAmou" & _
        "nt IS NULL) AND (NICommissionAsPercent = @Original_NICommissionAsPercent OR @Ori" & _
        "ginal_NICommissionAsPercent IS NULL AND NICommissionAsPercent IS NULL) AND (NICo" & _
        "mmissionPercent = @Original_NICommissionPercent OR @Original_NICommissionPercent" & _
        " IS NULL AND NICommissionPercent IS NULL) AND (NICost = @Original_NICost OR @Ori" & _
        "ginal_NICost IS NULL AND NICost IS NULL) AND (NIDeliveredToClient = @Original_NI" & _
        "DeliveredToClient OR @Original_NIDeliveredToClient IS NULL AND NIDeliveredToClie" & _
        "nt IS NULL) AND (NIDeliveryDate = @Original_NIDeliveryDate OR @Original_NIDelive" & _
        "ryDate IS NULL AND NIDeliveryDate IS NULL) AND (NIDesc = @Original_NIDesc OR @Or" & _
        "iginal_NIDesc IS NULL AND NIDesc IS NULL) AND (NIName = @Original_NIName OR @Ori" & _
        "ginal_NIName IS NULL AND NIName IS NULL) AND (NIOrderedByClient = @Original_NIOr" & _
        "deredByClient OR @Original_NIOrderedByClient IS NULL AND NIOrderedByClient IS NU" & _
        "LL) AND (NIPaidByClient = @Original_NIPaidByClient OR @Original_NIPaidByClient I" & _
        "S NULL AND NIPaidByClient IS NULL) AND (NIPayCommission = @Original_NIPayCommiss" & _
        "ion OR @Original_NIPayCommission IS NULL AND NIPayCommission IS NULL) AND (NIPri" & _
        "ce = @Original_NIPrice OR @Original_NIPrice IS NULL AND NIPrice IS NULL) AND (NI" & _
        "Received = @Original_NIReceived OR @Original_NIReceived IS NULL AND NIReceived I" & _
        "S NULL) AND (NISupplier = @Original_NISupplier OR @Original_NISupplier IS NULL A" & _
        "ND NISupplier IS NULL) AND (NIType = @Original_NIType) AND (NIUserType = @Origin" & _
        "al_NIUserType OR @Original_NIUserType IS NULL AND NIUserType IS NULL)"
        Me.SqlDeleteCommand9.Connection = Me.SqlConnection
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDRep", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDRep", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIBrand", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIBrand", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICommissionAmount", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NICommissionAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICommissionAsPercent", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NICommissionAsPercent", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICommissionPercent", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "NICommissionPercent", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NICost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIDeliveredToClient", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIDeliveredToClient", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIDeliveryDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIDeliveryDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIDesc", System.Data.SqlDbType.VarChar, 2000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIDesc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIOrderedByClient", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIOrderedByClient", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIPaidByClient", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIPaidByClient", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIPayCommission", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIPayCommission", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIPrice", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIPrice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIReceived", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIReceived", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NISupplier", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NISupplier", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIUserType", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIUserType", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand11
        '
        Me.SqlInsertCommand11.CommandText = "INSERT INTO JobsNonCoreSalesItems (BRID, JBID, NIName, NIDesc, NICost, NIPrice, N" & _
        "IType, NIUserType, NIBrand, NISupplier, EXIDRep, NICommissionAsPercent, NICommis" & _
        "sionAmount, NICommissionPercent, NIOrderedByClient, NIPaidByClient, NIDeliveredT" & _
        "oClient, NIDeliveryDate, NIPayCommission, NIReceived) VALUES (@BRID, @JBID, @NIN" & _
        "ame, @NIDesc, @NICost, @NIPrice, @NIType, @NIUserType, @NIBrand, @NISupplier, @E" & _
        "XIDRep, @NICommissionAsPercent, @NICommissionAmount, @NICommissionPercent, @NIOr" & _
        "deredByClient, @NIPaidByClient, @NIDeliveredToClient, @NIDeliveryDate, @NIPayCom" & _
        "mission, @NIReceived); SELECT BRID, NIID, JBID, NIName, NIDesc, NICost, NIPrice," & _
        " NIType, NIUserType, NIBrand, NISupplier, EXIDRep, NICommissionAsPercent, NIComm" & _
        "issionAmount, NICommissionPercent, NIOrderedByClient, NIPaidByClient, NIDelivere" & _
        "dToClient, NIDeliveryDate, NIOrderedBy, NIPaidBy, EXName, NIPayCommission, NIRec" & _
        "eived FROM VJobsNonCoreSalesItems WHERE (BRID = @BRID) AND (NIID = @@IDENTITY)"
        Me.SqlInsertCommand11.Connection = Me.SqlConnection
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIName", System.Data.SqlDbType.VarChar, 50, "NIName"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIDesc", System.Data.SqlDbType.VarChar, 2000, "NIDesc"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICost", System.Data.SqlDbType.Money, 8, "NICost"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIPrice", System.Data.SqlDbType.Money, 8, "NIPrice"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIType", System.Data.SqlDbType.VarChar, 2, "NIType"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIUserType", System.Data.SqlDbType.VarChar, 50, "NIUserType"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIBrand", System.Data.SqlDbType.VarChar, 50, "NIBrand"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NISupplier", System.Data.SqlDbType.VarChar, 50, "NISupplier"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDRep", System.Data.SqlDbType.Int, 4, "EXIDRep"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICommissionAsPercent", System.Data.SqlDbType.Int, 4, "NICommissionAsPercent"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICommissionAmount", System.Data.SqlDbType.Money, 8, "NICommissionAmount"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICommissionPercent", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "NICommissionPercent", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIOrderedByClient", System.Data.SqlDbType.Bit, 1, "NIOrderedByClient"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIPaidByClient", System.Data.SqlDbType.Bit, 1, "NIPaidByClient"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIDeliveredToClient", System.Data.SqlDbType.Bit, 1, "NIDeliveredToClient"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIDeliveryDate", System.Data.SqlDbType.DateTime, 8, "NIDeliveryDate"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIPayCommission", System.Data.SqlDbType.Bit, 1, "NIPayCommission"))
        Me.SqlInsertCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIReceived", System.Data.SqlDbType.Bit, 1, "NIReceived"))
        '
        'SqlSelectCommand11
        '
        Me.SqlSelectCommand11.CommandText = "SELECT BRID, NIID, JBID, NIName, NIDesc, NICost, NIPrice, NIType, NIUserType, NIB" & _
        "rand, NISupplier, EXIDRep, NICommissionAsPercent, NICommissionAmount, NICommissi" & _
        "onPercent, NIOrderedByClient, NIPaidByClient, NIDeliveredToClient, NIDeliveryDat" & _
        "e, NIOrderedBy, NIPaidBy, EXName, NIPayCommission, NIReceived FROM VJobsNonCoreS" & _
        "alesItems WHERE (BRID = @BRID) AND (JBID = @JBID)"
        Me.SqlSelectCommand11.Connection = Me.SqlConnection
        Me.SqlSelectCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlUpdateCommand9
        '
        Me.SqlUpdateCommand9.CommandText = "UPDATE JobsNonCoreSalesItems SET BRID = @BRID, JBID = @JBID, NIName = @NIName, NI" & _
        "Desc = @NIDesc, NICost = @NICost, NIPrice = @NIPrice, NIType = @NIType, NIUserTy" & _
        "pe = @NIUserType, NIBrand = @NIBrand, NISupplier = @NISupplier, EXIDRep = @EXIDR" & _
        "ep, NICommissionAsPercent = @NICommissionAsPercent, NICommissionAmount = @NIComm" & _
        "issionAmount, NICommissionPercent = @NICommissionPercent, NIOrderedByClient = @N" & _
        "IOrderedByClient, NIPaidByClient = @NIPaidByClient, NIDeliveredToClient = @NIDel" & _
        "iveredToClient, NIDeliveryDate = @NIDeliveryDate, NIPayCommission = @NIPayCommis" & _
        "sion, NIReceived = @NIReceived WHERE (BRID = @Original_BRID) AND (NIID = @Origin" & _
        "al_NIID) AND (EXIDRep = @Original_EXIDRep OR @Original_EXIDRep IS NULL AND EXIDR" & _
        "ep IS NULL) AND (JBID = @Original_JBID) AND (NIBrand = @Original_NIBrand OR @Ori" & _
        "ginal_NIBrand IS NULL AND NIBrand IS NULL) AND (NICommissionAmount = @Original_N" & _
        "ICommissionAmount OR @Original_NICommissionAmount IS NULL AND NICommissionAmount" & _
        " IS NULL) AND (NICommissionAsPercent = @Original_NICommissionAsPercent OR @Origi" & _
        "nal_NICommissionAsPercent IS NULL AND NICommissionAsPercent IS NULL) AND (NIComm" & _
        "issionPercent = @Original_NICommissionPercent OR @Original_NICommissionPercent I" & _
        "S NULL AND NICommissionPercent IS NULL) AND (NICost = @Original_NICost OR @Origi" & _
        "nal_NICost IS NULL AND NICost IS NULL) AND (NIDeliveredToClient = @Original_NIDe" & _
        "liveredToClient OR @Original_NIDeliveredToClient IS NULL AND NIDeliveredToClient" & _
        " IS NULL) AND (NIDeliveryDate = @Original_NIDeliveryDate OR @Original_NIDelivery" & _
        "Date IS NULL AND NIDeliveryDate IS NULL) AND (NIDesc = @Original_NIDesc OR @Orig" & _
        "inal_NIDesc IS NULL AND NIDesc IS NULL) AND (NIName = @Original_NIName OR @Origi" & _
        "nal_NIName IS NULL AND NIName IS NULL) AND (NIOrderedByClient = @Original_NIOrde" & _
        "redByClient OR @Original_NIOrderedByClient IS NULL AND NIOrderedByClient IS NULL" & _
        ") AND (NIPaidByClient = @Original_NIPaidByClient OR @Original_NIPaidByClient IS " & _
        "NULL AND NIPaidByClient IS NULL) AND (NIPayCommission = @Original_NIPayCommissio" & _
        "n OR @Original_NIPayCommission IS NULL AND NIPayCommission IS NULL) AND (NIPrice" & _
        " = @Original_NIPrice OR @Original_NIPrice IS NULL AND NIPrice IS NULL) AND (NIRe" & _
        "ceived = @Original_NIReceived OR @Original_NIReceived IS NULL AND NIReceived IS " & _
        "NULL) AND (NISupplier = @Original_NISupplier OR @Original_NISupplier IS NULL AND" & _
        " NISupplier IS NULL) AND (NIType = @Original_NIType) AND (NIUserType = @Original" & _
        "_NIUserType OR @Original_NIUserType IS NULL AND NIUserType IS NULL); SELECT BRID" & _
        ", NIID, JBID, NIName, NIDesc, NICost, NIPrice, NIType, NIUserType, NIBrand, NISu" & _
        "pplier, EXIDRep, NICommissionAsPercent, NICommissionAmount, NICommissionPercent," & _
        " NIOrderedByClient, NIPaidByClient, NIDeliveredToClient, NIDeliveryDate, NIOrder" & _
        "edBy, NIPaidBy, EXName, NIPayCommission, NIReceived FROM VJobsNonCoreSalesItems " & _
        "WHERE (BRID = @BRID) AND (NIID = @NIID)"
        Me.SqlUpdateCommand9.Connection = Me.SqlConnection
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIName", System.Data.SqlDbType.VarChar, 50, "NIName"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIDesc", System.Data.SqlDbType.VarChar, 2000, "NIDesc"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICost", System.Data.SqlDbType.Money, 8, "NICost"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIPrice", System.Data.SqlDbType.Money, 8, "NIPrice"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIType", System.Data.SqlDbType.VarChar, 2, "NIType"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIUserType", System.Data.SqlDbType.VarChar, 50, "NIUserType"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIBrand", System.Data.SqlDbType.VarChar, 50, "NIBrand"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NISupplier", System.Data.SqlDbType.VarChar, 50, "NISupplier"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDRep", System.Data.SqlDbType.Int, 4, "EXIDRep"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICommissionAsPercent", System.Data.SqlDbType.Int, 4, "NICommissionAsPercent"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICommissionAmount", System.Data.SqlDbType.Money, 8, "NICommissionAmount"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NICommissionPercent", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "NICommissionPercent", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIOrderedByClient", System.Data.SqlDbType.Bit, 1, "NIOrderedByClient"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIPaidByClient", System.Data.SqlDbType.Bit, 1, "NIPaidByClient"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIDeliveredToClient", System.Data.SqlDbType.Bit, 1, "NIDeliveredToClient"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIDeliveryDate", System.Data.SqlDbType.DateTime, 8, "NIDeliveryDate"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIPayCommission", System.Data.SqlDbType.Bit, 1, "NIPayCommission"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIReceived", System.Data.SqlDbType.Bit, 1, "NIReceived"))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDRep", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDRep", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIBrand", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIBrand", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICommissionAmount", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NICommissionAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICommissionAsPercent", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NICommissionAsPercent", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICommissionPercent", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "NICommissionPercent", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NICost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NICost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIDeliveredToClient", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIDeliveredToClient", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIDeliveryDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIDeliveryDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIDesc", System.Data.SqlDbType.VarChar, 2000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIDesc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIOrderedByClient", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIOrderedByClient", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIPaidByClient", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIPaidByClient", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIPayCommission", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIPayCommission", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIPrice", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIPrice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIReceived", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIReceived", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NISupplier", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NISupplier", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIUserType", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIUserType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIID", System.Data.SqlDbType.BigInt, 8, "NIID"))
        '
        'daJobs_Jobissues
        '
        Me.daJobs_Jobissues.DeleteCommand = Me.SqlDeleteCommand5
        Me.daJobs_Jobissues.InsertCommand = Me.SqlInsertCommand6
        Me.daJobs_Jobissues.SelectCommand = Me.SqlSelectCommand6
        Me.daJobs_Jobissues.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VJobs_JobIssues", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("JIID", "JIID"), New System.Data.Common.DataColumnMapping("JBID", "JBID"), New System.Data.Common.DataColumnMapping("IfOccured", "IfOccured"), New System.Data.Common.DataColumnMapping("IfOverSchedule", "IfOverSchedule")})})
        Me.daJobs_Jobissues.UpdateCommand = Me.SqlUpdateCommand5
        '
        'SqlDeleteCommand5
        '
        Me.SqlDeleteCommand5.CommandText = "DELETE FROM Jobs_JobIssues WHERE (BRID = @Original_BRID) AND (JBID = @Original_JB" & _
        "ID) AND (JIID = @Original_JIID) AND (IfOccured = @Original_IfOccured OR @Origina" & _
        "l_IfOccured IS NULL AND IfOccured IS NULL) AND (IfOverSchedule = @Original_IfOve" & _
        "rSchedule OR @Original_IfOverSchedule IS NULL AND IfOverSchedule IS NULL)"
        Me.SqlDeleteCommand5.Connection = Me.SqlConnection
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IfOccured", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IfOccured", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IfOverSchedule", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IfOverSchedule", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand6
        '
        Me.SqlInsertCommand6.CommandText = "INSERT INTO Jobs_JobIssues (BRID, JIID, JBID, IfOccured, IfOverSchedule) VALUES (" & _
        "@BRID, @JIID, @JBID, @IfOccured, @IfOverSchedule); SELECT BRID, JIID, JBID, IfOc" & _
        "cured, IfOverSchedule, JIDescription FROM VJobs_JobIssues WHERE (BRID = @BRID) A" & _
        "ND (JBID = @JBID) AND (JIID = @JIID)"
        Me.SqlInsertCommand6.Connection = Me.SqlConnection
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JIID", System.Data.SqlDbType.Int, 4, "JIID"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IfOccured", System.Data.SqlDbType.Bit, 1, "IfOccured"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IfOverSchedule", System.Data.SqlDbType.Bit, 1, "IfOverSchedule"))
        '
        'SqlSelectCommand6
        '
        Me.SqlSelectCommand6.CommandText = "SELECT BRID, JIID, JBID, IfOccured, IfOverSchedule, JIDescription FROM VJobs_JobI" & _
        "ssues WHERE (BRID = @BRID) AND (JBID = @JBID)"
        Me.SqlSelectCommand6.Connection = Me.SqlConnection
        Me.SqlSelectCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlUpdateCommand5
        '
        Me.SqlUpdateCommand5.CommandText = "UPDATE Jobs_JobIssues SET BRID = @BRID, JIID = @JIID, JBID = @JBID, IfOccured = @" & _
        "IfOccured, IfOverSchedule = @IfOverSchedule WHERE (BRID = @Original_BRID) AND (J" & _
        "BID = @Original_JBID) AND (JIID = @Original_JIID) AND (IfOccured = @Original_IfO" & _
        "ccured OR @Original_IfOccured IS NULL AND IfOccured IS NULL) AND (IfOverSchedule" & _
        " = @Original_IfOverSchedule OR @Original_IfOverSchedule IS NULL AND IfOverSchedu" & _
        "le IS NULL); SELECT BRID, JIID, JBID, IfOccured, IfOverSchedule, JIDescription F" & _
        "ROM VJobs_JobIssues WHERE (BRID = @BRID) AND (JBID = @JBID) AND (JIID = @JIID)"
        Me.SqlUpdateCommand5.Connection = Me.SqlConnection
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JIID", System.Data.SqlDbType.Int, 4, "JIID"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IfOccured", System.Data.SqlDbType.Bit, 1, "IfOccured"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IfOverSchedule", System.Data.SqlDbType.Bit, 1, "IfOverSchedule"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IfOccured", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IfOccured", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IfOverSchedule", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IfOverSchedule", System.Data.DataRowVersion.Original, Nothing))
        '
        'HelpProvider1
        '
        Me.HelpProvider1.HelpNamespace = "C:\Documents and Settings\djpower\My Documents\My Projects\GTMS\Version1\Help\Hel" & _
        "pProject.chm"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(632, 536)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 3
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(712, 536)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.Location = New System.Drawing.Point(8, 536)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(72, 23)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Help"
        '
        'daExpenses
        '
        Me.daExpenses.InsertCommand = Me.SqlInsertCommand9
        Me.daExpenses.SelectCommand = Me.SqlSelectCommand9
        Me.daExpenses.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VExpenses", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("EXName", "EXName"), New System.Data.Common.DataColumnMapping("EGType", "EGType"), New System.Data.Common.DataColumnMapping("EGName", "EGName"), New System.Data.Common.DataColumnMapping("EGID", "EGID"), New System.Data.Common.DataColumnMapping("PMAllowValueInJobScreen", "PMAllowValueInJobScreen"), New System.Data.Common.DataColumnMapping("PMAllowHoursInJobScreen", "PMAllowHoursInJobScreen")})})
        '
        'SqlInsertCommand9
        '
        Me.SqlInsertCommand9.CommandText = "INSERT INTO VExpenses(BRID, EXName, EGType, EGName, EGID, PMAllowValueInJobScreen" & _
        ", PMAllowHoursInJobScreen) VALUES (@BRID, @EXName, @EGType, @EGName, @EGID, @PMA" & _
        "llowValueInJobScreen, @PMAllowHoursInJobScreen); SELECT BRID, EXID, EXName, EGTy" & _
        "pe, EGName, EGID, PMAllowValueInJobScreen, PMAllowHoursInJobScreen FROM VExpense" & _
        "s"
        Me.SqlInsertCommand9.Connection = Me.SqlConnection
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 2, "EGType"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGName", System.Data.SqlDbType.VarChar, 50, "EGName"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGID", System.Data.SqlDbType.Int, 4, "EGID"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMAllowValueInJobScreen", System.Data.SqlDbType.Bit, 1, "PMAllowValueInJobScreen"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMAllowHoursInJobScreen", System.Data.SqlDbType.Bit, 1, "PMAllowHoursInJobScreen"))
        '
        'SqlSelectCommand9
        '
        Me.SqlSelectCommand9.CommandText = "SELECT BRID, EXID, EXName, EGType, EGName, EGID, PMAllowValueInJobScreen, PMAllow" & _
        "HoursInJobScreen FROM VExpenses WHERE (EXDiscontinued = 0) AND (BRID = @BRID)"
        Me.SqlSelectCommand9.Connection = Me.SqlConnection
        Me.SqlSelectCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'daINFollowUpTypes
        '
        Me.daINFollowUpTypes.DeleteCommand = Me.SqlCommand7
        Me.daINFollowUpTypes.InsertCommand = Me.SqlCommand8
        Me.daINFollowUpTypes.SelectCommand = Me.SqlCommand9
        Me.daINFollowUpTypes.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "INFollowUpTypes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("INFollowUpType", "INFollowUpType"), New System.Data.Common.DataColumnMapping("INFollowUpTypeDesc", "INFollowUpTypeDesc")})})
        Me.daINFollowUpTypes.UpdateCommand = Me.SqlCommand10
        '
        'SqlCommand7
        '
        Me.SqlCommand7.CommandText = "DELETE FROM INFollowUpTypes WHERE (INFollowUpType = @Original_INFollowUpType) AND" & _
        " (INFollowUpTypeDesc = @Original_INFollowUpTypeDesc OR @Original_INFollowUpTypeD" & _
        "esc IS NULL AND INFollowUpTypeDesc IS NULL)"
        Me.SqlCommand7.Connection = Me.SqlConnection
        Me.SqlCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpTypeDesc", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpTypeDesc", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlCommand8
        '
        Me.SqlCommand8.CommandText = "INSERT INTO INFollowUpTypes(INFollowUpType, INFollowUpTypeDesc) VALUES (@INFollow" & _
        "UpType, @INFollowUpTypeDesc); SELECT INFollowUpType, INFollowUpTypeDesc FROM INF" & _
        "ollowUpTypes WHERE (INFollowUpType = @INFollowUpType)"
        Me.SqlCommand8.Connection = Me.SqlConnection
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpType", System.Data.SqlDbType.VarChar, 2, "INFollowUpType"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpTypeDesc", System.Data.SqlDbType.VarChar, 50, "INFollowUpTypeDesc"))
        '
        'SqlCommand9
        '
        Me.SqlCommand9.CommandText = "SELECT INFollowUpType, INFollowUpTypeDesc FROM INFollowUpTypes WHERE (INFollowUpT" & _
        "ypeEnabled = 1)"
        Me.SqlCommand9.Connection = Me.SqlConnection
        '
        'SqlCommand10
        '
        Me.SqlCommand10.CommandText = "UPDATE INFollowUpTypes SET INFollowUpType = @INFollowUpType, INFollowUpTypeDesc =" & _
        " @INFollowUpTypeDesc WHERE (INFollowUpType = @Original_INFollowUpType) AND (INFo" & _
        "llowUpTypeDesc = @Original_INFollowUpTypeDesc OR @Original_INFollowUpTypeDesc IS" & _
        " NULL AND INFollowUpTypeDesc IS NULL); SELECT INFollowUpType, INFollowUpTypeDesc" & _
        " FROM INFollowUpTypes WHERE (INFollowUpType = @INFollowUpType)"
        Me.SqlCommand10.Connection = Me.SqlConnection
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpType", System.Data.SqlDbType.VarChar, 2, "INFollowUpType"))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpTypeDesc", System.Data.SqlDbType.VarChar, 50, "INFollowUpTypeDesc"))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpTypeDesc", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpTypeDesc", System.Data.DataRowVersion.Original, Nothing))
        '
        'daIDNotes
        '
        Me.daIDNotes.DeleteCommand = Me.SqlDeleteCommand6
        Me.daIDNotes.InsertCommand = Me.SqlCommand1
        Me.daIDNotes.SelectCommand = Me.SqlCommand2
        Me.daIDNotes.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "IDNotes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("INID", "INID"), New System.Data.Common.DataColumnMapping("ID", "ID"), New System.Data.Common.DataColumnMapping("INUser", "INUser"), New System.Data.Common.DataColumnMapping("INDate", "INDate"), New System.Data.Common.DataColumnMapping("INNotes", "INNotes"), New System.Data.Common.DataColumnMapping("INFollowUpType", "INFollowUpType"), New System.Data.Common.DataColumnMapping("INFollowUpDate", "INFollowUpDate"), New System.Data.Common.DataColumnMapping("INFollowUpComplete", "INFollowUpComplete"), New System.Data.Common.DataColumnMapping("EXIDFollowUp", "EXIDFollowUp"), New System.Data.Common.DataColumnMapping("INFollowUpText", "INFollowUpText")})})
        Me.daIDNotes.UpdateCommand = Me.SqlUpdateCommand6
        '
        'SqlDeleteCommand6
        '
        Me.SqlDeleteCommand6.CommandText = "DELETE FROM IDNotes WHERE (BRID = @Original_BRID) AND (INID = @Original_INID) AND" & _
        " (EXIDFollowUp = @Original_EXIDFollowUp OR @Original_EXIDFollowUp IS NULL AND EX" & _
        "IDFollowUp IS NULL) AND (ID = @Original_ID) AND (INDate = @Original_INDate OR @O" & _
        "riginal_INDate IS NULL AND INDate IS NULL) AND (INFollowUpComplete = @Original_I" & _
        "NFollowUpComplete OR @Original_INFollowUpComplete IS NULL AND INFollowUpComplete" & _
        " IS NULL) AND (INFollowUpDate = @Original_INFollowUpDate OR @Original_INFollowUp" & _
        "Date IS NULL AND INFollowUpDate IS NULL) AND (INFollowUpText = @Original_INFollo" & _
        "wUpText OR @Original_INFollowUpText IS NULL AND INFollowUpText IS NULL) AND (INF" & _
        "ollowUpType = @Original_INFollowUpType OR @Original_INFollowUpType IS NULL AND I" & _
        "NFollowUpType IS NULL) AND (INNotes = @Original_INNotes OR @Original_INNotes IS " & _
        "NULL AND INNotes IS NULL) AND (INUser = @Original_INUser OR @Original_INUser IS " & _
        "NULL AND INUser IS NULL)"
        Me.SqlDeleteCommand6.Connection = Me.SqlConnection
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDFollowUp", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDFollowUp", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpComplete", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpComplete", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpText", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpText", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INNotes", System.Data.SqlDbType.VarChar, 1000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INUser", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INUser", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlCommand1
        '
        Me.SqlCommand1.CommandText = "INSERT INTO IDNotes(BRID, ID, INUser, INDate, INNotes, INFollowUpType, INFollowUp" & _
        "Date, INFollowUpComplete, EXIDFollowUp, INFollowUpText) VALUES (@BRID, @ID, @INU" & _
        "ser, @INDate, @INNotes, @INFollowUpType, @INFollowUpDate, @INFollowUpComplete, @" & _
        "EXIDFollowUp, @INFollowUpText); SELECT BRID, INID, ID, INUser, INDate, INNotes, " & _
        "INFollowUpType, INFollowUpDate, INFollowUpComplete, EXIDFollowUp, INFollowUpText" & _
        " FROM IDNotes WHERE (BRID = @BRID) AND (INID = @@IDENTITY)"
        Me.SqlCommand1.Connection = Me.SqlConnection
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INUser", System.Data.SqlDbType.VarChar, 50, "INUser"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INDate", System.Data.SqlDbType.DateTime, 8, "INDate"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INNotes", System.Data.SqlDbType.VarChar, 1000, "INNotes"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpType", System.Data.SqlDbType.VarChar, 2, "INFollowUpType"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpDate", System.Data.SqlDbType.DateTime, 8, "INFollowUpDate"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpComplete", System.Data.SqlDbType.Bit, 1, "INFollowUpComplete"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDFollowUp", System.Data.SqlDbType.Int, 4, "EXIDFollowUp"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpText", System.Data.SqlDbType.VarChar, 100, "INFollowUpText"))
        '
        'SqlCommand2
        '
        Me.SqlCommand2.CommandText = "SELECT BRID, INID, ID, INUser, INDate, INNotes, INFollowUpType, INFollowUpDate, I" & _
        "NFollowUpComplete, EXIDFollowUp, INFollowUpText FROM IDNotes WHERE (BRID = @BRID" & _
        ") AND (ID = @ID)"
        Me.SqlCommand2.Connection = Me.SqlConnection
        Me.SqlCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        '
        'SqlUpdateCommand6
        '
        Me.SqlUpdateCommand6.CommandText = "UPDATE IDNotes SET BRID = @BRID, ID = @ID, INUser = @INUser, INDate = @INDate, IN" & _
        "Notes = @INNotes, INFollowUpType = @INFollowUpType, INFollowUpDate = @INFollowUp" & _
        "Date, INFollowUpComplete = @INFollowUpComplete, EXIDFollowUp = @EXIDFollowUp, IN" & _
        "FollowUpText = @INFollowUpText WHERE (BRID = @Original_BRID) AND (INID = @Origin" & _
        "al_INID) AND (EXIDFollowUp = @Original_EXIDFollowUp OR @Original_EXIDFollowUp IS" & _
        " NULL AND EXIDFollowUp IS NULL) AND (ID = @Original_ID) AND (INDate = @Original_" & _
        "INDate OR @Original_INDate IS NULL AND INDate IS NULL) AND (INFollowUpComplete =" & _
        " @Original_INFollowUpComplete OR @Original_INFollowUpComplete IS NULL AND INFoll" & _
        "owUpComplete IS NULL) AND (INFollowUpDate = @Original_INFollowUpDate OR @Origina" & _
        "l_INFollowUpDate IS NULL AND INFollowUpDate IS NULL) AND (INFollowUpText = @Orig" & _
        "inal_INFollowUpText OR @Original_INFollowUpText IS NULL AND INFollowUpText IS NU" & _
        "LL) AND (INFollowUpType = @Original_INFollowUpType OR @Original_INFollowUpType I" & _
        "S NULL AND INFollowUpType IS NULL) AND (INNotes = @Original_INNotes OR @Original" & _
        "_INNotes IS NULL AND INNotes IS NULL) AND (INUser = @Original_INUser OR @Origina" & _
        "l_INUser IS NULL AND INUser IS NULL); SELECT BRID, INID, ID, INUser, INDate, INN" & _
        "otes, INFollowUpType, INFollowUpDate, INFollowUpComplete, EXIDFollowUp, INFollow" & _
        "UpText FROM IDNotes WHERE (BRID = @BRID) AND (INID = @INID)"
        Me.SqlUpdateCommand6.Connection = Me.SqlConnection
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INUser", System.Data.SqlDbType.VarChar, 50, "INUser"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INDate", System.Data.SqlDbType.DateTime, 8, "INDate"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INNotes", System.Data.SqlDbType.VarChar, 1000, "INNotes"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpType", System.Data.SqlDbType.VarChar, 2, "INFollowUpType"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpDate", System.Data.SqlDbType.DateTime, 8, "INFollowUpDate"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpComplete", System.Data.SqlDbType.Bit, 1, "INFollowUpComplete"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDFollowUp", System.Data.SqlDbType.Int, 4, "EXIDFollowUp"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpText", System.Data.SqlDbType.VarChar, 100, "INFollowUpText"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDFollowUp", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDFollowUp", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpComplete", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpComplete", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpText", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpText", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INNotes", System.Data.SqlDbType.VarChar, 1000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INUser", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INUser", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INID", System.Data.SqlDbType.BigInt, 8, "INID"))
        '
        'daContacts
        '
        Me.daContacts.DeleteCommand = Me.SqlCommand3
        Me.daContacts.InsertCommand = Me.SqlCommand4
        Me.daContacts.SelectCommand = Me.SqlCommand5
        Me.daContacts.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Contacts", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("CTID", "CTID"), New System.Data.Common.DataColumnMapping("CTSurname", "CTSurname"), New System.Data.Common.DataColumnMapping("CTFirstName", "CTFirstName"), New System.Data.Common.DataColumnMapping("CTStreetAddress01", "CTStreetAddress01"), New System.Data.Common.DataColumnMapping("CTStreetAddress02", "CTStreetAddress02"), New System.Data.Common.DataColumnMapping("CTSuburb", "CTSuburb"), New System.Data.Common.DataColumnMapping("CTState", "CTState"), New System.Data.Common.DataColumnMapping("CTPostCode", "CTPostCode"), New System.Data.Common.DataColumnMapping("CTHomePhoneNumber", "CTHomePhoneNumber"), New System.Data.Common.DataColumnMapping("CTWorkPhoneNumber", "CTWorkPhoneNumber"), New System.Data.Common.DataColumnMapping("CTMobileNumber", "CTMobileNumber"), New System.Data.Common.DataColumnMapping("CTFaxNumber", "CTFaxNumber"), New System.Data.Common.DataColumnMapping("CTEmail", "CTEmail"), New System.Data.Common.DataColumnMapping("CTReferenceName", "CTReferenceName"), New System.Data.Common.DataColumnMapping("CTName", "CTName"), New System.Data.Common.DataColumnMapping("CTAddress", "CTAddress"), New System.Data.Common.DataColumnMapping("CTAddressMultiline", "CTAddressMultiline")})})
        Me.daContacts.UpdateCommand = Me.SqlCommand6
        '
        'SqlCommand3
        '
        Me.SqlCommand3.CommandText = "DELETE FROM Contacts WHERE (BRID = @Original_BRID) AND (CTID = @Original_CTID) AN" & _
        "D (CTAddress = @Original_CTAddress OR @Original_CTAddress IS NULL AND CTAddress " & _
        "IS NULL) AND (CTAddressMultiline = @Original_CTAddressMultiline OR @Original_CTA" & _
        "ddressMultiline IS NULL AND CTAddressMultiline IS NULL) AND (CTEmail = @Original" & _
        "_CTEmail OR @Original_CTEmail IS NULL AND CTEmail IS NULL) AND (CTFaxNumber = @O" & _
        "riginal_CTFaxNumber OR @Original_CTFaxNumber IS NULL AND CTFaxNumber IS NULL) AN" & _
        "D (CTFirstName = @Original_CTFirstName OR @Original_CTFirstName IS NULL AND CTFi" & _
        "rstName IS NULL) AND (CTHomePhoneNumber = @Original_CTHomePhoneNumber OR @Origin" & _
        "al_CTHomePhoneNumber IS NULL AND CTHomePhoneNumber IS NULL) AND (CTMobileNumber " & _
        "= @Original_CTMobileNumber OR @Original_CTMobileNumber IS NULL AND CTMobileNumbe" & _
        "r IS NULL) AND (CTName = @Original_CTName OR @Original_CTName IS NULL AND CTName" & _
        " IS NULL) AND (CTPostCode = @Original_CTPostCode OR @Original_CTPostCode IS NULL" & _
        " AND CTPostCode IS NULL) AND (CTReferenceName = @Original_CTReferenceName OR @Or" & _
        "iginal_CTReferenceName IS NULL AND CTReferenceName IS NULL) AND (CTState = @Orig" & _
        "inal_CTState OR @Original_CTState IS NULL AND CTState IS NULL) AND (CTStreetAddr" & _
        "ess01 = @Original_CTStreetAddress01 OR @Original_CTStreetAddress01 IS NULL AND C" & _
        "TStreetAddress01 IS NULL) AND (CTStreetAddress02 = @Original_CTStreetAddress02 O" & _
        "R @Original_CTStreetAddress02 IS NULL AND CTStreetAddress02 IS NULL) AND (CTSubu" & _
        "rb = @Original_CTSuburb OR @Original_CTSuburb IS NULL AND CTSuburb IS NULL) AND " & _
        "(CTSurname = @Original_CTSurname OR @Original_CTSurname IS NULL AND CTSurname IS" & _
        " NULL) AND (CTWorkPhoneNumber = @Original_CTWorkPhoneNumber OR @Original_CTWorkP" & _
        "honeNumber IS NULL AND CTWorkPhoneNumber IS NULL)"
        Me.SqlCommand3.Connection = Me.SqlConnection
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTAddressMultiline", System.Data.SqlDbType.VarChar, 261, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTAddressMultiline", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTFirstName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTFirstName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTHomePhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTName", System.Data.SqlDbType.VarChar, 102, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTReferenceName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTReferenceName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTSurname", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTWorkPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlCommand4
        '
        Me.SqlCommand4.CommandText = "INSERT INTO Contacts(BRID, CTSurname, CTFirstName, CTStreetAddress01, CTStreetAdd" & _
        "ress02, CTSuburb, CTState, CTPostCode, CTHomePhoneNumber, CTWorkPhoneNumber, CTM" & _
        "obileNumber, CTFaxNumber, CTEmail, CTReferenceName, CTName, CTAddress, CTAddress" & _
        "Multiline) VALUES (@BRID, @CTSurname, @CTFirstName, @CTStreetAddress01, @CTStree" & _
        "tAddress02, @CTSuburb, @CTState, @CTPostCode, @CTHomePhoneNumber, @CTWorkPhoneNu" & _
        "mber, @CTMobileNumber, @CTFaxNumber, @CTEmail, @CTReferenceName, @CTName, @CTAdd" & _
        "ress, @CTAddressMultiline); SELECT BRID, CTID, CTSurname, CTFirstName, CTStreetA" & _
        "ddress01, CTStreetAddress02, CTSuburb, CTState, CTPostCode, CTHomePhoneNumber, C" & _
        "TWorkPhoneNumber, CTMobileNumber, CTFaxNumber, CTEmail, CTReferenceName, CTName," & _
        " CTAddress, CTAddressMultiline FROM Contacts WHERE (BRID = @BRID) AND (CTID = @@" & _
        "IDENTITY)"
        Me.SqlCommand4.Connection = Me.SqlConnection
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTSurname", System.Data.SqlDbType.VarChar, 50, "CTSurname"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTFirstName", System.Data.SqlDbType.VarChar, 50, "CTFirstName"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTStreetAddress01", System.Data.SqlDbType.VarChar, 100, "CTStreetAddress01"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTStreetAddress02", System.Data.SqlDbType.VarChar, 100, "CTStreetAddress02"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTSuburb", System.Data.SqlDbType.VarChar, 50, "CTSuburb"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTState", System.Data.SqlDbType.VarChar, 3, "CTState"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTPostCode", System.Data.SqlDbType.VarChar, 20, "CTPostCode"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, "CTHomePhoneNumber"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, "CTWorkPhoneNumber"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTMobileNumber", System.Data.SqlDbType.VarChar, 20, "CTMobileNumber"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTFaxNumber", System.Data.SqlDbType.VarChar, 20, "CTFaxNumber"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTEmail", System.Data.SqlDbType.VarChar, 50, "CTEmail"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTReferenceName", System.Data.SqlDbType.VarChar, 100, "CTReferenceName"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTName", System.Data.SqlDbType.VarChar, 102, "CTName"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTAddress", System.Data.SqlDbType.VarChar, 259, "CTAddress"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTAddressMultiline", System.Data.SqlDbType.VarChar, 261, "CTAddressMultiline"))
        '
        'SqlCommand5
        '
        Me.SqlCommand5.CommandText = "SELECT BRID, CTID, CTSurname, CTFirstName, CTStreetAddress01, CTStreetAddress02, " & _
        "CTSuburb, CTState, CTPostCode, CTHomePhoneNumber, CTWorkPhoneNumber, CTMobileNum" & _
        "ber, CTFaxNumber, CTEmail, CTReferenceName, CTName, CTAddress, CTAddressMultilin" & _
        "e FROM Contacts WHERE (BRID = @BRID) AND (CTID = @CTID)"
        Me.SqlCommand5.Connection = Me.SqlConnection
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTID", System.Data.SqlDbType.BigInt, 8, "CTID"))
        '
        'SqlCommand6
        '
        Me.SqlCommand6.CommandText = "UPDATE Contacts SET BRID = @BRID, CTSurname = @CTSurname, CTFirstName = @CTFirstN" & _
        "ame, CTStreetAddress01 = @CTStreetAddress01, CTStreetAddress02 = @CTStreetAddres" & _
        "s02, CTSuburb = @CTSuburb, CTState = @CTState, CTPostCode = @CTPostCode, CTHomeP" & _
        "honeNumber = @CTHomePhoneNumber, CTWorkPhoneNumber = @CTWorkPhoneNumber, CTMobil" & _
        "eNumber = @CTMobileNumber, CTFaxNumber = @CTFaxNumber, CTEmail = @CTEmail, CTRef" & _
        "erenceName = @CTReferenceName, CTName = @CTName, CTAddress = @CTAddress, CTAddre" & _
        "ssMultiline = @CTAddressMultiline WHERE (BRID = @Original_BRID) AND (CTID = @Ori" & _
        "ginal_CTID) AND (CTAddress = @Original_CTAddress OR @Original_CTAddress IS NULL " & _
        "AND CTAddress IS NULL) AND (CTAddressMultiline = @Original_CTAddressMultiline OR" & _
        " @Original_CTAddressMultiline IS NULL AND CTAddressMultiline IS NULL) AND (CTEma" & _
        "il = @Original_CTEmail OR @Original_CTEmail IS NULL AND CTEmail IS NULL) AND (CT" & _
        "FaxNumber = @Original_CTFaxNumber OR @Original_CTFaxNumber IS NULL AND CTFaxNumb" & _
        "er IS NULL) AND (CTFirstName = @Original_CTFirstName OR @Original_CTFirstName IS" & _
        " NULL AND CTFirstName IS NULL) AND (CTHomePhoneNumber = @Original_CTHomePhoneNum" & _
        "ber OR @Original_CTHomePhoneNumber IS NULL AND CTHomePhoneNumber IS NULL) AND (C" & _
        "TMobileNumber = @Original_CTMobileNumber OR @Original_CTMobileNumber IS NULL AND" & _
        " CTMobileNumber IS NULL) AND (CTName = @Original_CTName OR @Original_CTName IS N" & _
        "ULL AND CTName IS NULL) AND (CTPostCode = @Original_CTPostCode OR @Original_CTPo" & _
        "stCode IS NULL AND CTPostCode IS NULL) AND (CTReferenceName = @Original_CTRefere" & _
        "nceName OR @Original_CTReferenceName IS NULL AND CTReferenceName IS NULL) AND (C" & _
        "TState = @Original_CTState OR @Original_CTState IS NULL AND CTState IS NULL) AND" & _
        " (CTStreetAddress01 = @Original_CTStreetAddress01 OR @Original_CTStreetAddress01" & _
        " IS NULL AND CTStreetAddress01 IS NULL) AND (CTStreetAddress02 = @Original_CTStr" & _
        "eetAddress02 OR @Original_CTStreetAddress02 IS NULL AND CTStreetAddress02 IS NUL" & _
        "L) AND (CTSuburb = @Original_CTSuburb OR @Original_CTSuburb IS NULL AND CTSuburb" & _
        " IS NULL) AND (CTSurname = @Original_CTSurname OR @Original_CTSurname IS NULL AN" & _
        "D CTSurname IS NULL) AND (CTWorkPhoneNumber = @Original_CTWorkPhoneNumber OR @Or" & _
        "iginal_CTWorkPhoneNumber IS NULL AND CTWorkPhoneNumber IS NULL); SELECT BRID, CT" & _
        "ID, CTSurname, CTFirstName, CTStreetAddress01, CTStreetAddress02, CTSuburb, CTSt" & _
        "ate, CTPostCode, CTHomePhoneNumber, CTWorkPhoneNumber, CTMobileNumber, CTFaxNumb" & _
        "er, CTEmail, CTReferenceName, CTName, CTAddress, CTAddressMultiline FROM Contact" & _
        "s WHERE (BRID = @BRID) AND (CTID = @CTID)"
        Me.SqlCommand6.Connection = Me.SqlConnection
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTSurname", System.Data.SqlDbType.VarChar, 50, "CTSurname"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTFirstName", System.Data.SqlDbType.VarChar, 50, "CTFirstName"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTStreetAddress01", System.Data.SqlDbType.VarChar, 100, "CTStreetAddress01"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTStreetAddress02", System.Data.SqlDbType.VarChar, 100, "CTStreetAddress02"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTSuburb", System.Data.SqlDbType.VarChar, 50, "CTSuburb"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTState", System.Data.SqlDbType.VarChar, 3, "CTState"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTPostCode", System.Data.SqlDbType.VarChar, 20, "CTPostCode"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, "CTHomePhoneNumber"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, "CTWorkPhoneNumber"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTMobileNumber", System.Data.SqlDbType.VarChar, 20, "CTMobileNumber"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTFaxNumber", System.Data.SqlDbType.VarChar, 20, "CTFaxNumber"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTEmail", System.Data.SqlDbType.VarChar, 50, "CTEmail"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTReferenceName", System.Data.SqlDbType.VarChar, 100, "CTReferenceName"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTName", System.Data.SqlDbType.VarChar, 102, "CTName"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTAddress", System.Data.SqlDbType.VarChar, 259, "CTAddress"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTAddressMultiline", System.Data.SqlDbType.VarChar, 261, "CTAddressMultiline"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTAddressMultiline", System.Data.SqlDbType.VarChar, 261, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTAddressMultiline", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTFirstName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTFirstName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTHomePhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTName", System.Data.SqlDbType.VarChar, 102, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTReferenceName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTReferenceName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTSurname", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTWorkPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTID", System.Data.SqlDbType.BigInt, 8, "CTID"))
        '
        'daOrders
        '
        Me.daOrders.ContinueUpdateOnError = True
        Me.daOrders.DeleteCommand = Me.SqlDeleteCommand3
        Me.daOrders.InsertCommand = Me.SqlInsertCommand3
        Me.daOrders.SelectCommand = Me.SqlSelectCommand3
        Me.daOrders.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VOrders", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("ORID", "ORID"), New System.Data.Common.DataColumnMapping("JBID", "JBID"), New System.Data.Common.DataColumnMapping("ORType", "ORType"), New System.Data.Common.DataColumnMapping("OROrderNumber", "OROrderNumber"), New System.Data.Common.DataColumnMapping("ORDate", "ORDate"), New System.Data.Common.DataColumnMapping("CTIDSupplier", "CTIDSupplier"), New System.Data.Common.DataColumnMapping("ORShipToJobAddress", "ORShipToJobAddress"), New System.Data.Common.DataColumnMapping("CTIDShipTo", "CTIDShipTo"), New System.Data.Common.DataColumnMapping("ORSupplierInvoiceNumber", "ORSupplierInvoiceNumber"), New System.Data.Common.DataColumnMapping("EXIDOrderedBy", "EXIDOrderedBy"), New System.Data.Common.DataColumnMapping("ORQuotedCost", "ORQuotedCost"), New System.Data.Common.DataColumnMapping("ORInvoicedCost", "ORInvoicedCost"), New System.Data.Common.DataColumnMapping("ORDescription", "ORDescription"), New System.Data.Common.DataColumnMapping("ORDeliveryDate", "ORDeliveryDate"), New System.Data.Common.DataColumnMapping("ORNotes", "ORNotes"), New System.Data.Common.DataColumnMapping("ORReceived", "ORReceived")})})
        Me.daOrders.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM Orders WHERE (BRID = @Original_BRID) AND (ORID = @Original_ORID) AND " & _
        "(CTIDShipTo = @Original_CTIDShipTo OR @Original_CTIDShipTo IS NULL AND CTIDShipT" & _
        "o IS NULL) AND (CTIDSupplier = @Original_CTIDSupplier OR @Original_CTIDSupplier " & _
        "IS NULL AND CTIDSupplier IS NULL) AND (EXIDOrderedBy = @Original_EXIDOrderedBy O" & _
        "R @Original_EXIDOrderedBy IS NULL AND EXIDOrderedBy IS NULL) AND (JBID = @Origin" & _
        "al_JBID OR @Original_JBID IS NULL AND JBID IS NULL) AND (ORDate = @Original_ORDa" & _
        "te OR @Original_ORDate IS NULL AND ORDate IS NULL) AND (ORDeliveryDate = @Origin" & _
        "al_ORDeliveryDate OR @Original_ORDeliveryDate IS NULL AND ORDeliveryDate IS NULL" & _
        ") AND (ORDescription = @Original_ORDescription OR @Original_ORDescription IS NUL" & _
        "L AND ORDescription IS NULL) AND (ORInvoicedCost = @Original_ORInvoicedCost OR @" & _
        "Original_ORInvoicedCost IS NULL AND ORInvoicedCost IS NULL) AND (ORNotes = @Orig" & _
        "inal_ORNotes OR @Original_ORNotes IS NULL AND ORNotes IS NULL) AND (OROrderNumbe" & _
        "r = @Original_OROrderNumber OR @Original_OROrderNumber IS NULL AND OROrderNumber" & _
        " IS NULL) AND (ORQuotedCost = @Original_ORQuotedCost OR @Original_ORQuotedCost I" & _
        "S NULL AND ORQuotedCost IS NULL) AND (ORReceived = @Original_ORReceived OR @Orig" & _
        "inal_ORReceived IS NULL AND ORReceived IS NULL) AND (ORShipToJobAddress = @Origi" & _
        "nal_ORShipToJobAddress OR @Original_ORShipToJobAddress IS NULL AND ORShipToJobAd" & _
        "dress IS NULL) AND (ORSupplierInvoiceNumber = @Original_ORSupplierInvoiceNumber " & _
        "OR @Original_ORSupplierInvoiceNumber IS NULL AND ORSupplierInvoiceNumber IS NULL" & _
        ") AND (ORType = @Original_ORType OR @Original_ORType IS NULL AND ORType IS NULL)" & _
        ""
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTIDShipTo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTIDShipTo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTIDSupplier", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTIDSupplier", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDOrderedBy", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDOrderedBy", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORDeliveryDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORDeliveryDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORDescription", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORInvoicedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORInvoicedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORNotes", System.Data.SqlDbType.VarChar, 500, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OROrderNumber", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OROrderNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORQuotedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORQuotedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORReceived", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORReceived", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORShipToJobAddress", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORShipToJobAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORSupplierInvoiceNumber", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORSupplierInvoiceNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORType", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORType", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO Orders (BRID, JBID, ORType, OROrderNumber, ORDate, CTIDSupplier, ORSh" & _
        "ipToJobAddress, CTIDShipTo, ORSupplierInvoiceNumber, EXIDOrderedBy, ORQuotedCost" & _
        ", ORInvoicedCost, ORDescription, ORDeliveryDate, ORNotes, ORReceived) VALUES (@B" & _
        "RID, @JBID, @ORType, @OROrderNumber, @ORDate, @CTIDSupplier, @ORShipToJobAddress" & _
        ", @CTIDShipTo, @ORSupplierInvoiceNumber, @EXIDOrderedBy, @ORQuotedCost, @ORInvoi" & _
        "cedCost, @ORDescription, @ORDeliveryDate, @ORNotes, @ORReceived); SELECT BRID, O" & _
        "RID, JBID, ORType, OROrderNumber, ORDate, CTIDSupplier, ORShipToJobAddress, CTID" & _
        "ShipTo, ORSupplierInvoiceNumber, EXIDOrderedBy, ORQuotedCost, ORInvoicedCost, OR" & _
        "Description, ORDeliveryDate, ORNotes, ORReceived, ORIsComplete, ORInvoicedCostTo" & _
        "tal, ORStatus, ID FROM VOrders WHERE (BRID = @BRID) AND (ORID = @@IDENTITY)"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORType", System.Data.SqlDbType.VarChar, 3, "ORType"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OROrderNumber", System.Data.SqlDbType.BigInt, 8, "OROrderNumber"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORDate", System.Data.SqlDbType.DateTime, 8, "ORDate"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTIDSupplier", System.Data.SqlDbType.BigInt, 8, "CTIDSupplier"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORShipToJobAddress", System.Data.SqlDbType.Bit, 1, "ORShipToJobAddress"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTIDShipTo", System.Data.SqlDbType.BigInt, 8, "CTIDShipTo"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORSupplierInvoiceNumber", System.Data.SqlDbType.VarChar, 50, "ORSupplierInvoiceNumber"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDOrderedBy", System.Data.SqlDbType.Int, 4, "EXIDOrderedBy"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORQuotedCost", System.Data.SqlDbType.Money, 8, "ORQuotedCost"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORInvoicedCost", System.Data.SqlDbType.Money, 8, "ORInvoicedCost"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORDescription", System.Data.SqlDbType.VarChar, 50, "ORDescription"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORDeliveryDate", System.Data.SqlDbType.DateTime, 8, "ORDeliveryDate"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORNotes", System.Data.SqlDbType.VarChar, 500, "ORNotes"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORReceived", System.Data.SqlDbType.Bit, 1, "ORReceived"))
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT BRID, ORID, JBID, ORType, OROrderNumber, ORDate, CTIDSupplier, ORShipToJob" & _
        "Address, CTIDShipTo, ORSupplierInvoiceNumber, EXIDOrderedBy, ORQuotedCost, ORInv" & _
        "oicedCost, ORDescription, ORDeliveryDate, ORNotes, ORReceived, ORIsComplete, ORI" & _
        "nvoicedCostTotal, ORStatus, ID FROM VOrders WHERE (BRID = @BRID) AND (JBID = @JB" & _
        "ID)"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection
        Me.SqlSelectCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE Orders SET BRID = @BRID, JBID = @JBID, ORType = @ORType, OROrderNumber = @" & _
        "OROrderNumber, ORDate = @ORDate, CTIDSupplier = @CTIDSupplier, ORShipToJobAddres" & _
        "s = @ORShipToJobAddress, CTIDShipTo = @CTIDShipTo, ORSupplierInvoiceNumber = @OR" & _
        "SupplierInvoiceNumber, EXIDOrderedBy = @EXIDOrderedBy, ORQuotedCost = @ORQuotedC" & _
        "ost, ORInvoicedCost = @ORInvoicedCost, ORDescription = @ORDescription, ORDeliver" & _
        "yDate = @ORDeliveryDate, ORNotes = @ORNotes, ORReceived = @ORReceived WHERE (BRI" & _
        "D = @Original_BRID) AND (ORID = @Original_ORID) AND (CTIDShipTo = @Original_CTID" & _
        "ShipTo OR @Original_CTIDShipTo IS NULL AND CTIDShipTo IS NULL) AND (CTIDSupplier" & _
        " = @Original_CTIDSupplier OR @Original_CTIDSupplier IS NULL AND CTIDSupplier IS " & _
        "NULL) AND (EXIDOrderedBy = @Original_EXIDOrderedBy OR @Original_EXIDOrderedBy IS" & _
        " NULL AND EXIDOrderedBy IS NULL) AND (JBID = @Original_JBID OR @Original_JBID IS" & _
        " NULL AND JBID IS NULL) AND (ORDate = @Original_ORDate OR @Original_ORDate IS NU" & _
        "LL AND ORDate IS NULL) AND (ORDeliveryDate = @Original_ORDeliveryDate OR @Origin" & _
        "al_ORDeliveryDate IS NULL AND ORDeliveryDate IS NULL) AND (ORDescription = @Orig" & _
        "inal_ORDescription OR @Original_ORDescription IS NULL AND ORDescription IS NULL)" & _
        " AND (ORInvoicedCost = @Original_ORInvoicedCost OR @Original_ORInvoicedCost IS N" & _
        "ULL AND ORInvoicedCost IS NULL) AND (ORNotes = @Original_ORNotes OR @Original_OR" & _
        "Notes IS NULL AND ORNotes IS NULL) AND (OROrderNumber = @Original_OROrderNumber " & _
        "OR @Original_OROrderNumber IS NULL AND OROrderNumber IS NULL) AND (ORQuotedCost " & _
        "= @Original_ORQuotedCost OR @Original_ORQuotedCost IS NULL AND ORQuotedCost IS N" & _
        "ULL) AND (ORReceived = @Original_ORReceived OR @Original_ORReceived IS NULL AND " & _
        "ORReceived IS NULL) AND (ORShipToJobAddress = @Original_ORShipToJobAddress OR @O" & _
        "riginal_ORShipToJobAddress IS NULL AND ORShipToJobAddress IS NULL) AND (ORSuppli" & _
        "erInvoiceNumber = @Original_ORSupplierInvoiceNumber OR @Original_ORSupplierInvoi" & _
        "ceNumber IS NULL AND ORSupplierInvoiceNumber IS NULL) AND (ORType = @Original_OR" & _
        "Type OR @Original_ORType IS NULL AND ORType IS NULL); SELECT BRID, ORID, JBID, O" & _
        "RType, OROrderNumber, ORDate, CTIDSupplier, ORShipToJobAddress, CTIDShipTo, ORSu" & _
        "pplierInvoiceNumber, EXIDOrderedBy, ORQuotedCost, ORInvoicedCost, ORDescription," & _
        " ORDeliveryDate, ORNotes, ORReceived, ORIsComplete, ORInvoicedCostTotal, ORStatu" & _
        "s, ID FROM VOrders WHERE (BRID = @BRID) AND (ORID = @ORID)"
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORType", System.Data.SqlDbType.VarChar, 3, "ORType"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OROrderNumber", System.Data.SqlDbType.BigInt, 8, "OROrderNumber"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORDate", System.Data.SqlDbType.DateTime, 8, "ORDate"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTIDSupplier", System.Data.SqlDbType.BigInt, 8, "CTIDSupplier"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORShipToJobAddress", System.Data.SqlDbType.Bit, 1, "ORShipToJobAddress"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTIDShipTo", System.Data.SqlDbType.BigInt, 8, "CTIDShipTo"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORSupplierInvoiceNumber", System.Data.SqlDbType.VarChar, 50, "ORSupplierInvoiceNumber"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDOrderedBy", System.Data.SqlDbType.Int, 4, "EXIDOrderedBy"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORQuotedCost", System.Data.SqlDbType.Money, 8, "ORQuotedCost"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORInvoicedCost", System.Data.SqlDbType.Money, 8, "ORInvoicedCost"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORDescription", System.Data.SqlDbType.VarChar, 50, "ORDescription"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORDeliveryDate", System.Data.SqlDbType.DateTime, 8, "ORDeliveryDate"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORNotes", System.Data.SqlDbType.VarChar, 500, "ORNotes"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORReceived", System.Data.SqlDbType.Bit, 1, "ORReceived"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTIDShipTo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTIDShipTo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTIDSupplier", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTIDSupplier", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDOrderedBy", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDOrderedBy", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORDeliveryDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORDeliveryDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORDescription", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORInvoicedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORInvoicedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORNotes", System.Data.SqlDbType.VarChar, 500, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OROrderNumber", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OROrderNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORQuotedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORQuotedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORReceived", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORReceived", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORShipToJobAddress", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORShipToJobAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORSupplierInvoiceNumber", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORSupplierInvoiceNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORType", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORID", System.Data.SqlDbType.BigInt, 8, "ORID"))
        '
        'daOrders_Materials
        '
        Me.daOrders_Materials.ContinueUpdateOnError = True
        Me.daOrders_Materials.DeleteCommand = Me.SqlDeleteCommand8
        Me.daOrders_Materials.InsertCommand = Me.SqlInsertCommand7
        Me.daOrders_Materials.SelectCommand = Me.SqlSelectCommand7
        Me.daOrders_Materials.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VOrders_Materials", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("OMID", "OMID"), New System.Data.Common.DataColumnMapping("ORID", "ORID"), New System.Data.Common.DataColumnMapping("MTID", "MTID"), New System.Data.Common.DataColumnMapping("OMPrimaryAmount", "OMPrimaryAmount"), New System.Data.Common.DataColumnMapping("OMQuotedCost", "OMQuotedCost"), New System.Data.Common.DataColumnMapping("OMInvoicedCost", "OMInvoicedCost"), New System.Data.Common.DataColumnMapping("OMUseInvoicedCost", "OMUseInvoicedCost"), New System.Data.Common.DataColumnMapping("OMBrand", "OMBrand"), New System.Data.Common.DataColumnMapping("OMModelNumber", "OMModelNumber"), New System.Data.Common.DataColumnMapping("OMDescription", "OMDescription"), New System.Data.Common.DataColumnMapping("OMQty", "OMQty")})})
        Me.daOrders_Materials.UpdateCommand = Me.SqlUpdateCommand8
        '
        'SqlDeleteCommand8
        '
        Me.SqlDeleteCommand8.CommandText = "DELETE FROM Orders_Materials WHERE (BRID = @Original_BRID) AND (OMID = @Original_" & _
        "OMID) AND (MTID = @Original_MTID OR @Original_MTID IS NULL AND MTID IS NULL) AND" & _
        " (OMBrand = @Original_OMBrand OR @Original_OMBrand IS NULL AND OMBrand IS NULL) " & _
        "AND (OMDescription = @Original_OMDescription OR @Original_OMDescription IS NULL " & _
        "AND OMDescription IS NULL) AND (OMInvoicedCost = @Original_OMInvoicedCost OR @Or" & _
        "iginal_OMInvoicedCost IS NULL AND OMInvoicedCost IS NULL) AND (OMModelNumber = @" & _
        "Original_OMModelNumber OR @Original_OMModelNumber IS NULL AND OMModelNumber IS N" & _
        "ULL) AND (OMPrimaryAmount = @Original_OMPrimaryAmount OR @Original_OMPrimaryAmou" & _
        "nt IS NULL AND OMPrimaryAmount IS NULL) AND (OMQty = @Original_OMQty OR @Origina" & _
        "l_OMQty IS NULL AND OMQty IS NULL) AND (OMQuotedCost = @Original_OMQuotedCost OR" & _
        " @Original_OMQuotedCost IS NULL AND OMQuotedCost IS NULL) AND (OMUseInvoicedCost" & _
        " = @Original_OMUseInvoicedCost OR @Original_OMUseInvoicedCost IS NULL AND OMUseI" & _
        "nvoicedCost IS NULL) AND (ORID = @Original_ORID)"
        Me.SqlDeleteCommand8.Connection = Me.SqlConnection
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMBrand", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMBrand", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMDescription", System.Data.SqlDbType.VarChar, 500, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMInvoicedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMInvoicedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMModelNumber", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMModelNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMPrimaryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OMPrimaryAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMQty", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMQty", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMQuotedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMQuotedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMUseInvoicedCost", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMUseInvoicedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand7
        '
        Me.SqlInsertCommand7.CommandText = "INSERT INTO Orders_Materials (BRID, ORID, MTID, OMPrimaryAmount, OMQuotedCost, OM" & _
        "InvoicedCost, OMUseInvoicedCost, OMBrand, OMModelNumber, OMDescription, OMQty) V" & _
        "ALUES (@BRID, @ORID, @MTID, @OMPrimaryAmount, @OMQuotedCost, @OMInvoicedCost, @O" & _
        "MUseInvoicedCost, @OMBrand, @OMModelNumber, @OMDescription, @OMQty); SELECT BRID" & _
        ", OMID, ORID, MTID, OMPrimaryAmount, OMQuotedCost, OMInvoicedCost, OMUseInvoiced" & _
        "Cost, OMBrand, OMModelNumber, OMDescription, OMQty, MTPrimaryUOMShortName FROM V" & _
        "Orders_Materials WHERE (BRID = @BRID) AND (OMID = @@IDENTITY)"
        Me.SqlInsertCommand7.Connection = Me.SqlConnection
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORID", System.Data.SqlDbType.BigInt, 8, "ORID"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMPrimaryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OMPrimaryAmount", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMQuotedCost", System.Data.SqlDbType.Money, 8, "OMQuotedCost"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMInvoicedCost", System.Data.SqlDbType.Money, 8, "OMInvoicedCost"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMUseInvoicedCost", System.Data.SqlDbType.Bit, 1, "OMUseInvoicedCost"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMBrand", System.Data.SqlDbType.VarChar, 50, "OMBrand"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMModelNumber", System.Data.SqlDbType.VarChar, 50, "OMModelNumber"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMDescription", System.Data.SqlDbType.VarChar, 500, "OMDescription"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMQty", System.Data.SqlDbType.Int, 4, "OMQty"))
        '
        'SqlSelectCommand7
        '
        Me.SqlSelectCommand7.CommandText = "SELECT BRID, OMID, ORID, MTID, OMPrimaryAmount, OMQuotedCost, OMInvoicedCost, OMU" & _
        "seInvoicedCost, OMBrand, OMModelNumber, OMDescription, OMQty, MTPrimaryUOMShortN" & _
        "ame FROM VOrders_Materials WHERE (BRID = @BRID) AND (JBID = @JBID)"
        Me.SqlSelectCommand7.Connection = Me.SqlConnection
        Me.SqlSelectCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlUpdateCommand8
        '
        Me.SqlUpdateCommand8.CommandText = "UPDATE Orders_Materials SET BRID = @BRID, ORID = @ORID, MTID = @MTID, OMPrimaryAm" & _
        "ount = @OMPrimaryAmount, OMQuotedCost = @OMQuotedCost, OMInvoicedCost = @OMInvoi" & _
        "cedCost, OMUseInvoicedCost = @OMUseInvoicedCost, OMBrand = @OMBrand, OMModelNumb" & _
        "er = @OMModelNumber, OMDescription = @OMDescription, OMQty = @OMQty WHERE (BRID " & _
        "= @Original_BRID) AND (OMID = @Original_OMID) AND (MTID = @Original_MTID OR @Ori" & _
        "ginal_MTID IS NULL AND MTID IS NULL) AND (OMBrand = @Original_OMBrand OR @Origin" & _
        "al_OMBrand IS NULL AND OMBrand IS NULL) AND (OMDescription = @Original_OMDescrip" & _
        "tion OR @Original_OMDescription IS NULL AND OMDescription IS NULL) AND (OMInvoic" & _
        "edCost = @Original_OMInvoicedCost OR @Original_OMInvoicedCost IS NULL AND OMInvo" & _
        "icedCost IS NULL) AND (OMModelNumber = @Original_OMModelNumber OR @Original_OMMo" & _
        "delNumber IS NULL AND OMModelNumber IS NULL) AND (OMPrimaryAmount = @Original_OM" & _
        "PrimaryAmount OR @Original_OMPrimaryAmount IS NULL AND OMPrimaryAmount IS NULL) " & _
        "AND (OMQty = @Original_OMQty OR @Original_OMQty IS NULL AND OMQty IS NULL) AND (" & _
        "OMQuotedCost = @Original_OMQuotedCost OR @Original_OMQuotedCost IS NULL AND OMQu" & _
        "otedCost IS NULL) AND (OMUseInvoicedCost = @Original_OMUseInvoicedCost OR @Origi" & _
        "nal_OMUseInvoicedCost IS NULL AND OMUseInvoicedCost IS NULL) AND (ORID = @Origin" & _
        "al_ORID); SELECT BRID, OMID, ORID, MTID, OMPrimaryAmount, OMQuotedCost, OMInvoic" & _
        "edCost, OMUseInvoicedCost, OMBrand, OMModelNumber, OMDescription, OMQty, MTPrima" & _
        "ryUOMShortName FROM VOrders_Materials WHERE (BRID = @BRID) AND (OMID = @OMID)"
        Me.SqlUpdateCommand8.Connection = Me.SqlConnection
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORID", System.Data.SqlDbType.BigInt, 8, "ORID"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMPrimaryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OMPrimaryAmount", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMQuotedCost", System.Data.SqlDbType.Money, 8, "OMQuotedCost"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMInvoicedCost", System.Data.SqlDbType.Money, 8, "OMInvoicedCost"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMUseInvoicedCost", System.Data.SqlDbType.Bit, 1, "OMUseInvoicedCost"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMBrand", System.Data.SqlDbType.VarChar, 50, "OMBrand"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMModelNumber", System.Data.SqlDbType.VarChar, 50, "OMModelNumber"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMDescription", System.Data.SqlDbType.VarChar, 500, "OMDescription"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMQty", System.Data.SqlDbType.Int, 4, "OMQty"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMBrand", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMBrand", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMDescription", System.Data.SqlDbType.VarChar, 500, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMInvoicedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMInvoicedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMModelNumber", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMModelNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMPrimaryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OMPrimaryAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMQty", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMQty", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMQuotedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMQuotedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMUseInvoicedCost", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMUseInvoicedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMID", System.Data.SqlDbType.BigInt, 8, "OMID"))
        '
        'daOrders_Materials_SubItems
        '
        Me.daOrders_Materials_SubItems.ContinueUpdateOnError = True
        Me.daOrders_Materials_SubItems.DeleteCommand = Me.SqlDeleteCommand10
        Me.daOrders_Materials_SubItems.InsertCommand = Me.SqlInsertCommand8
        Me.daOrders_Materials_SubItems.SelectCommand = Me.SqlSelectCommand8
        Me.daOrders_Materials_SubItems.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Orders_Materials_SubItems", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("OMSID", "OMSID"), New System.Data.Common.DataColumnMapping("OMID", "OMID"), New System.Data.Common.DataColumnMapping("OMSBrand", "OMSBrand"), New System.Data.Common.DataColumnMapping("OMSModelNumber", "OMSModelNumber"), New System.Data.Common.DataColumnMapping("OMSDescription", "OMSDescription"), New System.Data.Common.DataColumnMapping("OMSQty", "OMSQty"), New System.Data.Common.DataColumnMapping("OMSQuotedCost", "OMSQuotedCost")})})
        Me.daOrders_Materials_SubItems.UpdateCommand = Me.SqlUpdateCommand10
        '
        'SqlDeleteCommand10
        '
        Me.SqlDeleteCommand10.CommandText = "DELETE FROM Orders_Materials_SubItems WHERE (BRID = @Original_BRID) AND (OMSID = " & _
        "@Original_OMSID) AND (OMID = @Original_OMID) AND (OMSBrand = @Original_OMSBrand " & _
        "OR @Original_OMSBrand IS NULL AND OMSBrand IS NULL) AND (OMSDescription = @Origi" & _
        "nal_OMSDescription OR @Original_OMSDescription IS NULL AND OMSDescription IS NUL" & _
        "L) AND (OMSModelNumber = @Original_OMSModelNumber OR @Original_OMSModelNumber IS" & _
        " NULL AND OMSModelNumber IS NULL) AND (OMSQty = @Original_OMSQty OR @Original_OM" & _
        "SQty IS NULL AND OMSQty IS NULL) AND (OMSQuotedCost = @Original_OMSQuotedCost OR" & _
        " @Original_OMSQuotedCost IS NULL AND OMSQuotedCost IS NULL)"
        Me.SqlDeleteCommand10.Connection = Me.SqlConnection
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSBrand", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSBrand", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSDescription", System.Data.SqlDbType.VarChar, 500, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSModelNumber", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSModelNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSQty", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSQty", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSQuotedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSQuotedCost", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand8
        '
        Me.SqlInsertCommand8.CommandText = "INSERT INTO Orders_Materials_SubItems(BRID, OMID, OMSBrand, OMSModelNumber, OMSDe" & _
        "scription, OMSQty, OMSQuotedCost) VALUES (@BRID, @OMID, @OMSBrand, @OMSModelNumb" & _
        "er, @OMSDescription, @OMSQty, @OMSQuotedCost); SELECT BRID, OMSID, OMID, OMSBran" & _
        "d, OMSModelNumber, OMSDescription, OMSQty, OMSQuotedCost FROM Orders_Materials_S" & _
        "ubItems WHERE (BRID = @BRID) AND (OMSID = @@IDENTITY)"
        Me.SqlInsertCommand8.Connection = Me.SqlConnection
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMID", System.Data.SqlDbType.BigInt, 8, "OMID"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSBrand", System.Data.SqlDbType.VarChar, 50, "OMSBrand"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSModelNumber", System.Data.SqlDbType.VarChar, 50, "OMSModelNumber"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSDescription", System.Data.SqlDbType.VarChar, 500, "OMSDescription"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSQty", System.Data.SqlDbType.Int, 4, "OMSQty"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSQuotedCost", System.Data.SqlDbType.Money, 8, "OMSQuotedCost"))
        '
        'SqlSelectCommand8
        '
        Me.SqlSelectCommand8.CommandText = "SELECT BRID, OMSID, OMID, OMSBrand, OMSModelNumber, OMSDescription, OMSQty, OMSQu" & _
        "otedCost FROM VOrders_Materials_SubItems WHERE (BRID = @BRID) AND (JBID = @JBID)" & _
        ""
        Me.SqlSelectCommand8.Connection = Me.SqlConnection
        Me.SqlSelectCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlUpdateCommand10
        '
        Me.SqlUpdateCommand10.CommandText = "UPDATE Orders_Materials_SubItems SET BRID = @BRID, OMID = @OMID, OMSBrand = @OMSB" & _
        "rand, OMSModelNumber = @OMSModelNumber, OMSDescription = @OMSDescription, OMSQty" & _
        " = @OMSQty, OMSQuotedCost = @OMSQuotedCost WHERE (BRID = @Original_BRID) AND (OM" & _
        "SID = @Original_OMSID) AND (OMID = @Original_OMID) AND (OMSBrand = @Original_OMS" & _
        "Brand OR @Original_OMSBrand IS NULL AND OMSBrand IS NULL) AND (OMSDescription = " & _
        "@Original_OMSDescription OR @Original_OMSDescription IS NULL AND OMSDescription " & _
        "IS NULL) AND (OMSModelNumber = @Original_OMSModelNumber OR @Original_OMSModelNum" & _
        "ber IS NULL AND OMSModelNumber IS NULL) AND (OMSQty = @Original_OMSQty OR @Origi" & _
        "nal_OMSQty IS NULL AND OMSQty IS NULL) AND (OMSQuotedCost = @Original_OMSQuotedC" & _
        "ost OR @Original_OMSQuotedCost IS NULL AND OMSQuotedCost IS NULL); SELECT BRID, " & _
        "OMSID, OMID, OMSBrand, OMSModelNumber, OMSDescription, OMSQty, OMSQuotedCost FRO" & _
        "M Orders_Materials_SubItems WHERE (BRID = @BRID) AND (OMSID = @OMSID)"
        Me.SqlUpdateCommand10.Connection = Me.SqlConnection
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMID", System.Data.SqlDbType.BigInt, 8, "OMID"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSBrand", System.Data.SqlDbType.VarChar, 50, "OMSBrand"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSModelNumber", System.Data.SqlDbType.VarChar, 50, "OMSModelNumber"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSDescription", System.Data.SqlDbType.VarChar, 500, "OMSDescription"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSQty", System.Data.SqlDbType.Int, 4, "OMSQty"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSQuotedCost", System.Data.SqlDbType.Money, 8, "OMSQuotedCost"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSBrand", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSBrand", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSDescription", System.Data.SqlDbType.VarChar, 500, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSModelNumber", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSModelNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSQty", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSQty", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSQuotedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSQuotedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSID", System.Data.SqlDbType.BigInt, 8, "OMSID"))
        '
        'SqlSelectCommand10
        '
        Me.SqlSelectCommand10.CommandText = "SELECT BRID, OSID, ORID, SIID, OSInventoryAmount, OSInventoryDate, SIInventoryUOM" & _
        "ShortName FROM VOrders_StockedItems WHERE (BRID = @BRID) AND (JBID = @JBID)"
        Me.SqlSelectCommand10.Connection = Me.SqlConnection
        Me.SqlSelectCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlInsertCommand10
        '
        Me.SqlInsertCommand10.CommandText = "INSERT INTO Orders_StockedItems (BRID, ORID, SIID, OSInventoryAmount, OSInventory" & _
        "Date) VALUES (@BRID, @ORID, @SIID, @OSInventoryAmount, @OSInventoryDate); SELECT" & _
        " BRID, OSID, ORID, SIID, OSInventoryAmount, OSInventoryDate, SIInventoryUOMShort" & _
        "Name FROM VOrders_StockedItems WHERE (BRID = @BRID) AND (OSID = @@IDENTITY)"
        Me.SqlInsertCommand10.Connection = Me.SqlConnection
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORID", System.Data.SqlDbType.BigInt, 8, "ORID"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIID", System.Data.SqlDbType.Int, 4, "SIID"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OSInventoryAmount", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OSInventoryDate", System.Data.SqlDbType.DateTime, 8, "OSInventoryDate"))
        '
        'SqlUpdateCommand11
        '
        Me.SqlUpdateCommand11.CommandText = "UPDATE Orders_StockedItems SET BRID = @BRID, ORID = @ORID, SIID = @SIID, OSInvent" & _
        "oryAmount = @OSInventoryAmount, OSInventoryDate = @OSInventoryDate WHERE (BRID =" & _
        " @Original_BRID) AND (OSID = @Original_OSID) AND (ORID = @Original_ORID OR @Orig" & _
        "inal_ORID IS NULL AND ORID IS NULL) AND (OSInventoryAmount = @Original_OSInvento" & _
        "ryAmount OR @Original_OSInventoryAmount IS NULL AND OSInventoryAmount IS NULL) A" & _
        "ND (OSInventoryDate = @Original_OSInventoryDate OR @Original_OSInventoryDate IS " & _
        "NULL AND OSInventoryDate IS NULL) AND (SIID = @Original_SIID OR @Original_SIID I" & _
        "S NULL AND SIID IS NULL); SELECT BRID, OSID, ORID, SIID, OSInventoryAmount, OSIn" & _
        "ventoryDate, SIInventoryUOMShortName FROM VOrders_StockedItems WHERE (BRID = @BR" & _
        "ID) AND (OSID = @OSID)"
        Me.SqlUpdateCommand11.Connection = Me.SqlConnection
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORID", System.Data.SqlDbType.BigInt, 8, "ORID"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIID", System.Data.SqlDbType.Int, 4, "SIID"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OSInventoryAmount", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OSInventoryDate", System.Data.SqlDbType.DateTime, 8, "OSInventoryDate"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OSID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OSInventoryAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OSInventoryDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OSInventoryDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OSID", System.Data.SqlDbType.BigInt, 8, "OSID"))
        '
        'SqlDeleteCommand11
        '
        Me.SqlDeleteCommand11.CommandText = "DELETE FROM Orders_StockedItems WHERE (BRID = @Original_BRID) AND (OSID = @Origin" & _
        "al_OSID) AND (ORID = @Original_ORID OR @Original_ORID IS NULL AND ORID IS NULL) " & _
        "AND (OSInventoryAmount = @Original_OSInventoryAmount OR @Original_OSInventoryAmo" & _
        "unt IS NULL AND OSInventoryAmount IS NULL) AND (OSInventoryDate = @Original_OSIn" & _
        "ventoryDate OR @Original_OSInventoryDate IS NULL AND OSInventoryDate IS NULL) AN" & _
        "D (SIID = @Original_SIID OR @Original_SIID IS NULL AND SIID IS NULL)"
        Me.SqlDeleteCommand11.Connection = Me.SqlConnection
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OSID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OSInventoryAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OSInventoryDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OSInventoryDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SIID", System.Data.DataRowVersion.Original, Nothing))
        '
        'daOrders_StockedItems
        '
        Me.daOrders_StockedItems.ContinueUpdateOnError = True
        Me.daOrders_StockedItems.DeleteCommand = Me.SqlDeleteCommand11
        Me.daOrders_StockedItems.InsertCommand = Me.SqlInsertCommand10
        Me.daOrders_StockedItems.SelectCommand = Me.SqlSelectCommand10
        Me.daOrders_StockedItems.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VOrders_StockedItems", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("OSID", "OSID"), New System.Data.Common.DataColumnMapping("ORID", "ORID"), New System.Data.Common.DataColumnMapping("SIID", "SIID"), New System.Data.Common.DataColumnMapping("OSInventoryAmount", "OSInventoryAmount"), New System.Data.Common.DataColumnMapping("OSInventoryDate", "OSInventoryDate")})})
        Me.daOrders_StockedItems.UpdateCommand = Me.SqlUpdateCommand11
        '
        'daMaterials
        '
        Me.daMaterials.InsertCommand = Me.SqlCommand11
        Me.daMaterials.SelectCommand = Me.SqlCommand12
        Me.daMaterials.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VMaterials", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("MTID", "MTID"), New System.Data.Common.DataColumnMapping("MTName", "MTName"), New System.Data.Common.DataColumnMapping("MTInventoryUOM", "MTInventoryUOM"), New System.Data.Common.DataColumnMapping("MTPrimaryUOM", "MTPrimaryUOM"), New System.Data.Common.DataColumnMapping("MTDiscontinued", "MTDiscontinued"), New System.Data.Common.DataColumnMapping("MTStocked", "MTStocked"), New System.Data.Common.DataColumnMapping("MGID", "MGID"), New System.Data.Common.DataColumnMapping("MTCurrentPurchaseCost", "MTCurrentPurchaseCost"), New System.Data.Common.DataColumnMapping("MTCurrentWasteFactorAllowance", "MTCurrentWasteFactorAllowance"), New System.Data.Common.DataColumnMapping("MGControlledAtHeadOffice", "MGControlledAtHeadOffice"), New System.Data.Common.DataColumnMapping("MTCurrentInventory", "MTCurrentInventory"), New System.Data.Common.DataColumnMapping("MTCurrentWasteFactor", "MTCurrentWasteFactor"), New System.Data.Common.DataColumnMapping("MGAssignToPortion", "MGAssignToPortion"), New System.Data.Common.DataColumnMapping("MMTID", "MMTID"), New System.Data.Common.DataColumnMapping("MTCurrentWastePercentage", "MTCurrentWastePercentage"), New System.Data.Common.DataColumnMapping("MTPrimaryUOMShortName", "MTPrimaryUOMShortName")})})
        '
        'SqlCommand11
        '
        Me.SqlCommand11.CommandText = "INSERT INTO VMaterials(BRID, MTName, MTInventoryUOM, MTPrimaryUOM, MTDiscontinued" & _
        ", MTStocked, MGID, MTCurrentPurchaseCost, MTCurrentWasteFactorAllowance, MGContr" & _
        "olledAtHeadOffice, MTCurrentInventory, MTCurrentWasteFactor, MGAssignToPortion, " & _
        "MMTID, MTCurrentWastePercentage, MTPrimaryUOMShortName) VALUES (@BRID, @MTName, " & _
        "@MTInventoryUOM, @MTPrimaryUOM, @MTDiscontinued, @MTStocked, @MGID, @MTCurrentPu" & _
        "rchaseCost, @MTCurrentWasteFactorAllowance, @MGControlledAtHeadOffice, @MTCurren" & _
        "tInventory, @MTCurrentWasteFactor, @MGAssignToPortion, @MMTID, @MTCurrentWastePe" & _
        "rcentage, @MTPrimaryUOMShortName); SELECT BRID, MTID, MTName, MTInventoryUOM, MT" & _
        "PrimaryUOM, MTDiscontinued, MTStocked, MGID, MTCurrentPurchaseCost, MTCurrentWas" & _
        "teFactorAllowance, MGControlledAtHeadOffice, MTCurrentInventory, MTCurrentWasteF" & _
        "actor, MGAssignToPortion, MMTID, MTCurrentWastePercentage, MTPrimaryUOMShortName" & _
        " FROM VMaterials"
        Me.SqlCommand11.Connection = Me.SqlConnection
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTName", System.Data.SqlDbType.VarChar, 50, "MTName"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTInventoryUOM", System.Data.SqlDbType.VarChar, 2, "MTInventoryUOM"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTPrimaryUOM", System.Data.SqlDbType.VarChar, 2, "MTPrimaryUOM"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTDiscontinued", System.Data.SqlDbType.Bit, 1, "MTDiscontinued"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTStocked", System.Data.SqlDbType.Bit, 1, "MTStocked"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTCurrentPurchaseCost", System.Data.SqlDbType.Money, 8, "MTCurrentPurchaseCost"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTCurrentWasteFactorAllowance", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "MTCurrentWasteFactorAllowance", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGControlledAtHeadOffice", System.Data.SqlDbType.Bit, 1, "MGControlledAtHeadOffice"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTCurrentInventory", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "MTCurrentInventory", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTCurrentWasteFactor", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "MTCurrentWasteFactor", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGAssignToPortion", System.Data.SqlDbType.VarChar, 2, "MGAssignToPortion"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTID", System.Data.SqlDbType.Int, 4, "MMTID"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTCurrentWastePercentage", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(4, Byte), "MTCurrentWastePercentage", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTPrimaryUOMShortName", System.Data.SqlDbType.VarChar, 50, "MTPrimaryUOMShortName"))
        '
        'SqlCommand12
        '
        Me.SqlCommand12.CommandText = "SELECT BRID, MTID, MTName, MTInventoryUOM, MTPrimaryUOM, MTDiscontinued, MTStocke" & _
        "d, MGID, MTCurrentPurchaseCost, MTCurrentWasteFactorAllowance, MGControlledAtHea" & _
        "dOffice, MTCurrentInventory, MTCurrentWasteFactor, MGAssignToPortion, MMTID, MTC" & _
        "urrentWastePercentage, MTPrimaryUOMShortName FROM VMaterials WHERE (BRID = @BRID" & _
        ") AND (MTDiscontinued = 0)"
        Me.SqlCommand12.Connection = Me.SqlConnection
        Me.SqlCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'daStockedItems
        '
        Me.daStockedItems.InsertCommand = Me.SqlCommand13
        Me.daStockedItems.SelectCommand = Me.SqlCommand14
        Me.daStockedItems.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VStockedItems", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("SIID", "SIID"), New System.Data.Common.DataColumnMapping("MTID", "MTID"), New System.Data.Common.DataColumnMapping("SIConversionToPrimary", "SIConversionToPrimary"), New System.Data.Common.DataColumnMapping("MTPrimaryUOM", "MTPrimaryUOM"), New System.Data.Common.DataColumnMapping("SIName", "SIName"), New System.Data.Common.DataColumnMapping("SIInventoryUOM", "SIInventoryUOM"), New System.Data.Common.DataColumnMapping("SICurrentInventory", "SICurrentInventory"), New System.Data.Common.DataColumnMapping("SICurrentValue", "SICurrentValue"), New System.Data.Common.DataColumnMapping("MTPrimaryUOMShortName", "MTPrimaryUOMShortName"), New System.Data.Common.DataColumnMapping("SICurrentPurchaseCost", "SICurrentPurchaseCost"), New System.Data.Common.DataColumnMapping("SICurrentInventoryInPrimaryUOM", "SICurrentInventoryInPrimaryUOM"), New System.Data.Common.DataColumnMapping("SIInventoryUOMShortName", "SIInventoryUOMShortName")})})
        '
        'SqlCommand13
        '
        Me.SqlCommand13.CommandText = "INSERT INTO VStockedItems(BRID, MTID, SIConversionToPrimary, MTPrimaryUOM, SIName" & _
        ", SIInventoryUOM, SICurrentInventory, SICurrentValue, MTPrimaryUOMShortName, SIC" & _
        "urrentPurchaseCost, SICurrentInventoryInPrimaryUOM, SIInventoryUOMShortName) VAL" & _
        "UES (@BRID, @MTID, @SIConversionToPrimary, @MTPrimaryUOM, @SIName, @SIInventoryU" & _
        "OM, @SICurrentInventory, @SICurrentValue, @MTPrimaryUOMShortName, @SICurrentPurc" & _
        "haseCost, @SICurrentInventoryInPrimaryUOM, @SIInventoryUOMShortName); SELECT BRI" & _
        "D, SIID, MTID, SIConversionToPrimary, MTPrimaryUOM, SIName, SIInventoryUOM, SICu" & _
        "rrentInventory, SICurrentValue, MTPrimaryUOMShortName, SICurrentPurchaseCost, SI" & _
        "CurrentInventoryInPrimaryUOM, SIInventoryUOMShortName FROM VStockedItems"
        Me.SqlCommand13.Connection = Me.SqlConnection
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIConversionToPrimary", System.Data.SqlDbType.Real, 4, "SIConversionToPrimary"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTPrimaryUOM", System.Data.SqlDbType.VarChar, 2, "MTPrimaryUOM"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIName", System.Data.SqlDbType.VarChar, 50, "SIName"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIInventoryUOM", System.Data.SqlDbType.VarChar, 2, "SIInventoryUOM"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SICurrentInventory", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "SICurrentInventory", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SICurrentValue", System.Data.SqlDbType.Money, 8, "SICurrentValue"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTPrimaryUOMShortName", System.Data.SqlDbType.VarChar, 50, "MTPrimaryUOMShortName"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SICurrentPurchaseCost", System.Data.SqlDbType.Money, 8, "SICurrentPurchaseCost"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SICurrentInventoryInPrimaryUOM", System.Data.SqlDbType.Real, 4, "SICurrentInventoryInPrimaryUOM"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIInventoryUOMShortName", System.Data.SqlDbType.VarChar, 50, "SIInventoryUOMShortName"))
        '
        'SqlCommand14
        '
        Me.SqlCommand14.CommandText = "SELECT BRID, SIID, MTID, SIConversionToPrimary, MTPrimaryUOM, SIName, SIInventory" & _
        "UOM, SICurrentInventory, SICurrentValue, MTPrimaryUOMShortName, SICurrentPurchas" & _
        "eCost, SICurrentInventoryInPrimaryUOM, SIInventoryUOMShortName FROM VStockedItem" & _
        "s WHERE (BRID = @BRID)"
        Me.SqlCommand14.Connection = Me.SqlConnection
        Me.SqlCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'frmJob2
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(794, 568)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.ListBox)
        Me.Controls.Add(Me.pccContacts)
        Me.DockPadding.All = 8
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmJob2"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Job"
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMain.ResumeLayout(False)
        CType(Me.pnlInternalOrders, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlInternalOrders.ResumeLayout(False)
        CType(Me.dgInternalOrders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvInternalOrders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsOrders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvInternalOrders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlJobPurchaseOrders, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlJobPurchaseOrders.ResumeLayout(False)
        CType(Me.dgJobPurchaseOrders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvJobPurchaseOrders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvJobPurchaseOrders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlClientInformation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlClientInformation.ResumeLayout(False)
        CType(Me.rgLDUseContactFalse.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pceContact.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pccContacts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgLDUseContactTrue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientWorkPhoneNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientFirstName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBReferenceName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientSuburb.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientState.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientPostCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientHomePhoneNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientFaxNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientMobileNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlJobInformation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlJobInformation.ResumeLayout(False)
        CType(Me.dpJBScheduledStartDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dpJBScheduledFinishDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBJobInsurance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBJobType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dpJBActualFinishDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBCabinetryBAOTPrice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBGraniteBAOTPrice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBPrice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlClientPayments, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlClientPayments.ResumeLayout(False)
        CType(Me.dgClientPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvClientPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvClientPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCurrencyClientPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgDiscounts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvDiscounts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvDiscounts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCurrencyDiscounts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgBadDebts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvBadDebts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvBadDebts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCurrencyBadDebts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlAppliances, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlAppliances.ResumeLayout(False)
        CType(Me.dgAppliances, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvAppliances, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvAppliances, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlDirectLabour, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlDirectLabour.ResumeLayout(False)
        CType(Me.dgDirectLabour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvJobs_DirectLabour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvDirectLabour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgPayAsDirectLabour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDollarDirectLabour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPercentageDirectLabour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlSummary, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlSummary.ResumeLayout(False)
        CType(Me.lblJBOtherReduction.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsReports, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblJBBalance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblJBClientPaid.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblJBPriceTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblJBJobInsurance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblJBPriceOtherTrades.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblJBPriceAppliances.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblJBPrice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlSalesReps, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlSalesReps.ResumeLayout(False)
        CType(Me.dgSalesReps, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvJobs_SalesReps, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvSalesReps, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgPayAsSalesReps, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDollarSalesRep, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPercentageSalesReps, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlNotes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlNotes.ResumeLayout(False)
        CType(Me.dgNotes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsNotes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvNotes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEXID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvExpenses2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtINDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtINNotes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtINFollowUpText, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlJobAddress, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlJobAddress.ResumeLayout(False)
        CType(Me.txtJBJobStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgJBJobAddressAsAbove.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBJobStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBJobSuburb.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBJobState.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBJobPostCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlOtherTrades, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlOtherTrades.ResumeLayout(False)
        CType(Me.dgOtherTrades, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvOtherTrades, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvOtherTrades, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlIntro, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlIntro.ResumeLayout(False)
        CType(Me.PictureEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlResponsibilities, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlResponsibilities.ResumeLayout(False)
        CType(Me.dgResponsibilities, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvResponsibilities, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPercentOfJob, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlJobIssues, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlJobIssues.ResumeLayout(False)
        CType(Me.dgJobIssues, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvJobs_Materials, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvJobs_Granite, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvExpenses, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnOK.Enabled = Not HasRole("branch_read_only")
        ListBox.SelectedIndex = 0
        PopulateJBCabinetryBAOTPrice()
    End Sub

    Private Sub LoadTreeViews(ByVal BRID As Integer)
        ' --- DIRECT LABOR ---
        tvDirectLabour.Filter.SetOrAddField("BRID", BRID)
        tvDirectLabour.LoadFromStream(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream("WindowsApplication.xmlJobsDirectLabourTree.xml"))
        tvDirectLabour.ExpandAll()
        ' --- SALES REPS ---
        tvSalesReps.Filter.SetOrAddField("BRID", BRID)
        tvSalesReps.LoadFromStream(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream("WindowsApplication.xmlJobsSalesReps.xml"))
        tvSalesReps.ExpandAll()
    End Sub

    Private Sub FillPreliminaryData(ByVal BRID As Integer)
        ' --- JOB TYPES ---
        daJobTypes.Fill(dataSet)
        ' --- EXPENSES ---
        daExpenses.SelectCommand.Parameters("@BRID").Value = BRID
        daExpenses.Fill(dataSet)
        daExpenses.Fill(DsNotes)
        daExpenses.Fill(DsOrders)
        ' --- INFollowUpTypes ---
        daINFollowUpTypes.Fill(DsNotes)
        ' --- SHOW HOURS ---
        ShowHours = DataAccess.ShowHours(BRID, SqlConnection)
        ' Materials
        daMaterials.SelectCommand.Parameters("@BRID").Value = BRID
        daMaterials.Fill(DsOrders)
        daStockedItems.SelectCommand.Parameters("@BRID").Value = BRID
        daStockedItems.Fill(DsOrders)
    End Sub

    Private Sub FillData()
        ' --- ID NOTES ---
        daIDNotes.SelectCommand.Parameters("@BRID").Value = DataRow("BRID")
        daIDNotes.SelectCommand.Parameters("@ID").Value = DataRow("ID")
        daIDNotes.Fill(DsNotes)
        ' --- ORDERS ---
        daOrders.SelectCommand.Parameters("@BRID").Value = DataRow("BRID")
        daOrders.SelectCommand.Parameters("@JBID").Value = DataRow("JBID")
        daOrders.Fill(DsOrders)
        ' --- ORDERS_MATERIALS ---
        daOrders_Materials.SelectCommand.Parameters("@BRID").Value = DataRow("BRID")
        daOrders_Materials.SelectCommand.Parameters("@JBID").Value = DataRow("JBID")
        daOrders_Materials.Fill(DsOrders)
        ' --- ORDERS_MATERIALS_SUBITEMS ---
        daOrders_Materials_SubItems.SelectCommand.Parameters("@BRID").Value = DataRow("BRID")
        daOrders_Materials_SubItems.SelectCommand.Parameters("@JBID").Value = DataRow("JBID")
        daOrders_Materials_SubItems.Fill(DsOrders)
        ' --- ORDERS_STOCKEDITEMS ---
        daOrders_StockedItems.SelectCommand.Parameters("@BRID").Value = DataRow("BRID")
        daOrders_StockedItems.SelectCommand.Parameters("@JBID").Value = DataRow("JBID")
        daOrders_StockedItems.Fill(DsOrders)
        ' --- Customer PAYMENTS ---
        daClientPayments.SelectCommand.Parameters("@BRID").Value = DataRow("BRID")
        daClientPayments.SelectCommand.Parameters("@JBID").Value = DataRow("JBID")
        daClientPayments.Fill(dataSet)
        ' --- DIRECT LABOR ---
        daJobs_Expenses.SelectCommand.Parameters("@BRID").Value = DataRow("BRID")
        daJobs_Expenses.SelectCommand.Parameters("@JBID").Value = DataRow("JBID")
        daJobs_Expenses.Fill(dataSet)
        ' --- APPLIANCES AND OTHER TRADES ---
        daJobNonCoreSalesItems.SelectCommand.Parameters("@BRID").Value = DataRow("BRID")
        daJobNonCoreSalesItems.SelectCommand.Parameters("@JBID").Value = DataRow("JBID")
        daJobNonCoreSalesItems.Fill(dataSet)
        ' --- JOB ISSUES ---
        daJobs_Jobissues.SelectCommand.Parameters("@BRID").Value = DataRow("BRID")
        daJobs_Jobissues.SelectCommand.Parameters("@JBID").Value = DataRow("JBID")
        daJobs_Jobissues.Fill(dataSet)
        ' --- SUMMARY ---
        LoadSummary()
        ' --- CONTACT ---
        If Not DataRow("CTID") Is DBNull.Value Then
            If LoadContactDataRow(DataRow("BRID"), DataRow("CTID")) Then
                pceContact.Text = ContactDataRow("CTFirstName") & " (" & ContactDataRow("CTStreetAddress01)") & ")"
            End If
        End If
    End Sub

    Private Sub UpdateData()
        SqlDataAdapter.Update(dataSet)
        daJobs_Expenses.Update(dataSet)
        daClientPayments.Update(dataSet)
        daJobs_Jobissues.Update(dataSet)
        daIDNotes.Update(DsNotes)
        daJobNonCoreSalesItems.Update(dataSet)

        ' Update JBID
        For Each row As DataRow In DsOrders.VOrders
            If row.RowState <> DataRowState.Deleted Then
                row("JBID") = DataRow("JBID")
            End If
        Next

        'update orders
        Dim deletionsDataSet As DataSet = DsOrders.GetChanges(DataRowState.Deleted)
        Dim insertionsDataSet As DataSet = DsOrders.GetChanges(DataRowState.Added + DataRowState.Modified)
        If Not deletionsDataSet Is Nothing Then
            daOrders_Materials_SubItems.Update(deletionsDataSet)
            daOrders_Materials.Update(deletionsDataSet)
            daOrders_StockedItems.Update(deletionsDataSet)
            daOrders.Update(deletionsDataSet)
        End If
        'If Not insertionsDataSet Is Nothing Then
        daOrders.Update(DsOrders)
            daOrders_Materials.Update(DsOrders)
            daOrders_StockedItems.Update(DsOrders)
            daOrders_Materials_SubItems.Update(DsOrders)


        'daOrders.Update(insertionsDataSet)
        'daOrders_Materials.Update(insertionsDataSet)
        'daOrders_StockedItems.Update(insertionsDataSet)
        'daOrders_Materials_SubItems.Update(insertionsDataSet)


        'DsOrders.AcceptChanges()





        'End If
    End Sub

    Private Sub EnableDisable()
        lblJobStreetAddress.Enabled = Not rgJBJobAddressAsAbove.EditValue
        lblJobSuburb.Enabled = Not rgJBJobAddressAsAbove.EditValue
        lblJobState.Enabled = Not rgJBJobAddressAsAbove.EditValue
        lblJobPostCode.Enabled = Not rgJBJobAddressAsAbove.EditValue
        txtJBJobStreetAddress01.Enabled = Not rgJBJobAddressAsAbove.EditValue
        txtJBJobStreetAddress02.Enabled = Not rgJBJobAddressAsAbove.EditValue
        txtJBJobSuburb.Enabled = Not rgJBJobAddressAsAbove.EditValue
        txtJBJobState.Enabled = Not rgJBJobAddressAsAbove.EditValue
        txtJBJobPostCode.Enabled = Not rgJBJobAddressAsAbove.EditValue
    End Sub

    Private Sub CustomiseScreen()
        colJEHours.Visible = ShowHours
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        ' EndEdit() to end editing the dataset record so that we can update
        DataRow.EndEdit()

        UpdateData()

        OK = True
        Me.Close()
    End Sub

    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            DataRow.EndEdit()
            If dataSet.HasChanges Or DsNotes.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If response = MsgBoxResult.No Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private HelpTopic As String = Nothing
    Private Sub ListBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox.SelectedIndexChanged
        Dim pnl As DevExpress.XtraEditors.GroupControl
        pnl = Nothing
        Select Case ListBox.SelectedIndex
            Case 0
                pnl = pnlIntro
                HelpTopic = Nothing
            Case 1
                pnl = pnlClientInformation
                HelpTopic = "ClientInformation"
            Case 2
                pnl = pnlJobAddress
                HelpTopic = "JobAddress"
            Case 3
                pnl = pnlJobInformation
                HelpTopic = "JobInformation"
            Case 4
                pnl = pnlJobIssues
                HelpTopic = "JobIssues"
            Case 5
                pnl = pnlJobPurchaseOrders
                HelpTopic = "StockedMaterial"
            Case 6
                pnl = pnlInternalOrders
                HelpTopic = "InternalOrders"
            Case 7
                pnl = pnlDirectLabour
                HelpTopic = "DirectLabour"
            Case 8
                pnl = pnlSalesReps
                HelpTopic = "Salespeople"
            Case 9
                pnl = pnlResponsibilities
                gvResponsibilities.ExpandAllGroups()
                HelpTopic = "Responsibilities"
            Case 10
                pnl = pnlAppliances
                HelpTopic = "Appliances"
            Case 11
                pnl = pnlOtherTrades
                HelpTopic = "OtherTrades"
            Case 12
                pnl = pnlClientPayments
                HelpTopic = "ClientPayments"
            Case 13
                pnl = pnlNotes
                HelpTopic = "Notes"
            Case 14
                pnl = pnlSummary
                UpdateSummary()
                HelpTopic = Nothing
        End Select
        For Each pnl2 As DevExpress.XtraEditors.GroupControl In pnlMain.Controls
            pnl.Enabled = False
        Next
        If Not pnl Is Nothing Then
            pnl.Enabled = True
            pnl.Dock = DockStyle.Fill
            Power.Library.Library.CenterControls(pnl)
            pnl.BringToFront()
        End If
    End Sub

#Region " Price "

    Private Sub txtJBPrice_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtJBPrice.Validated, txtJBGraniteBAOTPrice.Validated
        PopulateJBCabinetryBAOTPrice()
    End Sub

    Private Sub PopulateJBCabinetryBAOTPrice()
        If IsNumeric(txtJBPrice.Text) And IsNumeric(txtJBGraniteBAOTPrice.Text) Then
            txtJBCabinetryBAOTPrice.EditValue = CDbl(txtJBPrice.EditValue) - CDbl(txtJBGraniteBAOTPrice.EditValue)
        Else
            txtJBCabinetryBAOTPrice.EditValue = DBNull.Value
        End If
    End Sub

#End Region

#Region " Job Purchase Orders "

    Public ReadOnly Property SelectedJobPurchaseOrder() As DataRow
        Get
            If Not gvJobPurchaseOrders.GetSelectedRows Is Nothing Then
                Return gvJobPurchaseOrders.GetDataRow(gvJobPurchaseOrders.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub dgJobPurchaseOrders_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgJobPurchaseOrders.DoubleClick
        btnEditJobPurchaseOrder_Click(sender, e)
    End Sub

    Private Sub btnAddJobPurchaseOrder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddJobPurchaseOrder.Click
        Dim c As Cursor = Me.Cursor
        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        frmJobPurchaseOrder2.Add(dvJobPurchaseOrders.Table, DataRow)
        Me.Cursor = c
    End Sub

    Private Sub btnEditJobPurchaseOrder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditJobPurchaseOrder.Click
        If Not SelectedJobPurchaseOrder Is Nothing Then
            If SelectedJobPurchaseOrder.RowState = DataRowState.Added Then
                frmJobPurchaseOrder2.Edit(SelectedJobPurchaseOrder, DataRow)
            Else
                If AddOrderLock(SelectedJobPurchaseOrder.Item("ORID")) Then
                    frmJobPurchaseOrder2.Edit(SelectedJobPurchaseOrder, DataRow)
                Else
                    Message.CurrentlyAccessed("order")
                End If
            End If
        End If
    End Sub

    Private Sub btnRemoveJobPurchaseOrder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveJobPurchaseOrder.Click
        If Not SelectedJobPurchaseOrder Is Nothing Then
            If SelectedJobPurchaseOrder.RowState = DataRowState.Added Then
                SelectedJobPurchaseOrder.Delete()
            Else
                If AddOrderLock(SelectedJobPurchaseOrder.Item("ORID")) Then
                    SelectedJobPurchaseOrder.Delete()
                Else
                    Message.CurrentlyAccessed("order")
                End If
            End If
        End If
    End Sub

#End Region

#Region " Internal Orders "

    Public ReadOnly Property SelectedInternalOrder() As DataRow
        Get
            If Not gvInternalOrders.GetSelectedRows Is Nothing Then
                Return gvInternalOrders.GetDataRow(gvInternalOrders.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub dgInternalOrders_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgInternalOrders.DoubleClick
        btnEditInternalOrder_Click(sender, e)
    End Sub

    Private Sub btnAddInternalOrder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddInternalOrder.Click
        Dim c As Cursor = Me.Cursor
        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        frmInternalOrder2.Add(dvInternalOrders.Table, DataRow("BRID"), DataRow("JBID"), DataRow("ID"))
        Me.Cursor = c
    End Sub

    Private Sub btnEditInternalOrder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditInternalOrder.Click
        If Not SelectedInternalOrder Is Nothing Then
            If SelectedInternalOrder.RowState = DataRowState.Added Then
                frmInternalOrder2.Edit(SelectedInternalOrder)
            Else
                If AddOrderLock(SelectedInternalOrder.Item("ORID")) Then
                    frmInternalOrder2.Edit(SelectedInternalOrder)
                Else
                    Message.CurrentlyAccessed("order")
                End If
            End If
        End If
    End Sub

    Private Sub btnRemoveInternalOrder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveInternalOrder.Click
        If Not SelectedInternalOrder Is Nothing Then
            If SelectedInternalOrder.RowState = DataRowState.Added Then
                SelectedInternalOrder.Delete()
            Else
                If AddOrderLock(SelectedInternalOrder.Item("ORID")) Then
                    SelectedInternalOrder.Delete()
                Else
                    Message.CurrentlyAccessed("order")
                End If
            End If
        End If
    End Sub

#End Region

    Private EditedOrderList As New ArrayList

    Private Function AddOrderLock(ByVal ORID As Long) As Boolean
        If Not IsInEditOrderList(ORID) Then
            If DataAccess.spExecLockRequest("sp_GetOrderLock", DataRow("BRID"), ORID, SqlConnection) Then
                EditedOrderList.Add(ORID)
                Return True
            Else
                Return False
            End If
        Else
            Return True
        End If
    End Function

    Private Function IsInEditOrderList(ByVal APID As Long) As Boolean
        For Each lng As Long In EditedOrderList
            If lng = APID Then
                Return True
            End If
        Next
        Return False
    End Function

    Private Sub ReleaseOrderLocks()
        For Each lng As Long In EditedOrderList
            DataAccess.spExecLockRequest("sp_ReleaseOrderLock", DataRow("BRID"), lng, SqlConnection)
        Next
    End Sub

#Region " Direct Labor "

    Public ReadOnly Property SelectedDirectLabour() As DataRow
        Get
            If Not gvDirectLabour.GetSelectedRows Is Nothing Then
                Return gvDirectLabour.GetDataRow(gvDirectLabour.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub btnAddDirectLabour_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddDirectLabour.Click
        If CType(tvDirectLabour.SelectedNode, Power.Forms.TreeNode).Action = "LIST" Then
            Message.ShowMessage("You cannot add this item. You may however select from the list of " & tvDirectLabour.SelectedNode.Text & ".", MsgBoxStyle.Information)
            Exit Sub
        End If
        Dim row As dsJobs.VJobs_ExpensesRow = dataSet.VJobs_Expenses.NewVJobs_ExpensesRow
        Dim expense As dsJobs.VExpensesRow = dataSet.VExpenses.FindByBRIDEXID(DataRow("BRID"), CType(tvDirectLabour.SelectedNode, Power.Forms.TreeNode).PKey("EXID"))
        row.BRID = expense.BRID
        row.JBID = DataRow("JBID")
        row.EXID = expense.EXID
        row.EGID = expense.EGID
        row.EXName = expense.EXName
        row.EGName = expense.EGName
        row.EGType = expense.EGType
        row.PMAllowValueInJobScreen = expense.PMAllowValueInJobScreen
        row.PMAllowHoursInJobScreen = expense.PMAllowHoursInJobScreen
        row.JEAsPercent = 0
        row.JEPercentageOfJob = 1
        Try
            dataSet.VJobs_Expenses.AddVJobs_ExpensesRow(row)
            DistributeResponsibilitiesEvenly_Add(row)
        Catch ex As System.Data.ConstraintException
            row.Delete()
            Message.ShowMessage(tvDirectLabour.SelectedNode.Text & " could not be added, as it already exists in the job.", MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub btnRemoveDirectLabour_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveDirectLabour.Click
        If Not SelectedDirectLabour Is Nothing Then
            Dim row As DataRow = SelectedDirectLabour
            DistributeResponsibilitiesEvenly_Remove(row)
            row.Delete()
        End If
    End Sub

#End Region

#Region " Sales Reps "

    Public ReadOnly Property SelectedSalesRep() As DataRow
        Get
            If Not gvSalesReps.GetSelectedRows Is Nothing Then
                Return gvSalesReps.GetDataRow(gvSalesReps.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub btnAddSalesRep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddSalesRep.Click
        If CType(tvSalesReps.SelectedNode, Power.Forms.TreeNode).Action = "LIST" Then
            Message.ShowMessage("You cannot add this item. You may however select from the list of " & tvSalesReps.SelectedNode.Text & ".", MsgBoxStyle.Information)
            Exit Sub
        End If
        Dim row As dsJobs.VJobs_ExpensesRow = dataSet.VJobs_Expenses.NewVJobs_ExpensesRow
        Dim expense As dsJobs.VExpensesRow = dataSet.VExpenses.FindByBRIDEXID(DataRow("BRID"), CType(tvSalesReps.SelectedNode, Power.Forms.TreeNode).PKey("EXID"))
        row.JBID = DataRow("JBID")
        row.BRID = expense.BRID
        row.EXID = expense.EXID
        row.EGID = expense.EGID
        row.EXName = expense.EXName
        row.EGName = expense.EGName
        row.EGType = expense.EGType
        row.PMAllowValueInJobScreen = expense.PMAllowValueInJobScreen
        row.PMAllowHoursInJobScreen = expense.PMAllowHoursInJobScreen
        row.JEAsPercent = 1
        row.JEPercentageOfJob = 1
        Try
            dataSet.VJobs_Expenses.AddVJobs_ExpensesRow(row)
            DistributeResponsibilitiesEvenly_Add(row)
        Catch ex As System.Data.ConstraintException
            row.Delete()
            Message.ShowMessage(tvSalesReps.SelectedNode.Text & " could not be added, as it already exists in the job.", MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub btnRemoveSalesRep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveSalesRep.Click
        If Not SelectedSalesRep Is Nothing Then
            Dim row As DataRow = SelectedSalesRep
            DistributeResponsibilitiesEvenly_Remove(row)
            row.Delete()
        End If
    End Sub

#End Region

#Region " Customer Payments "

    Private Sub gvClientPayments_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gvClientPayments.InitNewRow
        gvClientPayments.GetDataRow(e.RowHandle)("BRID") = DataRow("BRID")
        gvClientPayments.GetDataRow(e.RowHandle)("JBID") = DataRow("JBID")
        gvClientPayments.GetDataRow(e.RowHandle)("CPType") = "CP"
    End Sub

    Private Sub gvClientPayments_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gvClientPayments.KeyDown
        If e.KeyCode = Keys.Delete Then
            If gvClientPayments.SelectedRowsCount > 0 Then
                gvClientPayments.GetDataRow(gvClientPayments.GetSelectedRows(0)).Delete()
            End If
        End If
    End Sub
    Public ReadOnly Property SelectedClientPayment() As DataRow
        Get
            If Not gvClientPayments.GetSelectedRows Is Nothing Then
                Return gvClientPayments.GetDataRow(gvClientPayments.GetSelectedRows(0))
            End If
        End Get
    End Property
    Private Sub btnRemoveClientPayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveClientPayment.Click
        If Not SelectedClientPayment Is Nothing Then
            SelectedClientPayment.Delete()
        End If
    End Sub
    Public ReadOnly Property SelectedDiscount() As DataRow
        Get
            If Not gvDiscounts.GetSelectedRows Is Nothing Then
                Return gvDiscounts.GetDataRow(gvDiscounts.GetSelectedRows(0))
            End If
        End Get
    End Property
    Private Sub gvDiscounts_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gvDiscounts.InitNewRow
        gvDiscounts.GetDataRow(e.RowHandle)("BRID") = DataRow("BRID")
        gvDiscounts.GetDataRow(e.RowHandle)("JBID") = DataRow("JBID")
        gvDiscounts.GetDataRow(e.RowHandle)("CPType") = "DC"
    End Sub

    Private Sub gvDiscounts_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gvDiscounts.KeyDown
        If e.KeyCode = Keys.Delete Then
            If gvDiscounts.SelectedRowsCount > 0 Then
                gvDiscounts.GetDataRow(gvDiscounts.GetSelectedRows(0)).Delete()
            End If
        End If
    End Sub

    Private Sub btnRemoveDiscount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveDiscount.Click
        If Not SelectedDiscount Is Nothing Then
            SelectedDiscount.Delete()
        End If
    End Sub

    Private Sub gvBadDebts_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gvBadDebts.InitNewRow
        gvBadDebts.GetDataRow(e.RowHandle)("BRID") = DataRow("BRID")
        gvBadDebts.GetDataRow(e.RowHandle)("JBID") = DataRow("JBID")
        gvBadDebts.GetDataRow(e.RowHandle)("CPType") = "BD"
    End Sub

    Private Sub gvBadDebts_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gvBadDebts.KeyDown
        If e.KeyCode = Keys.Delete Then
            If gvBadDebts.SelectedRowsCount > 0 Then
                gvBadDebts.GetDataRow(gvBadDebts.GetSelectedRows(0)).Delete()
            End If
        End If
    End Sub
    Public ReadOnly Property SelectedBadDebt() As DataRow
        Get
            If Not gvBadDebts.GetSelectedRows Is Nothing Then
                Return gvBadDebts.GetDataRow(gvBadDebts.GetSelectedRows(0))
            End If
        End Get
    End Property
    Private Sub btnRemoveBadDebt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveBadDebt.Click
        If Not SelectedBadDebt Is Nothing Then
            SelectedBadDebt.Delete()
        End If
    End Sub

    Private Sub gvClientPayments_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gvClientPayments.InvalidRowException, gvDiscounts.InvalidRowException, gvBadDebts.InvalidRowException
        GridView_InvalidRowException(sender, e)
    End Sub

#End Region

#Region " Appliances "

    Public ReadOnly Property SelectedAppliance() As DataRow
        Get
            If Not gvAppliances.GetSelectedRows Is Nothing Then
                Return gvAppliances.GetDataRow(gvAppliances.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub dgAppliances_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgAppliances.DoubleClick
        btnEditAppliance_Click(sender, e)
    End Sub

    Private Sub btnAddAppliance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAppliance.Click
        frmJobAppliance2.Add(dvAppliances.Table, DataRow("BRID"), DataRow("JBID"))
    End Sub

    Private Sub btnEditAppliance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditAppliance.Click
        If Not SelectedAppliance Is Nothing Then
            frmJobAppliance2.Edit(SelectedAppliance)
        End If
    End Sub

    Private Sub btnRemoveAppliance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveAppliance.Click
        If Not SelectedAppliance Is Nothing Then
            SelectedAppliance.Delete()
        End If
    End Sub

#End Region

#Region " Other Trades "

    Public ReadOnly Property SelectedOtherTrade() As DataRow
        Get
            If Not gvOtherTrades.GetSelectedRows Is Nothing Then
                Return gvOtherTrades.GetDataRow(gvOtherTrades.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub dgOtherTrades_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgOtherTrades.DoubleClick
        btnEditOtherTrade_Click(sender, e)
    End Sub

    Private Sub btnAddOtherTrade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddOtherTrade.Click
        frmJobOtherTrade2.Add(dvAppliances.Table, DataRow("BRID"), DataRow("JBID"))
    End Sub

    Private Sub btnEditOtherTrade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditOtherTrade.Click
        If Not SelectedOtherTrade Is Nothing Then
            frmJobOtherTrade2.Edit(SelectedOtherTrade)
        End If
    End Sub

    Private Sub btnRemoveOtherTrade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveOtherTrade.Click
        If Not SelectedOtherTrade Is Nothing Then
            SelectedOtherTrade.Delete()
        End If
    End Sub

#End Region

#Region " Notes "

    Public ReadOnly Property SelectedNote() As DataRow
        Get
            If Not gvNotes.GetSelectedRows Is Nothing Then
                Return gvNotes.GetDataRow(gvNotes.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub gvNotes_InitNewRow(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) _
        Handles gvNotes.InitNewRow
        gvNotes.GetDataRow(e.RowHandle)("BRID") = DataRow("BRID")
        gvNotes.GetDataRow(e.RowHandle)("ID") = DataRow("ID")
        gvNotes.GetDataRow(e.RowHandle)("INFollowUpText") = DsNotes.INFollowUpTypes.FindByINFollowUpType _
            (gvNotes.GetDataRow(e.RowHandle)("INFollowUpType"))("INFollowUpTypeDesc")
    End Sub

    Private Sub gvNotes_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) _
    Handles gvNotes.KeyDown
        If e.KeyCode = Keys.Delete Then
            If gvNotes.SelectedRowsCount > 0 Then
                gvNotes.GetDataRow(gvNotes.GetSelectedRows(0)).Delete()
            End If
        End If
    End Sub

    Private Sub gvNotes_InvalidRowException(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) _
    Handles gvNotes.InvalidRowException
        GridView_InvalidRowException(sender, e)
    End Sub

    Private Sub txtINFollowUpText_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) _
    Handles txtINFollowUpText.ButtonClick
        If SelectedNote Is Nothing Then
            gvNotes.AddNewRow()
        End If
        frmFollowUp2.Edit(SelectedNote)
        gvNotes.RefreshRow(gvNotes.GetSelectedRows(0))
    End Sub

    Private Sub txtINNotes_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) _
    Handles txtINNotes.ButtonClick
        If SelectedNote Is Nothing Then
            gvNotes.AddNewRow()
        End If
        SelectedNote("INNotes") = frmMemo.Edit(IsNull(SelectedNote("INNotes"), ""), "Notes")
        gvNotes.RefreshRow(gvNotes.GetSelectedRows(0))
    End Sub

#End Region

    Private Sub DistributeResponsibilitiesEvenly_Add(ByVal row As DataRow)
        Dim total, newtotal As Double
        total = 0
        newtotal = 0
        Dim count As Int16 = 0
        For Each r As DataRow In row.Table.Rows
            If Not r.RowState = DataRowState.Deleted Then
                If r("EGID") = row("EGID") And Not r Is row Then
                    total += r("JEPercentageOfJob")
                    count += 1
                End If
            End If
        Next
        If total >= 0.9999 And total <= 1 Then
            total = 1
        End If
        If count > 0 Then
            newtotal = (total - total / (count + 1)) ' This is the total the existing rows must NOW add up to
        Else
            total = 1
        End If
        For Each r As DataRow In row.Table.Rows
            If Not r.RowState = DataRowState.Deleted Then
                If r("EGID") = row("EGID") Then
                    If Not r Is row Then
                        r("JEPercentageOfJob") = r("JEPercentageOfJob") * newtotal / total
                    Else
                        r("JEPercentageOfJob") = total / (count + 1)
                    End If
                End If
            End If
        Next
    End Sub

    Private Sub DistributeResponsibilitiesEvenly_Remove(ByVal row As DataRow)
        Dim total, newtotal As Double
        total = 0
        newtotal = 0
        Dim count As Int16 = 0
        For Each r As DataRow In row.Table.Rows
            If Not r.RowState = DataRowState.Deleted Then
                If r("EGID") = row("EGID") And Not r Is row Then
                    total += r("JEPercentageOfJob")
                    count += 1
                End If
            End If
        Next
        newtotal = total + row("JEPercentageOfJob") ' This is the total the existing rows must NOW add up to
        If newtotal >= 0.9999 And newtotal <= 1 Then
            newtotal = 1
        End If
        For Each r As DataRow In row.Table.Rows
            If Not r.RowState = DataRowState.Deleted Then
                If r("EGID") = row("EGID") And Not r Is row Then
                    r("JEPercentageOfJob") = r("JEPercentageOfJob") * newtotal / total
                End If
            End If
        Next
    End Sub

    Private Sub rgJBJobAddressAsAbove_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles rgJBJobAddressAsAbove.SelectedIndexChanged
        EnableDisable()
    End Sub

    Private Sub gvDirectLabour_ShowingEditor(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles gvDirectLabour.ShowingEditor, gvSalesReps.ShowingEditor
        Dim view As DevExpress.XtraGrid.Views.Base.ColumnView = sender
        If view.FocusedColumn.FieldName = "JECharge" And (view.GetFocusedRowCellValue("JEAsPercent") = 1 Or _
                    Not view.GetFocusedRowCellValue("PMAllowValueInJobScreen")) Or _
        view.FocusedColumn.FieldName = "JEPercent" And (view.GetFocusedRowCellValue("JEAsPercent") = 0 Or _
                    Not view.GetFocusedRowCellValue("PMAllowValueInJobScreen")) Or _
        view.FocusedColumn.FieldName = "JEAsPercent" And Not view.GetFocusedRowCellValue("PMAllowValueInJobScreen") Or _
        view.FocusedColumn.FieldName = "JEHours" And Not view.GetFocusedRowCellValue("PMAllowHoursInJobScreen") Then
            e.Cancel = True
        End If
    End Sub

    Private Sub gvDirectLabour_RowCellStyle(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs)
        Dim view As DevExpress.XtraGrid.Views.Grid.GridView = sender
        If e.Column.FieldName = "JECharge" And (view.GetRowCellValue(e.RowHandle, "JEAsPercent") = 1 Or _
                Not view.GetRowCellValue(e.RowHandle, "PMAllowValueInJobScreen")) Or _
        e.Column.FieldName = "JEPercent" And (view.GetRowCellValue(e.RowHandle, "JEAsPercent") = 0 Or _
                Not view.GetRowCellValue(e.RowHandle, "PMAllowValueInJobScreen")) Or _
        e.Column.FieldName = "JEAsPercent" And Not view.GetRowCellValue(e.RowHandle, "PMAllowValueInJobScreen") Or _
        e.Column.FieldName = "JEHours" And Not view.GetRowCellValue(e.RowHandle, "PMAllowHoursInJobScreen") Then
            Dim appear As New DevExpress.Utils.AppearanceObject
            appear.BackColor = System.Drawing.SystemColors.Control
            appear.ForeColor = System.Drawing.SystemColors.GrayText
            e.CombineAppearance(appear)
        End If
    End Sub

    Private Sub gvDirectLabour_CustomDrawCardFieldCaption(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs) _
    Handles gvDirectLabour.CustomDrawCardFieldCaption, gvSalesReps.CustomDrawCardFieldCaption
        CustomDrawCardFieldAppearance(sender, e)
    End Sub

    Private Sub gvDirectLabour_CustomDrawCardFieldValue(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs) _
    Handles gvDirectLabour.CustomDrawCardFieldValue, gvSalesReps.CustomDrawCardFieldValue
        CustomDrawCardFieldAppearance(sender, e)
    End Sub

    Private Sub CustomDrawCardFieldAppearance(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs)
        Dim view As DevExpress.XtraGrid.Views.Base.ColumnView = sender
        If e.Column.FieldName = "JECharge" And (view.GetRowCellValue(e.RowHandle, "JEAsPercent") = 1 Or _
                Not view.GetRowCellValue(e.RowHandle, "PMAllowValueInJobScreen")) Or _
        e.Column.FieldName = "JEPercent" And (view.GetRowCellValue(e.RowHandle, "JEAsPercent") = 0 Or _
                Not view.GetRowCellValue(e.RowHandle, "PMAllowValueInJobScreen")) Or _
        e.Column.FieldName = "JEAsPercent" And Not view.GetRowCellValue(e.RowHandle, "PMAllowValueInJobScreen") Or _
        e.Column.FieldName = "JEHours" And Not view.GetRowCellValue(e.RowHandle, "PMAllowHoursInJobScreen") Then
            Dim appear As New DevExpress.Utils.AppearanceObject
            appear.BackColor = System.Drawing.SystemColors.Control
            appear.ForeColor = System.Drawing.SystemColors.GrayText
            e.Appearance.Combine(appear)
        End If
    End Sub

    Private Sub Card_CustomDrawCardCaption(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Card.CardCaptionCustomDrawEventArgs) Handles gvDirectLabour.CustomDrawCardCaption, gvSalesReps.CustomDrawCardCaption
        Dim view As DevExpress.XtraGrid.Views.Card.CardView = CType(sender, DevExpress.XtraGrid.Views.Card.CardView)
        CType(e.CardInfo, DevExpress.XtraGrid.Views.Card.ViewInfo.CardInfo).CaptionInfo.CardCaption = _
            view.GetRowCellDisplayText(e.RowHandle, view.Columns("EXName"))
    End Sub

    Private Sub Text_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) _
            Handles txtJBClientSurname.ParseEditValue, txtJBJobType.ParseEditValue
        Format.Text_ParseEditValue(sender, e)
    End Sub

    Private Sub Decimal_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) Handles txtJBGraniteBAOTPrice.ParseEditValue, txtJBPrice.ParseEditValue, txtCurrencyClientPayments.ParseEditValue, txtPercentOfJob.ParseEditValue, txtPercentOfJob.ParseEditValue, txtCurrencyBadDebts.ParseEditValue, txtCurrencyDiscounts.ParseEditValue, txtJBJobInsurance.ParseEditValue, txtPercentageDirectLabour.ParseEditValue, txtPercentageSalesReps.ParseEditValue, txtDollarDirectLabour.ParseEditValue, txtDollarSalesRep.ParseEditValue
        Format.Decimal_ParseEditValue(sender, e)
    End Sub

    Private Sub txtPercentage_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) _
    Handles txtPercentageDirectLabour.ParseEditValue, txtPercentageSalesReps.ParseEditValue, txtPercentOfJob.ParseEditValue
        If TypeOf e.Value Is String Then
            If Trim(e.Value) = "" Then
                e.Value = DBNull.Value
            Else
                Try
                    e.Value = e.Value / 100
                Catch ex As Exception
                End Try
            End If
        End If
    End Sub

    Private Sub Date_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles dpJBScheduledStartDate.Validating, dpJBScheduledFinishDate.Validating, dpJBActualFinishDate.Validating, txtDate.Validating, txtINDate.Validating
        Library.DateEdit_Validating(sender, e)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If HelpTopic Is Nothing Then
            ShowHelpTopic(Me, "JobFinancialsTerms.html")
        Else
            ShowHelpTopic(Me, "JobFinancialsTerms.html#" & HelpTopic)
        End If
    End Sub

    Private Sub txtJBClientFirstName_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtJBClientFirstName.EditValueChanged, txtJBClientSurname.EditValueChanged, txtJBClientFirstName.Validated, txtJBClientSurname.Validated
        If Not DataRow Is Nothing Then
            Me.Text = "Job Financials " & DataRow("ID") & " - " & FormatName(DataRow("JBClientFirstName"), DataRow("JBClientSurname"))
        Else
            Me.Text = "Job Financials"
        End If
    End Sub

    Private Sub UpdateSummary()
        DataRow.EndEdit()

        Me.Cursor = Windows.Forms.Cursors.WaitCursor

        Dim jobReport As dsReports.repJobReportRow = DsReports.repJobReport(0)

        If (Not (jobReport) Is Nothing) Then

            jobReport("JBPrice") = DataRow("JBPrice")
            jobReport("JBPriceAppliances") = TotalOfField(dvAppliances, "NIPrice", "NIPaidByClient", False)
            jobReport("JBPriceOtherTrades") = TotalOfField(dvOtherTrades, "NIPrice", "NIPaidByClient", False)
            jobReport("JBJobInsurance") = DataRow("JBJobInsurance")
            If jobReport("JBPrice") Is DBNull.Value Or jobReport("JBJobInsurance") Is DBNull.Value Or
                    jobReport("JBPriceAppliances") Is DBNull.Value Or jobReport("JBPriceOtherTrades") Is DBNull.Value Then
                jobReport("JBPriceTotal") = DBNull.Value
            Else
                jobReport("JBPriceTotal") = jobReport("JBPrice") + jobReport("JBPriceAppliances") + jobReport("JBPriceOtherTrades") +
                    jobReport("JBJobInsurance")
            End If
            jobReport("JBClientPaid") = TotalOfField(dvClientPayments, "CPAmount")
            jobReport("JBOtherReduction") = TotalOfField(dvDiscounts, "CPAmount") + TotalOfField(dvBadDebts, "CPAmount")
            If jobReport("JBPriceTotal") Is DBNull.Value Or jobReport("JBClientPaid") Is DBNull.Value Or
                    jobReport("JBOtherReduction") Is DBNull.Value Then
                jobReport("JBBalance") = DBNull.Value
            Else
                jobReport("JBBalance") = jobReport("JBPriceTotal") - jobReport("JBClientPaid") - jobReport("JBOtherReduction")
            End If
        End If

        Me.Cursor = Windows.Forms.Cursors.Default
    End Sub

    Private Function TotalOfField(ByVal dataView As DataView, ByVal fieldName As String, Optional ByVal whereField As String = Nothing, Optional ByVal whereValue As Object = Nothing) As Decimal
        TotalOfField = 0
        For Each item As DataRowView In dataView
            If Not whereField Is Nothing Then
                If item(whereField) = whereValue Then
                    TotalOfField += IsNull(item(fieldName), 0)
                End If
            Else
                TotalOfField += IsNull(item(fieldName), 0)
            End If
        Next
    End Function

    Private Sub LoadSummary()
        Dim cmd As SqlClient.SqlCommand
        Dim da As SqlClient.SqlDataAdapter

        cmd = New SqlClient.SqlCommand("SELECT * FROM dbo.[repJobReport](@BRID, @JBID)", SqlConnection)
        cmd.Parameters.Add("@BRID", DataRow("BRID"))
        cmd.Parameters.Add("@JBID", DataRow("JBID"))
        da = New SqlClient.SqlDataAdapter(cmd)
        da.Fill(DsReports.repJobReport)
    End Sub

    Private Sub btnViewJobReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewJobReport.Click
        Dim cmd As SqlClient.SqlCommand
        Dim da As SqlClient.SqlDataAdapter

        DataRow.EndEdit()

        Dim response As DialogResult
        If Not HasRole("branch_read_only") Then
            If dataSet.HasChanges Then
                response = Message.SaveChangesForJobFigures
            Else
                response = MsgBoxResult.No
            End If
        Else
            If dataSet.HasChanges Then
                Message.ShowMessage("Changes have been made to your job.  To reflect these in your job price summary, your changes must be saved." & _
                                vbNewLine & vbNewLine & "Because you have read only access to this section your changes cannot be saved and will not be reflected in this report.", MessageBoxIcon.Exclamation)
            End If
            response = MsgBoxResult.No
        End If
        If response = MsgBoxResult.Yes Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor

            UpdateData()

            sp_UpdateJobStatistics(DataRow("BRID"), DataRow("JBID"), SqlConnection)

            cmd = New SqlClient.SqlCommand("SELECT * FROM dbo.[repJobReport](@BRID, @JBID)", SqlConnection)
            cmd.Parameters.Add("@BRID", DataRow("BRID"))
            cmd.Parameters.Add("@JBID", DataRow("JBID"))
            da = New SqlClient.SqlDataAdapter(cmd)
            da.Fill(DsReports.repJobReport)

            Me.Cursor = Windows.Forms.Cursors.Default
        End If

        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        Dim gui As New frmReport(Nothing)
        Dim rep As New repJobReport
        rep.Load()

        DsReports.VJobs_Expenses.Clear()
        cmd = New SqlClient.SqlCommand("SELECT * FROM VJobs_Expenses WHERE BRID = @BRID AND JBID = @JBID", SqlConnection)
        cmd.Parameters.Add("@BRID", DataRow("BRID"))
        cmd.Parameters.Add("@JBID", DataRow("JBID"))
        da = New SqlClient.SqlDataAdapter(cmd)
        da.Fill(DsReports.VJobs_Expenses)

        rep.SetDataSource(DsReports)
        'rep.SetParameterValue("Filename", MyBusinessPlan.Filename)
        gui.Display(rep)
        Me.Cursor = Windows.Forms.Cursors.Default
        gui.Text = "Job Report"
        gui.Show()
    End Sub

    Private contactsLoaded As Boolean = False
    Private lvContacts As lvContacts2
    Private dropDownOpen As Boolean = False
    Private Sub pceContact_Popup(ByVal sender As Object, ByVal e As System.EventArgs) Handles pceContact.Popup
        If contactsLoaded Then
            If Not ReopeningPopup Then
                If Not DataRow("CTID") Is DBNull.Value Then
                    lvContacts.SelectRow(DataRow("BRID"), DataRow("CTID"))
                End If
            End If
        Else
            Dim c As Cursor = Me.Cursor
            Me.Cursor = Cursors.WaitCursor
            lvContacts = New lvContacts2(Me.SqlConnection, DataRow("BRID"))
            AddHandler lvContacts.ContactSelected, AddressOf Me.ContactSelected
            AddHandler lvContacts.PopupClosed, AddressOf Me.ContactsPopupClosed
            lvContacts.DoubleClickAction = lvContacts2.ContactAction.SelectContact
            lvContacts.Dock = DockStyle.Fill
            lvContacts.Visible = False
            pccContacts.Controls.Add(lvContacts)
            lvContacts.Visible = True
            If Not ReopeningPopup Then
                If Not DataRow("CTID") Is DBNull.Value Then
                    lvContacts.SelectRow(DataRow("BRID"), DataRow("CTID"))
                End If
            End If
            lvContacts.Select()
            contactsLoaded = True
            Me.Cursor = c
        End If
        dropDownOpen = True
    End Sub

    Private ReopeningPopup As Boolean = False
    Private Sub ContactsPopupClosed()
        'Dim BRID As Integer
        'Dim CTID As Long
        'BRID = lvContacts.SelectedRowField("BRID")
        'CTID = lvContacts.SelectedRowField("CTID")
        ReopeningPopup = True
        Me.pceContact.ShowPopup()
        ReopeningPopup = False
        'lvContacts.SelectRow(BRID, CTID)
    End Sub

    Private ContactDataRow As DataRow
    Private Function LoadContactDataRow(ByVal BRID As Integer, ByVal CTID As Long) As Boolean
        dataSet.Contacts.Clear()
        daContacts.SelectCommand.Parameters("@BRID").Value = BRID
        daContacts.SelectCommand.Parameters("@CTID").Value = CTID
        daContacts.Fill(dataSet)
        If dataSet.Contacts.Count > 0 Then
            ContactDataRow = dataSet.Contacts(0)
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub ContactSelected(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal BRID As Integer, ByVal CTID As Long)
        Dim oldCTID As Object = DataRow("CTID")
        If LoadContactDataRow(BRID, CTID) Then
            Dim copyAcross As Boolean
            If IsNull(ContactDataRow("CTID"), -1) = IsNull(oldCTID, -2) And Not (IsNull(DataRow("JBClientSurname"), "") = IsNull(ContactDataRow("CTSurname"), "") And _
                    IsNull(DataRow("JBClientFirstName"), "") = IsNull(ContactDataRow("CTFirstName"), "") And _
                    IsNull(DataRow("JBReferenceName"), "") = IsNull(ContactDataRow("CTReferenceName"), "") And _
                    IsNull(DataRow("JBClientStreetAddress01"), "") = IsNull(ContactDataRow("CTStreetAddress01"), "") And _
                    IsNull(DataRow("JBClientStreetAddress02"), "") = IsNull(ContactDataRow("CTStreetAddress02"), "") And _
                    IsNull(DataRow("JBClientSuburb"), "") = IsNull(ContactDataRow("CTSuburb"), "") And _
                    IsNull(DataRow("JBClientState"), "") = IsNull(ContactDataRow("CTState"), "") And _
                    IsNull(DataRow("JBClientPostCode"), "") = IsNull(ContactDataRow("CTPostCode"), "") And _
                    IsNull(DataRow("JBClientHomePhoneNumber"), "") = IsNull(ContactDataRow("CTHomePhoneNumber"), "") And _
                    IsNull(DataRow("JBClientWorkPhoneNumber"), "") = IsNull(ContactDataRow("CTWorkPhoneNumber"), "") And _
                    IsNull(DataRow("JBClientFaxNumber"), "") = IsNull(ContactDataRow("CTFaxNumber"), "") And _
                    IsNull(DataRow("JBClientMobileNumber"), "") = IsNull(ContactDataRow("CTMobileNumber"), "") And _
                    IsNull(DataRow("JBClientEmail"), "") = IsNull(ContactDataRow("CTEmail"), "")) Then
                copyAcross = (Message.AskCopyContactDetails("job") = DialogResult.Yes)
            Else
                copyAcross = True
            End If
            If copyAcross Then
                With ContactDataRow
                    pceContact.Text = .Item("CTName") & " (" & .Item("CTAddress") & ")"
                    DataRow("CTID") = .Item("CTID")
                    DataRow("JBClientSurname") = .Item("CTSurname")
                    DataRow("JBClientFirstName") = .Item("CTFirstName")
                    DataRow("JBReferenceName") = .Item("CTReferenceName")
                    DataRow("JBClientStreetAddress01") = .Item("CTStreetAddress01")
                    DataRow("JBClientStreetAddress02") = .Item("CTStreetAddress02")
                    DataRow("JBClientSuburb") = .Item("CTSuburb")
                    DataRow("JBClientState") = .Item("CTState")
                    DataRow("JBClientPostCode") = .Item("CTPostCode")
                    DataRow("JBClientHomePhoneNumber") = .Item("CTHomePhoneNumber")
                    DataRow("JBClientWorkPhoneNumber") = .Item("CTWorkPhoneNumber")
                    DataRow("JBClientFaxNumber") = .Item("CTFaxNumber")
                    DataRow("JBClientMobileNumber") = .Item("CTMobileNumber")
                    DataRow("JBClientEmail") = .Item("CTEmail")
                End With
            End If
            dropDownOpen = False
            pceContact.ClosePopup()
        End If
    End Sub

    Private Sub pceContact_CloseUp(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.CloseUpEventArgs) Handles pceContact.CloseUp
        If Not DataRow("CTID") Is DBNull.Value Then
            If LoadContactDataRow(DataRow("BRID"), DataRow("CTID")) Then
                pceContact.Text = ContactDataRow("CTName") & " (" & ContactDataRow("CTAddress") & ")"
            End If
        End If
        e.AcceptValue = True
    End Sub

    Private Sub rgLDUseContactTrue_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgLDUseContactTrue.SelectedIndexChanged
        If rgLDUseContactTrue.EditValue = True Then
            rgLDUseContactTrue.DoValidate()
            rgLDUseContactFalse.EditValue = rgLDUseContactTrue.EditValue
            rgLDUseContactFalse.DoValidate()
            'DataRow("LDUseContact") = False
            DataRow.EndEdit()
            Me.pceContact.Enabled = True
            Me.txtJBClientSurname.Properties.ReadOnly = True
            Me.txtJBClientFirstName.Properties.ReadOnly = True
            Me.txtJBReferenceName.Properties.ReadOnly = True
            Me.txtJBClientStreetAddress01.Properties.ReadOnly = True
            Me.txtJBClientStreetAddress02.Properties.ReadOnly = True
            Me.txtJBClientSuburb.Properties.ReadOnly = True
            Me.txtJBClientState.Properties.ReadOnly = True
            Me.txtJBClientPostCode.Properties.ReadOnly = True
            Me.txtJBClientHomePhoneNumber.Properties.ReadOnly = True
            Me.txtJBClientWorkPhoneNumber.Properties.ReadOnly = True
            Me.txtJBClientFaxNumber.Properties.ReadOnly = True
            Me.txtJBClientMobileNumber.Properties.ReadOnly = True
            Me.txtJBClientEmail.Properties.ReadOnly = True
        End If
    End Sub

    Private Sub rgLDUseContactFalse_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgLDUseContactFalse.SelectedIndexChanged
        If rgLDUseContactFalse.EditValue = False Then
            rgLDUseContactFalse.DoValidate()
            rgLDUseContactTrue.EditValue = rgLDUseContactFalse.EditValue
            rgLDUseContactTrue.DoValidate()
            'DataRow("LDUseContact") = False
            DataRow.EndEdit()
            DataRow("CTID") = DBNull.Value
            Me.pceContact.EditValue = Nothing
            Me.pceContact.Enabled = False
            Me.txtJBClientSurname.Properties.ReadOnly = False
            Me.txtJBClientFirstName.Properties.ReadOnly = False
            Me.txtJBReferenceName.Properties.ReadOnly = False
            Me.txtJBClientStreetAddress01.Properties.ReadOnly = False
            Me.txtJBClientStreetAddress02.Properties.ReadOnly = False
            Me.txtJBClientSuburb.Properties.ReadOnly = False
            Me.txtJBClientState.Properties.ReadOnly = False
            Me.txtJBClientPostCode.Properties.ReadOnly = False
            Me.txtJBClientHomePhoneNumber.Properties.ReadOnly = False
            Me.txtJBClientWorkPhoneNumber.Properties.ReadOnly = False
            Me.txtJBClientFaxNumber.Properties.ReadOnly = False
            Me.txtJBClientMobileNumber.Properties.ReadOnly = False
            Me.txtJBClientEmail.Properties.ReadOnly = False
        End If
    End Sub

End Class
