Public Class frmJobOtherTrade2
    Inherits DevExpress.XtraEditors.XtraForm

    Private DataRow As DataRow

    Public Shared Sub Add(ByVal dataTable As DataTable, ByVal BRID As Integer, ByVal JBID As Int64)
        Dim gui As New frmJobOtherTrade2

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()
            .FillPreliminaryData(BRID)
            .SqlConnection.Close()

            .DataRow = dataTable.NewRow()
            .DataRow("BRID") = BRID
            .DataRow("JBID") = JBID
            .DataRow("NIType") = "OT"
            .DataRow("NIPayCommission") = 0
            .DataRow("NICommissionAsPercent") = 1
            .DataRow("NIReceived") = False
            .DataRow = .dataSet.Tables(dataTable.TableName).Rows.Add(.DataRow.ItemArray)
            .dataSet.AcceptChanges()

            If gui.ShowDialog() = .DialogResult.OK Then
                dataTable.Rows.Add(.DataRow.ItemArray)
            End If
        End With
    End Sub

    Public Shared Sub Edit(ByVal row As DataRow)
        Dim gui As New frmJobOtherTrade2

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()
            .FillPreliminaryData(row("BRID"))
            .SqlConnection.Close()

            .DataRow = .dataSet.Tables(row.Table.TableName).Rows.Add(row.ItemArray)
            .dataSet.AcceptChanges()

            If gui.ShowDialog = .DialogResult.OK Then
                UpdateOriginalRow(.DataRow, row)
            End If
        End With
    End Sub

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()
        ReadMRU(txtNIName, UserAppDataPath)
        ReadMRU(txtNIUserType, UserAppDataPath)
        'Add any initialization after the InitializeComponent() call
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents daSalesReps As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtNIUserType As DevExpress.XtraEditors.MRUEdit
    Friend WithEvents txtNIDesc As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents rdNIOrderedByClient As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents rdNIPaidByClient As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtNIPrice As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNICost As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNIName As DevExpress.XtraEditors.MRUEdit
    Friend WithEvents dvSalesReps As System.Data.DataView
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmHistory As System.Windows.Forms.ContextMenu
    Friend WithEvents miClearHistory As System.Windows.Forms.MenuItem
    Friend WithEvents tpSalesperson As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents dataSet As WindowsApplication.dsJobs
    Friend WithEvents rgNIPayCommission As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtEXIDRep As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents txtNICommissionAmount As DevExpress.XtraEditors.TextEdit
    Friend WithEvents rgNICommissionAsPercent As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtNICommissionPercent As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmJobOtherTrade2))
        Me.dataSet = New WindowsApplication.dsJobs
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.daSalesReps = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage
        Me.txtNIDesc = New DevExpress.XtraEditors.MemoEdit
        Me.txtNIUserType = New DevExpress.XtraEditors.MRUEdit
        Me.cmHistory = New System.Windows.Forms.ContextMenu
        Me.miClearHistory = New System.Windows.Forms.MenuItem
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtNIName = New DevExpress.XtraEditors.MRUEdit
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.rdNIOrderedByClient = New DevExpress.XtraEditors.RadioGroup
        Me.rdNIPaidByClient = New DevExpress.XtraEditors.RadioGroup
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.txtNIPrice = New DevExpress.XtraEditors.TextEdit
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.txtNICost = New DevExpress.XtraEditors.TextEdit
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.tpSalesperson = New DevExpress.XtraTab.XtraTabPage
        Me.rgNIPayCommission = New DevExpress.XtraEditors.RadioGroup
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.txtEXIDRep = New DevExpress.XtraEditors.LookUpEdit
        Me.dvSalesReps = New System.Data.DataView
        Me.txtNICommissionAmount = New DevExpress.XtraEditors.TextEdit
        Me.rgNICommissionAsPercent = New DevExpress.XtraEditors.RadioGroup
        Me.Label15 = New System.Windows.Forms.Label
        Me.txtNICommissionPercent = New DevExpress.XtraEditors.TextEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.txtNIDesc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNIUserType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNIName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.rdNIOrderedByClient.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rdNIPaidByClient.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage3.SuspendLayout()
        CType(Me.txtNIPrice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNICost.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpSalesperson.SuspendLayout()
        CType(Me.rgNIPayCommission.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEXIDRep.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvSalesReps, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNICommissionAmount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgNICommissionAsPercent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNICommissionPercent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dataSet
        '
        Me.dataSet.DataSetName = "dsJobs"
        Me.dataSet.EnforceConstraints = False
        Me.dataSet.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;integrated security=SSPI;data source=SERVER;" & _
        "persist security info=False;initial catalog=GTMS_DEV"
        '
        'daSalesReps
        '
        Me.daSalesReps.DeleteCommand = Me.SqlDeleteCommand2
        Me.daSalesReps.InsertCommand = Me.SqlInsertCommand2
        Me.daSalesReps.SelectCommand = Me.SqlSelectCommand2
        Me.daSalesReps.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VExpenses", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("EXName", "EXName")})})
        Me.daSalesReps.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM Expenses WHERE (BRID = @Original_BRID) AND (EXID = @Original_EXID) AN" & _
        "D (EXName = @Original_EXName OR @Original_EXName IS NULL AND EXName IS NULL)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXName", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO Expenses(BRID, EXName) VALUES (@BRID, @EXName); SELECT BRID, EXID, EX" & _
        "Name FROM Expenses WHERE (BRID = @BRID) AND (EXID = @@IDENTITY)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT BRID, EXID, EXName, EGType FROM Expenses WHERE (EGType = 'RC') AND (BRID =" & _
        " @BRID)"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection
        Me.SqlSelectCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE Expenses SET BRID = @BRID, EXName = @EXName WHERE (BRID = @Original_BRID) " & _
        "AND (EXID = @Original_EXID) AND (EXName = @Original_EXName OR @Original_EXName I" & _
        "S NULL AND EXName IS NULL); SELECT BRID, EXID, EXName FROM Expenses WHERE (BRID " & _
        "= @BRID) AND (EXID = @EXID)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(8, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 21)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Name:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(8, 80)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 21)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Description:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.XtraTabControl1.Location = New System.Drawing.Point(8, 8)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(440, 312)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.XtraTabPage3, Me.tpSalesperson})
        Me.XtraTabControl1.Text = "XtraTabControl1"
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.txtNIDesc)
        Me.XtraTabPage1.Controls.Add(Me.txtNIUserType)
        Me.XtraTabPage1.Controls.Add(Me.Label3)
        Me.XtraTabPage1.Controls.Add(Me.Label4)
        Me.XtraTabPage1.Controls.Add(Me.Label1)
        Me.XtraTabPage1.Controls.Add(Me.txtNIName)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(431, 282)
        Me.XtraTabPage1.Text = "Other Trade Details"
        '
        'txtNIDesc
        '
        Me.txtNIDesc.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "VJobsNonCoreSalesItems.NIDesc"))
        Me.txtNIDesc.EditValue = "MemoEdit1"
        Me.txtNIDesc.Location = New System.Drawing.Point(8, 104)
        Me.txtNIDesc.Name = "txtNIDesc"
        Me.txtNIDesc.Size = New System.Drawing.Size(408, 152)
        Me.txtNIDesc.TabIndex = 2
        '
        'txtNIUserType
        '
        Me.txtNIUserType.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "VJobsNonCoreSalesItems.NIUserType"))
        Me.txtNIUserType.EditValue = ""
        Me.txtNIUserType.Location = New System.Drawing.Point(104, 48)
        Me.txtNIUserType.Name = "txtNIUserType"
        '
        'txtNIUserType.Properties
        '
        Me.txtNIUserType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtNIUserType.Properties.ContextMenu = Me.cmHistory
        Me.txtNIUserType.Properties.MaxItemCount = 15
        Me.txtNIUserType.Size = New System.Drawing.Size(312, 20)
        Me.txtNIUserType.TabIndex = 1
        '
        'cmHistory
        '
        Me.cmHistory.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.miClearHistory})
        '
        'miClearHistory
        '
        Me.miClearHistory.Index = 0
        Me.miClearHistory.Text = "Clear History"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 48)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 21)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Type:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNIName
        '
        Me.txtNIName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "VJobsNonCoreSalesItems.NIName"))
        Me.txtNIName.EditValue = ""
        Me.txtNIName.Location = New System.Drawing.Point(104, 16)
        Me.txtNIName.Name = "txtNIName"
        '
        'txtNIName.Properties
        '
        Me.txtNIName.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtNIName.Properties.ContextMenu = Me.cmHistory
        Me.txtNIName.Properties.MaxItemCount = 15
        Me.txtNIName.Size = New System.Drawing.Size(312, 20)
        Me.txtNIName.TabIndex = 0
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.Label19)
        Me.XtraTabPage2.Controls.Add(Me.Label20)
        Me.XtraTabPage2.Controls.Add(Me.rdNIOrderedByClient)
        Me.XtraTabPage2.Controls.Add(Me.rdNIPaidByClient)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(431, 282)
        Me.XtraTabPage2.Text = "Ordering and Payment"
        '
        'Label19
        '
        Me.Label19.Location = New System.Drawing.Point(31, 32)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(368, 21)
        Me.Label19.TabIndex = 0
        Me.Label19.Text = "The responsibility for ordering this Other Trade will be undertaken by:"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label20
        '
        Me.Label20.Location = New System.Drawing.Point(31, 120)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(272, 21)
        Me.Label20.TabIndex = 2
        Me.Label20.Text = "This Other Trade will be invoiced to and paid for by:"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rdNIOrderedByClient
        '
        Me.rdNIOrderedByClient.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "VJobsNonCoreSalesItems.NIOrderedByClient"))
        Me.rdNIOrderedByClient.Location = New System.Drawing.Point(64, 48)
        Me.rdNIOrderedByClient.Name = "rdNIOrderedByClient"
        '
        'rdNIOrderedByClient.Properties
        '
        Me.rdNIOrderedByClient.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.rdNIOrderedByClient.Properties.Appearance.Options.UseBackColor = True
        Me.rdNIOrderedByClient.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.rdNIOrderedByClient.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "Align Kitchens"), New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "Customer")})
        Me.rdNIOrderedByClient.Size = New System.Drawing.Size(192, 48)
        Me.rdNIOrderedByClient.TabIndex = 1
        '
        'rdNIPaidByClient
        '
        Me.rdNIPaidByClient.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "VJobsNonCoreSalesItems.NIPaidByClient"))
        Me.rdNIPaidByClient.Location = New System.Drawing.Point(64, 136)
        Me.rdNIPaidByClient.Name = "rdNIPaidByClient"
        '
        'rdNIPaidByClient.Properties
        '
        Me.rdNIPaidByClient.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.rdNIPaidByClient.Properties.Appearance.Options.UseBackColor = True
        Me.rdNIPaidByClient.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.rdNIPaidByClient.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "Align Kitchens"), New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "Customer")})
        Me.rdNIPaidByClient.Size = New System.Drawing.Size(192, 48)
        Me.rdNIPaidByClient.TabIndex = 3
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.Label13)
        Me.XtraTabPage3.Controls.Add(Me.Label25)
        Me.XtraTabPage3.Controls.Add(Me.Label16)
        Me.XtraTabPage3.Controls.Add(Me.txtNIPrice)
        Me.XtraTabPage3.Controls.Add(Me.Label11)
        Me.XtraTabPage3.Controls.Add(Me.Label12)
        Me.XtraTabPage3.Controls.Add(Me.txtNICost)
        Me.XtraTabPage3.Controls.Add(Me.Label14)
        Me.XtraTabPage3.Controls.Add(Me.Label17)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(431, 282)
        Me.XtraTabPage3.Text = "Pricing and Costing"
        '
        'Label13
        '
        Me.Label13.Location = New System.Drawing.Point(32, 16)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(368, 64)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "This amount may have been entered in the Administration (Job Bookings) section if" & _
        " this figure was known at that stage. If no amount appears from the Job Booking " & _
        "stage, then enter the Sale Price of the Other Trade here. If the Sale Price has " & _
        "changed enter the new price here."
        '
        'Label25
        '
        Me.Label25.Location = New System.Drawing.Point(32, 200)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(360, 48)
        Me.Label25.TabIndex = 8
        Me.Label25.Text = "The fields above will be <Incomplete> by default and if no information is require" & _
        "d to be entered (i.e. the Other Trade is invoiced to and paid for by the custome" & _
        "r) then enter a $0 amount."
        '
        'Label16
        '
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label16.Location = New System.Drawing.Point(280, 96)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(24, 16)
        Me.Label16.TabIndex = 3
        Me.Label16.Text = "*"
        '
        'txtNIPrice
        '
        Me.txtNIPrice.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "VJobsNonCoreSalesItems.NIPrice"))
        Me.txtNIPrice.EditValue = ""
        Me.txtNIPrice.Location = New System.Drawing.Point(96, 96)
        Me.txtNIPrice.Name = "txtNIPrice"
        '
        'txtNIPrice.Properties
        '
        Me.txtNIPrice.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNIPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtNIPrice.Properties.DisplayFormat.FormatString = "c"
        Me.txtNIPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtNIPrice.Properties.EditFormat.FormatString = "c"
        Me.txtNIPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtNIPrice.Properties.NullText = "<Incomplete>"
        Me.txtNIPrice.Size = New System.Drawing.Size(184, 20)
        Me.txtNIPrice.TabIndex = 2
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(32, 96)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(64, 16)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "Sale Price:"
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(32, 168)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(32, 16)
        Me.Label12.TabIndex = 5
        Me.Label12.Text = "Cost:"
        '
        'txtNICost
        '
        Me.txtNICost.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "VJobsNonCoreSalesItems.NICost"))
        Me.txtNICost.EditValue = ""
        Me.txtNICost.Location = New System.Drawing.Point(96, 168)
        Me.txtNICost.Name = "txtNICost"
        '
        'txtNICost.Properties
        '
        Me.txtNICost.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNICost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtNICost.Properties.DisplayFormat.FormatString = "c"
        Me.txtNICost.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtNICost.Properties.EditFormat.FormatString = "c"
        Me.txtNICost.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtNICost.Properties.NullText = "<Incomplete>"
        Me.txtNICost.Size = New System.Drawing.Size(184, 20)
        Me.txtNICost.TabIndex = 6
        '
        'Label14
        '
        Me.Label14.Location = New System.Drawing.Point(32, 128)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(360, 32)
        Me.Label14.TabIndex = 4
        Me.Label14.Text = "This amount will be entered when the invoice to Align Kitchens is receiv" &
        "ed from the supplier."
        '
        'Label17
        '
        Me.Label17.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label17.Location = New System.Drawing.Point(280, 168)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(24, 16)
        Me.Label17.TabIndex = 7
        Me.Label17.Text = "*"
        '
        'tpSalesperson
        '
        Me.tpSalesperson.Controls.Add(Me.rgNIPayCommission)
        Me.tpSalesperson.Controls.Add(Me.Label18)
        Me.tpSalesperson.Controls.Add(Me.Label21)
        Me.tpSalesperson.Controls.Add(Me.Label22)
        Me.tpSalesperson.Controls.Add(Me.txtEXIDRep)
        Me.tpSalesperson.Controls.Add(Me.txtNICommissionAmount)
        Me.tpSalesperson.Controls.Add(Me.rgNICommissionAsPercent)
        Me.tpSalesperson.Controls.Add(Me.Label15)
        Me.tpSalesperson.Controls.Add(Me.txtNICommissionPercent)
        Me.tpSalesperson.Controls.Add(Me.Label2)
        Me.tpSalesperson.Controls.Add(Me.Label5)
        Me.tpSalesperson.Controls.Add(Me.Label6)
        Me.tpSalesperson.Controls.Add(Me.Label7)
        Me.tpSalesperson.Name = "tpSalesperson"
        Me.tpSalesperson.Size = New System.Drawing.Size(431, 282)
        Me.tpSalesperson.Text = "Salesperson"
        '
        'rgNIPayCommission
        '
        Me.rgNIPayCommission.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "VJobsNonCoreSalesItems.NIPayCommission"))
        Me.rgNIPayCommission.Location = New System.Drawing.Point(264, 8)
        Me.rgNIPayCommission.Name = "rgNIPayCommission"
        '
        'rgNIPayCommission.Properties
        '
        Me.rgNIPayCommission.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.rgNIPayCommission.Properties.Appearance.Options.UseBackColor = True
        Me.rgNIPayCommission.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.rgNIPayCommission.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "Yes"), New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "No")})
        Me.rgNIPayCommission.Size = New System.Drawing.Size(56, 56)
        Me.rgNIPayCommission.TabIndex = 1
        '
        'Label18
        '
        Me.Label18.Location = New System.Drawing.Point(16, 24)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(248, 16)
        Me.Label18.TabIndex = 0
        Me.Label18.Text = "Are you paying commission on this appliance?"
        '
        'Label21
        '
        Me.Label21.Location = New System.Drawing.Point(360, 248)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(16, 21)
        Me.Label21.TabIndex = 12
        Me.Label21.Text = "*"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label22
        '
        Me.Label22.Location = New System.Drawing.Point(184, 248)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(16, 21)
        Me.Label22.TabIndex = 9
        Me.Label22.Text = "*"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEXIDRep
        '
        Me.txtEXIDRep.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "VJobsNonCoreSalesItems.EXIDRep"))
        Me.txtEXIDRep.Location = New System.Drawing.Point(112, 72)
        Me.txtEXIDRep.Name = "txtEXIDRep"
        '
        'txtEXIDRep.Properties
        '
        Me.txtEXIDRep.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtEXIDRep.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXName")})
        Me.txtEXIDRep.Properties.DataSource = Me.dvSalesReps
        Me.txtEXIDRep.Properties.DisplayMember = "EXName"
        Me.txtEXIDRep.Properties.NullText = "<Incomplete>"
        Me.txtEXIDRep.Properties.ShowFooter = False
        Me.txtEXIDRep.Properties.ShowHeader = False
        Me.txtEXIDRep.Properties.ShowLines = False
        Me.txtEXIDRep.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.txtEXIDRep.Properties.ValueMember = "EXID"
        Me.txtEXIDRep.Size = New System.Drawing.Size(152, 20)
        Me.txtEXIDRep.TabIndex = 3
        '
        'dvSalesReps
        '
        Me.dvSalesReps.RowFilter = "EGType <> 'OE'"
        Me.dvSalesReps.Table = Me.dataSet.VExpenses
        '
        'txtNICommissionAmount
        '
        Me.txtNICommissionAmount.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "VJobsNonCoreSalesItems.NICommissionAmount"))
        Me.txtNICommissionAmount.EditValue = ""
        Me.txtNICommissionAmount.Location = New System.Drawing.Point(104, 248)
        Me.txtNICommissionAmount.Name = "txtNICommissionAmount"
        '
        'txtNICommissionAmount.Properties
        '
        Me.txtNICommissionAmount.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNICommissionAmount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtNICommissionAmount.Properties.DisplayFormat.FormatString = "c"
        Me.txtNICommissionAmount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtNICommissionAmount.Properties.EditFormat.FormatString = "c"
        Me.txtNICommissionAmount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtNICommissionAmount.Properties.NullText = "<Incomplete>"
        Me.txtNICommissionAmount.Size = New System.Drawing.Size(75, 20)
        Me.txtNICommissionAmount.TabIndex = 8
        '
        'rgNICommissionAsPercent
        '
        Me.rgNICommissionAsPercent.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "VJobsNonCoreSalesItems.NICommissionAsPercent"))
        Me.rgNICommissionAsPercent.Location = New System.Drawing.Point(16, 128)
        Me.rgNICommissionAsPercent.Name = "rgNICommissionAsPercent"
        '
        'rgNICommissionAsPercent.Properties
        '
        Me.rgNICommissionAsPercent.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.rgNICommissionAsPercent.Properties.Appearance.Options.UseBackColor = True
        Me.rgNICommissionAsPercent.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.rgNICommissionAsPercent.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Dollar Amount"), New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Percentage"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Both")})
        Me.rgNICommissionAsPercent.Size = New System.Drawing.Size(248, 80)
        Me.rgNICommissionAsPercent.TabIndex = 5
        '
        'Label15
        '
        Me.Label15.Location = New System.Drawing.Point(16, 72)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(88, 21)
        Me.Label15.TabIndex = 2
        Me.Label15.Text = "Salesperson:"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNICommissionPercent
        '
        Me.txtNICommissionPercent.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "VJobsNonCoreSalesItems.NICommissionPercent"))
        Me.txtNICommissionPercent.EditValue = ""
        Me.txtNICommissionPercent.Location = New System.Drawing.Point(280, 248)
        Me.txtNICommissionPercent.Name = "txtNICommissionPercent"
        '
        'txtNICommissionPercent.Properties
        '
        Me.txtNICommissionPercent.Properties.Appearance.Options.UseTextOptions = True
        Me.txtNICommissionPercent.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtNICommissionPercent.Properties.DisplayFormat.FormatString = "p"
        Me.txtNICommissionPercent.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtNICommissionPercent.Properties.EditFormat.FormatString = "p"
        Me.txtNICommissionPercent.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtNICommissionPercent.Properties.NullText = "<Incomplete>"
        Me.txtNICommissionPercent.Size = New System.Drawing.Size(75, 20)
        Me.txtNICommissionPercent.TabIndex = 11
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(16, 248)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(88, 21)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Dollar amount:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(208, 248)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(64, 21)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Percentage:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(16, 112)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(312, 16)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "How are you going to pay the commission on this appliance?"
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(16, 224)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(336, 16)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Enter the amount and/or percentage the salesperson was paid"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(376, 328)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(296, 328)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "OK"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton1.Image = CType(resources.GetObject("SimpleButton1.Image"), System.Drawing.Image)
        Me.SimpleButton1.Location = New System.Drawing.Point(8, 328)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.TabIndex = 1
        Me.SimpleButton1.Text = "Help"
        '
        'frmJobOtherTrade2
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(458, 360)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmJobOtherTrade2"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Other Trade"
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.txtNIDesc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNIUserType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNIName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.rdNIOrderedByClient.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rdNIPaidByClient.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage3.ResumeLayout(False)
        CType(Me.txtNIPrice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNICost.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpSalesperson.ResumeLayout(False)
        CType(Me.rgNIPayCommission.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEXIDRep.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvSalesReps, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNICommissionAmount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgNICommissionAsPercent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNICommissionPercent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmJobAppliance_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Me.XtraTabControl1.TabPages.Remove(tpSalesperson)
    End Sub

    Private Sub FillPreliminaryData(ByVal BRID As Integer)
        daSalesReps.SelectCommand.Parameters("@BRID").Value = BRID
        daSalesReps.Fill(dataSet)
    End Sub

    Private Sub FillData()
    End Sub

    Private Sub EnableDisable()
        txtEXIDRep.Enabled = rgNIPayCommission.EditValue
        rgNICommissionAsPercent.Enabled = rgNIPayCommission.EditValue
        txtNICommissionAmount.Enabled = rgNIPayCommission.EditValue And (rgNICommissionAsPercent.EditValue = 0 Or rgNICommissionAsPercent.EditValue = 2)
        txtNICommissionPercent.Enabled = rgNIPayCommission.EditValue And (rgNICommissionAsPercent.EditValue = 1 Or rgNICommissionAsPercent.EditValue = 2)
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        rdNIOrderedByClient_Validated(sender, e)
        rdNIPaidByClient_Validated(sender, e)

        DataRow.EndEdit()
        OK = True
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        WriteMRU(txtNIName, UserAppDataPath)
        WriteMRU(txtNIUserType, UserAppDataPath)

        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            DataRow.EndEdit()
            If dataSet.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If Not response = MsgBoxResult.Yes Then
                e.Cancel = True
            End If
        End If
    End Sub
    Private Sub rgNICommissionAsPercent_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgNICommissionAsPercent.SelectedIndexChanged
        EnableDisable()
    End Sub

    Private Sub rgNIPayCommission_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgNIPayCommission.SelectedIndexChanged
        EnableDisable()
    End Sub

    Private Sub TextBox_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) Handles txtNIPrice.ParseEditValue, txtNICost.ParseEditValue
        Format.Decimal_ParseEditValue(sender, e)
    End Sub

    Private Sub txtPercentage_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) _
    Handles txtNICommissionPercent.ParseEditValue
        If TypeOf e.Value Is String Then
            If Trim(e.Value) = "" Then
                e.Value = DBNull.Value
            Else
                Try
                    e.Value = e.Value / 100
                Catch ex As Exception
                End Try
            End If
        End If
    End Sub

    Private Sub miClearHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miClearHistory.Click
        ClearHistory(sender, e)
    End Sub

    Private Sub rdNIOrderedByClient_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdNIOrderedByClient.Validated
        If DataRow("NIOrderedByClient") Is DBNull.Value Then
            DataRow("NIOrderedBy") = DBNull.Value
        ElseIf DataRow("NIOrderedByClient") Then
            DataRow("NIOrderedBy") = "Customer"
        Else
            DataRow("NIOrderedBy") = "Align Kitchens"
        End If
    End Sub

    Private Sub rdNIPaidByClient_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdNIPaidByClient.Validated
        If DataRow("NIPaidByClient") Is DBNull.Value Then
            DataRow("NIPaidBy") = DBNull.Value
        ElseIf DataRow("NIPaidByClient") Then
            DataRow("NIPaidBy") = "Customer"
        Else
            DataRow("NIPaidBy") = "Align Kitchens"
        End If
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        ShowHelpTopic(Me, "JobFinancialsTerms.html#OtherTrades")
    End Sub
End Class
