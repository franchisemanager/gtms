Imports Power.Forms

Public Class lvOrders2
    Inherits System.Windows.Forms.UserControl
    Implements IListControl, IPrintableControl

    Private HideAccountingInformation As Boolean

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Connection As SqlClient.SqlConnection, ByVal BRID As Integer, ByVal ExplorerForm As frmExplorer, Optional ByVal HideAccountingInformation As Boolean = False)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.Connection = Connection
        Me.BRID = BRID
        hExplorerForm = ExplorerForm
        SetUpFilters()
        DateFilterIsListening = True
        FillDataSet()
        EnableDisable()
        Me.HideAccountingInformation = HideAccountingInformation
        If HideAccountingInformation Then
            Me.GridView1.Columns.Remove(colORInvoicedCostTotal)
        End If
    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents hSqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents ImageListToolBar As System.Windows.Forms.ImageList
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents bStandard As DevExpress.XtraBars.Bar
    Friend WithEvents btnNew As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnEdit As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subReportsOnSelectedItem As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnQuickPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPrintPreview As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnHelp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarAndDockingController1 As DevExpress.XtraBars.BarAndDockingController
    Friend WithEvents btnDelete As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subReportsOnSelectedItem2 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents PopupMenu1 As DevExpress.XtraBars.PopupMenu
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents bAdvancedFilter As DevExpress.XtraBars.Bar
    Friend WithEvents beiFromDate As DevExpress.XtraBars.BarEditItem
    Friend WithEvents beiToDate As DevExpress.XtraBars.BarEditItem
    Friend WithEvents btnResetFilters As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents daOrders_StockedItems As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daMaterials As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommand11 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand12 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daStockedItems As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommand13 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand14 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daOrders_Materials_SubItems As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daOrders As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daOrders_Materials As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents DataSet As WindowsApplication.dsOrders
    Friend WithEvents colOROrderNumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colORDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colORDescription As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colORInvoicedCostTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colORStatus As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dvOrders As System.Data.DataView
    Friend WithEvents colORReceived As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents daExpenses As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlInsertCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents colJBDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents beiFromJobDate As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemDateEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents beiToJobDate As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemDateEdit4 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(lvOrders2))
        Me.hSqlConnection = New System.Data.SqlClient.SqlConnection
        Me.DataSet = New WindowsApplication.dsOrders
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.dvOrders = New System.Data.DataView
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colOROrderNumber = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colORDate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colORDescription = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colORInvoicedCostTotal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colORStatus = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colID = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colORReceived = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colJBDate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.ImageListToolBar = New System.Windows.Forms.ImageList(Me.components)
        Me.BarManager1 = New DevExpress.XtraBars.BarManager
        Me.bStandard = New DevExpress.XtraBars.Bar
        Me.btnNew = New DevExpress.XtraBars.BarButtonItem
        Me.btnEdit = New DevExpress.XtraBars.BarButtonItem
        Me.btnDelete = New DevExpress.XtraBars.BarButtonItem
        Me.subReportsOnSelectedItem = New DevExpress.XtraBars.BarSubItem
        Me.btnQuickPrint = New DevExpress.XtraBars.BarButtonItem
        Me.btnPrintPreview = New DevExpress.XtraBars.BarButtonItem
        Me.btnHelp = New DevExpress.XtraBars.BarButtonItem
        Me.bAdvancedFilter = New DevExpress.XtraBars.Bar
        Me.beiFromDate = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.beiToDate = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.beiFromJobDate = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemDateEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.beiToJobDate = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemDateEdit4 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.btnResetFilters = New DevExpress.XtraBars.BarButtonItem
        Me.BarAndDockingController1 = New DevExpress.XtraBars.BarAndDockingController(Me.components)
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
        Me.btnPrint = New DevExpress.XtraBars.BarButtonItem
        Me.subReportsOnSelectedItem2 = New DevExpress.XtraBars.BarSubItem
        Me.PopupMenu1 = New DevExpress.XtraBars.PopupMenu
        Me.daOrders_StockedItems = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand11 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand10 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand10 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand11 = New System.Data.SqlClient.SqlCommand
        Me.daMaterials = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlCommand11 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand12 = New System.Data.SqlClient.SqlCommand
        Me.daStockedItems = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlCommand13 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand14 = New System.Data.SqlClient.SqlCommand
        Me.daOrders_Materials_SubItems = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand10 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand10 = New System.Data.SqlClient.SqlCommand
        Me.daOrders = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.daOrders_Materials = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand8 = New System.Data.SqlClient.SqlCommand
        Me.daExpenses = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlInsertCommand9 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand9 = New System.Data.SqlClient.SqlCommand
        CType(Me.DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'hSqlConnection
        '
        Me.hSqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;user id=sa;integrated security=SSPI;data sou" & _
        "rce=""SERVER\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'DataSet
        '
        Me.DataSet.DataSetName = "dsOrders"
        Me.DataSet.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.dvOrders
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'GridControl1.EmbeddedNavigator
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.Location = New System.Drawing.Point(0, 77)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.ShowOnlyPredefinedDetails = True
        Me.GridControl1.Size = New System.Drawing.Size(632, 315)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'dvOrders
        '
        Me.dvOrders.RowFilter = "ORType <> 'IO'"
        Me.dvOrders.Table = Me.DataSet.VOrders
        '
        'GridView1
        '
        Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
        Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
        Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colOROrderNumber, Me.colORDate, Me.colORDescription, Me.colORInvoicedCostTotal, Me.colORStatus, Me.colID, Me.colORReceived, Me.colJBDate})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsNavigation.AutoFocusNewRow = True
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.OptionsView.ShowHorzLines = False
        Me.GridView1.OptionsView.ShowIndicator = False
        Me.GridView1.OptionsView.ShowVertLines = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colORDate, DevExpress.Data.ColumnSortOrder.Descending)})
        '
        'colOROrderNumber
        '
        Me.colOROrderNumber.Caption = "Order #"
        Me.colOROrderNumber.FieldName = "OROrderNumber"
        Me.colOROrderNumber.Name = "colOROrderNumber"
        Me.colOROrderNumber.Visible = True
        Me.colOROrderNumber.VisibleIndex = 1
        Me.colOROrderNumber.Width = 106
        '
        'colORDate
        '
        Me.colORDate.Caption = "Date Ordered"
        Me.colORDate.FieldName = "ORDate"
        Me.colORDate.Name = "colORDate"
        Me.colORDate.Visible = True
        Me.colORDate.VisibleIndex = 2
        Me.colORDate.Width = 131
        '
        'colORDescription
        '
        Me.colORDescription.Caption = "Description"
        Me.colORDescription.FieldName = "ORDescription"
        Me.colORDescription.Name = "colORDescription"
        Me.colORDescription.Visible = True
        Me.colORDescription.VisibleIndex = 4
        Me.colORDescription.Width = 247
        '
        'colORInvoicedCostTotal
        '
        Me.colORInvoicedCostTotal.Caption = "Invoiced Cost"
        Me.colORInvoicedCostTotal.DisplayFormat.FormatString = "c"
        Me.colORInvoicedCostTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colORInvoicedCostTotal.FieldName = "ORInvoicedCostTotal"
        Me.colORInvoicedCostTotal.Name = "colORInvoicedCostTotal"
        Me.colORInvoicedCostTotal.Visible = True
        Me.colORInvoicedCostTotal.VisibleIndex = 5
        Me.colORInvoicedCostTotal.Width = 138
        '
        'colORStatus
        '
        Me.colORStatus.Caption = "Status"
        Me.colORStatus.FieldName = "ORStatus"
        Me.colORStatus.Name = "colORStatus"
        Me.colORStatus.Visible = True
        Me.colORStatus.VisibleIndex = 6
        Me.colORStatus.Width = 148
        '
        'colID
        '
        Me.colID.Caption = "ID"
        Me.colID.FieldName = "ID"
        Me.colID.Name = "colID"
        Me.colID.Visible = True
        Me.colID.VisibleIndex = 0
        Me.colID.Width = 97
        '
        'colORReceived
        '
        Me.colORReceived.Caption = "Order Received"
        Me.colORReceived.FieldName = "ORReceived"
        Me.colORReceived.Name = "colORReceived"
        Me.colORReceived.Visible = True
        Me.colORReceived.VisibleIndex = 7
        Me.colORReceived.Width = 105
        '
        'colJBDate
        '
        Me.colJBDate.Caption = "Job Date"
        Me.colJBDate.FieldName = "JBDate"
        Me.colJBDate.Name = "colJBDate"
        Me.colJBDate.Visible = True
        Me.colJBDate.VisibleIndex = 3
        Me.colJBDate.Width = 133
        '
        'ImageListToolBar
        '
        Me.ImageListToolBar.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImageListToolBar.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageListToolBar.ImageStream = CType(resources.GetObject("ImageListToolBar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListToolBar.TransparentColor = System.Drawing.Color.Transparent
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.bStandard, Me.bAdvancedFilter})
        Me.BarManager1.Categories.AddRange(New DevExpress.XtraBars.BarManagerCategory() {New DevExpress.XtraBars.BarManagerCategory("Built-in Menus", New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd"), False), New DevExpress.XtraBars.BarManagerCategory("File", New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")), New DevExpress.XtraBars.BarManagerCategory("Reports", New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")), New DevExpress.XtraBars.BarManagerCategory("Forms", New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")), New DevExpress.XtraBars.BarManagerCategory("Help", New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")), New DevExpress.XtraBars.BarManagerCategory("Search", New System.Guid("d27cdec9-bd13-47ce-a40f-a58b83ae2195"))})
        Me.BarManager1.Controller = Me.BarAndDockingController1
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Images = Me.ImageListToolBar
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnNew, Me.btnEdit, Me.btnDelete, Me.btnHelp, Me.btnPrintPreview, Me.btnQuickPrint, Me.btnPrint, Me.subReportsOnSelectedItem, Me.subReportsOnSelectedItem2, Me.beiFromDate, Me.beiToDate, Me.beiFromJobDate, Me.beiToJobDate, Me.btnResetFilters})
        Me.BarManager1.MainMenu = Me.bStandard
        Me.BarManager1.MaxItemId = 99
        Me.BarManager1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1, Me.RepositoryItemDateEdit2, Me.RepositoryItemDateEdit3, Me.RepositoryItemDateEdit4})
        '
        'bStandard
        '
        Me.bStandard.BarName = "Standard Toolbar"
        Me.bStandard.DockCol = 0
        Me.bStandard.DockRow = 0
        Me.bStandard.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bStandard.FloatLocation = New System.Drawing.Point(44, 188)
        Me.bStandard.FloatSize = New System.Drawing.Size(659, 24)
        Me.bStandard.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnNew, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnEdit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnDelete, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(Me.subReportsOnSelectedItem, True), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnQuickPrint, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnPrintPreview, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnHelp, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.bStandard.OptionsBar.AllowQuickCustomization = False
        Me.bStandard.OptionsBar.DisableClose = True
        Me.bStandard.OptionsBar.DrawDragBorder = False
        Me.bStandard.OptionsBar.MultiLine = True
        Me.bStandard.OptionsBar.UseWholeRow = True
        Me.bStandard.Text = "Standard Toolbar"
        '
        'btnNew
        '
        Me.btnNew.Caption = "&New..."
        Me.btnNew.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnNew.Id = 19
        Me.btnNew.ImageIndex = 0
        Me.btnNew.ImageIndexDisabled = 0
        Me.btnNew.Name = "btnNew"
        '
        'btnEdit
        '
        Me.btnEdit.Caption = "&Edit..."
        Me.btnEdit.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnEdit.Id = 20
        Me.btnEdit.ImageIndex = 1
        Me.btnEdit.ImageIndexDisabled = 1
        Me.btnEdit.Name = "btnEdit"
        '
        'btnDelete
        '
        Me.btnDelete.Caption = "&Delete"
        Me.btnDelete.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnDelete.Id = 23
        Me.btnDelete.ImageIndex = 13
        Me.btnDelete.ImageIndexDisabled = 13
        Me.btnDelete.Name = "btnDelete"
        '
        'subReportsOnSelectedItem
        '
        Me.subReportsOnSelectedItem.Caption = "Reports on Selected Item"
        Me.subReportsOnSelectedItem.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.subReportsOnSelectedItem.Id = 78
        Me.subReportsOnSelectedItem.Name = "subReportsOnSelectedItem"
        Me.subReportsOnSelectedItem.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInCustomizing
        '
        'btnQuickPrint
        '
        Me.btnQuickPrint.Caption = "Print"
        Me.btnQuickPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnQuickPrint.Id = 46
        Me.btnQuickPrint.ImageIndex = 11
        Me.btnQuickPrint.ImageIndexDisabled = 11
        Me.btnQuickPrint.Name = "btnQuickPrint"
        '
        'btnPrintPreview
        '
        Me.btnPrintPreview.Caption = "Print Pre&view"
        Me.btnPrintPreview.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrintPreview.Id = 44
        Me.btnPrintPreview.ImageIndex = 10
        Me.btnPrintPreview.ImageIndexDisabled = 10
        Me.btnPrintPreview.Name = "btnPrintPreview"
        '
        'btnHelp
        '
        Me.btnHelp.Caption = "&Help"
        Me.btnHelp.CategoryGuid = New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")
        Me.btnHelp.Id = 68
        Me.btnHelp.ImageIndex = 4
        Me.btnHelp.ImageIndexDisabled = 4
        Me.btnHelp.Name = "btnHelp"
        '
        'bAdvancedFilter
        '
        Me.bAdvancedFilter.BarName = "Search Bar"
        Me.bAdvancedFilter.DockCol = 0
        Me.bAdvancedFilter.DockRow = 1
        Me.bAdvancedFilter.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bAdvancedFilter.FloatLocation = New System.Drawing.Point(40, 189)
        Me.bAdvancedFilter.FloatSize = New System.Drawing.Size(570, 24)
        Me.bAdvancedFilter.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.beiFromDate, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.beiToDate, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.beiFromJobDate, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.beiToJobDate, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnResetFilters, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.bAdvancedFilter.OptionsBar.AllowQuickCustomization = False
        Me.bAdvancedFilter.OptionsBar.DisableClose = True
        Me.bAdvancedFilter.OptionsBar.MultiLine = True
        Me.bAdvancedFilter.Text = "Search Bar"
        '
        'beiFromDate
        '
        Me.beiFromDate.Caption = "Search for Ordered Dates from:"
        Me.beiFromDate.CategoryGuid = New System.Guid("d27cdec9-bd13-47ce-a40f-a58b83ae2195")
        Me.beiFromDate.Edit = Me.RepositoryItemDateEdit1
        Me.beiFromDate.Id = 93
        Me.beiFromDate.Name = "beiFromDate"
        Me.beiFromDate.Width = 150
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'beiToDate
        '
        Me.beiToDate.Caption = "to:"
        Me.beiToDate.CategoryGuid = New System.Guid("d27cdec9-bd13-47ce-a40f-a58b83ae2195")
        Me.beiToDate.Edit = Me.RepositoryItemDateEdit2
        Me.beiToDate.Id = 94
        Me.beiToDate.Name = "beiToDate"
        Me.beiToDate.Width = 150
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'beiFromJobDate
        '
        Me.beiFromJobDate.Caption = "Search for Job Dates from:"
        Me.beiFromJobDate.CategoryGuid = New System.Guid("d27cdec9-bd13-47ce-a40f-a58b83ae2195")
        Me.beiFromJobDate.Edit = Me.RepositoryItemDateEdit3
        Me.beiFromJobDate.Id = 97
        Me.beiFromJobDate.Name = "beiFromJobDate"
        Me.beiFromJobDate.Width = 150
        '
        'RepositoryItemDateEdit3
        '
        Me.RepositoryItemDateEdit3.AutoHeight = False
        Me.RepositoryItemDateEdit3.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit3.Name = "RepositoryItemDateEdit3"
        '
        'beiToJobDate
        '
        Me.beiToJobDate.Caption = "to:"
        Me.beiToJobDate.CategoryGuid = New System.Guid("d27cdec9-bd13-47ce-a40f-a58b83ae2195")
        Me.beiToJobDate.Edit = Me.RepositoryItemDateEdit4
        Me.beiToJobDate.Id = 98
        Me.beiToJobDate.Name = "beiToJobDate"
        Me.beiToJobDate.Width = 150
        '
        'RepositoryItemDateEdit4
        '
        Me.RepositoryItemDateEdit4.AutoHeight = False
        Me.RepositoryItemDateEdit4.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit4.Name = "RepositoryItemDateEdit4"
        '
        'btnResetFilters
        '
        Me.btnResetFilters.Caption = "&Reset Filters"
        Me.btnResetFilters.CategoryGuid = New System.Guid("d27cdec9-bd13-47ce-a40f-a58b83ae2195")
        Me.btnResetFilters.Id = 95
        Me.btnResetFilters.ImageIndex = 14
        Me.btnResetFilters.ImageIndexDisabled = 14
        Me.btnResetFilters.Name = "btnResetFilters"
        '
        'BarAndDockingController1
        '
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseForeColor = True
        Me.BarAndDockingController1.PaintStyleName = "OfficeXP"
        '
        'btnPrint
        '
        Me.btnPrint.Caption = "&Print..."
        Me.btnPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrint.Id = 43
        Me.btnPrint.ImageIndex = 11
        Me.btnPrint.ImageIndexDisabled = 11
        Me.btnPrint.Name = "btnPrint"
        '
        'subReportsOnSelectedItem2
        '
        Me.subReportsOnSelectedItem2.Caption = "&Reports on Selected Item"
        Me.subReportsOnSelectedItem2.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subReportsOnSelectedItem2.Id = 87
        Me.subReportsOnSelectedItem2.Name = "subReportsOnSelectedItem2"
        '
        'PopupMenu1
        '
        Me.PopupMenu1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.subReportsOnSelectedItem, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnEdit, True)})
        Me.PopupMenu1.Manager = Me.BarManager1
        Me.PopupMenu1.Name = "PopupMenu1"
        '
        'daOrders_StockedItems
        '
        Me.daOrders_StockedItems.ContinueUpdateOnError = True
        Me.daOrders_StockedItems.DeleteCommand = Me.SqlDeleteCommand11
        Me.daOrders_StockedItems.InsertCommand = Me.SqlInsertCommand10
        Me.daOrders_StockedItems.SelectCommand = Me.SqlSelectCommand10
        Me.daOrders_StockedItems.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VOrders_StockedItems", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("OSID", "OSID"), New System.Data.Common.DataColumnMapping("ORID", "ORID"), New System.Data.Common.DataColumnMapping("SIID", "SIID"), New System.Data.Common.DataColumnMapping("OSInventoryAmount", "OSInventoryAmount"), New System.Data.Common.DataColumnMapping("OSInventoryDate", "OSInventoryDate")})})
        Me.daOrders_StockedItems.UpdateCommand = Me.SqlUpdateCommand11
        '
        'SqlDeleteCommand11
        '
        Me.SqlDeleteCommand11.CommandText = "DELETE FROM Orders_StockedItems WHERE (BRID = @Original_BRID) AND (OSID = @Origin" & _
        "al_OSID) AND (ORID = @Original_ORID OR @Original_ORID IS NULL AND ORID IS NULL) " & _
        "AND (OSInventoryAmount = @Original_OSInventoryAmount OR @Original_OSInventoryAmo" & _
        "unt IS NULL AND OSInventoryAmount IS NULL) AND (OSInventoryDate = @Original_OSIn" & _
        "ventoryDate OR @Original_OSInventoryDate IS NULL AND OSInventoryDate IS NULL) AN" & _
        "D (SIID = @Original_SIID OR @Original_SIID IS NULL AND SIID IS NULL)"
        Me.SqlDeleteCommand11.Connection = Me.hSqlConnection
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OSID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OSInventoryAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OSInventoryDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OSInventoryDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SIID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand10
        '
        Me.SqlInsertCommand10.CommandText = "INSERT INTO Orders_StockedItems (BRID, ORID, SIID, OSInventoryAmount, OSInventory" & _
        "Date) VALUES (@BRID, @ORID, @SIID, @OSInventoryAmount, @OSInventoryDate); SELECT" & _
        " BRID, OSID, ORID, SIID, OSInventoryAmount, OSInventoryDate, SIInventoryUOMShort" & _
        "Name FROM VOrders_StockedItems WHERE (BRID = @BRID) AND (OSID = @@IDENTITY)"
        Me.SqlInsertCommand10.Connection = Me.hSqlConnection
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORID", System.Data.SqlDbType.BigInt, 8, "ORID"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIID", System.Data.SqlDbType.Int, 4, "SIID"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OSInventoryAmount", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OSInventoryDate", System.Data.SqlDbType.DateTime, 8, "OSInventoryDate"))
        '
        'SqlSelectCommand10
        '
        Me.SqlSelectCommand10.CommandText = "SELECT BRID, OSID, ORID, SIID, OSInventoryAmount, OSInventoryDate, SIInventoryUOM" & _
        "ShortName FROM VOrders_StockedItems WHERE (BRID = @BRID) AND (ORDate >= @FROM_DA" & _
        "TE OR @FROM_DATE IS NULL) AND (ORDate <= @TO_DATE OR @TO_DATE IS NULL) AND (ORTy" & _
        "pe <> 'IO') AND (JBDate >= @FROM_JOB_DATE OR @FROM_JOB_DATE IS NULL) AND (JBDate" & _
        " <= @TO_JOB_DATE OR @TO_JOB_DATE IS NULL)"
        Me.SqlSelectCommand10.Connection = Me.hSqlConnection
        Me.SqlSelectCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FROM_DATE", System.Data.SqlDbType.DateTime, 8, "ORDate"))
        Me.SqlSelectCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TO_DATE", System.Data.SqlDbType.DateTime, 8, "ORDate"))
        Me.SqlSelectCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FROM_JOB_DATE", System.Data.SqlDbType.DateTime, 8, "JBDate"))
        Me.SqlSelectCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TO_JOB_DATE", System.Data.SqlDbType.DateTime, 8, "JBDate"))
        '
        'SqlUpdateCommand11
        '
        Me.SqlUpdateCommand11.CommandText = "UPDATE Orders_StockedItems SET BRID = @BRID, ORID = @ORID, SIID = @SIID, OSInvent" & _
        "oryAmount = @OSInventoryAmount, OSInventoryDate = @OSInventoryDate WHERE (BRID =" & _
        " @Original_BRID) AND (OSID = @Original_OSID) AND (ORID = @Original_ORID OR @Orig" & _
        "inal_ORID IS NULL AND ORID IS NULL) AND (OSInventoryAmount = @Original_OSInvento" & _
        "ryAmount OR @Original_OSInventoryAmount IS NULL AND OSInventoryAmount IS NULL) A" & _
        "ND (OSInventoryDate = @Original_OSInventoryDate OR @Original_OSInventoryDate IS " & _
        "NULL AND OSInventoryDate IS NULL) AND (SIID = @Original_SIID OR @Original_SIID I" & _
        "S NULL AND SIID IS NULL); SELECT BRID, OSID, ORID, SIID, OSInventoryAmount, OSIn" & _
        "ventoryDate, SIInventoryUOMShortName FROM VOrders_StockedItems WHERE (BRID = @BR" & _
        "ID) AND (OSID = @OSID)"
        Me.SqlUpdateCommand11.Connection = Me.hSqlConnection
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORID", System.Data.SqlDbType.BigInt, 8, "ORID"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIID", System.Data.SqlDbType.Int, 4, "SIID"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OSInventoryAmount", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OSInventoryDate", System.Data.SqlDbType.DateTime, 8, "OSInventoryDate"))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OSID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OSInventoryAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OSInventoryDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OSInventoryDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OSID", System.Data.SqlDbType.BigInt, 8, "OSID"))
        '
        'daMaterials
        '
        Me.daMaterials.InsertCommand = Me.SqlCommand11
        Me.daMaterials.SelectCommand = Me.SqlCommand12
        Me.daMaterials.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VMaterials", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("MTID", "MTID"), New System.Data.Common.DataColumnMapping("MTName", "MTName"), New System.Data.Common.DataColumnMapping("MTInventoryUOM", "MTInventoryUOM"), New System.Data.Common.DataColumnMapping("MTPrimaryUOM", "MTPrimaryUOM"), New System.Data.Common.DataColumnMapping("MTDiscontinued", "MTDiscontinued"), New System.Data.Common.DataColumnMapping("MTStocked", "MTStocked"), New System.Data.Common.DataColumnMapping("MGID", "MGID"), New System.Data.Common.DataColumnMapping("MTCurrentPurchaseCost", "MTCurrentPurchaseCost"), New System.Data.Common.DataColumnMapping("MTCurrentWasteFactorAllowance", "MTCurrentWasteFactorAllowance"), New System.Data.Common.DataColumnMapping("MGControlledAtHeadOffice", "MGControlledAtHeadOffice"), New System.Data.Common.DataColumnMapping("MTCurrentInventory", "MTCurrentInventory"), New System.Data.Common.DataColumnMapping("MTCurrentWasteFactor", "MTCurrentWasteFactor"), New System.Data.Common.DataColumnMapping("MGAssignToPortion", "MGAssignToPortion"), New System.Data.Common.DataColumnMapping("MMTID", "MMTID"), New System.Data.Common.DataColumnMapping("MTCurrentWastePercentage", "MTCurrentWastePercentage"), New System.Data.Common.DataColumnMapping("MTPrimaryUOMShortName", "MTPrimaryUOMShortName")})})
        '
        'SqlCommand11
        '
        Me.SqlCommand11.CommandText = "INSERT INTO VMaterials(BRID, MTName, MTInventoryUOM, MTPrimaryUOM, MTDiscontinued" & _
        ", MTStocked, MGID, MTCurrentPurchaseCost, MTCurrentWasteFactorAllowance, MGContr" & _
        "olledAtHeadOffice, MTCurrentInventory, MTCurrentWasteFactor, MGAssignToPortion, " & _
        "MMTID, MTCurrentWastePercentage, MTPrimaryUOMShortName) VALUES (@BRID, @MTName, " & _
        "@MTInventoryUOM, @MTPrimaryUOM, @MTDiscontinued, @MTStocked, @MGID, @MTCurrentPu" & _
        "rchaseCost, @MTCurrentWasteFactorAllowance, @MGControlledAtHeadOffice, @MTCurren" & _
        "tInventory, @MTCurrentWasteFactor, @MGAssignToPortion, @MMTID, @MTCurrentWastePe" & _
        "rcentage, @MTPrimaryUOMShortName); SELECT BRID, MTID, MTName, MTInventoryUOM, MT" & _
        "PrimaryUOM, MTDiscontinued, MTStocked, MGID, MTCurrentPurchaseCost, MTCurrentWas" & _
        "teFactorAllowance, MGControlledAtHeadOffice, MTCurrentInventory, MTCurrentWasteF" & _
        "actor, MGAssignToPortion, MMTID, MTCurrentWastePercentage, MTPrimaryUOMShortName" & _
        " FROM VMaterials"
        Me.SqlCommand11.Connection = Me.hSqlConnection
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTName", System.Data.SqlDbType.VarChar, 50, "MTName"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTInventoryUOM", System.Data.SqlDbType.VarChar, 2, "MTInventoryUOM"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTPrimaryUOM", System.Data.SqlDbType.VarChar, 2, "MTPrimaryUOM"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTDiscontinued", System.Data.SqlDbType.Bit, 1, "MTDiscontinued"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTStocked", System.Data.SqlDbType.Bit, 1, "MTStocked"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTCurrentPurchaseCost", System.Data.SqlDbType.Money, 8, "MTCurrentPurchaseCost"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTCurrentWasteFactorAllowance", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "MTCurrentWasteFactorAllowance", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGControlledAtHeadOffice", System.Data.SqlDbType.Bit, 1, "MGControlledAtHeadOffice"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTCurrentInventory", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "MTCurrentInventory", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTCurrentWasteFactor", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "MTCurrentWasteFactor", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGAssignToPortion", System.Data.SqlDbType.VarChar, 2, "MGAssignToPortion"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTID", System.Data.SqlDbType.Int, 4, "MMTID"))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTCurrentWastePercentage", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(4, Byte), "MTCurrentWastePercentage", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlCommand11.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTPrimaryUOMShortName", System.Data.SqlDbType.VarChar, 50, "MTPrimaryUOMShortName"))
        '
        'SqlCommand12
        '
        Me.SqlCommand12.CommandText = "SELECT BRID, MTID, MTName, MTInventoryUOM, MTPrimaryUOM, MTDiscontinued, MTStocke" & _
        "d, MGID, MTCurrentPurchaseCost, MTCurrentWasteFactorAllowance, MGControlledAtHea" & _
        "dOffice, MTCurrentInventory, MTCurrentWasteFactor, MGAssignToPortion, MMTID, MTC" & _
        "urrentWastePercentage, MTPrimaryUOMShortName FROM VMaterials WHERE (BRID = @BRID" & _
        ") AND (MTDiscontinued = 0)"
        Me.SqlCommand12.Connection = Me.hSqlConnection
        Me.SqlCommand12.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'daStockedItems
        '
        Me.daStockedItems.InsertCommand = Me.SqlCommand13
        Me.daStockedItems.SelectCommand = Me.SqlCommand14
        Me.daStockedItems.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VStockedItems", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("SIID", "SIID"), New System.Data.Common.DataColumnMapping("MTID", "MTID"), New System.Data.Common.DataColumnMapping("SIConversionToPrimary", "SIConversionToPrimary"), New System.Data.Common.DataColumnMapping("MTPrimaryUOM", "MTPrimaryUOM"), New System.Data.Common.DataColumnMapping("SIName", "SIName"), New System.Data.Common.DataColumnMapping("SIInventoryUOM", "SIInventoryUOM"), New System.Data.Common.DataColumnMapping("SICurrentInventory", "SICurrentInventory"), New System.Data.Common.DataColumnMapping("SICurrentValue", "SICurrentValue"), New System.Data.Common.DataColumnMapping("MTPrimaryUOMShortName", "MTPrimaryUOMShortName"), New System.Data.Common.DataColumnMapping("SICurrentPurchaseCost", "SICurrentPurchaseCost"), New System.Data.Common.DataColumnMapping("SICurrentInventoryInPrimaryUOM", "SICurrentInventoryInPrimaryUOM"), New System.Data.Common.DataColumnMapping("SIInventoryUOMShortName", "SIInventoryUOMShortName")})})
        '
        'SqlCommand13
        '
        Me.SqlCommand13.CommandText = "INSERT INTO VStockedItems(BRID, MTID, SIConversionToPrimary, MTPrimaryUOM, SIName" & _
        ", SIInventoryUOM, SICurrentInventory, SICurrentValue, MTPrimaryUOMShortName, SIC" & _
        "urrentPurchaseCost, SICurrentInventoryInPrimaryUOM, SIInventoryUOMShortName) VAL" & _
        "UES (@BRID, @MTID, @SIConversionToPrimary, @MTPrimaryUOM, @SIName, @SIInventoryU" & _
        "OM, @SICurrentInventory, @SICurrentValue, @MTPrimaryUOMShortName, @SICurrentPurc" & _
        "haseCost, @SICurrentInventoryInPrimaryUOM, @SIInventoryUOMShortName); SELECT BRI" & _
        "D, SIID, MTID, SIConversionToPrimary, MTPrimaryUOM, SIName, SIInventoryUOM, SICu" & _
        "rrentInventory, SICurrentValue, MTPrimaryUOMShortName, SICurrentPurchaseCost, SI" & _
        "CurrentInventoryInPrimaryUOM, SIInventoryUOMShortName FROM VStockedItems"
        Me.SqlCommand13.Connection = Me.hSqlConnection
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIConversionToPrimary", System.Data.SqlDbType.Real, 4, "SIConversionToPrimary"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTPrimaryUOM", System.Data.SqlDbType.VarChar, 2, "MTPrimaryUOM"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIName", System.Data.SqlDbType.VarChar, 50, "SIName"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIInventoryUOM", System.Data.SqlDbType.VarChar, 2, "SIInventoryUOM"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SICurrentInventory", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "SICurrentInventory", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SICurrentValue", System.Data.SqlDbType.Money, 8, "SICurrentValue"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTPrimaryUOMShortName", System.Data.SqlDbType.VarChar, 50, "MTPrimaryUOMShortName"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SICurrentPurchaseCost", System.Data.SqlDbType.Money, 8, "SICurrentPurchaseCost"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SICurrentInventoryInPrimaryUOM", System.Data.SqlDbType.Real, 4, "SICurrentInventoryInPrimaryUOM"))
        Me.SqlCommand13.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIInventoryUOMShortName", System.Data.SqlDbType.VarChar, 50, "SIInventoryUOMShortName"))
        '
        'SqlCommand14
        '
        Me.SqlCommand14.CommandText = "SELECT BRID, SIID, MTID, SIConversionToPrimary, MTPrimaryUOM, SIName, SIInventory" & _
        "UOM, SICurrentInventory, SICurrentValue, MTPrimaryUOMShortName, SICurrentPurchas" & _
        "eCost, SICurrentInventoryInPrimaryUOM, SIInventoryUOMShortName FROM VStockedItem" & _
        "s WHERE (BRID = @BRID)"
        Me.SqlCommand14.Connection = Me.hSqlConnection
        Me.SqlCommand14.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'daOrders_Materials_SubItems
        '
        Me.daOrders_Materials_SubItems.ContinueUpdateOnError = True
        Me.daOrders_Materials_SubItems.DeleteCommand = Me.SqlDeleteCommand10
        Me.daOrders_Materials_SubItems.InsertCommand = Me.SqlInsertCommand8
        Me.daOrders_Materials_SubItems.SelectCommand = Me.SqlSelectCommand8
        Me.daOrders_Materials_SubItems.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Orders_Materials_SubItems", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("OMSID", "OMSID"), New System.Data.Common.DataColumnMapping("OMID", "OMID"), New System.Data.Common.DataColumnMapping("OMSBrand", "OMSBrand"), New System.Data.Common.DataColumnMapping("OMSModelNumber", "OMSModelNumber"), New System.Data.Common.DataColumnMapping("OMSDescription", "OMSDescription"), New System.Data.Common.DataColumnMapping("OMSQty", "OMSQty"), New System.Data.Common.DataColumnMapping("OMSQuotedCost", "OMSQuotedCost")})})
        Me.daOrders_Materials_SubItems.UpdateCommand = Me.SqlUpdateCommand10
        '
        'SqlDeleteCommand10
        '
        Me.SqlDeleteCommand10.CommandText = "DELETE FROM Orders_Materials_SubItems WHERE (BRID = @Original_BRID) AND (OMSID = " & _
        "@Original_OMSID) AND (OMID = @Original_OMID) AND (OMSBrand = @Original_OMSBrand " & _
        "OR @Original_OMSBrand IS NULL AND OMSBrand IS NULL) AND (OMSDescription = @Origi" & _
        "nal_OMSDescription OR @Original_OMSDescription IS NULL AND OMSDescription IS NUL" & _
        "L) AND (OMSModelNumber = @Original_OMSModelNumber OR @Original_OMSModelNumber IS" & _
        " NULL AND OMSModelNumber IS NULL) AND (OMSQty = @Original_OMSQty OR @Original_OM" & _
        "SQty IS NULL AND OMSQty IS NULL) AND (OMSQuotedCost = @Original_OMSQuotedCost OR" & _
        " @Original_OMSQuotedCost IS NULL AND OMSQuotedCost IS NULL)"
        Me.SqlDeleteCommand10.Connection = Me.hSqlConnection
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSBrand", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSBrand", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSDescription", System.Data.SqlDbType.VarChar, 500, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSModelNumber", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSModelNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSQty", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSQty", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSQuotedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSQuotedCost", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand8
        '
        Me.SqlInsertCommand8.CommandText = "INSERT INTO Orders_Materials_SubItems(BRID, OMID, OMSBrand, OMSModelNumber, OMSDe" & _
        "scription, OMSQty, OMSQuotedCost) VALUES (@BRID, @OMID, @OMSBrand, @OMSModelNumb" & _
        "er, @OMSDescription, @OMSQty, @OMSQuotedCost); SELECT BRID, OMSID, OMID, OMSBran" & _
        "d, OMSModelNumber, OMSDescription, OMSQty, OMSQuotedCost FROM Orders_Materials_S" & _
        "ubItems WHERE (BRID = @BRID) AND (OMSID = @@IDENTITY)"
        Me.SqlInsertCommand8.Connection = Me.hSqlConnection
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMID", System.Data.SqlDbType.BigInt, 8, "OMID"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSBrand", System.Data.SqlDbType.VarChar, 50, "OMSBrand"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSModelNumber", System.Data.SqlDbType.VarChar, 50, "OMSModelNumber"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSDescription", System.Data.SqlDbType.VarChar, 500, "OMSDescription"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSQty", System.Data.SqlDbType.Int, 4, "OMSQty"))
        Me.SqlInsertCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSQuotedCost", System.Data.SqlDbType.Money, 8, "OMSQuotedCost"))
        '
        'SqlSelectCommand8
        '
        Me.SqlSelectCommand8.CommandText = "SELECT BRID, OMSID, OMID, OMSBrand, OMSModelNumber, OMSDescription, OMSQty, OMSQu" & _
        "otedCost FROM VOrders_Materials_SubItems WHERE (BRID = @BRID) AND (ORDate >= @FR" & _
        "OM_DATE OR @FROM_DATE IS NULL) AND (ORDate <= @TO_DATE OR @TO_DATE IS NULL) AND " & _
        "(ORType <> 'IO') AND (JBDate >= @FROM_JOB_DATE OR @FROM_JOB_DATE IS NULL) AND (J" & _
        "BDate <= @TO_JOB_DATE OR @TO_JOB_DATE IS NULL)"
        Me.SqlSelectCommand8.Connection = Me.hSqlConnection
        Me.SqlSelectCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FROM_DATE", System.Data.SqlDbType.DateTime, 8, "ORDate"))
        Me.SqlSelectCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TO_DATE", System.Data.SqlDbType.DateTime, 8, "ORDate"))
        Me.SqlSelectCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FROM_JOB_DATE", System.Data.SqlDbType.DateTime, 8, "JBDate"))
        Me.SqlSelectCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TO_JOB_DATE", System.Data.SqlDbType.DateTime, 8, "JBDate"))
        '
        'SqlUpdateCommand10
        '
        Me.SqlUpdateCommand10.CommandText = "UPDATE Orders_Materials_SubItems SET BRID = @BRID, OMID = @OMID, OMSBrand = @OMSB" & _
        "rand, OMSModelNumber = @OMSModelNumber, OMSDescription = @OMSDescription, OMSQty" & _
        " = @OMSQty, OMSQuotedCost = @OMSQuotedCost WHERE (BRID = @Original_BRID) AND (OM" & _
        "SID = @Original_OMSID) AND (OMID = @Original_OMID) AND (OMSBrand = @Original_OMS" & _
        "Brand OR @Original_OMSBrand IS NULL AND OMSBrand IS NULL) AND (OMSDescription = " & _
        "@Original_OMSDescription OR @Original_OMSDescription IS NULL AND OMSDescription " & _
        "IS NULL) AND (OMSModelNumber = @Original_OMSModelNumber OR @Original_OMSModelNum" & _
        "ber IS NULL AND OMSModelNumber IS NULL) AND (OMSQty = @Original_OMSQty OR @Origi" & _
        "nal_OMSQty IS NULL AND OMSQty IS NULL) AND (OMSQuotedCost = @Original_OMSQuotedC" & _
        "ost OR @Original_OMSQuotedCost IS NULL AND OMSQuotedCost IS NULL); SELECT BRID, " & _
        "OMSID, OMID, OMSBrand, OMSModelNumber, OMSDescription, OMSQty, OMSQuotedCost FRO" & _
        "M Orders_Materials_SubItems WHERE (BRID = @BRID) AND (OMSID = @OMSID)"
        Me.SqlUpdateCommand10.Connection = Me.hSqlConnection
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMID", System.Data.SqlDbType.BigInt, 8, "OMID"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSBrand", System.Data.SqlDbType.VarChar, 50, "OMSBrand"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSModelNumber", System.Data.SqlDbType.VarChar, 50, "OMSModelNumber"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSDescription", System.Data.SqlDbType.VarChar, 500, "OMSDescription"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSQty", System.Data.SqlDbType.Int, 4, "OMSQty"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSQuotedCost", System.Data.SqlDbType.Money, 8, "OMSQuotedCost"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSBrand", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSBrand", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSDescription", System.Data.SqlDbType.VarChar, 500, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSModelNumber", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSModelNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSQty", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSQty", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMSQuotedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMSQuotedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMSID", System.Data.SqlDbType.BigInt, 8, "OMSID"))
        '
        'daOrders
        '
        Me.daOrders.ContinueUpdateOnError = True
        Me.daOrders.DeleteCommand = Me.SqlDeleteCommand3
        Me.daOrders.InsertCommand = Me.SqlInsertCommand3
        Me.daOrders.SelectCommand = Me.SqlSelectCommand3
        Me.daOrders.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VOrders", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("ORID", "ORID"), New System.Data.Common.DataColumnMapping("JBID", "JBID"), New System.Data.Common.DataColumnMapping("ORType", "ORType"), New System.Data.Common.DataColumnMapping("OROrderNumber", "OROrderNumber"), New System.Data.Common.DataColumnMapping("ORDate", "ORDate"), New System.Data.Common.DataColumnMapping("CTIDSupplier", "CTIDSupplier"), New System.Data.Common.DataColumnMapping("ORShipToJobAddress", "ORShipToJobAddress"), New System.Data.Common.DataColumnMapping("CTIDShipTo", "CTIDShipTo"), New System.Data.Common.DataColumnMapping("ORSupplierInvoiceNumber", "ORSupplierInvoiceNumber"), New System.Data.Common.DataColumnMapping("EXIDOrderedBy", "EXIDOrderedBy"), New System.Data.Common.DataColumnMapping("ORQuotedCost", "ORQuotedCost"), New System.Data.Common.DataColumnMapping("ORInvoicedCost", "ORInvoicedCost"), New System.Data.Common.DataColumnMapping("ORDescription", "ORDescription"), New System.Data.Common.DataColumnMapping("ORDeliveryDate", "ORDeliveryDate"), New System.Data.Common.DataColumnMapping("ORNotes", "ORNotes"), New System.Data.Common.DataColumnMapping("ORReceived", "ORReceived")})})
        Me.daOrders.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM Orders WHERE (BRID = @Original_BRID) AND (ORID = @Original_ORID) AND " & _
        "(CTIDShipTo = @Original_CTIDShipTo OR @Original_CTIDShipTo IS NULL AND CTIDShipT" & _
        "o IS NULL) AND (CTIDSupplier = @Original_CTIDSupplier OR @Original_CTIDSupplier " & _
        "IS NULL AND CTIDSupplier IS NULL) AND (EXIDOrderedBy = @Original_EXIDOrderedBy O" & _
        "R @Original_EXIDOrderedBy IS NULL AND EXIDOrderedBy IS NULL) AND (JBID = @Origin" & _
        "al_JBID OR @Original_JBID IS NULL AND JBID IS NULL) AND (ORDate = @Original_ORDa" & _
        "te OR @Original_ORDate IS NULL AND ORDate IS NULL) AND (ORDeliveryDate = @Origin" & _
        "al_ORDeliveryDate OR @Original_ORDeliveryDate IS NULL AND ORDeliveryDate IS NULL" & _
        ") AND (ORDescription = @Original_ORDescription OR @Original_ORDescription IS NUL" & _
        "L AND ORDescription IS NULL) AND (ORInvoicedCost = @Original_ORInvoicedCost OR @" & _
        "Original_ORInvoicedCost IS NULL AND ORInvoicedCost IS NULL) AND (ORNotes = @Orig" & _
        "inal_ORNotes OR @Original_ORNotes IS NULL AND ORNotes IS NULL) AND (OROrderNumbe" & _
        "r = @Original_OROrderNumber OR @Original_OROrderNumber IS NULL AND OROrderNumber" & _
        " IS NULL) AND (ORQuotedCost = @Original_ORQuotedCost OR @Original_ORQuotedCost I" & _
        "S NULL AND ORQuotedCost IS NULL) AND (ORReceived = @Original_ORReceived OR @Orig" & _
        "inal_ORReceived IS NULL AND ORReceived IS NULL) AND (ORShipToJobAddress = @Origi" & _
        "nal_ORShipToJobAddress OR @Original_ORShipToJobAddress IS NULL AND ORShipToJobAd" & _
        "dress IS NULL) AND (ORSupplierInvoiceNumber = @Original_ORSupplierInvoiceNumber " & _
        "OR @Original_ORSupplierInvoiceNumber IS NULL AND ORSupplierInvoiceNumber IS NULL" & _
        ") AND (ORType = @Original_ORType OR @Original_ORType IS NULL AND ORType IS NULL)" & _
        ""
        Me.SqlDeleteCommand3.Connection = Me.hSqlConnection
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTIDShipTo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTIDShipTo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTIDSupplier", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTIDSupplier", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDOrderedBy", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDOrderedBy", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORDeliveryDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORDeliveryDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORDescription", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORInvoicedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORInvoicedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORNotes", System.Data.SqlDbType.VarChar, 500, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OROrderNumber", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OROrderNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORQuotedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORQuotedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORReceived", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORReceived", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORShipToJobAddress", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORShipToJobAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORSupplierInvoiceNumber", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORSupplierInvoiceNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORType", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORType", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO Orders (BRID, JBID, ORType, OROrderNumber, ORDate, CTIDSupplier, ORSh" & _
        "ipToJobAddress, CTIDShipTo, ORSupplierInvoiceNumber, EXIDOrderedBy, ORQuotedCost" & _
        ", ORInvoicedCost, ORDescription, ORDeliveryDate, ORNotes, ORReceived) VALUES (@B" & _
        "RID, @JBID, @ORType, @OROrderNumber, @ORDate, @CTIDSupplier, @ORShipToJobAddress" & _
        ", @CTIDShipTo, @ORSupplierInvoiceNumber, @EXIDOrderedBy, @ORQuotedCost, @ORInvoi" & _
        "cedCost, @ORDescription, @ORDeliveryDate, @ORNotes, @ORReceived); SELECT BRID, O" & _
        "RID, JBID, ORType, OROrderNumber, ORDate, CTIDSupplier, ORShipToJobAddress, CTID" & _
        "ShipTo, ORSupplierInvoiceNumber, EXIDOrderedBy, ORQuotedCost, ORInvoicedCost, OR" & _
        "Description, ORDeliveryDate, ORNotes, ORReceived, ORIsComplete, ORInvoicedCostTo" & _
        "tal, ORStatus, ID, JBDate FROM VOrders WHERE (BRID = @BRID) AND (ORID = SCOPE_ID" & _
        "ENTITY())"
        Me.SqlInsertCommand3.Connection = Me.hSqlConnection
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORType", System.Data.SqlDbType.VarChar, 3, "ORType"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OROrderNumber", System.Data.SqlDbType.BigInt, 8, "OROrderNumber"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORDate", System.Data.SqlDbType.DateTime, 8, "ORDate"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTIDSupplier", System.Data.SqlDbType.BigInt, 8, "CTIDSupplier"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORShipToJobAddress", System.Data.SqlDbType.Bit, 1, "ORShipToJobAddress"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTIDShipTo", System.Data.SqlDbType.BigInt, 8, "CTIDShipTo"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORSupplierInvoiceNumber", System.Data.SqlDbType.VarChar, 50, "ORSupplierInvoiceNumber"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDOrderedBy", System.Data.SqlDbType.Int, 4, "EXIDOrderedBy"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORQuotedCost", System.Data.SqlDbType.Money, 8, "ORQuotedCost"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORInvoicedCost", System.Data.SqlDbType.Money, 8, "ORInvoicedCost"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORDescription", System.Data.SqlDbType.VarChar, 50, "ORDescription"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORDeliveryDate", System.Data.SqlDbType.DateTime, 8, "ORDeliveryDate"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORNotes", System.Data.SqlDbType.VarChar, 500, "ORNotes"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORReceived", System.Data.SqlDbType.Bit, 1, "ORReceived"))
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT BRID, ORID, JBID, ORType, OROrderNumber, ORDate, CTIDSupplier, ORShipToJob" & _
        "Address, CTIDShipTo, ORSupplierInvoiceNumber, EXIDOrderedBy, ORQuotedCost, ORInv" & _
        "oicedCost, ORDescription, ORDeliveryDate, ORNotes, ORReceived, ORIsComplete, ORI" & _
        "nvoicedCostTotal, ORStatus, ID, JBDate FROM VOrders WHERE (BRID = @BRID) AND (OR" & _
        "Date >= @FROM_DATE OR @FROM_DATE IS NULL) AND (ORDate <= @TO_DATE OR @TO_DATE IS" & _
        " NULL) AND (ORType <> 'IO') AND (JBDate >= @FROM_JOB_DATE OR @FROM_JOB_DATE IS N" & _
        "ULL) AND (JBDate <= @TO_JOB_DATE OR @TO_JOB_DATE IS NULL)"
        Me.SqlSelectCommand3.Connection = Me.hSqlConnection
        Me.SqlSelectCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FROM_DATE", System.Data.SqlDbType.DateTime, 8, "ORDate"))
        Me.SqlSelectCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TO_DATE", System.Data.SqlDbType.DateTime, 8, "ORDate"))
        Me.SqlSelectCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FROM_JOB_DATE", System.Data.SqlDbType.DateTime, 8, "JBDate"))
        Me.SqlSelectCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TO_JOB_DATE", System.Data.SqlDbType.DateTime, 8, "JBDate"))
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE Orders SET BRID = @BRID, JBID = @JBID, ORType = @ORType, OROrderNumber = @" & _
        "OROrderNumber, ORDate = @ORDate, CTIDSupplier = @CTIDSupplier, ORShipToJobAddres" & _
        "s = @ORShipToJobAddress, CTIDShipTo = @CTIDShipTo, ORSupplierInvoiceNumber = @OR" & _
        "SupplierInvoiceNumber, EXIDOrderedBy = @EXIDOrderedBy, ORQuotedCost = @ORQuotedC" & _
        "ost, ORInvoicedCost = @ORInvoicedCost, ORDescription = @ORDescription, ORDeliver" & _
        "yDate = @ORDeliveryDate, ORNotes = @ORNotes, ORReceived = @ORReceived WHERE (BRI" & _
        "D = @Original_BRID) AND (ORID = @Original_ORID) AND (CTIDShipTo = @Original_CTID" & _
        "ShipTo OR @Original_CTIDShipTo IS NULL AND CTIDShipTo IS NULL) AND (CTIDSupplier" & _
        " = @Original_CTIDSupplier OR @Original_CTIDSupplier IS NULL AND CTIDSupplier IS " & _
        "NULL) AND (EXIDOrderedBy = @Original_EXIDOrderedBy OR @Original_EXIDOrderedBy IS" & _
        " NULL AND EXIDOrderedBy IS NULL) AND (JBID = @Original_JBID OR @Original_JBID IS" & _
        " NULL AND JBID IS NULL) AND (ORDate = @Original_ORDate OR @Original_ORDate IS NU" & _
        "LL AND ORDate IS NULL) AND (ORDeliveryDate = @Original_ORDeliveryDate OR @Origin" & _
        "al_ORDeliveryDate IS NULL AND ORDeliveryDate IS NULL) AND (ORDescription = @Orig" & _
        "inal_ORDescription OR @Original_ORDescription IS NULL AND ORDescription IS NULL)" & _
        " AND (ORInvoicedCost = @Original_ORInvoicedCost OR @Original_ORInvoicedCost IS N" & _
        "ULL AND ORInvoicedCost IS NULL) AND (ORNotes = @Original_ORNotes OR @Original_OR" & _
        "Notes IS NULL AND ORNotes IS NULL) AND (OROrderNumber = @Original_OROrderNumber " & _
        "OR @Original_OROrderNumber IS NULL AND OROrderNumber IS NULL) AND (ORQuotedCost " & _
        "= @Original_ORQuotedCost OR @Original_ORQuotedCost IS NULL AND ORQuotedCost IS N" & _
        "ULL) AND (ORReceived = @Original_ORReceived OR @Original_ORReceived IS NULL AND " & _
        "ORReceived IS NULL) AND (ORShipToJobAddress = @Original_ORShipToJobAddress OR @O" & _
        "riginal_ORShipToJobAddress IS NULL AND ORShipToJobAddress IS NULL) AND (ORSuppli" & _
        "erInvoiceNumber = @Original_ORSupplierInvoiceNumber OR @Original_ORSupplierInvoi" & _
        "ceNumber IS NULL AND ORSupplierInvoiceNumber IS NULL) AND (ORType = @Original_OR" & _
        "Type OR @Original_ORType IS NULL AND ORType IS NULL); SELECT BRID, ORID, JBID, O" & _
        "RType, OROrderNumber, ORDate, CTIDSupplier, ORShipToJobAddress, CTIDShipTo, ORSu" & _
        "pplierInvoiceNumber, EXIDOrderedBy, ORQuotedCost, ORInvoicedCost, ORDescription," & _
        " ORDeliveryDate, ORNotes, ORReceived, ORIsComplete, ORInvoicedCostTotal, ORStatu" & _
        "s, ID, JBDate FROM VOrders WHERE (BRID = @BRID) AND (ORID = @ORID)"
        Me.SqlUpdateCommand3.Connection = Me.hSqlConnection
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORType", System.Data.SqlDbType.VarChar, 3, "ORType"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OROrderNumber", System.Data.SqlDbType.BigInt, 8, "OROrderNumber"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORDate", System.Data.SqlDbType.DateTime, 8, "ORDate"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTIDSupplier", System.Data.SqlDbType.BigInt, 8, "CTIDSupplier"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORShipToJobAddress", System.Data.SqlDbType.Bit, 1, "ORShipToJobAddress"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTIDShipTo", System.Data.SqlDbType.BigInt, 8, "CTIDShipTo"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORSupplierInvoiceNumber", System.Data.SqlDbType.VarChar, 50, "ORSupplierInvoiceNumber"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDOrderedBy", System.Data.SqlDbType.Int, 4, "EXIDOrderedBy"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORQuotedCost", System.Data.SqlDbType.Money, 8, "ORQuotedCost"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORInvoicedCost", System.Data.SqlDbType.Money, 8, "ORInvoicedCost"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORDescription", System.Data.SqlDbType.VarChar, 50, "ORDescription"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORDeliveryDate", System.Data.SqlDbType.DateTime, 8, "ORDeliveryDate"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORNotes", System.Data.SqlDbType.VarChar, 500, "ORNotes"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORReceived", System.Data.SqlDbType.Bit, 1, "ORReceived"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTIDShipTo", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTIDShipTo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTIDSupplier", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTIDSupplier", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDOrderedBy", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDOrderedBy", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORDeliveryDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORDeliveryDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORDescription", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORInvoicedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORInvoicedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORNotes", System.Data.SqlDbType.VarChar, 500, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OROrderNumber", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OROrderNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORQuotedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORQuotedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORReceived", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORReceived", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORShipToJobAddress", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORShipToJobAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORSupplierInvoiceNumber", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORSupplierInvoiceNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORType", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORID", System.Data.SqlDbType.BigInt, 8, "ORID"))
        '
        'daOrders_Materials
        '
        Me.daOrders_Materials.ContinueUpdateOnError = True
        Me.daOrders_Materials.DeleteCommand = Me.SqlDeleteCommand8
        Me.daOrders_Materials.InsertCommand = Me.SqlInsertCommand7
        Me.daOrders_Materials.SelectCommand = Me.SqlSelectCommand7
        Me.daOrders_Materials.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VOrders_Materials", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("OMID", "OMID"), New System.Data.Common.DataColumnMapping("ORID", "ORID"), New System.Data.Common.DataColumnMapping("MTID", "MTID"), New System.Data.Common.DataColumnMapping("OMPrimaryAmount", "OMPrimaryAmount"), New System.Data.Common.DataColumnMapping("OMQuotedCost", "OMQuotedCost"), New System.Data.Common.DataColumnMapping("OMInvoicedCost", "OMInvoicedCost"), New System.Data.Common.DataColumnMapping("OMUseInvoicedCost", "OMUseInvoicedCost"), New System.Data.Common.DataColumnMapping("OMBrand", "OMBrand"), New System.Data.Common.DataColumnMapping("OMModelNumber", "OMModelNumber"), New System.Data.Common.DataColumnMapping("OMDescription", "OMDescription"), New System.Data.Common.DataColumnMapping("OMQty", "OMQty")})})
        Me.daOrders_Materials.UpdateCommand = Me.SqlUpdateCommand8
        '
        'SqlDeleteCommand8
        '
        Me.SqlDeleteCommand8.CommandText = "DELETE FROM Orders_Materials WHERE (BRID = @Original_BRID) AND (OMID = @Original_" & _
        "OMID) AND (MTID = @Original_MTID OR @Original_MTID IS NULL AND MTID IS NULL) AND" & _
        " (OMBrand = @Original_OMBrand OR @Original_OMBrand IS NULL AND OMBrand IS NULL) " & _
        "AND (OMDescription = @Original_OMDescription OR @Original_OMDescription IS NULL " & _
        "AND OMDescription IS NULL) AND (OMInvoicedCost = @Original_OMInvoicedCost OR @Or" & _
        "iginal_OMInvoicedCost IS NULL AND OMInvoicedCost IS NULL) AND (OMModelNumber = @" & _
        "Original_OMModelNumber OR @Original_OMModelNumber IS NULL AND OMModelNumber IS N" & _
        "ULL) AND (OMPrimaryAmount = @Original_OMPrimaryAmount OR @Original_OMPrimaryAmou" & _
        "nt IS NULL AND OMPrimaryAmount IS NULL) AND (OMQty = @Original_OMQty OR @Origina" & _
        "l_OMQty IS NULL AND OMQty IS NULL) AND (OMQuotedCost = @Original_OMQuotedCost OR" & _
        " @Original_OMQuotedCost IS NULL AND OMQuotedCost IS NULL) AND (OMUseInvoicedCost" & _
        " = @Original_OMUseInvoicedCost OR @Original_OMUseInvoicedCost IS NULL AND OMUseI" & _
        "nvoicedCost IS NULL) AND (ORID = @Original_ORID)"
        Me.SqlDeleteCommand8.Connection = Me.hSqlConnection
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMBrand", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMBrand", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMDescription", System.Data.SqlDbType.VarChar, 500, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMInvoicedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMInvoicedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMModelNumber", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMModelNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMPrimaryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OMPrimaryAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMQty", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMQty", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMQuotedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMQuotedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMUseInvoicedCost", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMUseInvoicedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand7
        '
        Me.SqlInsertCommand7.CommandText = "INSERT INTO Orders_Materials (BRID, ORID, MTID, OMPrimaryAmount, OMQuotedCost, OM" & _
        "InvoicedCost, OMUseInvoicedCost, OMBrand, OMModelNumber, OMDescription, OMQty) V" & _
        "ALUES (@BRID, @ORID, @MTID, @OMPrimaryAmount, @OMQuotedCost, @OMInvoicedCost, @O" & _
        "MUseInvoicedCost, @OMBrand, @OMModelNumber, @OMDescription, @OMQty); SELECT BRID" & _
        ", OMID, ORID, MTID, OMPrimaryAmount, OMQuotedCost, OMInvoicedCost, OMUseInvoiced" & _
        "Cost, OMBrand, OMModelNumber, OMDescription, OMQty, MTPrimaryUOMShortName FROM V" & _
        "Orders_Materials WHERE (BRID = @BRID) AND (OMID = @@IDENTITY)"
        Me.SqlInsertCommand7.Connection = Me.hSqlConnection
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORID", System.Data.SqlDbType.BigInt, 8, "ORID"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMPrimaryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OMPrimaryAmount", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMQuotedCost", System.Data.SqlDbType.Money, 8, "OMQuotedCost"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMInvoicedCost", System.Data.SqlDbType.Money, 8, "OMInvoicedCost"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMUseInvoicedCost", System.Data.SqlDbType.Bit, 1, "OMUseInvoicedCost"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMBrand", System.Data.SqlDbType.VarChar, 50, "OMBrand"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMModelNumber", System.Data.SqlDbType.VarChar, 50, "OMModelNumber"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMDescription", System.Data.SqlDbType.VarChar, 500, "OMDescription"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMQty", System.Data.SqlDbType.Int, 4, "OMQty"))
        '
        'SqlSelectCommand7
        '
        Me.SqlSelectCommand7.CommandText = "SELECT BRID, OMID, ORID, MTID, OMPrimaryAmount, OMQuotedCost, OMInvoicedCost, OMU" & _
        "seInvoicedCost, OMBrand, OMModelNumber, OMDescription, OMQty, MTPrimaryUOMShortN" & _
        "ame FROM VOrders_Materials WHERE (BRID = @BRID) AND (ORDate >= @FROM_DATE OR @FR" & _
        "OM_DATE IS NULL) AND (ORDate <= @TO_DATE OR @TO_DATE IS NULL) AND (ORType <> 'IO" & _
        "') AND (JBDate >= @FROM_JOB_DATE OR @FROM_JOB_DATE IS NULL) AND (JBDate <= @TO_J" & _
        "OB_DATE OR @TO_JOB_DATE IS NULL)"
        Me.SqlSelectCommand7.Connection = Me.hSqlConnection
        Me.SqlSelectCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FROM_DATE", System.Data.SqlDbType.DateTime, 8, "ORDate"))
        Me.SqlSelectCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TO_DATE", System.Data.SqlDbType.DateTime, 8, "ORDate"))
        Me.SqlSelectCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FROM_JOB_DATE", System.Data.SqlDbType.DateTime, 8, "JBDate"))
        Me.SqlSelectCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TO_JOB_DATE", System.Data.SqlDbType.DateTime, 8, "JBDate"))
        '
        'SqlUpdateCommand8
        '
        Me.SqlUpdateCommand8.CommandText = "UPDATE Orders_Materials SET BRID = @BRID, ORID = @ORID, MTID = @MTID, OMPrimaryAm" & _
        "ount = @OMPrimaryAmount, OMQuotedCost = @OMQuotedCost, OMInvoicedCost = @OMInvoi" & _
        "cedCost, OMUseInvoicedCost = @OMUseInvoicedCost, OMBrand = @OMBrand, OMModelNumb" & _
        "er = @OMModelNumber, OMDescription = @OMDescription, OMQty = @OMQty WHERE (BRID " & _
        "= @Original_BRID) AND (OMID = @Original_OMID) AND (MTID = @Original_MTID OR @Ori" & _
        "ginal_MTID IS NULL AND MTID IS NULL) AND (OMBrand = @Original_OMBrand OR @Origin" & _
        "al_OMBrand IS NULL AND OMBrand IS NULL) AND (OMDescription = @Original_OMDescrip" & _
        "tion OR @Original_OMDescription IS NULL AND OMDescription IS NULL) AND (OMInvoic" & _
        "edCost = @Original_OMInvoicedCost OR @Original_OMInvoicedCost IS NULL AND OMInvo" & _
        "icedCost IS NULL) AND (OMModelNumber = @Original_OMModelNumber OR @Original_OMMo" & _
        "delNumber IS NULL AND OMModelNumber IS NULL) AND (OMPrimaryAmount = @Original_OM" & _
        "PrimaryAmount OR @Original_OMPrimaryAmount IS NULL AND OMPrimaryAmount IS NULL) " & _
        "AND (OMQty = @Original_OMQty OR @Original_OMQty IS NULL AND OMQty IS NULL) AND (" & _
        "OMQuotedCost = @Original_OMQuotedCost OR @Original_OMQuotedCost IS NULL AND OMQu" & _
        "otedCost IS NULL) AND (OMUseInvoicedCost = @Original_OMUseInvoicedCost OR @Origi" & _
        "nal_OMUseInvoicedCost IS NULL AND OMUseInvoicedCost IS NULL) AND (ORID = @Origin" & _
        "al_ORID); SELECT BRID, OMID, ORID, MTID, OMPrimaryAmount, OMQuotedCost, OMInvoic" & _
        "edCost, OMUseInvoicedCost, OMBrand, OMModelNumber, OMDescription, OMQty, MTPrima" & _
        "ryUOMShortName FROM VOrders_Materials WHERE (BRID = @BRID) AND (OMID = @OMID)"
        Me.SqlUpdateCommand8.Connection = Me.hSqlConnection
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ORID", System.Data.SqlDbType.BigInt, 8, "ORID"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMPrimaryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OMPrimaryAmount", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMQuotedCost", System.Data.SqlDbType.Money, 8, "OMQuotedCost"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMInvoicedCost", System.Data.SqlDbType.Money, 8, "OMInvoicedCost"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMUseInvoicedCost", System.Data.SqlDbType.Bit, 1, "OMUseInvoicedCost"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMBrand", System.Data.SqlDbType.VarChar, 50, "OMBrand"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMModelNumber", System.Data.SqlDbType.VarChar, 50, "OMModelNumber"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMDescription", System.Data.SqlDbType.VarChar, 500, "OMDescription"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMQty", System.Data.SqlDbType.Int, 4, "OMQty"))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMBrand", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMBrand", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMDescription", System.Data.SqlDbType.VarChar, 500, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMInvoicedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMInvoicedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMModelNumber", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMModelNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMPrimaryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "OMPrimaryAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMQty", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMQty", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMQuotedCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMQuotedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OMUseInvoicedCost", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OMUseInvoicedCost", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ORID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ORID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OMID", System.Data.SqlDbType.BigInt, 8, "OMID"))
        '
        'daExpenses
        '
        Me.daExpenses.InsertCommand = Me.SqlInsertCommand9
        Me.daExpenses.SelectCommand = Me.SqlSelectCommand9
        Me.daExpenses.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VExpenses", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("EXName", "EXName"), New System.Data.Common.DataColumnMapping("EGType", "EGType"), New System.Data.Common.DataColumnMapping("EGName", "EGName"), New System.Data.Common.DataColumnMapping("EGID", "EGID"), New System.Data.Common.DataColumnMapping("PMAllowValueInJobScreen", "PMAllowValueInJobScreen"), New System.Data.Common.DataColumnMapping("PMAllowHoursInJobScreen", "PMAllowHoursInJobScreen")})})
        '
        'SqlInsertCommand9
        '
        Me.SqlInsertCommand9.CommandText = "INSERT INTO VExpenses(BRID, EXName, EGType, EGName, EGID, PMAllowValueInJobScreen" & _
        ", PMAllowHoursInJobScreen) VALUES (@BRID, @EXName, @EGType, @EGName, @EGID, @PMA" & _
        "llowValueInJobScreen, @PMAllowHoursInJobScreen); SELECT BRID, EXID, EXName, EGTy" & _
        "pe, EGName, EGID, PMAllowValueInJobScreen, PMAllowHoursInJobScreen FROM VExpense" & _
        "s"
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 2, "EGType"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGName", System.Data.SqlDbType.VarChar, 50, "EGName"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGID", System.Data.SqlDbType.Int, 4, "EGID"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMAllowValueInJobScreen", System.Data.SqlDbType.Bit, 1, "PMAllowValueInJobScreen"))
        Me.SqlInsertCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMAllowHoursInJobScreen", System.Data.SqlDbType.Bit, 1, "PMAllowHoursInJobScreen"))
        '
        'SqlSelectCommand9
        '
        Me.SqlSelectCommand9.CommandText = "SELECT BRID, EXID, EXName, EGType, EGName, EGID, PMAllowValueInJobScreen, PMAllow" & _
        "HoursInJobScreen FROM VExpenses WHERE (EXDiscontinued = 0) AND (BRID = @BRID)"
        Me.SqlSelectCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'lvOrders2
        '
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "lvOrders2"
        Me.Size = New System.Drawing.Size(632, 392)
        CType(Me.DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvOrders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return hSqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            hSqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private Sub EnableDisable()
        btnNew.Enabled = AllowNew
        btnEdit.Enabled = AllowEdit
        btnDelete.Enabled = AllowDelete
    End Sub

    Public Function FillDataSet() As Integer Implements IListControl.FillDataSet
        Dim cursor As System.Windows.Forms.Cursor = ExplorerForm.Cursor
        ExplorerForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
        ' Open connection
        Dim trans As SqlClient.SqlTransaction = Connection.BeginTransaction(IsolationLevel.ReadUncommitted)
        Power.Library.Library.ApplyTransactionToAllDataAdapters(trans, Me)
        'Me.GridControl1.DataSource.Clear()
        daExpenses.Fill(DataSet)
        daMaterials.Fill(DataSet)
        daStockedItems.Fill(DataSet)
        FillData()
        'Close connecton
        trans.Commit()
        ExplorerForm.Cursor = cursor
    End Function

    Private Sub ClearDataSet()
        Me.DataSet.Orders_Materials_SubItems.Clear()
        Me.DataSet.VOrders_Materials.Clear()
        Me.DataSet.VOrders_StockedItems.Clear()
        Me.DataSet.VOrders.Clear()
    End Sub

    Private Sub FillData()
        daOrders.Fill(DataSet)
        daOrders_Materials.Fill(DataSet)
        daOrders_Materials_SubItems.Fill(DataSet)
        daOrders_StockedItems.Fill(DataSet)
    End Sub

    Private Sub UpdateData()
        Dim deletionsDataSet As DataSet = DataSet.GetChanges(DataRowState.Deleted)
        Dim insertionsDataSet As DataSet = DataSet.GetChanges(DataRowState.Added + DataRowState.Modified)
        If Not deletionsDataSet Is Nothing Then
            daOrders_Materials_SubItems.Update(deletionsDataSet)
            daOrders_Materials.Update(deletionsDataSet)
            daOrders_StockedItems.Update(deletionsDataSet)
            daOrders.Update(deletionsDataSet)
        End If
        'If Not insertionsDataSet Is Nothing Then
        daOrders.Update(DataSet)
        daOrders_Materials.Update(DataSet)
        daOrders_StockedItems.Update(DataSet)
        daOrders_Materials_SubItems.Update(DataSet)
        'End If
        'DataSet.AcceptChanges()
    End Sub

#Region " Filters "

    Private Sub SetUpFilters()
        ' Set Up Default Advanced Filter
        beiFromDate.EditValue = DateAdd(DateInterval.Month, -6, Today.Date)
        beiToDate.EditValue = Nothing
        beiFromJobDate.EditValue = Nothing
        beiToJobDate.EditValue = Nothing
        UpdateDateFilter()
        GridView1.ClearColumnsFilter()
    End Sub

    Private Sub DateFilter_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beiFromDate.EditValueChanged, beiToDate.EditValueChanged, beiFromJobDate.EditValueChanged, beiToJobDate.EditValueChanged
        UpdateDateFilter()
    End Sub

    Private DateFilterIsListening As Boolean = False ' This is so this doesn't trigger during load (this will happen in code)
    Private Sub UpdateDateFilter()
        Me.daOrders.SelectCommand.Parameters("@FROM_DATE").Value = IsNull(beiFromDate.EditValue, DBNull.Value)
        Me.daOrders_Materials.SelectCommand.Parameters("@FROM_DATE").Value = IsNull(beiFromDate.EditValue, DBNull.Value)
        Me.daOrders_Materials_SubItems.SelectCommand.Parameters("@FROM_DATE").Value = IsNull(beiFromDate.EditValue, DBNull.Value)
        Me.daOrders_StockedItems.SelectCommand.Parameters("@FROM_DATE").Value = IsNull(beiFromDate.EditValue, DBNull.Value)
        Me.daOrders.SelectCommand.Parameters("@TO_DATE").Value = IsNull(beiToDate.EditValue, DBNull.Value)
        Me.daOrders_Materials.SelectCommand.Parameters("@TO_DATE").Value = IsNull(beiToDate.EditValue, DBNull.Value)
        Me.daOrders_Materials_SubItems.SelectCommand.Parameters("@TO_DATE").Value = IsNull(beiToDate.EditValue, DBNull.Value)
        Me.daOrders_StockedItems.SelectCommand.Parameters("@TO_DATE").Value = IsNull(beiToDate.EditValue, DBNull.Value)
        Me.daOrders.SelectCommand.Parameters("@FROM_JOB_DATE").Value = IsNull(beiFromJobDate.EditValue, DBNull.Value)
        Me.daOrders_Materials.SelectCommand.Parameters("@FROM_JOB_DATE").Value = IsNull(beiFromJobDate.EditValue, DBNull.Value)
        Me.daOrders_Materials_SubItems.SelectCommand.Parameters("@FROM_JOB_DATE").Value = IsNull(beiFromJobDate.EditValue, DBNull.Value)
        Me.daOrders_StockedItems.SelectCommand.Parameters("@FROM_JOB_DATE").Value = IsNull(beiFromJobDate.EditValue, DBNull.Value)
        Me.daOrders.SelectCommand.Parameters("@TO_JOB_DATE").Value = IsNull(beiToJobDate.EditValue, DBNull.Value)
        Me.daOrders_Materials.SelectCommand.Parameters("@TO_JOB_DATE").Value = IsNull(beiToJobDate.EditValue, DBNull.Value)
        Me.daOrders_Materials_SubItems.SelectCommand.Parameters("@TO_JOB_DATE").Value = IsNull(beiToJobDate.EditValue, DBNull.Value)
        Me.daOrders_StockedItems.SelectCommand.Parameters("@TO_JOB_DATE").Value = IsNull(beiToJobDate.EditValue, DBNull.Value)
        If DateFilterIsListening Then
            ClearDataSet()
            FillDataSet()
        End If
    End Sub

    Private Sub btnResetFilters_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnResetFilters.ItemClick
        DateFilterIsListening = False
        SetUpFilters()
        Me.DataSet.Tables(Me.daOrders_StockedItems.TableMappings(0).DataSetTable).Clear()
        Me.DataSet.Tables(Me.daOrders_Materials_SubItems.TableMappings(0).DataSetTable).Clear()
        Me.DataSet.Tables(Me.daOrders_Materials.TableMappings(0).DataSetTable).Clear()
        Me.DataSet.Tables(Me.daOrders.TableMappings(0).DataSetTable).Clear()
        FillDataSet()
        DateFilterIsListening = True
    End Sub

#End Region

    Private Sub GridView1_RowCountChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.RowCountChanged
        ExplorerForm.UpdateNumItems(NumItems)
        ExplorerForm.UpdateVisibleItems(VisibleItems)
    End Sub

    Public ReadOnly Property VisibleItems() As Integer Implements Power.Library.IListControl.VisibleItems
        Get
            Return GridView1.RowCount
        End Get
    End Property

    Public ReadOnly Property NumItems() As Integer Implements Power.Library.IListControl.NumItems
        Get
            Return DataSet.Tables(daOrders.TableMappings(0).DataSetTable).Rows.Count
        End Get
    End Property

    Private hBRID As Integer
    Private Property BRID() As Integer
        Get
            Return hBRID
        End Get
        Set(ByVal Value As Integer)
            hBRID = Value
            daOrders.SelectCommand.Parameters("@BRID").Value = Value
            daOrders_Materials.SelectCommand.Parameters("@BRID").Value = Value
            daOrders_Materials_SubItems.SelectCommand.Parameters("@BRID").Value = Value
            daOrders_StockedItems.SelectCommand.Parameters("@BRID").Value = Value
            daMaterials.SelectCommand.Parameters("@BRID").Value = Value
            daStockedItems.SelectCommand.Parameters("@BRID").Value = Value
            daExpenses.SelectCommand.Parameters("@BRID").Value = Value
        End Set
    End Property

    Public ReadOnly Property SelectedRow() As DataRow
        Get
            If Not GridView1.GetSelectedRows Is Nothing Then
                Return GridView1.GetDataRow(GridView1.GetSelectedRows(0))
            End If
        End Get
    End Property

    Public ReadOnly Property SelectedRowField(ByVal Field As String) As Object
        Get
            If Not SelectedRow Is Nothing Then
                Return SelectedRow.Item(Field)
            Else
                Return DBNull.Value
            End If
        End Get
    End Property

    Public Function SelectRow(ByVal BRID As Int32, ByVal SAID As Int64) As Boolean
        Dim i As Integer = 0
        Do Until False
            Try
                If GridView1.GetDataRow(GridView1.GetVisibleRowHandle(i))("BRID") = BRID And _
                        GridView1.GetDataRow(GridView1.GetVisibleRowHandle(i))("SAID") = SAID Then
                    GridView1.ClearSelection()
                    GridView1.SelectRow(GridView1.GetVisibleRowHandle(i))
                    Return True
                End If
                i += 1
            Catch ex As NullReferenceException
                Return False
            End Try
        Loop
        Return False
    End Function

#Region " New, Open and Delete "

    Private Sub DataListView_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GridControl1.DoubleClick
        List_Edit()
    End Sub

    Private Sub btnNew_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnNew.ItemClick
        List_New()
    End Sub

    Public Sub List_New() Implements IListControl.List_New
        Dim c As Cursor = ParentForm.Cursor
        ParentForm.Cursor = Windows.Forms.Cursors.WaitCursor
        frmGeneralPurchaseOrder2.Add(dvOrders.Table, BRID, HideAccountingInformation)
        UpdateData()
        'FillData()
        ParentForm.Cursor = c
    End Sub

    Private Sub btnEdit_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnEdit.ItemClick
        List_Edit()
    End Sub

    Public Sub List_Edit() Implements IListControl.List_Edit
        If Not SelectedRow Is Nothing Then
            Dim c As Cursor = ParentForm.Cursor
            ParentForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Dim ORID As Long = SelectedRowField("ORID")
            If OrderExists(SelectedRowField("BRID"), ORID) Then
                If DataAccess.spExecLockRequest("sp_GetOrderLock", BRID, ORID, Connection) Then
                    Select Case Trim(SelectedRow("ORType"))
                        Case "IO"
                            frmInternalOrder2.Edit(SelectedRow)
                        Case "JPO"
                            frmJobPurchaseOrder2.Edit(SelectedRow, Nothing, HideAccountingInformation)
                        Case "GPO"
                            frmGeneralPurchaseOrder2.Edit(SelectedRow, HideAccountingInformation)
                    End Select
                    UpdateData()
                    'FillData()
                    DataAccess.spExecLockRequest("sp_ReleaseOrderLock", BRID, ORID, Connection)
                Else
                    Message.CurrentlyAccessed("order")
                End If
            Else
                Message.AlreadyDeleted("order", Message.ObjectAction.Edit)
                ClearDataSet()
                FillDataSet()
            End If
            ParentForm.Cursor = c
        End If
    End Sub

    Public Sub List_Cancel() Implements Power.Library.IListControl.List_Cancel

    End Sub

    Public Sub List_Uncancel() Implements Power.Library.IListControl.List_Uncancel

    End Sub

    Private Sub btnDelete_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnDelete.ItemClick
        List_Delete()
    End Sub

    Public Sub List_Delete() Implements IListControl.List_Delete
        If Not SelectedRow Is Nothing Then
            Dim ORID As Long = SelectedRowField("ORID")
            If Message.AskDeleteObject("order", Message.ObjectAction.Delete) = MsgBoxResult.Yes Then
                If OrderExists(SelectedRowField("BRID"), SelectedRowField("ORID")) Then
                    If DataAccess.spExecLockRequest("sp_GetOrderLock", BRID, ORID, Connection) Then
                        SelectedRow.Delete()
                        UpdateData()
                        'FillData()
                        DataAccess.spExecLockRequest("sp_ReleaseOrderLock", BRID, ORID, Connection)
                    Else
                        Message.CurrentlyAccessed("order")
                    End If
                Else
                    ClearDataSet()
                    FillDataSet()
                End If
            End If
        End If
    End Sub

    Private Function OrderExists(ByVal BRID As Int32, ByVal ORID As Int64) As Boolean
        Dim cnn As SqlClient.SqlConnection = Connection
        Dim cmd As New SqlClient.SqlCommand("SELECT Count(ORID) FROM Orders WHERE BRID = @BRID AND ORID = @ORID", cnn)
        cmd.CommandType = CommandType.Text
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@ORID", ORID)
        Return cmd.ExecuteScalar > 0
    End Function

#End Region

    Private hExplorerForm As Form
    Public ReadOnly Property ExplorerForm() As frmExplorer
        Get
            Return hExplorerForm
        End Get
    End Property

#Region " Printing "

    Private Sub btnPrint_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrint.ItemClick
        Print(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub Print(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.Print
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.PrintDlg()
    End Sub

    Private Sub btnPrintPreview_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrintPreview.ItemClick
        PrintPreview(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub PrintPreview(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.PrintPreview
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(ExplorerForm.PrintingSystem)
        link.Component = GridControl1
        link.ShowPreview()
    End Sub

    Private Sub btnQuickPrint_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnQuickPrint.ItemClick
        QuickPrint(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub QuickPrint(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.QuickPrint
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.Print(PrintingSystem.PageSettings.PrinterName)
    End Sub

#End Region

    Public ReadOnly Property AllowDelete() As Boolean Implements Power.Library.IListControl.AllowDelete
        Get
            Return Not HasRole("branch_read_only")
        End Get
    End Property

    Public ReadOnly Property AllowEdit() As Boolean Implements Power.Library.IListControl.AllowEdit
        Get
            Return True
        End Get
    End Property

    Public ReadOnly Property AllowNew() As Boolean Implements Power.Library.IListControl.AllowNew
        Get
            Return Not HasRole("branch_read_only")
        End Get
    End Property

    Public ReadOnly Property AllowCancel() As Boolean Implements Power.Library.IListControl.AllowCancel
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property AllowUncancel() As Boolean Implements Power.Library.IListControl.AllowUncancel
        Get
            Return False
        End Get
    End Property

    Private Sub btnHelp_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHelp.ItemClick
        ShowHelpTopic(ExplorerForm, "UsingGeneralPurchaseOrders.htm")
    End Sub
End Class
