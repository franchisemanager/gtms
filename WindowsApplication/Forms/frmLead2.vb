Public Class frmLead2
    Inherits DevExpress.XtraEditors.XtraForm

    Private DataRow As DataRow

    Public Shared Sub Add(ByVal BRID As Int32)
        Dim gui As New frmLead2

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            .FillPreliminaryData(BRID)

            .DataRow = .dataSet.Leads.NewRow()
            .DataRow("BRID") = BRID
            .DataRow("ID") = sp_GetNextID(BRID, .SqlConnection)
            .DataRow("LDDate") = Today.Date
            .dataSet.Leads.Rows.Add(.DataRow)

            .FillInitData()

            gui.ShowDialog()

            gui.ReleaseAppointmentLocks()

            .SqlConnection.Close()
        End With
    End Sub

    Public Shared Sub Edit(ByVal BRID As Int32, ByRef LDID As Int64)
        Dim gui As New frmLead2

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            If DataAccess.spExecLockRequest("sp_GetLeadLock", BRID, LDID, .SqlConnection) Then

                .FillPreliminaryData(BRID)

                .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = BRID
                .SqlDataAdapter.SelectCommand.Parameters("@LDID").Value = LDID
                .SqlDataAdapter.Fill(.dataSet)
                .DataRow = .dataSet.Leads(0)

                .FillData()

                gui.ShowDialog()

                gui.ReleaseAppointmentLocks()
                DataAccess.spExecLockRequest("sp_ReleaseLeadLock", BRID, LDID, .SqlConnection)
            Else
                Message.CurrentlyAccessed("lead")
            End If

            .SqlConnection.Close()
        End With
    End Sub

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()
        ReadMRU(txtLDJobDescription, UserAppDataPath)

        'Add any initialization after the InitializeComponent() call
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents GroupLine1 As Power.Forms.GroupLine
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupLine2 As Power.Forms.GroupLine
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents GroupLine3 As Power.Forms.GroupLine
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupLine17 As Power.Forms.GroupLine
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents GroupLine18 As Power.Forms.GroupLine
    Friend WithEvents GroupLine19 As Power.Forms.GroupLine
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents GroupLine4 As Power.Forms.GroupLine
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents GroupLine5 As Power.Forms.GroupLine
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents GroupLine6 As Power.Forms.GroupLine
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents chkLDQuoteReceived As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ListBox As Power.Forms.ListBox
    Friend WithEvents pnlMain As DevExpress.XtraEditors.PanelControl
    Friend WithEvents dpLDDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents txtLDJobDescription As DevExpress.XtraEditors.MRUEdit
    Friend WithEvents txtEXIDRep As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents daLeadSources As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents txtLSName As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents txtLDJobStreetAddress01 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDJobStreetAddress02 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDJobSuburb As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDJobState As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDJobPostCode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents dvSalesReps As System.Data.DataView
    Friend WithEvents lblJobState As System.Windows.Forms.Label
    Friend WithEvents lblJobStreetAddress As System.Windows.Forms.Label
    Friend WithEvents lblJobSuburb As System.Windows.Forms.Label
    Friend WithEvents rgLDJobAddressAsAbove As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents GroupLine7 As Power.Forms.GroupLine
    Friend WithEvents txtLDClientSurname As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientFirstName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDReferenceName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientStreetAddress01 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientStreetAddress02 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientSuburb As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientState As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientPostCode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientFaxNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientMobileNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents pnlEnquiryInformation As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlScheduling As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlJobAddress As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlEnquirySources As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlClientInformation As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtLDUser As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtLDClientEmail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnAddAppointment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEditAppointment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveAppointment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents daLeadsSubSources As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents colSSName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSSUserSelected As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dgAppointments As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvAppointments As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colAPBegin As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEXName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dvAppointments As System.Data.DataView
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents GroupLine8 As Power.Forms.GroupLine
    Friend WithEvents GroupLine9 As Power.Forms.GroupLine
    Friend WithEvents btnViewCalendar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents cmHistory As System.Windows.Forms.ContextMenu
    Friend WithEvents miClearHistory As System.Windows.Forms.MenuItem
    Friend WithEvents DsCalendar As WindowsApplication.dsCalendar
    Friend WithEvents daTasks As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents Storage As DevExpress.XtraScheduler.SchedulerStorage
    Friend WithEvents dvExpenses As System.Data.DataView
    Friend WithEvents dvResources As System.Data.DataView
    Friend WithEvents pnlNotes As DevExpress.XtraEditors.GroupControl
    Friend WithEvents dgNotes As DevExpress.XtraGrid.GridControl
    Friend WithEvents daIDNotes As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents txtINDate As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents lblJobPostCode As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents dataSet As WindowsApplication.dsLeads
    Friend WithEvents cmdLeadsSubSources_init As System.Data.SqlClient.SqlCommand
    Friend WithEvents pnlQuoteRequestInformation As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents rgLDQuoteRequested As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daINFollowUpTypes As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents txtEXID As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents txtINFollowUpText As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents DsNotes As WindowsApplication.dsNotes
    Friend WithEvents dvExpenses2 As System.Data.DataView
    Friend WithEvents SqlSelectCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents gvNotes As DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
    Friend WithEvents GridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents colINUser As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colINDate As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colINNotes As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colINFollowUpText As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents txtINNotes As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents daExpenses As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daAppointments As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents pccContacts As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents pceContact As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents daContacts As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents rgLDUseContactFalse As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents rgLDUseContactTrue As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents txtLDClientWorkPhoneNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientHomePhoneNumber As DevExpress.XtraEditors.TextEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmLead2))
        Me.dataSet = New WindowsApplication.dsLeads
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.ListBox = New Power.Forms.ListBox
        Me.txtEXIDRep = New DevExpress.XtraEditors.LookUpEdit
        Me.dvSalesReps = New System.Data.DataView
        Me.txtLDJobDescription = New DevExpress.XtraEditors.MRUEdit
        Me.cmHistory = New System.Windows.Forms.ContextMenu
        Me.miClearHistory = New System.Windows.Forms.MenuItem
        Me.chkLDQuoteReceived = New DevExpress.XtraEditors.CheckEdit
        Me.dpLDDate = New DevExpress.XtraEditors.DateEdit
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupLine1 = New Power.Forms.GroupLine
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupLine2 = New Power.Forms.GroupLine
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.GroupLine3 = New Power.Forms.GroupLine
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.GroupLine6 = New Power.Forms.GroupLine
        Me.Label28 = New System.Windows.Forms.Label
        Me.Label29 = New System.Windows.Forms.Label
        Me.Label31 = New System.Windows.Forms.Label
        Me.GroupLine7 = New Power.Forms.GroupLine
        Me.pnlMain = New DevExpress.XtraEditors.PanelControl
        Me.pnlScheduling = New DevExpress.XtraEditors.GroupControl
        Me.Label16 = New System.Windows.Forms.Label
        Me.btnViewCalendar = New DevExpress.XtraEditors.SimpleButton
        Me.GroupLine9 = New Power.Forms.GroupLine
        Me.GroupLine8 = New Power.Forms.GroupLine
        Me.btnAddAppointment = New DevExpress.XtraEditors.SimpleButton
        Me.btnEditAppointment = New DevExpress.XtraEditors.SimpleButton
        Me.btnRemoveAppointment = New DevExpress.XtraEditors.SimpleButton
        Me.Label19 = New System.Windows.Forms.Label
        Me.dgAppointments = New DevExpress.XtraGrid.GridControl
        Me.dvAppointments = New System.Data.DataView
        Me.DsCalendar = New WindowsApplication.dsCalendar
        Me.gvAppointments = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colAPBegin = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colEXName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.pnlQuoteRequestInformation = New DevExpress.XtraEditors.GroupControl
        Me.Label20 = New System.Windows.Forms.Label
        Me.rgLDQuoteRequested = New DevExpress.XtraEditors.RadioGroup
        Me.pnlNotes = New DevExpress.XtraEditors.GroupControl
        Me.dgNotes = New DevExpress.XtraGrid.GridControl
        Me.DsNotes = New WindowsApplication.dsNotes
        Me.gvNotes = New DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
        Me.GridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.colINUser = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.txtEXID = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.dvExpenses2 = New System.Data.DataView
        Me.colINDate = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.txtINDate = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.colINNotes = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.txtINNotes = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
        Me.colINFollowUpText = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.txtINFollowUpText = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
        Me.pnlJobAddress = New DevExpress.XtraEditors.GroupControl
        Me.lblJobPostCode = New System.Windows.Forms.Label
        Me.txtLDJobStreetAddress01 = New DevExpress.XtraEditors.TextEdit
        Me.rgLDJobAddressAsAbove = New DevExpress.XtraEditors.RadioGroup
        Me.Label18 = New System.Windows.Forms.Label
        Me.lblJobState = New System.Windows.Forms.Label
        Me.lblJobStreetAddress = New System.Windows.Forms.Label
        Me.lblJobSuburb = New System.Windows.Forms.Label
        Me.txtLDJobStreetAddress02 = New DevExpress.XtraEditors.TextEdit
        Me.txtLDJobSuburb = New DevExpress.XtraEditors.TextEdit
        Me.txtLDJobState = New DevExpress.XtraEditors.TextEdit
        Me.txtLDJobPostCode = New DevExpress.XtraEditors.TextEdit
        Me.pnlEnquirySources = New DevExpress.XtraEditors.GroupControl
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colSSName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSSUserSelected = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.GroupLine4 = New Power.Forms.GroupLine
        Me.Label24 = New System.Windows.Forms.Label
        Me.GroupLine5 = New Power.Forms.GroupLine
        Me.Label25 = New System.Windows.Forms.Label
        Me.txtLSName = New DevExpress.XtraEditors.LookUpEdit
        Me.pnlEnquiryInformation = New DevExpress.XtraEditors.GroupControl
        Me.txtLDUser = New DevExpress.XtraEditors.LookUpEdit
        Me.dvExpenses = New System.Data.DataView
        Me.pnlClientInformation = New DevExpress.XtraEditors.GroupControl
        Me.rgLDUseContactFalse = New DevExpress.XtraEditors.RadioGroup
        Me.Label22 = New System.Windows.Forms.Label
        Me.pceContact = New DevExpress.XtraEditors.PopupContainerEdit
        Me.pccContacts = New DevExpress.XtraEditors.PopupContainerControl
        Me.Label21 = New System.Windows.Forms.Label
        Me.txtLDClientWorkPhoneNumber = New DevExpress.XtraEditors.TextEdit
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.txtLDClientEmail = New DevExpress.XtraEditors.TextEdit
        Me.txtLDClientSurname = New DevExpress.XtraEditors.TextEdit
        Me.GroupLine19 = New Power.Forms.GroupLine
        Me.GroupLine17 = New Power.Forms.GroupLine
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.GroupLine18 = New Power.Forms.GroupLine
        Me.Label30 = New System.Windows.Forms.Label
        Me.txtLDClientFirstName = New DevExpress.XtraEditors.TextEdit
        Me.txtLDReferenceName = New DevExpress.XtraEditors.TextEdit
        Me.txtLDClientStreetAddress01 = New DevExpress.XtraEditors.TextEdit
        Me.txtLDClientStreetAddress02 = New DevExpress.XtraEditors.TextEdit
        Me.txtLDClientSuburb = New DevExpress.XtraEditors.TextEdit
        Me.txtLDClientState = New DevExpress.XtraEditors.TextEdit
        Me.txtLDClientPostCode = New DevExpress.XtraEditors.TextEdit
        Me.txtLDClientHomePhoneNumber = New DevExpress.XtraEditors.TextEdit
        Me.txtLDClientFaxNumber = New DevExpress.XtraEditors.TextEdit
        Me.txtLDClientMobileNumber = New DevExpress.XtraEditors.TextEdit
        Me.rgLDUseContactTrue = New DevExpress.XtraEditors.RadioGroup
        Me.daLeadSources = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton
        Me.daLeadsSubSources = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand5 = New System.Data.SqlClient.SqlCommand
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider
        Me.daTasks = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand2 = New System.Data.SqlClient.SqlCommand
        Me.Storage = New DevExpress.XtraScheduler.SchedulerStorage(Me.components)
        Me.dvResources = New System.Data.DataView
        Me.daIDNotes = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand6 = New System.Data.SqlClient.SqlCommand
        Me.cmdLeadsSubSources_init = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand
        Me.daINFollowUpTypes = New System.Data.SqlClient.SqlDataAdapter
        Me.daExpenses = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.daAppointments = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand6 = New System.Data.SqlClient.SqlCommand
        Me.daContacts = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEXIDRep.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvSalesReps, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDJobDescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkLDQuoteReceived.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dpLDDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMain.SuspendLayout()
        CType(Me.pnlScheduling, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlScheduling.SuspendLayout()
        CType(Me.dgAppointments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvAppointments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsCalendar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvAppointments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlQuoteRequestInformation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlQuoteRequestInformation.SuspendLayout()
        CType(Me.rgLDQuoteRequested.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlNotes.SuspendLayout()
        CType(Me.dgNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEXID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvExpenses2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtINDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtINNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtINFollowUpText, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlJobAddress, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlJobAddress.SuspendLayout()
        CType(Me.txtLDJobStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgLDJobAddressAsAbove.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDJobStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDJobSuburb.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDJobState.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDJobPostCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlEnquirySources, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlEnquirySources.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLSName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlEnquiryInformation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlEnquiryInformation.SuspendLayout()
        CType(Me.txtLDUser.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvExpenses, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlClientInformation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlClientInformation.SuspendLayout()
        CType(Me.rgLDUseContactFalse.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pceContact.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pccContacts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientWorkPhoneNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientFirstName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDReferenceName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientSuburb.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientState.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientPostCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientHomePhoneNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientFaxNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientMobileNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgLDUseContactTrue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Storage, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvResources, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dataSet
        '
        Me.dataSet.DataSetName = "dsLeads"
        Me.dataSet.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(688, 536)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(608, 536)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 3
        Me.btnOK.Text = "OK"
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;user id=sa;integrated security=SSPI;data sou" & _
        "rce=""SERVER\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Leads", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("LDID", "LDID"), New System.Data.Common.DataColumnMapping("ID", "ID"), New System.Data.Common.DataColumnMapping("LDDate", "LDDate"), New System.Data.Common.DataColumnMapping("LDClientFirstName", "LDClientFirstName"), New System.Data.Common.DataColumnMapping("LDClientSurname", "LDClientSurname"), New System.Data.Common.DataColumnMapping("LDClientName", "LDClientName"), New System.Data.Common.DataColumnMapping("LDClientStreetAddress01", "LDClientStreetAddress01"), New System.Data.Common.DataColumnMapping("LDClientStreetAddress02", "LDClientStreetAddress02"), New System.Data.Common.DataColumnMapping("LDClientSuburb", "LDClientSuburb"), New System.Data.Common.DataColumnMapping("LDClientState", "LDClientState"), New System.Data.Common.DataColumnMapping("LDClientPostCode", "LDClientPostCode"), New System.Data.Common.DataColumnMapping("LDClientAddress", "LDClientAddress"), New System.Data.Common.DataColumnMapping("LDClientHomePhoneNumber", "LDClientHomePhoneNumber"), New System.Data.Common.DataColumnMapping("LDClientWorkPhoneNumber", "LDClientWorkPhoneNumber"), New System.Data.Common.DataColumnMapping("LDClientFaxNumber", "LDClientFaxNumber"), New System.Data.Common.DataColumnMapping("LDClientMobileNumber", "LDClientMobileNumber"), New System.Data.Common.DataColumnMapping("LDJobAddressAsAbove", "LDJobAddressAsAbove"), New System.Data.Common.DataColumnMapping("LDJobStreetAddress01", "LDJobStreetAddress01"), New System.Data.Common.DataColumnMapping("LDJobStreetAddress02", "LDJobStreetAddress02"), New System.Data.Common.DataColumnMapping("LDJobSuburb", "LDJobSuburb"), New System.Data.Common.DataColumnMapping("LDJobState", "LDJobState"), New System.Data.Common.DataColumnMapping("LDJobPostCode", "LDJobPostCode"), New System.Data.Common.DataColumnMapping("LDJobAddress", "LDJobAddress"), New System.Data.Common.DataColumnMapping("LDReferenceName", "LDReferenceName"), New System.Data.Common.DataColumnMapping("LSID", "LSID"), New System.Data.Common.DataColumnMapping("LDHasAppointments", "LDHasAppointments"), New System.Data.Common.DataColumnMapping("LDUser", "LDUser"), New System.Data.Common.DataColumnMapping("LDJobDescription", "LDJobDescription"), New System.Data.Common.DataColumnMapping("EXIDRep", "EXIDRep"), New System.Data.Common.DataColumnMapping("LDQuoteReceived", "LDQuoteReceived"), New System.Data.Common.DataColumnMapping("LDClientEmail", "LDClientEmail"), New System.Data.Common.DataColumnMapping("LDQuoteRequested", "LDQuoteRequested"), New System.Data.Common.DataColumnMapping("CTID", "CTID"), New System.Data.Common.DataColumnMapping("LDUseContact", "LDUseContact")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Leads WHERE (BRID = @Original_BRID) AND (LDID = @Original_LDID) AND (" & _
        "CTID = @Original_CTID OR @Original_CTID IS NULL AND CTID IS NULL) AND (EXIDRep =" & _
        " @Original_EXIDRep OR @Original_EXIDRep IS NULL AND EXIDRep IS NULL) AND (ID = @" & _
        "Original_ID) AND (LDClientAddress = @Original_LDClientAddress OR @Original_LDCli" & _
        "entAddress IS NULL AND LDClientAddress IS NULL) AND (LDClientEmail = @Original_L" & _
        "DClientEmail OR @Original_LDClientEmail IS NULL AND LDClientEmail IS NULL) AND (" & _
        "LDClientFaxNumber = @Original_LDClientFaxNumber OR @Original_LDClientFaxNumber I" & _
        "S NULL AND LDClientFaxNumber IS NULL) AND (LDClientFirstName = @Original_LDClien" & _
        "tFirstName OR @Original_LDClientFirstName IS NULL AND LDClientFirstName IS NULL)" & _
        " AND (LDClientHomePhoneNumber = @Original_LDClientHomePhoneNumber OR @Original_L" & _
        "DClientHomePhoneNumber IS NULL AND LDClientHomePhoneNumber IS NULL) AND (LDClien" & _
        "tMobileNumber = @Original_LDClientMobileNumber OR @Original_LDClientMobileNumber" & _
        " IS NULL AND LDClientMobileNumber IS NULL) AND (LDClientName = @Original_LDClien" & _
        "tName OR @Original_LDClientName IS NULL AND LDClientName IS NULL) AND (LDClientP" & _
        "ostCode = @Original_LDClientPostCode OR @Original_LDClientPostCode IS NULL AND L" & _
        "DClientPostCode IS NULL) AND (LDClientState = @Original_LDClientState OR @Origin" & _
        "al_LDClientState IS NULL AND LDClientState IS NULL) AND (LDClientStreetAddress01" & _
        " = @Original_LDClientStreetAddress01 OR @Original_LDClientStreetAddress01 IS NUL" & _
        "L AND LDClientStreetAddress01 IS NULL) AND (LDClientStreetAddress02 = @Original_" & _
        "LDClientStreetAddress02 OR @Original_LDClientStreetAddress02 IS NULL AND LDClien" & _
        "tStreetAddress02 IS NULL) AND (LDClientSuburb = @Original_LDClientSuburb OR @Ori" & _
        "ginal_LDClientSuburb IS NULL AND LDClientSuburb IS NULL) AND (LDClientSurname = " & _
        "@Original_LDClientSurname OR @Original_LDClientSurname IS NULL AND LDClientSurna" & _
        "me IS NULL) AND (LDClientWorkPhoneNumber = @Original_LDClientWorkPhoneNumber OR " & _
        "@Original_LDClientWorkPhoneNumber IS NULL AND LDClientWorkPhoneNumber IS NULL) A" & _
        "ND (LDDate = @Original_LDDate OR @Original_LDDate IS NULL AND LDDate IS NULL) AN" & _
        "D (LDHasAppointments = @Original_LDHasAppointments OR @Original_LDHasAppointment" & _
        "s IS NULL AND LDHasAppointments IS NULL) AND (LDJobAddress = @Original_LDJobAddr" & _
        "ess OR @Original_LDJobAddress IS NULL AND LDJobAddress IS NULL) AND (LDJobAddres" & _
        "sAsAbove = @Original_LDJobAddressAsAbove OR @Original_LDJobAddressAsAbove IS NUL" & _
        "L AND LDJobAddressAsAbove IS NULL) AND (LDJobDescription = @Original_LDJobDescri" & _
        "ption OR @Original_LDJobDescription IS NULL AND LDJobDescription IS NULL) AND (L" & _
        "DJobPostCode = @Original_LDJobPostCode OR @Original_LDJobPostCode IS NULL AND LD" & _
        "JobPostCode IS NULL) AND (LDJobState = @Original_LDJobState OR @Original_LDJobSt" & _
        "ate IS NULL AND LDJobState IS NULL) AND (LDJobStreetAddress01 = @Original_LDJobS" & _
        "treetAddress01 OR @Original_LDJobStreetAddress01 IS NULL AND LDJobStreetAddress0" & _
        "1 IS NULL) AND (LDJobStreetAddress02 = @Original_LDJobStreetAddress02 OR @Origin" & _
        "al_LDJobStreetAddress02 IS NULL AND LDJobStreetAddress02 IS NULL) AND (LDJobSubu" & _
        "rb = @Original_LDJobSuburb OR @Original_LDJobSuburb IS NULL AND LDJobSuburb IS N" & _
        "ULL) AND (LDQuoteReceived = @Original_LDQuoteReceived OR @Original_LDQuoteReceiv" & _
        "ed IS NULL AND LDQuoteReceived IS NULL) AND (LDQuoteRequested = @Original_LDQuot" & _
        "eRequested OR @Original_LDQuoteRequested IS NULL AND LDQuoteRequested IS NULL) A" & _
        "ND (LDReferenceName = @Original_LDReferenceName OR @Original_LDReferenceName IS " & _
        "NULL AND LDReferenceName IS NULL) AND (LDUseContact = @Original_LDUseContact OR " & _
        "@Original_LDUseContact IS NULL AND LDUseContact IS NULL) AND (LDUser = @Original" & _
        "_LDUser OR @Original_LDUser IS NULL AND LDUser IS NULL) AND (LSID = @Original_LS" & _
        "ID OR @Original_LSID IS NULL AND LSID IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDRep", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDRep", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientFirstName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientFirstName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientHomePhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientName", System.Data.SqlDbType.VarChar, 102, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientSurname", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientWorkPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDHasAppointments", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDHasAppointments", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobAddressAsAbove", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobDescription", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDQuoteReceived", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDQuoteReceived", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDQuoteRequested", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDQuoteRequested", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDReferenceName", System.Data.SqlDbType.VarChar, 200, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDReferenceName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDUseContact", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDUseContact", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDUser", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDUser", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LSID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LSID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Leads(BRID, ID, LDDate, LDClientFirstName, LDClientSurname, LDClientS" & _
        "treetAddress01, LDClientStreetAddress02, LDClientSuburb, LDClientState, LDClient" & _
        "PostCode, LDClientHomePhoneNumber, LDClientWorkPhoneNumber, LDClientFaxNumber, L" & _
        "DClientMobileNumber, LDJobAddressAsAbove, LDJobStreetAddress01, LDJobStreetAddre" & _
        "ss02, LDJobSuburb, LDJobState, LDJobPostCode, LDReferenceName, LSID, LDHasAppoin" & _
        "tments, LDUser, LDJobDescription, EXIDRep, LDQuoteReceived, LDClientEmail, LDQuo" & _
        "teRequested, CTID, LDUseContact) VALUES (@BRID, @ID, @LDDate, @LDClientFirstName" & _
        ", @LDClientSurname, @LDClientStreetAddress01, @LDClientStreetAddress02, @LDClien" & _
        "tSuburb, @LDClientState, @LDClientPostCode, @LDClientHomePhoneNumber, @LDClientW" & _
        "orkPhoneNumber, @LDClientFaxNumber, @LDClientMobileNumber, @LDJobAddressAsAbove," & _
        " @LDJobStreetAddress01, @LDJobStreetAddress02, @LDJobSuburb, @LDJobState, @LDJob" & _
        "PostCode, @LDReferenceName, @LSID, @LDHasAppointments, @LDUser, @LDJobDescriptio" & _
        "n, @EXIDRep, @LDQuoteReceived, @LDClientEmail, @LDQuoteRequested, @CTID, @LDUseC" & _
        "ontact); SELECT BRID, LDID, ID, LDDate, LDClientFirstName, LDClientSurname, LDCl" & _
        "ientName, LDClientStreetAddress01, LDClientStreetAddress02, LDClientSuburb, LDCl" & _
        "ientState, LDClientPostCode, LDClientAddress, LDClientHomePhoneNumber, LDClientW" & _
        "orkPhoneNumber, LDClientFaxNumber, LDClientMobileNumber, LDJobAddressAsAbove, LD" & _
        "JobStreetAddress01, LDJobStreetAddress02, LDJobSuburb, LDJobState, LDJobPostCode" & _
        ", LDJobAddress, LDReferenceName, LSID, LDHasAppointments, LDUser, LDJobDescripti" & _
        "on, EXIDRep, LDQuoteReceived, LDClientEmail, LDQuoteRequested, CTID, LDUseContac" & _
        "t FROM Leads WHERE (BRID = @BRID) AND (LDID = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDDate", System.Data.SqlDbType.DateTime, 8, "LDDate"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientFirstName", System.Data.SqlDbType.VarChar, 50, "LDClientFirstName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientSurname", System.Data.SqlDbType.VarChar, 50, "LDClientSurname"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, "LDClientStreetAddress01"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, "LDClientStreetAddress02"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientSuburb", System.Data.SqlDbType.VarChar, 50, "LDClientSuburb"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientState", System.Data.SqlDbType.VarChar, 3, "LDClientState"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientPostCode", System.Data.SqlDbType.VarChar, 20, "LDClientPostCode"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, "LDClientHomePhoneNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, "LDClientWorkPhoneNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientFaxNumber", System.Data.SqlDbType.VarChar, 20, "LDClientFaxNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientMobileNumber", System.Data.SqlDbType.VarChar, 20, "LDClientMobileNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, "LDJobAddressAsAbove"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, "LDJobStreetAddress01"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, "LDJobStreetAddress02"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobSuburb", System.Data.SqlDbType.VarChar, 50, "LDJobSuburb"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobState", System.Data.SqlDbType.VarChar, 3, "LDJobState"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobPostCode", System.Data.SqlDbType.VarChar, 20, "LDJobPostCode"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDReferenceName", System.Data.SqlDbType.VarChar, 200, "LDReferenceName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LSID", System.Data.SqlDbType.Int, 4, "LSID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDHasAppointments", System.Data.SqlDbType.Bit, 1, "LDHasAppointments"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDUser", System.Data.SqlDbType.VarChar, 50, "LDUser"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobDescription", System.Data.SqlDbType.VarChar, 50, "LDJobDescription"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDRep", System.Data.SqlDbType.Int, 4, "EXIDRep"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDQuoteReceived", System.Data.SqlDbType.Bit, 1, "LDQuoteReceived"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientEmail", System.Data.SqlDbType.VarChar, 50, "LDClientEmail"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDQuoteRequested", System.Data.SqlDbType.Bit, 1, "LDQuoteRequested"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTID", System.Data.SqlDbType.BigInt, 8, "CTID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDUseContact", System.Data.SqlDbType.Bit, 1, "LDUseContact"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, LDID, ID, LDDate, LDClientFirstName, LDClientSurname, LDClientName, " & _
        "LDClientStreetAddress01, LDClientStreetAddress02, LDClientSuburb, LDClientState," & _
        " LDClientPostCode, LDClientAddress, LDClientHomePhoneNumber, LDClientWorkPhoneNu" & _
        "mber, LDClientFaxNumber, LDClientMobileNumber, LDJobAddressAsAbove, LDJobStreetA" & _
        "ddress01, LDJobStreetAddress02, LDJobSuburb, LDJobState, LDJobPostCode, LDJobAdd" & _
        "ress, LDReferenceName, LSID, LDHasAppointments, LDUser, LDJobDescription, EXIDRe" & _
        "p, LDQuoteReceived, LDClientEmail, LDQuoteRequested, CTID, LDUseContact FROM Lea" & _
        "ds WHERE (BRID = @BRID) AND (LDID = @LDID)"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDID", System.Data.SqlDbType.BigInt, 8, "LDID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Leads SET BRID = @BRID, ID = @ID, LDDate = @LDDate, LDClientFirstName = @L" & _
        "DClientFirstName, LDClientSurname = @LDClientSurname, LDClientStreetAddress01 = " & _
        "@LDClientStreetAddress01, LDClientStreetAddress02 = @LDClientStreetAddress02, LD" & _
        "ClientSuburb = @LDClientSuburb, LDClientState = @LDClientState, LDClientPostCode" & _
        " = @LDClientPostCode, LDClientHomePhoneNumber = @LDClientHomePhoneNumber, LDClie" & _
        "ntWorkPhoneNumber = @LDClientWorkPhoneNumber, LDClientFaxNumber = @LDClientFaxNu" & _
        "mber, LDClientMobileNumber = @LDClientMobileNumber, LDJobAddressAsAbove = @LDJob" & _
        "AddressAsAbove, LDJobStreetAddress01 = @LDJobStreetAddress01, LDJobStreetAddress" & _
        "02 = @LDJobStreetAddress02, LDJobSuburb = @LDJobSuburb, LDJobState = @LDJobState" & _
        ", LDJobPostCode = @LDJobPostCode, LDReferenceName = @LDReferenceName, LSID = @LS" & _
        "ID, LDHasAppointments = @LDHasAppointments, LDUser = @LDUser, LDJobDescription =" & _
        " @LDJobDescription, EXIDRep = @EXIDRep, LDQuoteReceived = @LDQuoteReceived, LDCl" & _
        "ientEmail = @LDClientEmail, LDQuoteRequested = @LDQuoteRequested, CTID = @CTID, " & _
        "LDUseContact = @LDUseContact WHERE (BRID = @Original_BRID) AND (LDID = @Original" & _
        "_LDID) AND (CTID = @Original_CTID OR @Original_CTID IS NULL AND CTID IS NULL) AN" & _
        "D (EXIDRep = @Original_EXIDRep OR @Original_EXIDRep IS NULL AND EXIDRep IS NULL)" & _
        " AND (ID = @Original_ID) AND (LDClientEmail = @Original_LDClientEmail OR @Origin" & _
        "al_LDClientEmail IS NULL AND LDClientEmail IS NULL) AND (LDClientFaxNumber = @Or" & _
        "iginal_LDClientFaxNumber OR @Original_LDClientFaxNumber IS NULL AND LDClientFaxN" & _
        "umber IS NULL) AND (LDClientFirstName = @Original_LDClientFirstName OR @Original" & _
        "_LDClientFirstName IS NULL AND LDClientFirstName IS NULL) AND (LDClientHomePhone" & _
        "Number = @Original_LDClientHomePhoneNumber OR @Original_LDClientHomePhoneNumber " & _
        "IS NULL AND LDClientHomePhoneNumber IS NULL) AND (LDClientMobileNumber = @Origin" & _
        "al_LDClientMobileNumber OR @Original_LDClientMobileNumber IS NULL AND LDClientMo" & _
        "bileNumber IS NULL) AND (LDClientPostCode = @Original_LDClientPostCode OR @Origi" & _
        "nal_LDClientPostCode IS NULL AND LDClientPostCode IS NULL) AND (LDClientState = " & _
        "@Original_LDClientState OR @Original_LDClientState IS NULL AND LDClientState IS " & _
        "NULL) AND (LDClientStreetAddress01 = @Original_LDClientStreetAddress01 OR @Origi" & _
        "nal_LDClientStreetAddress01 IS NULL AND LDClientStreetAddress01 IS NULL) AND (LD" & _
        "ClientStreetAddress02 = @Original_LDClientStreetAddress02 OR @Original_LDClientS" & _
        "treetAddress02 IS NULL AND LDClientStreetAddress02 IS NULL) AND (LDClientSuburb " & _
        "= @Original_LDClientSuburb OR @Original_LDClientSuburb IS NULL AND LDClientSubur" & _
        "b IS NULL) AND (LDClientSurname = @Original_LDClientSurname OR @Original_LDClien" & _
        "tSurname IS NULL AND LDClientSurname IS NULL) AND (LDClientWorkPhoneNumber = @Or" & _
        "iginal_LDClientWorkPhoneNumber OR @Original_LDClientWorkPhoneNumber IS NULL AND " & _
        "LDClientWorkPhoneNumber IS NULL) AND (LDDate = @Original_LDDate OR @Original_LDD" & _
        "ate IS NULL AND LDDate IS NULL) AND (LDHasAppointments = @Original_LDHasAppointm" & _
        "ents OR @Original_LDHasAppointments IS NULL AND LDHasAppointments IS NULL) AND (" & _
        "LDJobAddressAsAbove = @Original_LDJobAddressAsAbove OR @Original_LDJobAddressAsA" & _
        "bove IS NULL AND LDJobAddressAsAbove IS NULL) AND (LDJobDescription = @Original_" & _
        "LDJobDescription OR @Original_LDJobDescription IS NULL AND LDJobDescription IS N" & _
        "ULL) AND (LDJobPostCode = @Original_LDJobPostCode OR @Original_LDJobPostCode IS " & _
        "NULL AND LDJobPostCode IS NULL) AND (LDJobState = @Original_LDJobState OR @Origi" & _
        "nal_LDJobState IS NULL AND LDJobState IS NULL) AND (LDJobStreetAddress01 = @Orig" & _
        "inal_LDJobStreetAddress01 OR @Original_LDJobStreetAddress01 IS NULL AND LDJobStr" & _
        "eetAddress01 IS NULL) AND (LDJobStreetAddress02 = @Original_LDJobStreetAddress02" & _
        " OR @Original_LDJobStreetAddress02 IS NULL AND LDJobStreetAddress02 IS NULL) AND" & _
        " (LDJobSuburb = @Original_LDJobSuburb OR @Original_LDJobSuburb IS NULL AND LDJob" & _
        "Suburb IS NULL) AND (LDQuoteReceived = @Original_LDQuoteReceived OR @Original_LD" & _
        "QuoteReceived IS NULL AND LDQuoteReceived IS NULL) AND (LDQuoteRequested = @Orig" & _
        "inal_LDQuoteRequested OR @Original_LDQuoteRequested IS NULL AND LDQuoteRequested" & _
        " IS NULL) AND (LDReferenceName = @Original_LDReferenceName OR @Original_LDRefere" & _
        "nceName IS NULL AND LDReferenceName IS NULL) AND (LDUseContact = @Original_LDUse" & _
        "Contact OR @Original_LDUseContact IS NULL AND LDUseContact IS NULL) AND (LDUser " & _
        "= @Original_LDUser OR @Original_LDUser IS NULL AND LDUser IS NULL) AND (LSID = @" & _
        "Original_LSID OR @Original_LSID IS NULL AND LSID IS NULL); SELECT BRID, LDID, ID" & _
        ", LDDate, LDClientFirstName, LDClientSurname, LDClientName, LDClientStreetAddres" & _
        "s01, LDClientStreetAddress02, LDClientSuburb, LDClientState, LDClientPostCode, L" & _
        "DClientAddress, LDClientHomePhoneNumber, LDClientWorkPhoneNumber, LDClientFaxNum" & _
        "ber, LDClientMobileNumber, LDJobAddressAsAbove, LDJobStreetAddress01, LDJobStree" & _
        "tAddress02, LDJobSuburb, LDJobState, LDJobPostCode, LDJobAddress, LDReferenceNam" & _
        "e, LSID, LDHasAppointments, LDUser, LDJobDescription, EXIDRep, LDQuoteReceived, " & _
        "LDClientEmail, LDQuoteRequested, CTID, LDUseContact FROM Leads WHERE (BRID = @BR" & _
        "ID) AND (LDID = @LDID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDDate", System.Data.SqlDbType.DateTime, 8, "LDDate"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientFirstName", System.Data.SqlDbType.VarChar, 50, "LDClientFirstName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientSurname", System.Data.SqlDbType.VarChar, 50, "LDClientSurname"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, "LDClientStreetAddress01"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, "LDClientStreetAddress02"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientSuburb", System.Data.SqlDbType.VarChar, 50, "LDClientSuburb"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientState", System.Data.SqlDbType.VarChar, 3, "LDClientState"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientPostCode", System.Data.SqlDbType.VarChar, 20, "LDClientPostCode"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, "LDClientHomePhoneNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, "LDClientWorkPhoneNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientFaxNumber", System.Data.SqlDbType.VarChar, 20, "LDClientFaxNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientMobileNumber", System.Data.SqlDbType.VarChar, 20, "LDClientMobileNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, "LDJobAddressAsAbove"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, "LDJobStreetAddress01"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, "LDJobStreetAddress02"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobSuburb", System.Data.SqlDbType.VarChar, 50, "LDJobSuburb"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobState", System.Data.SqlDbType.VarChar, 3, "LDJobState"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobPostCode", System.Data.SqlDbType.VarChar, 20, "LDJobPostCode"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDReferenceName", System.Data.SqlDbType.VarChar, 200, "LDReferenceName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LSID", System.Data.SqlDbType.Int, 4, "LSID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDHasAppointments", System.Data.SqlDbType.Bit, 1, "LDHasAppointments"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDUser", System.Data.SqlDbType.VarChar, 50, "LDUser"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobDescription", System.Data.SqlDbType.VarChar, 50, "LDJobDescription"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDRep", System.Data.SqlDbType.Int, 4, "EXIDRep"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDQuoteReceived", System.Data.SqlDbType.Bit, 1, "LDQuoteReceived"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientEmail", System.Data.SqlDbType.VarChar, 50, "LDClientEmail"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDQuoteRequested", System.Data.SqlDbType.Bit, 1, "LDQuoteRequested"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTID", System.Data.SqlDbType.BigInt, 8, "CTID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDUseContact", System.Data.SqlDbType.Bit, 1, "LDUseContact"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDRep", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDRep", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientFirstName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientFirstName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientHomePhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientSurname", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientWorkPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDHasAppointments", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDHasAppointments", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobAddressAsAbove", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobDescription", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDQuoteReceived", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDQuoteReceived", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDQuoteRequested", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDQuoteRequested", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDReferenceName", System.Data.SqlDbType.VarChar, 200, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDReferenceName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDUseContact", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDUseContact", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDUser", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDUser", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LSID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDID", System.Data.SqlDbType.BigInt, 8, "LDID"))
        '
        'ListBox
        '
        Me.ListBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ListBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ListBox.IntegralHeight = False
        Me.ListBox.ItemHeight = 25
        Me.ListBox.Items.AddRange(New Object() {"Customer Information", "Enquiry Information", "Enquiry Sources", "Quote Request Information", "Job Address", "Scheduling", "Notes and Reminders"})
        Me.ListBox.Location = New System.Drawing.Point(8, 8)
        Me.ListBox.Name = "ListBox"
        Me.ListBox.Size = New System.Drawing.Size(160, 521)
        Me.ListBox.TabIndex = 0
        '
        'txtEXIDRep
        '
        Me.txtEXIDRep.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.EXIDRep"))
        Me.txtEXIDRep.Location = New System.Drawing.Point(168, 264)
        Me.txtEXIDRep.Name = "txtEXIDRep"
        '
        'txtEXIDRep.Properties
        '
        Me.txtEXIDRep.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtEXIDRep.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXName", "", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.txtEXIDRep.Properties.DataSource = Me.dvSalesReps
        Me.txtEXIDRep.Properties.DisplayMember = "EXName"
        Me.txtEXIDRep.Properties.NullText = "--- Salesperson not specified ---"
        Me.txtEXIDRep.Properties.ShowFooter = False
        Me.txtEXIDRep.Properties.ShowHeader = False
        Me.txtEXIDRep.Properties.ShowLines = False
        Me.txtEXIDRep.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.txtEXIDRep.Properties.ValueMember = "EXID"
        Me.txtEXIDRep.Size = New System.Drawing.Size(192, 20)
        Me.txtEXIDRep.TabIndex = 2
        '
        'dvSalesReps
        '
        Me.dvSalesReps.RowFilter = "EXAppearInSalesperson = 1"
        Me.dvSalesReps.Sort = "EXName"
        Me.dvSalesReps.Table = Me.dataSet.VExpenses
        '
        'txtLDJobDescription
        '
        Me.txtLDJobDescription.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDJobDescription"))
        Me.txtLDJobDescription.EditValue = ""
        Me.txtLDJobDescription.Location = New System.Drawing.Point(168, 152)
        Me.txtLDJobDescription.Name = "txtLDJobDescription"
        '
        'txtLDJobDescription.Properties
        '
        Me.txtLDJobDescription.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtLDJobDescription.Properties.ContextMenu = Me.cmHistory
        Me.txtLDJobDescription.Properties.MaxItemCount = 15
        Me.txtLDJobDescription.Size = New System.Drawing.Size(192, 20)
        Me.txtLDJobDescription.TabIndex = 1
        '
        'cmHistory
        '
        Me.cmHistory.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.miClearHistory})
        '
        'miClearHistory
        '
        Me.miClearHistory.Index = 0
        Me.miClearHistory.Text = "Clear History"
        '
        'chkLDQuoteReceived
        '
        Me.chkLDQuoteReceived.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDQuoteReceived"))
        Me.chkLDQuoteReceived.Location = New System.Drawing.Point(168, 352)
        Me.chkLDQuoteReceived.Name = "chkLDQuoteReceived"
        '
        'chkLDQuoteReceived.Properties
        '
        Me.chkLDQuoteReceived.Properties.Caption = "Quote received"
        Me.chkLDQuoteReceived.Size = New System.Drawing.Size(128, 18)
        Me.chkLDQuoteReceived.TabIndex = 3
        '
        'dpLDDate
        '
        Me.dpLDDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDDate"))
        Me.dpLDDate.EditValue = New Date(2005, 7, 21, 0, 0, 0, 0)
        Me.dpLDDate.Location = New System.Drawing.Point(152, 80)
        Me.dpLDDate.Name = "dpLDDate"
        '
        'dpLDDate.Properties
        '
        Me.dpLDDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False
        Me.dpLDDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpLDDate.Size = New System.Drawing.Size(152, 20)
        Me.dpLDDate.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(48, 56)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(304, 23)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Please enter the date the enquiry was recieved on."
        '
        'GroupLine1
        '
        Me.GroupLine1.Location = New System.Drawing.Point(16, 32)
        Me.GroupLine1.Name = "GroupLine1"
        Me.GroupLine1.Size = New System.Drawing.Size(336, 16)
        Me.GroupLine1.TabIndex = 0
        Me.GroupLine1.TextString = "Enquiry Date"
        Me.GroupLine1.TextWidth = 70
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(48, 80)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 23)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Enquiry date:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine2
        '
        Me.GroupLine2.Location = New System.Drawing.Point(16, 112)
        Me.GroupLine2.Name = "GroupLine2"
        Me.GroupLine2.Size = New System.Drawing.Size(336, 16)
        Me.GroupLine2.TabIndex = 0
        Me.GroupLine2.TextString = "Recorded by"
        Me.GroupLine2.TextWidth = 70
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(48, 136)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(304, 32)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Please enter the person who recorded this lead."
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(48, 168)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(88, 23)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Recorded by:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine3
        '
        Me.GroupLine3.Location = New System.Drawing.Point(24, 104)
        Me.GroupLine3.Name = "GroupLine3"
        Me.GroupLine3.Size = New System.Drawing.Size(336, 16)
        Me.GroupLine3.TabIndex = 0
        Me.GroupLine3.TextString = "Job Description"
        Me.GroupLine3.TextWidth = 90
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(56, 128)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(296, 16)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "So the salesperson knows what they are going to quote."
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(56, 152)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(88, 23)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Job description:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine6
        '
        Me.GroupLine6.Location = New System.Drawing.Point(24, 192)
        Me.GroupLine6.Name = "GroupLine6"
        Me.GroupLine6.Size = New System.Drawing.Size(336, 16)
        Me.GroupLine6.TabIndex = 0
        Me.GroupLine6.TextString = "Principal Salesperson"
        Me.GroupLine6.TextWidth = 110
        '
        'Label28
        '
        Me.Label28.Location = New System.Drawing.Point(56, 216)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(320, 48)
        Me.Label28.TabIndex = 1
        Me.Label28.Text = "Select the salesperson responsible for this appointment. This salesperson or any " & _
        "other salesperson who may be involved in this appointment can be scheduled in th" & _
        "e ""Scheduling"" section."
        '
        'Label29
        '
        Me.Label29.Location = New System.Drawing.Point(56, 320)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(304, 32)
        Me.Label29.TabIndex = 1
        Me.Label29.Text = "Tick if this salesperson has completed the quote and given it to administration."
        '
        'Label31
        '
        Me.Label31.Location = New System.Drawing.Point(56, 264)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(112, 23)
        Me.Label31.TabIndex = 1
        Me.Label31.Text = "Principal salesperson:"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine7
        '
        Me.GroupLine7.Location = New System.Drawing.Point(24, 296)
        Me.GroupLine7.Name = "GroupLine7"
        Me.GroupLine7.Size = New System.Drawing.Size(336, 16)
        Me.GroupLine7.TabIndex = 0
        Me.GroupLine7.TextString = "Quote Received"
        Me.GroupLine7.TextWidth = 90
        '
        'pnlMain
        '
        Me.pnlMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlMain.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.pnlMain.Controls.Add(Me.pnlScheduling)
        Me.pnlMain.Controls.Add(Me.pnlQuoteRequestInformation)
        Me.pnlMain.Controls.Add(Me.pnlNotes)
        Me.pnlMain.Controls.Add(Me.pnlJobAddress)
        Me.pnlMain.Controls.Add(Me.pnlEnquirySources)
        Me.pnlMain.Controls.Add(Me.pnlEnquiryInformation)
        Me.pnlMain.Controls.Add(Me.pnlClientInformation)
        Me.pnlMain.Location = New System.Drawing.Point(176, 8)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(584, 520)
        Me.pnlMain.TabIndex = 1
        Me.pnlMain.Text = "PanelControl1"
        '
        'pnlScheduling
        '
        Me.pnlScheduling.Controls.Add(Me.Label16)
        Me.pnlScheduling.Controls.Add(Me.btnViewCalendar)
        Me.pnlScheduling.Controls.Add(Me.GroupLine9)
        Me.pnlScheduling.Controls.Add(Me.GroupLine8)
        Me.pnlScheduling.Controls.Add(Me.btnAddAppointment)
        Me.pnlScheduling.Controls.Add(Me.btnEditAppointment)
        Me.pnlScheduling.Controls.Add(Me.btnRemoveAppointment)
        Me.pnlScheduling.Controls.Add(Me.Label19)
        Me.pnlScheduling.Controls.Add(Me.dgAppointments)
        Me.pnlScheduling.Location = New System.Drawing.Point(32, 56)
        Me.pnlScheduling.Name = "pnlScheduling"
        Me.pnlScheduling.Size = New System.Drawing.Size(520, 400)
        Me.pnlScheduling.TabIndex = 19
        Me.pnlScheduling.Text = "Scheduling"
        '
        'Label16
        '
        Me.Label16.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label16.Location = New System.Drawing.Point(16, 80)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(480, 32)
        Me.Label16.TabIndex = 18
        Me.Label16.Text = "You can schedule appointments one of two ways.  You can interact directly with th" & _
        "e calendar by clicking 'View Calendar...' or you may enter into the list below b" & _
        "y clicking 'Add...'."
        '
        'btnViewCalendar
        '
        Me.btnViewCalendar.Location = New System.Drawing.Point(40, 144)
        Me.btnViewCalendar.Name = "btnViewCalendar"
        Me.btnViewCalendar.Size = New System.Drawing.Size(136, 23)
        Me.btnViewCalendar.TabIndex = 0
        Me.btnViewCalendar.Text = "View Calendar..."
        '
        'GroupLine9
        '
        Me.GroupLine9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupLine9.Location = New System.Drawing.Point(16, 184)
        Me.GroupLine9.Name = "GroupLine9"
        Me.GroupLine9.Size = New System.Drawing.Size(480, 16)
        Me.GroupLine9.TabIndex = 16
        Me.GroupLine9.TextString = "List View"
        Me.GroupLine9.TextWidth = 50
        '
        'GroupLine8
        '
        Me.GroupLine8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupLine8.Location = New System.Drawing.Point(16, 120)
        Me.GroupLine8.Name = "GroupLine8"
        Me.GroupLine8.Size = New System.Drawing.Size(480, 16)
        Me.GroupLine8.TabIndex = 15
        Me.GroupLine8.TextString = "Calendar View"
        Me.GroupLine8.TextWidth = 80
        '
        'btnAddAppointment
        '
        Me.btnAddAppointment.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddAppointment.Image = CType(resources.GetObject("btnAddAppointment.Image"), System.Drawing.Image)
        Me.btnAddAppointment.Location = New System.Drawing.Point(40, 368)
        Me.btnAddAppointment.Name = "btnAddAppointment"
        Me.btnAddAppointment.Size = New System.Drawing.Size(72, 23)
        Me.btnAddAppointment.TabIndex = 2
        Me.btnAddAppointment.Text = "Add..."
        '
        'btnEditAppointment
        '
        Me.btnEditAppointment.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEditAppointment.Image = CType(resources.GetObject("btnEditAppointment.Image"), System.Drawing.Image)
        Me.btnEditAppointment.Location = New System.Drawing.Point(120, 368)
        Me.btnEditAppointment.Name = "btnEditAppointment"
        Me.btnEditAppointment.Size = New System.Drawing.Size(72, 23)
        Me.btnEditAppointment.TabIndex = 3
        Me.btnEditAppointment.Text = "Edit..."
        '
        'btnRemoveAppointment
        '
        Me.btnRemoveAppointment.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveAppointment.Image = CType(resources.GetObject("btnRemoveAppointment.Image"), System.Drawing.Image)
        Me.btnRemoveAppointment.Location = New System.Drawing.Point(200, 368)
        Me.btnRemoveAppointment.Name = "btnRemoveAppointment"
        Me.btnRemoveAppointment.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveAppointment.TabIndex = 4
        Me.btnRemoveAppointment.Text = "Remove"
        '
        'Label19
        '
        Me.Label19.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label19.Location = New System.Drawing.Point(16, 32)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(480, 48)
        Me.Label19.TabIndex = 0
        Me.Label19.Text = "If necessary, this screen is used to schedule an appointment for the salesperson " & _
        "responsible for this quote request. You may schedule more than one salesperson t" & _
        "o this quote request. You may add appointments using the list or calendar view."
        '
        'dgAppointments
        '
        Me.dgAppointments.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgAppointments.DataSource = Me.dvAppointments
        '
        'dgAppointments.EmbeddedNavigator
        '
        Me.dgAppointments.EmbeddedNavigator.Name = ""
        Me.dgAppointments.Location = New System.Drawing.Point(40, 208)
        Me.dgAppointments.MainView = Me.gvAppointments
        Me.dgAppointments.Name = "dgAppointments"
        Me.dgAppointments.Size = New System.Drawing.Size(456, 152)
        Me.dgAppointments.TabIndex = 1
        Me.dgAppointments.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvAppointments})
        '
        'dvAppointments
        '
        Me.dvAppointments.Table = Me.DsCalendar.VAppointments
        '
        'DsCalendar
        '
        Me.DsCalendar.DataSetName = "dsCalendar"
        Me.DsCalendar.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'gvAppointments
        '
        Me.gvAppointments.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.gvAppointments.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvAppointments.Appearance.FocusedCell.Options.UseBackColor = True
        Me.gvAppointments.Appearance.FocusedCell.Options.UseForeColor = True
        Me.gvAppointments.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.gvAppointments.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gvAppointments.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvAppointments.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvAppointments.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colAPBegin, Me.colEXName})
        Me.gvAppointments.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.gvAppointments.GridControl = Me.dgAppointments
        Me.gvAppointments.Name = "gvAppointments"
        Me.gvAppointments.OptionsBehavior.Editable = False
        Me.gvAppointments.OptionsCustomization.AllowFilter = False
        Me.gvAppointments.OptionsNavigation.AutoFocusNewRow = True
        Me.gvAppointments.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.gvAppointments.OptionsView.ShowGroupPanel = False
        Me.gvAppointments.OptionsView.ShowHorzLines = False
        Me.gvAppointments.OptionsView.ShowIndicator = False
        Me.gvAppointments.OptionsView.ShowVertLines = False
        Me.gvAppointments.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colAPBegin, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colAPBegin
        '
        Me.colAPBegin.Caption = "Date"
        Me.colAPBegin.DisplayFormat.FormatString = "d"
        Me.colAPBegin.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colAPBegin.FieldName = "APBegin"
        Me.colAPBegin.Name = "colAPBegin"
        Me.colAPBegin.Visible = True
        Me.colAPBegin.VisibleIndex = 0
        '
        'colEXName
        '
        Me.colEXName.Caption = "Staff Member"
        Me.colEXName.FieldName = "EXName"
        Me.colEXName.Name = "colEXName"
        Me.colEXName.Visible = True
        Me.colEXName.VisibleIndex = 1
        '
        'pnlQuoteRequestInformation
        '
        Me.pnlQuoteRequestInformation.Controls.Add(Me.Label20)
        Me.pnlQuoteRequestInformation.Controls.Add(Me.rgLDQuoteRequested)
        Me.pnlQuoteRequestInformation.Controls.Add(Me.Label5)
        Me.pnlQuoteRequestInformation.Controls.Add(Me.Label6)
        Me.pnlQuoteRequestInformation.Controls.Add(Me.GroupLine6)
        Me.pnlQuoteRequestInformation.Controls.Add(Me.Label28)
        Me.pnlQuoteRequestInformation.Controls.Add(Me.Label29)
        Me.pnlQuoteRequestInformation.Controls.Add(Me.Label31)
        Me.pnlQuoteRequestInformation.Controls.Add(Me.GroupLine7)
        Me.pnlQuoteRequestInformation.Controls.Add(Me.txtLDJobDescription)
        Me.pnlQuoteRequestInformation.Controls.Add(Me.txtEXIDRep)
        Me.pnlQuoteRequestInformation.Controls.Add(Me.chkLDQuoteReceived)
        Me.pnlQuoteRequestInformation.Controls.Add(Me.GroupLine3)
        Me.pnlQuoteRequestInformation.Location = New System.Drawing.Point(16, 40)
        Me.pnlQuoteRequestInformation.Name = "pnlQuoteRequestInformation"
        Me.pnlQuoteRequestInformation.Size = New System.Drawing.Size(528, 424)
        Me.pnlQuoteRequestInformation.TabIndex = 24
        Me.pnlQuoteRequestInformation.Text = "Quote Request Information"
        '
        'Label20
        '
        Me.Label20.Location = New System.Drawing.Point(24, 48)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(240, 21)
        Me.Label20.TabIndex = 28
        Me.Label20.Text = "Has a quote been requested by the customer?"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rgLDQuoteRequested
        '
        Me.rgLDQuoteRequested.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDQuoteRequested"))
        Me.rgLDQuoteRequested.Location = New System.Drawing.Point(264, 32)
        Me.rgLDQuoteRequested.Name = "rgLDQuoteRequested"
        '
        'rgLDQuoteRequested.Properties
        '
        Me.rgLDQuoteRequested.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.rgLDQuoteRequested.Properties.Appearance.Options.UseBackColor = True
        Me.rgLDQuoteRequested.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.rgLDQuoteRequested.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "Yes"), New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "No")})
        Me.rgLDQuoteRequested.Size = New System.Drawing.Size(64, 56)
        Me.rgLDQuoteRequested.TabIndex = 0
        '
        'pnlNotes
        '
        Me.pnlNotes.Controls.Add(Me.dgNotes)
        Me.pnlNotes.Location = New System.Drawing.Point(48, 24)
        Me.pnlNotes.Name = "pnlNotes"
        Me.pnlNotes.Size = New System.Drawing.Size(512, 424)
        Me.pnlNotes.TabIndex = 23
        Me.pnlNotes.Text = "Notes and Reminders"
        '
        'dgNotes
        '
        Me.dgNotes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgNotes.DataSource = Me.DsNotes.IDNotes
        '
        'dgNotes.EmbeddedNavigator
        '
        Me.dgNotes.EmbeddedNavigator.Name = ""
        Me.dgNotes.Location = New System.Drawing.Point(16, 32)
        Me.dgNotes.MainView = Me.gvNotes
        Me.dgNotes.Name = "dgNotes"
        Me.dgNotes.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtEXID, Me.txtINDate, Me.txtINFollowUpText, Me.txtINNotes})
        Me.dgNotes.Size = New System.Drawing.Size(480, 376)
        Me.dgNotes.TabIndex = 0
        Me.dgNotes.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvNotes})
        '
        'DsNotes
        '
        Me.DsNotes.DataSetName = "dsNotes"
        Me.DsNotes.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'gvNotes
        '
        Me.gvNotes.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvNotes.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvNotes.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvNotes.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvNotes.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvNotes.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvNotes.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvNotes.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvNotes.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvNotes.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvNotes.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvNotes.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvNotes.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvNotes.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvNotes.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvNotes.Appearance.VertLine.Options.UseBackColor = True
        Me.gvNotes.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand1})
        Me.gvNotes.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.colINUser, Me.colINDate, Me.colINNotes, Me.colINFollowUpText})
        Me.gvNotes.GridControl = Me.dgNotes
        Me.gvNotes.Name = "gvNotes"
        Me.gvNotes.NewItemRowText = "Type here to add a new row"
        Me.gvNotes.OptionsCustomization.AllowFilter = False
        Me.gvNotes.OptionsCustomization.AllowRowSizing = True
        Me.gvNotes.OptionsView.ColumnAutoWidth = True
        Me.gvNotes.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvNotes.OptionsView.ShowGroupPanel = False
        Me.gvNotes.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colINDate, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'GridBand1
        '
        Me.GridBand1.Caption = "GridBand1"
        Me.GridBand1.Columns.Add(Me.colINUser)
        Me.GridBand1.Columns.Add(Me.colINDate)
        Me.GridBand1.Columns.Add(Me.colINNotes)
        Me.GridBand1.Columns.Add(Me.colINFollowUpText)
        Me.GridBand1.Name = "GridBand1"
        Me.GridBand1.OptionsBand.ShowCaption = False
        Me.GridBand1.Width = 466
        '
        'colINUser
        '
        Me.colINUser.Caption = "User"
        Me.colINUser.ColumnEdit = Me.txtEXID
        Me.colINUser.FieldName = "INUser"
        Me.colINUser.Name = "colINUser"
        Me.colINUser.Visible = True
        Me.colINUser.Width = 120
        '
        'txtEXID
        '
        Me.txtEXID.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXName")})
        Me.txtEXID.DataSource = Me.dvExpenses2
        Me.txtEXID.DisplayMember = "EXName"
        Me.txtEXID.Name = "txtEXID"
        Me.txtEXID.NullText = ""
        Me.txtEXID.ShowFooter = False
        Me.txtEXID.ShowHeader = False
        Me.txtEXID.ShowLines = False
        Me.txtEXID.ValueMember = "EXName"
        '
        'dvExpenses2
        '
        Me.dvExpenses2.RowFilter = "EGType <> 'OE'"
        Me.dvExpenses2.Sort = "EXName"
        Me.dvExpenses2.Table = Me.DsNotes.VExpenses
        '
        'colINDate
        '
        Me.colINDate.Caption = "Date"
        Me.colINDate.ColumnEdit = Me.txtINDate
        Me.colINDate.FieldName = "INDate"
        Me.colINDate.Name = "colINDate"
        Me.colINDate.Visible = True
        Me.colINDate.Width = 96
        '
        'txtINDate
        '
        Me.txtINDate.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtINDate.Name = "txtINDate"
        '
        'colINNotes
        '
        Me.colINNotes.Caption = "Notes"
        Me.colINNotes.ColumnEdit = Me.txtINNotes
        Me.colINNotes.FieldName = "INNotes"
        Me.colINNotes.Name = "colINNotes"
        Me.colINNotes.Visible = True
        Me.colINNotes.Width = 250
        '
        'txtINNotes
        '
        Me.txtINNotes.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.txtINNotes.Name = "txtINNotes"
        Me.txtINNotes.ReadOnly = True
        Me.txtINNotes.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        '
        'colINFollowUpText
        '
        Me.colINFollowUpText.Caption = "Follow Up Information"
        Me.colINFollowUpText.ColumnEdit = Me.txtINFollowUpText
        Me.colINFollowUpText.FieldName = "INFollowUpText"
        Me.colINFollowUpText.Name = "colINFollowUpText"
        Me.colINFollowUpText.RowIndex = 1
        Me.colINFollowUpText.Visible = True
        Me.colINFollowUpText.Width = 466
        '
        'txtINFollowUpText
        '
        Me.txtINFollowUpText.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.txtINFollowUpText.Name = "txtINFollowUpText"
        Me.txtINFollowUpText.ReadOnly = True
        Me.txtINFollowUpText.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        '
        'pnlJobAddress
        '
        Me.pnlJobAddress.Controls.Add(Me.lblJobPostCode)
        Me.pnlJobAddress.Controls.Add(Me.txtLDJobStreetAddress01)
        Me.pnlJobAddress.Controls.Add(Me.rgLDJobAddressAsAbove)
        Me.pnlJobAddress.Controls.Add(Me.Label18)
        Me.pnlJobAddress.Controls.Add(Me.lblJobState)
        Me.pnlJobAddress.Controls.Add(Me.lblJobStreetAddress)
        Me.pnlJobAddress.Controls.Add(Me.lblJobSuburb)
        Me.pnlJobAddress.Controls.Add(Me.txtLDJobStreetAddress02)
        Me.pnlJobAddress.Controls.Add(Me.txtLDJobSuburb)
        Me.pnlJobAddress.Controls.Add(Me.txtLDJobState)
        Me.pnlJobAddress.Controls.Add(Me.txtLDJobPostCode)
        Me.pnlJobAddress.Location = New System.Drawing.Point(32, 48)
        Me.pnlJobAddress.Name = "pnlJobAddress"
        Me.pnlJobAddress.Size = New System.Drawing.Size(560, 336)
        Me.pnlJobAddress.TabIndex = 20
        Me.pnlJobAddress.Text = "Job Address"
        '
        'lblJobPostCode
        '
        Me.lblJobPostCode.Location = New System.Drawing.Point(304, 248)
        Me.lblJobPostCode.Name = "lblJobPostCode"
        Me.lblJobPostCode.Size = New System.Drawing.Size(96, 21)
        Me.lblJobPostCode.TabIndex = 32
        Me.lblJobPostCode.Text = "ZIP/postal code:"
        Me.lblJobPostCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLDJobStreetAddress01
        '
        Me.txtLDJobStreetAddress01.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDJobStreetAddress01"))
        Me.txtLDJobStreetAddress01.EditValue = ""
        Me.txtLDJobStreetAddress01.Location = New System.Drawing.Point(112, 152)
        Me.txtLDJobStreetAddress01.Name = "txtLDJobStreetAddress01"
        Me.txtLDJobStreetAddress01.Size = New System.Drawing.Size(376, 20)
        Me.txtLDJobStreetAddress01.TabIndex = 1
        '
        'rgLDJobAddressAsAbove
        '
        Me.rgLDJobAddressAsAbove.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDJobAddressAsAbove"))
        Me.rgLDJobAddressAsAbove.Location = New System.Drawing.Point(352, 56)
        Me.rgLDJobAddressAsAbove.Name = "rgLDJobAddressAsAbove"
        '
        'rgLDJobAddressAsAbove.Properties
        '
        Me.rgLDJobAddressAsAbove.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.rgLDJobAddressAsAbove.Properties.Appearance.Options.UseBackColor = True
        Me.rgLDJobAddressAsAbove.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.rgLDJobAddressAsAbove.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "Yes"), New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "No")})
        Me.rgLDJobAddressAsAbove.Size = New System.Drawing.Size(64, 56)
        Me.rgLDJobAddressAsAbove.TabIndex = 0
        '
        'Label18
        '
        Me.Label18.Location = New System.Drawing.Point(24, 72)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(328, 21)
        Me.Label18.TabIndex = 27
        Me.Label18.Text = "Is this job at the customer's billing address (same as previous)?"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobState
        '
        Me.lblJobState.Location = New System.Drawing.Point(24, 248)
        Me.lblJobState.Name = "lblJobState"
        Me.lblJobState.Size = New System.Drawing.Size(80, 21)
        Me.lblJobState.TabIndex = 24
        Me.lblJobState.Text = "State/region:"
        Me.lblJobState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobStreetAddress
        '
        Me.lblJobStreetAddress.Location = New System.Drawing.Point(24, 152)
        Me.lblJobStreetAddress.Name = "lblJobStreetAddress"
        Me.lblJobStreetAddress.Size = New System.Drawing.Size(88, 21)
        Me.lblJobStreetAddress.TabIndex = 26
        Me.lblJobStreetAddress.Text = "Street address:"
        Me.lblJobStreetAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobSuburb
        '
        Me.lblJobSuburb.Location = New System.Drawing.Point(24, 216)
        Me.lblJobSuburb.Name = "lblJobSuburb"
        Me.lblJobSuburb.Size = New System.Drawing.Size(80, 21)
        Me.lblJobSuburb.TabIndex = 23
        Me.lblJobSuburb.Text = "Suburb/town:"
        Me.lblJobSuburb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLDJobStreetAddress02
        '
        Me.txtLDJobStreetAddress02.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDJobStreetAddress02"))
        Me.txtLDJobStreetAddress02.EditValue = ""
        Me.txtLDJobStreetAddress02.Location = New System.Drawing.Point(112, 184)
        Me.txtLDJobStreetAddress02.Name = "txtLDJobStreetAddress02"
        Me.txtLDJobStreetAddress02.Size = New System.Drawing.Size(376, 20)
        Me.txtLDJobStreetAddress02.TabIndex = 2
        '
        'txtLDJobSuburb
        '
        Me.txtLDJobSuburb.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDJobSuburb"))
        Me.txtLDJobSuburb.EditValue = ""
        Me.txtLDJobSuburb.Location = New System.Drawing.Point(112, 216)
        Me.txtLDJobSuburb.Name = "txtLDJobSuburb"
        Me.txtLDJobSuburb.Size = New System.Drawing.Size(376, 20)
        Me.txtLDJobSuburb.TabIndex = 3
        '
        'txtLDJobState
        '
        Me.txtLDJobState.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDJobState"))
        Me.txtLDJobState.EditValue = ""
        Me.txtLDJobState.Location = New System.Drawing.Point(112, 248)
        Me.txtLDJobState.Name = "txtLDJobState"
        Me.txtLDJobState.Size = New System.Drawing.Size(88, 20)
        Me.txtLDJobState.TabIndex = 4
        '
        'txtLDJobPostCode
        '
        Me.txtLDJobPostCode.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDJobPostCode"))
        Me.txtLDJobPostCode.EditValue = ""
        Me.txtLDJobPostCode.Location = New System.Drawing.Point(400, 248)
        Me.txtLDJobPostCode.Name = "txtLDJobPostCode"
        Me.txtLDJobPostCode.Size = New System.Drawing.Size(88, 20)
        Me.txtLDJobPostCode.TabIndex = 5
        '
        'pnlEnquirySources
        '
        Me.pnlEnquirySources.Controls.Add(Me.GridControl1)
        Me.pnlEnquirySources.Controls.Add(Me.Label26)
        Me.pnlEnquirySources.Controls.Add(Me.Label23)
        Me.pnlEnquirySources.Controls.Add(Me.GroupLine4)
        Me.pnlEnquirySources.Controls.Add(Me.Label24)
        Me.pnlEnquirySources.Controls.Add(Me.GroupLine5)
        Me.pnlEnquirySources.Controls.Add(Me.Label25)
        Me.pnlEnquirySources.Controls.Add(Me.txtLSName)
        Me.pnlEnquirySources.Location = New System.Drawing.Point(32, 8)
        Me.pnlEnquirySources.Name = "pnlEnquirySources"
        Me.pnlEnquirySources.Size = New System.Drawing.Size(552, 464)
        Me.pnlEnquirySources.TabIndex = 21
        Me.pnlEnquirySources.Text = "Enquiry Sources"
        '
        'GridControl1
        '
        Me.GridControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GridControl1.DataSource = Me.dataSet.VLeads_SubSources
        '
        'GridControl1.EmbeddedNavigator
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.Location = New System.Drawing.Point(40, 256)
        Me.GridControl1.MainView = Me.GridView2
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(440, 200)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2, Me.GridView1})
        '
        'GridView2
        '
        Me.GridView2.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView2.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView2.Appearance.FocusedRow.Options.UseBackColor = True
        Me.GridView2.Appearance.FocusedRow.Options.UseForeColor = True
        Me.GridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView2.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView2.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView2.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.GridView2.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.GridView2.Appearance.HorzLine.Options.UseBackColor = True
        Me.GridView2.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView2.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView2.Appearance.SelectedRow.Options.UseBackColor = True
        Me.GridView2.Appearance.SelectedRow.Options.UseForeColor = True
        Me.GridView2.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.GridView2.Appearance.VertLine.Options.UseBackColor = True
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSSName, Me.colSSUserSelected})
        Me.GridView2.GridControl = Me.GridControl1
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsCustomization.AllowFilter = False
        Me.GridView2.OptionsView.ShowGroupPanel = False
        Me.GridView2.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colSSName, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colSSName
        '
        Me.colSSName.Caption = "Description"
        Me.colSSName.FieldName = "SSName"
        Me.colSSName.Name = "colSSName"
        Me.colSSName.OptionsColumn.AllowEdit = False
        Me.colSSName.OptionsColumn.AllowFocus = False
        Me.colSSName.Visible = True
        Me.colSSName.VisibleIndex = 0
        Me.colSSName.Width = 399
        '
        'colSSUserSelected
        '
        Me.colSSUserSelected.FieldName = "SSUserSelected"
        Me.colSSUserSelected.Name = "colSSUserSelected"
        Me.colSSUserSelected.Visible = True
        Me.colSSUserSelected.VisibleIndex = 1
        Me.colSSUserSelected.Width = 91
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        '
        'Label26
        '
        Me.Label26.Location = New System.Drawing.Point(40, 232)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(104, 23)
        Me.Label26.TabIndex = 1
        Me.Label26.Text = "Available sources:"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label23
        '
        Me.Label23.Location = New System.Drawing.Point(40, 56)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(440, 32)
        Me.Label23.TabIndex = 1
        Me.Label23.Text = "Primary enquiry sources are preset in the program. Because they are common to all" & _
        " franchisees, group information can be collated. "
        '
        'GroupLine4
        '
        Me.GroupLine4.Location = New System.Drawing.Point(16, 32)
        Me.GroupLine4.Name = "GroupLine4"
        Me.GroupLine4.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine4.TabIndex = 0
        Me.GroupLine4.TextString = "Primary Source"
        Me.GroupLine4.TextWidth = 90
        '
        'Label24
        '
        Me.Label24.Location = New System.Drawing.Point(40, 104)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(128, 23)
        Me.Label24.TabIndex = 1
        Me.Label24.Text = "Primary enquiry source:"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine5
        '
        Me.GroupLine5.Location = New System.Drawing.Point(16, 152)
        Me.GroupLine5.Name = "GroupLine5"
        Me.GroupLine5.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine5.TabIndex = 0
        Me.GroupLine5.TextString = "Secondary Source"
        Me.GroupLine5.TextWidth = 100
        '
        'Label25
        '
        Me.Label25.Location = New System.Drawing.Point(40, 176)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(440, 56)
        Me.Label25.TabIndex = 1
        Me.Label25.Text = "A secondary enquiry source allows you to add any additional information about the" & _
        " primary enquiry source, for market research purposes. For example you may wish " & _
        "to add the name of the shopping center, the name of the person on the display an" & _
        "d what sex they are. "
        '
        'txtLSName
        '
        Me.txtLSName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LSID"))
        Me.txtLSName.Location = New System.Drawing.Point(184, 104)
        Me.txtLSName.Name = "txtLSName"
        '
        'txtLSName.Properties
        '
        Me.txtLSName.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtLSName.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("LSName", "", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.txtLSName.Properties.DataSource = Me.dataSet.LeadSources
        Me.txtLSName.Properties.DisplayMember = "LSName"
        Me.txtLSName.Properties.NullText = ""
        Me.txtLSName.Properties.ShowFooter = False
        Me.txtLSName.Properties.ShowHeader = False
        Me.txtLSName.Properties.ShowLines = False
        Me.txtLSName.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.txtLSName.Properties.ValueMember = "LSID"
        Me.txtLSName.Size = New System.Drawing.Size(200, 20)
        Me.txtLSName.TabIndex = 0
        '
        'pnlEnquiryInformation
        '
        Me.pnlEnquiryInformation.Controls.Add(Me.txtLDUser)
        Me.pnlEnquiryInformation.Controls.Add(Me.dpLDDate)
        Me.pnlEnquiryInformation.Controls.Add(Me.Label1)
        Me.pnlEnquiryInformation.Controls.Add(Me.GroupLine1)
        Me.pnlEnquiryInformation.Controls.Add(Me.Label2)
        Me.pnlEnquiryInformation.Controls.Add(Me.GroupLine2)
        Me.pnlEnquiryInformation.Controls.Add(Me.Label3)
        Me.pnlEnquiryInformation.Controls.Add(Me.Label4)
        Me.pnlEnquiryInformation.Location = New System.Drawing.Point(144, 88)
        Me.pnlEnquiryInformation.Name = "pnlEnquiryInformation"
        Me.pnlEnquiryInformation.Size = New System.Drawing.Size(376, 344)
        Me.pnlEnquiryInformation.TabIndex = 18
        Me.pnlEnquiryInformation.Text = "Enquiry Information"
        '
        'txtLDUser
        '
        Me.txtLDUser.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDUser"))
        Me.txtLDUser.Location = New System.Drawing.Point(144, 168)
        Me.txtLDUser.Name = "txtLDUser"
        '
        'txtLDUser.Properties
        '
        Me.txtLDUser.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtLDUser.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXName", "", 45, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.txtLDUser.Properties.DataSource = Me.dvExpenses
        Me.txtLDUser.Properties.DisplayMember = "EXName"
        Me.txtLDUser.Properties.NullText = ""
        Me.txtLDUser.Properties.ShowFooter = False
        Me.txtLDUser.Properties.ShowHeader = False
        Me.txtLDUser.Properties.ShowLines = False
        Me.txtLDUser.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.txtLDUser.Properties.ValueMember = "EXName"
        Me.txtLDUser.Size = New System.Drawing.Size(208, 20)
        Me.txtLDUser.TabIndex = 1
        '
        'dvExpenses
        '
        Me.dvExpenses.RowFilter = "EGType <> 'OE'"
        Me.dvExpenses.Sort = "EXName"
        Me.dvExpenses.Table = Me.dataSet.VExpenses
        '
        'pnlClientInformation
        '
        Me.pnlClientInformation.Controls.Add(Me.rgLDUseContactFalse)
        Me.pnlClientInformation.Controls.Add(Me.Label22)
        Me.pnlClientInformation.Controls.Add(Me.pceContact)
        Me.pnlClientInformation.Controls.Add(Me.Label21)
        Me.pnlClientInformation.Controls.Add(Me.txtLDClientWorkPhoneNumber)
        Me.pnlClientInformation.Controls.Add(Me.Label10)
        Me.pnlClientInformation.Controls.Add(Me.Label17)
        Me.pnlClientInformation.Controls.Add(Me.txtLDClientEmail)
        Me.pnlClientInformation.Controls.Add(Me.txtLDClientSurname)
        Me.pnlClientInformation.Controls.Add(Me.GroupLine19)
        Me.pnlClientInformation.Controls.Add(Me.GroupLine17)
        Me.pnlClientInformation.Controls.Add(Me.Label7)
        Me.pnlClientInformation.Controls.Add(Me.Label8)
        Me.pnlClientInformation.Controls.Add(Me.Label9)
        Me.pnlClientInformation.Controls.Add(Me.Label11)
        Me.pnlClientInformation.Controls.Add(Me.Label12)
        Me.pnlClientInformation.Controls.Add(Me.Label13)
        Me.pnlClientInformation.Controls.Add(Me.Label14)
        Me.pnlClientInformation.Controls.Add(Me.Label15)
        Me.pnlClientInformation.Controls.Add(Me.GroupLine18)
        Me.pnlClientInformation.Controls.Add(Me.Label30)
        Me.pnlClientInformation.Controls.Add(Me.txtLDClientFirstName)
        Me.pnlClientInformation.Controls.Add(Me.txtLDReferenceName)
        Me.pnlClientInformation.Controls.Add(Me.txtLDClientStreetAddress01)
        Me.pnlClientInformation.Controls.Add(Me.txtLDClientStreetAddress02)
        Me.pnlClientInformation.Controls.Add(Me.txtLDClientSuburb)
        Me.pnlClientInformation.Controls.Add(Me.txtLDClientState)
        Me.pnlClientInformation.Controls.Add(Me.txtLDClientPostCode)
        Me.pnlClientInformation.Controls.Add(Me.txtLDClientHomePhoneNumber)
        Me.pnlClientInformation.Controls.Add(Me.txtLDClientFaxNumber)
        Me.pnlClientInformation.Controls.Add(Me.txtLDClientMobileNumber)
        Me.pnlClientInformation.Controls.Add(Me.rgLDUseContactTrue)
        Me.pnlClientInformation.Location = New System.Drawing.Point(40, 0)
        Me.pnlClientInformation.Name = "pnlClientInformation"
        Me.pnlClientInformation.Size = New System.Drawing.Size(552, 520)
        Me.pnlClientInformation.TabIndex = 22
        Me.pnlClientInformation.Text = "Customer Information"
        '
        'rgLDUseContactFalse
        '
        Me.rgLDUseContactFalse.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDUseContact"))
        Me.rgLDUseContactFalse.Location = New System.Drawing.Point(16, 24)
        Me.rgLDUseContactFalse.Name = "rgLDUseContactFalse"
        '
        'rgLDUseContactFalse.Properties
        '
        Me.rgLDUseContactFalse.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.rgLDUseContactFalse.Properties.Appearance.Options.UseBackColor = True
        Me.rgLDUseContactFalse.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.rgLDUseContactFalse.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "I don't want this customer to be in my list of contacts (eg. for one off customer" & _
        "s)")})
        Me.rgLDUseContactFalse.Size = New System.Drawing.Size(496, 24)
        Me.rgLDUseContactFalse.TabIndex = 0
        '
        'Label22
        '
        Me.Label22.Location = New System.Drawing.Point(48, 72)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(288, 21)
        Me.Label22.TabIndex = 2
        Me.Label22.Text = "Select an existing contact or create a new contact:"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'pceContact
        '
        Me.pceContact.Location = New System.Drawing.Point(48, 96)
        Me.pceContact.Name = "pceContact"
        '
        'pceContact.Properties
        '
        Me.pceContact.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.pceContact.Properties.CloseOnLostFocus = False
        Me.pceContact.Properties.NullText = "<No contact is selected>"
        Me.pceContact.Properties.PopupControl = Me.pccContacts
        Me.pceContact.Properties.ShowPopupShadow = False
        Me.pceContact.Size = New System.Drawing.Size(464, 20)
        Me.pceContact.TabIndex = 3
        '
        'pccContacts
        '
        Me.pccContacts.Location = New System.Drawing.Point(128, 224)
        Me.pccContacts.Name = "pccContacts"
        Me.pccContacts.Size = New System.Drawing.Size(464, 336)
        Me.pccContacts.TabIndex = 5
        Me.pccContacts.Text = "PopupContainerControl1"
        '
        'Label21
        '
        Me.Label21.Location = New System.Drawing.Point(72, 416)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(80, 21)
        Me.Label21.TabIndex = 34
        Me.Label21.Text = "Work phone:"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLDClientWorkPhoneNumber
        '
        Me.txtLDClientWorkPhoneNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDClientWorkPhoneNumber"))
        Me.txtLDClientWorkPhoneNumber.EditValue = ""
        Me.txtLDClientWorkPhoneNumber.Location = New System.Drawing.Point(216, 416)
        Me.txtLDClientWorkPhoneNumber.Name = "txtLDClientWorkPhoneNumber"
        Me.txtLDClientWorkPhoneNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientWorkPhoneNumber.TabIndex = 13
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(328, 336)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(96, 21)
        Me.Label10.TabIndex = 32
        Me.Label10.Text = "ZIP/postal code:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label17
        '
        Me.Label17.Location = New System.Drawing.Point(72, 488)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(80, 21)
        Me.Label17.TabIndex = 30
        Me.Label17.Text = "Email:"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLDClientEmail
        '
        Me.txtLDClientEmail.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDClientEmail"))
        Me.txtLDClientEmail.EditValue = ""
        Me.txtLDClientEmail.Location = New System.Drawing.Point(216, 488)
        Me.txtLDClientEmail.Name = "txtLDClientEmail"
        Me.txtLDClientEmail.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientEmail.TabIndex = 16
        '
        'txtLDClientSurname
        '
        Me.txtLDClientSurname.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDClientSurname"))
        Me.txtLDClientSurname.EditValue = ""
        Me.txtLDClientSurname.Location = New System.Drawing.Point(216, 160)
        Me.txtLDClientSurname.Name = "txtLDClientSurname"
        Me.txtLDClientSurname.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientSurname.TabIndex = 4
        '
        'GroupLine19
        '
        Me.GroupLine19.Location = New System.Drawing.Point(48, 136)
        Me.GroupLine19.Name = "GroupLine19"
        Me.GroupLine19.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine19.TabIndex = 25
        Me.GroupLine19.TextString = "Name"
        Me.GroupLine19.TextWidth = 35
        '
        'GroupLine17
        '
        Me.GroupLine17.Location = New System.Drawing.Point(48, 240)
        Me.GroupLine17.Name = "GroupLine17"
        Me.GroupLine17.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine17.TabIndex = 27
        Me.GroupLine17.TextString = "Billing Address"
        Me.GroupLine17.TextWidth = 90
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(72, 184)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(72, 21)
        Me.Label7.TabIndex = 20
        Me.Label7.Text = "First name:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(72, 160)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(144, 21)
        Me.Label8.TabIndex = 19
        Me.Label8.Text = "Surname / business name:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(72, 336)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(136, 21)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "State/region:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(72, 264)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(80, 21)
        Me.Label11.TabIndex = 21
        Me.Label11.Text = "Street address:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(72, 312)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(80, 21)
        Me.Label12.TabIndex = 22
        Me.Label12.Text = "Suburb/town:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label13
        '
        Me.Label13.Location = New System.Drawing.Point(72, 392)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(80, 21)
        Me.Label13.TabIndex = 16
        Me.Label13.Text = "Home phone:"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.Location = New System.Drawing.Point(72, 440)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(80, 21)
        Me.Label14.TabIndex = 17
        Me.Label14.Text = "Fax:"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label15
        '
        Me.Label15.Location = New System.Drawing.Point(72, 464)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(80, 21)
        Me.Label15.TabIndex = 14
        Me.Label15.Text = "Mobile:"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine18
        '
        Me.GroupLine18.Location = New System.Drawing.Point(48, 368)
        Me.GroupLine18.Name = "GroupLine18"
        Me.GroupLine18.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine18.TabIndex = 26
        Me.GroupLine18.TextString = "Contact Details"
        Me.GroupLine18.TextWidth = 79
        '
        'Label30
        '
        Me.Label30.Location = New System.Drawing.Point(72, 208)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(96, 21)
        Me.Label30.TabIndex = 15
        Me.Label30.Text = "Reference name:"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLDClientFirstName
        '
        Me.txtLDClientFirstName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDClientFirstName"))
        Me.txtLDClientFirstName.EditValue = ""
        Me.txtLDClientFirstName.Location = New System.Drawing.Point(216, 184)
        Me.txtLDClientFirstName.Name = "txtLDClientFirstName"
        Me.txtLDClientFirstName.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientFirstName.TabIndex = 5
        '
        'txtLDReferenceName
        '
        Me.txtLDReferenceName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDReferenceName"))
        Me.txtLDReferenceName.EditValue = ""
        Me.txtLDReferenceName.Location = New System.Drawing.Point(216, 208)
        Me.txtLDReferenceName.Name = "txtLDReferenceName"
        Me.txtLDReferenceName.Size = New System.Drawing.Size(296, 20)
        Me.txtLDReferenceName.TabIndex = 6
        '
        'txtLDClientStreetAddress01
        '
        Me.txtLDClientStreetAddress01.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDClientStreetAddress01"))
        Me.txtLDClientStreetAddress01.EditValue = ""
        Me.txtLDClientStreetAddress01.Location = New System.Drawing.Point(216, 264)
        Me.txtLDClientStreetAddress01.Name = "txtLDClientStreetAddress01"
        Me.txtLDClientStreetAddress01.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientStreetAddress01.TabIndex = 7
        '
        'txtLDClientStreetAddress02
        '
        Me.txtLDClientStreetAddress02.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDClientStreetAddress02"))
        Me.txtLDClientStreetAddress02.EditValue = ""
        Me.txtLDClientStreetAddress02.Location = New System.Drawing.Point(216, 288)
        Me.txtLDClientStreetAddress02.Name = "txtLDClientStreetAddress02"
        Me.txtLDClientStreetAddress02.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientStreetAddress02.TabIndex = 8
        '
        'txtLDClientSuburb
        '
        Me.txtLDClientSuburb.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDClientSuburb"))
        Me.txtLDClientSuburb.EditValue = ""
        Me.txtLDClientSuburb.Location = New System.Drawing.Point(216, 312)
        Me.txtLDClientSuburb.Name = "txtLDClientSuburb"
        Me.txtLDClientSuburb.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientSuburb.TabIndex = 9
        '
        'txtLDClientState
        '
        Me.txtLDClientState.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDClientState"))
        Me.txtLDClientState.EditValue = ""
        Me.txtLDClientState.Location = New System.Drawing.Point(216, 336)
        Me.txtLDClientState.Name = "txtLDClientState"
        Me.txtLDClientState.Size = New System.Drawing.Size(88, 20)
        Me.txtLDClientState.TabIndex = 10
        '
        'txtLDClientPostCode
        '
        Me.txtLDClientPostCode.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDClientPostCode"))
        Me.txtLDClientPostCode.EditValue = ""
        Me.txtLDClientPostCode.Location = New System.Drawing.Point(424, 336)
        Me.txtLDClientPostCode.Name = "txtLDClientPostCode"
        Me.txtLDClientPostCode.Size = New System.Drawing.Size(88, 20)
        Me.txtLDClientPostCode.TabIndex = 11
        '
        'txtLDClientHomePhoneNumber
        '
        Me.txtLDClientHomePhoneNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDClientHomePhoneNumber"))
        Me.txtLDClientHomePhoneNumber.EditValue = ""
        Me.txtLDClientHomePhoneNumber.Location = New System.Drawing.Point(216, 392)
        Me.txtLDClientHomePhoneNumber.Name = "txtLDClientHomePhoneNumber"
        Me.txtLDClientHomePhoneNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientHomePhoneNumber.TabIndex = 12
        '
        'txtLDClientFaxNumber
        '
        Me.txtLDClientFaxNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDClientFaxNumber"))
        Me.txtLDClientFaxNumber.EditValue = ""
        Me.txtLDClientFaxNumber.Location = New System.Drawing.Point(216, 440)
        Me.txtLDClientFaxNumber.Name = "txtLDClientFaxNumber"
        Me.txtLDClientFaxNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientFaxNumber.TabIndex = 14
        '
        'txtLDClientMobileNumber
        '
        Me.txtLDClientMobileNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDClientMobileNumber"))
        Me.txtLDClientMobileNumber.EditValue = ""
        Me.txtLDClientMobileNumber.Location = New System.Drawing.Point(216, 464)
        Me.txtLDClientMobileNumber.Name = "txtLDClientMobileNumber"
        Me.txtLDClientMobileNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientMobileNumber.TabIndex = 15
        '
        'rgLDUseContactTrue
        '
        Me.rgLDUseContactTrue.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Leads.LDUseContact"))
        Me.rgLDUseContactTrue.Location = New System.Drawing.Point(16, 48)
        Me.rgLDUseContactTrue.Name = "rgLDUseContactTrue"
        '
        'rgLDUseContactTrue.Properties
        '
        Me.rgLDUseContactTrue.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.rgLDUseContactTrue.Properties.Appearance.Options.UseBackColor = True
        Me.rgLDUseContactTrue.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.rgLDUseContactTrue.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "I want this customer to be in my list of contacts (eg. for repeat customers)")})
        Me.rgLDUseContactTrue.Size = New System.Drawing.Size(496, 24)
        Me.rgLDUseContactTrue.TabIndex = 1
        '
        'daLeadSources
        '
        Me.daLeadSources.DeleteCommand = Me.SqlDeleteCommand2
        Me.daLeadSources.InsertCommand = Me.SqlInsertCommand2
        Me.daLeadSources.SelectCommand = Me.SqlSelectCommand2
        Me.daLeadSources.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "LeadSources", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("LSID", "LSID"), New System.Data.Common.DataColumnMapping("LSName", "LSName")})})
        Me.daLeadSources.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM LeadSources WHERE (LSID = @Original_LSID) AND (LSName = @Original_LSN" & _
        "ame OR @Original_LSName IS NULL AND LSName IS NULL)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LSID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LSName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LSName", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO LeadSources(LSID, LSName) VALUES (@LSID, @LSName); SELECT LSID, LSNam" & _
        "e FROM LeadSources WHERE (LSID = @LSID)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LSID", System.Data.SqlDbType.Int, 4, "LSID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LSName", System.Data.SqlDbType.VarChar, 50, "LSName"))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT LSID, LSName FROM LeadSources"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE LeadSources SET LSID = @LSID, LSName = @LSName WHERE (LSID = @Original_LSI" & _
        "D) AND (LSName = @Original_LSName OR @Original_LSName IS NULL AND LSName IS NULL" & _
        "); SELECT LSID, LSName FROM LeadSources WHERE (LSID = @LSID)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LSID", System.Data.SqlDbType.Int, 4, "LSID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LSName", System.Data.SqlDbType.VarChar, 50, "LSName"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LSID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LSName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LSName", System.Data.DataRowVersion.Original, Nothing))
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton1.Image = CType(resources.GetObject("SimpleButton1.Image"), System.Drawing.Image)
        Me.SimpleButton1.Location = New System.Drawing.Point(8, 536)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(72, 23)
        Me.SimpleButton1.TabIndex = 2
        Me.SimpleButton1.Text = "Help"
        '
        'daLeadsSubSources
        '
        Me.daLeadsSubSources.DeleteCommand = Me.SqlDeleteCommand5
        Me.daLeadsSubSources.InsertCommand = Me.SqlInsertCommand5
        Me.daLeadsSubSources.SelectCommand = Me.SqlSelectCommand5
        Me.daLeadsSubSources.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "vLeads_SubSources", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("LDID", "LDID"), New System.Data.Common.DataColumnMapping("SSID", "SSID"), New System.Data.Common.DataColumnMapping("SSUserSelected", "SSUserSelected")})})
        Me.daLeadsSubSources.UpdateCommand = Me.SqlUpdateCommand5
        '
        'SqlDeleteCommand5
        '
        Me.SqlDeleteCommand5.CommandText = "DELETE FROM Leads_SubSources WHERE (BRID = @Original_BRID) AND (LDID = @Original_" & _
        "LDID) AND (SSID = @Original_SSID) AND (SSUserSelected = @Original_SSUserSelected" & _
        " OR @Original_SSUserSelected IS NULL AND SSUserSelected IS NULL)"
        Me.SqlDeleteCommand5.Connection = Me.SqlConnection
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SSID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SSUserSelected", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SSUserSelected", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand5
        '
        Me.SqlInsertCommand5.CommandText = "sp_Insert_Leads_SubSources"
        Me.SqlInsertCommand5.CommandType = System.Data.CommandType.StoredProcedure
        Me.SqlInsertCommand5.Connection = Me.SqlConnection
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDID", System.Data.SqlDbType.BigInt, 8, "LDID"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SSID", System.Data.SqlDbType.Int, 4, "SSID"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SSUserSelected", System.Data.SqlDbType.Bit, 1, "SSUserSelected"))
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT BRID, LDID, SSID, SSUserSelected, SSName FROM VLeads_SubSources WHERE (BRI" & _
        "D = @BRID) AND (LDID = @LDID)"
        Me.SqlSelectCommand5.Connection = Me.SqlConnection
        Me.SqlSelectCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDID", System.Data.SqlDbType.BigInt, 8, "LDID"))
        '
        'SqlUpdateCommand5
        '
        Me.SqlUpdateCommand5.CommandText = "UPDATE Leads_SubSources SET BRID = @BRID, LDID = @LDID, SSID = @SSID, SSUserSelec" & _
        "ted = @SSUserSelected WHERE (BRID = @Original_BRID) AND (LDID = @Original_LDID) " & _
        "AND (SSID = @Original_SSID) AND (SSUserSelected = @Original_SSUserSelected OR @O" & _
        "riginal_SSUserSelected IS NULL AND SSUserSelected IS NULL); SELECT BRID, LDID, S" & _
        "SID, SSUserSelected FROM Leads_SubSources WHERE (BRID = @BRID) AND (LDID = @LDID" & _
        ") AND (SSID = @SSID)"
        Me.SqlUpdateCommand5.Connection = Me.SqlConnection
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDID", System.Data.SqlDbType.BigInt, 8, "LDID"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SSID", System.Data.SqlDbType.Int, 4, "SSID"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SSUserSelected", System.Data.SqlDbType.Bit, 1, "SSUserSelected"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SSID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SSUserSelected", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SSUserSelected", System.Data.DataRowVersion.Original, Nothing))
        '
        'HelpProvider1
        '
        Me.HelpProvider1.HelpNamespace = "C:\Documents and Settings\djpower\My Documents\My Projects\GTMS\Version1\Help\Hel" & _
        "pProject.chm"
        '
        'daTasks
        '
        Me.daTasks.InsertCommand = Me.SqlCommand1
        Me.daTasks.SelectCommand = Me.SqlCommand2
        Me.daTasks.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Tasks", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("TAName", "TAName"), New System.Data.Common.DataColumnMapping("TAControlsJobDate", "TAControlsJobDate"), New System.Data.Common.DataColumnMapping("TAMenuCaption", "TAMenuCaption"), New System.Data.Common.DataColumnMapping("TAColourR", "TAColourR"), New System.Data.Common.DataColumnMapping("TAColourG", "TAColourG"), New System.Data.Common.DataColumnMapping("TAColourB", "TAColourB"), New System.Data.Common.DataColumnMapping("TAID", "TAID")})})
        '
        'SqlCommand1
        '
        Me.SqlCommand1.CommandText = "INSERT INTO Tasks(TAName, TAControlsJobDate, TAMenuCaption, TAColourR, TAColourG," & _
        " TAColourB, TAID) VALUES (@TAName, @TAControlsJobDate, @TAMenuCaption, @TAColour" & _
        "R, @TAColourG, @TAColourB, @TAID); SELECT TAName, TAControlsJobDate, TAMenuCapti" & _
        "on, TAColourR, TAColourG, TAColourB, TAID FROM Tasks ORDER BY TAID"
        Me.SqlCommand1.Connection = Me.SqlConnection
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAName", System.Data.SqlDbType.VarChar, 50, "TAName"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAControlsJobDate", System.Data.SqlDbType.Bit, 1, "TAControlsJobDate"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAMenuCaption", System.Data.SqlDbType.VarChar, 50, "TAMenuCaption"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourR", System.Data.SqlDbType.SmallInt, 2, "TAColourR"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourG", System.Data.SqlDbType.SmallInt, 2, "TAColourG"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourB", System.Data.SqlDbType.SmallInt, 2, "TAColourB"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAID", System.Data.SqlDbType.Int, 4, "TAID"))
        '
        'SqlCommand2
        '
        Me.SqlCommand2.CommandText = "SELECT TAName, TAControlsJobDate, TAMenuCaption, TAColourR, TAColourG, TAColourB," & _
        " TAID FROM Tasks ORDER BY TAID"
        Me.SqlCommand2.Connection = Me.SqlConnection
        '
        'Storage
        '
        Me.Storage.Appointments.DataSource = Me.dvAppointments
        Me.Storage.Appointments.Mappings.AllDay = "APAllDay"
        Me.Storage.Appointments.Mappings.End = "APEnd"
        Me.Storage.Appointments.Mappings.Label = "APTask"
        Me.Storage.Appointments.Mappings.Location = "APSuburb"
        Me.Storage.Appointments.Mappings.ResourceId = "EXID"
        Me.Storage.Appointments.Mappings.Start = "APBegin"
        Me.Storage.Appointments.Mappings.Status = "APStatus"
        Me.Storage.Appointments.Mappings.Subject = "APDescription"
        Me.Storage.Appointments.Statuses.Add(New DevExpress.XtraScheduler.AppointmentStatus(DevExpress.XtraScheduler.AppointmentStatusType.Free, "Free", "&Free"))
        Me.Storage.Appointments.Statuses.Add(New DevExpress.XtraScheduler.AppointmentStatus(DevExpress.XtraScheduler.AppointmentStatusType.Tentative, System.Drawing.Color.FromArgb(CType(74, Byte), CType(135, Byte), CType(226, Byte)), "Tentative", "&Tentative"))
        Me.Storage.Appointments.Statuses.Add(New DevExpress.XtraScheduler.AppointmentStatus(DevExpress.XtraScheduler.AppointmentStatusType.Busy, System.Drawing.Color.FromArgb(CType(74, Byte), CType(135, Byte), CType(226, Byte)), "Busy", "&Busy"))
        Me.Storage.Appointments.Statuses.Add(New DevExpress.XtraScheduler.AppointmentStatus(DevExpress.XtraScheduler.AppointmentStatusType.OutOfOffice, System.Drawing.Color.FromArgb(CType(217, Byte), CType(83, Byte), CType(83, Byte)), "Off Work", "&Off Work"))
        Me.Storage.Resources.DataSource = Me.dvResources
        Me.Storage.Resources.Mappings.Caption = "EXCalendarName"
        Me.Storage.Resources.Mappings.Id = "EXID"
        '
        'dvResources
        '
        Me.dvResources.Sort = "EGName, EXName"
        Me.dvResources.Table = Me.DsCalendar.VExpenses
        '
        'daIDNotes
        '
        Me.daIDNotes.DeleteCommand = Me.SqlDeleteCommand6
        Me.daIDNotes.InsertCommand = Me.SqlInsertCommand6
        Me.daIDNotes.SelectCommand = Me.SqlSelectCommand6
        Me.daIDNotes.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "IDNotes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("INID", "INID"), New System.Data.Common.DataColumnMapping("ID", "ID"), New System.Data.Common.DataColumnMapping("INUser", "INUser"), New System.Data.Common.DataColumnMapping("INDate", "INDate"), New System.Data.Common.DataColumnMapping("INNotes", "INNotes"), New System.Data.Common.DataColumnMapping("INFollowUpType", "INFollowUpType"), New System.Data.Common.DataColumnMapping("INFollowUpDate", "INFollowUpDate"), New System.Data.Common.DataColumnMapping("INFollowUpComplete", "INFollowUpComplete"), New System.Data.Common.DataColumnMapping("EXIDFollowUp", "EXIDFollowUp"), New System.Data.Common.DataColumnMapping("INFollowUpText", "INFollowUpText")})})
        Me.daIDNotes.UpdateCommand = Me.SqlUpdateCommand6
        '
        'SqlDeleteCommand6
        '
        Me.SqlDeleteCommand6.CommandText = "DELETE FROM IDNotes WHERE (BRID = @Original_BRID) AND (INID = @Original_INID) AND" & _
        " (EXIDFollowUp = @Original_EXIDFollowUp OR @Original_EXIDFollowUp IS NULL AND EX" & _
        "IDFollowUp IS NULL) AND (ID = @Original_ID) AND (INDate = @Original_INDate OR @O" & _
        "riginal_INDate IS NULL AND INDate IS NULL) AND (INFollowUpComplete = @Original_I" & _
        "NFollowUpComplete OR @Original_INFollowUpComplete IS NULL AND INFollowUpComplete" & _
        " IS NULL) AND (INFollowUpDate = @Original_INFollowUpDate OR @Original_INFollowUp" & _
        "Date IS NULL AND INFollowUpDate IS NULL) AND (INFollowUpText = @Original_INFollo" & _
        "wUpText OR @Original_INFollowUpText IS NULL AND INFollowUpText IS NULL) AND (INF" & _
        "ollowUpType = @Original_INFollowUpType OR @Original_INFollowUpType IS NULL AND I" & _
        "NFollowUpType IS NULL) AND (INNotes = @Original_INNotes OR @Original_INNotes IS " & _
        "NULL AND INNotes IS NULL) AND (INUser = @Original_INUser OR @Original_INUser IS " & _
        "NULL AND INUser IS NULL)"
        Me.SqlDeleteCommand6.Connection = Me.SqlConnection
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDFollowUp", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDFollowUp", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpComplete", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpComplete", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpText", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpText", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INNotes", System.Data.SqlDbType.VarChar, 1000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INUser", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INUser", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand6
        '
        Me.SqlInsertCommand6.CommandText = "INSERT INTO IDNotes(BRID, ID, INUser, INDate, INNotes, INFollowUpType, INFollowUp" & _
        "Date, INFollowUpComplete, EXIDFollowUp, INFollowUpText) VALUES (@BRID, @ID, @INU" & _
        "ser, @INDate, @INNotes, @INFollowUpType, @INFollowUpDate, @INFollowUpComplete, @" & _
        "EXIDFollowUp, @INFollowUpText); SELECT BRID, INID, ID, INUser, INDate, INNotes, " & _
        "INFollowUpType, INFollowUpDate, INFollowUpComplete, EXIDFollowUp, INFollowUpText" & _
        " FROM IDNotes WHERE (BRID = @BRID) AND (INID = @@IDENTITY)"
        Me.SqlInsertCommand6.Connection = Me.SqlConnection
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INUser", System.Data.SqlDbType.VarChar, 50, "INUser"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INDate", System.Data.SqlDbType.DateTime, 8, "INDate"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INNotes", System.Data.SqlDbType.VarChar, 1000, "INNotes"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpType", System.Data.SqlDbType.VarChar, 2, "INFollowUpType"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpDate", System.Data.SqlDbType.DateTime, 8, "INFollowUpDate"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpComplete", System.Data.SqlDbType.Bit, 1, "INFollowUpComplete"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDFollowUp", System.Data.SqlDbType.Int, 4, "EXIDFollowUp"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpText", System.Data.SqlDbType.VarChar, 100, "INFollowUpText"))
        '
        'SqlSelectCommand6
        '
        Me.SqlSelectCommand6.CommandText = "SELECT BRID, INID, ID, INUser, INDate, INNotes, INFollowUpType, INFollowUpDate, I" & _
        "NFollowUpComplete, EXIDFollowUp, INFollowUpText FROM IDNotes WHERE (BRID = @BRID" & _
        ") AND (ID = @ID)"
        Me.SqlSelectCommand6.Connection = Me.SqlConnection
        Me.SqlSelectCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        '
        'SqlUpdateCommand6
        '
        Me.SqlUpdateCommand6.CommandText = "UPDATE IDNotes SET BRID = @BRID, ID = @ID, INUser = @INUser, INDate = @INDate, IN" & _
        "Notes = @INNotes, INFollowUpType = @INFollowUpType, INFollowUpDate = @INFollowUp" & _
        "Date, INFollowUpComplete = @INFollowUpComplete, EXIDFollowUp = @EXIDFollowUp, IN" & _
        "FollowUpText = @INFollowUpText WHERE (BRID = @Original_BRID) AND (INID = @Origin" & _
        "al_INID) AND (EXIDFollowUp = @Original_EXIDFollowUp OR @Original_EXIDFollowUp IS" & _
        " NULL AND EXIDFollowUp IS NULL) AND (ID = @Original_ID) AND (INDate = @Original_" & _
        "INDate OR @Original_INDate IS NULL AND INDate IS NULL) AND (INFollowUpComplete =" & _
        " @Original_INFollowUpComplete OR @Original_INFollowUpComplete IS NULL AND INFoll" & _
        "owUpComplete IS NULL) AND (INFollowUpDate = @Original_INFollowUpDate OR @Origina" & _
        "l_INFollowUpDate IS NULL AND INFollowUpDate IS NULL) AND (INFollowUpText = @Orig" & _
        "inal_INFollowUpText OR @Original_INFollowUpText IS NULL AND INFollowUpText IS NU" & _
        "LL) AND (INFollowUpType = @Original_INFollowUpType OR @Original_INFollowUpType I" & _
        "S NULL AND INFollowUpType IS NULL) AND (INNotes = @Original_INNotes OR @Original" & _
        "_INNotes IS NULL AND INNotes IS NULL) AND (INUser = @Original_INUser OR @Origina" & _
        "l_INUser IS NULL AND INUser IS NULL); SELECT BRID, INID, ID, INUser, INDate, INN" & _
        "otes, INFollowUpType, INFollowUpDate, INFollowUpComplete, EXIDFollowUp, INFollow" & _
        "UpText FROM IDNotes WHERE (BRID = @BRID) AND (INID = @INID)"
        Me.SqlUpdateCommand6.Connection = Me.SqlConnection
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INUser", System.Data.SqlDbType.VarChar, 50, "INUser"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INDate", System.Data.SqlDbType.DateTime, 8, "INDate"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INNotes", System.Data.SqlDbType.VarChar, 1000, "INNotes"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpType", System.Data.SqlDbType.VarChar, 2, "INFollowUpType"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpDate", System.Data.SqlDbType.DateTime, 8, "INFollowUpDate"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpComplete", System.Data.SqlDbType.Bit, 1, "INFollowUpComplete"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDFollowUp", System.Data.SqlDbType.Int, 4, "EXIDFollowUp"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpText", System.Data.SqlDbType.VarChar, 100, "INFollowUpText"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDFollowUp", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDFollowUp", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpComplete", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpComplete", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpText", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpText", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INNotes", System.Data.SqlDbType.VarChar, 1000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INUser", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INUser", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INID", System.Data.SqlDbType.BigInt, 8, "INID"))
        '
        'cmdLeadsSubSources_init
        '
        Me.cmdLeadsSubSources_init.CommandText = "SELECT BRID, @LDID AS LDID, SSID, 0 AS SSUserSelected, SSName FROM LeadSubSources" & _
        " SS WHERE (BRID = @BRID)"
        Me.cmdLeadsSubSources_init.Connection = Me.SqlConnection
        Me.cmdLeadsSubSources_init.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDID", System.Data.SqlDbType.BigInt, 8, "LDID"))
        Me.cmdLeadsSubSources_init.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT INFollowUpType, INFollowUpTypeDesc FROM INFollowUpTypes WHERE (INFollowUpT" & _
        "ypeEnabled = 1)"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = "INSERT INTO INFollowUpTypes(INFollowUpType, INFollowUpTypeDesc) VALUES (@INFollow" & _
        "UpType, @INFollowUpTypeDesc); SELECT INFollowUpType, INFollowUpTypeDesc FROM INF" & _
        "ollowUpTypes WHERE (INFollowUpType = @INFollowUpType)"
        Me.SqlInsertCommand4.Connection = Me.SqlConnection
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpType", System.Data.SqlDbType.VarChar, 2, "INFollowUpType"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpTypeDesc", System.Data.SqlDbType.VarChar, 50, "INFollowUpTypeDesc"))
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = "UPDATE INFollowUpTypes SET INFollowUpType = @INFollowUpType, INFollowUpTypeDesc =" & _
        " @INFollowUpTypeDesc WHERE (INFollowUpType = @Original_INFollowUpType) AND (INFo" & _
        "llowUpTypeDesc = @Original_INFollowUpTypeDesc OR @Original_INFollowUpTypeDesc IS" & _
        " NULL AND INFollowUpTypeDesc IS NULL); SELECT INFollowUpType, INFollowUpTypeDesc" & _
        " FROM INFollowUpTypes WHERE (INFollowUpType = @INFollowUpType)"
        Me.SqlUpdateCommand4.Connection = Me.SqlConnection
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpType", System.Data.SqlDbType.VarChar, 2, "INFollowUpType"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INFollowUpTypeDesc", System.Data.SqlDbType.VarChar, 50, "INFollowUpTypeDesc"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpTypeDesc", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpTypeDesc", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = "DELETE FROM INFollowUpTypes WHERE (INFollowUpType = @Original_INFollowUpType) AND" & _
        " (INFollowUpTypeDesc = @Original_INFollowUpTypeDesc OR @Original_INFollowUpTypeD" & _
        "esc IS NULL AND INFollowUpTypeDesc IS NULL)"
        Me.SqlDeleteCommand4.Connection = Me.SqlConnection
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INFollowUpTypeDesc", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INFollowUpTypeDesc", System.Data.DataRowVersion.Original, Nothing))
        '
        'daINFollowUpTypes
        '
        Me.daINFollowUpTypes.DeleteCommand = Me.SqlDeleteCommand4
        Me.daINFollowUpTypes.InsertCommand = Me.SqlInsertCommand4
        Me.daINFollowUpTypes.SelectCommand = Me.SqlSelectCommand4
        Me.daINFollowUpTypes.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "INFollowUpTypes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("INFollowUpType", "INFollowUpType"), New System.Data.Common.DataColumnMapping("INFollowUpTypeDesc", "INFollowUpTypeDesc")})})
        Me.daINFollowUpTypes.UpdateCommand = Me.SqlUpdateCommand4
        '
        'daExpenses
        '
        Me.daExpenses.InsertCommand = Me.SqlInsertCommand3
        Me.daExpenses.SelectCommand = Me.SqlSelectCommand3
        Me.daExpenses.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VExpenses", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("EXName", "EXName"), New System.Data.Common.DataColumnMapping("EGType", "EGType"), New System.Data.Common.DataColumnMapping("EXAppearInCalendarRC", "EXAppearInCalendarRC"), New System.Data.Common.DataColumnMapping("EXAppearInCalendarDL", "EXAppearInCalendarDL"), New System.Data.Common.DataColumnMapping("EXCalendarName", "EXCalendarName"), New System.Data.Common.DataColumnMapping("EGName", "EGName"), New System.Data.Common.DataColumnMapping("EXAppearInSalesperson", "EXAppearInSalesperson")})})
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO VExpenses(BRID, EXName, EGType, EXAppearInCalendarRC, EXAppearInCalen" & _
        "darDL, EXCalendarName, EGName, EXAppearInSalesperson) VALUES (@BRID, @EXName, @E" & _
        "GType, @EXAppearInCalendarRC, @EXAppearInCalendarDL, @EXCalendarName, @EGName, @" & _
        "EXAppearInSalesperson); SELECT BRID, EXID, EXName, EGType, EXAppearInCalendarRC," & _
        " EXAppearInCalendarDL, EXCalendarName, EGName, EXAppearInSalesperson FROM VExpen" & _
        "ses"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 2, "EGType"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAppearInCalendarRC", System.Data.SqlDbType.Bit, 1, "EXAppearInCalendarRC"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAppearInCalendarDL", System.Data.SqlDbType.Bit, 1, "EXAppearInCalendarDL"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXCalendarName", System.Data.SqlDbType.VarChar, 103, "EXCalendarName"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGName", System.Data.SqlDbType.VarChar, 50, "EGName"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAppearInSalesperson", System.Data.SqlDbType.Bit, 1, "EXAppearInSalesperson"))
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT BRID, EXID, EXName, EGType, EXAppearInCalendarRC, EXAppearInCalendarDL, EX" & _
        "CalendarName, EGName, EXAppearInSalesperson FROM VExpenses WHERE (EXDiscontinued" & _
        " = 0) AND (BRID = @BRID)"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection
        Me.SqlSelectCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'daAppointments
        '
        Me.daAppointments.DeleteCommand = Me.SqlCommand3
        Me.daAppointments.InsertCommand = Me.SqlCommand4
        Me.daAppointments.SelectCommand = Me.SqlCommand5
        Me.daAppointments.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VAppointments", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("APID", "APID"), New System.Data.Common.DataColumnMapping("APBegin", "APBegin"), New System.Data.Common.DataColumnMapping("APEnd", "APEnd"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("APType", "APType"), New System.Data.Common.DataColumnMapping("APTypeID", "APTypeID"), New System.Data.Common.DataColumnMapping("APNotes", "APNotes"), New System.Data.Common.DataColumnMapping("APStatus", "APStatus"), New System.Data.Common.DataColumnMapping("APTask", "APTask"), New System.Data.Common.DataColumnMapping("APAllDay", "APAllDay")})})
        Me.daAppointments.UpdateCommand = Me.SqlCommand6
        '
        'SqlCommand3
        '
        Me.SqlCommand3.CommandText = "DELETE FROM Appointments WHERE (APID = @Original_APID) AND (BRID = @Original_BRID" &
        ");"


        'Me.SqlCommand3.CommandText = "UPDATE Jobs SET JBScheduledStartDate = NULL, JBScheduledFinishDate = NULL WHERE (JBID in " & " (select APTypeID from Appointments where (APID = @Original_APID)));DELETE FROM Appointments WHERE (APID = " & "@Original_APID) AND (BRID = @Original_BRID);"

        Me.SqlCommand3.Connection = Me.SqlConnection
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlCommand4
        '
        Me.SqlCommand4.CommandText = "INSERT INTO Appointments (BRID, APBegin, APEnd, EXID, APType, APTypeID, APNotes, " & _
        "APStatus, APTask, APAllDay) VALUES (@BRID, @APBegin, @APEnd, @EXID, @APType, @AP" & _
        "TypeID, @APNotes, @APStatus, @APTask, @APAllDay); SELECT BRID, APID, APBegin, AP" & _
        "End, EXID, APType, APTypeID, APNotes, APStatus, APTask, APAllDay, APDescription," & _
        " APClientName, EXName, ID, APJobDescription, APClientHomePhoneNumber, APClientWo" & _
        "rkPhoneNumber, APClientMobileNumber, APSuburb, TAName, JBPriceQuoted, APAddressM" & _
        "ultiline FROM VAppointments WHERE (APID = @@IDENTITY) AND (BRID = @BRID)"
        Me.SqlCommand4.Connection = Me.SqlConnection
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APBegin", System.Data.SqlDbType.DateTime, 8, "APBegin"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APEnd", System.Data.SqlDbType.DateTime, 8, "APEnd"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APType", System.Data.SqlDbType.VarChar, 2, "APType"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTypeID", System.Data.SqlDbType.BigInt, 8, "APTypeID"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APNotes", System.Data.SqlDbType.VarChar, 2000, "APNotes"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APStatus", System.Data.SqlDbType.Int, 4, "APStatus"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTask", System.Data.SqlDbType.Int, 4, "APTask"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APAllDay", System.Data.SqlDbType.Bit, 1, "APAllDay"))
        '
        'SqlCommand5
        '
        'Me.SqlCommand5.CommandText = "SELECT BRID, APID, APBegin, APEnd, EXID, APType, APTypeID, APNotes, APStatus, APT" & _
        '"ask, APAllDay, APDescription, APClientName, EXName, ID, APJobDescription, APClie" & _
        '"ntHomePhoneNumber, APClientWorkPhoneNumber, APClientMobileNumber, APSuburb, TANa" & _
        '"me, JBPriceQuoted, APAddressMultiline FROM VAppointments WHERE (BRID = @BRID) AN" & _
        '"D (APIsCancelled = 0) AND (APBegin < @TO_DATE OR @TO_DATE IS NULL) AND (APEnd > " & _
        '"@FROM_DATE OR @FROM_DATE IS NULL)"



        Me.SqlCommand5.CommandText = "SELECT BRID, APID, APBegin, APEnd, EXID, APType, APTypeID, APNotes, APStatus, APT" &
        "ask, APAllDay, APDescription, APClientName, EXName, ID, APJobDescription, APClie" &
        "ntHomePhoneNumber, APClientWorkPhoneNumber, APClientMobileNumber, APSuburb, TANa" &
        "me, JBPriceQuoted, APAddressMultiline FROM VAppointments WHERE (BRID = @BRID) AN" &
        "D (APIsCancelled = 0)"


        Me.SqlCommand5.Connection = Me.SqlConnection
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TO_DATE", System.Data.SqlDbType.DateTime, 8, "APBegin"))
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FROM_DATE", System.Data.SqlDbType.DateTime, 8, "APEnd"))
        '
        'SqlCommand6
        '
        Me.SqlCommand6.CommandText = "UPDATE Appointments SET BRID = @BRID, APBegin = @APBegin, APEnd = @APEnd, EXID = " & _
        "@EXID, APType = @APType, APTypeID = @APTypeID, APNotes = @APNotes, APStatus = @A" & _
        "PStatus, APTask = @APTask, APAllDay = @APAllDay WHERE (APID = @Original_APID) AN" & _
        "D (BRID = @Original_BRID); SELECT BRID, APID, APBegin, APEnd, EXID, APType, APTy" & _
        "peID, APNotes, APStatus, APTask, APAllDay, APDescription, APClientName, EXName, " & _
        "ID, APJobDescription, APClientHomePhoneNumber, APClientWorkPhoneNumber, APClient" & _
        "MobileNumber, APSuburb, TAName, JBPriceQuoted, APAddressMultiline FROM VAppointm" & _
        "ents WHERE (APID = @APID) AND (BRID = @BRID)"
        Me.SqlCommand6.Connection = Me.SqlConnection
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APBegin", System.Data.SqlDbType.DateTime, 8, "APBegin"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APEnd", System.Data.SqlDbType.DateTime, 8, "APEnd"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APType", System.Data.SqlDbType.VarChar, 2, "APType"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTypeID", System.Data.SqlDbType.BigInt, 8, "APTypeID"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APNotes", System.Data.SqlDbType.VarChar, 2000, "APNotes"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APStatus", System.Data.SqlDbType.Int, 4, "APStatus"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTask", System.Data.SqlDbType.Int, 4, "APTask"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APAllDay", System.Data.SqlDbType.Bit, 1, "APAllDay"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APID", System.Data.SqlDbType.BigInt, 8, "APID"))
        '
        'daContacts
        '
        Me.daContacts.DeleteCommand = Me.SqlDeleteCommand3
        Me.daContacts.InsertCommand = Me.SqlInsertCommand7
        Me.daContacts.SelectCommand = Me.SqlSelectCommand7
        Me.daContacts.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Contacts", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("CTID", "CTID"), New System.Data.Common.DataColumnMapping("CTSurname", "CTSurname"), New System.Data.Common.DataColumnMapping("CTFirstName", "CTFirstName"), New System.Data.Common.DataColumnMapping("CTStreetAddress01", "CTStreetAddress01"), New System.Data.Common.DataColumnMapping("CTStreetAddress02", "CTStreetAddress02"), New System.Data.Common.DataColumnMapping("CTSuburb", "CTSuburb"), New System.Data.Common.DataColumnMapping("CTState", "CTState"), New System.Data.Common.DataColumnMapping("CTPostCode", "CTPostCode"), New System.Data.Common.DataColumnMapping("CTHomePhoneNumber", "CTHomePhoneNumber"), New System.Data.Common.DataColumnMapping("CTWorkPhoneNumber", "CTWorkPhoneNumber"), New System.Data.Common.DataColumnMapping("CTMobileNumber", "CTMobileNumber"), New System.Data.Common.DataColumnMapping("CTFaxNumber", "CTFaxNumber"), New System.Data.Common.DataColumnMapping("CTEmail", "CTEmail"), New System.Data.Common.DataColumnMapping("CTReferenceName", "CTReferenceName"), New System.Data.Common.DataColumnMapping("CTName", "CTName"), New System.Data.Common.DataColumnMapping("CTAddress", "CTAddress"), New System.Data.Common.DataColumnMapping("CTAddressMultiline", "CTAddressMultiline")})})
        Me.daContacts.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM Contacts WHERE (BRID = @Original_BRID) AND (CTID = @Original_CTID) AN" & _
        "D (CTAddress = @Original_CTAddress OR @Original_CTAddress IS NULL AND CTAddress " & _
        "IS NULL) AND (CTAddressMultiline = @Original_CTAddressMultiline OR @Original_CTA" & _
        "ddressMultiline IS NULL AND CTAddressMultiline IS NULL) AND (CTEmail = @Original" & _
        "_CTEmail OR @Original_CTEmail IS NULL AND CTEmail IS NULL) AND (CTFaxNumber = @O" & _
        "riginal_CTFaxNumber OR @Original_CTFaxNumber IS NULL AND CTFaxNumber IS NULL) AN" & _
        "D (CTFirstName = @Original_CTFirstName OR @Original_CTFirstName IS NULL AND CTFi" & _
        "rstName IS NULL) AND (CTHomePhoneNumber = @Original_CTHomePhoneNumber OR @Origin" & _
        "al_CTHomePhoneNumber IS NULL AND CTHomePhoneNumber IS NULL) AND (CTMobileNumber " & _
        "= @Original_CTMobileNumber OR @Original_CTMobileNumber IS NULL AND CTMobileNumbe" & _
        "r IS NULL) AND (CTName = @Original_CTName OR @Original_CTName IS NULL AND CTName" & _
        " IS NULL) AND (CTPostCode = @Original_CTPostCode OR @Original_CTPostCode IS NULL" & _
        " AND CTPostCode IS NULL) AND (CTReferenceName = @Original_CTReferenceName OR @Or" & _
        "iginal_CTReferenceName IS NULL AND CTReferenceName IS NULL) AND (CTState = @Orig" & _
        "inal_CTState OR @Original_CTState IS NULL AND CTState IS NULL) AND (CTStreetAddr" & _
        "ess01 = @Original_CTStreetAddress01 OR @Original_CTStreetAddress01 IS NULL AND C" & _
        "TStreetAddress01 IS NULL) AND (CTStreetAddress02 = @Original_CTStreetAddress02 O" & _
        "R @Original_CTStreetAddress02 IS NULL AND CTStreetAddress02 IS NULL) AND (CTSubu" & _
        "rb = @Original_CTSuburb OR @Original_CTSuburb IS NULL AND CTSuburb IS NULL) AND " & _
        "(CTSurname = @Original_CTSurname OR @Original_CTSurname IS NULL AND CTSurname IS" & _
        " NULL) AND (CTWorkPhoneNumber = @Original_CTWorkPhoneNumber OR @Original_CTWorkP" & _
        "honeNumber IS NULL AND CTWorkPhoneNumber IS NULL)"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTAddressMultiline", System.Data.SqlDbType.VarChar, 261, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTAddressMultiline", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTFirstName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTFirstName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTHomePhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTName", System.Data.SqlDbType.VarChar, 102, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTReferenceName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTReferenceName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTSurname", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTWorkPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand7
        '
        Me.SqlInsertCommand7.CommandText = "INSERT INTO Contacts(BRID, CTSurname, CTFirstName, CTStreetAddress01, CTStreetAdd" & _
        "ress02, CTSuburb, CTState, CTPostCode, CTHomePhoneNumber, CTWorkPhoneNumber, CTM" & _
        "obileNumber, CTFaxNumber, CTEmail, CTReferenceName, CTName, CTAddress, CTAddress" & _
        "Multiline) VALUES (@BRID, @CTSurname, @CTFirstName, @CTStreetAddress01, @CTStree" & _
        "tAddress02, @CTSuburb, @CTState, @CTPostCode, @CTHomePhoneNumber, @CTWorkPhoneNu" & _
        "mber, @CTMobileNumber, @CTFaxNumber, @CTEmail, @CTReferenceName, @CTName, @CTAdd" & _
        "ress, @CTAddressMultiline); SELECT BRID, CTID, CTSurname, CTFirstName, CTStreetA" & _
        "ddress01, CTStreetAddress02, CTSuburb, CTState, CTPostCode, CTHomePhoneNumber, C" & _
        "TWorkPhoneNumber, CTMobileNumber, CTFaxNumber, CTEmail, CTReferenceName, CTName," & _
        " CTAddress, CTAddressMultiline FROM Contacts WHERE (BRID = @BRID) AND (CTID = @@" & _
        "IDENTITY)"
        Me.SqlInsertCommand7.Connection = Me.SqlConnection
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTSurname", System.Data.SqlDbType.VarChar, 50, "CTSurname"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTFirstName", System.Data.SqlDbType.VarChar, 50, "CTFirstName"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTStreetAddress01", System.Data.SqlDbType.VarChar, 100, "CTStreetAddress01"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTStreetAddress02", System.Data.SqlDbType.VarChar, 100, "CTStreetAddress02"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTSuburb", System.Data.SqlDbType.VarChar, 50, "CTSuburb"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTState", System.Data.SqlDbType.VarChar, 3, "CTState"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTPostCode", System.Data.SqlDbType.VarChar, 20, "CTPostCode"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, "CTHomePhoneNumber"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, "CTWorkPhoneNumber"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTMobileNumber", System.Data.SqlDbType.VarChar, 20, "CTMobileNumber"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTFaxNumber", System.Data.SqlDbType.VarChar, 20, "CTFaxNumber"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTEmail", System.Data.SqlDbType.VarChar, 50, "CTEmail"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTReferenceName", System.Data.SqlDbType.VarChar, 100, "CTReferenceName"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTName", System.Data.SqlDbType.VarChar, 102, "CTName"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTAddress", System.Data.SqlDbType.VarChar, 259, "CTAddress"))
        Me.SqlInsertCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTAddressMultiline", System.Data.SqlDbType.VarChar, 261, "CTAddressMultiline"))
        '
        'SqlSelectCommand7
        '
        Me.SqlSelectCommand7.CommandText = "SELECT BRID, CTID, CTSurname, CTFirstName, CTStreetAddress01, CTStreetAddress02, " & _
        "CTSuburb, CTState, CTPostCode, CTHomePhoneNumber, CTWorkPhoneNumber, CTMobileNum" & _
        "ber, CTFaxNumber, CTEmail, CTReferenceName, CTName, CTAddress, CTAddressMultilin" & _
        "e FROM Contacts WHERE (BRID = @BRID) AND (CTID = @CTID)"
        Me.SqlSelectCommand7.Connection = Me.SqlConnection
        Me.SqlSelectCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTID", System.Data.SqlDbType.BigInt, 8, "CTID"))
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE Contacts SET BRID = @BRID, CTSurname = @CTSurname, CTFirstName = @CTFirstN" & _
        "ame, CTStreetAddress01 = @CTStreetAddress01, CTStreetAddress02 = @CTStreetAddres" & _
        "s02, CTSuburb = @CTSuburb, CTState = @CTState, CTPostCode = @CTPostCode, CTHomeP" & _
        "honeNumber = @CTHomePhoneNumber, CTWorkPhoneNumber = @CTWorkPhoneNumber, CTMobil" & _
        "eNumber = @CTMobileNumber, CTFaxNumber = @CTFaxNumber, CTEmail = @CTEmail, CTRef" & _
        "erenceName = @CTReferenceName, CTName = @CTName, CTAddress = @CTAddress, CTAddre" & _
        "ssMultiline = @CTAddressMultiline WHERE (BRID = @Original_BRID) AND (CTID = @Ori" & _
        "ginal_CTID) AND (CTAddress = @Original_CTAddress OR @Original_CTAddress IS NULL " & _
        "AND CTAddress IS NULL) AND (CTAddressMultiline = @Original_CTAddressMultiline OR" & _
        " @Original_CTAddressMultiline IS NULL AND CTAddressMultiline IS NULL) AND (CTEma" & _
        "il = @Original_CTEmail OR @Original_CTEmail IS NULL AND CTEmail IS NULL) AND (CT" & _
        "FaxNumber = @Original_CTFaxNumber OR @Original_CTFaxNumber IS NULL AND CTFaxNumb" & _
        "er IS NULL) AND (CTFirstName = @Original_CTFirstName OR @Original_CTFirstName IS" & _
        " NULL AND CTFirstName IS NULL) AND (CTHomePhoneNumber = @Original_CTHomePhoneNum" & _
        "ber OR @Original_CTHomePhoneNumber IS NULL AND CTHomePhoneNumber IS NULL) AND (C" & _
        "TMobileNumber = @Original_CTMobileNumber OR @Original_CTMobileNumber IS NULL AND" & _
        " CTMobileNumber IS NULL) AND (CTName = @Original_CTName OR @Original_CTName IS N" & _
        "ULL AND CTName IS NULL) AND (CTPostCode = @Original_CTPostCode OR @Original_CTPo" & _
        "stCode IS NULL AND CTPostCode IS NULL) AND (CTReferenceName = @Original_CTRefere" & _
        "nceName OR @Original_CTReferenceName IS NULL AND CTReferenceName IS NULL) AND (C" & _
        "TState = @Original_CTState OR @Original_CTState IS NULL AND CTState IS NULL) AND" & _
        " (CTStreetAddress01 = @Original_CTStreetAddress01 OR @Original_CTStreetAddress01" & _
        " IS NULL AND CTStreetAddress01 IS NULL) AND (CTStreetAddress02 = @Original_CTStr" & _
        "eetAddress02 OR @Original_CTStreetAddress02 IS NULL AND CTStreetAddress02 IS NUL" & _
        "L) AND (CTSuburb = @Original_CTSuburb OR @Original_CTSuburb IS NULL AND CTSuburb" & _
        " IS NULL) AND (CTSurname = @Original_CTSurname OR @Original_CTSurname IS NULL AN" & _
        "D CTSurname IS NULL) AND (CTWorkPhoneNumber = @Original_CTWorkPhoneNumber OR @Or" & _
        "iginal_CTWorkPhoneNumber IS NULL AND CTWorkPhoneNumber IS NULL); SELECT BRID, CT" & _
        "ID, CTSurname, CTFirstName, CTStreetAddress01, CTStreetAddress02, CTSuburb, CTSt" & _
        "ate, CTPostCode, CTHomePhoneNumber, CTWorkPhoneNumber, CTMobileNumber, CTFaxNumb" & _
        "er, CTEmail, CTReferenceName, CTName, CTAddress, CTAddressMultiline FROM Contact" & _
        "s WHERE (BRID = @BRID) AND (CTID = @CTID)"
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTSurname", System.Data.SqlDbType.VarChar, 50, "CTSurname"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTFirstName", System.Data.SqlDbType.VarChar, 50, "CTFirstName"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTStreetAddress01", System.Data.SqlDbType.VarChar, 100, "CTStreetAddress01"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTStreetAddress02", System.Data.SqlDbType.VarChar, 100, "CTStreetAddress02"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTSuburb", System.Data.SqlDbType.VarChar, 50, "CTSuburb"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTState", System.Data.SqlDbType.VarChar, 3, "CTState"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTPostCode", System.Data.SqlDbType.VarChar, 20, "CTPostCode"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, "CTHomePhoneNumber"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, "CTWorkPhoneNumber"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTMobileNumber", System.Data.SqlDbType.VarChar, 20, "CTMobileNumber"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTFaxNumber", System.Data.SqlDbType.VarChar, 20, "CTFaxNumber"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTEmail", System.Data.SqlDbType.VarChar, 50, "CTEmail"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTReferenceName", System.Data.SqlDbType.VarChar, 100, "CTReferenceName"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTName", System.Data.SqlDbType.VarChar, 102, "CTName"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTAddress", System.Data.SqlDbType.VarChar, 259, "CTAddress"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTAddressMultiline", System.Data.SqlDbType.VarChar, 261, "CTAddressMultiline"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTAddressMultiline", System.Data.SqlDbType.VarChar, 261, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTAddressMultiline", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTFirstName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTFirstName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTHomePhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTName", System.Data.SqlDbType.VarChar, 102, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTReferenceName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTReferenceName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTSurname", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTWorkPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTID", System.Data.SqlDbType.BigInt, 8, "CTID"))
        '
        'frmLead2
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(770, 568)
        Me.Controls.Add(Me.ListBox)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.pccContacts)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.SimpleButton1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLead2"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Lead"
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEXIDRep.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvSalesReps, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDJobDescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkLDQuoteReceived.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dpLDDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMain.ResumeLayout(False)
        CType(Me.pnlScheduling, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlScheduling.ResumeLayout(False)
        CType(Me.dgAppointments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvAppointments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsCalendar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvAppointments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlQuoteRequestInformation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlQuoteRequestInformation.ResumeLayout(False)
        CType(Me.rgLDQuoteRequested.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlNotes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlNotes.ResumeLayout(False)
        CType(Me.dgNotes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsNotes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvNotes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEXID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvExpenses2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtINDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtINNotes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtINFollowUpText, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlJobAddress, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlJobAddress.ResumeLayout(False)
        CType(Me.txtLDJobStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgLDJobAddressAsAbove.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDJobStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDJobSuburb.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDJobState.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDJobPostCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlEnquirySources, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlEnquirySources.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLSName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlEnquiryInformation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlEnquiryInformation.ResumeLayout(False)
        CType(Me.txtLDUser.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvExpenses, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlClientInformation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlClientInformation.ResumeLayout(False)
        CType(Me.rgLDUseContactFalse.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pceContact.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pccContacts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientWorkPhoneNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientFirstName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDReferenceName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientSuburb.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientState.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientPostCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientHomePhoneNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientFaxNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientMobileNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgLDUseContactTrue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Storage, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvResources, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnOK.Enabled = Not HasRole("branch_read_only")
        ListBox.SelectedIndex = 0
    End Sub

    ' This data must be loaded BEFORE the DataRow is loaded (eg lookups etc)
    Private Sub FillPreliminaryData(ByVal BRID As Integer)
        ' --- LEAD SOURCES ---
        daLeadSources.Fill(dataSet)
        ' --- SALES REPS ---
        daExpenses.SelectCommand.Parameters("@BRID").Value = BRID
        daExpenses.Fill(dataSet)
        daExpenses.Fill(DsNotes)
        daExpenses.Fill(DsCalendar.VExpenses)
        ' --- TASKS ---
        daTasks.Fill(DsCalendar)
        ' --- INFollowUpTypes ---
        daINFollowUpTypes.Fill(DsNotes)
    End Sub

    ' This data must be loaded AFTER the DataRow is loaded (eg record related data)
    Private Sub FillData()
        ' --- LEAD SUBSOURCES ---
        daLeadsSubSources.SelectCommand.Parameters("@BRID").Value = DataRow("BRID")
        daLeadsSubSources.SelectCommand.Parameters("@LDID").Value = DataRow("LDID")
        daLeadsSubSources.Fill(dataSet)
        ' --- ID NOTES ---
        daIDNotes.SelectCommand.Parameters("@BRID").Value = DataRow("BRID")
        daIDNotes.SelectCommand.Parameters("@ID").Value = DataRow("ID")
        daIDNotes.Fill(DsNotes)
        ' --- CONTACT ---
        If Not DataRow("CTID") Is DBNull.Value Then
            If LoadContactDataRow(DataRow("BRID"), DataRow("CTID")) Then
                pceContact.Text = ContactDataRow("CTFirstName") & " (" & ContactDataRow("CTStreetAddress01") & ")"
            End If
        End If
        ' --- CALEDANR ---
        SetUpDateFilter()
        FillCalendar()
    End Sub

    ' This data must be loaded AFTER the DataRow is loaded (eg record related data)
    Private Sub FillInitData()
        ' --- LEAD SUBSOURCES ---
        Dim reader As SqlClient.SqlDataReader
        cmdLeadsSubSources_init.Parameters("@BRID").Value = DataRow("BRID")
        cmdLeadsSubSources_init.Parameters("@LDID").Value = DataRow("LDID")
        reader = cmdLeadsSubSources_init.ExecuteReader
        If reader.HasRows Then
            While reader.Read
                dataSet.VLeads_SubSources.AddVLeads_SubSourcesRow(reader("BRID"), reader("LDID"), reader("SSID"), reader("SSName"), reader("SSUserSelected"))
            End While
        End If
        If Not reader.IsClosed Then reader.Close()
        ' --- CALEDANR ---
        SetUpDateFilter()
        FillCalendar()
    End Sub

    Private Sub UpdateData()
        SqlDataAdapter.Update(dataSet)
        daLeadsSubSources.Update(dataSet)
        daIDNotes.Update(DsNotes)

        UpdateAppointmentsWithLeadData() ' Must update after SqlDataAdapter
        daAppointments.Update(DsCalendar.VAppointments)
        dvAppointments.RowFilter = "(BRID = " & DataRow("BRID") & ") AND (APType = 'LD') AND (APTypeID = " & DataRow("LDID") & ")"
    End Sub

    Private Sub EnableDisable()
        On Error Resume Next
        lblJobStreetAddress.Enabled = Not rgLDJobAddressAsAbove.EditValue
        lblJobSuburb.Enabled = Not rgLDJobAddressAsAbove.EditValue
        lblJobState.Enabled = Not rgLDJobAddressAsAbove.EditValue
        lblJobPostCode.Enabled = Not rgLDJobAddressAsAbove.EditValue
        txtLDJobStreetAddress01.Enabled = Not rgLDJobAddressAsAbove.EditValue
        txtLDJobStreetAddress02.Enabled = Not rgLDJobAddressAsAbove.EditValue
        txtLDJobSuburb.Enabled = Not rgLDJobAddressAsAbove.EditValue
        txtLDJobState.Enabled = Not rgLDJobAddressAsAbove.EditValue
        txtLDJobPostCode.Enabled = Not rgLDJobAddressAsAbove.EditValue
        '  tcScheduling.Enabled = rgLDHasAppointments.EditValue
        txtLDJobDescription.Enabled = rgLDQuoteRequested.EditValue
        txtEXIDRep.Enabled = rgLDQuoteRequested.EditValue
        chkLDQuoteReceived.Enabled = rgLDQuoteRequested.EditValue
    End Sub

    Private Const DEFAULT_SORT As String = "EGName, EXName"
    Private Sub SetUpCalendar()
        Storage.Appointments.Labels.Clear()
        For Each task As DataRow In DsCalendar.Tasks.Rows
            Storage.Appointments.Labels.Add(System.Drawing.Color.FromArgb(task("TAColourR"), task("TAColourG"), task("TAColourB")), _
                task("TAName"), task("TAMenuCaption"))
        Next
        ' --- SET UP FILTER ---
        dvResources.RowFilter = "EXAppearInCalendarRC = 1"
        dvResources.Sort = DEFAULT_SORT
    End Sub

    Public CalendarFromDate As Object = Nothing
    Public CalendarToDate As Object = Nothing
    Private Sub FillCalendar()
        ' --- APPOINTMENTS ---
        daAppointments.SelectCommand.Parameters("@BRID").Value = DataRow("BRID")
        Me.daAppointments.SelectCommand.Parameters("@FROM_DATE").Value = IsNull(CalendarFromDate, DBNull.Value)
        Me.daAppointments.SelectCommand.Parameters("@TO_DATE").Value = IsNull(CalendarToDate, DBNull.Value)
        DsCalendar.VAppointments.Clear()
        daAppointments.Fill(DsCalendar.VAppointments)
        dvAppointments.RowFilter = "(BRID = " & DataRow("BRID") & ") AND (APType = 'LD') AND (APTypeID = " & DataRow("LDID") & ")"
        ' --- Calendar ---
        SetUpCalendar()
    End Sub

    Private UpdatingDateFilter As Boolean = False
    Public Sub UpdateDateFilter(ByVal FromDate As Object, ByVal ToDate As Object)
        If Not UpdatingDateFilter Then
            UpdatingDateFilter = True
            Dim changes As DataSet = DsCalendar.GetChanges
            Dim originalFromDate As Object = CalendarFromDate
            Dim originalToDate As Object = CalendarToDate
            CalendarFromDate = FromDate
            CalendarToDate = ToDate
            FillCalendar()
            If Not changes Is Nothing Then
                Try
                    Library.UpdateOriginalDataSet(changes, DsCalendar)
                Catch ex As OriginalRowNotFoundException
                    Message.ShowMessage("The date filter could not be changed because you have made changes to appointments outside the date range you selected.", MessageBoxIcon.Exclamation)
                    CalendarFromDate = originalFromDate
                    CalendarToDate = originalToDate
                    FillCalendar()
                    Library.UpdateOriginalDataSet(changes, DsCalendar)
                    UpdatingDateFilter = False
                    Throw New CouldNotUpdateDateFilterException(originalFromDate, originalToDate, ex)
                End Try
            End If
            UpdatingDateFilter = False
        End If
    End Sub

    Private Sub SetUpDateFilter()
        ' Set Up Default Advanced Filter
        CalendarFromDate = IsNull(DataRow("LDDate"), Nothing)
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        ' EndEdit() to end editing the dataset record so that we can update
        DataRow.EndEdit()

        If ValidateForm() Then
            WriteMRU(txtLDJobDescription, UserAppDataPath)
            UpdateData()

            OK = True
            Me.Close()
        End If
    End Sub

    Private Function ValidateForm() As Boolean
        If Me.dvAppointments.Count > 0 Then
            If DataRow("CTID") Is DBNull.Value And DataRow("LDUseContact") Then
                Message.ShowMessage("You must either select an existing contact or create a new contact.", MessageBoxIcon.Exclamation)
                Return False
            End If
            If DataRow("EXIDRep") Is DBNull.Value Then
                If Message.AskQuestion("You have scheduled a salesperson in the calendar but you have not entered this person in the Principal Salesperson field." & _
                        "  Would you like to go back and enter this in the drop down?" & _
                        vbNewLine & vbNewLine & "Click 'Yes' to go back and change the entry.  Click 'No' to continue without changing the entry.") = DialogResult.Yes Then
                    Return False
                End If
            ElseIf Not RepIsScheduled(DataRow("EXIDRep")) Then
                If Message.AskQuestion("The Principal Salesperson entered does not correspond to any of the salespeople scheduled." & _
                        "  Would you like to go back and change the entry?" & _
                        vbNewLine & vbNewLine & "Click 'Yes' to go back and change the entry.  Click 'No' to continue without changing the entry.") = DialogResult.Yes Then
                    Return False
                End If
            End If
        End If
        Return True
    End Function

    Private Function RepIsScheduled(ByVal EXID As Integer) As Boolean
        For Each appointment As DataRowView In dvAppointments
            If appointment("EXID") = EXID Then
                Return True
            End If
        Next
        Return False
    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            DataRow.EndEdit()
            If dataSet.HasChanges Or DsNotes.HasChanges Or DsCalendar.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If response = MsgBoxResult.No Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private HelpTopic As String = Nothing
    Private Sub ListBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox.SelectedIndexChanged
        Dim pnl As DevExpress.XtraEditors.GroupControl

        For Each pnl In pnlMain.Controls
            pnl.Enabled = False
        Next
        pnl = Nothing
        Select Case ListBox.SelectedIndex
            Case 0
                pnl = Me.pnlClientInformation
                HelpTopic = "ClientInformation"
            Case 1
                pnl = Me.pnlEnquiryInformation
                HelpTopic = "EnquiryInformation"
            Case 2
                pnl = Me.pnlEnquirySources
                HelpTopic = "EnquirySources"
            Case 3
                pnl = Me.pnlQuoteRequestInformation
                HelpTopic = "QuoteRequestInformation"
            Case 4
                pnl = pnlJobAddress
                HelpTopic = "JobAddress"
            Case 5
                pnl = pnlScheduling
                HelpTopic = "Scheduling"
                UpdateAppointmentsWithLeadData()
            Case 6
                pnl = Me.pnlNotes
                HelpTopic = "Notes"
        End Select
        If Not pnl Is Nothing Then
            pnl.Enabled = True
            pnl.Dock = DockStyle.Fill
            Power.Library.Library.CenterControls(pnl)
            pnl.BringToFront()
        End If
    End Sub

#Region " Appointments "

    Public ReadOnly Property SelectedAppointment() As DataRow
        Get
            If Not gvAppointments.GetSelectedRows Is Nothing Then
                Return gvAppointments.GetDataRow(gvAppointments.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub Calendar_NewAllDayEvent(ByVal sender As Object, ByVal e As System.EventArgs)
        frmAppointment2.Add(SqlConnection, Storage, dvAppointments.Table, DataRow("BRID"), "LD", DataRow("LDID"), True, 1, , , , Me)
    End Sub

    Private Sub Calendar_NewAppointment(ByVal sender As Object, ByVal e As System.EventArgs)
        frmAppointment2.Add(SqlConnection, Storage, dvAppointments.Table, DataRow("BRID"), "LD", DataRow("LDID"), False, 1, , , , Me)
    End Sub

    Private Sub btnAddAppointment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAppointment.Click
        Calendar_NewAppointment(sender, e)
    End Sub

    Private Sub Calendar_EditAppointment(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not SelectedAppointment Is Nothing Then
            'If Not (SelectedAppointment.Item("BRID") = Me.BRID And SelectedAppointment.Item("APType") = "LD" And SelectedAppointment.Item("APTypeID") = LDID) Then
            '    If Message.AppointmentNotCurrentObject("quote request", Message.ObjectAction.Edit) = MsgBoxResult.No Then
            '        Exit Sub
            '    End If
            'End If
            If SelectedAppointment.RowState = DataRowState.Added Then
                frmAppointment2.Edit(SqlConnection, Storage, SelectedAppointment, 1)
            Else
                If AddAppointmentLock(SelectedAppointment.Item("APID")) Then
                    frmAppointment2.Edit(SqlConnection, Storage, SelectedAppointment, 1)
                Else
                    Message.CurrentlyAccessed("appointment")
                End If
            End If
        End If
    End Sub

    Private Sub dgAppointments_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgAppointments.DoubleClick
        Calendar_EditAppointment(sender, e)
    End Sub

    Private Sub btnEditAppointment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditAppointment.Click
        Calendar_EditAppointment(sender, e)
    End Sub

    Private Sub btnRemoveAppointment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveAppointment.Click
        If Not SelectedAppointment Is Nothing Then
            'If Not (SelectedAppointment.Item("BRID") = Me.BRID And SelectedAppointment.Item("APType") = "LD" And SelectedAppointment.Item("APTypeID") = LDID) Then
            '    If Message.AppointmentNotCurrentObject("quote request", Message.ObjectAction.Delete) = MsgBoxResult.No Then
            '        Exit Sub
            '    End If
            'End If
            If SelectedAppointment.RowState = DataRowState.Added Then
                SelectedAppointment.Delete()
            Else
                If AddAppointmentLock(SelectedAppointment.Item("APID")) Then
                    SelectedAppointment.Delete()
                Else
                    Message.CurrentlyAccessed("appointment")
                End If
            End If
        End If
    End Sub

    Private Sub btnViewCalendar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewCalendar.Click
        Dim gui As New frmCalendar2(SqlConnection, DsCalendar, DataRow("BRID"), "LD", DataRow("LDID"), Me)
        gui.EditedAppointmentList = EditedAppointmentList
        gui.Calendar.Calendar.SelectedInterval.Start = Today.Date
        gui.Calendar.Calendar.SelectedInterval.End = Today.Date
        gui.Calendar.Calendar.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Day
        If Not DataRow("EXIDRep") Is DBNull.Value Then
            gui.Calendar.SelectResource(DataRow("EXIDRep"))
        End If
        gui.ShowDialog(Me)
    End Sub

    Private EditedAppointmentList As New ArrayList

    Private Function AddAppointmentLock(ByVal APID As Long) As Boolean
        If Not IsInEditList(APID) Then
            If DataAccess.spExecLockRequest("sp_GetAppointmentLock", DataRow("BRID"), APID, SqlConnection) Then
                EditedAppointmentList.Add(APID)
                Return True
            Else
                Return False
            End If
        Else
            Return True
        End If
    End Function

    Private Function IsInEditList(ByVal APID As Long) As Boolean
        For Each lng As Long In EditedAppointmentList
            If lng = APID Then
                Return True
            End If
        Next
        Return False
    End Function

    Private Sub ReleaseAppointmentLocks()
        For Each lng As Long In EditedAppointmentList
            DataAccess.spExecLockRequest("sp_ReleaseAppointmentLock", DataRow("BRID"), lng, SqlConnection)
        Next
    End Sub

#End Region

#Region " Notes "

    Public ReadOnly Property SelectedNote() As DataRow
        Get
            If Not gvNotes.GetSelectedRows Is Nothing Then
                Return gvNotes.GetDataRow(gvNotes.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub gvNotes_InitNewRow(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) _
    Handles gvNotes.InitNewRow
        gvNotes.GetDataRow(e.RowHandle)("BRID") = DataRow("BRID")
        gvNotes.GetDataRow(e.RowHandle)("ID") = DataRow("ID")
        gvNotes.GetDataRow(e.RowHandle)("INFollowUpText") = DsNotes.INFollowUpTypes.FindByINFollowUpType _
            (gvNotes.GetDataRow(e.RowHandle)("INFollowUpType"))("INFollowUpTypeDesc")
    End Sub

    Private Sub gvNotes_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) _
    Handles gvNotes.KeyDown
        If e.KeyCode = Keys.Delete Then
            If gvNotes.SelectedRowsCount > 0 Then
                gvNotes.GetDataRow(gvNotes.GetSelectedRows(0)).Delete()
            End If
        End If
    End Sub

    Private Sub gvNotes_InvalidRowException(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) _
    Handles gvNotes.InvalidRowException
        GridView_InvalidRowException(sender, e)
    End Sub

    Private Sub txtINFollowUpText_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) _
    Handles txtINFollowUpText.ButtonClick
        If SelectedNote Is Nothing Then
            gvNotes.AddNewRow()
        End If
        frmFollowUp2.Edit(SelectedNote)
        gvNotes.RefreshRow(gvNotes.GetSelectedRows(0))
    End Sub

    Private Sub txtINNotes_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) _
    Handles txtINNotes.ButtonClick
        If SelectedNote Is Nothing Then
            gvNotes.AddNewRow()
        End If
        SelectedNote("INNotes") = frmMemo.Edit(IsNull(SelectedNote("INNotes"), ""), "Notes")
        gvNotes.RefreshRow(gvNotes.GetSelectedRows(0))
    End Sub

#End Region

    Private Sub Date_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) _
    Handles txtINDate.Validating, dpLDDate.Validating
        Library.DateEdit_Validating(sender, e)
    End Sub

    Private Sub rgLDJobAddressAsAbove_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgLDJobAddressAsAbove.SelectedIndexChanged
        EnableDisable()
    End Sub

    Private Sub rgLDQuoteRequested_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgLDQuoteRequested.SelectedIndexChanged
        EnableDisable()
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        If HelpTopic Is Nothing Then
            ShowHelpTopic(Me, "QuoteRequestsTerms.html")
        Else
            ShowHelpTopic(Me, "QuoteRequestsTerms.html#" & HelpTopic)
        End If
    End Sub

    Private Sub txtLDClientFirstName_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLDClientFirstName.EditValueChanged, txtLDClientSurname.EditValueChanged, txtLDClientFirstName.Validated, txtLDClientSurname.Validated
        If Not DataRow Is Nothing Then
            Me.Text = "Lead " & DataRow("ID") & " - " & FormatName(DataRow("LDClientFirstName"), DataRow("LDClientSurname"))
        Else
            Me.Text = "Lead"
        End If
    End Sub

    Private Sub miClearHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miClearHistory.Click
        ClearHistory(sender, e)
    End Sub

    Private Sub UpdateAppointmentsWithLeadData()
        For Each appointment As DataRowView In dvAppointments
            PopulateAppointmentWithLeadData(appointment.Row)
        Next
    End Sub

    Public Sub PopulateAppointmentWithLeadData(ByVal appointment As DsCalendar.VAppointmentsRow)
        If DataRowFieldDifferent(appointment("APTypeID"), DataRow("LDID")) Then appointment("APTypeID") = DataRow("LDID")
        If DataRowFieldDifferent(appointment("APClientName"), FormatName(DataRow("LDClientFirstName"), DataRow("LDClientSurname"))) Then appointment("APClientName") = FormatName(DataRow("LDClientFirstName"), DataRow("LDClientSurname"))
        If DataRowFieldDifferent(appointment("ID"), DataRow("ID")) Then appointment("ID") = DataRow("ID")
        If DataRowFieldDifferent(appointment("APDescription"), appointment("ID") & ": " & appointment("APClientName")) Then appointment("APDescription") = appointment("ID") & ": " & appointment("APClientName")
        If DataRowFieldDifferent(appointment("APJobDescription"), DataRow("LDJobDescription")) Then appointment("APJobDescription") = DataRow("LDJobDescription")
        If DataRowFieldDifferent(appointment("APClientHomePhoneNumber"), DataRow("LDClientHomePhoneNumber")) Then appointment("APClientHomePhoneNumber") = DataRow("LDClientHomePhoneNumber")
        If DataRowFieldDifferent(appointment("APClientWorkPhoneNumber"), DataRow("LDClientWorkPhoneNumber")) Then appointment("APClientWorkPhoneNumber") = DataRow("LDClientWorkPhoneNumber")
        If DataRowFieldDifferent(appointment("APClientMobileNumber"), DataRow("LDClientMobileNumber")) Then appointment("APClientMobileNumber") = DataRow("LDClientMobileNumber")
        If IsNull(DataRow("LDJobAddressAsAbove"), True) Then
            If DataRowFieldDifferent(appointment("APAddressMultiline"), FormatAddressMultiline(DataRow("LDClientStreetAddress01"), DataRow("LDClientStreetAddress02"), DataRow("LDClientSuburb"), DataRow("LDClientState"), DataRow("LDClientPostCode"))) Then appointment("APAddressMultiline") = FormatAddressMultiline(DataRow("LDClientStreetAddress01"), DataRow("LDClientStreetAddress02"), DataRow("LDClientSuburb"), DataRow("LDClientState"), DataRow("LDClientPostCode"))
            If DataRowFieldDifferent(appointment("APSuburb"), DataRow("LDClientSuburb")) Then appointment("APSuburb") = DataRow("LDClientSuburb")
        Else
            If DataRowFieldDifferent(appointment("APAddressMultiline"), FormatAddressMultiline(DataRow("LDJobStreetAddress01"), DataRow("LDJobStreetAddress02"), DataRow("LDJobSuburb"), DataRow("LDJobState"), DataRow("LDJobPostCode"))) Then appointment("APAddressMultiline") = FormatAddressMultiline(DataRow("LDJobStreetAddress01"), DataRow("LDJobStreetAddress02"), DataRow("LDJobSuburb"), DataRow("LDJobState"), DataRow("LDJobPostCode"))
            If DataRowFieldDifferent(appointment("APSuburb"), DataRow("LDJobSuburb")) Then appointment("APSuburb") = DataRow("LDJobSuburb")
        End If
    End Sub

    Private contactsLoaded As Boolean = False
    Private lvContacts As lvContacts2
    Private dropDownOpen As Boolean = False
    Private Sub pceContact_Popup(ByVal sender As Object, ByVal e As System.EventArgs) Handles pceContact.Popup
        If contactsLoaded Then
            If Not ReopeningPopup Then
                If Not DataRow("CTID") Is DBNull.Value Then
                    lvContacts.SelectRow(DataRow("BRID"), DataRow("CTID"))
                End If
            End If
        Else
            Dim c As Cursor = Me.Cursor
            Me.Cursor = Cursors.WaitCursor
            lvContacts = New lvContacts2(Me.SqlConnection, DataRow("BRID"))
            AddHandler lvContacts.ContactSelected, AddressOf Me.ContactSelected
            AddHandler lvContacts.PopupClosed, AddressOf Me.ContactsPopupClosed
            lvContacts.DoubleClickAction = lvContacts2.ContactAction.SelectContact
            lvContacts.Dock = DockStyle.Fill
            lvContacts.Visible = False
            pccContacts.Controls.Add(lvContacts)
            lvContacts.Visible = True
            If Not ReopeningPopup Then
                If Not DataRow("CTID") Is DBNull.Value Then
                    lvContacts.SelectRow(DataRow("BRID"), DataRow("CTID"))
                End If
            End If
            lvContacts.Select()
            contactsLoaded = True
            Me.Cursor = c
        End If
        dropDownOpen = True
    End Sub

    Private ReopeningPopup As Boolean = False
    Private Sub ContactsPopupClosed()
        'Dim BRID As Integer
        'Dim CTID As Long
        'BRID = lvContacts.SelectedRowField("BRID")
        'CTID = lvContacts.SelectedRowField("CTID")
        ReopeningPopup = True
        Me.pceContact.ShowPopup()
        ReopeningPopup = False
        'lvContacts.SelectRow(BRID, CTID)
    End Sub

    Private ContactDataRow As DataRow
    Private Function LoadContactDataRow(ByVal BRID As Integer, ByVal CTID As Long) As Boolean
        dataSet.Contacts.Clear()
        daContacts.SelectCommand.Parameters("@BRID").Value = BRID
        daContacts.SelectCommand.Parameters("@CTID").Value = CTID
        daContacts.Fill(dataSet)
        If dataSet.Contacts.Count > 0 Then
            ContactDataRow = dataSet.Contacts(0)
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub ContactSelected(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal BRID As Integer, ByVal CTID As Long)
        Dim oldCTID As Object = DataRow("CTID")
        If LoadContactDataRow(BRID, CTID) Then
            Dim copyAcross As Boolean
            If IsNull(ContactDataRow("CTID"), -1) = IsNull(oldCTID, -2) And Not (IsNull(DataRow("LDClientSurname"), "") = IsNull(ContactDataRow("CTSurname"), "") And _
                    IsNull(DataRow("LDClientFirstName"), "") = IsNull(ContactDataRow("CTFirstName"), "") And _
                    IsNull(DataRow("LDReferenceName"), "") = IsNull(ContactDataRow("CTReferenceName"), "") And _
                    IsNull(DataRow("LDClientStreetAddress01"), "") = IsNull(ContactDataRow("CTStreetAddress01"), "") And _
                    IsNull(DataRow("LDClientStreetAddress02"), "") = IsNull(ContactDataRow("CTStreetAddress02"), "") And _
                    IsNull(DataRow("LDClientSuburb"), "") = IsNull(ContactDataRow("CTSuburb"), "") And _
                    IsNull(DataRow("LDClientState"), "") = IsNull(ContactDataRow("CTState"), "") And _
                    IsNull(DataRow("LDClientPostCode"), "") = IsNull(ContactDataRow("CTPostCode"), "") And _
                    IsNull(DataRow("LDClientHomePhoneNumber"), "") = IsNull(ContactDataRow("CTHomePhoneNumber"), "") And _
                    IsNull(DataRow("LDClientWorkPhoneNumber"), "") = IsNull(ContactDataRow("CTWorkPhoneNumber"), "") And _
                    IsNull(DataRow("LDClientFaxNumber"), "") = IsNull(ContactDataRow("CTFaxNumber"), "") And _
                    IsNull(DataRow("LDClientMobileNumber"), "") = IsNull(ContactDataRow("CTMobileNumber"), "") And _
                    IsNull(DataRow("LDClientEmail"), "") = IsNull(ContactDataRow("CTEmail"), "")) Then
                copyAcross = (Message.AskCopyContactDetails("lead") = DialogResult.Yes)
            Else
                copyAcross = True
            End If
            If copyAcross Then
                With ContactDataRow
                    pceContact.Text = .Item("CTFirstName") & " (" & .Item("CTStreetAddress01") & ")"
                    DataRow("CTID") = .Item("CTID")
                    DataRow("LDClientSurname") = .Item("CTSurname")
                    DataRow("LDClientFirstName") = .Item("CTFirstName")
                    DataRow("LDReferenceName") = .Item("CTReferenceName")
                    DataRow("LDClientStreetAddress01") = .Item("CTStreetAddress01")
                    DataRow("LDClientStreetAddress02") = .Item("CTStreetAddress02")
                    DataRow("LDClientSuburb") = .Item("CTSuburb")
                    DataRow("LDClientState") = .Item("CTState")
                    DataRow("LDClientPostCode") = .Item("CTPostCode")
                    DataRow("LDClientHomePhoneNumber") = .Item("CTHomePhoneNumber")
                    DataRow("LDClientWorkPhoneNumber") = .Item("CTWorkPhoneNumber")
                    DataRow("LDClientFaxNumber") = .Item("CTFaxNumber")
                    DataRow("LDClientMobileNumber") = .Item("CTMobileNumber")
                    DataRow("LDClientEmail") = .Item("CTEmail")
                End With
            End If
            dropDownOpen = False
            pceContact.ClosePopup()
        End If
    End Sub

    Private Sub pceContact_CloseUp(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.CloseUpEventArgs) Handles pceContact.CloseUp
        If Not DataRow("CTID") Is DBNull.Value Then
            If LoadContactDataRow(DataRow("BRID"), DataRow("CTID")) Then
                pceContact.Text = ContactDataRow("CTFirstName") & " (" & ContactDataRow("CTStreetAddress01") & ")"
            End If
        End If
        e.AcceptValue = True
    End Sub

    Private Sub rgLDUseContactTrue_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgLDUseContactTrue.SelectedIndexChanged
        If rgLDUseContactTrue.EditValue = True Then
            rgLDUseContactTrue.DoValidate()
            rgLDUseContactFalse.EditValue = rgLDUseContactTrue.EditValue
            rgLDUseContactFalse.DoValidate()
            'DataRow("LDUseContact") = False
            DataRow.EndEdit()
            Me.pceContact.Enabled = True
            Me.txtLDClientSurname.Properties.ReadOnly = True
            Me.txtLDClientFirstName.Properties.ReadOnly = True
            Me.txtLDReferenceName.Properties.ReadOnly = True
            Me.txtLDClientStreetAddress01.Properties.ReadOnly = True
            Me.txtLDClientStreetAddress02.Properties.ReadOnly = True
            Me.txtLDClientSuburb.Properties.ReadOnly = True
            Me.txtLDClientState.Properties.ReadOnly = True
            Me.txtLDClientPostCode.Properties.ReadOnly = True
            Me.txtLDClientHomePhoneNumber.Properties.ReadOnly = True
            Me.txtLDClientWorkPhoneNumber.Properties.ReadOnly = True
            Me.txtLDClientFaxNumber.Properties.ReadOnly = True
            Me.txtLDClientMobileNumber.Properties.ReadOnly = True
            Me.txtLDClientEmail.Properties.ReadOnly = True
        End If
    End Sub

    Private Sub rgLDUseContactFalse_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgLDUseContactFalse.SelectedIndexChanged
        If rgLDUseContactFalse.EditValue = False Then
            rgLDUseContactFalse.DoValidate()
            rgLDUseContactTrue.EditValue = rgLDUseContactFalse.EditValue
            rgLDUseContactTrue.DoValidate()
            'DataRow("LDUseContact") = False
            DataRow.EndEdit()
            DataRow("CTID") = DBNull.Value
            Me.pceContact.EditValue = Nothing
            Me.pceContact.Enabled = False
            Me.txtLDClientSurname.Properties.ReadOnly = False
            Me.txtLDClientFirstName.Properties.ReadOnly = False
            Me.txtLDReferenceName.Properties.ReadOnly = False
            Me.txtLDClientStreetAddress01.Properties.ReadOnly = False
            Me.txtLDClientStreetAddress02.Properties.ReadOnly = False
            Me.txtLDClientSuburb.Properties.ReadOnly = False
            Me.txtLDClientState.Properties.ReadOnly = False
            Me.txtLDClientPostCode.Properties.ReadOnly = False
            Me.txtLDClientHomePhoneNumber.Properties.ReadOnly = False
            Me.txtLDClientWorkPhoneNumber.Properties.ReadOnly = False
            Me.txtLDClientFaxNumber.Properties.ReadOnly = False
            Me.txtLDClientMobileNumber.Properties.ReadOnly = False
            Me.txtLDClientEmail.Properties.ReadOnly = False
        End If
    End Sub

End Class
