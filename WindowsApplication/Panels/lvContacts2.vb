Imports Power.Library
Imports Power.Forms

Public Class lvContacts2
    Inherits System.Windows.Forms.UserControl
    Implements IListControl, IPrintableControl, ISelectedItemReports

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Connection As SqlClient.SqlConnection, ByVal BRID As Integer, Optional ByVal ExplorerForm As frmExplorer = Nothing)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.Connection = Connection
        Me.BRID = BRID
        hExplorerForm = ExplorerForm
        If ExplorerForm Is Nothing Then
            bStandard.Visible = False
            bDropDown.Visible = True
        Else
            bStandard.Visible = True
            bDropDown.Visible = False
        End If
        SetUpPermissions()
        FillDataSet()
        EnableDisable()
    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DsGTMS As WindowsApplication.dsGTMS
    Friend WithEvents hSqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents bStandard As DevExpress.XtraBars.Bar
    Friend WithEvents btnNew As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnEdit As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnDelete As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnMailMerge As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subReportsOnSelectedItem As DevExpress.XtraBars.BarSubItem
    Friend WithEvents subFormsOnSelectedItem As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnHelp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarAndDockingController1 As DevExpress.XtraBars.BarAndDockingController
    Friend WithEvents ImageListToolBar As System.Windows.Forms.ImageList
    Friend WithEvents btnPrintPreview As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnQuickPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subReportsOnSelectedItem2 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents subFormsOnSelectedItem2 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents PopupMenu1 As DevExpress.XtraBars.PopupMenu
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents btnCancel As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnUncancel As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bAdvancedFilter As DevExpress.XtraBars.Bar
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents RepositoryItemDateEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents RepositoryItemDateEdit4 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents DataView As System.Data.DataView
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemPopupContainerEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit
    Friend WithEvents btnResetFilters As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents colCTSurname As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTStreetAddress01 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTHomePhoneNumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTWorkPhoneNumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTMobileNumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents bDropDown As DevExpress.XtraBars.Bar
    Friend WithEvents btnSelect As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnJobReportByContact As DevExpress.XtraBars.BarButtonItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim StyleFormatCondition1 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(lvContacts2))
        Me.hSqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.DsGTMS = New WindowsApplication.dsGTMS
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.DataView = New System.Data.DataView
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colCTSurname = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colCTStreetAddress01 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colCTHomePhoneNumber = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colCTWorkPhoneNumber = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colCTMobileNumber = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox
        Me.BarManager1 = New DevExpress.XtraBars.BarManager
        Me.bStandard = New DevExpress.XtraBars.Bar
        Me.btnNew = New DevExpress.XtraBars.BarButtonItem
        Me.btnEdit = New DevExpress.XtraBars.BarButtonItem
        Me.subReportsOnSelectedItem = New DevExpress.XtraBars.BarSubItem
        Me.btnJobReportByContact = New DevExpress.XtraBars.BarButtonItem
        Me.subFormsOnSelectedItem = New DevExpress.XtraBars.BarSubItem
        Me.btnQuickPrint = New DevExpress.XtraBars.BarButtonItem
        Me.btnPrintPreview = New DevExpress.XtraBars.BarButtonItem
        Me.btnMailMerge = New DevExpress.XtraBars.BarButtonItem
        Me.btnHelp = New DevExpress.XtraBars.BarButtonItem
        Me.bAdvancedFilter = New DevExpress.XtraBars.Bar
        Me.bDropDown = New DevExpress.XtraBars.Bar
        Me.btnSelect = New DevExpress.XtraBars.BarButtonItem
        Me.BarAndDockingController1 = New DevExpress.XtraBars.BarAndDockingController(Me.components)
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
        Me.ImageListToolBar = New System.Windows.Forms.ImageList(Me.components)
        Me.btnCancel = New DevExpress.XtraBars.BarButtonItem
        Me.btnUncancel = New DevExpress.XtraBars.BarButtonItem
        Me.btnDelete = New DevExpress.XtraBars.BarButtonItem
        Me.btnPrint = New DevExpress.XtraBars.BarButtonItem
        Me.subReportsOnSelectedItem2 = New DevExpress.XtraBars.BarSubItem
        Me.subFormsOnSelectedItem2 = New DevExpress.XtraBars.BarSubItem
        Me.btnResetFilters = New DevExpress.XtraBars.BarButtonItem
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.RepositoryItemDateEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.RepositoryItemDateEdit4 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.RepositoryItemPopupContainerEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit
        Me.PopupMenu1 = New DevExpress.XtraBars.PopupMenu
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemPopupContainerEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'hSqlConnection
        '
        Me.hSqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;user id=sa;integrated security=SSPI;data sou" & _
        "rce=""SERVER\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VContacts", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("CTID", "CTID"), New System.Data.Common.DataColumnMapping("CTSurname", "CTSurname"), New System.Data.Common.DataColumnMapping("CTStreetAddress01", "CTStreetAddress01"), New System.Data.Common.DataColumnMapping("CTHomePhoneNumber", "CTHomePhoneNumber"), New System.Data.Common.DataColumnMapping("CTWorkPhoneNumber", "CTWorkPhoneNumber"), New System.Data.Common.DataColumnMapping("CTMobileNumber", "CTMobileNumber"), New System.Data.Common.DataColumnMapping("CTFaxNumber", "CTFaxNumber"), New System.Data.Common.DataColumnMapping("CTEmail", "CTEmail")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Contacts WHERE (BRID = @Original_BRID) AND (CTID = @Original_CTID) AN" &
        "D (CTStreetAddress01 = @Original_CTStreetAddress01 OR @Original_CTStreetAddress01 IS NULL AND CTStreetAddress01 " &
        "IS NULL) AND (CTEmail = @Original_CTEmail OR @Original_CTEmail IS NULL AND CTEma" &
        "il IS NULL) AND (CTFaxNumber = @Original_CTFaxNumber OR @Original_CTFaxNumber IS" &
        " NULL AND CTFaxNumber IS NULL) AND (CTHomePhoneNumber = @Original_CTHomePhoneNum" &
        "ber OR @Original_CTHomePhoneNumber IS NULL AND CTHomePhoneNumber IS NULL) AND (C" &
        "TMobileNumber = @Original_CTMobileNumber OR @Original_CTMobileNumber IS NULL AND" &
        " CTMobileNumber IS NULL) AND (CTSurname = @Original_CTSurname OR @Original_CTSurname IS N" &
        "ULL AND CTSurname IS NULL) AND (CTWorkPhoneNumber = @Original_CTWorkPhoneNumber OR " &
        "@Original_CTWorkPhoneNumber IS NULL AND CTWorkPhoneNumber IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.hSqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTStreetAddress01", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTHomePhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTSurname", System.Data.SqlDbType.VarChar, 102, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTWorkPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Contacts (BRID, CTSurname, CTStreetAddress01, CTHomePhoneNumber, CTWorkPhoneNumb" &
        "er, CTMobileNumber, CTFaxNumber, CTEmail) VALUES (@BRID, @CTSurname, @CTStreetAddress01, @C" &
        "THomePhoneNumber, @CTWorkPhoneNumber, @CTMobileNumber, @CTFaxNumber, @CTEmail); " &
        "SELECT BRID, CTID, CTSurname, CTStreetAddress01, CTHomePhoneNumber, CTWorkPhoneNumber, CTMo" &
        "bileNumber, CTFaxNumber, CTEmail FROM VContacts WHERE (BRID = @BRID) AND (CTID =" &
        " @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.hSqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTSurname", System.Data.SqlDbType.VarChar, 102, "CTSurname"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTStreetAddress01", System.Data.SqlDbType.VarChar, 259, "CTStreetAddress01"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, "CTHomePhoneNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, "CTWorkPhoneNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTMobileNumber", System.Data.SqlDbType.VarChar, 20, "CTMobileNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTFaxNumber", System.Data.SqlDbType.VarChar, 20, "CTFaxNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTEmail", System.Data.SqlDbType.VarChar, 50, "CTEmail"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, CTID, CTSurname, CTStreetAddress01, CTHomePhoneNumber, CTWorkPhoneNumber, CTMob" &
        "ileNumber, CTFaxNumber, CTEmail FROM VContacts WHERE (BRID = @BRID)"
        Me.SqlSelectCommand1.Connection = Me.hSqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Contacts SET BRID = @BRID, CTSurname = @CTSurname, CTStreetAddress01 = @CTStreetAddress01, CTHom" &
        "ePhoneNumber = @CTHomePhoneNumber, CTWorkPhoneNumber = @CTWorkPhoneNumber, CTMob" &
        "ileNumber = @CTMobileNumber, CTFaxNumber = @CTFaxNumber, CTEmail = @CTEmail WHER" &
        "E (BRID = @Original_BRID) AND (CTID = @Original_CTID) AND (CTStreetAddress01 = @Original" &
        "_CTStreetAddress01 OR @Original_CTStreetAddress01 IS NULL AND CTStreetAddress01 IS NULL) AND (CTEmail = " &
        "@Original_CTEmail OR @Original_CTEmail IS NULL AND CTEmail IS NULL) AND (CTFaxNu" &
        "mber = @Original_CTFaxNumber OR @Original_CTFaxNumber IS NULL AND CTFaxNumber IS" &
        " NULL) AND (CTHomePhoneNumber = @Original_CTHomePhoneNumber OR @Original_CTHomeP" &
        "honeNumber IS NULL AND CTHomePhoneNumber IS NULL) AND (CTMobileNumber = @Origina" &
        "l_CTMobileNumber OR @Original_CTMobileNumber IS NULL AND CTMobileNumber IS NULL)" &
        " AND (CTSurname = @Original_CTSurname OR @Original_CTSurname IS NULL AND CTSurname IS NULL) " &
        "AND (CTWorkPhoneNumber = @Original_CTWorkPhoneNumber OR @Original_CTWorkPhoneNum" &
        "ber IS NULL AND CTWorkPhoneNumber IS NULL); SELECT BRID, CTID, CTSurname, CTStreetAddress01" &
        ", CTHomePhoneNumber, CTWorkPhoneNumber, CTMobileNumber, CTFaxNumber, CTEmail FRO" &
        "M VContacts WHERE (BRID = @BRID) AND (CTID = @CTID)"
        Me.SqlUpdateCommand1.Connection = Me.hSqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTSurname", System.Data.SqlDbType.VarChar, 102, "CTSurname"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTStreetAddress01", System.Data.SqlDbType.VarChar, 259, "CTStreetAddress01"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, "CTHomePhoneNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, "CTWorkPhoneNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTMobileNumber", System.Data.SqlDbType.VarChar, 20, "CTMobileNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTFaxNumber", System.Data.SqlDbType.VarChar, 20, "CTFaxNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTEmail", System.Data.SqlDbType.VarChar, 50, "CTEmail"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTStreetAddress01", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTHomePhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTHomePhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTSurname", System.Data.SqlDbType.VarChar, 102, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CTWorkPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CTWorkPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CTID", System.Data.SqlDbType.BigInt, 8, "CTID"))
        '
        'DsGTMS
        '
        Me.DsGTMS.DataSetName = "dsGTMS"
        Me.DsGTMS.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.DataView
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'GridControl1.EmbeddedNavigator
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridControl1.Location = New System.Drawing.Point(0, 79)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemComboBox1})
        Me.GridControl1.Size = New System.Drawing.Size(632, 313)
        Me.GridControl1.Styles.AddReplace("CardBorder", New DevExpress.Utils.ViewStyleEx("CardBorder", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.InactiveBorder, System.Drawing.SystemColors.WindowFrame, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("BandPanelBackground", New DevExpress.Utils.ViewStyleEx("BandPanelBackground", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ControlDark, System.Drawing.Color.DarkSalmon, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("EmptySpace", New DevExpress.Utils.ViewStyleEx("EmptySpace", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("FieldValue", New DevExpress.Utils.ViewStyleEx("FieldValue", "CardView", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("BandPanel", New DevExpress.Utils.ViewStyleEx("BandPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("CardButton", New DevExpress.Utils.ViewStyleEx("CardButton", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("FocusedCardCaption", New DevExpress.Utils.ViewStyleEx("FocusedCardCaption", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ActiveCaption, System.Drawing.SystemColors.ActiveCaptionText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("CardCaption", New DevExpress.Utils.ViewStyleEx("CardCaption", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.InactiveCaption, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("HeaderPanelBackground", New DevExpress.Utils.ViewStyleEx("HeaderPanelBackground", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ControlDark, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("SeparatorLine", New DevExpress.Utils.ViewStyleEx("SeparatorLine", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ActiveBorder, System.Drawing.SystemColors.ActiveBorder, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("FieldCaption", New DevExpress.Utils.ViewStyleEx("FieldCaption", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Top, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'DataView
        '
        Me.DataView.Table = Me.DsGTMS.VContacts
        '
        'GridView1
        '
        Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
        Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
        Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCTSurname, Me.colCTStreetAddress01, Me.colCTHomePhoneNumber, Me.colCTWorkPhoneNumber, Me.colCTMobileNumber})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        StyleFormatCondition1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Strikeout, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        StyleFormatCondition1.Appearance.ForeColor = System.Drawing.SystemColors.GrayText
        StyleFormatCondition1.Appearance.Options.UseFont = True
        StyleFormatCondition1.Appearance.Options.UseForeColor = True
        StyleFormatCondition1.ApplyToRow = True
        StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal
        StyleFormatCondition1.Value1 = "Cancelled"
        Me.GridView1.FormatConditions.AddRange(New DevExpress.XtraGrid.StyleFormatCondition() {StyleFormatCondition1})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsNavigation.AutoFocusNewRow = True
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowGroupedColumns = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.OptionsView.ShowHorzLines = False
        Me.GridView1.OptionsView.ShowIndicator = False
        Me.GridView1.OptionsView.ShowVertLines = False
        Me.GridView1.RowHeight = 8
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colCTSurname, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colCTSurname
        '
        Me.colCTSurname.Caption = "Name"
        Me.colCTSurname.FieldName = "CTSurname"
        Me.colCTSurname.Name = "colCTSurname"
        Me.colCTSurname.Visible = True
        Me.colCTSurname.VisibleIndex = 0
        Me.colCTSurname.Width = 325
        '
        'colCTStreetAddress01
        '
        Me.colCTStreetAddress01.Caption = "Address"
        Me.colCTStreetAddress01.FieldName = "CTStreetAddress01"
        Me.colCTStreetAddress01.Name = "colCTStreetAddress01"
        Me.colCTStreetAddress01.Visible = True
        Me.colCTStreetAddress01.VisibleIndex = 1
        Me.colCTStreetAddress01.Width = 295
        '
        'colCTHomePhoneNumber
        '
        Me.colCTHomePhoneNumber.Caption = "Home Phone"
        Me.colCTHomePhoneNumber.FieldName = "CTHomePhoneNumber"
        Me.colCTHomePhoneNumber.Name = "colCTHomePhoneNumber"
        Me.colCTHomePhoneNumber.Visible = True
        Me.colCTHomePhoneNumber.VisibleIndex = 2
        Me.colCTHomePhoneNumber.Width = 157
        '
        'colCTWorkPhoneNumber
        '
        Me.colCTWorkPhoneNumber.Caption = "Work Phone"
        Me.colCTWorkPhoneNumber.FieldName = "CTWorkPhoneNumber"
        Me.colCTWorkPhoneNumber.Name = "colCTWorkPhoneNumber"
        Me.colCTWorkPhoneNumber.Visible = True
        Me.colCTWorkPhoneNumber.VisibleIndex = 3
        Me.colCTWorkPhoneNumber.Width = 157
        '
        'colCTMobileNumber
        '
        Me.colCTMobileNumber.Caption = "Mobile"
        Me.colCTMobileNumber.FieldName = "CTMobileNumber"
        Me.colCTMobileNumber.Name = "colCTMobileNumber"
        Me.colCTMobileNumber.Visible = True
        Me.colCTMobileNumber.VisibleIndex = 4
        Me.colCTMobileNumber.Width = 163
        '
        'RepositoryItemComboBox1
        '
        Me.RepositoryItemComboBox1.AutoHeight = False
        Me.RepositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox1.Name = "RepositoryItemComboBox1"
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.bStandard, Me.bAdvancedFilter, Me.bDropDown})
        Me.BarManager1.Categories.AddRange(New DevExpress.XtraBars.BarManagerCategory() {New DevExpress.XtraBars.BarManagerCategory("Built-in Menus", New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd"), False), New DevExpress.XtraBars.BarManagerCategory("File", New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")), New DevExpress.XtraBars.BarManagerCategory("Reports", New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")), New DevExpress.XtraBars.BarManagerCategory("Forms", New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")), New DevExpress.XtraBars.BarManagerCategory("Help", New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")), New DevExpress.XtraBars.BarManagerCategory("Search", New System.Guid("2524bb1c-b1f8-4399-8539-f6369f28ea6d"))})
        Me.BarManager1.Controller = Me.BarAndDockingController1
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Images = Me.ImageListToolBar
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnNew, Me.btnEdit, Me.btnCancel, Me.btnUncancel, Me.btnDelete, Me.btnHelp, Me.btnPrintPreview, Me.btnQuickPrint, Me.btnPrint, Me.subReportsOnSelectedItem, Me.subFormsOnSelectedItem, Me.subReportsOnSelectedItem2, Me.subFormsOnSelectedItem2, Me.btnMailMerge, Me.btnResetFilters, Me.btnSelect, Me.btnJobReportByContact})
        Me.BarManager1.MainMenu = Me.bStandard
        Me.BarManager1.MaxItemId = 107
        Me.BarManager1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1, Me.RepositoryItemDateEdit2, Me.RepositoryItemDateEdit3, Me.RepositoryItemDateEdit4, Me.RepositoryItemTextEdit1, Me.RepositoryItemPopupContainerEdit1})
        '
        'bStandard
        '
        Me.bStandard.BarName = "Standard Toolbar"
        Me.bStandard.DockCol = 0
        Me.bStandard.DockRow = 0
        Me.bStandard.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bStandard.FloatLocation = New System.Drawing.Point(44, 188)
        Me.bStandard.FloatSize = New System.Drawing.Size(659, 24)
        Me.bStandard.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnNew, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnEdit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(Me.subReportsOnSelectedItem, True), New DevExpress.XtraBars.LinkPersistInfo(Me.subFormsOnSelectedItem, True), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnQuickPrint, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnPrintPreview, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnMailMerge, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnHelp, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.bStandard.OptionsBar.AllowQuickCustomization = False
        Me.bStandard.OptionsBar.DisableClose = True
        Me.bStandard.OptionsBar.DrawDragBorder = False
        Me.bStandard.OptionsBar.MultiLine = True
        Me.bStandard.OptionsBar.UseWholeRow = True
        Me.bStandard.Text = "Standard Toolbar"
        '
        'btnNew
        '
        Me.btnNew.Caption = "&New..."
        Me.btnNew.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnNew.Id = 19
        Me.btnNew.ImageIndex = 0
        Me.btnNew.ImageIndexDisabled = 0
        Me.btnNew.Name = "btnNew"
        '
        'btnEdit
        '
        Me.btnEdit.Caption = "&Edit..."
        Me.btnEdit.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnEdit.Id = 20
        Me.btnEdit.ImageIndex = 1
        Me.btnEdit.ImageIndexDisabled = 1
        Me.btnEdit.Name = "btnEdit"
        '
        'subReportsOnSelectedItem
        '
        Me.subReportsOnSelectedItem.Caption = "Reports on Selected Contact"
        Me.subReportsOnSelectedItem.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.subReportsOnSelectedItem.Id = 78
        Me.subReportsOnSelectedItem.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnJobReportByContact)})
        Me.subReportsOnSelectedItem.Name = "subReportsOnSelectedItem"
        '
        'btnJobReportByContact
        '
        Me.btnJobReportByContact.Caption = "Job Report by Contact"
        Me.btnJobReportByContact.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnJobReportByContact.Id = 106
        Me.btnJobReportByContact.Name = "btnJobReportByContact"
        '
        'subFormsOnSelectedItem
        '
        Me.subFormsOnSelectedItem.Caption = "Forms on Selected Contact"
        Me.subFormsOnSelectedItem.CategoryGuid = New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")
        Me.subFormsOnSelectedItem.Id = 79
        Me.subFormsOnSelectedItem.Name = "subFormsOnSelectedItem"
        Me.subFormsOnSelectedItem.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInCustomizing
        '
        'btnQuickPrint
        '
        Me.btnQuickPrint.Caption = "Print"
        Me.btnQuickPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnQuickPrint.Id = 46
        Me.btnQuickPrint.ImageIndex = 11
        Me.btnQuickPrint.ImageIndexDisabled = 11
        Me.btnQuickPrint.Name = "btnQuickPrint"
        '
        'btnPrintPreview
        '
        Me.btnPrintPreview.Caption = "Print Pre&view"
        Me.btnPrintPreview.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrintPreview.Id = 44
        Me.btnPrintPreview.ImageIndex = 10
        Me.btnPrintPreview.ImageIndexDisabled = 10
        Me.btnPrintPreview.Name = "btnPrintPreview"
        '
        'btnMailMerge
        '
        Me.btnMailMerge.Caption = "&Mail Merge..."
        Me.btnMailMerge.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnMailMerge.Id = 91
        Me.btnMailMerge.ImageIndex = 12
        Me.btnMailMerge.ImageIndexDisabled = 12
        Me.btnMailMerge.Name = "btnMailMerge"
        '
        'btnHelp
        '
        Me.btnHelp.Caption = "&Help"
        Me.btnHelp.CategoryGuid = New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")
        Me.btnHelp.Id = 68
        Me.btnHelp.ImageIndex = 4
        Me.btnHelp.ImageIndexDisabled = 4
        Me.btnHelp.Name = "btnHelp"
        '
        'bAdvancedFilter
        '
        Me.bAdvancedFilter.BarName = "Search Bar"
        Me.bAdvancedFilter.DockCol = 0
        Me.bAdvancedFilter.DockRow = 1
        Me.bAdvancedFilter.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bAdvancedFilter.FloatLocation = New System.Drawing.Point(40, 390)
        Me.bAdvancedFilter.FloatSize = New System.Drawing.Size(767, 51)
        Me.bAdvancedFilter.Offset = 71
        Me.bAdvancedFilter.OptionsBar.AllowQuickCustomization = False
        Me.bAdvancedFilter.OptionsBar.DisableClose = True
        Me.bAdvancedFilter.OptionsBar.MultiLine = True
        Me.bAdvancedFilter.Text = "Search Bar"
        Me.bAdvancedFilter.Visible = False
        '
        'bDropDown
        '
        Me.bDropDown.BarName = "Drop Down Toolbar"
        Me.bDropDown.DockCol = 0
        Me.bDropDown.DockRow = 1
        Me.bDropDown.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bDropDown.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnSelect, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnNew, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnEdit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.bDropDown.OptionsBar.AllowQuickCustomization = False
        Me.bDropDown.OptionsBar.DisableClose = True
        Me.bDropDown.OptionsBar.DrawDragBorder = False
        Me.bDropDown.OptionsBar.MultiLine = True
        Me.bDropDown.OptionsBar.UseWholeRow = True
        Me.bDropDown.Text = "Drop Down Toolbar"
        '
        'btnSelect
        '
        Me.btnSelect.Caption = "&Select"
        Me.btnSelect.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnSelect.Id = 105
        Me.btnSelect.ImageIndex = 16
        Me.btnSelect.ImageIndexDisabled = 16
        Me.btnSelect.Name = "btnSelect"
        '
        'BarAndDockingController1
        '
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseForeColor = True
        Me.BarAndDockingController1.PaintStyleName = "OfficeXP"
        '
        'ImageListToolBar
        '
        Me.ImageListToolBar.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImageListToolBar.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageListToolBar.ImageStream = CType(resources.GetObject("ImageListToolBar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListToolBar.TransparentColor = System.Drawing.Color.Transparent
        '
        'btnCancel
        '
        Me.btnCancel.Caption = "&Cancel"
        Me.btnCancel.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnCancel.Id = 92
        Me.btnCancel.ImageIndex = 2
        Me.btnCancel.ImageIndexDisabled = 2
        Me.btnCancel.Name = "btnCancel"
        '
        'btnUncancel
        '
        Me.btnUncancel.Caption = "&Uncancel"
        Me.btnUncancel.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnUncancel.Id = 86
        Me.btnUncancel.Name = "btnUncancel"
        '
        'btnDelete
        '
        Me.btnDelete.Caption = "&Delete"
        Me.btnDelete.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnDelete.Id = 23
        Me.btnDelete.ImageIndex = 13
        Me.btnDelete.ImageIndexDisabled = 13
        Me.btnDelete.Name = "btnDelete"
        '
        'btnPrint
        '
        Me.btnPrint.Caption = "&Print..."
        Me.btnPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrint.Id = 43
        Me.btnPrint.ImageIndex = 11
        Me.btnPrint.ImageIndexDisabled = 11
        Me.btnPrint.Name = "btnPrint"
        '
        'subReportsOnSelectedItem2
        '
        Me.subReportsOnSelectedItem2.Caption = "&Reports on Selected Contact"
        Me.subReportsOnSelectedItem2.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subReportsOnSelectedItem2.Id = 87
        Me.subReportsOnSelectedItem2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnJobReportByContact)})
        Me.subReportsOnSelectedItem2.Name = "subReportsOnSelectedItem2"
        '
        'subFormsOnSelectedItem2
        '
        Me.subFormsOnSelectedItem2.Caption = "&Forms on Selected Contact"
        Me.subFormsOnSelectedItem2.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subFormsOnSelectedItem2.Id = 88
        Me.subFormsOnSelectedItem2.Name = "subFormsOnSelectedItem2"
        '
        'btnResetFilters
        '
        Me.btnResetFilters.Caption = "&Reset Filters"
        Me.btnResetFilters.CategoryGuid = New System.Guid("2524bb1c-b1f8-4399-8539-f6369f28ea6d")
        Me.btnResetFilters.Id = 95
        Me.btnResetFilters.ImageIndex = 14
        Me.btnResetFilters.ImageIndexDisabled = 14
        Me.btnResetFilters.Name = "btnResetFilters"
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'RepositoryItemDateEdit3
        '
        Me.RepositoryItemDateEdit3.AutoHeight = False
        Me.RepositoryItemDateEdit3.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit3.Name = "RepositoryItemDateEdit3"
        '
        'RepositoryItemDateEdit4
        '
        Me.RepositoryItemDateEdit4.AutoHeight = False
        Me.RepositoryItemDateEdit4.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit4.Name = "RepositoryItemDateEdit4"
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'RepositoryItemPopupContainerEdit1
        '
        Me.RepositoryItemPopupContainerEdit1.AutoHeight = False
        Me.RepositoryItemPopupContainerEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemPopupContainerEdit1.Name = "RepositoryItemPopupContainerEdit1"
        Me.RepositoryItemPopupContainerEdit1.PopupSizeable = False
        Me.RepositoryItemPopupContainerEdit1.ShowPopupCloseButton = False
        '
        'PopupMenu1
        '
        Me.PopupMenu1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.subReportsOnSelectedItem, True), New DevExpress.XtraBars.LinkPersistInfo(Me.subFormsOnSelectedItem), New DevExpress.XtraBars.LinkPersistInfo(Me.btnEdit, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnDelete)})
        Me.PopupMenu1.Manager = Me.BarManager1
        Me.PopupMenu1.Name = "PopupMenu1"
        '
        'lvContacts2
        '
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "lvContacts2"
        Me.Size = New System.Drawing.Size(632, 392)
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemPopupContainerEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return hSqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            hSqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private Sub EnableDisable()
        btnNew.Enabled = AllowNew
        btnEdit.Enabled = AllowEdit
        btnCancel.Enabled = AllowCancel
        btnUncancel.Enabled = AllowUncancel
        btnDelete.Enabled = AllowDelete
    End Sub

    Public Function FillDataSet() As Integer Implements IListControl.FillDataSet
        If Not ExplorerForm Is Nothing Then
            Dim cursor As System.Windows.Forms.Cursor = ExplorerForm.Cursor
            ExplorerForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
        End If
        ' Open connection
        Dim trans As SqlClient.SqlTransaction = Connection.BeginTransaction(IsolationLevel.ReadUncommitted)
        Power.Library.Library.ApplyTransactionToAllDataAdapters(trans, Me)
        'Me.GridControl1.DataSource.Clear()
        FillDataSet = SqlDataAdapter.Fill(DsGTMS)
        'Close connecton
        trans.Commit()

        If Not ExplorerForm Is Nothing Then
            ExplorerForm.Cursor = cursor
        End If





    End Function

    Private Sub GridView1_RowCountChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.RowCountChanged
        If Not ExplorerForm Is Nothing Then
            ExplorerForm.UpdateNumItems(NumItems)
            ExplorerForm.UpdateVisibleItems(VisibleItems)
        End If
    End Sub

    Public ReadOnly Property VisibleItems() As Integer Implements Power.Library.IListControl.VisibleItems
        Get
            Return GridView1.RowCount
        End Get
    End Property

    Public ReadOnly Property NumItems() As Integer Implements Power.Library.IListControl.NumItems
        Get
            Return DsGTMS.Tables(SqlDataAdapter.TableMappings(0).DataSetTable).Rows.Count
        End Get
    End Property

    Private hBRID As Integer
    Private Property BRID() As Integer
        Get
            Return hBRID
        End Get
        Set(ByVal Value As Integer)
            hBRID = Value
            SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = Value
        End Set
    End Property

    Public ReadOnly Property SelectedRow() As DataRow
        Get
            If Not GridView1.GetSelectedRows Is Nothing Then
                Return GridView1.GetDataRow(GridView1.GetSelectedRows(0))
            End If
        End Get
    End Property

    Public ReadOnly Property SelectedRowField(ByVal Field As String) As Object
        Get
            If Not SelectedRow Is Nothing Then
                Return SelectedRow.Item(Field)
            Else
                Return DBNull.Value
            End If
        End Get
    End Property

    Public Function SelectRow(ByVal BRID As Int32, ByVal CTID As Int64) As Boolean
        Dim i As Integer = 0
        'Do Until GridView1.SelectedRowsCount = 0
        'GridView1.UnselectRow(GridView1.GetVisibleRowHandle(i))
        'Loop
        Do Until False
            Try
                If GridView1.GetDataRow(GridView1.GetVisibleRowHandle(i))("BRID") = BRID And _
                        GridView1.GetDataRow(GridView1.GetVisibleRowHandle(i))("CTID") = CTID Then
                    GridView1.ClearSelection()
                    GridView1.FocusedRowHandle = GridView1.GetVisibleRowHandle(i)
                    Return True
                End If
                i += 1
            Catch ex As NullReferenceException
                Return False
            End Try
        Loop
        Return False
    End Function

#Region " New, Open and Delete "

    Public Event PopupClosed()

    Public Enum ContactAction
        EditContact = 0
        SelectContact = 1
    End Enum

    Private hDoubleClickAction As ContactAction = ContactAction.EditContact
    Public Property DoubleClickAction() As ContactAction
        Get
            Return hDoubleClickAction
        End Get
        Set(ByVal Value As ContactAction)
            hDoubleClickAction = Value
        End Set
    End Property

    Private Sub GridControl1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridControl1.DoubleClick
        If DoubleClickAction = ContactAction.EditContact Then
            List_Edit()
        Else
            If Not SelectedRow Is Nothing Then
                RaiseEvent ContactSelected(sender, e, SelectedRowField("BRID"), SelectedRowField("CTID"))
            End If
        End If
    End Sub

    Private Sub btnNew_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnNew.ItemClick
        List_New()
    End Sub

    Public Sub List_New() Implements IListControl.List_New
        Dim c As Cursor = ParentForm.Cursor
        ParentForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
        frmContact2.Add(BRID)
        RaiseEvent PopupClosed()
        FillDataSet()
        ParentForm.Cursor = c
    End Sub

    Private Sub btnEdit_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnEdit.ItemClick
        List_Edit()
    End Sub

    Public Sub List_Edit() Implements IListControl.List_Edit
        If Not SelectedRow Is Nothing Then
            Dim c As Cursor = ParentForm.Cursor
            ParentForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If ContactExists(SelectedRowField("BRID"), SelectedRowField("CTID")) Then
                frmContact2.Edit(SelectedRowField("BRID"), SelectedRowField("CTID"))
                RaiseEvent PopupClosed()
                FillDataSet()
            Else
                Message.AlreadyDeleted("contact", Message.ObjectAction.Edit)
                RaiseEvent PopupClosed()
                If TypeOf Me.GridControl1.DataSource Is DataView Then
                    CType(Me.GridControl1.DataSource, DataView).Table.Clear()
                ElseIf TypeOf Me.GridControl1.DataSource Is DataSet Then
                    CType(Me.GridControl1.DataSource, DataSet).Clear()
                End If
                FillDataSet()
            End If
            ParentForm.Cursor = c
        End If
    End Sub

    Private Sub btnCancel_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnCancel.ItemClick
        List_Cancel()
    End Sub

    Public Sub List_Cancel() Implements Power.Library.IListControl.List_Cancel
    End Sub

    Private Sub btnUncancel_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnUncancel.ItemClick
        List_Uncancel()
    End Sub

    Public Sub List_Uncancel() Implements Power.Library.IListControl.List_Uncancel
    End Sub

    Private Sub btnDelete_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnDelete.ItemClick
        List_Delete()
    End Sub

    Public Sub List_Delete() Implements IListControl.List_Delete
        If Not SelectedRow Is Nothing Then
            If Message.AskDeleteObject("contact", Message.ObjectAction.Delete) = MsgBoxResult.Yes Then
                Dim BRID As Integer = SelectedRowField("BRID")
                Dim CTID As Int64 = SelectedRowField("CTID")
                If ContactExists(BRID, CTID) Then
                    If DataAccess.spExecLockRequest("sp_GetContactLock", BRID, CTID, Connection) Then
                        SelectedRow.Delete()
                        Try
                            SqlDataAdapter.Update(DsGTMS)
                        Catch ex As Exception
                            If CType(ex, SqlClient.SqlException).Number = 50000 Then
                                Message.ShowMessage("A lead or job with this contact is currently being accessed by another user." & _
                                    vbNewLine & vbNewLine & "Please try again later.", MessageBoxIcon.Exclamation)
                                SqlDataAdapter.Fill(DsGTMS)
                            Else
                                Throw ex
                            End If
                        End Try
                        DataAccess.spExecLockRequest("sp_ReleaseContactLock", BRID, CTID, Connection)
                    Else
                        Message.CurrentlyAccessed("contact")
                        RaiseEvent PopupClosed()
                    End If
                Else
                    If TypeOf Me.GridControl1.DataSource Is DataView Then
                        CType(Me.GridControl1.DataSource, DataView).Table.Clear()
                    ElseIf TypeOf Me.GridControl1.DataSource Is DataSet Then
                        CType(Me.GridControl1.DataSource, DataSet).Clear()
                    End If
                    FillDataSet()
                End If
            End If
        End If
    End Sub

    Private Function ContactExists(ByVal BRID As Int32, ByVal CTID As Int64) As Boolean
        Dim cnn As SqlClient.SqlConnection = Connection
        Dim cmd As New SqlClient.SqlCommand("SELECT Count(CTID) FROM Contacts WHERE BRID = @BRID AND CTID = @CTID", cnn)
        cmd.CommandType = CommandType.Text
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@CTID", CTID)
        Return cmd.ExecuteScalar > 0
    End Function

#End Region

    Private hExplorerForm As Form
    Public ReadOnly Property ExplorerForm() As frmExplorer
        Get
            Return hExplorerForm
        End Get
    End Property

#Region " Reports "

    Public ReadOnly Property SelectedItemForms() As DevExpress.XtraBars.BarItem Implements ISelectedItemReports.SelectedItemForms
        Get
            Return subFormsOnSelectedItem2
        End Get
    End Property

    Public ReadOnly Property SelectedItemReports() As DevExpress.XtraBars.BarItem Implements ISelectedItemReports.SelectedItemReports
        Get
            Return subReportsOnSelectedItem2
        End Get
    End Property

    Private Sub btnJobReportByContact_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnJobReportByContact.ItemClick
        If Not SelectedRow Is Nothing Then
            Dim DateRangeGui As New frmDateRange(Connection)
            If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
                Me.Cursor = Windows.Forms.Cursors.WaitCursor
                Dim gui As New frmReport(Nothing)
                Dim rep As New repContactJobReport
                rep.Load()
                Dim ds As New dsReports
                Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repContactJobsReport](@BRID, @CTID, @FROM_DATE, @TO_DATE)", Connection)
                cmd.Parameters.Add("@BRID", BRID)
                cmd.Parameters.Add("@CTID", SelectedRowField("CTID"))
                cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
                cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
                Dim da As New SqlClient.SqlDataAdapter(cmd)
                ReadUncommittedFill(da, ds.repAllJobsReport)
                rep.SetDataSource(ds)
                rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
                rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
                rep.SetParameterValue("ClientName", SelectedRowField("CTSurname"))
                gui.Display(rep)
                Me.Cursor = Windows.Forms.Cursors.Default
                gui.Text = "Job Report by Contact"
                gui.Show()
            End If
        End If
    End Sub

#End Region

#Region " Printing "

    Private Sub btnPrint_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrint.ItemClick
        If Not ExplorerForm Is Nothing Then
            Print(ExplorerForm.PrintingSystem)
        End If
    End Sub

    Public Sub Print(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.Print
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.PrintDlg()
    End Sub

    Private Sub btnPrintPreview_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrintPreview.ItemClick
        If Not ExplorerForm Is Nothing Then
            PrintPreview(ExplorerForm.PrintingSystem)
        End If
    End Sub

    Public Sub PrintPreview(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.PrintPreview
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.ShowPreview()
    End Sub

    Private Sub btnQuickPrint_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnQuickPrint.ItemClick
        If Not ExplorerForm Is Nothing Then
            QuickPrint(ExplorerForm.PrintingSystem)
        End If
    End Sub

    Public Sub QuickPrint(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.QuickPrint
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.Print(PrintingSystem.PageSettings.PrinterName)
    End Sub

#End Region

    Private Sub btnMailMerge_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnMailMerge.ItemClick
        If MailMergeFilterValid() Then
            Dim rep As New repGenericContactsMailout
            Dim cmd As SqlClient.SqlCommand
            Dim cmdString As String = "SELECT * FROM dbo.[Contacts Mail Merge] WHERE BRID = @BRID"
            Dim filter As String = MailMergeFilter()
            If filter = "" Then
                cmd = New SqlClient.SqlCommand(cmdString, Connection)
            Else
                cmd = New SqlClient.SqlCommand(cmdString & " AND " & filter, Connection)
            End If
            cmd.Parameters.Add("@BRID", Me.BRID)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            'da.MissingMappingAction = MissingMappingAction.Ignore
            ReadUncommittedFill(da, rep.dataSet.Contacts_Mail_Merge)
            If rep.dataSet.Contacts_Mail_Merge.Count > 100 Then
                Message.ShowMessage_MailoutTooLarge(100)
                Exit Sub
            End If
            Dim designerGui As New frmXtraReportDesigner(rep)
            designerGui.Show()
        End If
    End Sub

    Private Function MailMergeFilter() As String
        Return Replace(Replace(Replace(Replace(Replace(Replace(GridView1.RowFilter,
            "[CTSurname]", "[Contact Name]"),
            "[CTStreetAddress01]", "[Contact Address (Full)]"),
            "[CTHomePhoneNumber]", "[Contact Home Phone]"),
            "[CTWorkPhoneNumber]", "[Contact Work Phone]"),
            "[CTMobileNumber]", "[Contact Mobile]"),
            "*", "%")
    End Function

    Private Function MailMergeFilterValid() As Boolean
        If InStr(GridView1.RowFilter, "[LDDate]") > 0 Or InStr(GridView1.RowFilter, "[LDAppointmentBeginDate]") > 0 Then
            Message.ShowMessage_DateInSearchRow()
            Return False
        End If
        Return True
    End Function

    Public ReadOnly Property AllowDelete() As Boolean Implements Power.Library.IListControl.AllowDelete
        Get
            Return HasRole("full")
        End Get
    End Property

    Public ReadOnly Property AllowEdit() As Boolean Implements Power.Library.IListControl.AllowEdit
        Get
            Return True
        End Get
    End Property

    Public ReadOnly Property AllowNew() As Boolean Implements Power.Library.IListControl.AllowNew
        Get
            Return Not HasRole("branch_read_only")
        End Get
    End Property

    Public ReadOnly Property AllowCancel() As Boolean Implements Power.Library.IListControl.AllowCancel
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property AllowUncancel() As Boolean Implements Power.Library.IListControl.AllowUncancel
        Get
            Return False
        End Get
    End Property

    Private Sub GridView1_ShowGridMenu(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.GridMenuEventArgs) Handles GridView1.ShowGridMenu
        If SelectedRow Is Nothing Then Exit Sub
        Dim View As DevExpress.XtraGrid.Views.Grid.GridView = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
        Dim HitInfo As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo
        HitInfo = View.CalcHitInfo(e.Point)
        If HitInfo.InRow Then
            Dim p2 As New Point(e.Point.X + View.GridControl.Left, e.Point.Y + View.GridControl.Top)
            PopupMenu1.ShowPopup(Me.PointToScreen(p2))
        End If
    End Sub

    Public Sub SetUpPermissions()
        For Each cmd As CommandAccess In Commands
            If Not Me.BarManager1.Items(cmd.Command) Is Nothing Then  ' This will not fail if the cmd.Command is not found
                Me.BarManager1.Items(cmd.Command).Tag = cmd
                Me.BarManager1.Items(cmd.Command).Enabled = cmd.Allowed
            End If
        Next
    End Sub

    Public Event ContactSelected(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal BRID As Integer, ByVal CTID As Long)
    Private Sub btnSelect_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnSelect.ItemClick
        If Not SelectedRow Is Nothing Then
            RaiseEvent ContactSelected(sender, e, SelectedRowField("BRID"), SelectedRowField("CTID"))
        End If
    End Sub

    Private Sub btnHelp_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHelp.ItemClick
        ShowHelpTopic(ExplorerForm, "UsingTheContactsSection.htm")
    End Sub
End Class
