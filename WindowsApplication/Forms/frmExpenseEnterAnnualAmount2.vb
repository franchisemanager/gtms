Public Class frmExpenseEnterAnnualAmount2
    Inherits DevExpress.XtraEditors.XtraForm

    Private ExpenseAllowanceDataRow As DataRow
    Public BRID As Int32
    Public EXID As Int32
    Public ShowHours As Boolean

    Private Connection As System.Data.SqlClient.SqlConnection

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal BRID As Integer, ByVal EXID As Integer, ByVal ShowHours As Boolean, ByVal Connection As System.Data.SqlClient.SqlConnection)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.BRID = BRID
        Me.EXID = EXID
        Me.ShowHours = ShowHours
        Me.Connection = Connection
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DsWizards As WindowsApplication.dsWizards
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txtALAllowance As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txtALFromDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents lblALHours As System.Windows.Forms.Label
    Friend WithEvents txtALHours As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label1 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.DsWizards = New WindowsApplication.dsWizards
        Me.Label28 = New System.Windows.Forms.Label
        Me.txtALAllowance = New DevExpress.XtraEditors.TextEdit
        Me.Label27 = New System.Windows.Forms.Label
        Me.txtALFromDate = New DevExpress.XtraEditors.DateEdit
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.lblALHours = New System.Windows.Forms.Label
        Me.txtALHours = New DevExpress.XtraEditors.TextEdit
        Me.Label1 = New System.Windows.Forms.Label
        CType(Me.DsWizards, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtALAllowance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtALFromDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtALHours.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(296, 280)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 10
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(376, 280)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 11
        Me.btnCancel.Text = "Cancel"
        '
        'DsWizards
        '
        Me.DsWizards.DataSetName = "dsWizards"
        Me.DsWizards.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'Label28
        '
        Me.Label28.Location = New System.Drawing.Point(80, 192)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(96, 21)
        Me.Label28.TabIndex = 5
        Me.Label28.Text = "Annual amount:"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtALAllowance
        '
        Me.txtALAllowance.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsWizards, "ExpenseAllowances.ALAllowance"))
        Me.txtALAllowance.Location = New System.Drawing.Point(176, 192)
        Me.txtALAllowance.Name = "txtALAllowance"
        '
        'txtALAllowance.Properties
        '
        Me.txtALAllowance.Properties.Appearance.Options.UseTextOptions = True
        Me.txtALAllowance.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtALAllowance.Properties.DisplayFormat.FormatString = "c"
        Me.txtALAllowance.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtALAllowance.Properties.EditFormat.FormatString = "c"
        Me.txtALAllowance.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtALAllowance.Size = New System.Drawing.Size(176, 20)
        Me.txtALAllowance.TabIndex = 6
        '
        'Label27
        '
        Me.Label27.Location = New System.Drawing.Point(80, 160)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(96, 21)
        Me.Label27.TabIndex = 3
        Me.Label27.Text = "Start date:"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtALFromDate
        '
        Me.txtALFromDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsWizards, "ExpenseAllowances.ALFromDate"))
        Me.txtALFromDate.EditValue = Nothing
        Me.txtALFromDate.Location = New System.Drawing.Point(176, 160)
        Me.txtALFromDate.Name = "txtALFromDate"
        '
        'txtALFromDate.Properties
        '
        Me.txtALFromDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtALFromDate.Size = New System.Drawing.Size(176, 20)
        Me.txtALFromDate.TabIndex = 4
        '
        'Label26
        '
        Me.Label26.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label26.Location = New System.Drawing.Point(8, 120)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(448, 32)
        Me.Label26.TabIndex = 2
        Me.Label26.Text = "If the annual amount changes, this can be altered in the Amount Specified Annuall" & _
        "y list in the accounting section of the program."
        '
        'Label25
        '
        Me.Label25.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label25.Location = New System.Drawing.Point(8, 48)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(448, 64)
        Me.Label25.TabIndex = 1
        Me.Label25.Text = "Note: The annual amount must be the amount the expense WOULD BE PAID if the expen" & _
        "se spanned the entire finiancial year.  If the expense was $40 000 over a full f" & _
        "inancial year, but the start date is half way through the financial year, the pr" & _
        "ogram will only assign $20 000 for this finacial year."
        '
        'Label24
        '
        Me.Label24.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label24.Location = New System.Drawing.Point(8, 8)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(448, 32)
        Me.Label24.TabIndex = 0
        Me.Label24.Text = "You must set up an annual amount for this expense.  Select the date the expense b" & _
        "egan and a total annual amount."
        '
        'lblALHours
        '
        Me.lblALHours.Location = New System.Drawing.Point(80, 224)
        Me.lblALHours.Name = "lblALHours"
        Me.lblALHours.Size = New System.Drawing.Size(96, 21)
        Me.lblALHours.TabIndex = 7
        Me.lblALHours.Text = "Hours:"
        Me.lblALHours.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtALHours
        '
        Me.txtALHours.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsWizards, "ExpenseAllowances.ALHours"))
        Me.txtALHours.Location = New System.Drawing.Point(176, 224)
        Me.txtALHours.Name = "txtALHours"
        '
        'txtALHours.Properties
        '
        Me.txtALHours.Properties.Appearance.Options.UseTextOptions = True
        Me.txtALHours.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtALHours.Properties.DisplayFormat.FormatString = "#0.00"
        Me.txtALHours.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtALHours.Properties.EditFormat.FormatString = "#0.00"
        Me.txtALHours.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtALHours.Size = New System.Drawing.Size(176, 20)
        Me.txtALHours.TabIndex = 8
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 256)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(416, 16)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "NOTE: All costs entered into the program include Indirect Tax, where applicable"
        '
        'frmExpenseEnterAnnualAmount2
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(458, 312)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtALHours)
        Me.Controls.Add(Me.lblALHours)
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.txtALAllowance)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.txtALFromDate)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExpenseEnterAnnualAmount2"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Enter Annual Amount"
        CType(Me.DsWizards, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtALAllowance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtALFromDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtALHours.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmExpenseEndAnnualAmount_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ExpenseAllowanceDataRow = DsWizards.ExpenseAllowances.NewExpenseAllowancesRow
        ExpenseAllowanceDataRow("BRID") = BRID
        ExpenseAllowanceDataRow("EXID") = EXID
        DsWizards.ExpenseAllowances.AddExpenseAllowancesRow(ExpenseAllowanceDataRow)

        ' Hours
        lblALHours.Visible = ShowHours
        txtALHours.Visible = ShowHours
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If ValidateForm() Then
            sp_InsertExpenseAllowance(BRID, EXID, ExpenseAllowanceDataRow("ALFromDate"), ExpenseAllowanceDataRow("ALAllowance"), ExpenseAllowanceDataRow("ALHours"), Connection)

            OK = True
            Me.Close()
        End If
    End Sub

    Private Function ValidateForm() As Boolean
        If ExpenseAllowanceDataRow("ALFromDate") Is DBNull.Value Then
            DevExpress.XtraEditors.XtraMessageBox.Show("You must enter a start date.", "Franchise Manager")
            Return False
        End If
        If ExpenseAllowanceDataRow("ALAllowance") Is DBNull.Value Then
            DevExpress.XtraEditors.XtraMessageBox.Show("You must enter an annual amount.", "Franchise Manager")
            Return False
        End If
        Return True
    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        End If
    End Sub

End Class

