Imports System.Data.SqlClient

Public Class frmMasterMaterialWizard
    Inherits DevExpress.XtraEditors.XtraForm

    Private DataRow As DataRow
    Public MMTID As Int32
    Public MGID As Int32

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return SqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            SqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private hTransaction As SqlClient.SqlTransaction
    Private Property Transaction() As SqlClient.SqlTransaction
        Get
            Return hTransaction
        End Get
        Set(ByVal Value As SqlClient.SqlTransaction)
            hTransaction = Value
            Power.Library.Library.ApplyTransactionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Public Shared Function Add(ByVal MGID As Int32) As frmMasterMaterialWizard
        Dim gui As New frmMasterMaterialWizard

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            .MGID = MGID

            .Transaction = .SqlConnection.BeginTransaction(IsolationLevel.ReadUncommitted)
            .FillPreliminaryData()
            .MMTID = spNew_Master_Material(MGID, .Transaction)
            .FillMGIDDependantData()

            If DataAccess.spExecLockRequest("sp_GetMasterMaterialLock", .MMTID, .Transaction) Then

                .SqlDataAdapter.SelectCommand.Parameters("@MMTID").Value = .MMTID
                .SqlDataAdapter.Fill(.DsHeadOffice)
                .DataRow = .DsHeadOffice.Master_Materials(0)

                .FillData()

            Else
                Throw New ObjectLockedException
            End If
        End With

        Return gui
    End Function

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents DsHeadOffice As WindowsApplication.dsHeadOffice
    Friend WithEvents daStockedItems As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daUOM As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daMaterialGroups As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents DsPrimaryUOM As WindowsApplication.dsUOM
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents DsInventoryUOM As WindowsApplication.dsUOM
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents HorizonalRuleLine3D8 As Power.Forms.HorizonalRuleLine3D
    Friend WithEvents btnBack As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnNext As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage4 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage5 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage6 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents lblIntroTitle As System.Windows.Forms.Label
    Friend WithEvents lblIntroDesc As System.Windows.Forms.Label
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents HorizonalRuleLine3D9 As Power.Forms.HorizonalRuleLine3D
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents HorizonalRuleLine3D1 As Power.Forms.HorizonalRuleLine3D
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents HorizonalRuleLine3D2 As Power.Forms.HorizonalRuleLine3D
    Friend WithEvents PanelControl5 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents HorizonalRuleLine3D3 As Power.Forms.HorizonalRuleLine3D
    Friend WithEvents PanelControl6 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents PanelControl11 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents lblFinishDesc2 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents lblFinishDesc1 As System.Windows.Forms.Label
    Friend WithEvents PanelControl12 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents txtMTName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtMTPrimaryUOM As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtMTInventoryUOM As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lblMTInventoryUOM As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents btnRemoveSheetSize As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dgStockedItems As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvStockedItems As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colSIConversionToPrimary As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtSIConversion As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents colMTPrimaryUOMShortName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSIName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim configurationAppSettings As System.Configuration.AppSettingsReader = New System.Configuration.AppSettingsReader
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMasterMaterialWizard))
        Me.DsHeadOffice = New WindowsApplication.dsHeadOffice
        Me.daStockedItems = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.daUOM = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.daMaterialGroups = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand
        Me.DsPrimaryUOM = New WindowsApplication.dsUOM
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.DsInventoryUOM = New WindowsApplication.dsUOM
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl
        Me.lblIntroTitle = New System.Windows.Forms.Label
        Me.lblIntroDesc = New System.Windows.Forms.Label
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage
        Me.txtMTName = New DevExpress.XtraEditors.TextEdit
        Me.Label9 = New System.Windows.Forms.Label
        Me.HorizonalRuleLine3D9 = New Power.Forms.HorizonalRuleLine3D
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage
        Me.Label22 = New System.Windows.Forms.Label
        Me.txtMTPrimaryUOM = New DevExpress.XtraEditors.LookUpEdit
        Me.Label10 = New System.Windows.Forms.Label
        Me.HorizonalRuleLine3D1 = New Power.Forms.HorizonalRuleLine3D
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.XtraTabPage4 = New DevExpress.XtraTab.XtraTabPage
        Me.Label23 = New System.Windows.Forms.Label
        Me.txtMTInventoryUOM = New DevExpress.XtraEditors.LookUpEdit
        Me.lblMTInventoryUOM = New System.Windows.Forms.Label
        Me.HorizonalRuleLine3D2 = New Power.Forms.HorizonalRuleLine3D
        Me.PanelControl5 = New DevExpress.XtraEditors.PanelControl
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.XtraTabPage5 = New DevExpress.XtraTab.XtraTabPage
        Me.Label16 = New System.Windows.Forms.Label
        Me.btnRemoveSheetSize = New DevExpress.XtraEditors.SimpleButton
        Me.dgStockedItems = New DevExpress.XtraGrid.GridControl
        Me.gvStockedItems = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colSIConversionToPrimary = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtSIConversion = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colMTPrimaryUOMShortName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSIName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.HorizonalRuleLine3D3 = New Power.Forms.HorizonalRuleLine3D
        Me.PanelControl6 = New DevExpress.XtraEditors.PanelControl
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.XtraTabPage6 = New DevExpress.XtraTab.XtraTabPage
        Me.PanelControl11 = New DevExpress.XtraEditors.PanelControl
        Me.lblFinishDesc2 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.lblFinishDesc1 = New System.Windows.Forms.Label
        Me.PanelControl12 = New DevExpress.XtraEditors.PanelControl
        Me.HorizonalRuleLine3D8 = New Power.Forms.HorizonalRuleLine3D
        Me.btnBack = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.btnNext = New DevExpress.XtraEditors.SimpleButton
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton
        CType(Me.DsHeadOffice, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsPrimaryUOM, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsInventoryUOM, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.txtMTName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        Me.XtraTabPage3.SuspendLayout()
        CType(Me.txtMTPrimaryUOM.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        Me.XtraTabPage4.SuspendLayout()
        CType(Me.txtMTInventoryUOM.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl5.SuspendLayout()
        Me.XtraTabPage5.SuspendLayout()
        CType(Me.dgStockedItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvStockedItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSIConversion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl6.SuspendLayout()
        Me.XtraTabPage6.SuspendLayout()
        CType(Me.PanelControl11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl11.SuspendLayout()
        CType(Me.PanelControl12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DsHeadOffice
        '
        Me.DsHeadOffice.DataSetName = "dsHeadOffice"
        Me.DsHeadOffice.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'daStockedItems
        '
        Me.daStockedItems.DeleteCommand = Me.SqlDeleteCommand3
        Me.daStockedItems.InsertCommand = Me.SqlInsertCommand3
        Me.daStockedItems.SelectCommand = Me.SqlSelectCommand3
        Me.daStockedItems.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VMaster_StockedItems", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("MSIID", "MSIID"), New System.Data.Common.DataColumnMapping("MMTID", "MMTID"), New System.Data.Common.DataColumnMapping("MSIConversionToPrimary", "MSIConversionToPrimary")})})
        Me.daStockedItems.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM Master_StockedItems WHERE (MSIID = @Original_MSIID) AND (MMTID = @Ori" & _
        "ginal_MMTID) AND (MSIConversionToPrimary = @Original_MSIConversionToPrimary OR @" & _
        "Original_MSIConversionToPrimary IS NULL AND MSIConversionToPrimary IS NULL)"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MSIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MSIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MSIConversionToPrimary", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MSIConversionToPrimary", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = CType(configurationAppSettings.GetValue("SqlConnection.ConnectionString", GetType(System.String)), String)
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO Master_StockedItems (MMTID, MSIConversionToPrimary) VALUES (@MMTID, @" & _
        "MSIConversionToPrimary); SELECT MSIID, MMTID, MSIConversionToPrimary, MMTPrimary" & _
        "UOM, MSIName, MSIInventoryUOM, MMTPrimaryUOMShortName FROM VMaster_StockedItems " & _
        "WHERE (MSIID = IDENT_CURRENT('Master_StockedItems'))"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTID", System.Data.SqlDbType.Int, 4, "MMTID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MSIConversionToPrimary", System.Data.SqlDbType.Real, 4, "MSIConversionToPrimary"))
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT MSIID, MMTID, MSIConversionToPrimary, MMTPrimaryUOM, MSIName, MSIInventory" & _
        "UOM, MMTPrimaryUOMShortName FROM VMaster_StockedItems WHERE (MMTID = @MMTID)"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection
        Me.SqlSelectCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTID", System.Data.SqlDbType.Int, 4, "MMTID"))
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE Master_StockedItems SET MMTID = @MMTID, MSIConversionToPrimary = @MSIConve" & _
        "rsionToPrimary WHERE (MSIID = @Original_MSIID) AND (MMTID = @Original_MMTID) AND" & _
        " (MSIConversionToPrimary = @Original_MSIConversionToPrimary OR @Original_MSIConv" & _
        "ersionToPrimary IS NULL AND MSIConversionToPrimary IS NULL); SELECT MSIID, MMTID" & _
        ", MSIConversionToPrimary, MMTPrimaryUOM, MSIName, MSIInventoryUOM, MMTPrimaryUOM" & _
        "ShortName FROM VMaster_StockedItems WHERE (MSIID = @MSIID)"
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTID", System.Data.SqlDbType.Int, 4, "MMTID"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MSIConversionToPrimary", System.Data.SqlDbType.Real, 4, "MSIConversionToPrimary"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MSIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MSIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MSIConversionToPrimary", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MSIConversionToPrimary", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MSIID", System.Data.SqlDbType.Int, 4, "MSIID"))
        '
        'daUOM
        '
        Me.daUOM.DeleteCommand = Me.SqlDeleteCommand2
        Me.daUOM.InsertCommand = Me.SqlInsertCommand2
        Me.daUOM.SelectCommand = Me.SqlSelectCommand2
        Me.daUOM.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "UOM", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("UOMID", "UOMID"), New System.Data.Common.DataColumnMapping("UOMLongName", "UOMLongName"), New System.Data.Common.DataColumnMapping("UOMShortName", "UOMShortName")})})
        Me.daUOM.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM UOM WHERE (UOMID = @Original_UOMID) AND (UOMLongName = @Original_UOML" & _
        "ongName OR @Original_UOMLongName IS NULL AND UOMLongName IS NULL) AND (UOMShortN" & _
        "ame = @Original_UOMShortName OR @Original_UOMShortName IS NULL AND UOMShortName " & _
        "IS NULL)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UOMID", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UOMID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UOMLongName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UOMLongName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UOMShortName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UOMShortName", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO UOM(UOMID, UOMLongName, UOMShortName) VALUES (@UOMID, @UOMLongName, @" & _
        "UOMShortName); SELECT UOMID, UOMLongName, UOMShortName FROM UOM WHERE (UOMID = @" & _
        "UOMID)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UOMID", System.Data.SqlDbType.VarChar, 2, "UOMID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UOMLongName", System.Data.SqlDbType.VarChar, 50, "UOMLongName"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UOMShortName", System.Data.SqlDbType.VarChar, 50, "UOMShortName"))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT UOMID, UOMLongName, UOMShortName FROM UOM"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE UOM SET UOMID = @UOMID, UOMLongName = @UOMLongName, UOMShortName = @UOMSho" & _
        "rtName WHERE (UOMID = @Original_UOMID) AND (UOMLongName = @Original_UOMLongName " & _
        "OR @Original_UOMLongName IS NULL AND UOMLongName IS NULL) AND (UOMShortName = @O" & _
        "riginal_UOMShortName OR @Original_UOMShortName IS NULL AND UOMShortName IS NULL)" & _
        "; SELECT UOMID, UOMLongName, UOMShortName FROM UOM WHERE (UOMID = @UOMID)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UOMID", System.Data.SqlDbType.VarChar, 2, "UOMID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UOMLongName", System.Data.SqlDbType.VarChar, 50, "UOMLongName"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UOMShortName", System.Data.SqlDbType.VarChar, 50, "UOMShortName"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UOMID", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UOMID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UOMLongName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UOMLongName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UOMShortName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UOMShortName", System.Data.DataRowVersion.Original, Nothing))
        '
        'daMaterialGroups
        '
        Me.daMaterialGroups.DeleteCommand = Me.SqlDeleteCommand4
        Me.daMaterialGroups.InsertCommand = Me.SqlInsertCommand4
        Me.daMaterialGroups.SelectCommand = Me.SqlSelectCommand4
        Me.daMaterialGroups.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "MaterialGroups", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("MGID", "MGID"), New System.Data.Common.DataColumnMapping("MGInventoryUOM", "MGInventoryUOM"), New System.Data.Common.DataColumnMapping("MGPrimaryUOM", "MGPrimaryUOM"), New System.Data.Common.DataColumnMapping("MGStocked", "MGStocked"), New System.Data.Common.DataColumnMapping("MGName", "MGName"), New System.Data.Common.DataColumnMapping("MGControlledAtHeadOffice", "MGControlledAtHeadOffice"), New System.Data.Common.DataColumnMapping("MGAssignToPortion", "MGAssignToPortion")})})
        Me.daMaterialGroups.UpdateCommand = Me.SqlUpdateCommand4
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = "DELETE FROM MaterialGroups WHERE (MGID = @Original_MGID) AND (MGAssignToPortion =" & _
        " @Original_MGAssignToPortion OR @Original_MGAssignToPortion IS NULL AND MGAssign" & _
        "ToPortion IS NULL) AND (MGControlledAtHeadOffice = @Original_MGControlledAtHeadO" & _
        "ffice OR @Original_MGControlledAtHeadOffice IS NULL AND MGControlledAtHeadOffice" & _
        " IS NULL) AND (MGInventoryUOM = @Original_MGInventoryUOM OR @Original_MGInventor" & _
        "yUOM IS NULL AND MGInventoryUOM IS NULL) AND (MGName = @Original_MGName OR @Orig" & _
        "inal_MGName IS NULL AND MGName IS NULL) AND (MGPrimaryUOM = @Original_MGPrimaryU" & _
        "OM OR @Original_MGPrimaryUOM IS NULL AND MGPrimaryUOM IS NULL) AND (MGStocked = " & _
        "@Original_MGStocked OR @Original_MGStocked IS NULL AND MGStocked IS NULL)"
        Me.SqlDeleteCommand4.Connection = Me.SqlConnection
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGAssignToPortion", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGAssignToPortion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGControlledAtHeadOffice", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGControlledAtHeadOffice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGInventoryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGInventoryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGPrimaryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGPrimaryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGStocked", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGStocked", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = "INSERT INTO MaterialGroups(MGInventoryUOM, MGPrimaryUOM, MGStocked, MGName, MGCon" & _
        "trolledAtHeadOffice, MGAssignToPortion) VALUES (@MGInventoryUOM, @MGPrimaryUOM, " & _
        "@MGStocked, @MGName, @MGControlledAtHeadOffice, @MGAssignToPortion); SELECT MGID" & _
        ", MGInventoryUOM, MGPrimaryUOM, MGStocked, MGName, MGControlledAtHeadOffice, MGA" & _
        "ssignToPortion FROM MaterialGroups WHERE (MGID = @@IDENTITY)"
        Me.SqlInsertCommand4.Connection = Me.SqlConnection
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGInventoryUOM", System.Data.SqlDbType.VarChar, 2, "MGInventoryUOM"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGPrimaryUOM", System.Data.SqlDbType.VarChar, 2, "MGPrimaryUOM"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGStocked", System.Data.SqlDbType.Bit, 1, "MGStocked"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGName", System.Data.SqlDbType.VarChar, 50, "MGName"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGControlledAtHeadOffice", System.Data.SqlDbType.Bit, 1, "MGControlledAtHeadOffice"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGAssignToPortion", System.Data.SqlDbType.VarChar, 2, "MGAssignToPortion"))
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT MGID, MGInventoryUOM, MGPrimaryUOM, MGStocked, MGName, MGControlledAtHeadO" & _
        "ffice, MGAssignToPortion FROM MaterialGroups WHERE (MGID = @MGID)"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection
        Me.SqlSelectCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = "UPDATE MaterialGroups SET MGInventoryUOM = @MGInventoryUOM, MGPrimaryUOM = @MGPri" & _
        "maryUOM, MGStocked = @MGStocked, MGName = @MGName, MGControlledAtHeadOffice = @M" & _
        "GControlledAtHeadOffice, MGAssignToPortion = @MGAssignToPortion WHERE (MGID = @O" & _
        "riginal_MGID) AND (MGAssignToPortion = @Original_MGAssignToPortion OR @Original_" & _
        "MGAssignToPortion IS NULL AND MGAssignToPortion IS NULL) AND (MGControlledAtHead" & _
        "Office = @Original_MGControlledAtHeadOffice OR @Original_MGControlledAtHeadOffic" & _
        "e IS NULL AND MGControlledAtHeadOffice IS NULL) AND (MGInventoryUOM = @Original_" & _
        "MGInventoryUOM OR @Original_MGInventoryUOM IS NULL AND MGInventoryUOM IS NULL) A" & _
        "ND (MGName = @Original_MGName OR @Original_MGName IS NULL AND MGName IS NULL) AN" & _
        "D (MGPrimaryUOM = @Original_MGPrimaryUOM OR @Original_MGPrimaryUOM IS NULL AND M" & _
        "GPrimaryUOM IS NULL) AND (MGStocked = @Original_MGStocked OR @Original_MGStocked" & _
        " IS NULL AND MGStocked IS NULL); SELECT MGID, MGInventoryUOM, MGPrimaryUOM, MGSt" & _
        "ocked, MGName, MGControlledAtHeadOffice, MGAssignToPortion FROM MaterialGroups W" & _
        "HERE (MGID = @MGID)"
        Me.SqlUpdateCommand4.Connection = Me.SqlConnection
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGInventoryUOM", System.Data.SqlDbType.VarChar, 2, "MGInventoryUOM"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGPrimaryUOM", System.Data.SqlDbType.VarChar, 2, "MGPrimaryUOM"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGStocked", System.Data.SqlDbType.Bit, 1, "MGStocked"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGName", System.Data.SqlDbType.VarChar, 50, "MGName"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGControlledAtHeadOffice", System.Data.SqlDbType.Bit, 1, "MGControlledAtHeadOffice"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGAssignToPortion", System.Data.SqlDbType.VarChar, 2, "MGAssignToPortion"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGAssignToPortion", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGAssignToPortion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGControlledAtHeadOffice", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGControlledAtHeadOffice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGInventoryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGInventoryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGPrimaryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGPrimaryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGStocked", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGStocked", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        '
        'DsPrimaryUOM
        '
        Me.DsPrimaryUOM.DataSetName = "dsUOM"
        Me.DsPrimaryUOM.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Master_Materials", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("MMTID", "MMTID"), New System.Data.Common.DataColumnMapping("MMTName", "MMTName"), New System.Data.Common.DataColumnMapping("MMTInventoryUOM", "MMTInventoryUOM"), New System.Data.Common.DataColumnMapping("MMTPrimaryUOM", "MMTPrimaryUOM"), New System.Data.Common.DataColumnMapping("MMTStocked", "MMTStocked"), New System.Data.Common.DataColumnMapping("MGID", "MGID")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Master_Materials WHERE (MMTID = @Original_MMTID) AND (MGID = @Origina" & _
        "l_MGID OR @Original_MGID IS NULL AND MGID IS NULL) AND (MMTInventoryUOM = @Origi" & _
        "nal_MMTInventoryUOM OR @Original_MMTInventoryUOM IS NULL AND MMTInventoryUOM IS " & _
        "NULL) AND (MMTName = @Original_MMTName OR @Original_MMTName IS NULL AND MMTName " & _
        "IS NULL) AND (MMTPrimaryUOM = @Original_MMTPrimaryUOM OR @Original_MMTPrimaryUOM" & _
        " IS NULL AND MMTPrimaryUOM IS NULL) AND (MMTStocked = @Original_MMTStocked OR @O" & _
        "riginal_MMTStocked IS NULL AND MMTStocked IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTInventoryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTInventoryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTPrimaryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTPrimaryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTStocked", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTStocked", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Master_Materials(MMTName, MMTInventoryUOM, MMTPrimaryUOM, MMTStocked," & _
        " MGID) VALUES (@MMTName, @MMTInventoryUOM, @MMTPrimaryUOM, @MMTStocked, @MGID); " & _
        "SELECT MMTID, MMTName, MMTInventoryUOM, MMTPrimaryUOM, MMTStocked, MGID FROM Mas" & _
        "ter_Materials WHERE (MMTID = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTName", System.Data.SqlDbType.VarChar, 50, "MMTName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTInventoryUOM", System.Data.SqlDbType.VarChar, 2, "MMTInventoryUOM"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTPrimaryUOM", System.Data.SqlDbType.VarChar, 2, "MMTPrimaryUOM"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTStocked", System.Data.SqlDbType.Bit, 1, "MMTStocked"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT MMTID, MMTName, MMTInventoryUOM, MMTPrimaryUOM, MMTStocked, MGID FROM Mast" & _
        "er_Materials WHERE (MMTID = @MMTID)"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTID", System.Data.SqlDbType.Int, 4, "MMTID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Master_Materials SET MMTName = @MMTName, MMTInventoryUOM = @MMTInventoryUO" & _
        "M, MMTPrimaryUOM = @MMTPrimaryUOM, MMTStocked = @MMTStocked, MGID = @MGID WHERE " & _
        "(MMTID = @Original_MMTID) AND (MGID = @Original_MGID OR @Original_MGID IS NULL A" & _
        "ND MGID IS NULL) AND (MMTInventoryUOM = @Original_MMTInventoryUOM OR @Original_M" & _
        "MTInventoryUOM IS NULL AND MMTInventoryUOM IS NULL) AND (MMTName = @Original_MMT" & _
        "Name OR @Original_MMTName IS NULL AND MMTName IS NULL) AND (MMTPrimaryUOM = @Ori" & _
        "ginal_MMTPrimaryUOM OR @Original_MMTPrimaryUOM IS NULL AND MMTPrimaryUOM IS NULL" & _
        ") AND (MMTStocked = @Original_MMTStocked OR @Original_MMTStocked IS NULL AND MMT" & _
        "Stocked IS NULL); SELECT MMTID, MMTName, MMTInventoryUOM, MMTPrimaryUOM, MMTStoc" & _
        "ked, MGID FROM Master_Materials WHERE (MMTID = @MMTID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTName", System.Data.SqlDbType.VarChar, 50, "MMTName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTInventoryUOM", System.Data.SqlDbType.VarChar, 2, "MMTInventoryUOM"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTPrimaryUOM", System.Data.SqlDbType.VarChar, 2, "MMTPrimaryUOM"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTStocked", System.Data.SqlDbType.Bit, 1, "MMTStocked"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTInventoryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTInventoryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTPrimaryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTPrimaryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTStocked", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTStocked", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTID", System.Data.SqlDbType.Int, 4, "MMTID"))
        '
        'DsInventoryUOM
        '
        Me.DsInventoryUOM.DataSetName = "dsUOM"
        Me.DsInventoryUOM.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.PaintStyleName = "Flat"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(490, 352)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.XtraTabPage3, Me.XtraTabPage4, Me.XtraTabPage5, Me.XtraTabPage6})
        Me.XtraTabControl1.TabStop = False
        Me.XtraTabControl1.Text = "XtraTabControl1"
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.PanelControl2)
        Me.XtraTabPage1.Controls.Add(Me.PanelControl1)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(490, 330)
        Me.XtraTabPage1.Text = "Intro"
        '
        'PanelControl2
        '
        Me.PanelControl2.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.PanelControl2.Appearance.Options.UseBackColor = True
        Me.PanelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl2.Controls.Add(Me.lblIntroTitle)
        Me.PanelControl2.Controls.Add(Me.lblIntroDesc)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl2.Location = New System.Drawing.Point(120, 0)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(370, 330)
        Me.PanelControl2.TabIndex = 1
        Me.PanelControl2.Text = "PanelControl2"
        '
        'lblIntroTitle
        '
        Me.lblIntroTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblIntroTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblIntroTitle.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIntroTitle.Location = New System.Drawing.Point(16, 24)
        Me.lblIntroTitle.Name = "lblIntroTitle"
        Me.lblIntroTitle.Size = New System.Drawing.Size(338, 48)
        Me.lblIntroTitle.TabIndex = 0
        Me.lblIntroTitle.Text = "Welcome to the Add New Material Wizard"
        '
        'lblIntroDesc
        '
        Me.lblIntroDesc.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblIntroDesc.BackColor = System.Drawing.Color.Transparent
        Me.lblIntroDesc.Location = New System.Drawing.Point(16, 80)
        Me.lblIntroDesc.Name = "lblIntroDesc"
        Me.lblIntroDesc.Size = New System.Drawing.Size(338, 56)
        Me.lblIntroDesc.TabIndex = 1
        Me.lblIntroDesc.Text = "This wizard guides you through adding a new material to your business setup."
        '
        'PanelControl1
        '
        Me.PanelControl1.Appearance.BackColor = System.Drawing.Color.MidnightBlue
        Me.PanelControl1.Appearance.Options.UseBackColor = True
        Me.PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(120, 330)
        Me.PanelControl1.TabIndex = 0
        Me.PanelControl1.Text = "PanelControl1"
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.txtMTName)
        Me.XtraTabPage2.Controls.Add(Me.Label9)
        Me.XtraTabPage2.Controls.Add(Me.HorizonalRuleLine3D9)
        Me.XtraTabPage2.Controls.Add(Me.PanelControl3)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(490, 330)
        Me.XtraTabPage2.Text = "Name"
        '
        'txtMTName
        '
        Me.txtMTName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsHeadOffice, "Master_Materials.MMTName"))
        Me.txtMTName.EditValue = ""
        Me.txtMTName.Location = New System.Drawing.Point(104, 160)
        Me.txtMTName.Name = "txtMTName"
        Me.txtMTName.Size = New System.Drawing.Size(336, 20)
        Me.txtMTName.TabIndex = 3
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(48, 160)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(48, 21)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "Name:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'HorizonalRuleLine3D9
        '
        Me.HorizonalRuleLine3D9.BackColor = System.Drawing.SystemColors.Control
        Me.HorizonalRuleLine3D9.Dock = System.Windows.Forms.DockStyle.Top
        Me.HorizonalRuleLine3D9.Location = New System.Drawing.Point(0, 56)
        Me.HorizonalRuleLine3D9.Name = "HorizonalRuleLine3D9"
        Me.HorizonalRuleLine3D9.Size = New System.Drawing.Size(490, 2)
        Me.HorizonalRuleLine3D9.TabIndex = 1
        '
        'PanelControl3
        '
        Me.PanelControl3.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.PanelControl3.Appearance.Options.UseBackColor = True
        Me.PanelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl3.Controls.Add(Me.Label3)
        Me.PanelControl3.Controls.Add(Me.Label2)
        Me.PanelControl3.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl3.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(490, 56)
        Me.PanelControl3.TabIndex = 0
        Me.PanelControl3.Text = "PanelControl3"
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Location = New System.Drawing.Point(24, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(450, 16)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Enter the name of the material."
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(112, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Name"
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.Label22)
        Me.XtraTabPage3.Controls.Add(Me.txtMTPrimaryUOM)
        Me.XtraTabPage3.Controls.Add(Me.Label10)
        Me.XtraTabPage3.Controls.Add(Me.HorizonalRuleLine3D1)
        Me.XtraTabPage3.Controls.Add(Me.PanelControl4)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(490, 330)
        Me.XtraTabPage3.Text = "Primary Unit of Measure"
        '
        'Label22
        '
        Me.Label22.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label22.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(64, 136)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(360, 40)
        Me.Label22.TabIndex = 2
        Me.Label22.Text = "You must enter the primary unit of measure for this material.  This is the unit o" & _
        "f measure in which this material is sold and costed into jobs."
        '
        'txtMTPrimaryUOM
        '
        Me.txtMTPrimaryUOM.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsHeadOffice, "Master_Materials.MMTPrimaryUOM"))
        Me.txtMTPrimaryUOM.Location = New System.Drawing.Point(216, 184)
        Me.txtMTPrimaryUOM.Name = "txtMTPrimaryUOM"
        '
        'txtMTPrimaryUOM.Properties
        '
        Me.txtMTPrimaryUOM.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True
        Me.txtMTPrimaryUOM.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtMTPrimaryUOM.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("UOMLongName")})
        Me.txtMTPrimaryUOM.Properties.DataSource = Me.DsPrimaryUOM.UOM
        Me.txtMTPrimaryUOM.Properties.DisplayMember = "UOMLongName"
        Me.txtMTPrimaryUOM.Properties.NullText = "<Group default - >"
        Me.txtMTPrimaryUOM.Properties.ShowFooter = False
        Me.txtMTPrimaryUOM.Properties.ShowHeader = False
        Me.txtMTPrimaryUOM.Properties.ValueMember = "UOMID"
        Me.txtMTPrimaryUOM.Size = New System.Drawing.Size(208, 20)
        Me.txtMTPrimaryUOM.TabIndex = 4
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(64, 184)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(136, 21)
        Me.Label10.TabIndex = 3
        Me.Label10.Text = "Primary unit of measure:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'HorizonalRuleLine3D1
        '
        Me.HorizonalRuleLine3D1.BackColor = System.Drawing.SystemColors.Control
        Me.HorizonalRuleLine3D1.Dock = System.Windows.Forms.DockStyle.Top
        Me.HorizonalRuleLine3D1.Location = New System.Drawing.Point(0, 56)
        Me.HorizonalRuleLine3D1.Name = "HorizonalRuleLine3D1"
        Me.HorizonalRuleLine3D1.Size = New System.Drawing.Size(490, 2)
        Me.HorizonalRuleLine3D1.TabIndex = 1
        '
        'PanelControl4
        '
        Me.PanelControl4.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.PanelControl4.Appearance.Options.UseBackColor = True
        Me.PanelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl4.Controls.Add(Me.Label1)
        Me.PanelControl4.Controls.Add(Me.Label4)
        Me.PanelControl4.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl4.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(490, 56)
        Me.PanelControl4.TabIndex = 0
        Me.PanelControl4.Text = "PanelControl4"
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Location = New System.Drawing.Point(24, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(450, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Select the primary unit of measure."
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(8, 8)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(256, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Primary Unit of Measure"
        '
        'XtraTabPage4
        '
        Me.XtraTabPage4.Controls.Add(Me.Label23)
        Me.XtraTabPage4.Controls.Add(Me.txtMTInventoryUOM)
        Me.XtraTabPage4.Controls.Add(Me.lblMTInventoryUOM)
        Me.XtraTabPage4.Controls.Add(Me.HorizonalRuleLine3D2)
        Me.XtraTabPage4.Controls.Add(Me.PanelControl5)
        Me.XtraTabPage4.Name = "XtraTabPage4"
        Me.XtraTabPage4.Size = New System.Drawing.Size(490, 330)
        Me.XtraTabPage4.Text = "Inventory Unit of Measure"
        '
        'Label23
        '
        Me.Label23.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label23.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(64, 136)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(360, 40)
        Me.Label23.TabIndex = 2
        Me.Label23.Text = "You must enter the inventory unit of measure for this material.  This is the unit" & _
        " of measure in which this material counted in inventory."
        '
        'txtMTInventoryUOM
        '
        Me.txtMTInventoryUOM.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsHeadOffice, "Master_Materials.MMTInventoryUOM"))
        Me.txtMTInventoryUOM.Location = New System.Drawing.Point(216, 184)
        Me.txtMTInventoryUOM.Name = "txtMTInventoryUOM"
        '
        'txtMTInventoryUOM.Properties
        '
        Me.txtMTInventoryUOM.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtMTInventoryUOM.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("UOMLongName")})
        Me.txtMTInventoryUOM.Properties.DataSource = Me.DsInventoryUOM.UOM
        Me.txtMTInventoryUOM.Properties.DisplayMember = "UOMLongName"
        Me.txtMTInventoryUOM.Properties.NullText = "<Group default - >"
        Me.txtMTInventoryUOM.Properties.ShowFooter = False
        Me.txtMTInventoryUOM.Properties.ShowHeader = False
        Me.txtMTInventoryUOM.Properties.ValueMember = "UOMID"
        Me.txtMTInventoryUOM.Size = New System.Drawing.Size(208, 20)
        Me.txtMTInventoryUOM.TabIndex = 4
        '
        'lblMTInventoryUOM
        '
        Me.lblMTInventoryUOM.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMTInventoryUOM.Location = New System.Drawing.Point(64, 184)
        Me.lblMTInventoryUOM.Name = "lblMTInventoryUOM"
        Me.lblMTInventoryUOM.Size = New System.Drawing.Size(144, 21)
        Me.lblMTInventoryUOM.TabIndex = 3
        Me.lblMTInventoryUOM.Text = "Inventory unit of measure:"
        Me.lblMTInventoryUOM.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'HorizonalRuleLine3D2
        '
        Me.HorizonalRuleLine3D2.BackColor = System.Drawing.SystemColors.Control
        Me.HorizonalRuleLine3D2.Dock = System.Windows.Forms.DockStyle.Top
        Me.HorizonalRuleLine3D2.Location = New System.Drawing.Point(0, 56)
        Me.HorizonalRuleLine3D2.Name = "HorizonalRuleLine3D2"
        Me.HorizonalRuleLine3D2.Size = New System.Drawing.Size(490, 2)
        Me.HorizonalRuleLine3D2.TabIndex = 1
        '
        'PanelControl5
        '
        Me.PanelControl5.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.PanelControl5.Appearance.Options.UseBackColor = True
        Me.PanelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl5.Controls.Add(Me.Label5)
        Me.PanelControl5.Controls.Add(Me.Label6)
        Me.PanelControl5.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl5.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl5.Name = "PanelControl5"
        Me.PanelControl5.Size = New System.Drawing.Size(490, 56)
        Me.PanelControl5.TabIndex = 0
        Me.PanelControl5.Text = "PanelControl5"
        '
        'Label5
        '
        Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Location = New System.Drawing.Point(24, 32)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(450, 16)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Select the inventory unit of measure."
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(8, 8)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(240, 16)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Inventory Unit of Measure"
        '
        'XtraTabPage5
        '
        Me.XtraTabPage5.Controls.Add(Me.Label16)
        Me.XtraTabPage5.Controls.Add(Me.btnRemoveSheetSize)
        Me.XtraTabPage5.Controls.Add(Me.dgStockedItems)
        Me.XtraTabPage5.Controls.Add(Me.HorizonalRuleLine3D3)
        Me.XtraTabPage5.Controls.Add(Me.PanelControl6)
        Me.XtraTabPage5.Name = "XtraTabPage5"
        Me.XtraTabPage5.Size = New System.Drawing.Size(490, 330)
        Me.XtraTabPage5.Text = "Sheet Sizes"
        '
        'Label16
        '
        Me.Label16.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(8, 64)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(472, 16)
        Me.Label16.TabIndex = 2
        Me.Label16.Text = "Enter the sheet sizes for this material."
        '
        'btnRemoveSheetSize
        '
        Me.btnRemoveSheetSize.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveSheetSize.Image = CType(resources.GetObject("btnRemoveSheetSize.Image"), System.Drawing.Image)
        Me.btnRemoveSheetSize.Location = New System.Drawing.Point(8, 296)
        Me.btnRemoveSheetSize.Name = "btnRemoveSheetSize"
        Me.btnRemoveSheetSize.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveSheetSize.TabIndex = 4
        Me.btnRemoveSheetSize.Text = "Remove"
        '
        'dgStockedItems
        '
        Me.dgStockedItems.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgStockedItems.DataSource = Me.DsHeadOffice.VMaster_StockedItems
        '
        'dgStockedItems.EmbeddedNavigator
        '
        Me.dgStockedItems.EmbeddedNavigator.Name = ""
        Me.dgStockedItems.Location = New System.Drawing.Point(8, 80)
        Me.dgStockedItems.MainView = Me.gvStockedItems
        Me.dgStockedItems.Name = "dgStockedItems"
        Me.dgStockedItems.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtSIConversion})
        Me.dgStockedItems.Size = New System.Drawing.Size(472, 208)
        Me.dgStockedItems.Styles.AddReplace("CardBorder", New DevExpress.Utils.ViewStyleEx("CardBorder", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.InactiveBorder, System.Drawing.SystemColors.WindowFrame, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("BandPanelBackground", New DevExpress.Utils.ViewStyleEx("BandPanelBackground", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ControlDark, System.Drawing.Color.DarkSalmon, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("EmptySpace", New DevExpress.Utils.ViewStyleEx("EmptySpace", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("FieldValue", New DevExpress.Utils.ViewStyleEx("FieldValue", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("BandPanel", New DevExpress.Utils.ViewStyleEx("BandPanel", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("CardButton", New DevExpress.Utils.ViewStyleEx("CardButton", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("FocusedCardCaption", New DevExpress.Utils.ViewStyleEx("FocusedCardCaption", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ActiveCaption, System.Drawing.SystemColors.ActiveCaptionText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("CardCaption", New DevExpress.Utils.ViewStyleEx("CardCaption", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.InactiveCaption, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("HeaderPanelBackground", New DevExpress.Utils.ViewStyleEx("HeaderPanelBackground", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ControlDark, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("SeparatorLine", New DevExpress.Utils.ViewStyleEx("SeparatorLine", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ActiveBorder, System.Drawing.SystemColors.ActiveBorder, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("FieldCaption", New DevExpress.Utils.ViewStyleEx("FieldCaption", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Top, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.TabIndex = 3
        Me.dgStockedItems.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvStockedItems})
        '
        'gvStockedItems
        '
        Me.gvStockedItems.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvStockedItems.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvStockedItems.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvStockedItems.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvStockedItems.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvStockedItems.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvStockedItems.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvStockedItems.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvStockedItems.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvStockedItems.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvStockedItems.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvStockedItems.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvStockedItems.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvStockedItems.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvStockedItems.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvStockedItems.Appearance.VertLine.Options.UseBackColor = True
        Me.gvStockedItems.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSIConversionToPrimary, Me.colMTPrimaryUOMShortName, Me.colSIName})
        Me.gvStockedItems.GridControl = Me.dgStockedItems
        Me.gvStockedItems.Name = "gvStockedItems"
        Me.gvStockedItems.OptionsCustomization.AllowFilter = False
        Me.gvStockedItems.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvStockedItems.OptionsView.ShowGroupPanel = False
        Me.gvStockedItems.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colSIConversionToPrimary, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colSIConversionToPrimary
        '
        Me.colSIConversionToPrimary.Caption = "Sheet Size"
        Me.colSIConversionToPrimary.ColumnEdit = Me.txtSIConversion
        Me.colSIConversionToPrimary.FieldName = "MSIConversionToPrimary"
        Me.colSIConversionToPrimary.Name = "colSIConversionToPrimary"
        Me.colSIConversionToPrimary.Visible = True
        Me.colSIConversionToPrimary.VisibleIndex = 0
        '
        'txtSIConversion
        '
        Me.txtSIConversion.AutoHeight = False
        Me.txtSIConversion.Name = "txtSIConversion"
        '
        'colMTPrimaryUOMShortName
        '
        Me.colMTPrimaryUOMShortName.FieldName = "MMTPrimaryUOMShortName"
        Me.colMTPrimaryUOMShortName.Name = "colMTPrimaryUOMShortName"
        Me.colMTPrimaryUOMShortName.OptionsColumn.AllowEdit = False
        Me.colMTPrimaryUOMShortName.OptionsColumn.AllowFocus = False
        Me.colMTPrimaryUOMShortName.OptionsColumn.ReadOnly = True
        Me.colMTPrimaryUOMShortName.Visible = True
        Me.colMTPrimaryUOMShortName.VisibleIndex = 1
        Me.colMTPrimaryUOMShortName.Width = 42
        '
        'colSIName
        '
        Me.colSIName.Caption = "Display Name"
        Me.colSIName.FieldName = "MSIName"
        Me.colSIName.Name = "colSIName"
        Me.colSIName.OptionsColumn.AllowEdit = False
        Me.colSIName.OptionsColumn.AllowFocus = False
        Me.colSIName.OptionsColumn.ReadOnly = True
        Me.colSIName.Visible = True
        Me.colSIName.VisibleIndex = 2
        Me.colSIName.Width = 237
        '
        'HorizonalRuleLine3D3
        '
        Me.HorizonalRuleLine3D3.BackColor = System.Drawing.SystemColors.Control
        Me.HorizonalRuleLine3D3.Dock = System.Windows.Forms.DockStyle.Top
        Me.HorizonalRuleLine3D3.Location = New System.Drawing.Point(0, 56)
        Me.HorizonalRuleLine3D3.Name = "HorizonalRuleLine3D3"
        Me.HorizonalRuleLine3D3.Size = New System.Drawing.Size(490, 2)
        Me.HorizonalRuleLine3D3.TabIndex = 1
        '
        'PanelControl6
        '
        Me.PanelControl6.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.PanelControl6.Appearance.Options.UseBackColor = True
        Me.PanelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl6.Controls.Add(Me.Label7)
        Me.PanelControl6.Controls.Add(Me.Label8)
        Me.PanelControl6.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl6.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl6.Name = "PanelControl6"
        Me.PanelControl6.Size = New System.Drawing.Size(490, 56)
        Me.PanelControl6.TabIndex = 0
        Me.PanelControl6.Text = "PanelControl6"
        '
        'Label7
        '
        Me.Label7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Location = New System.Drawing.Point(24, 32)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(450, 16)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Enter the sheet sizes for this material."
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(8, 8)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(112, 16)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Sheet Sizes"
        '
        'XtraTabPage6
        '
        Me.XtraTabPage6.Controls.Add(Me.PanelControl11)
        Me.XtraTabPage6.Controls.Add(Me.PanelControl12)
        Me.XtraTabPage6.Name = "XtraTabPage6"
        Me.XtraTabPage6.Size = New System.Drawing.Size(490, 330)
        Me.XtraTabPage6.Text = "Finish"
        '
        'PanelControl11
        '
        Me.PanelControl11.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.PanelControl11.Appearance.Options.UseBackColor = True
        Me.PanelControl11.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl11.Controls.Add(Me.lblFinishDesc2)
        Me.PanelControl11.Controls.Add(Me.Label17)
        Me.PanelControl11.Controls.Add(Me.lblFinishDesc1)
        Me.PanelControl11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl11.Location = New System.Drawing.Point(120, 0)
        Me.PanelControl11.Name = "PanelControl11"
        Me.PanelControl11.Size = New System.Drawing.Size(370, 330)
        Me.PanelControl11.TabIndex = 7
        Me.PanelControl11.Text = "PanelControl11"
        '
        'lblFinishDesc2
        '
        Me.lblFinishDesc2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblFinishDesc2.BackColor = System.Drawing.Color.Transparent
        Me.lblFinishDesc2.Location = New System.Drawing.Point(16, 120)
        Me.lblFinishDesc2.Name = "lblFinishDesc2"
        Me.lblFinishDesc2.Size = New System.Drawing.Size(338, 40)
        Me.lblFinishDesc2.TabIndex = 4
        Me.lblFinishDesc2.Text = "Click 'Finish' to complete the wizard and add the new material to your business s" & _
        "etup."
        '
        'Label17
        '
        Me.Label17.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(16, 24)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(338, 48)
        Me.Label17.TabIndex = 3
        Me.Label17.Text = "Wizard Completed Successfully"
        '
        'lblFinishDesc1
        '
        Me.lblFinishDesc1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblFinishDesc1.BackColor = System.Drawing.Color.Transparent
        Me.lblFinishDesc1.Location = New System.Drawing.Point(16, 80)
        Me.lblFinishDesc1.Name = "lblFinishDesc1"
        Me.lblFinishDesc1.Size = New System.Drawing.Size(338, 32)
        Me.lblFinishDesc1.TabIndex = 0
        Me.lblFinishDesc1.Text = "The wizard now has enough information to add the new material."
        '
        'PanelControl12
        '
        Me.PanelControl12.Appearance.BackColor = System.Drawing.Color.MidnightBlue
        Me.PanelControl12.Appearance.Options.UseBackColor = True
        Me.PanelControl12.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl12.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelControl12.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl12.Name = "PanelControl12"
        Me.PanelControl12.Size = New System.Drawing.Size(120, 330)
        Me.PanelControl12.TabIndex = 6
        Me.PanelControl12.Text = "PanelControl12"
        '
        'HorizonalRuleLine3D8
        '
        Me.HorizonalRuleLine3D8.BackColor = System.Drawing.SystemColors.Control
        Me.HorizonalRuleLine3D8.Dock = System.Windows.Forms.DockStyle.Top
        Me.HorizonalRuleLine3D8.Location = New System.Drawing.Point(0, 352)
        Me.HorizonalRuleLine3D8.Name = "HorizonalRuleLine3D8"
        Me.HorizonalRuleLine3D8.Size = New System.Drawing.Size(490, 2)
        Me.HorizonalRuleLine3D8.TabIndex = 1
        '
        'btnBack
        '
        Me.btnBack.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBack.Enabled = False
        Me.btnBack.Location = New System.Drawing.Point(256, 360)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(72, 23)
        Me.btnBack.TabIndex = 3
        Me.btnBack.Text = "< &Back"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(408, 360)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 5
        Me.btnCancel.Text = "Cancel"
        '
        'btnNext
        '
        Me.btnNext.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNext.Location = New System.Drawing.Point(328, 360)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(72, 23)
        Me.btnNext.TabIndex = 4
        Me.btnNext.Text = "&Next >"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton1.Image = CType(resources.GetObject("SimpleButton1.Image"), System.Drawing.Image)
        Me.SimpleButton1.Location = New System.Drawing.Point(8, 360)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(72, 23)
        Me.SimpleButton1.TabIndex = 2
        Me.SimpleButton1.Text = "Help"
        '
        'frmMasterMaterialWizard
        '
        Me.AcceptButton = Me.btnNext
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(490, 392)
        Me.ControlBox = False
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.btnBack)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnNext)
        Me.Controls.Add(Me.HorizonalRuleLine3D8)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMasterMaterialWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add New Material Wizard"
        CType(Me.DsHeadOffice, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsPrimaryUOM, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsInventoryUOM, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.txtMTName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.XtraTabPage3.ResumeLayout(False)
        CType(Me.txtMTPrimaryUOM.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        Me.XtraTabPage4.ResumeLayout(False)
        CType(Me.txtMTInventoryUOM.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl5.ResumeLayout(False)
        Me.XtraTabPage5.ResumeLayout(False)
        CType(Me.dgStockedItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvStockedItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSIConversion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl6.ResumeLayout(False)
        Me.XtraTabPage6.ResumeLayout(False)
        CType(Me.PanelControl11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl11.ResumeLayout(False)
        CType(Me.PanelControl12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmMasterMaterialWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.XtraTabControl1.SelectedTabPageIndex = 0
        Me.XtraTabControl1.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False
    End Sub

    ' This data must be loaded BEFORE the DataRow is loaded (eg lookups etc)
    Private Sub FillPreliminaryData()
    End Sub

    Private Sub FillMGIDDependantData()
        ' Material Group
        daMaterialGroups.SelectCommand.Parameters("@MGID").Value = MGID
        daMaterialGroups.Fill(DsHeadOffice)

        ' UOM
        daUOM.Fill(DsPrimaryUOM)
        daUOM.Fill(DsInventoryUOM)
        Dim PrimaryNullText As String = "<Group default - " & DsPrimaryUOM.UOM.FindByUOMID(DsHeadOffice.MaterialGroups(0).MGPrimaryUOM).UOMLongName & ">"
        Dim InventoryNullText As String = "<Group default - " & DsInventoryUOM.UOM.FindByUOMID(DsHeadOffice.MaterialGroups(0).MGInventoryUOM).UOMLongName & ">"
        DsPrimaryUOM.UOM.AddUOMRow("", PrimaryNullText, DsPrimaryUOM.UOM.FindByUOMID(DsHeadOffice.MaterialGroups(0).MGPrimaryUOM).UOMShortName)
        DsInventoryUOM.UOM.AddUOMRow("", InventoryNullText, DsInventoryUOM.UOM.FindByUOMID(DsHeadOffice.MaterialGroups(0).MGInventoryUOM).UOMShortName)
        txtMTPrimaryUOM.Properties.NullText = PrimaryNullText
        txtMTInventoryUOM.Properties.NullText = InventoryNullText
    End Sub

    ' This data must be loaded AFTER the DataRow is loaded (eg record related data)
    Private Sub FillData()
        ' --- STOCKED ITEMS ---
        daStockedItems.SelectCommand.Parameters("@MMTID").Value = MMTID
        daStockedItems.Fill(DsHeadOffice)

        CustomizeScreen()
    End Sub

    Dim OK As Boolean = False
    Private Sub RunOK()
        ' EndEdit() to end editing the dataset record so that we can update
        DataRow.EndEdit()
        SqlDataAdapter.Update(DsHeadOffice)
        daStockedItems.Update(DsHeadOffice)

        DataAccess.spExecLockRequest("sp_ReleaseMasterMaterialLock", MMTID, Transaction)
        Transaction.Commit()
        SqlConnection.Close()

        OK = True
        Me.Close()
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        btnBack.Enabled = True
        If ValidateTab(XtraTabControl1.SelectedTabPageIndex) Then
            If XtraTabControl1.SelectedTabPageIndex = XtraTabControl1.TabPages.Count - 1 Then
                RunOK()
            Else
                XtraTabControl1.SelectedTabPageIndex = NextTabIndex(XtraTabControl1.SelectedTabPageIndex)
            End If
        End If
    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        XtraTabControl1.SelectedTabPageIndex = PreviousTabIndex(XtraTabControl1.SelectedTabPageIndex)
    End Sub

    Private Function NextTabIndex(ByVal CurrentTabIndex As Integer) As Integer
        If CurrentTabIndex = XtraTabControl1.TabPages.Count - 1 Then
            Throw New ArgumentOutOfRangeException("CurrentTabIndex", CurrentTabIndex)
        End If
        NextTabIndex = CurrentTabIndex + 1
        Select Case NextTabIndex
            Case 3 ' Inventory Unit of Measure
                If Not IsStocked() Then
                    Return NextTabIndex(NextTabIndex) ' This tab is not valid - get next
                End If
            Case 4 ' Sheet sizes
                If Not IsStocked() Then
                    Return NextTabIndex(NextTabIndex) ' This tab is not valid - get next
                End If
            Case XtraTabControl1.TabPages.Count - 1
                btnNext.Text = "&Finish"
                btnNext.DialogResult = DialogResult.OK
        End Select
        Return NextTabIndex ' Found the correct value
    End Function

    Private Function PreviousTabIndex(ByVal CurrentTabIndex As Integer) As Integer
        If CurrentTabIndex = XtraTabControl1.TabPages.Count - 1 Then
            btnNext.Text = "&Next >"
            btnNext.DialogResult = DialogResult.None
        End If
        PreviousTabIndex = CurrentTabIndex - 1
        Select Case PreviousTabIndex
            Case 0
                btnBack.Enabled = False
            Case 3 ' Inventory Unit of Measure
                If Not IsStocked() Then
                    Return PreviousTabIndex(PreviousTabIndex) ' This tab is not valid - get next
                End If
            Case 4 ' Sheet sizes
                If Not IsStocked() Then
                    Return PreviousTabIndex(PreviousTabIndex) ' This tab is not valid - get next
                End If
        End Select
        Return PreviousTabIndex ' Found the correct value
    End Function

    Private Function ValidateTab(ByVal Tab As Integer) As Boolean
        Select Case Tab
            Case 1
                If DataRow("MMTName") Is DBNull.Value Then
                    Message.ShowMessage("You must enter a name.", MessageBoxIcon.Exclamation)
                    Return False
                End If
        End Select
        Return True
    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private HasChanges As Boolean = False
    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            DataRow.EndEdit()
            If HasChanges Or DsHeadOffice.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If response = MsgBoxResult.Yes Then
                DataAccess.spExecLockRequest("sp_ReleaseMasterMaterialLock", MMTID, Transaction)
                Transaction.Rollback()
                SqlConnection.Close()
            Else
                e.Cancel = True
            End If
        End If
    End Sub

    Private Function IsStocked() As Boolean
        If DataRow("MMTStocked") Is DBNull.Value Then
            Return DsHeadOffice.MaterialGroups(0).MGStocked
        Else
            Return DataRow("MMTStocked")
        End If
    End Function

    Private Sub CustomizeScreen()
        Dim MGName As String = ""
        If Not DsHeadOffice.MaterialGroups(0)("MGName") Is DBNull.Value Then
            MGName = DsHeadOffice.MaterialGroups(0)("MGName")
        End If
    End Sub

#Region " Sheet Sizes "

    Private Sub gvStockedItems_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gvStockedItems.InitNewRow
        gvStockedItems.GetDataRow(e.RowHandle)("MMTID") = MMTID
    End Sub

    Private Sub gvStockedItems_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gvStockedItems.RowUpdated
        ' Update sheet sizes and refresh
        gvStockedItems.UpdateCurrentRow()
        daStockedItems.Update(DsHeadOffice)
        HasChanges = True
    End Sub

    Private Sub gvStockedItems_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles gvStockedItems.CellValueChanged
        If gvStockedItems.GetDataRow(e.RowHandle)("MSIConversionToPrimary") Is DBNull.Value Then
            If ExecuteScalar("SELECT dbo.MSIIsUsed(@MSIID)", CommandType.Text, New SqlParameter() {New SqlParameter("@MSIID", SelectedSheetSize("MSIID"))}, Transaction) Then
                Message.ShowMessage("You cannot delete this stocked item because it has either been used in a job or is currently being held in stock at a branch.", MessageBoxIcon.Exclamation)
                gvStockedItems.GetDataRow(e.RowHandle).CancelEdit()
            Else
                gvStockedItems.GetDataRow(e.RowHandle).Delete()
            End If
        End If
    End Sub

    Private Sub gvStockedItems_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gvStockedItems.KeyDown
        If e.KeyCode = Keys.Delete Then
            If gvStockedItems.SelectedRowsCount > 0 Then
                If ExecuteScalar("SELECT dbo.MSIIsUsed(@MSIID)", CommandType.Text, New SqlParameter() {New SqlParameter("@MSIID", SelectedSheetSize("MSIID"))}, Transaction) Then
                    Message.ShowMessage("You cannot delete this stocked item because it has either been used in a job or is currently being held in stock at a branch.", MessageBoxIcon.Exclamation)
                Else
                    gvStockedItems.GetDataRow(gvStockedItems.GetSelectedRows(0)).Delete()
                End If
            End If
        End If
    End Sub

    Public ReadOnly Property SelectedSheetSize() As DataRow
        Get
            If Not gvStockedItems.GetSelectedRows Is Nothing Then
                Return gvStockedItems.GetDataRow(gvStockedItems.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub btnRemoveSheetSize_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveSheetSize.Click
        If Not SelectedSheetSize Is Nothing Then
            If ExecuteScalar("SELECT dbo.MSIIsUsed(@MSIID)", CommandType.Text, New SqlParameter() {New SqlParameter("@MSIID", SelectedSheetSize("MSIID"))}, Transaction) Then
                Message.ShowMessage("You cannot delete this stocked item because it has either been used in a job or is currently being held in stock at a branch.", MessageBoxIcon.Exclamation)
            Else
                SelectedSheetSize.Delete()
            End If
        End If
    End Sub

#End Region

    Private Sub Decimal_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) _
    Handles txtSIConversion.ParseEditValue
        Format.Decimal_ParseEditValue(sender, e)
    End Sub

    Private Sub Text_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) _
    Handles txtMTName.ParseEditValue
        Format.Text_ParseEditValue(sender, e)
    End Sub

    Private Sub txtUOM_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles txtMTPrimaryUOM.EditValueChanged, txtMTInventoryUOM.EditValueChanged
        If TypeOf CType(sender, DevExpress.XtraEditors.LookUpEdit).EditValue Is String Then
            If CType(sender, DevExpress.XtraEditors.LookUpEdit).EditValue = "" Then
                CType(sender, DevExpress.XtraEditors.LookUpEdit).EditValue = DBNull.Value
            End If
        End If
    End Sub

    Private Sub txtUOM_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) _
    Handles txtMTInventoryUOM.KeyDown, txtMTPrimaryUOM.KeyDown
        If e.KeyCode = Keys.Delete Then
            CType(sender, DevExpress.XtraEditors.LookUpEdit).EditValue = Nothing
        End If
    End Sub

    Private Sub txtUOM_Validated(ByVal sender As Object, ByVal e As System.EventArgs) _
    Handles txtMTInventoryUOM.Validated, txtMTPrimaryUOM.Validated
        ' Update main screen info
        DataRow.EndEdit()
        SqlDataAdapter.Update(DsHeadOffice)
        HasChanges = True
        daStockedItems.Fill(DsHeadOffice)
    End Sub

    Private Sub txtMTName_Validated(ByVal sender As Object, ByVal e As System.EventArgs) _
    Handles txtMTName.Validated
        ' Update main screen info
        DataRow.EndEdit()
        SqlDataAdapter.Update(DsHeadOffice)
        HasChanges = True
        daStockedItems.Fill(DsHeadOffice)
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        ShowHelpTopic(Me, "MaterialsTerms.html")
    End Sub
End Class

