Public Class frmJob
    Inherits DevExpress.XtraEditors.XtraForm

    Private DataRow As DataRow
    Public BRID As Int32
    Public JBID As Int64

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return SqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            SqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private hTransaction As SqlClient.SqlTransaction
    Private Property Transaction() As SqlClient.SqlTransaction
        Get
            Return hTransaction
        End Get
        Set(ByVal Value As SqlClient.SqlTransaction)
            hTransaction = Value
            Power.Library.Library.ApplyTransactionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Public Shared Function Add(ByVal BRID As Int32) As frmJob
        Dim gui As New frmJob

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()
            Dim tempTrans As SqlClient.SqlTransaction = .SqlConnection.BeginTransaction(IsolationLevel.ReadUncommitted)
            Dim NextID As Long = sp_GetNextID(BRID, tempTrans)
            tempTrans.Commit()

            .BRID = BRID
            .LoadTreeViews()

            .Transaction = .SqlConnection.BeginTransaction(IsolationLevel.ReadUncommitted)
            .FillPreliminaryData()
            .JBID = spNew_Job(.BRID, NextID, .Transaction)

            If DataAccess.spExecLockRequest("sp_GetJobLock", .BRID, .JBID, .Transaction) Then

                .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = .BRID
                .SqlDataAdapter.SelectCommand.Parameters("@JBID").Value = .JBID
                .SqlDataAdapter.Fill(.DsGTMS)
                .DataRow = .DsGTMS.Jobs(0)

                .FillData()

            Else
                Throw New ObjectLockedException
            End If
        End With

        Return gui
    End Function

    Public Shared Function Edit(ByVal BRID As Int32, ByRef JBID As Int64) As frmJob
        Dim gui As New frmJob

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            .BRID = BRID
            .JBID = JBID
            .LoadTreeViews()

            .Transaction = .SqlConnection.BeginTransaction(IsolationLevel.ReadUncommitted)
            .FillPreliminaryData()

            If DataAccess.spExecLockRequest("sp_GetJobLock", .BRID, .JBID, .Transaction) Then

                .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = BRID
                .SqlDataAdapter.SelectCommand.Parameters("@JBID").Value = JBID
                .SqlDataAdapter.Fill(.DsGTMS)
                .DataRow = .DsGTMS.Jobs(0)

                .FillData()

            Else
                Throw New ObjectLockedException
            End If
        End With

        Return gui
    End Function

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents DsGTMS As WindowsApplication.dsGTMS
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents ListBox As Power.Forms.ListBox
    Friend WithEvents pnlMain As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents GroupLine5 As Power.Forms.GroupLine
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents GroupLine2 As Power.Forms.GroupLine
    Friend WithEvents GroupLine4 As Power.Forms.GroupLine
    Friend WithEvents GroupLine3 As Power.Forms.GroupLine
    Friend WithEvents GroupLine6 As Power.Forms.GroupLine
    Friend WithEvents GroupLine7 As Power.Forms.GroupLine
    Friend WithEvents GroupLine8 As Power.Forms.GroupLine
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daJobTypes As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents daJobs_Materials As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents dvJobs_Materials As System.Data.DataView
    Friend WithEvents dvJobs_Granite As System.Data.DataView
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents dvJobs_DirectLabour As System.Data.DataView
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents dvJobs_SalesReps As System.Data.DataView
    Friend WithEvents pnlSalesReps As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlDirectLabour As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlGranite As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlMaterials As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlJobInformation As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlJobAddress As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlClientPayments As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupLine1 As Power.Forms.GroupLine
    Friend WithEvents GroupLine9 As Power.Forms.GroupLine
    Friend WithEvents GroupLine10 As Power.Forms.GroupLine
    Friend WithEvents GroupLine11 As Power.Forms.GroupLine
    Friend WithEvents GroupLine12 As Power.Forms.GroupLine
    Friend WithEvents GroupLine13 As Power.Forms.GroupLine
    Friend WithEvents daClientPayments As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents dvClientPayments As System.Data.DataView
    Friend WithEvents pnlAppliances As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlOtherTrades As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupLine14 As Power.Forms.GroupLine
    Friend WithEvents GroupLine15 As Power.Forms.GroupLine
    Friend WithEvents GroupLine16 As Power.Forms.GroupLine
    Friend WithEvents tvGranite As Power.Forms.TreeView
    Friend WithEvents tvMaterial As Power.Forms.TreeView
    Friend WithEvents tvDirectLabour As Power.Forms.TreeView
    Friend WithEvents tvSalesReps As Power.Forms.TreeView
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents dvBadDebts As System.Data.DataView
    Friend WithEvents dvDiscounts As System.Data.DataView
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents dgAppliances As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvAppliances As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colNIUserType1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIBrand As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIName2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIOrderedBy As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIPaidBy As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btnAddAppliance As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEditAppliance As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveAppliance As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents daJobNonCoreSalesItems As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents dgOtherTrades As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvOtherTrades As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colNIName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIUserType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIOrderedBy1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNIPaidBy1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnRemoveOtherTrade As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEditOtherTrade As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAddOtherTrade As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dvAppliances As System.Data.DataView
    Friend WithEvents dvOtherTrades As System.Data.DataView
    Friend WithEvents btnRemoveClientPayment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dgClientPayments As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvClientPayments As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colCPDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCPAmount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCPNotes As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtCurrencyClientPayments As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents btnRemoveDiscount As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dgBadDebts As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dgDiscounts As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvDiscounts As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colCPDate2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCPAmount2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtCurrencyDiscounts As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents colCPNotes2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gvBadDebts As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtCurrencyBadDebts As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents btnRemoveBadDebt As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents dgStockedItems As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvStockedItems As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colJSInventoryAmount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSIName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents daJobs_StockedItems As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents txtJBPrice As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBGraniteBAOTPrice As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBCabinetryBAOTPrice As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBJobStreetAddress01 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents rgJBJobAddressAsAbove As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents lblJobState As System.Windows.Forms.Label
    Friend WithEvents lblJobPostCode As System.Windows.Forms.Label
    Friend WithEvents lblJobStreetAddress As System.Windows.Forms.Label
    Friend WithEvents lblJobSuburb As System.Windows.Forms.Label
    Friend WithEvents txtJBJobStreetAddress02 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBJobSuburb As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBJobState As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBJobPostCode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNumericStockedItems As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents colMTName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colJMPrimarySold As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtNumericMaterials As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents dgGranite As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvGranite As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtNumericGranite As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents dgMaterials As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvMaterials As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daJobs_Expenses As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents dgSalesReps As DevExpress.XtraGrid.GridControl
    Friend WithEvents pnlResponsibilities As DevExpress.XtraEditors.GroupControl
    Friend WithEvents dgResponsibilities As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvResponsibilities As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents BandedGridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BandedGridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BandedGridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gvSalesReps As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dgDirectLabour As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvDirectLabour As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents txtJBJobType As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents pnlIntro As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtJBClientSurname As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupLine17 As Power.Forms.GroupLine
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents GroupLine18 As Power.Forms.GroupLine
    Friend WithEvents GroupLine19 As Power.Forms.GroupLine
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents pnlClientInformation As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtJBClientFirstName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBReferenceName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientStreetAddress01 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientStreetAddress02 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientPostCode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientPhoneNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientFaxNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtBClientMobileNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents dpJBActualFinishDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents PictureEdit4 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents txtPercentOfJob As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents txtJBClientEmail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents pnlJobIssues As DevExpress.XtraEditors.GroupControl
    Friend WithEvents daJobs_Jobissues As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colIfOccured1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIfOverSchedule1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDescription1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlSelectCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents GroupLine20 As Power.Forms.GroupLine
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents txtJBJobInsurance As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtJBClientSuburb As DevExpress.XtraEditors.TextEdit
    Friend WithEvents JBClientState As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Button1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colMTPrimaryUOMShortName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSIInventoryUOMShortName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents PictureEdit3 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents btnAddGranite As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveGranite As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAddDirectLabour As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveDirectLabour As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAddMaterial As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveMaterial As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAddSalesRep As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveSalesRep As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents PictureEdit1 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents colMTPrimaryUOMShortName1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dpJBScheduledFinishDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dpJBScheduledStartDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents txtPercentageDirectLabour As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents txtPercentageSalesReps As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents txtDollarDirectLabour As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents txtDollarSalesRep As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents pnlSummary As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents lblJBPrice As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblJBPriceAppliances As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblJBPriceOtherTrades As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblJBJobInsurance As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblJBPriceTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblJBClientPaid As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblJBBalance As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents lblJBOtherReduction As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DsReports As WindowsApplication.dsReports
    Friend WithEvents btnViewJobReport As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents pnlNotes As DevExpress.XtraEditors.GroupControl
    Friend WithEvents dgNotes As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvNotes As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colINUser As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colINDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colINNotes As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dvExpenses As System.Data.DataView
    Friend WithEvents daIDNotes As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daExpenses As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommand7 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand8 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand9 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand10 As System.Data.SqlClient.SqlCommand
    Friend WithEvents txtJBInventoryDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dgJobIssues As DevExpress.XtraGrid.GridControl
    Friend WithEvents txtOrderedOrPaidBy As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents rgPayAsDirectLabour As DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup
    Friend WithEvents rgPayAsSalesReps As DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup
    Friend WithEvents txtUser As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents RepositoryItemMemoEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit
    Friend WithEvents txtINDate As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents PersistentRepository1 As DevExpress.XtraEditors.Repository.PersistentRepository
    Friend WithEvents txtDate As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmJob))
        Me.ListBox = New Power.Forms.ListBox
        Me.DsGTMS = New WindowsApplication.dsGTMS
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.pnlMain = New DevExpress.XtraEditors.PanelControl
        Me.pnlMaterials = New DevExpress.XtraEditors.GroupControl
        Me.btnRemoveMaterial = New DevExpress.XtraEditors.SimpleButton
        Me.btnAddMaterial = New DevExpress.XtraEditors.SimpleButton
        Me.dgMaterials = New DevExpress.XtraGrid.GridControl
        Me.dvJobs_Materials = New System.Data.DataView
        Me.gvMaterials = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colMTName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colJMPrimarySold = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtNumericMaterials = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colMTPrimaryUOMShortName1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tvMaterial = New Power.Forms.TreeView
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.pnlIntro = New DevExpress.XtraEditors.GroupControl
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Label44 = New System.Windows.Forms.Label
        Me.PictureEdit4 = New DevExpress.XtraEditors.PictureEdit
        Me.Label43 = New System.Windows.Forms.Label
        Me.Label42 = New System.Windows.Forms.Label
        Me.PictureEdit3 = New DevExpress.XtraEditors.PictureEdit
        Me.Label48 = New System.Windows.Forms.Label
        Me.PictureEdit1 = New DevExpress.XtraEditors.PictureEdit
        Me.pnlClientPayments = New DevExpress.XtraEditors.GroupControl
        Me.Label41 = New System.Windows.Forms.Label
        Me.dgClientPayments = New DevExpress.XtraGrid.GridControl
        Me.dvClientPayments = New System.Data.DataView
        Me.PersistentRepository1 = New DevExpress.XtraEditors.Repository.PersistentRepository(Me.components)
        Me.txtDate = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.gvClientPayments = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colCPDate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colCPAmount = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtCurrencyClientPayments = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colCPNotes = New DevExpress.XtraGrid.Columns.GridColumn
        Me.btnRemoveClientPayment = New DevExpress.XtraEditors.SimpleButton
        Me.dgDiscounts = New DevExpress.XtraGrid.GridControl
        Me.dvDiscounts = New System.Data.DataView
        Me.gvDiscounts = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colCPDate2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colCPAmount2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtCurrencyDiscounts = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colCPNotes2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Label35 = New System.Windows.Forms.Label
        Me.btnRemoveBadDebt = New DevExpress.XtraEditors.SimpleButton
        Me.dgBadDebts = New DevExpress.XtraGrid.GridControl
        Me.dvBadDebts = New System.Data.DataView
        Me.gvBadDebts = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtCurrencyBadDebts = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.btnRemoveDiscount = New DevExpress.XtraEditors.SimpleButton
        Me.Label34 = New System.Windows.Forms.Label
        Me.pnlJobAddress = New DevExpress.XtraEditors.GroupControl
        Me.txtJBJobStreetAddress01 = New DevExpress.XtraEditors.TextEdit
        Me.rgJBJobAddressAsAbove = New DevExpress.XtraEditors.RadioGroup
        Me.Label17 = New System.Windows.Forms.Label
        Me.lblJobState = New System.Windows.Forms.Label
        Me.lblJobPostCode = New System.Windows.Forms.Label
        Me.lblJobStreetAddress = New System.Windows.Forms.Label
        Me.lblJobSuburb = New System.Windows.Forms.Label
        Me.txtJBJobStreetAddress02 = New DevExpress.XtraEditors.TextEdit
        Me.txtJBJobSuburb = New DevExpress.XtraEditors.TextEdit
        Me.txtJBJobState = New DevExpress.XtraEditors.TextEdit
        Me.txtJBJobPostCode = New DevExpress.XtraEditors.TextEdit
        Me.pnlClientInformation = New DevExpress.XtraEditors.GroupControl
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label45 = New System.Windows.Forms.Label
        Me.txtJBClientEmail = New DevExpress.XtraEditors.TextEdit
        Me.Label13 = New System.Windows.Forms.Label
        Me.txtJBClientFirstName = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientSurname = New DevExpress.XtraEditors.TextEdit
        Me.GroupLine17 = New Power.Forms.GroupLine
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.GroupLine18 = New Power.Forms.GroupLine
        Me.GroupLine19 = New Power.Forms.GroupLine
        Me.Label30 = New System.Windows.Forms.Label
        Me.txtJBReferenceName = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientStreetAddress01 = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientStreetAddress02 = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientSuburb = New DevExpress.XtraEditors.TextEdit
        Me.JBClientState = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientPostCode = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientPhoneNumber = New DevExpress.XtraEditors.TextEdit
        Me.txtJBClientFaxNumber = New DevExpress.XtraEditors.TextEdit
        Me.txtBClientMobileNumber = New DevExpress.XtraEditors.TextEdit
        Me.pnlAppliances = New DevExpress.XtraEditors.GroupControl
        Me.btnRemoveAppliance = New DevExpress.XtraEditors.SimpleButton
        Me.btnEditAppliance = New DevExpress.XtraEditors.SimpleButton
        Me.btnAddAppliance = New DevExpress.XtraEditors.SimpleButton
        Me.dgAppliances = New DevExpress.XtraGrid.GridControl
        Me.dvAppliances = New System.Data.DataView
        Me.gvAppliances = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colNIUserType1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIBrand = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIName2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIOrderedBy = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtOrderedOrPaidBy = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox
        Me.colNIPaidBy = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.pnlGranite = New DevExpress.XtraEditors.GroupControl
        Me.Label58 = New System.Windows.Forms.Label
        Me.Label55 = New System.Windows.Forms.Label
        Me.txtJBInventoryDate = New DevExpress.XtraEditors.DateEdit
        Me.dgStockedItems = New DevExpress.XtraGrid.GridControl
        Me.gvStockedItems = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colSIName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colJSInventoryAmount = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtNumericStockedItems = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colSIInventoryUOMShortName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.btnRemoveGranite = New DevExpress.XtraEditors.SimpleButton
        Me.btnAddGranite = New DevExpress.XtraEditors.SimpleButton
        Me.dgGranite = New DevExpress.XtraGrid.GridControl
        Me.dvJobs_Granite = New System.Data.DataView
        Me.gvGranite = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtNumericGranite = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colMTPrimaryUOMShortName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.tvGranite = New Power.Forms.TreeView
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label37 = New System.Windows.Forms.Label
        Me.pnlSalesReps = New DevExpress.XtraEditors.GroupControl
        Me.btnRemoveSalesRep = New DevExpress.XtraEditors.SimpleButton
        Me.btnAddSalesRep = New DevExpress.XtraEditors.SimpleButton
        Me.dgSalesReps = New DevExpress.XtraGrid.GridControl
        Me.dvJobs_SalesReps = New System.Data.DataView
        Me.gvSalesReps = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.rgPayAsSalesReps = New DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtDollarSalesRep = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtPercentageSalesReps = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.tvSalesReps = New Power.Forms.TreeView
        Me.Label28 = New System.Windows.Forms.Label
        Me.Label29 = New System.Windows.Forms.Label
        Me.pnlResponsibilities = New DevExpress.XtraEditors.GroupControl
        Me.dgResponsibilities = New DevExpress.XtraGrid.GridControl
        Me.gvResponsibilities = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.BandedGridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.BandedGridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtPercentOfJob = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.BandedGridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.pnlOtherTrades = New DevExpress.XtraEditors.GroupControl
        Me.btnRemoveOtherTrade = New DevExpress.XtraEditors.SimpleButton
        Me.btnEditOtherTrade = New DevExpress.XtraEditors.SimpleButton
        Me.btnAddOtherTrade = New DevExpress.XtraEditors.SimpleButton
        Me.dgOtherTrades = New DevExpress.XtraGrid.GridControl
        Me.dvOtherTrades = New System.Data.DataView
        Me.gvOtherTrades = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colNIName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIUserType = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIOrderedBy1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colNIPaidBy1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.pnlNotes = New DevExpress.XtraEditors.GroupControl
        Me.dgNotes = New DevExpress.XtraGrid.GridControl
        Me.gvNotes = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colINUser = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtUser = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.dvExpenses = New System.Data.DataView
        Me.colINDate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtINDate = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.colINNotes = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemMemoEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit
        Me.pnlJobIssues = New DevExpress.XtraEditors.GroupControl
        Me.dgJobIssues = New DevExpress.XtraGrid.GridControl
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colIfOccured1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colIfOverSchedule1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colDescription1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.pnlJobInformation = New DevExpress.XtraEditors.GroupControl
        Me.dpJBScheduledStartDate = New DevExpress.XtraEditors.DateEdit
        Me.dpJBScheduledFinishDate = New DevExpress.XtraEditors.DateEdit
        Me.txtJBJobInsurance = New DevExpress.XtraEditors.TextEdit
        Me.Label46 = New System.Windows.Forms.Label
        Me.Label40 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtJBJobType = New DevExpress.XtraEditors.LookUpEdit
        Me.dpJBActualFinishDate = New DevExpress.XtraEditors.DateEdit
        Me.txtJBCabinetryBAOTPrice = New DevExpress.XtraEditors.TextEdit
        Me.txtJBGraniteBAOTPrice = New DevExpress.XtraEditors.TextEdit
        Me.txtJBPrice = New DevExpress.XtraEditors.TextEdit
        Me.GroupLine14 = New Power.Forms.GroupLine
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.GroupLine15 = New Power.Forms.GroupLine
        Me.GroupLine16 = New Power.Forms.GroupLine
        Me.Label31 = New System.Windows.Forms.Label
        Me.Label32 = New System.Windows.Forms.Label
        Me.Label33 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label36 = New System.Windows.Forms.Label
        Me.Label38 = New System.Windows.Forms.Label
        Me.Label39 = New System.Windows.Forms.Label
        Me.GroupLine20 = New Power.Forms.GroupLine
        Me.Label47 = New System.Windows.Forms.Label
        Me.pnlDirectLabour = New DevExpress.XtraEditors.GroupControl
        Me.btnRemoveDirectLabour = New DevExpress.XtraEditors.SimpleButton
        Me.btnAddDirectLabour = New DevExpress.XtraEditors.SimpleButton
        Me.dgDirectLabour = New DevExpress.XtraGrid.GridControl
        Me.dvJobs_DirectLabour = New System.Data.DataView
        Me.gvDirectLabour = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.rgPayAsDirectLabour = New DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtDollarDirectLabour = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtPercentageDirectLabour = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.tvDirectLabour = New Power.Forms.TreeView
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.pnlSummary = New DevExpress.XtraEditors.GroupControl
        Me.btnViewJobReport = New DevExpress.XtraEditors.SimpleButton
        Me.Label57 = New System.Windows.Forms.Label
        Me.lblJBOtherReduction = New DevExpress.XtraEditors.TextEdit
        Me.DsReports = New WindowsApplication.dsReports
        Me.lblJBBalance = New DevExpress.XtraEditors.TextEdit
        Me.lblJBClientPaid = New DevExpress.XtraEditors.TextEdit
        Me.lblJBPriceTotal = New DevExpress.XtraEditors.TextEdit
        Me.lblJBJobInsurance = New DevExpress.XtraEditors.TextEdit
        Me.lblJBPriceOtherTrades = New DevExpress.XtraEditors.TextEdit
        Me.lblJBPriceAppliances = New DevExpress.XtraEditors.TextEdit
        Me.lblJBPrice = New DevExpress.XtraEditors.TextEdit
        Me.Label56 = New System.Windows.Forms.Label
        Me.Label54 = New System.Windows.Forms.Label
        Me.Label53 = New System.Windows.Forms.Label
        Me.Label52 = New System.Windows.Forms.Label
        Me.Label51 = New System.Windows.Forms.Label
        Me.Label50 = New System.Windows.Forms.Label
        Me.Label49 = New System.Windows.Forms.Label
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.daJobTypes = New System.Data.SqlClient.SqlDataAdapter
        Me.daJobs_Materials = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.daJobs_Expenses = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand
        Me.daClientPayments = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand7 = New System.Data.SqlClient.SqlCommand
        Me.daJobNonCoreSalesItems = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand4 = New System.Data.SqlClient.SqlCommand
        Me.daJobs_StockedItems = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand10 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand10 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand10 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand10 = New System.Data.SqlClient.SqlCommand
        Me.daJobs_Jobissues = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand5 = New System.Data.SqlClient.SqlCommand
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.Button1 = New DevExpress.XtraEditors.SimpleButton
        Me.daIDNotes = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand6 = New System.Data.SqlClient.SqlCommand
        Me.daExpenses = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlCommand7 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand8 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand9 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand10 = New System.Data.SqlClient.SqlCommand
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMain.SuspendLayout()
        CType(Me.pnlMaterials, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMaterials.SuspendLayout()
        CType(Me.dgMaterials, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvJobs_Materials, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvMaterials, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumericMaterials, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlIntro, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlIntro.SuspendLayout()
        CType(Me.PictureEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlClientPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlClientPayments.SuspendLayout()
        CType(Me.dgClientPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvClientPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvClientPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCurrencyClientPayments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgDiscounts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvDiscounts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvDiscounts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCurrencyDiscounts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgBadDebts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvBadDebts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvBadDebts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCurrencyBadDebts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlJobAddress, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlJobAddress.SuspendLayout()
        CType(Me.txtJBJobStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgJBJobAddressAsAbove.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBJobStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBJobSuburb.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBJobState.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBJobPostCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlClientInformation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlClientInformation.SuspendLayout()
        CType(Me.txtJBClientEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientFirstName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBReferenceName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientSuburb.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JBClientState.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientPostCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientPhoneNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBClientFaxNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBClientMobileNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlAppliances, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlAppliances.SuspendLayout()
        CType(Me.dgAppliances, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvAppliances, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvAppliances, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOrderedOrPaidBy, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlGranite, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlGranite.SuspendLayout()
        CType(Me.txtJBInventoryDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgStockedItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvStockedItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumericStockedItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgGranite, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvJobs_Granite, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvGranite, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumericGranite, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlSalesReps, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlSalesReps.SuspendLayout()
        CType(Me.dgSalesReps, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvJobs_SalesReps, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvSalesReps, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgPayAsSalesReps, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDollarSalesRep, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPercentageSalesReps, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlResponsibilities, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlResponsibilities.SuspendLayout()
        CType(Me.dgResponsibilities, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvResponsibilities, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPercentOfJob, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlOtherTrades, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlOtherTrades.SuspendLayout()
        CType(Me.dgOtherTrades, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvOtherTrades, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvOtherTrades, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlNotes.SuspendLayout()
        CType(Me.dgNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUser, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvExpenses, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtINDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemMemoEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlJobIssues, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlJobIssues.SuspendLayout()
        CType(Me.dgJobIssues, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlJobInformation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlJobInformation.SuspendLayout()
        CType(Me.dpJBScheduledStartDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dpJBScheduledFinishDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBJobInsurance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBJobType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dpJBActualFinishDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBCabinetryBAOTPrice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBGraniteBAOTPrice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtJBPrice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlDirectLabour, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlDirectLabour.SuspendLayout()
        CType(Me.dgDirectLabour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvJobs_DirectLabour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvDirectLabour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgPayAsDirectLabour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDollarDirectLabour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPercentageDirectLabour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlSummary.SuspendLayout()
        CType(Me.lblJBOtherReduction.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsReports, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblJBBalance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblJBClientPaid.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblJBPriceTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblJBJobInsurance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblJBPriceOtherTrades.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblJBPriceAppliances.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblJBPrice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ListBox
        '
        Me.ListBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ListBox.ColumnWidth = 20
        Me.ListBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ListBox.IntegralHeight = False
        Me.ListBox.ItemHeight = 25
        Me.ListBox.Items.AddRange(New Object() {"Introduction", "Customer Information", "Job Address", "Job Information", "Job Issues", "Non-Tracked Material", "Tracked Material", "Specified Direct Labor", "Specified Salespeople", "Responsibilities", "Appliances", "Other Trades", "Customer Payments", "Notes", "Job Price Summary"})
        Me.ListBox.Location = New System.Drawing.Point(8, 8)
        Me.ListBox.Name = "ListBox"
        Me.ListBox.Size = New System.Drawing.Size(136, 488)
        Me.ListBox.TabIndex = 0
        '
        'DsGTMS
        '
        Me.DsGTMS.DataSetName = "dsGTMS"
        Me.DsGTMS.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;integrated security=SSPI;data source=""SERVER" & _
        "\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Jobs", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("JBID", "JBID"), New System.Data.Common.DataColumnMapping("ID", "ID"), New System.Data.Common.DataColumnMapping("JBScheduledStartDate", "JBScheduledStartDate"), New System.Data.Common.DataColumnMapping("JBScheduledFinishDate", "JBScheduledFinishDate"), New System.Data.Common.DataColumnMapping("JBActualFinishDate", "JBActualFinishDate"), New System.Data.Common.DataColumnMapping("JBClientFirstName", "JBClientFirstName"), New System.Data.Common.DataColumnMapping("JBClientSurname", "JBClientSurname"), New System.Data.Common.DataColumnMapping("JBClientName", "JBClientName"), New System.Data.Common.DataColumnMapping("JBClientStreetAddress01", "JBClientStreetAddress01"), New System.Data.Common.DataColumnMapping("JBClientStreetAddress02", "JBClientStreetAddress02"), New System.Data.Common.DataColumnMapping("JBClientSuburb", "JBClientSuburb"), New System.Data.Common.DataColumnMapping("JBClientState", "JBClientState"), New System.Data.Common.DataColumnMapping("JBClientPostCode", "JBClientPostCode"), New System.Data.Common.DataColumnMapping("JBClientAddress", "JBClientAddress"), New System.Data.Common.DataColumnMapping("JBClientPhoneNumber", "JBClientPhoneNumber"), New System.Data.Common.DataColumnMapping("JBClientFaxNumber", "JBClientFaxNumber"), New System.Data.Common.DataColumnMapping("JBClientMobileNumber", "JBClientMobileNumber"), New System.Data.Common.DataColumnMapping("JBJobAddressAsAbove", "JBJobAddressAsAbove"), New System.Data.Common.DataColumnMapping("JBJobStreetAddress01", "JBJobStreetAddress01"), New System.Data.Common.DataColumnMapping("JBJobStreetAddress02", "JBJobStreetAddress02"), New System.Data.Common.DataColumnMapping("JBJobSuburb", "JBJobSuburb"), New System.Data.Common.DataColumnMapping("JBJobState", "JBJobState"), New System.Data.Common.DataColumnMapping("JBJobPostCode", "JBJobPostCode"), New System.Data.Common.DataColumnMapping("JBJobAddress", "JBJobAddress"), New System.Data.Common.DataColumnMapping("JTID", "JTID"), New System.Data.Common.DataColumnMapping("JBGraniteBAOTPrice", "JBGraniteBAOTPrice"), New System.Data.Common.DataColumnMapping("JBCabinetryBAOTPrice", "JBCabinetryBAOTPrice"), New System.Data.Common.DataColumnMapping("JBPrice", "JBPrice"), New System.Data.Common.DataColumnMapping("JBReferenceName", "JBReferenceName"), New System.Data.Common.DataColumnMapping("JBJobInsurance", "JBJobInsurance"), New System.Data.Common.DataColumnMapping("JBIsCancelled", "JBIsCancelled"), New System.Data.Common.DataColumnMapping("JBClientEmail", "JBClientEmail"), New System.Data.Common.DataColumnMapping("JBInventoryDate", "JBInventoryDate")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Jobs WHERE (BRID = @Original_BRID) AND (JBID = @Original_JBID) AND (I" & _
        "D = @Original_ID) AND (JBActualFinishDate = @Original_JBActualFinishDate OR @Ori" & _
        "ginal_JBActualFinishDate IS NULL AND JBActualFinishDate IS NULL) AND (JBCabinetr" & _
        "yBAOTPrice = @Original_JBCabinetryBAOTPrice OR @Original_JBCabinetryBAOTPrice IS" & _
        " NULL AND JBCabinetryBAOTPrice IS NULL) AND (JBClientAddress = @Original_JBClien" & _
        "tAddress OR @Original_JBClientAddress IS NULL AND JBClientAddress IS NULL) AND (" & _
        "JBClientEmail = @Original_JBClientEmail OR @Original_JBClientEmail IS NULL AND J" & _
        "BClientEmail IS NULL) AND (JBClientFaxNumber = @Original_JBClientFaxNumber OR @O" & _
        "riginal_JBClientFaxNumber IS NULL AND JBClientFaxNumber IS NULL) AND (JBClientFi" & _
        "rstName = @Original_JBClientFirstName OR @Original_JBClientFirstName IS NULL AND" & _
        " JBClientFirstName IS NULL) AND (JBClientMobileNumber = @Original_JBClientMobile" & _
        "Number OR @Original_JBClientMobileNumber IS NULL AND JBClientMobileNumber IS NUL" & _
        "L) AND (JBClientName = @Original_JBClientName OR @Original_JBClientName IS NULL " & _
        "AND JBClientName IS NULL) AND (JBClientPhoneNumber = @Original_JBClientPhoneNumb" & _
        "er OR @Original_JBClientPhoneNumber IS NULL AND JBClientPhoneNumber IS NULL) AND" & _
        " (JBClientPostCode = @Original_JBClientPostCode OR @Original_JBClientPostCode IS" & _
        " NULL AND JBClientPostCode IS NULL) AND (JBClientState = @Original_JBClientState" & _
        " OR @Original_JBClientState IS NULL AND JBClientState IS NULL) AND (JBClientStre" & _
        "etAddress01 = @Original_JBClientStreetAddress01 OR @Original_JBClientStreetAddre" & _
        "ss01 IS NULL AND JBClientStreetAddress01 IS NULL) AND (JBClientStreetAddress02 =" & _
        " @Original_JBClientStreetAddress02 OR @Original_JBClientStreetAddress02 IS NULL " & _
        "AND JBClientStreetAddress02 IS NULL) AND (JBClientSuburb = @Original_JBClientSub" & _
        "urb OR @Original_JBClientSuburb IS NULL AND JBClientSuburb IS NULL) AND (JBClien" & _
        "tSurname = @Original_JBClientSurname OR @Original_JBClientSurname IS NULL AND JB" & _
        "ClientSurname IS NULL) AND (JBGraniteBAOTPrice = @Original_JBGraniteBAOTPrice OR" & _
        " @Original_JBGraniteBAOTPrice IS NULL AND JBGraniteBAOTPrice IS NULL) AND (JBInv" & _
        "entoryDate = @Original_JBInventoryDate OR @Original_JBInventoryDate IS NULL AND " & _
        "JBInventoryDate IS NULL) AND (JBIsCancelled = @Original_JBIsCancelled OR @Origin" & _
        "al_JBIsCancelled IS NULL AND JBIsCancelled IS NULL) AND (JBJobAddress = @Origina" & _
        "l_JBJobAddress OR @Original_JBJobAddress IS NULL AND JBJobAddress IS NULL) AND (" & _
        "JBJobAddressAsAbove = @Original_JBJobAddressAsAbove OR @Original_JBJobAddressAsA" & _
        "bove IS NULL AND JBJobAddressAsAbove IS NULL) AND (JBJobInsurance = @Original_JB" & _
        "JobInsurance OR @Original_JBJobInsurance IS NULL AND JBJobInsurance IS NULL) AND" & _
        " (JBJobPostCode = @Original_JBJobPostCode OR @Original_JBJobPostCode IS NULL AND" & _
        " JBJobPostCode IS NULL) AND (JBJobState = @Original_JBJobState OR @Original_JBJo" & _
        "bState IS NULL AND JBJobState IS NULL) AND (JBJobStreetAddress01 = @Original_JBJ" & _
        "obStreetAddress01 OR @Original_JBJobStreetAddress01 IS NULL AND JBJobStreetAddre" & _
        "ss01 IS NULL) AND (JBJobStreetAddress02 = @Original_JBJobStreetAddress02 OR @Ori" & _
        "ginal_JBJobStreetAddress02 IS NULL AND JBJobStreetAddress02 IS NULL) AND (JBJobS" & _
        "uburb = @Original_JBJobSuburb OR @Original_JBJobSuburb IS NULL AND JBJobSuburb I" & _
        "S NULL) AND (JBPrice = @Original_JBPrice OR @Original_JBPrice IS NULL AND JBPric" & _
        "e IS NULL) AND (JBReferenceName = @Original_JBReferenceName OR @Original_JBRefer" & _
        "enceName IS NULL AND JBReferenceName IS NULL) AND (JBScheduledFinishDate = @Orig" & _
        "inal_JBScheduledFinishDate OR @Original_JBScheduledFinishDate IS NULL AND JBSche" & _
        "duledFinishDate IS NULL) AND (JBScheduledStartDate = @Original_JBScheduledStartD" & _
        "ate OR @Original_JBScheduledStartDate IS NULL AND JBScheduledStartDate IS NULL) " & _
        "AND (JTID = @Original_JTID OR @Original_JTID IS NULL AND JTID IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBActualFinishDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBActualFinishDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBCabinetryBAOTPrice", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBCabinetryBAOTPrice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientFirstName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientFirstName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientName", System.Data.SqlDbType.VarChar, 102, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientSurname", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBGraniteBAOTPrice", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBGraniteBAOTPrice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBInventoryDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBInventoryDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBIsCancelled", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBIsCancelled", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobAddressAsAbove", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobInsurance", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobInsurance", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBPrice", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBPrice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBReferenceName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBReferenceName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBScheduledFinishDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBScheduledFinishDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBScheduledStartDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBScheduledStartDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JTID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Jobs(BRID, ID, JBScheduledStartDate, JBScheduledFinishDate, JBActualF" & _
        "inishDate, JBClientFirstName, JBClientSurname, JBClientName, JBClientStreetAddre" & _
        "ss01, JBClientStreetAddress02, JBClientSuburb, JBClientState, JBClientPostCode, " & _
        "JBClientAddress, JBClientPhoneNumber, JBClientFaxNumber, JBClientMobileNumber, J" & _
        "BJobAddressAsAbove, JBJobStreetAddress01, JBJobStreetAddress02, JBJobSuburb, JBJ" & _
        "obState, JBJobPostCode, JBJobAddress, JTID, JBGraniteBAOTPrice, JBCabinetryBAOTP" & _
        "rice, JBPrice, JBReferenceName, JBJobInsurance, JBIsCancelled, JBClientEmail, JB" & _
        "InventoryDate) VALUES (@BRID, @ID, @JBScheduledStartDate, @JBScheduledFinishDate" & _
        ", @JBActualFinishDate, @JBClientFirstName, @JBClientSurname, @JBClientName, @JBC" & _
        "lientStreetAddress01, @JBClientStreetAddress02, @JBClientSuburb, @JBClientState," & _
        " @JBClientPostCode, @JBClientAddress, @JBClientPhoneNumber, @JBClientFaxNumber, " & _
        "@JBClientMobileNumber, @JBJobAddressAsAbove, @JBJobStreetAddress01, @JBJobStreet" & _
        "Address02, @JBJobSuburb, @JBJobState, @JBJobPostCode, @JBJobAddress, @JTID, @JBG" & _
        "raniteBAOTPrice, @JBCabinetryBAOTPrice, @JBPrice, @JBReferenceName, @JBJobInsura" & _
        "nce, @JBIsCancelled, @JBClientEmail, @JBInventoryDate); SELECT BRID, JBID, ID, J" & _
        "BScheduledStartDate, JBScheduledFinishDate, JBActualFinishDate, JBClientFirstNam" & _
        "e, JBClientSurname, JBClientName, JBClientStreetAddress01, JBClientStreetAddress" & _
        "02, JBClientSuburb, JBClientState, JBClientPostCode, JBClientAddress, JBClientPh" & _
        "oneNumber, JBClientFaxNumber, JBClientMobileNumber, JBJobAddressAsAbove, JBJobSt" & _
        "reetAddress01, JBJobStreetAddress02, JBJobSuburb, JBJobState, JBJobPostCode, JBJ" & _
        "obAddress, JTID, JBGraniteBAOTPrice, JBCabinetryBAOTPrice, JBPrice, JBReferenceN" & _
        "ame, JBJobInsurance, JBIsCancelled, JBClientEmail, JBInventoryDate FROM Jobs WHE" & _
        "RE (BRID = @BRID) AND (JBID = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBScheduledStartDate", System.Data.SqlDbType.DateTime, 8, "JBScheduledStartDate"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBScheduledFinishDate", System.Data.SqlDbType.DateTime, 8, "JBScheduledFinishDate"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBActualFinishDate", System.Data.SqlDbType.DateTime, 8, "JBActualFinishDate"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientFirstName", System.Data.SqlDbType.VarChar, 50, "JBClientFirstName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientSurname", System.Data.SqlDbType.VarChar, 50, "JBClientSurname"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientName", System.Data.SqlDbType.VarChar, 102, "JBClientName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, "JBClientStreetAddress01"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, "JBClientStreetAddress02"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientSuburb", System.Data.SqlDbType.VarChar, 50, "JBClientSuburb"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientState", System.Data.SqlDbType.VarChar, 3, "JBClientState"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientPostCode", System.Data.SqlDbType.VarChar, 20, "JBClientPostCode"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientAddress", System.Data.SqlDbType.VarChar, 259, "JBClientAddress"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientPhoneNumber", System.Data.SqlDbType.VarChar, 20, "JBClientPhoneNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientFaxNumber", System.Data.SqlDbType.VarChar, 20, "JBClientFaxNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientMobileNumber", System.Data.SqlDbType.VarChar, 20, "JBClientMobileNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, "JBJobAddressAsAbove"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, "JBJobStreetAddress01"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, "JBJobStreetAddress02"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobSuburb", System.Data.SqlDbType.VarChar, 50, "JBJobSuburb"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobState", System.Data.SqlDbType.VarChar, 3, "JBJobState"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobPostCode", System.Data.SqlDbType.VarChar, 20, "JBJobPostCode"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobAddress", System.Data.SqlDbType.VarChar, 259, "JBJobAddress"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JTID", System.Data.SqlDbType.Int, 4, "JTID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBGraniteBAOTPrice", System.Data.SqlDbType.Money, 8, "JBGraniteBAOTPrice"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBCabinetryBAOTPrice", System.Data.SqlDbType.Money, 8, "JBCabinetryBAOTPrice"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBPrice", System.Data.SqlDbType.Money, 8, "JBPrice"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBReferenceName", System.Data.SqlDbType.VarChar, 100, "JBReferenceName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobInsurance", System.Data.SqlDbType.Money, 8, "JBJobInsurance"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBIsCancelled", System.Data.SqlDbType.Bit, 1, "JBIsCancelled"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientEmail", System.Data.SqlDbType.VarChar, 50, "JBClientEmail"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBInventoryDate", System.Data.SqlDbType.DateTime, 8, "JBInventoryDate"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, JBID, ID, JBScheduledStartDate, JBScheduledFinishDate, JBActualFinis" & _
        "hDate, JBClientFirstName, JBClientSurname, JBClientName, JBClientStreetAddress01" & _
        ", JBClientStreetAddress02, JBClientSuburb, JBClientState, JBClientPostCode, JBCl" & _
        "ientAddress, JBClientPhoneNumber, JBClientFaxNumber, JBClientMobileNumber, JBJob" & _
        "AddressAsAbove, JBJobStreetAddress01, JBJobStreetAddress02, JBJobSuburb, JBJobSt" & _
        "ate, JBJobPostCode, JBJobAddress, JTID, JBGraniteBAOTPrice, JBCabinetryBAOTPrice" & _
        ", JBPrice, JBReferenceName, JBJobInsurance, JBIsCancelled, JBClientEmail, JBInve" & _
        "ntoryDate FROM Jobs WHERE (BRID = @BRID) AND (JBID = @JBID)"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Jobs SET BRID = @BRID, ID = @ID, JBScheduledStartDate = @JBScheduledStartD" & _
        "ate, JBScheduledFinishDate = @JBScheduledFinishDate, JBActualFinishDate = @JBAct" & _
        "ualFinishDate, JBClientFirstName = @JBClientFirstName, JBClientSurname = @JBClie" & _
        "ntSurname, JBClientName = @JBClientName, JBClientStreetAddress01 = @JBClientStre" & _
        "etAddress01, JBClientStreetAddress02 = @JBClientStreetAddress02, JBClientSuburb " & _
        "= @JBClientSuburb, JBClientState = @JBClientState, JBClientPostCode = @JBClientP" & _
        "ostCode, JBClientAddress = @JBClientAddress, JBClientPhoneNumber = @JBClientPhon" & _
        "eNumber, JBClientFaxNumber = @JBClientFaxNumber, JBClientMobileNumber = @JBClien" & _
        "tMobileNumber, JBJobAddressAsAbove = @JBJobAddressAsAbove, JBJobStreetAddress01 " & _
        "= @JBJobStreetAddress01, JBJobStreetAddress02 = @JBJobStreetAddress02, JBJobSubu" & _
        "rb = @JBJobSuburb, JBJobState = @JBJobState, JBJobPostCode = @JBJobPostCode, JBJ" & _
        "obAddress = @JBJobAddress, JTID = @JTID, JBGraniteBAOTPrice = @JBGraniteBAOTPric" & _
        "e, JBCabinetryBAOTPrice = @JBCabinetryBAOTPrice, JBPrice = @JBPrice, JBReference" & _
        "Name = @JBReferenceName, JBJobInsurance = @JBJobInsurance, JBIsCancelled = @JBIs" & _
        "Cancelled, JBClientEmail = @JBClientEmail, JBInventoryDate = @JBInventoryDate WH" & _
        "ERE (BRID = @Original_BRID) AND (JBID = @Original_JBID) AND (ID = @Original_ID) " & _
        "AND (JBActualFinishDate = @Original_JBActualFinishDate OR @Original_JBActualFini" & _
        "shDate IS NULL AND JBActualFinishDate IS NULL) AND (JBCabinetryBAOTPrice = @Orig" & _
        "inal_JBCabinetryBAOTPrice OR @Original_JBCabinetryBAOTPrice IS NULL AND JBCabine" & _
        "tryBAOTPrice IS NULL) AND (JBClientAddress = @Original_JBClientAddress OR @Origi" & _
        "nal_JBClientAddress IS NULL AND JBClientAddress IS NULL) AND (JBClientEmail = @O" & _
        "riginal_JBClientEmail OR @Original_JBClientEmail IS NULL AND JBClientEmail IS NU" & _
        "LL) AND (JBClientFaxNumber = @Original_JBClientFaxNumber OR @Original_JBClientFa" & _
        "xNumber IS NULL AND JBClientFaxNumber IS NULL) AND (JBClientFirstName = @Origina" & _
        "l_JBClientFirstName OR @Original_JBClientFirstName IS NULL AND JBClientFirstName" & _
        " IS NULL) AND (JBClientMobileNumber = @Original_JBClientMobileNumber OR @Origina" & _
        "l_JBClientMobileNumber IS NULL AND JBClientMobileNumber IS NULL) AND (JBClientNa" & _
        "me = @Original_JBClientName OR @Original_JBClientName IS NULL AND JBClientName I" & _
        "S NULL) AND (JBClientPhoneNumber = @Original_JBClientPhoneNumber OR @Original_JB" & _
        "ClientPhoneNumber IS NULL AND JBClientPhoneNumber IS NULL) AND (JBClientPostCode" & _
        " = @Original_JBClientPostCode OR @Original_JBClientPostCode IS NULL AND JBClient" & _
        "PostCode IS NULL) AND (JBClientState = @Original_JBClientState OR @Original_JBCl" & _
        "ientState IS NULL AND JBClientState IS NULL) AND (JBClientStreetAddress01 = @Ori" & _
        "ginal_JBClientStreetAddress01 OR @Original_JBClientStreetAddress01 IS NULL AND J" & _
        "BClientStreetAddress01 IS NULL) AND (JBClientStreetAddress02 = @Original_JBClien" & _
        "tStreetAddress02 OR @Original_JBClientStreetAddress02 IS NULL AND JBClientStreet" & _
        "Address02 IS NULL) AND (JBClientSuburb = @Original_JBClientSuburb OR @Original_J" & _
        "BClientSuburb IS NULL AND JBClientSuburb IS NULL) AND (JBClientSurname = @Origin" & _
        "al_JBClientSurname OR @Original_JBClientSurname IS NULL AND JBClientSurname IS N" & _
        "ULL) AND (JBGraniteBAOTPrice = @Original_JBGraniteBAOTPrice OR @Original_JBGrani" & _
        "teBAOTPrice IS NULL AND JBGraniteBAOTPrice IS NULL) AND (JBInventoryDate = @Orig" & _
        "inal_JBInventoryDate OR @Original_JBInventoryDate IS NULL AND JBInventoryDate IS" & _
        " NULL) AND (JBIsCancelled = @Original_JBIsCancelled OR @Original_JBIsCancelled I" & _
        "S NULL AND JBIsCancelled IS NULL) AND (JBJobAddress = @Original_JBJobAddress OR " & _
        "@Original_JBJobAddress IS NULL AND JBJobAddress IS NULL) AND (JBJobAddressAsAbov" & _
        "e = @Original_JBJobAddressAsAbove OR @Original_JBJobAddressAsAbove IS NULL AND J" & _
        "BJobAddressAsAbove IS NULL) AND (JBJobInsurance = @Original_JBJobInsurance OR @O" & _
        "riginal_JBJobInsurance IS NULL AND JBJobInsurance IS NULL) AND (JBJobPostCode = " & _
        "@Original_JBJobPostCode OR @Original_JBJobPostCode IS NULL AND JBJobPostCode IS " & _
        "NULL) AND (JBJobState = @Original_JBJobState OR @Original_JBJobState IS NULL AND" & _
        " JBJobState IS NULL) AND (JBJobStreetAddress01 = @Original_JBJobStreetAddress01 " & _
        "OR @Original_JBJobStreetAddress01 IS NULL AND JBJobStreetAddress01 IS NULL) AND " & _
        "(JBJobStreetAddress02 = @Original_JBJobStreetAddress02 OR @Original_JBJobStreetA" & _
        "ddress02 IS NULL AND JBJobStreetAddress02 IS NULL) AND (JBJobSuburb = @Original_" & _
        "JBJobSuburb OR @Original_JBJobSuburb IS NULL AND JBJobSuburb IS NULL) AND (JBPri" & _
        "ce = @Original_JBPrice OR @Original_JBPrice IS NULL AND JBPrice IS NULL) AND (JB" & _
        "ReferenceName = @Original_JBReferenceName OR @Original_JBReferenceName IS NULL A" & _
        "ND JBReferenceName IS NULL) AND (JBScheduledFinishDate = @Original_JBScheduledFi" & _
        "nishDate OR @Original_JBScheduledFinishDate IS NULL AND JBScheduledFinishDate IS" & _
        " NULL) AND (JBScheduledStartDate = @Original_JBScheduledStartDate OR @Original_J" & _
        "BScheduledStartDate IS NULL AND JBScheduledStartDate IS NULL) AND (JTID = @Origi" & _
        "nal_JTID OR @Original_JTID IS NULL AND JTID IS NULL); SELECT BRID, JBID, ID, JBS" & _
        "cheduledStartDate, JBScheduledFinishDate, JBActualFinishDate, JBClientFirstName," & _
        " JBClientSurname, JBClientName, JBClientStreetAddress01, JBClientStreetAddress02" & _
        ", JBClientSuburb, JBClientState, JBClientPostCode, JBClientAddress, JBClientPhon" & _
        "eNumber, JBClientFaxNumber, JBClientMobileNumber, JBJobAddressAsAbove, JBJobStre" & _
        "etAddress01, JBJobStreetAddress02, JBJobSuburb, JBJobState, JBJobPostCode, JBJob" & _
        "Address, JTID, JBGraniteBAOTPrice, JBCabinetryBAOTPrice, JBPrice, JBReferenceNam" & _
        "e, JBJobInsurance, JBIsCancelled, JBClientEmail, JBInventoryDate FROM Jobs WHERE" & _
        " (BRID = @BRID) AND (JBID = @JBID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBScheduledStartDate", System.Data.SqlDbType.DateTime, 8, "JBScheduledStartDate"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBScheduledFinishDate", System.Data.SqlDbType.DateTime, 8, "JBScheduledFinishDate"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBActualFinishDate", System.Data.SqlDbType.DateTime, 8, "JBActualFinishDate"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientFirstName", System.Data.SqlDbType.VarChar, 50, "JBClientFirstName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientSurname", System.Data.SqlDbType.VarChar, 50, "JBClientSurname"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientName", System.Data.SqlDbType.VarChar, 102, "JBClientName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, "JBClientStreetAddress01"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, "JBClientStreetAddress02"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientSuburb", System.Data.SqlDbType.VarChar, 50, "JBClientSuburb"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientState", System.Data.SqlDbType.VarChar, 3, "JBClientState"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientPostCode", System.Data.SqlDbType.VarChar, 20, "JBClientPostCode"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientAddress", System.Data.SqlDbType.VarChar, 259, "JBClientAddress"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientPhoneNumber", System.Data.SqlDbType.VarChar, 20, "JBClientPhoneNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientFaxNumber", System.Data.SqlDbType.VarChar, 20, "JBClientFaxNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientMobileNumber", System.Data.SqlDbType.VarChar, 20, "JBClientMobileNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, "JBJobAddressAsAbove"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, "JBJobStreetAddress01"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, "JBJobStreetAddress02"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobSuburb", System.Data.SqlDbType.VarChar, 50, "JBJobSuburb"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobState", System.Data.SqlDbType.VarChar, 3, "JBJobState"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobPostCode", System.Data.SqlDbType.VarChar, 20, "JBJobPostCode"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobAddress", System.Data.SqlDbType.VarChar, 259, "JBJobAddress"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JTID", System.Data.SqlDbType.Int, 4, "JTID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBGraniteBAOTPrice", System.Data.SqlDbType.Money, 8, "JBGraniteBAOTPrice"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBCabinetryBAOTPrice", System.Data.SqlDbType.Money, 8, "JBCabinetryBAOTPrice"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBPrice", System.Data.SqlDbType.Money, 8, "JBPrice"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBReferenceName", System.Data.SqlDbType.VarChar, 100, "JBReferenceName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobInsurance", System.Data.SqlDbType.Money, 8, "JBJobInsurance"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBIsCancelled", System.Data.SqlDbType.Bit, 1, "JBIsCancelled"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientEmail", System.Data.SqlDbType.VarChar, 50, "JBClientEmail"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBInventoryDate", System.Data.SqlDbType.DateTime, 8, "JBInventoryDate"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBActualFinishDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBActualFinishDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBCabinetryBAOTPrice", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBCabinetryBAOTPrice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientFirstName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientFirstName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientName", System.Data.SqlDbType.VarChar, 102, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBClientSurname", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBClientSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBGraniteBAOTPrice", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBGraniteBAOTPrice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBInventoryDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBInventoryDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBIsCancelled", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBIsCancelled", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobAddressAsAbove", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobInsurance", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobInsurance", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBJobSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBJobSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBPrice", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBPrice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBReferenceName", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBReferenceName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBScheduledFinishDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBScheduledFinishDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBScheduledStartDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBScheduledStartDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'pnlMain
        '
        Me.pnlMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlMain.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.pnlMain.Controls.Add(Me.pnlSalesReps)
        Me.pnlMain.Controls.Add(Me.pnlDirectLabour)
        Me.pnlMain.Controls.Add(Me.pnlMaterials)
        Me.pnlMain.Controls.Add(Me.pnlIntro)
        Me.pnlMain.Controls.Add(Me.pnlClientPayments)
        Me.pnlMain.Controls.Add(Me.pnlJobAddress)
        Me.pnlMain.Controls.Add(Me.pnlClientInformation)
        Me.pnlMain.Controls.Add(Me.pnlAppliances)
        Me.pnlMain.Controls.Add(Me.pnlGranite)
        Me.pnlMain.Controls.Add(Me.pnlResponsibilities)
        Me.pnlMain.Controls.Add(Me.pnlOtherTrades)
        Me.pnlMain.Controls.Add(Me.pnlNotes)
        Me.pnlMain.Controls.Add(Me.pnlJobIssues)
        Me.pnlMain.Controls.Add(Me.pnlJobInformation)
        Me.pnlMain.Controls.Add(Me.pnlSummary)
        Me.pnlMain.Location = New System.Drawing.Point(152, 8)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(632, 488)
        Me.pnlMain.TabIndex = 1
        '
        'pnlMaterials
        '
        Me.pnlMaterials.Controls.Add(Me.btnRemoveMaterial)
        Me.pnlMaterials.Controls.Add(Me.btnAddMaterial)
        Me.pnlMaterials.Controls.Add(Me.dgMaterials)
        Me.pnlMaterials.Controls.Add(Me.tvMaterial)
        Me.pnlMaterials.Controls.Add(Me.Label22)
        Me.pnlMaterials.Controls.Add(Me.Label23)
        Me.pnlMaterials.Location = New System.Drawing.Point(48, 136)
        Me.pnlMaterials.Name = "pnlMaterials"
        Me.pnlMaterials.Size = New System.Drawing.Size(584, 328)
        Me.pnlMaterials.TabIndex = 3
        Me.pnlMaterials.Text = "Non-Tracked Material"
        '
        'btnRemoveMaterial
        '
        Me.btnRemoveMaterial.Image = CType(resources.GetObject("btnRemoveMaterial.Image"), System.Drawing.Image)
        Me.btnRemoveMaterial.Location = New System.Drawing.Point(208, 184)
        Me.btnRemoveMaterial.Name = "btnRemoveMaterial"
        Me.btnRemoveMaterial.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveMaterial.TabIndex = 2
        Me.btnRemoveMaterial.Text = "Remove"
        '
        'btnAddMaterial
        '
        Me.btnAddMaterial.Image = CType(resources.GetObject("btnAddMaterial.Image"), System.Drawing.Image)
        Me.btnAddMaterial.ImageAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnAddMaterial.Location = New System.Drawing.Point(208, 152)
        Me.btnAddMaterial.Name = "btnAddMaterial"
        Me.btnAddMaterial.Size = New System.Drawing.Size(72, 23)
        Me.btnAddMaterial.TabIndex = 1
        Me.btnAddMaterial.Text = "Add"
        '
        'dgMaterials
        '
        Me.dgMaterials.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgMaterials.DataSource = Me.dvJobs_Materials
        '
        'dgMaterials.EmbeddedNavigator
        '
        Me.dgMaterials.EmbeddedNavigator.Name = ""
        Me.dgMaterials.Location = New System.Drawing.Point(288, 48)
        Me.dgMaterials.MainView = Me.gvMaterials
        Me.dgMaterials.Name = "dgMaterials"
        Me.dgMaterials.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtNumericMaterials})
        Me.dgMaterials.Size = New System.Drawing.Size(280, 264)
        Me.dgMaterials.TabIndex = 3
        Me.dgMaterials.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvMaterials})
        '
        'dvJobs_Materials
        '
        Me.dvJobs_Materials.AllowNew = False
        Me.dvJobs_Materials.RowFilter = "MTStocked = 0"
        Me.dvJobs_Materials.Table = Me.DsGTMS.VJobs_Materials
        '
        'gvMaterials
        '
        Me.gvMaterials.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvMaterials.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvMaterials.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvMaterials.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvMaterials.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvMaterials.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvMaterials.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvMaterials.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvMaterials.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvMaterials.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvMaterials.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvMaterials.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvMaterials.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvMaterials.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvMaterials.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvMaterials.Appearance.VertLine.Options.UseBackColor = True
        Me.gvMaterials.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colMTName, Me.colJMPrimarySold, Me.colMTPrimaryUOMShortName1})
        Me.gvMaterials.GridControl = Me.dgMaterials
        Me.gvMaterials.Name = "gvMaterials"
        Me.gvMaterials.OptionsCustomization.AllowFilter = False
        Me.gvMaterials.OptionsNavigation.AutoFocusNewRow = True
        Me.gvMaterials.OptionsView.ShowGroupPanel = False
        Me.gvMaterials.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colMTName, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colMTName
        '
        Me.colMTName.Caption = "Material"
        Me.colMTName.FieldName = "MTName"
        Me.colMTName.Name = "colMTName"
        Me.colMTName.OptionsColumn.AllowEdit = False
        Me.colMTName.OptionsColumn.AllowFocus = False
        Me.colMTName.Visible = True
        Me.colMTName.VisibleIndex = 0
        Me.colMTName.Width = 153
        '
        'colJMPrimarySold
        '
        Me.colJMPrimarySold.Caption = "Amount *"
        Me.colJMPrimarySold.ColumnEdit = Me.txtNumericMaterials
        Me.colJMPrimarySold.DisplayFormat.FormatString = "#0.000"
        Me.colJMPrimarySold.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colJMPrimarySold.FieldName = "JMPrimarySold"
        Me.colJMPrimarySold.Name = "colJMPrimarySold"
        Me.colJMPrimarySold.Visible = True
        Me.colJMPrimarySold.VisibleIndex = 1
        Me.colJMPrimarySold.Width = 80
        '
        'txtNumericMaterials
        '
        Me.txtNumericMaterials.AutoHeight = False
        Me.txtNumericMaterials.DisplayFormat.FormatString = "#0.000"
        Me.txtNumericMaterials.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtNumericMaterials.EditFormat.FormatString = "#0.000"
        Me.txtNumericMaterials.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtNumericMaterials.Name = "txtNumericMaterials"
        Me.txtNumericMaterials.NullText = "<Incomplete>"
        '
        'colMTPrimaryUOMShortName1
        '
        Me.colMTPrimaryUOMShortName1.FieldName = "MTPrimaryUOMShortName"
        Me.colMTPrimaryUOMShortName1.Name = "colMTPrimaryUOMShortName1"
        Me.colMTPrimaryUOMShortName1.OptionsColumn.AllowEdit = False
        Me.colMTPrimaryUOMShortName1.OptionsColumn.AllowFocus = False
        Me.colMTPrimaryUOMShortName1.Visible = True
        Me.colMTPrimaryUOMShortName1.VisibleIndex = 2
        Me.colMTPrimaryUOMShortName1.Width = 57
        '
        'tvMaterial
        '
        Me.tvMaterial.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.tvMaterial.Connection = Me.SqlConnection
        Me.tvMaterial.HideSelection = False
        Me.tvMaterial.ImageIndex = -1
        Me.tvMaterial.Location = New System.Drawing.Point(16, 48)
        Me.tvMaterial.Name = "tvMaterial"
        Me.tvMaterial.SelectedImageIndex = -1
        Me.tvMaterial.Size = New System.Drawing.Size(184, 264)
        Me.tvMaterial.TabIndex = 0
        '
        'Label22
        '
        Me.Label22.Location = New System.Drawing.Point(16, 24)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(176, 21)
        Me.Label22.TabIndex = 2
        Me.Label22.Text = "Available non-tracked material:"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label23
        '
        Me.Label23.Location = New System.Drawing.Point(288, 24)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(192, 21)
        Me.Label23.TabIndex = 2
        Me.Label23.Text = "Non-tracked material in job:"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlIntro
        '
        Me.pnlIntro.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.pnlIntro.Appearance.Options.UseBackColor = True
        Me.pnlIntro.Controls.Add(Me.PictureBox1)
        Me.pnlIntro.Controls.Add(Me.Label44)
        Me.pnlIntro.Controls.Add(Me.PictureEdit4)
        Me.pnlIntro.Controls.Add(Me.Label43)
        Me.pnlIntro.Controls.Add(Me.Label42)
        Me.pnlIntro.Controls.Add(Me.PictureEdit3)
        Me.pnlIntro.Controls.Add(Me.Label48)
        Me.pnlIntro.Controls.Add(Me.PictureEdit1)
        Me.pnlIntro.Location = New System.Drawing.Point(48, 40)
        Me.pnlIntro.Name = "pnlIntro"
        Me.pnlIntro.Size = New System.Drawing.Size(600, 432)
        Me.pnlIntro.TabIndex = 14
        Me.pnlIntro.Text = "Introduction"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(96, 90)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(16, 16)
        Me.PictureBox1.TabIndex = 6
        Me.PictureBox1.TabStop = False
        '
        'Label44
        '
        Me.Label44.BackColor = System.Drawing.Color.Transparent
        Me.Label44.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.Location = New System.Drawing.Point(96, 264)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(472, 32)
        Me.Label44.TabIndex = 5
        Me.Label44.Text = "Where indirect tax applies, all information entered into the Job screen is includ" & _
        "ing indirect tax."
        '
        'PictureEdit4
        '
        Me.PictureEdit4.EditValue = CType(resources.GetObject("PictureEdit4.EditValue"), Object)
        Me.PictureEdit4.Location = New System.Drawing.Point(72, 264)
        Me.PictureEdit4.Name = "PictureEdit4"
        '
        'PictureEdit4.Properties
        '
        Me.PictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit4.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit4.Size = New System.Drawing.Size(16, 16)
        Me.PictureEdit4.TabIndex = 4
        '
        'Label43
        '
        Me.Label43.BackColor = System.Drawing.Color.Transparent
        Me.Label43.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.Location = New System.Drawing.Point(96, 192)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(472, 64)
        Me.Label43.TabIndex = 3
        Me.Label43.Text = "Any data or inputs that have a * must be entered to make the status of the job ""C" & _
        "omplete"". Any data or inputs with * that have not yet been entered will have <In" & _
        "complete> status.  These fields must be entered in order to make the job status " & _
        """Complete"". You may have to enter zero into a field in order to make the status " & _
        "of the job ""Complete"". "
        '
        'Label42
        '
        Me.Label42.BackColor = System.Drawing.Color.Transparent
        Me.Label42.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.Location = New System.Drawing.Point(120, 88)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(280, 23)
        Me.Label42.TabIndex = 2
        Me.Label42.Text = "Read below before you begin:"
        '
        'PictureEdit3
        '
        Me.PictureEdit3.EditValue = CType(resources.GetObject("PictureEdit3.EditValue"), Object)
        Me.PictureEdit3.Location = New System.Drawing.Point(72, 192)
        Me.PictureEdit3.Name = "PictureEdit3"
        '
        'PictureEdit3.Properties
        '
        Me.PictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit3.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit3.Size = New System.Drawing.Size(16, 16)
        Me.PictureEdit3.TabIndex = 0
        '
        'Label48
        '
        Me.Label48.BackColor = System.Drawing.Color.Transparent
        Me.Label48.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.Location = New System.Drawing.Point(96, 128)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(472, 56)
        Me.Label48.TabIndex = 5
        Me.Label48.Text = "A job may have a status of ""Complete"" or ""Incomplete"". This has nothing to do wit" & _
        "h whether a job has been finished or not.  It actually refers to whether or not " & _
        "all the required information has been entered for this job. "
        '
        'PictureEdit1
        '
        Me.PictureEdit1.EditValue = CType(resources.GetObject("PictureEdit1.EditValue"), Object)
        Me.PictureEdit1.Location = New System.Drawing.Point(72, 128)
        Me.PictureEdit1.Name = "PictureEdit1"
        '
        'PictureEdit1.Properties
        '
        Me.PictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit1.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit1.Size = New System.Drawing.Size(16, 16)
        Me.PictureEdit1.TabIndex = 4
        '
        'pnlClientPayments
        '
        Me.pnlClientPayments.Controls.Add(Me.Label41)
        Me.pnlClientPayments.Controls.Add(Me.dgClientPayments)
        Me.pnlClientPayments.Controls.Add(Me.btnRemoveClientPayment)
        Me.pnlClientPayments.Controls.Add(Me.dgDiscounts)
        Me.pnlClientPayments.Controls.Add(Me.Label35)
        Me.pnlClientPayments.Controls.Add(Me.btnRemoveBadDebt)
        Me.pnlClientPayments.Controls.Add(Me.dgBadDebts)
        Me.pnlClientPayments.Controls.Add(Me.btnRemoveDiscount)
        Me.pnlClientPayments.Controls.Add(Me.Label34)
        Me.pnlClientPayments.Location = New System.Drawing.Point(40, 0)
        Me.pnlClientPayments.Name = "pnlClientPayments"
        Me.pnlClientPayments.Size = New System.Drawing.Size(560, 480)
        Me.pnlClientPayments.TabIndex = 7
        Me.pnlClientPayments.Text = "Customer Payments"
        '
        'Label41
        '
        Me.Label41.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.Location = New System.Drawing.Point(16, 24)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(240, 23)
        Me.Label41.TabIndex = 11
        Me.Label41.Text = "Payments made by customer:"
        Me.Label41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dgClientPayments
        '
        Me.dgClientPayments.DataSource = Me.dvClientPayments
        '
        'dgClientPayments.EmbeddedNavigator
        '
        Me.dgClientPayments.EmbeddedNavigator.Name = ""
        Me.dgClientPayments.ExternalRepository = Me.PersistentRepository1
        Me.dgClientPayments.Location = New System.Drawing.Point(16, 48)
        Me.dgClientPayments.MainView = Me.gvClientPayments
        Me.dgClientPayments.Name = "dgClientPayments"
        Me.dgClientPayments.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtCurrencyClientPayments})
        Me.dgClientPayments.Size = New System.Drawing.Size(528, 88)
        Me.dgClientPayments.TabIndex = 0
        Me.dgClientPayments.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvClientPayments})
        '
        'dvClientPayments
        '
        Me.dvClientPayments.RowFilter = "CPType = 'CP'"
        Me.dvClientPayments.Table = Me.DsGTMS.ClientPayments
        '
        'PersistentRepository1
        '
        Me.PersistentRepository1.Items.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtDate})
        '
        'txtDate
        '
        Me.txtDate.AutoHeight = False
        Me.txtDate.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtDate.Name = "txtDate"
        '
        'gvClientPayments
        '
        Me.gvClientPayments.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvClientPayments.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvClientPayments.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvClientPayments.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvClientPayments.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvClientPayments.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvClientPayments.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvClientPayments.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvClientPayments.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvClientPayments.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvClientPayments.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvClientPayments.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvClientPayments.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvClientPayments.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvClientPayments.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvClientPayments.Appearance.VertLine.Options.UseBackColor = True
        Me.gvClientPayments.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCPDate, Me.colCPAmount, Me.colCPNotes})
        Me.gvClientPayments.GridControl = Me.dgClientPayments
        Me.gvClientPayments.Name = "gvClientPayments"
        Me.gvClientPayments.OptionsCustomization.AllowFilter = False
        Me.gvClientPayments.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvClientPayments.OptionsView.ShowGroupPanel = False
        '
        'colCPDate
        '
        Me.colCPDate.Caption = "Date"
        Me.colCPDate.ColumnEdit = Me.txtDate
        Me.colCPDate.FieldName = "CPDate"
        Me.colCPDate.Name = "colCPDate"
        Me.colCPDate.Visible = True
        Me.colCPDate.VisibleIndex = 0
        Me.colCPDate.Width = 187
        '
        'colCPAmount
        '
        Me.colCPAmount.Caption = "Amount *"
        Me.colCPAmount.ColumnEdit = Me.txtCurrencyClientPayments
        Me.colCPAmount.FieldName = "CPAmount"
        Me.colCPAmount.Name = "colCPAmount"
        Me.colCPAmount.Visible = True
        Me.colCPAmount.VisibleIndex = 2
        Me.colCPAmount.Width = 161
        '
        'txtCurrencyClientPayments
        '
        Me.txtCurrencyClientPayments.AutoHeight = False
        Me.txtCurrencyClientPayments.DisplayFormat.FormatString = "c"
        Me.txtCurrencyClientPayments.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCurrencyClientPayments.EditFormat.FormatString = "c"
        Me.txtCurrencyClientPayments.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCurrencyClientPayments.Name = "txtCurrencyClientPayments"
        '
        'colCPNotes
        '
        Me.colCPNotes.Caption = "Notes"
        Me.colCPNotes.FieldName = "CPNotes"
        Me.colCPNotes.Name = "colCPNotes"
        Me.colCPNotes.Visible = True
        Me.colCPNotes.VisibleIndex = 1
        Me.colCPNotes.Width = 477
        '
        'btnRemoveClientPayment
        '
        Me.btnRemoveClientPayment.Image = CType(resources.GetObject("btnRemoveClientPayment.Image"), System.Drawing.Image)
        Me.btnRemoveClientPayment.Location = New System.Drawing.Point(16, 144)
        Me.btnRemoveClientPayment.Name = "btnRemoveClientPayment"
        Me.btnRemoveClientPayment.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveClientPayment.TabIndex = 1
        Me.btnRemoveClientPayment.Text = "Remove"
        '
        'dgDiscounts
        '
        Me.dgDiscounts.DataSource = Me.dvDiscounts
        '
        'dgDiscounts.EmbeddedNavigator
        '
        Me.dgDiscounts.EmbeddedNavigator.Name = ""
        Me.dgDiscounts.ExternalRepository = Me.PersistentRepository1
        Me.dgDiscounts.Location = New System.Drawing.Point(16, 200)
        Me.dgDiscounts.MainView = Me.gvDiscounts
        Me.dgDiscounts.Name = "dgDiscounts"
        Me.dgDiscounts.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtCurrencyDiscounts})
        Me.dgDiscounts.Size = New System.Drawing.Size(528, 88)
        Me.dgDiscounts.TabIndex = 2
        Me.dgDiscounts.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvDiscounts})
        '
        'dvDiscounts
        '
        Me.dvDiscounts.RowFilter = "CPType = 'DC'"
        Me.dvDiscounts.Table = Me.DsGTMS.ClientPayments
        '
        'gvDiscounts
        '
        Me.gvDiscounts.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvDiscounts.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvDiscounts.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvDiscounts.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvDiscounts.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvDiscounts.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvDiscounts.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvDiscounts.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvDiscounts.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvDiscounts.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvDiscounts.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvDiscounts.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvDiscounts.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvDiscounts.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvDiscounts.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvDiscounts.Appearance.VertLine.Options.UseBackColor = True
        Me.gvDiscounts.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCPDate2, Me.colCPAmount2, Me.colCPNotes2})
        Me.gvDiscounts.GridControl = Me.dgDiscounts
        Me.gvDiscounts.Name = "gvDiscounts"
        Me.gvDiscounts.OptionsCustomization.AllowFilter = False
        Me.gvDiscounts.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvDiscounts.OptionsView.ShowGroupPanel = False
        '
        'colCPDate2
        '
        Me.colCPDate2.Caption = "Date"
        Me.colCPDate2.ColumnEdit = Me.txtDate
        Me.colCPDate2.FieldName = "CPDate"
        Me.colCPDate2.Name = "colCPDate2"
        Me.colCPDate2.Visible = True
        Me.colCPDate2.VisibleIndex = 0
        Me.colCPDate2.Width = 187
        '
        'colCPAmount2
        '
        Me.colCPAmount2.Caption = "Amount *"
        Me.colCPAmount2.ColumnEdit = Me.txtCurrencyDiscounts
        Me.colCPAmount2.FieldName = "CPAmount"
        Me.colCPAmount2.Name = "colCPAmount2"
        Me.colCPAmount2.Visible = True
        Me.colCPAmount2.VisibleIndex = 2
        Me.colCPAmount2.Width = 161
        '
        'txtCurrencyDiscounts
        '
        Me.txtCurrencyDiscounts.AutoHeight = False
        Me.txtCurrencyDiscounts.DisplayFormat.FormatString = "c"
        Me.txtCurrencyDiscounts.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCurrencyDiscounts.EditFormat.FormatString = "c"
        Me.txtCurrencyDiscounts.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCurrencyDiscounts.Name = "txtCurrencyDiscounts"
        '
        'colCPNotes2
        '
        Me.colCPNotes2.Caption = "Notes"
        Me.colCPNotes2.FieldName = "CPNotes"
        Me.colCPNotes2.Name = "colCPNotes2"
        Me.colCPNotes2.Visible = True
        Me.colCPNotes2.VisibleIndex = 1
        Me.colCPNotes2.Width = 477
        '
        'Label35
        '
        Me.Label35.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Location = New System.Drawing.Point(16, 176)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(240, 23)
        Me.Label35.TabIndex = 5
        Me.Label35.Text = "Discounts given to customer:"
        Me.Label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnRemoveBadDebt
        '
        Me.btnRemoveBadDebt.Image = CType(resources.GetObject("btnRemoveBadDebt.Image"), System.Drawing.Image)
        Me.btnRemoveBadDebt.Location = New System.Drawing.Point(16, 448)
        Me.btnRemoveBadDebt.Name = "btnRemoveBadDebt"
        Me.btnRemoveBadDebt.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveBadDebt.TabIndex = 5
        Me.btnRemoveBadDebt.Text = "Remove"
        '
        'dgBadDebts
        '
        Me.dgBadDebts.DataSource = Me.dvBadDebts
        '
        'dgBadDebts.EmbeddedNavigator
        '
        Me.dgBadDebts.EmbeddedNavigator.Name = ""
        Me.dgBadDebts.ExternalRepository = Me.PersistentRepository1
        Me.dgBadDebts.Location = New System.Drawing.Point(16, 352)
        Me.dgBadDebts.MainView = Me.gvBadDebts
        Me.dgBadDebts.Name = "dgBadDebts"
        Me.dgBadDebts.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtCurrencyBadDebts})
        Me.dgBadDebts.Size = New System.Drawing.Size(528, 88)
        Me.dgBadDebts.TabIndex = 4
        Me.dgBadDebts.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvBadDebts})
        '
        'dvBadDebts
        '
        Me.dvBadDebts.RowFilter = "CPType = 'BD'"
        Me.dvBadDebts.Table = Me.DsGTMS.ClientPayments
        '
        'gvBadDebts
        '
        Me.gvBadDebts.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvBadDebts.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvBadDebts.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvBadDebts.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvBadDebts.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvBadDebts.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvBadDebts.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvBadDebts.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvBadDebts.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvBadDebts.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvBadDebts.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvBadDebts.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvBadDebts.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvBadDebts.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvBadDebts.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvBadDebts.Appearance.VertLine.Options.UseBackColor = True
        Me.gvBadDebts.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3})
        Me.gvBadDebts.GridControl = Me.dgBadDebts
        Me.gvBadDebts.Name = "gvBadDebts"
        Me.gvBadDebts.OptionsCustomization.AllowFilter = False
        Me.gvBadDebts.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvBadDebts.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Date"
        Me.GridColumn1.ColumnEdit = Me.txtDate
        Me.GridColumn1.FieldName = "CPDate"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 187
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Amount *"
        Me.GridColumn2.ColumnEdit = Me.txtCurrencyBadDebts
        Me.GridColumn2.FieldName = "CPAmount"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 2
        Me.GridColumn2.Width = 161
        '
        'txtCurrencyBadDebts
        '
        Me.txtCurrencyBadDebts.AutoHeight = False
        Me.txtCurrencyBadDebts.DisplayFormat.FormatString = "c"
        Me.txtCurrencyBadDebts.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCurrencyBadDebts.EditFormat.FormatString = "c"
        Me.txtCurrencyBadDebts.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCurrencyBadDebts.Name = "txtCurrencyBadDebts"
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Notes"
        Me.GridColumn3.FieldName = "CPNotes"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 1
        Me.GridColumn3.Width = 477
        '
        'btnRemoveDiscount
        '
        Me.btnRemoveDiscount.Image = CType(resources.GetObject("btnRemoveDiscount.Image"), System.Drawing.Image)
        Me.btnRemoveDiscount.Location = New System.Drawing.Point(16, 296)
        Me.btnRemoveDiscount.Name = "btnRemoveDiscount"
        Me.btnRemoveDiscount.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveDiscount.TabIndex = 3
        Me.btnRemoveDiscount.Text = "Remove"
        '
        'Label34
        '
        Me.Label34.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(16, 328)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(368, 23)
        Me.Label34.TabIndex = 5
        Me.Label34.Text = "Bad debts written off:"
        Me.Label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlJobAddress
        '
        Me.pnlJobAddress.Controls.Add(Me.txtJBJobStreetAddress01)
        Me.pnlJobAddress.Controls.Add(Me.rgJBJobAddressAsAbove)
        Me.pnlJobAddress.Controls.Add(Me.Label17)
        Me.pnlJobAddress.Controls.Add(Me.lblJobState)
        Me.pnlJobAddress.Controls.Add(Me.lblJobPostCode)
        Me.pnlJobAddress.Controls.Add(Me.lblJobStreetAddress)
        Me.pnlJobAddress.Controls.Add(Me.lblJobSuburb)
        Me.pnlJobAddress.Controls.Add(Me.txtJBJobStreetAddress02)
        Me.pnlJobAddress.Controls.Add(Me.txtJBJobSuburb)
        Me.pnlJobAddress.Controls.Add(Me.txtJBJobState)
        Me.pnlJobAddress.Controls.Add(Me.txtJBJobPostCode)
        Me.pnlJobAddress.Location = New System.Drawing.Point(32, 48)
        Me.pnlJobAddress.Name = "pnlJobAddress"
        Me.pnlJobAddress.Size = New System.Drawing.Size(576, 304)
        Me.pnlJobAddress.TabIndex = 2
        Me.pnlJobAddress.Text = "Job Address"
        '
        'txtJBJobStreetAddress01
        '
        Me.txtJBJobStreetAddress01.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBJobStreetAddress01"))
        Me.txtJBJobStreetAddress01.EditValue = ""
        Me.txtJBJobStreetAddress01.Location = New System.Drawing.Point(104, 104)
        Me.txtJBJobStreetAddress01.Name = "txtJBJobStreetAddress01"
        Me.txtJBJobStreetAddress01.Size = New System.Drawing.Size(368, 20)
        Me.txtJBJobStreetAddress01.TabIndex = 1
        '
        'rgJBJobAddressAsAbove
        '
        Me.rgJBJobAddressAsAbove.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBJobAddressAsAbove"))
        Me.rgJBJobAddressAsAbove.Location = New System.Drawing.Point(320, 40)
        Me.rgJBJobAddressAsAbove.Name = "rgJBJobAddressAsAbove"
        '
        'rgJBJobAddressAsAbove.Properties
        '
        Me.rgJBJobAddressAsAbove.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.rgJBJobAddressAsAbove.Properties.Appearance.Options.UseBackColor = True
        Me.rgJBJobAddressAsAbove.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.rgJBJobAddressAsAbove.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "Yes"), New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "No")})
        Me.rgJBJobAddressAsAbove.Size = New System.Drawing.Size(56, 56)
        Me.rgJBJobAddressAsAbove.TabIndex = 0
        '
        'Label17
        '
        Me.Label17.Location = New System.Drawing.Point(16, 56)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(296, 21)
        Me.Label17.TabIndex = 24
        Me.Label17.Text = "Is this job at the customer's address (same as previous)?"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobState
        '
        Me.lblJobState.Location = New System.Drawing.Point(16, 200)
        Me.lblJobState.Name = "lblJobState"
        Me.lblJobState.Size = New System.Drawing.Size(80, 21)
        Me.lblJobState.TabIndex = 21
        Me.lblJobState.Text = "State/region:"
        Me.lblJobState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobPostCode
        '
        Me.lblJobPostCode.Location = New System.Drawing.Point(296, 200)
        Me.lblJobPostCode.Name = "lblJobPostCode"
        Me.lblJobPostCode.Size = New System.Drawing.Size(96, 21)
        Me.lblJobPostCode.TabIndex = 22
        Me.lblJobPostCode.Text = "ZIP/postal code:"
        Me.lblJobPostCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobStreetAddress
        '
        Me.lblJobStreetAddress.Location = New System.Drawing.Point(16, 104)
        Me.lblJobStreetAddress.Name = "lblJobStreetAddress"
        Me.lblJobStreetAddress.Size = New System.Drawing.Size(88, 21)
        Me.lblJobStreetAddress.TabIndex = 23
        Me.lblJobStreetAddress.Text = "Street address:"
        Me.lblJobStreetAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobSuburb
        '
        Me.lblJobSuburb.Location = New System.Drawing.Point(16, 168)
        Me.lblJobSuburb.Name = "lblJobSuburb"
        Me.lblJobSuburb.Size = New System.Drawing.Size(80, 21)
        Me.lblJobSuburb.TabIndex = 20
        Me.lblJobSuburb.Text = "Suburb/town:"
        Me.lblJobSuburb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJBJobStreetAddress02
        '
        Me.txtJBJobStreetAddress02.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBJobStreetAddress02"))
        Me.txtJBJobStreetAddress02.EditValue = ""
        Me.txtJBJobStreetAddress02.Location = New System.Drawing.Point(104, 136)
        Me.txtJBJobStreetAddress02.Name = "txtJBJobStreetAddress02"
        Me.txtJBJobStreetAddress02.Size = New System.Drawing.Size(368, 20)
        Me.txtJBJobStreetAddress02.TabIndex = 2
        '
        'txtJBJobSuburb
        '
        Me.txtJBJobSuburb.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBJobSuburb"))
        Me.txtJBJobSuburb.EditValue = ""
        Me.txtJBJobSuburb.Location = New System.Drawing.Point(104, 168)
        Me.txtJBJobSuburb.Name = "txtJBJobSuburb"
        Me.txtJBJobSuburb.Size = New System.Drawing.Size(368, 20)
        Me.txtJBJobSuburb.TabIndex = 3
        '
        'txtJBJobState
        '
        Me.txtJBJobState.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBJobState"))
        Me.txtJBJobState.EditValue = ""
        Me.txtJBJobState.Location = New System.Drawing.Point(104, 200)
        Me.txtJBJobState.Name = "txtJBJobState"
        Me.txtJBJobState.Size = New System.Drawing.Size(104, 20)
        Me.txtJBJobState.TabIndex = 4
        '
        'txtJBJobPostCode
        '
        Me.txtJBJobPostCode.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBJobPostCode"))
        Me.txtJBJobPostCode.EditValue = ""
        Me.txtJBJobPostCode.Location = New System.Drawing.Point(392, 200)
        Me.txtJBJobPostCode.Name = "txtJBJobPostCode"
        Me.txtJBJobPostCode.Size = New System.Drawing.Size(80, 20)
        Me.txtJBJobPostCode.TabIndex = 5
        '
        'pnlClientInformation
        '
        Me.pnlClientInformation.Controls.Add(Me.Label5)
        Me.pnlClientInformation.Controls.Add(Me.Label45)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientEmail)
        Me.pnlClientInformation.Controls.Add(Me.Label13)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientFirstName)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientSurname)
        Me.pnlClientInformation.Controls.Add(Me.GroupLine17)
        Me.pnlClientInformation.Controls.Add(Me.Label1)
        Me.pnlClientInformation.Controls.Add(Me.Label2)
        Me.pnlClientInformation.Controls.Add(Me.Label4)
        Me.pnlClientInformation.Controls.Add(Me.Label6)
        Me.pnlClientInformation.Controls.Add(Me.Label3)
        Me.pnlClientInformation.Controls.Add(Me.Label8)
        Me.pnlClientInformation.Controls.Add(Me.Label9)
        Me.pnlClientInformation.Controls.Add(Me.Label10)
        Me.pnlClientInformation.Controls.Add(Me.GroupLine18)
        Me.pnlClientInformation.Controls.Add(Me.GroupLine19)
        Me.pnlClientInformation.Controls.Add(Me.Label30)
        Me.pnlClientInformation.Controls.Add(Me.txtJBReferenceName)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientStreetAddress01)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientStreetAddress02)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientSuburb)
        Me.pnlClientInformation.Controls.Add(Me.JBClientState)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientPostCode)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientPhoneNumber)
        Me.pnlClientInformation.Controls.Add(Me.txtJBClientFaxNumber)
        Me.pnlClientInformation.Controls.Add(Me.txtBClientMobileNumber)
        Me.pnlClientInformation.Location = New System.Drawing.Point(120, 8)
        Me.pnlClientInformation.Name = "pnlClientInformation"
        Me.pnlClientInformation.Size = New System.Drawing.Size(496, 472)
        Me.pnlClientInformation.TabIndex = 0
        Me.pnlClientInformation.Text = "Customer Information"
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(296, 280)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(96, 21)
        Me.Label5.TabIndex = 23
        Me.Label5.Text = "ZIP/postal code:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label45
        '
        Me.Label45.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.Location = New System.Drawing.Point(40, 440)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(80, 21)
        Me.Label45.TabIndex = 8
        Me.Label45.Text = "Email:"
        Me.Label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJBClientEmail
        '
        Me.txtJBClientEmail.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBClientEmail"))
        Me.txtJBClientEmail.EditValue = ""
        Me.txtJBClientEmail.Location = New System.Drawing.Point(184, 440)
        Me.txtJBClientEmail.Name = "txtJBClientEmail"
        Me.txtJBClientEmail.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientEmail.TabIndex = 11
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label13.Location = New System.Drawing.Point(480, 56)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(24, 16)
        Me.Label13.TabIndex = 7
        Me.Label13.Text = "*"
        '
        'txtJBClientFirstName
        '
        Me.txtJBClientFirstName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBClientFirstName"))
        Me.txtJBClientFirstName.EditValue = ""
        Me.txtJBClientFirstName.Location = New System.Drawing.Point(184, 88)
        Me.txtJBClientFirstName.Name = "txtJBClientFirstName"
        Me.txtJBClientFirstName.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientFirstName.TabIndex = 1
        '
        'txtJBClientSurname
        '
        Me.txtJBClientSurname.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBClientSurname"))
        Me.txtJBClientSurname.EditValue = ""
        Me.txtJBClientSurname.Location = New System.Drawing.Point(184, 56)
        Me.txtJBClientSurname.Name = "txtJBClientSurname"
        '
        'txtJBClientSurname.Properties
        '
        Me.txtJBClientSurname.Properties.NullText = "<Incomplete>"
        Me.txtJBClientSurname.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientSurname.TabIndex = 0
        '
        'GroupLine17
        '
        Me.GroupLine17.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupLine17.Location = New System.Drawing.Point(16, 160)
        Me.GroupLine17.Name = "GroupLine17"
        Me.GroupLine17.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine17.TabIndex = 3
        Me.GroupLine17.TextString = "Address"
        Me.GroupLine17.TextWidth = 50
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(40, 88)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 21)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "First name:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(40, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(144, 21)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Surname / business name:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(40, 280)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 21)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "State/region:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(40, 184)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 21)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Street address:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(40, 248)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 21)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Suburb/town:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(40, 344)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(80, 21)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Phone:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(40, 376)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(80, 21)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "Fax:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(40, 408)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(80, 21)
        Me.Label10.TabIndex = 1
        Me.Label10.Text = "Mobile:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine18
        '
        Me.GroupLine18.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupLine18.Location = New System.Drawing.Point(16, 320)
        Me.GroupLine18.Name = "GroupLine18"
        Me.GroupLine18.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine18.TabIndex = 3
        Me.GroupLine18.TextString = "Contact Details"
        Me.GroupLine18.TextWidth = 79
        '
        'GroupLine19
        '
        Me.GroupLine19.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupLine19.Location = New System.Drawing.Point(16, 32)
        Me.GroupLine19.Name = "GroupLine19"
        Me.GroupLine19.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine19.TabIndex = 3
        Me.GroupLine19.TextString = "Name"
        Me.GroupLine19.TextWidth = 35
        '
        'Label30
        '
        Me.Label30.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(40, 120)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(96, 21)
        Me.Label30.TabIndex = 1
        Me.Label30.Text = "Reference name:"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJBReferenceName
        '
        Me.txtJBReferenceName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBReferenceName"))
        Me.txtJBReferenceName.EditValue = ""
        Me.txtJBReferenceName.Location = New System.Drawing.Point(184, 120)
        Me.txtJBReferenceName.Name = "txtJBReferenceName"
        Me.txtJBReferenceName.Size = New System.Drawing.Size(296, 20)
        Me.txtJBReferenceName.TabIndex = 2
        '
        'txtJBClientStreetAddress01
        '
        Me.txtJBClientStreetAddress01.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBClientStreetAddress01"))
        Me.txtJBClientStreetAddress01.EditValue = ""
        Me.txtJBClientStreetAddress01.Location = New System.Drawing.Point(184, 184)
        Me.txtJBClientStreetAddress01.Name = "txtJBClientStreetAddress01"
        Me.txtJBClientStreetAddress01.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientStreetAddress01.TabIndex = 3
        '
        'txtJBClientStreetAddress02
        '
        Me.txtJBClientStreetAddress02.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBClientStreetAddress02"))
        Me.txtJBClientStreetAddress02.EditValue = ""
        Me.txtJBClientStreetAddress02.Location = New System.Drawing.Point(184, 216)
        Me.txtJBClientStreetAddress02.Name = "txtJBClientStreetAddress02"
        Me.txtJBClientStreetAddress02.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientStreetAddress02.TabIndex = 4
        '
        'txtJBClientSuburb
        '
        Me.txtJBClientSuburb.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBClientSuburb"))
        Me.txtJBClientSuburb.EditValue = ""
        Me.txtJBClientSuburb.Location = New System.Drawing.Point(184, 248)
        Me.txtJBClientSuburb.Name = "txtJBClientSuburb"
        Me.txtJBClientSuburb.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientSuburb.TabIndex = 5
        '
        'JBClientState
        '
        Me.JBClientState.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBClientState"))
        Me.JBClientState.EditValue = ""
        Me.JBClientState.Location = New System.Drawing.Point(184, 280)
        Me.JBClientState.Name = "JBClientState"
        Me.JBClientState.Size = New System.Drawing.Size(88, 20)
        Me.JBClientState.TabIndex = 6
        '
        'txtJBClientPostCode
        '
        Me.txtJBClientPostCode.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBClientPostCode"))
        Me.txtJBClientPostCode.EditValue = ""
        Me.txtJBClientPostCode.Location = New System.Drawing.Point(392, 280)
        Me.txtJBClientPostCode.Name = "txtJBClientPostCode"
        Me.txtJBClientPostCode.Size = New System.Drawing.Size(88, 20)
        Me.txtJBClientPostCode.TabIndex = 7
        '
        'txtJBClientPhoneNumber
        '
        Me.txtJBClientPhoneNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBClientPhoneNumber"))
        Me.txtJBClientPhoneNumber.EditValue = ""
        Me.txtJBClientPhoneNumber.Location = New System.Drawing.Point(184, 344)
        Me.txtJBClientPhoneNumber.Name = "txtJBClientPhoneNumber"
        Me.txtJBClientPhoneNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientPhoneNumber.TabIndex = 8
        '
        'txtJBClientFaxNumber
        '
        Me.txtJBClientFaxNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBClientFaxNumber"))
        Me.txtJBClientFaxNumber.EditValue = ""
        Me.txtJBClientFaxNumber.Location = New System.Drawing.Point(184, 376)
        Me.txtJBClientFaxNumber.Name = "txtJBClientFaxNumber"
        Me.txtJBClientFaxNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtJBClientFaxNumber.TabIndex = 9
        '
        'txtBClientMobileNumber
        '
        Me.txtBClientMobileNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBClientMobileNumber"))
        Me.txtBClientMobileNumber.EditValue = ""
        Me.txtBClientMobileNumber.Location = New System.Drawing.Point(184, 408)
        Me.txtBClientMobileNumber.Name = "txtBClientMobileNumber"
        Me.txtBClientMobileNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtBClientMobileNumber.TabIndex = 10
        '
        'pnlAppliances
        '
        Me.pnlAppliances.Controls.Add(Me.btnRemoveAppliance)
        Me.pnlAppliances.Controls.Add(Me.btnEditAppliance)
        Me.pnlAppliances.Controls.Add(Me.btnAddAppliance)
        Me.pnlAppliances.Controls.Add(Me.dgAppliances)
        Me.pnlAppliances.Location = New System.Drawing.Point(32, 64)
        Me.pnlAppliances.Name = "pnlAppliances"
        Me.pnlAppliances.Size = New System.Drawing.Size(576, 400)
        Me.pnlAppliances.TabIndex = 8
        Me.pnlAppliances.Text = "Appliances"
        '
        'btnRemoveAppliance
        '
        Me.btnRemoveAppliance.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveAppliance.Image = CType(resources.GetObject("btnRemoveAppliance.Image"), System.Drawing.Image)
        Me.btnRemoveAppliance.Location = New System.Drawing.Point(176, 368)
        Me.btnRemoveAppliance.Name = "btnRemoveAppliance"
        Me.btnRemoveAppliance.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveAppliance.TabIndex = 3
        Me.btnRemoveAppliance.Text = "Remove"
        '
        'btnEditAppliance
        '
        Me.btnEditAppliance.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEditAppliance.Image = CType(resources.GetObject("btnEditAppliance.Image"), System.Drawing.Image)
        Me.btnEditAppliance.Location = New System.Drawing.Point(96, 368)
        Me.btnEditAppliance.Name = "btnEditAppliance"
        Me.btnEditAppliance.Size = New System.Drawing.Size(72, 23)
        Me.btnEditAppliance.TabIndex = 2
        Me.btnEditAppliance.Text = "Edit..."
        '
        'btnAddAppliance
        '
        Me.btnAddAppliance.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddAppliance.Image = CType(resources.GetObject("btnAddAppliance.Image"), System.Drawing.Image)
        Me.btnAddAppliance.Location = New System.Drawing.Point(16, 368)
        Me.btnAddAppliance.Name = "btnAddAppliance"
        Me.btnAddAppliance.Size = New System.Drawing.Size(72, 23)
        Me.btnAddAppliance.TabIndex = 1
        Me.btnAddAppliance.Text = "Add..."
        '
        'dgAppliances
        '
        Me.dgAppliances.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgAppliances.DataSource = Me.dvAppliances
        '
        'dgAppliances.EmbeddedNavigator
        '
        Me.dgAppliances.EmbeddedNavigator.Name = ""
        Me.dgAppliances.Location = New System.Drawing.Point(16, 32)
        Me.dgAppliances.MainView = Me.gvAppliances
        Me.dgAppliances.Name = "dgAppliances"
        Me.dgAppliances.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtOrderedOrPaidBy})
        Me.dgAppliances.Size = New System.Drawing.Size(544, 328)
        Me.dgAppliances.TabIndex = 0
        Me.dgAppliances.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvAppliances, Me.GridView2})
        '
        'dvAppliances
        '
        Me.dvAppliances.RowFilter = "NIType = 'AP'"
        Me.dvAppliances.Table = Me.DsGTMS.VJobsNonCoreSalesItems
        '
        'gvAppliances
        '
        Me.gvAppliances.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.gvAppliances.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvAppliances.Appearance.FocusedCell.Options.UseBackColor = True
        Me.gvAppliances.Appearance.FocusedCell.Options.UseForeColor = True
        Me.gvAppliances.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.gvAppliances.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gvAppliances.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvAppliances.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvAppliances.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colNIUserType1, Me.colNIBrand, Me.colNIName2, Me.colNIOrderedBy, Me.colNIPaidBy})
        Me.gvAppliances.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.gvAppliances.GridControl = Me.dgAppliances
        Me.gvAppliances.Name = "gvAppliances"
        Me.gvAppliances.OptionsBehavior.Editable = False
        Me.gvAppliances.OptionsCustomization.AllowFilter = False
        Me.gvAppliances.OptionsNavigation.AutoFocusNewRow = True
        Me.gvAppliances.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.gvAppliances.OptionsView.ShowGroupPanel = False
        Me.gvAppliances.OptionsView.ShowHorzLines = False
        Me.gvAppliances.OptionsView.ShowIndicator = False
        Me.gvAppliances.OptionsView.ShowVertLines = False
        Me.gvAppliances.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colNIUserType1, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colNIUserType1
        '
        Me.colNIUserType1.Caption = "Appliance Type"
        Me.colNIUserType1.FieldName = "NIUserType"
        Me.colNIUserType1.Name = "colNIUserType1"
        Me.colNIUserType1.Visible = True
        Me.colNIUserType1.VisibleIndex = 0
        Me.colNIUserType1.Width = 176
        '
        'colNIBrand
        '
        Me.colNIBrand.Caption = "Brand"
        Me.colNIBrand.FieldName = "NIBrand"
        Me.colNIBrand.Name = "colNIBrand"
        Me.colNIBrand.Visible = True
        Me.colNIBrand.VisibleIndex = 1
        Me.colNIBrand.Width = 166
        '
        'colNIName2
        '
        Me.colNIName2.Caption = "Model Number"
        Me.colNIName2.FieldName = "NIName"
        Me.colNIName2.Name = "colNIName2"
        Me.colNIName2.Visible = True
        Me.colNIName2.VisibleIndex = 2
        Me.colNIName2.Width = 162
        '
        'colNIOrderedBy
        '
        Me.colNIOrderedBy.Caption = "Ordered By"
        Me.colNIOrderedBy.ColumnEdit = Me.txtOrderedOrPaidBy
        Me.colNIOrderedBy.FieldName = "NIOrderedBy"
        Me.colNIOrderedBy.Name = "colNIOrderedBy"
        Me.colNIOrderedBy.Visible = True
        Me.colNIOrderedBy.VisibleIndex = 3
        Me.colNIOrderedBy.Width = 168
        '
        'txtOrderedOrPaidBy
        '
        Me.txtOrderedOrPaidBy.AutoHeight = False
        Me.txtOrderedOrPaidBy.Items.AddRange(New Object() {"Granite Transformations", "Customer"})
        Me.txtOrderedOrPaidBy.Name = "txtOrderedOrPaidBy"
        '
        'colNIPaidBy
        '
        Me.colNIPaidBy.Caption = "Paid By"
        Me.colNIPaidBy.ColumnEdit = Me.txtOrderedOrPaidBy
        Me.colNIPaidBy.FieldName = "NIPaidBy"
        Me.colNIPaidBy.Name = "colNIPaidBy"
        Me.colNIPaidBy.Visible = True
        Me.colNIPaidBy.VisibleIndex = 4
        Me.colNIPaidBy.Width = 171
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.dgAppliances
        Me.GridView2.Name = "GridView2"
        '
        'pnlGranite
        '
        Me.pnlGranite.Controls.Add(Me.Label58)
        Me.pnlGranite.Controls.Add(Me.Label55)
        Me.pnlGranite.Controls.Add(Me.txtJBInventoryDate)
        Me.pnlGranite.Controls.Add(Me.dgStockedItems)
        Me.pnlGranite.Controls.Add(Me.btnRemoveGranite)
        Me.pnlGranite.Controls.Add(Me.btnAddGranite)
        Me.pnlGranite.Controls.Add(Me.dgGranite)
        Me.pnlGranite.Controls.Add(Me.tvGranite)
        Me.pnlGranite.Controls.Add(Me.Label24)
        Me.pnlGranite.Controls.Add(Me.Label25)
        Me.pnlGranite.Controls.Add(Me.Label37)
        Me.pnlGranite.Location = New System.Drawing.Point(16, 40)
        Me.pnlGranite.Name = "pnlGranite"
        Me.pnlGranite.Size = New System.Drawing.Size(608, 408)
        Me.pnlGranite.TabIndex = 4
        Me.pnlGranite.Text = "Tracked Material"
        '
        'Label58
        '
        Me.Label58.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label58.Location = New System.Drawing.Point(288, 328)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(304, 32)
        Me.Label58.TabIndex = 20
        Me.Label58.Text = "The inventory date is the date the sheets came out of inventory. This is typicall" & _
        "y the date the first sheet was cut into."
        '
        'Label55
        '
        Me.Label55.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label55.Location = New System.Drawing.Point(288, 368)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(88, 21)
        Me.Label55.TabIndex = 19
        Me.Label55.Text = "Inventory date:"
        Me.Label55.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtJBInventoryDate
        '
        Me.txtJBInventoryDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtJBInventoryDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBInventoryDate"))
        Me.txtJBInventoryDate.EditValue = New Date(2006, 3, 24, 0, 0, 0, 0)
        Me.txtJBInventoryDate.Location = New System.Drawing.Point(408, 368)
        Me.txtJBInventoryDate.Name = "txtJBInventoryDate"
        '
        'txtJBInventoryDate.Properties
        '
        Me.txtJBInventoryDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtJBInventoryDate.Properties.NullText = "<Incomplete>"
        Me.txtJBInventoryDate.Size = New System.Drawing.Size(184, 20)
        Me.txtJBInventoryDate.TabIndex = 5
        '
        'dgStockedItems
        '
        Me.dgStockedItems.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgStockedItems.DataSource = Me.DsGTMS.VJobs_StockedItems
        '
        'dgStockedItems.EmbeddedNavigator
        '
        Me.dgStockedItems.EmbeddedNavigator.Name = ""
        Me.dgStockedItems.Location = New System.Drawing.Point(288, 160)
        Me.dgStockedItems.MainView = Me.gvStockedItems
        Me.dgStockedItems.Name = "dgStockedItems"
        Me.dgStockedItems.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtNumericStockedItems})
        Me.dgStockedItems.Size = New System.Drawing.Size(304, 152)
        Me.dgStockedItems.TabIndex = 4
        Me.dgStockedItems.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvStockedItems})
        '
        'gvStockedItems
        '
        Me.gvStockedItems.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvStockedItems.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvStockedItems.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvStockedItems.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvStockedItems.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvStockedItems.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvStockedItems.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvStockedItems.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvStockedItems.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvStockedItems.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvStockedItems.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvStockedItems.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvStockedItems.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvStockedItems.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvStockedItems.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvStockedItems.Appearance.VertLine.Options.UseBackColor = True
        Me.gvStockedItems.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSIName, Me.colJSInventoryAmount, Me.colSIInventoryUOMShortName})
        Me.gvStockedItems.GridControl = Me.dgStockedItems
        Me.gvStockedItems.Name = "gvStockedItems"
        Me.gvStockedItems.OptionsCustomization.AllowFilter = False
        Me.gvStockedItems.OptionsNavigation.AutoFocusNewRow = True
        Me.gvStockedItems.OptionsView.ShowGroupPanel = False
        Me.gvStockedItems.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colSIName, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colSIName
        '
        Me.colSIName.Caption = "Stocked Item"
        Me.colSIName.FieldName = "SIName"
        Me.colSIName.Name = "colSIName"
        Me.colSIName.OptionsColumn.AllowEdit = False
        Me.colSIName.OptionsColumn.AllowFocus = False
        Me.colSIName.Visible = True
        Me.colSIName.VisibleIndex = 0
        Me.colSIName.Width = 158
        '
        'colJSInventoryAmount
        '
        Me.colJSInventoryAmount.Caption = "Amount *"
        Me.colJSInventoryAmount.ColumnEdit = Me.txtNumericStockedItems
        Me.colJSInventoryAmount.FieldName = "JSInventoryAmount"
        Me.colJSInventoryAmount.Name = "colJSInventoryAmount"
        Me.colJSInventoryAmount.Visible = True
        Me.colJSInventoryAmount.VisibleIndex = 1
        Me.colJSInventoryAmount.Width = 83
        '
        'txtNumericStockedItems
        '
        Me.txtNumericStockedItems.AutoHeight = False
        Me.txtNumericStockedItems.DisplayFormat.FormatString = "#0"
        Me.txtNumericStockedItems.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtNumericStockedItems.EditFormat.FormatString = "#0"
        Me.txtNumericStockedItems.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtNumericStockedItems.Name = "txtNumericStockedItems"
        Me.txtNumericStockedItems.NullText = "<Incomplete>"
        '
        'colSIInventoryUOMShortName
        '
        Me.colSIInventoryUOMShortName.FieldName = "SIInventoryUOMShortName"
        Me.colSIInventoryUOMShortName.Name = "colSIInventoryUOMShortName"
        Me.colSIInventoryUOMShortName.OptionsColumn.AllowEdit = False
        Me.colSIInventoryUOMShortName.OptionsColumn.AllowFocus = False
        Me.colSIInventoryUOMShortName.Visible = True
        Me.colSIInventoryUOMShortName.VisibleIndex = 2
        Me.colSIInventoryUOMShortName.Width = 57
        '
        'btnRemoveGranite
        '
        Me.btnRemoveGranite.Image = CType(resources.GetObject("btnRemoveGranite.Image"), System.Drawing.Image)
        Me.btnRemoveGranite.Location = New System.Drawing.Point(208, 96)
        Me.btnRemoveGranite.Name = "btnRemoveGranite"
        Me.btnRemoveGranite.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveGranite.TabIndex = 2
        Me.btnRemoveGranite.Text = "Remove"
        '
        'btnAddGranite
        '
        Me.btnAddGranite.Image = CType(resources.GetObject("btnAddGranite.Image"), System.Drawing.Image)
        Me.btnAddGranite.ImageAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnAddGranite.Location = New System.Drawing.Point(208, 64)
        Me.btnAddGranite.Name = "btnAddGranite"
        Me.btnAddGranite.Size = New System.Drawing.Size(72, 23)
        Me.btnAddGranite.TabIndex = 1
        Me.btnAddGranite.Text = "Add"
        '
        'dgGranite
        '
        Me.dgGranite.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgGranite.DataSource = Me.dvJobs_Granite
        '
        'dgGranite.EmbeddedNavigator
        '
        Me.dgGranite.EmbeddedNavigator.Name = ""
        Me.dgGranite.Location = New System.Drawing.Point(288, 48)
        Me.dgGranite.MainView = Me.gvGranite
        Me.dgGranite.Name = "dgGranite"
        Me.dgGranite.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtNumericGranite})
        Me.dgGranite.Size = New System.Drawing.Size(304, 80)
        Me.dgGranite.TabIndex = 3
        Me.dgGranite.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvGranite})
        '
        'dvJobs_Granite
        '
        Me.dvJobs_Granite.AllowNew = False
        Me.dvJobs_Granite.RowFilter = "MTStocked = 1"
        Me.dvJobs_Granite.Table = Me.DsGTMS.VJobs_Materials
        '
        'gvGranite
        '
        Me.gvGranite.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvGranite.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvGranite.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvGranite.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvGranite.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvGranite.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvGranite.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvGranite.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvGranite.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvGranite.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvGranite.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvGranite.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvGranite.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvGranite.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvGranite.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvGranite.Appearance.VertLine.Options.UseBackColor = True
        Me.gvGranite.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn4, Me.GridColumn5, Me.colMTPrimaryUOMShortName})
        Me.gvGranite.GridControl = Me.dgGranite
        Me.gvGranite.Name = "gvGranite"
        Me.gvGranite.OptionsCustomization.AllowFilter = False
        Me.gvGranite.OptionsNavigation.AutoFocusNewRow = True
        Me.gvGranite.OptionsView.ShowGroupPanel = False
        Me.gvGranite.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn4, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Tracked Material"
        Me.GridColumn4.DisplayFormat.FormatString = "c"
        Me.GridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn4.FieldName = "MTName"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.OptionsColumn.AllowEdit = False
        Me.GridColumn4.OptionsColumn.AllowFocus = False
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 0
        Me.GridColumn4.Width = 153
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Amount *"
        Me.GridColumn5.ColumnEdit = Me.txtNumericGranite
        Me.GridColumn5.FieldName = "JMPrimarySold"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 1
        Me.GridColumn5.Width = 80
        '
        'txtNumericGranite
        '
        Me.txtNumericGranite.AutoHeight = False
        Me.txtNumericGranite.DisplayFormat.FormatString = "#0.000"
        Me.txtNumericGranite.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtNumericGranite.EditFormat.FormatString = "#0.000"
        Me.txtNumericGranite.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtNumericGranite.Name = "txtNumericGranite"
        Me.txtNumericGranite.NullText = "<Incomplete>"
        '
        'colMTPrimaryUOMShortName
        '
        Me.colMTPrimaryUOMShortName.FieldName = "MTPrimaryUOMShortName"
        Me.colMTPrimaryUOMShortName.Name = "colMTPrimaryUOMShortName"
        Me.colMTPrimaryUOMShortName.OptionsColumn.AllowEdit = False
        Me.colMTPrimaryUOMShortName.OptionsColumn.AllowFocus = False
        Me.colMTPrimaryUOMShortName.Visible = True
        Me.colMTPrimaryUOMShortName.VisibleIndex = 2
        Me.colMTPrimaryUOMShortName.Width = 57
        '
        'tvGranite
        '
        Me.tvGranite.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.tvGranite.Connection = Me.SqlConnection
        Me.tvGranite.HideSelection = False
        Me.tvGranite.ImageIndex = -1
        Me.tvGranite.Location = New System.Drawing.Point(16, 48)
        Me.tvGranite.Name = "tvGranite"
        Me.tvGranite.SelectedImageIndex = -1
        Me.tvGranite.Size = New System.Drawing.Size(184, 344)
        Me.tvGranite.TabIndex = 0
        '
        'Label24
        '
        Me.Label24.Location = New System.Drawing.Point(16, 24)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(176, 21)
        Me.Label24.TabIndex = 8
        Me.Label24.Text = "Available tracked material:"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label25
        '
        Me.Label25.Location = New System.Drawing.Point(288, 24)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(232, 21)
        Me.Label25.TabIndex = 7
        Me.Label25.Text = "Amount of tracked material sold in job:"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label37
        '
        Me.Label37.Location = New System.Drawing.Point(288, 136)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(184, 21)
        Me.Label37.TabIndex = 11
        Me.Label37.Text = "Number of full sheets cut for job:"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlSalesReps
        '
        Me.pnlSalesReps.Controls.Add(Me.btnRemoveSalesRep)
        Me.pnlSalesReps.Controls.Add(Me.btnAddSalesRep)
        Me.pnlSalesReps.Controls.Add(Me.dgSalesReps)
        Me.pnlSalesReps.Controls.Add(Me.tvSalesReps)
        Me.pnlSalesReps.Controls.Add(Me.Label28)
        Me.pnlSalesReps.Controls.Add(Me.Label29)
        Me.pnlSalesReps.Location = New System.Drawing.Point(48, 120)
        Me.pnlSalesReps.Name = "pnlSalesReps"
        Me.pnlSalesReps.Size = New System.Drawing.Size(584, 344)
        Me.pnlSalesReps.TabIndex = 6
        Me.pnlSalesReps.Text = "Specified Salespeople"
        '
        'btnRemoveSalesRep
        '
        Me.btnRemoveSalesRep.Image = CType(resources.GetObject("btnRemoveSalesRep.Image"), System.Drawing.Image)
        Me.btnRemoveSalesRep.Location = New System.Drawing.Point(208, 184)
        Me.btnRemoveSalesRep.Name = "btnRemoveSalesRep"
        Me.btnRemoveSalesRep.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveSalesRep.TabIndex = 2
        Me.btnRemoveSalesRep.Text = "Remove"
        '
        'btnAddSalesRep
        '
        Me.btnAddSalesRep.Image = CType(resources.GetObject("btnAddSalesRep.Image"), System.Drawing.Image)
        Me.btnAddSalesRep.ImageAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnAddSalesRep.Location = New System.Drawing.Point(208, 152)
        Me.btnAddSalesRep.Name = "btnAddSalesRep"
        Me.btnAddSalesRep.Size = New System.Drawing.Size(72, 23)
        Me.btnAddSalesRep.TabIndex = 1
        Me.btnAddSalesRep.Text = "Add"
        '
        'dgSalesReps
        '
        Me.dgSalesReps.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgSalesReps.DataSource = Me.dvJobs_SalesReps
        '
        'dgSalesReps.EmbeddedNavigator
        '
        Me.dgSalesReps.EmbeddedNavigator.Name = ""
        Me.dgSalesReps.Location = New System.Drawing.Point(288, 48)
        Me.dgSalesReps.MainView = Me.gvSalesReps
        Me.dgSalesReps.Name = "dgSalesReps"
        Me.dgSalesReps.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtDollarSalesRep, Me.txtPercentageSalesReps, Me.rgPayAsSalesReps})
        Me.dgSalesReps.Size = New System.Drawing.Size(280, 280)
        Me.dgSalesReps.TabIndex = 3
        Me.dgSalesReps.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvSalesReps})
        '
        'dvJobs_SalesReps
        '
        Me.dvJobs_SalesReps.AllowNew = False
        Me.dvJobs_SalesReps.RowFilter = "EGType = 'RC'"
        Me.dvJobs_SalesReps.Table = Me.DsGTMS.VJobs_Expenses
        '
        'gvSalesReps
        '
        Me.gvSalesReps.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvSalesReps.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvSalesReps.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvSalesReps.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvSalesReps.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvSalesReps.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvSalesReps.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvSalesReps.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvSalesReps.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvSalesReps.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvSalesReps.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvSalesReps.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvSalesReps.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvSalesReps.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvSalesReps.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvSalesReps.Appearance.VertLine.Options.UseBackColor = True
        Me.gvSalesReps.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn7, Me.GridColumn8, Me.GridColumn9, Me.GridColumn10})
        Me.gvSalesReps.CustomizationFormBounds = New System.Drawing.Rectangle(592, 331, 208, 156)
        Me.gvSalesReps.GridControl = Me.dgSalesReps
        Me.gvSalesReps.Name = "gvSalesReps"
        Me.gvSalesReps.OptionsCustomization.AllowFilter = False
        Me.gvSalesReps.OptionsNavigation.AutoFocusNewRow = True
        Me.gvSalesReps.OptionsView.ShowGroupPanel = False
        Me.gvSalesReps.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn7, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Name"
        Me.GridColumn7.FieldName = "EXName"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.OptionsColumn.AllowEdit = False
        Me.GridColumn7.OptionsColumn.AllowFocus = False
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 0
        Me.GridColumn7.Width = 86
        '
        'GridColumn8
        '
        Me.GridColumn8.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn8.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.GridColumn8.Caption = "Pay As..."
        Me.GridColumn8.ColumnEdit = Me.rgPayAsSalesReps
        Me.GridColumn8.FieldName = "JEAsPercent"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 1
        Me.GridColumn8.Width = 124
        '
        'rgPayAsSalesReps
        '
        Me.rgPayAsSalesReps.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(0, "$"), New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "%"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Both")})
        Me.rgPayAsSalesReps.Name = "rgPayAsSalesReps"
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "$ *"
        Me.GridColumn9.ColumnEdit = Me.txtDollarSalesRep
        Me.GridColumn9.FieldName = "JECharge"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 2
        Me.GridColumn9.Width = 57
        '
        'txtDollarSalesRep
        '
        Me.txtDollarSalesRep.AutoHeight = False
        Me.txtDollarSalesRep.DisplayFormat.FormatString = "c"
        Me.txtDollarSalesRep.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtDollarSalesRep.EditFormat.FormatString = "c"
        Me.txtDollarSalesRep.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtDollarSalesRep.Name = "txtDollarSalesRep"
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "% *"
        Me.GridColumn10.ColumnEdit = Me.txtPercentageSalesReps
        Me.GridColumn10.FieldName = "JEPercent"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 3
        Me.GridColumn10.Width = 55
        '
        'txtPercentageSalesReps
        '
        Me.txtPercentageSalesReps.AutoHeight = False
        Me.txtPercentageSalesReps.DisplayFormat.FormatString = "P"
        Me.txtPercentageSalesReps.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtPercentageSalesReps.EditFormat.FormatString = "p"
        Me.txtPercentageSalesReps.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtPercentageSalesReps.Name = "txtPercentageSalesReps"
        '
        'tvSalesReps
        '
        Me.tvSalesReps.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.tvSalesReps.Connection = Me.SqlConnection
        Me.tvSalesReps.HideSelection = False
        Me.tvSalesReps.ImageIndex = -1
        Me.tvSalesReps.Location = New System.Drawing.Point(16, 48)
        Me.tvSalesReps.Name = "tvSalesReps"
        Me.tvSalesReps.SelectedImageIndex = -1
        Me.tvSalesReps.Size = New System.Drawing.Size(184, 280)
        Me.tvSalesReps.TabIndex = 0
        '
        'Label28
        '
        Me.Label28.Location = New System.Drawing.Point(16, 24)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(120, 21)
        Me.Label28.TabIndex = 14
        Me.Label28.Text = "Available salespeople:"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label29
        '
        Me.Label29.Location = New System.Drawing.Point(288, 24)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(120, 21)
        Me.Label29.TabIndex = 13
        Me.Label29.Text = "Salespeople in job:"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlResponsibilities
        '
        Me.pnlResponsibilities.Controls.Add(Me.dgResponsibilities)
        Me.pnlResponsibilities.Location = New System.Drawing.Point(32, 176)
        Me.pnlResponsibilities.Name = "pnlResponsibilities"
        Me.pnlResponsibilities.Size = New System.Drawing.Size(584, 216)
        Me.pnlResponsibilities.TabIndex = 13
        Me.pnlResponsibilities.Text = "Responsibilities"
        '
        'dgResponsibilities
        '
        Me.dgResponsibilities.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgResponsibilities.DataSource = Me.DsGTMS.VJobs_Expenses
        '
        'dgResponsibilities.EmbeddedNavigator
        '
        Me.dgResponsibilities.EmbeddedNavigator.Name = ""
        Me.dgResponsibilities.Location = New System.Drawing.Point(16, 32)
        Me.dgResponsibilities.MainView = Me.gvResponsibilities
        Me.dgResponsibilities.Name = "dgResponsibilities"
        Me.dgResponsibilities.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtPercentOfJob})
        Me.dgResponsibilities.Size = New System.Drawing.Size(552, 168)
        Me.dgResponsibilities.TabIndex = 21
        Me.dgResponsibilities.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvResponsibilities})
        '
        'gvResponsibilities
        '
        Me.gvResponsibilities.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvResponsibilities.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvResponsibilities.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvResponsibilities.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvResponsibilities.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvResponsibilities.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvResponsibilities.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvResponsibilities.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvResponsibilities.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvResponsibilities.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvResponsibilities.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvResponsibilities.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvResponsibilities.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvResponsibilities.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvResponsibilities.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvResponsibilities.Appearance.VertLine.Options.UseBackColor = True
        Me.gvResponsibilities.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.BandedGridColumn7, Me.BandedGridColumn12, Me.BandedGridColumn8})
        Me.gvResponsibilities.CustomizationFormBounds = New System.Drawing.Rectangle(592, 331, 208, 156)
        Me.gvResponsibilities.GridControl = Me.dgResponsibilities
        Me.gvResponsibilities.GroupCount = 1
        Me.gvResponsibilities.GroupSummary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "JEPercentageOfJob", Nothing, "(Total = {0:p})")})
        Me.gvResponsibilities.Name = "gvResponsibilities"
        Me.gvResponsibilities.OptionsCustomization.AllowFilter = False
        Me.gvResponsibilities.OptionsNavigation.AutoFocusNewRow = True
        Me.gvResponsibilities.OptionsView.ShowGroupPanel = False
        Me.gvResponsibilities.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.BandedGridColumn8, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.BandedGridColumn7, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'BandedGridColumn7
        '
        Me.BandedGridColumn7.Caption = "Name"
        Me.BandedGridColumn7.FieldName = "EXName"
        Me.BandedGridColumn7.Name = "BandedGridColumn7"
        Me.BandedGridColumn7.OptionsColumn.AllowEdit = False
        Me.BandedGridColumn7.OptionsColumn.AllowFocus = False
        Me.BandedGridColumn7.Visible = True
        Me.BandedGridColumn7.VisibleIndex = 0
        Me.BandedGridColumn7.Width = 490
        '
        'BandedGridColumn12
        '
        Me.BandedGridColumn12.Caption = "Percent of Job *"
        Me.BandedGridColumn12.ColumnEdit = Me.txtPercentOfJob
        Me.BandedGridColumn12.FieldName = "JEPercentageOfJob"
        Me.BandedGridColumn12.Name = "BandedGridColumn12"
        Me.BandedGridColumn12.Visible = True
        Me.BandedGridColumn12.VisibleIndex = 1
        Me.BandedGridColumn12.Width = 349
        '
        'txtPercentOfJob
        '
        Me.txtPercentOfJob.AutoHeight = False
        Me.txtPercentOfJob.DisplayFormat.FormatString = "p"
        Me.txtPercentOfJob.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtPercentOfJob.EditFormat.FormatString = "p"
        Me.txtPercentOfJob.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtPercentOfJob.Name = "txtPercentOfJob"
        Me.txtPercentOfJob.NullText = "<Incomplete>"
        '
        'BandedGridColumn8
        '
        Me.BandedGridColumn8.Caption = "Group"
        Me.BandedGridColumn8.FieldName = "EGName"
        Me.BandedGridColumn8.Name = "BandedGridColumn8"
        Me.BandedGridColumn8.OptionsColumn.AllowEdit = False
        Me.BandedGridColumn8.OptionsColumn.AllowFocus = False
        Me.BandedGridColumn8.Visible = True
        Me.BandedGridColumn8.VisibleIndex = 2
        Me.BandedGridColumn8.Width = 161
        '
        'pnlOtherTrades
        '
        Me.pnlOtherTrades.Controls.Add(Me.btnRemoveOtherTrade)
        Me.pnlOtherTrades.Controls.Add(Me.btnEditOtherTrade)
        Me.pnlOtherTrades.Controls.Add(Me.btnAddOtherTrade)
        Me.pnlOtherTrades.Controls.Add(Me.dgOtherTrades)
        Me.pnlOtherTrades.Location = New System.Drawing.Point(96, 16)
        Me.pnlOtherTrades.Name = "pnlOtherTrades"
        Me.pnlOtherTrades.Size = New System.Drawing.Size(504, 336)
        Me.pnlOtherTrades.TabIndex = 10
        Me.pnlOtherTrades.Text = "Other Trades"
        '
        'btnRemoveOtherTrade
        '
        Me.btnRemoveOtherTrade.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveOtherTrade.Image = CType(resources.GetObject("btnRemoveOtherTrade.Image"), System.Drawing.Image)
        Me.btnRemoveOtherTrade.Location = New System.Drawing.Point(176, 304)
        Me.btnRemoveOtherTrade.Name = "btnRemoveOtherTrade"
        Me.btnRemoveOtherTrade.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveOtherTrade.TabIndex = 3
        Me.btnRemoveOtherTrade.Text = "Remove"
        '
        'btnEditOtherTrade
        '
        Me.btnEditOtherTrade.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEditOtherTrade.Image = CType(resources.GetObject("btnEditOtherTrade.Image"), System.Drawing.Image)
        Me.btnEditOtherTrade.Location = New System.Drawing.Point(96, 304)
        Me.btnEditOtherTrade.Name = "btnEditOtherTrade"
        Me.btnEditOtherTrade.Size = New System.Drawing.Size(72, 23)
        Me.btnEditOtherTrade.TabIndex = 2
        Me.btnEditOtherTrade.Text = "Edit..."
        '
        'btnAddOtherTrade
        '
        Me.btnAddOtherTrade.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddOtherTrade.Image = CType(resources.GetObject("btnAddOtherTrade.Image"), System.Drawing.Image)
        Me.btnAddOtherTrade.Location = New System.Drawing.Point(16, 304)
        Me.btnAddOtherTrade.Name = "btnAddOtherTrade"
        Me.btnAddOtherTrade.Size = New System.Drawing.Size(72, 23)
        Me.btnAddOtherTrade.TabIndex = 1
        Me.btnAddOtherTrade.Text = "Add..."
        '
        'dgOtherTrades
        '
        Me.dgOtherTrades.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgOtherTrades.DataSource = Me.dvOtherTrades
        '
        'dgOtherTrades.EmbeddedNavigator
        '
        Me.dgOtherTrades.EmbeddedNavigator.Name = ""
        Me.dgOtherTrades.Location = New System.Drawing.Point(16, 32)
        Me.dgOtherTrades.MainView = Me.gvOtherTrades
        Me.dgOtherTrades.Name = "dgOtherTrades"
        Me.dgOtherTrades.Size = New System.Drawing.Size(472, 264)
        Me.dgOtherTrades.TabIndex = 0
        Me.dgOtherTrades.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvOtherTrades})
        '
        'dvOtherTrades
        '
        Me.dvOtherTrades.RowFilter = "NIType = 'OT'"
        Me.dvOtherTrades.Table = Me.DsGTMS.VJobsNonCoreSalesItems
        '
        'gvOtherTrades
        '
        Me.gvOtherTrades.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.gvOtherTrades.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvOtherTrades.Appearance.FocusedCell.Options.UseBackColor = True
        Me.gvOtherTrades.Appearance.FocusedCell.Options.UseForeColor = True
        Me.gvOtherTrades.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.gvOtherTrades.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gvOtherTrades.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvOtherTrades.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvOtherTrades.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colNIName, Me.colNIUserType, Me.colNIOrderedBy1, Me.colNIPaidBy1})
        Me.gvOtherTrades.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.gvOtherTrades.GridControl = Me.dgOtherTrades
        Me.gvOtherTrades.Name = "gvOtherTrades"
        Me.gvOtherTrades.OptionsBehavior.Editable = False
        Me.gvOtherTrades.OptionsCustomization.AllowFilter = False
        Me.gvOtherTrades.OptionsNavigation.AutoFocusNewRow = True
        Me.gvOtherTrades.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.gvOtherTrades.OptionsView.ShowGroupPanel = False
        Me.gvOtherTrades.OptionsView.ShowHorzLines = False
        Me.gvOtherTrades.OptionsView.ShowIndicator = False
        Me.gvOtherTrades.OptionsView.ShowVertLines = False
        Me.gvOtherTrades.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colNIName, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colNIName
        '
        Me.colNIName.Caption = "Name"
        Me.colNIName.FieldName = "NIName"
        Me.colNIName.Name = "colNIName"
        Me.colNIName.Visible = True
        Me.colNIName.VisibleIndex = 0
        '
        'colNIUserType
        '
        Me.colNIUserType.Caption = "Trade Type"
        Me.colNIUserType.FieldName = "NIUserType"
        Me.colNIUserType.Name = "colNIUserType"
        Me.colNIUserType.Visible = True
        Me.colNIUserType.VisibleIndex = 1
        '
        'colNIOrderedBy1
        '
        Me.colNIOrderedBy1.Caption = "Ordered By"
        Me.colNIOrderedBy1.FieldName = "NIOrderedBy"
        Me.colNIOrderedBy1.Name = "colNIOrderedBy1"
        Me.colNIOrderedBy1.Visible = True
        Me.colNIOrderedBy1.VisibleIndex = 2
        '
        'colNIPaidBy1
        '
        Me.colNIPaidBy1.Caption = "Paid By"
        Me.colNIPaidBy1.FieldName = "NIPaidBy"
        Me.colNIPaidBy1.Name = "colNIPaidBy1"
        Me.colNIPaidBy1.Visible = True
        Me.colNIPaidBy1.VisibleIndex = 3
        '
        'pnlNotes
        '
        Me.pnlNotes.Controls.Add(Me.dgNotes)
        Me.pnlNotes.Location = New System.Drawing.Point(32, 40)
        Me.pnlNotes.Name = "pnlNotes"
        Me.pnlNotes.Size = New System.Drawing.Size(584, 408)
        Me.pnlNotes.TabIndex = 34
        Me.pnlNotes.Text = "Notes"
        '
        'dgNotes
        '
        Me.dgNotes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgNotes.DataSource = Me.DsGTMS.IDNotes
        '
        'dgNotes.EmbeddedNavigator
        '
        Me.dgNotes.EmbeddedNavigator.Name = ""
        Me.dgNotes.Location = New System.Drawing.Point(16, 32)
        Me.dgNotes.MainView = Me.gvNotes
        Me.dgNotes.Name = "dgNotes"
        Me.dgNotes.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtUser, Me.RepositoryItemMemoEdit1, Me.txtINDate})
        Me.dgNotes.Size = New System.Drawing.Size(552, 360)
        Me.dgNotes.TabIndex = 1
        Me.dgNotes.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvNotes})
        '
        'gvNotes
        '
        Me.gvNotes.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvNotes.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvNotes.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvNotes.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvNotes.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvNotes.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvNotes.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvNotes.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvNotes.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvNotes.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvNotes.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvNotes.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvNotes.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvNotes.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvNotes.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvNotes.Appearance.VertLine.Options.UseBackColor = True
        Me.gvNotes.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colINUser, Me.colINDate, Me.colINNotes})
        Me.gvNotes.GridControl = Me.dgNotes
        Me.gvNotes.Name = "gvNotes"
        Me.gvNotes.NewItemRowText = "Type here to add a new row"
        Me.gvNotes.OptionsCustomization.AllowFilter = False
        Me.gvNotes.OptionsCustomization.AllowRowSizing = True
        Me.gvNotes.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvNotes.OptionsView.RowAutoHeight = True
        Me.gvNotes.OptionsView.ShowGroupPanel = False
        Me.gvNotes.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colINDate, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colINUser
        '
        Me.colINUser.Caption = "User"
        Me.colINUser.ColumnEdit = Me.txtUser
        Me.colINUser.FieldName = "INUser"
        Me.colINUser.Name = "colINUser"
        Me.colINUser.Visible = True
        Me.colINUser.VisibleIndex = 1
        Me.colINUser.Width = 516
        '
        'txtUser
        '
        Me.txtUser.AutoHeight = False
        Me.txtUser.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtUser.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXName")})
        Me.txtUser.DataSource = Me.dvExpenses
        Me.txtUser.DisplayMember = "EXName"
        Me.txtUser.Name = "txtUser"
        Me.txtUser.NullText = ""
        Me.txtUser.ShowFooter = False
        Me.txtUser.ShowHeader = False
        Me.txtUser.ShowLines = False
        Me.txtUser.ValueMember = "EXName"
        '
        'dvExpenses
        '
        Me.dvExpenses.RowFilter = "EGType <> 'OE'"
        Me.dvExpenses.Sort = "EXName"
        Me.dvExpenses.Table = Me.DsGTMS.Expenses
        '
        'colINDate
        '
        Me.colINDate.Caption = "Date"
        Me.colINDate.ColumnEdit = Me.txtINDate
        Me.colINDate.FieldName = "INDate"
        Me.colINDate.Name = "colINDate"
        Me.colINDate.Visible = True
        Me.colINDate.VisibleIndex = 0
        Me.colINDate.Width = 381
        '
        'txtINDate
        '
        Me.txtINDate.AutoHeight = False
        Me.txtINDate.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtINDate.Name = "txtINDate"
        '
        'colINNotes
        '
        Me.colINNotes.Caption = "Notes"
        Me.colINNotes.ColumnEdit = Me.RepositoryItemMemoEdit1
        Me.colINNotes.FieldName = "INNotes"
        Me.colINNotes.Name = "colINNotes"
        Me.colINNotes.Visible = True
        Me.colINNotes.VisibleIndex = 2
        Me.colINNotes.Width = 897
        '
        'RepositoryItemMemoEdit1
        '
        Me.RepositoryItemMemoEdit1.Name = "RepositoryItemMemoEdit1"
        '
        'pnlJobIssues
        '
        Me.pnlJobIssues.Controls.Add(Me.dgJobIssues)
        Me.HelpProvider1.SetHelpString(Me.pnlJobIssues, """HELPEP{TFUCKING OK BUTTON IS CLICKING! "" ")
        Me.pnlJobIssues.Location = New System.Drawing.Point(40, 8)
        Me.pnlJobIssues.Name = "pnlJobIssues"
        Me.HelpProvider1.SetShowHelp(Me.pnlJobIssues, True)
        Me.pnlJobIssues.Size = New System.Drawing.Size(600, 456)
        Me.pnlJobIssues.TabIndex = 32
        Me.pnlJobIssues.Text = "Job Issues"
        '
        'dgJobIssues
        '
        Me.dgJobIssues.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgJobIssues.DataSource = Me.DsGTMS.VJobs_JobIssues
        '
        'dgJobIssues.EmbeddedNavigator
        '
        Me.dgJobIssues.EmbeddedNavigator.Name = ""
        Me.dgJobIssues.Location = New System.Drawing.Point(16, 32)
        Me.dgJobIssues.MainView = Me.GridView3
        Me.dgJobIssues.Name = "dgJobIssues"
        Me.HelpProvider1.SetShowHelp(Me.dgJobIssues, True)
        Me.dgJobIssues.Size = New System.Drawing.Size(568, 408)
        Me.dgJobIssues.TabIndex = 2
        Me.dgJobIssues.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView3})
        '
        'GridView3
        '
        Me.GridView3.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView3.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView3.Appearance.FocusedRow.Options.UseBackColor = True
        Me.GridView3.Appearance.FocusedRow.Options.UseForeColor = True
        Me.GridView3.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView3.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView3.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView3.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.GridView3.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.GridView3.Appearance.HorzLine.Options.UseBackColor = True
        Me.GridView3.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView3.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView3.Appearance.SelectedRow.Options.UseBackColor = True
        Me.GridView3.Appearance.SelectedRow.Options.UseForeColor = True
        Me.GridView3.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.GridView3.Appearance.VertLine.Options.UseBackColor = True
        Me.GridView3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colIfOccured1, Me.colIfOverSchedule1, Me.colDescription1})
        Me.GridView3.GridControl = Me.dgJobIssues
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsCustomization.AllowFilter = False
        Me.GridView3.OptionsView.ShowGroupPanel = False
        Me.GridView3.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colDescription1, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colIfOccured1
        '
        Me.colIfOccured1.Caption = "Issue occured"
        Me.colIfOccured1.FieldName = "IfOccured"
        Me.colIfOccured1.Name = "colIfOccured1"
        Me.colIfOccured1.Visible = True
        Me.colIfOccured1.VisibleIndex = 1
        Me.colIfOccured1.Width = 73
        '
        'colIfOverSchedule1
        '
        Me.colIfOverSchedule1.Caption = "Issue pushed the job over schedule"
        Me.colIfOverSchedule1.FieldName = "IfOverSchedule"
        Me.colIfOverSchedule1.Name = "colIfOverSchedule1"
        Me.colIfOverSchedule1.Visible = True
        Me.colIfOverSchedule1.VisibleIndex = 2
        Me.colIfOverSchedule1.Width = 179
        '
        'colDescription1
        '
        Me.colDescription1.Caption = "Issue"
        Me.colDescription1.FieldName = "JIDescription"
        Me.colDescription1.Name = "colDescription1"
        Me.colDescription1.OptionsColumn.AllowEdit = False
        Me.colDescription1.OptionsColumn.AllowFocus = False
        Me.colDescription1.Visible = True
        Me.colDescription1.VisibleIndex = 0
        Me.colDescription1.Width = 302
        '
        'pnlJobInformation
        '
        Me.pnlJobInformation.Controls.Add(Me.dpJBScheduledStartDate)
        Me.pnlJobInformation.Controls.Add(Me.dpJBScheduledFinishDate)
        Me.pnlJobInformation.Controls.Add(Me.txtJBJobInsurance)
        Me.pnlJobInformation.Controls.Add(Me.Label46)
        Me.pnlJobInformation.Controls.Add(Me.Label40)
        Me.pnlJobInformation.Controls.Add(Me.Label14)
        Me.pnlJobInformation.Controls.Add(Me.txtJBJobType)
        Me.pnlJobInformation.Controls.Add(Me.dpJBActualFinishDate)
        Me.pnlJobInformation.Controls.Add(Me.txtJBCabinetryBAOTPrice)
        Me.pnlJobInformation.Controls.Add(Me.txtJBGraniteBAOTPrice)
        Me.pnlJobInformation.Controls.Add(Me.txtJBPrice)
        Me.pnlJobInformation.Controls.Add(Me.GroupLine14)
        Me.pnlJobInformation.Controls.Add(Me.Label7)
        Me.pnlJobInformation.Controls.Add(Me.Label11)
        Me.pnlJobInformation.Controls.Add(Me.Label12)
        Me.pnlJobInformation.Controls.Add(Me.Label18)
        Me.pnlJobInformation.Controls.Add(Me.Label19)
        Me.pnlJobInformation.Controls.Add(Me.Label20)
        Me.pnlJobInformation.Controls.Add(Me.Label21)
        Me.pnlJobInformation.Controls.Add(Me.GroupLine15)
        Me.pnlJobInformation.Controls.Add(Me.GroupLine16)
        Me.pnlJobInformation.Controls.Add(Me.Label31)
        Me.pnlJobInformation.Controls.Add(Me.Label32)
        Me.pnlJobInformation.Controls.Add(Me.Label33)
        Me.pnlJobInformation.Controls.Add(Me.Label15)
        Me.pnlJobInformation.Controls.Add(Me.Label16)
        Me.pnlJobInformation.Controls.Add(Me.Label36)
        Me.pnlJobInformation.Controls.Add(Me.Label38)
        Me.pnlJobInformation.Controls.Add(Me.Label39)
        Me.pnlJobInformation.Controls.Add(Me.GroupLine20)
        Me.pnlJobInformation.Controls.Add(Me.Label47)
        Me.pnlJobInformation.Location = New System.Drawing.Point(32, 0)
        Me.pnlJobInformation.Name = "pnlJobInformation"
        Me.pnlJobInformation.Size = New System.Drawing.Size(600, 488)
        Me.pnlJobInformation.TabIndex = 1
        Me.pnlJobInformation.Text = "Job Information"
        '
        'dpJBScheduledStartDate
        '
        Me.dpJBScheduledStartDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBScheduledStartDate"))
        Me.dpJBScheduledStartDate.EditValue = New Date(2005, 8, 19, 0, 0, 0, 0)
        Me.dpJBScheduledStartDate.Enabled = False
        Me.dpJBScheduledStartDate.Location = New System.Drawing.Point(200, 88)
        Me.dpJBScheduledStartDate.Name = "dpJBScheduledStartDate"
        '
        'dpJBScheduledStartDate.Properties
        '
        Me.dpJBScheduledStartDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpJBScheduledStartDate.Properties.NullText = "<Incomplete>"
        Me.dpJBScheduledStartDate.Size = New System.Drawing.Size(184, 20)
        Me.dpJBScheduledStartDate.TabIndex = 0
        '
        'dpJBScheduledFinishDate
        '
        Me.dpJBScheduledFinishDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBScheduledFinishDate"))
        Me.dpJBScheduledFinishDate.EditValue = New Date(2005, 8, 19, 0, 0, 0, 0)
        Me.dpJBScheduledFinishDate.Enabled = False
        Me.dpJBScheduledFinishDate.Location = New System.Drawing.Point(200, 120)
        Me.dpJBScheduledFinishDate.Name = "dpJBScheduledFinishDate"
        '
        'dpJBScheduledFinishDate.Properties
        '
        Me.dpJBScheduledFinishDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpJBScheduledFinishDate.Properties.NullText = "<Incomplete>"
        Me.dpJBScheduledFinishDate.Size = New System.Drawing.Size(184, 20)
        Me.dpJBScheduledFinishDate.TabIndex = 1
        '
        'txtJBJobInsurance
        '
        Me.txtJBJobInsurance.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBJobInsurance"))
        Me.txtJBJobInsurance.EditValue = ""
        Me.txtJBJobInsurance.Location = New System.Drawing.Point(200, 272)
        Me.txtJBJobInsurance.Name = "txtJBJobInsurance"
        '
        'txtJBJobInsurance.Properties
        '
        Me.txtJBJobInsurance.Properties.Appearance.Options.UseTextOptions = True
        Me.txtJBJobInsurance.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtJBJobInsurance.Properties.DisplayFormat.FormatString = "c"
        Me.txtJBJobInsurance.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtJBJobInsurance.Properties.EditFormat.FormatString = "c"
        Me.txtJBJobInsurance.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtJBJobInsurance.Properties.NullText = "<Incomplete>"
        Me.txtJBJobInsurance.Size = New System.Drawing.Size(184, 20)
        Me.txtJBJobInsurance.TabIndex = 4
        '
        'Label46
        '
        Me.Label46.Location = New System.Drawing.Point(40, 272)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(112, 21)
        Me.Label46.TabIndex = 32
        Me.Label46.Text = "Job insurance:"
        Me.Label46.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label40
        '
        Me.Label40.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label40.Location = New System.Drawing.Point(384, 440)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(24, 16)
        Me.Label40.TabIndex = 31
        Me.Label40.Text = "*"
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label14.Location = New System.Drawing.Point(384, 88)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(24, 16)
        Me.Label14.TabIndex = 30
        Me.Label14.Text = "*"
        '
        'txtJBJobType
        '
        Me.txtJBJobType.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JTID"))
        Me.txtJBJobType.Location = New System.Drawing.Point(200, 224)
        Me.txtJBJobType.Name = "txtJBJobType"
        '
        'txtJBJobType.Properties
        '
        Me.txtJBJobType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtJBJobType.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("JTName")})
        Me.txtJBJobType.Properties.DataSource = Me.DsGTMS.JobTypes
        Me.txtJBJobType.Properties.DisplayMember = "JTName"
        Me.txtJBJobType.Properties.NullText = "<Incomplete>"
        Me.txtJBJobType.Properties.ShowFooter = False
        Me.txtJBJobType.Properties.ShowHeader = False
        Me.txtJBJobType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.txtJBJobType.Properties.ValueMember = "JTID"
        Me.txtJBJobType.Size = New System.Drawing.Size(184, 20)
        Me.txtJBJobType.TabIndex = 3
        '
        'dpJBActualFinishDate
        '
        Me.dpJBActualFinishDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBActualFinishDate"))
        Me.dpJBActualFinishDate.EditValue = New Date(2005, 8, 19, 0, 0, 0, 0)
        Me.dpJBActualFinishDate.Location = New System.Drawing.Point(200, 152)
        Me.dpJBActualFinishDate.Name = "dpJBActualFinishDate"
        '
        'dpJBActualFinishDate.Properties
        '
        Me.dpJBActualFinishDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpJBActualFinishDate.Properties.NullText = "<Incomplete>"
        Me.dpJBActualFinishDate.Size = New System.Drawing.Size(184, 20)
        Me.dpJBActualFinishDate.TabIndex = 2
        '
        'txtJBCabinetryBAOTPrice
        '
        Me.txtJBCabinetryBAOTPrice.EditValue = ""
        Me.txtJBCabinetryBAOTPrice.Location = New System.Drawing.Point(200, 440)
        Me.txtJBCabinetryBAOTPrice.Name = "txtJBCabinetryBAOTPrice"
        '
        'txtJBCabinetryBAOTPrice.Properties
        '
        Me.txtJBCabinetryBAOTPrice.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.txtJBCabinetryBAOTPrice.Properties.Appearance.Options.UseBackColor = True
        Me.txtJBCabinetryBAOTPrice.Properties.Appearance.Options.UseTextOptions = True
        Me.txtJBCabinetryBAOTPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtJBCabinetryBAOTPrice.Properties.DisplayFormat.FormatString = "c"
        Me.txtJBCabinetryBAOTPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtJBCabinetryBAOTPrice.Properties.EditFormat.FormatString = "c"
        Me.txtJBCabinetryBAOTPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtJBCabinetryBAOTPrice.Properties.NullText = "<Incomplete>"
        Me.txtJBCabinetryBAOTPrice.Properties.ReadOnly = True
        Me.txtJBCabinetryBAOTPrice.Size = New System.Drawing.Size(184, 20)
        Me.txtJBCabinetryBAOTPrice.TabIndex = 7
        '
        'txtJBGraniteBAOTPrice
        '
        Me.txtJBGraniteBAOTPrice.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBGraniteBAOTPrice"))
        Me.txtJBGraniteBAOTPrice.EditValue = ""
        Me.txtJBGraniteBAOTPrice.Location = New System.Drawing.Point(200, 408)
        Me.txtJBGraniteBAOTPrice.Name = "txtJBGraniteBAOTPrice"
        '
        'txtJBGraniteBAOTPrice.Properties
        '
        Me.txtJBGraniteBAOTPrice.Properties.Appearance.Options.UseTextOptions = True
        Me.txtJBGraniteBAOTPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtJBGraniteBAOTPrice.Properties.DisplayFormat.FormatString = "c"
        Me.txtJBGraniteBAOTPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtJBGraniteBAOTPrice.Properties.EditFormat.FormatString = "c"
        Me.txtJBGraniteBAOTPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtJBGraniteBAOTPrice.Properties.NullText = "<Incomplete>"
        Me.txtJBGraniteBAOTPrice.Size = New System.Drawing.Size(184, 20)
        Me.txtJBGraniteBAOTPrice.TabIndex = 6
        '
        'txtJBPrice
        '
        Me.txtJBPrice.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Jobs.JBPrice"))
        Me.txtJBPrice.EditValue = ""
        Me.txtJBPrice.Location = New System.Drawing.Point(200, 376)
        Me.txtJBPrice.Name = "txtJBPrice"
        '
        'txtJBPrice.Properties
        '
        Me.txtJBPrice.Properties.Appearance.Options.UseTextOptions = True
        Me.txtJBPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtJBPrice.Properties.DisplayFormat.FormatString = "c"
        Me.txtJBPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtJBPrice.Properties.EditFormat.FormatString = "c"
        Me.txtJBPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtJBPrice.Properties.NullText = "<Incomplete>"
        Me.txtJBPrice.Size = New System.Drawing.Size(184, 20)
        Me.txtJBPrice.TabIndex = 5
        '
        'GroupLine14
        '
        Me.GroupLine14.Location = New System.Drawing.Point(16, 24)
        Me.GroupLine14.Name = "GroupLine14"
        Me.GroupLine14.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine14.TabIndex = 19
        Me.GroupLine14.TextString = "Job Date"
        Me.GroupLine14.TextWidth = 50
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(40, 40)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(440, 48)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "The scheduled start and finish date are pre-determined in the scheduling section " & _
        "of the ""Bookings"" stage.  The actual finish date is the date that the job is fin" & _
        "ished and ready for final payment."
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(40, 376)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(104, 21)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "Job price (BAOT):"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(40, 408)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(136, 21)
        Me.Label12.TabIndex = 1
        Me.Label12.Text = "Trend portion of job price:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label18
        '
        Me.Label18.Location = New System.Drawing.Point(40, 200)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(440, 16)
        Me.Label18.TabIndex = 1
        Me.Label18.Text = "Job type is set by head office. It will be used to categorize jobs for reporting " & _
        "purposes. "
        '
        'Label19
        '
        Me.Label19.Location = New System.Drawing.Point(40, 224)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(112, 21)
        Me.Label19.TabIndex = 1
        Me.Label19.Text = "Select the job type:"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label20
        '
        Me.Label20.Location = New System.Drawing.Point(40, 88)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(112, 21)
        Me.Label20.TabIndex = 1
        Me.Label20.Text = "Scheduled start date:"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label21
        '
        Me.Label21.Location = New System.Drawing.Point(40, 320)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(440, 48)
        Me.Label21.TabIndex = 1
        Me.Label21.Text = "BAOT is the job price Before Appliances and Other Trades. This price is broken do" & _
        "wn into the Trend portion and the non-Trend portion.  You need only enter the jo" & _
        "b price (BAOT) and the Trend portion."
        '
        'GroupLine15
        '
        Me.GroupLine15.Location = New System.Drawing.Point(16, 184)
        Me.GroupLine15.Name = "GroupLine15"
        Me.GroupLine15.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine15.TabIndex = 19
        Me.GroupLine15.TextString = "Job Type"
        Me.GroupLine15.TextWidth = 50
        '
        'GroupLine16
        '
        Me.GroupLine16.Location = New System.Drawing.Point(16, 304)
        Me.GroupLine16.Name = "GroupLine16"
        Me.GroupLine16.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine16.TabIndex = 19
        Me.GroupLine16.TextString = "Job Price (BAOT)"
        Me.GroupLine16.TextWidth = 100
        '
        'Label31
        '
        Me.Label31.Location = New System.Drawing.Point(40, 120)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(120, 21)
        Me.Label31.TabIndex = 1
        Me.Label31.Text = "Scheduled finish date:"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label32
        '
        Me.Label32.Location = New System.Drawing.Point(40, 152)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(112, 21)
        Me.Label32.TabIndex = 1
        Me.Label32.Text = "Actual finish date:"
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label33
        '
        Me.Label33.Location = New System.Drawing.Point(40, 440)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(168, 21)
        Me.Label33.TabIndex = 1
        Me.Label33.Text = "Non-Trend portion of job price:"
        Me.Label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label15
        '
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label15.Location = New System.Drawing.Point(384, 120)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(24, 16)
        Me.Label15.TabIndex = 30
        Me.Label15.Text = "*"
        '
        'Label16
        '
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label16.Location = New System.Drawing.Point(384, 152)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(24, 16)
        Me.Label16.TabIndex = 30
        Me.Label16.Text = "*"
        '
        'Label36
        '
        Me.Label36.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label36.Location = New System.Drawing.Point(384, 224)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(24, 16)
        Me.Label36.TabIndex = 30
        Me.Label36.Text = "*"
        '
        'Label38
        '
        Me.Label38.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label38.Location = New System.Drawing.Point(384, 376)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(24, 16)
        Me.Label38.TabIndex = 30
        Me.Label38.Text = "*"
        '
        'Label39
        '
        Me.Label39.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label39.Location = New System.Drawing.Point(384, 408)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(24, 16)
        Me.Label39.TabIndex = 30
        Me.Label39.Text = "*"
        '
        'GroupLine20
        '
        Me.GroupLine20.Location = New System.Drawing.Point(16, 256)
        Me.GroupLine20.Name = "GroupLine20"
        Me.GroupLine20.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine20.TabIndex = 19
        Me.GroupLine20.TextString = "Job Insurance"
        Me.GroupLine20.TextWidth = 80
        '
        'Label47
        '
        Me.Label47.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label47.Location = New System.Drawing.Point(384, 272)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(24, 16)
        Me.Label47.TabIndex = 30
        Me.Label47.Text = "*"
        '
        'pnlDirectLabour
        '
        Me.pnlDirectLabour.Controls.Add(Me.btnRemoveDirectLabour)
        Me.pnlDirectLabour.Controls.Add(Me.btnAddDirectLabour)
        Me.pnlDirectLabour.Controls.Add(Me.dgDirectLabour)
        Me.pnlDirectLabour.Controls.Add(Me.tvDirectLabour)
        Me.pnlDirectLabour.Controls.Add(Me.Label26)
        Me.pnlDirectLabour.Controls.Add(Me.Label27)
        Me.pnlDirectLabour.Location = New System.Drawing.Point(48, 104)
        Me.pnlDirectLabour.Name = "pnlDirectLabour"
        Me.pnlDirectLabour.Size = New System.Drawing.Size(584, 328)
        Me.pnlDirectLabour.TabIndex = 5
        Me.pnlDirectLabour.Text = "Specified Direct Labor"
        '
        'btnRemoveDirectLabour
        '
        Me.btnRemoveDirectLabour.Image = CType(resources.GetObject("btnRemoveDirectLabour.Image"), System.Drawing.Image)
        Me.btnRemoveDirectLabour.Location = New System.Drawing.Point(208, 184)
        Me.btnRemoveDirectLabour.Name = "btnRemoveDirectLabour"
        Me.btnRemoveDirectLabour.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveDirectLabour.TabIndex = 2
        Me.btnRemoveDirectLabour.Text = "Remove"
        '
        'btnAddDirectLabour
        '
        Me.btnAddDirectLabour.Image = CType(resources.GetObject("btnAddDirectLabour.Image"), System.Drawing.Image)
        Me.btnAddDirectLabour.ImageAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnAddDirectLabour.Location = New System.Drawing.Point(208, 152)
        Me.btnAddDirectLabour.Name = "btnAddDirectLabour"
        Me.btnAddDirectLabour.Size = New System.Drawing.Size(72, 23)
        Me.btnAddDirectLabour.TabIndex = 1
        Me.btnAddDirectLabour.Text = "Add"
        '
        'dgDirectLabour
        '
        Me.dgDirectLabour.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgDirectLabour.DataSource = Me.dvJobs_DirectLabour
        '
        'dgDirectLabour.EmbeddedNavigator
        '
        Me.dgDirectLabour.EmbeddedNavigator.Name = ""
        Me.dgDirectLabour.Location = New System.Drawing.Point(288, 48)
        Me.dgDirectLabour.MainView = Me.gvDirectLabour
        Me.dgDirectLabour.Name = "dgDirectLabour"
        Me.dgDirectLabour.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtDollarDirectLabour, Me.txtPercentageDirectLabour, Me.rgPayAsDirectLabour})
        Me.dgDirectLabour.Size = New System.Drawing.Size(280, 264)
        Me.dgDirectLabour.TabIndex = 3
        Me.dgDirectLabour.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvDirectLabour})
        '
        'dvJobs_DirectLabour
        '
        Me.dvJobs_DirectLabour.AllowNew = False
        Me.dvJobs_DirectLabour.RowFilter = "EGType = 'DL'"
        Me.dvJobs_DirectLabour.Table = Me.DsGTMS.VJobs_Expenses
        '
        'gvDirectLabour
        '
        Me.gvDirectLabour.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvDirectLabour.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvDirectLabour.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvDirectLabour.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvDirectLabour.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvDirectLabour.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvDirectLabour.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvDirectLabour.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvDirectLabour.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvDirectLabour.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvDirectLabour.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvDirectLabour.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvDirectLabour.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvDirectLabour.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvDirectLabour.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvDirectLabour.Appearance.VertLine.Options.UseBackColor = True
        Me.gvDirectLabour.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn11, Me.GridColumn12, Me.GridColumn13, Me.GridColumn14})
        Me.gvDirectLabour.CustomizationFormBounds = New System.Drawing.Rectangle(592, 331, 208, 156)
        Me.gvDirectLabour.GridControl = Me.dgDirectLabour
        Me.gvDirectLabour.Name = "gvDirectLabour"
        Me.gvDirectLabour.OptionsCustomization.AllowFilter = False
        Me.gvDirectLabour.OptionsNavigation.AutoFocusNewRow = True
        Me.gvDirectLabour.OptionsView.ShowGroupPanel = False
        Me.gvDirectLabour.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn11, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Name"
        Me.GridColumn11.FieldName = "EXName"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.OptionsColumn.AllowEdit = False
        Me.GridColumn11.OptionsColumn.AllowFocus = False
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 0
        Me.GridColumn11.Width = 87
        '
        'GridColumn12
        '
        Me.GridColumn12.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn12.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.GridColumn12.Caption = "Pay As..."
        Me.GridColumn12.ColumnEdit = Me.rgPayAsDirectLabour
        Me.GridColumn12.FieldName = "JEAsPercent"
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.Visible = True
        Me.GridColumn12.VisibleIndex = 1
        Me.GridColumn12.Width = 123
        '
        'rgPayAsDirectLabour
        '
        Me.rgPayAsDirectLabour.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(0, "$"), New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "%"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Both")})
        Me.rgPayAsDirectLabour.Name = "rgPayAsDirectLabour"
        '
        'GridColumn13
        '
        Me.GridColumn13.Caption = "$ *"
        Me.GridColumn13.ColumnEdit = Me.txtDollarDirectLabour
        Me.GridColumn13.FieldName = "JECharge"
        Me.GridColumn13.Name = "GridColumn13"
        Me.GridColumn13.Visible = True
        Me.GridColumn13.VisibleIndex = 2
        Me.GridColumn13.Width = 57
        '
        'txtDollarDirectLabour
        '
        Me.txtDollarDirectLabour.AutoHeight = False
        Me.txtDollarDirectLabour.DisplayFormat.FormatString = "c"
        Me.txtDollarDirectLabour.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtDollarDirectLabour.EditFormat.FormatString = "c"
        Me.txtDollarDirectLabour.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtDollarDirectLabour.Name = "txtDollarDirectLabour"
        '
        'GridColumn14
        '
        Me.GridColumn14.Caption = "% *"
        Me.GridColumn14.ColumnEdit = Me.txtPercentageDirectLabour
        Me.GridColumn14.FieldName = "JEPercent"
        Me.GridColumn14.Name = "GridColumn14"
        Me.GridColumn14.Visible = True
        Me.GridColumn14.VisibleIndex = 3
        Me.GridColumn14.Width = 55
        '
        'txtPercentageDirectLabour
        '
        Me.txtPercentageDirectLabour.AutoHeight = False
        Me.txtPercentageDirectLabour.DisplayFormat.FormatString = "P"
        Me.txtPercentageDirectLabour.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtPercentageDirectLabour.EditFormat.FormatString = "p"
        Me.txtPercentageDirectLabour.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtPercentageDirectLabour.Name = "txtPercentageDirectLabour"
        '
        'tvDirectLabour
        '
        Me.tvDirectLabour.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.tvDirectLabour.Connection = Me.SqlConnection
        Me.tvDirectLabour.HideSelection = False
        Me.tvDirectLabour.ImageIndex = -1
        Me.tvDirectLabour.Location = New System.Drawing.Point(16, 48)
        Me.tvDirectLabour.Name = "tvDirectLabour"
        Me.tvDirectLabour.SelectedImageIndex = -1
        Me.tvDirectLabour.Size = New System.Drawing.Size(184, 264)
        Me.tvDirectLabour.TabIndex = 0
        '
        'Label26
        '
        Me.Label26.Location = New System.Drawing.Point(16, 24)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(120, 21)
        Me.Label26.TabIndex = 9
        Me.Label26.Text = "Available direct labor:"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label27
        '
        Me.Label27.Location = New System.Drawing.Point(288, 24)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(120, 21)
        Me.Label27.TabIndex = 9
        Me.Label27.Text = "Direct labor in job:"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlSummary
        '
        Me.pnlSummary.Controls.Add(Me.btnViewJobReport)
        Me.pnlSummary.Controls.Add(Me.Label57)
        Me.pnlSummary.Controls.Add(Me.lblJBOtherReduction)
        Me.pnlSummary.Controls.Add(Me.lblJBBalance)
        Me.pnlSummary.Controls.Add(Me.lblJBClientPaid)
        Me.pnlSummary.Controls.Add(Me.lblJBPriceTotal)
        Me.pnlSummary.Controls.Add(Me.lblJBJobInsurance)
        Me.pnlSummary.Controls.Add(Me.lblJBPriceOtherTrades)
        Me.pnlSummary.Controls.Add(Me.lblJBPriceAppliances)
        Me.pnlSummary.Controls.Add(Me.lblJBPrice)
        Me.pnlSummary.Controls.Add(Me.Label56)
        Me.pnlSummary.Controls.Add(Me.Label54)
        Me.pnlSummary.Controls.Add(Me.Label53)
        Me.pnlSummary.Controls.Add(Me.Label52)
        Me.pnlSummary.Controls.Add(Me.Label51)
        Me.pnlSummary.Controls.Add(Me.Label50)
        Me.pnlSummary.Controls.Add(Me.Label49)
        Me.pnlSummary.Location = New System.Drawing.Point(24, 40)
        Me.pnlSummary.Name = "pnlSummary"
        Me.pnlSummary.Size = New System.Drawing.Size(592, 424)
        Me.pnlSummary.TabIndex = 33
        Me.pnlSummary.Text = "Job Price Summary"
        '
        'btnViewJobReport
        '
        Me.btnViewJobReport.Location = New System.Drawing.Point(16, 256)
        Me.btnViewJobReport.Name = "btnViewJobReport"
        Me.btnViewJobReport.Size = New System.Drawing.Size(248, 23)
        Me.btnViewJobReport.TabIndex = 18
        Me.btnViewJobReport.Text = "View Interim Job Report..."
        '
        'Label57
        '
        Me.Label57.Location = New System.Drawing.Point(16, 192)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(136, 18)
        Me.Label57.TabIndex = 17
        Me.Label57.Text = "- Other reduction:"
        Me.Label57.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJBOtherReduction
        '
        Me.lblJBOtherReduction.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsReports, "repJobReport.JBOtherReduction"))
        Me.lblJBOtherReduction.EditValue = "$0.00"
        Me.lblJBOtherReduction.Location = New System.Drawing.Point(152, 192)
        Me.lblJBOtherReduction.Name = "lblJBOtherReduction"
        '
        'lblJBOtherReduction.Properties
        '
        Me.lblJBOtherReduction.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.lblJBOtherReduction.Properties.Appearance.Options.UseBackColor = True
        Me.lblJBOtherReduction.Properties.Appearance.Options.UseTextOptions = True
        Me.lblJBOtherReduction.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblJBOtherReduction.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.lblJBOtherReduction.Properties.DisplayFormat.FormatString = "c"
        Me.lblJBOtherReduction.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBOtherReduction.Properties.EditFormat.FormatString = "c"
        Me.lblJBOtherReduction.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBOtherReduction.Properties.ReadOnly = True
        Me.lblJBOtherReduction.Size = New System.Drawing.Size(112, 18)
        Me.lblJBOtherReduction.TabIndex = 16
        Me.lblJBOtherReduction.TabStop = False
        '
        'DsReports
        '
        Me.DsReports.DataSetName = "dsReports"
        Me.DsReports.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'lblJBBalance
        '
        Me.lblJBBalance.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsReports, "repJobReport.JBBalance"))
        Me.lblJBBalance.EditValue = "$0.00"
        Me.lblJBBalance.Location = New System.Drawing.Point(152, 224)
        Me.lblJBBalance.Name = "lblJBBalance"
        '
        'lblJBBalance.Properties
        '
        Me.lblJBBalance.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.lblJBBalance.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJBBalance.Properties.Appearance.Options.UseBackColor = True
        Me.lblJBBalance.Properties.Appearance.Options.UseFont = True
        Me.lblJBBalance.Properties.Appearance.Options.UseTextOptions = True
        Me.lblJBBalance.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblJBBalance.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.lblJBBalance.Properties.DisplayFormat.FormatString = "c"
        Me.lblJBBalance.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBBalance.Properties.EditFormat.FormatString = "c"
        Me.lblJBBalance.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBBalance.Properties.ReadOnly = True
        Me.lblJBBalance.Size = New System.Drawing.Size(112, 18)
        Me.lblJBBalance.TabIndex = 15
        Me.lblJBBalance.TabStop = False
        '
        'lblJBClientPaid
        '
        Me.lblJBClientPaid.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsReports, "repJobReport.JBClientPaid"))
        Me.lblJBClientPaid.EditValue = "$0.00"
        Me.lblJBClientPaid.Location = New System.Drawing.Point(152, 168)
        Me.lblJBClientPaid.Name = "lblJBClientPaid"
        '
        'lblJBClientPaid.Properties
        '
        Me.lblJBClientPaid.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.lblJBClientPaid.Properties.Appearance.Options.UseBackColor = True
        Me.lblJBClientPaid.Properties.Appearance.Options.UseTextOptions = True
        Me.lblJBClientPaid.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblJBClientPaid.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.lblJBClientPaid.Properties.DisplayFormat.FormatString = "c"
        Me.lblJBClientPaid.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBClientPaid.Properties.EditFormat.FormatString = "c"
        Me.lblJBClientPaid.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBClientPaid.Properties.ReadOnly = True
        Me.lblJBClientPaid.Size = New System.Drawing.Size(112, 18)
        Me.lblJBClientPaid.TabIndex = 13
        Me.lblJBClientPaid.TabStop = False
        '
        'lblJBPriceTotal
        '
        Me.lblJBPriceTotal.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsReports, "repJobReport.JBPriceTotal"))
        Me.lblJBPriceTotal.EditValue = "$0.00"
        Me.lblJBPriceTotal.Location = New System.Drawing.Point(152, 136)
        Me.lblJBPriceTotal.Name = "lblJBPriceTotal"
        '
        'lblJBPriceTotal.Properties
        '
        Me.lblJBPriceTotal.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.lblJBPriceTotal.Properties.Appearance.Options.UseBackColor = True
        Me.lblJBPriceTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.lblJBPriceTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblJBPriceTotal.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.lblJBPriceTotal.Properties.DisplayFormat.FormatString = "c"
        Me.lblJBPriceTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBPriceTotal.Properties.EditFormat.FormatString = "c"
        Me.lblJBPriceTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBPriceTotal.Properties.ReadOnly = True
        Me.lblJBPriceTotal.Size = New System.Drawing.Size(112, 18)
        Me.lblJBPriceTotal.TabIndex = 12
        Me.lblJBPriceTotal.TabStop = False
        '
        'lblJBJobInsurance
        '
        Me.lblJBJobInsurance.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsReports, "repJobReport.JBJobInsurance"))
        Me.lblJBJobInsurance.EditValue = "$0.00"
        Me.lblJBJobInsurance.Location = New System.Drawing.Point(152, 104)
        Me.lblJBJobInsurance.Name = "lblJBJobInsurance"
        '
        'lblJBJobInsurance.Properties
        '
        Me.lblJBJobInsurance.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.lblJBJobInsurance.Properties.Appearance.Options.UseBackColor = True
        Me.lblJBJobInsurance.Properties.Appearance.Options.UseTextOptions = True
        Me.lblJBJobInsurance.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblJBJobInsurance.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.lblJBJobInsurance.Properties.DisplayFormat.FormatString = "c"
        Me.lblJBJobInsurance.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBJobInsurance.Properties.EditFormat.FormatString = "c"
        Me.lblJBJobInsurance.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBJobInsurance.Properties.ReadOnly = True
        Me.lblJBJobInsurance.Size = New System.Drawing.Size(112, 18)
        Me.lblJBJobInsurance.TabIndex = 11
        Me.lblJBJobInsurance.TabStop = False
        '
        'lblJBPriceOtherTrades
        '
        Me.lblJBPriceOtherTrades.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsReports, "repJobReport.JBPriceOtherTrades"))
        Me.lblJBPriceOtherTrades.EditValue = "$0.00"
        Me.lblJBPriceOtherTrades.Location = New System.Drawing.Point(152, 80)
        Me.lblJBPriceOtherTrades.Name = "lblJBPriceOtherTrades"
        '
        'lblJBPriceOtherTrades.Properties
        '
        Me.lblJBPriceOtherTrades.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.lblJBPriceOtherTrades.Properties.Appearance.Options.UseBackColor = True
        Me.lblJBPriceOtherTrades.Properties.Appearance.Options.UseTextOptions = True
        Me.lblJBPriceOtherTrades.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblJBPriceOtherTrades.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.lblJBPriceOtherTrades.Properties.DisplayFormat.FormatString = "c"
        Me.lblJBPriceOtherTrades.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBPriceOtherTrades.Properties.EditFormat.FormatString = "c"
        Me.lblJBPriceOtherTrades.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBPriceOtherTrades.Properties.ReadOnly = True
        Me.lblJBPriceOtherTrades.Size = New System.Drawing.Size(112, 18)
        Me.lblJBPriceOtherTrades.TabIndex = 10
        Me.lblJBPriceOtherTrades.TabStop = False
        '
        'lblJBPriceAppliances
        '
        Me.lblJBPriceAppliances.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsReports, "repJobReport.JBPriceAppliances"))
        Me.lblJBPriceAppliances.EditValue = "$0.00"
        Me.lblJBPriceAppliances.Location = New System.Drawing.Point(152, 56)
        Me.lblJBPriceAppliances.Name = "lblJBPriceAppliances"
        '
        'lblJBPriceAppliances.Properties
        '
        Me.lblJBPriceAppliances.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.lblJBPriceAppliances.Properties.Appearance.Options.UseBackColor = True
        Me.lblJBPriceAppliances.Properties.Appearance.Options.UseTextOptions = True
        Me.lblJBPriceAppliances.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblJBPriceAppliances.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.lblJBPriceAppliances.Properties.DisplayFormat.FormatString = "c"
        Me.lblJBPriceAppliances.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBPriceAppliances.Properties.EditFormat.FormatString = "c"
        Me.lblJBPriceAppliances.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBPriceAppliances.Properties.ReadOnly = True
        Me.lblJBPriceAppliances.Size = New System.Drawing.Size(112, 18)
        Me.lblJBPriceAppliances.TabIndex = 9
        Me.lblJBPriceAppliances.TabStop = False
        '
        'lblJBPrice
        '
        Me.lblJBPrice.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsReports, "repJobReport.JBPrice"))
        Me.lblJBPrice.EditValue = "$0.00"
        Me.lblJBPrice.Location = New System.Drawing.Point(152, 32)
        Me.lblJBPrice.Name = "lblJBPrice"
        '
        'lblJBPrice.Properties
        '
        Me.lblJBPrice.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.lblJBPrice.Properties.Appearance.Options.UseBackColor = True
        Me.lblJBPrice.Properties.Appearance.Options.UseTextOptions = True
        Me.lblJBPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblJBPrice.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.lblJBPrice.Properties.DisplayFormat.FormatString = "c"
        Me.lblJBPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBPrice.Properties.EditFormat.FormatString = "c"
        Me.lblJBPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.lblJBPrice.Properties.ReadOnly = True
        Me.lblJBPrice.Size = New System.Drawing.Size(112, 18)
        Me.lblJBPrice.TabIndex = 8
        Me.lblJBPrice.TabStop = False
        '
        'Label56
        '
        Me.Label56.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label56.Location = New System.Drawing.Point(16, 224)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(136, 18)
        Me.Label56.TabIndex = 7
        Me.Label56.Text = "= Balance:"
        Me.Label56.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label54
        '
        Me.Label54.Location = New System.Drawing.Point(16, 168)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(136, 18)
        Me.Label54.TabIndex = 5
        Me.Label54.Text = "- Paid:"
        Me.Label54.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label53
        '
        Me.Label53.Location = New System.Drawing.Point(16, 136)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(136, 18)
        Me.Label53.TabIndex = 4
        Me.Label53.Text = "= Total incl. GST:"
        Me.Label53.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label52
        '
        Me.Label52.Location = New System.Drawing.Point(16, 104)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(136, 18)
        Me.Label52.TabIndex = 3
        Me.Label52.Text = "+ Job insurance:"
        Me.Label52.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label51
        '
        Me.Label51.Location = New System.Drawing.Point(16, 80)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(136, 18)
        Me.Label51.TabIndex = 2
        Me.Label51.Text = "+ Other trades incl. GST:"
        Me.Label51.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label50
        '
        Me.Label50.Location = New System.Drawing.Point(16, 56)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(136, 18)
        Me.Label50.TabIndex = 1
        Me.Label50.Text = "+ Appliances incl. GST:"
        Me.Label50.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label49
        '
        Me.Label49.Location = New System.Drawing.Point(16, 32)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(136, 18)
        Me.Label49.TabIndex = 0
        Me.Label49.Text = "Job price incl. GST:"
        Me.Label49.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT JTID, JTName FROM JobTypes"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO JobTypes(JTName) VALUES (@JTName); SELECT JTID, JTName FROM JobTypes " & _
        "WHERE (JTID = @@IDENTITY)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JTName", System.Data.SqlDbType.VarChar, 50, "JTName"))
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE JobTypes SET JTName = @JTName WHERE (JTID = @Original_JTID) AND (JTName = " & _
        "@Original_JTName OR @Original_JTName IS NULL AND JTName IS NULL); SELECT JTID, J" & _
        "TName FROM JobTypes WHERE (JTID = @JTID)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JTName", System.Data.SqlDbType.VarChar, 50, "JTName"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JTName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JTName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JTID", System.Data.SqlDbType.Int, 4, "JTID"))
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM JobTypes WHERE (JTID = @Original_JTID) AND (JTName = @Original_JTName" & _
        " OR @Original_JTName IS NULL AND JTName IS NULL)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JTName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JTName", System.Data.DataRowVersion.Original, Nothing))
        '
        'daJobTypes
        '
        Me.daJobTypes.DeleteCommand = Me.SqlDeleteCommand2
        Me.daJobTypes.InsertCommand = Me.SqlInsertCommand2
        Me.daJobTypes.SelectCommand = Me.SqlSelectCommand2
        Me.daJobTypes.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "JobTypes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("JTID", "JTID"), New System.Data.Common.DataColumnMapping("JTName", "JTName")})})
        Me.daJobTypes.UpdateCommand = Me.SqlUpdateCommand2
        '
        'daJobs_Materials
        '
        Me.daJobs_Materials.DeleteCommand = Me.SqlDeleteCommand3
        Me.daJobs_Materials.InsertCommand = Me.SqlInsertCommand3
        Me.daJobs_Materials.SelectCommand = Me.SqlSelectCommand3
        Me.daJobs_Materials.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VJobs_Materials", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("JMID", "JMID"), New System.Data.Common.DataColumnMapping("JBID", "JBID"), New System.Data.Common.DataColumnMapping("MTID", "MTID"), New System.Data.Common.DataColumnMapping("JMPrimarySold", "JMPrimarySold")})})
        Me.daJobs_Materials.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM Jobs_Materials WHERE (BRID = @Original_BRID) AND (JMID = @Original_JM" & _
        "ID) AND (JBID = @Original_JBID) AND (JMPrimarySold = @Original_JMPrimarySold OR " & _
        "@Original_JMPrimarySold IS NULL AND JMPrimarySold IS NULL) AND (MTID = @Original" & _
        "_MTID)"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JMID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JMID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JMPrimarySold", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "JMPrimarySold", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO Jobs_Materials (BRID, JBID, MTID, JMPrimarySold) VALUES (@BRID, @JBID" & _
        ", @MTID, @JMPrimarySold); SELECT BRID, JMID, JBID, MTID, JMPrimarySold, MTPrimar" & _
        "yUOM, MTName, MGControlledAtHeadOffice, MTPrimaryUOMShortName, MTStocked FROM VJ" & _
        "obs_Materials WHERE (BRID = @BRID) AND (JMID = @@IDENTITY) ORDER BY JMID"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JMPrimarySold", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "JMPrimarySold", System.Data.DataRowVersion.Current, Nothing))
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT BRID, JMID, JBID, MTID, JMPrimarySold, MTPrimaryUOM, MTName, MGControlledA" & _
        "tHeadOffice, MTPrimaryUOMShortName, MTStocked FROM VJobs_Materials WHERE (BRID =" & _
        " @BRID) AND (JBID = @JBID)"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection
        Me.SqlSelectCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE Jobs_Materials SET BRID = @BRID, JBID = @JBID, MTID = @MTID, JMPrimarySold" & _
        " = @JMPrimarySold WHERE (BRID = @Original_BRID) AND (JMID = @Original_JMID) AND " & _
        "(JBID = @Original_JBID) AND (JMPrimarySold = @Original_JMPrimarySold OR @Origina" & _
        "l_JMPrimarySold IS NULL AND JMPrimarySold IS NULL) AND (MTID = @Original_MTID); " & _
        "SELECT BRID, JMID, JBID, MTID, JMPrimarySold, MTPrimaryUOM, MTName, MGControlled" & _
        "AtHeadOffice, MTPrimaryUOMShortName, MTStocked FROM VJobs_Materials WHERE (BRID " & _
        "= @BRID) AND (JMID = @JMID) ORDER BY JMID"
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JMPrimarySold", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "JMPrimarySold", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JMID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JMID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JMPrimarySold", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "JMPrimarySold", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JMID", System.Data.SqlDbType.BigInt, 8, "JMID"))
        '
        'daJobs_Expenses
        '
        Me.daJobs_Expenses.DeleteCommand = Me.SqlDeleteCommand4
        Me.daJobs_Expenses.InsertCommand = Me.SqlInsertCommand4
        Me.daJobs_Expenses.SelectCommand = Me.SqlSelectCommand4
        Me.daJobs_Expenses.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VJobs_Expenses", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("JBID", "JBID"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("JEAsPercent", "JEAsPercent"), New System.Data.Common.DataColumnMapping("JECharge", "JECharge"), New System.Data.Common.DataColumnMapping("JEPercent", "JEPercent"), New System.Data.Common.DataColumnMapping("JEPercentageOfJob", "JEPercentageOfJob")})})
        Me.daJobs_Expenses.UpdateCommand = Me.SqlUpdateCommand4
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = "DELETE FROM Jobs_Expenses WHERE (BRID = @Original_BRID) AND (EXID = @Original_EXI" & _
        "D) AND (JBID = @Original_JBID) AND (JEAsPercent = @Original_JEAsPercent OR @Orig" & _
        "inal_JEAsPercent IS NULL AND JEAsPercent IS NULL) AND (JECharge = @Original_JECh" & _
        "arge OR @Original_JECharge IS NULL AND JECharge IS NULL) AND (JEPercent = @Origi" & _
        "nal_JEPercent OR @Original_JEPercent IS NULL AND JEPercent IS NULL) AND (JEPerce" & _
        "ntageOfJob = @Original_JEPercentageOfJob OR @Original_JEPercentageOfJob IS NULL " & _
        "AND JEPercentageOfJob IS NULL)"
        Me.SqlDeleteCommand4.Connection = Me.SqlConnection
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JEAsPercent", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JEAsPercent", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JECharge", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JECharge", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JEPercent", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(4, Byte), "JEPercent", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JEPercentageOfJob", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(4, Byte), "JEPercentageOfJob", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = "INSERT INTO Jobs_Expenses (BRID, JBID, EXID, JEAsPercent, JECharge, JEPercent, JE" & _
        "PercentageOfJob) VALUES (@BRID, @JBID, @EXID, @JEAsPercent, @JECharge, @JEPercen" & _
        "t, @JEPercentageOfJob); SELECT BRID, JBID, EXID, JEAsPercent, JECharge, JEPercen" & _
        "t, JEPercentageOfJob, EXName, EGName, EGType, PMAllowValueInJobScreen, EGID FROM" & _
        " VJobs_Expenses WHERE (BRID = @BRID) AND (EXID = @EXID) AND (JBID = @JBID)"
        Me.SqlInsertCommand4.Connection = Me.SqlConnection
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JEAsPercent", System.Data.SqlDbType.Int, 4, "JEAsPercent"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JECharge", System.Data.SqlDbType.Money, 8, "JECharge"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JEPercent", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(4, Byte), "JEPercent", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JEPercentageOfJob", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(4, Byte), "JEPercentageOfJob", System.Data.DataRowVersion.Current, Nothing))
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT BRID, JBID, EXID, JEAsPercent, JECharge, JEPercent, JEPercentageOfJob, EXN" & _
        "ame, EGName, EGType, PMAllowValueInJobScreen, EGID FROM VJobs_Expenses WHERE (BR" & _
        "ID = @BRID) AND (JBID = @JBID)"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection
        Me.SqlSelectCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = "UPDATE Jobs_Expenses SET BRID = @BRID, JBID = @JBID, EXID = @EXID, JEAsPercent = " & _
        "@JEAsPercent, JECharge = @JECharge, JEPercent = @JEPercent, JEPercentageOfJob = " & _
        "@JEPercentageOfJob WHERE (BRID = @Original_BRID) AND (EXID = @Original_EXID) AND" & _
        " (JBID = @Original_JBID) AND (JEAsPercent = @Original_JEAsPercent OR @Original_J" & _
        "EAsPercent IS NULL AND JEAsPercent IS NULL) AND (JECharge = @Original_JECharge O" & _
        "R @Original_JECharge IS NULL AND JECharge IS NULL) AND (JEPercent = @Original_JE" & _
        "Percent OR @Original_JEPercent IS NULL AND JEPercent IS NULL) AND (JEPercentageO" & _
        "fJob = @Original_JEPercentageOfJob OR @Original_JEPercentageOfJob IS NULL AND JE" & _
        "PercentageOfJob IS NULL); SELECT BRID, JBID, EXID, JEAsPercent, JECharge, JEPerc" & _
        "ent, JEPercentageOfJob, EXName, EGName, EGType, PMAllowValueInJobScreen, EGID FR" & _
        "OM VJobs_Expenses WHERE (BRID = @BRID) AND (EXID = @EXID) AND (JBID = @JBID)"
        Me.SqlUpdateCommand4.Connection = Me.SqlConnection
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JEAsPercent", System.Data.SqlDbType.Int, 4, "JEAsPercent"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JECharge", System.Data.SqlDbType.Money, 8, "JECharge"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JEPercent", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(4, Byte), "JEPercent", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JEPercentageOfJob", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(4, Byte), "JEPercentageOfJob", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JEAsPercent", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JEAsPercent", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JECharge", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JECharge", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JEPercent", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(4, Byte), "JEPercent", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JEPercentageOfJob", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(4, Byte), "JEPercentageOfJob", System.Data.DataRowVersion.Original, Nothing))
        '
        'daClientPayments
        '
        Me.daClientPayments.DeleteCommand = Me.SqlDeleteCommand7
        Me.daClientPayments.InsertCommand = Me.SqlInsertCommand5
        Me.daClientPayments.SelectCommand = Me.SqlSelectCommand5
        Me.daClientPayments.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "ClientPayments", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("JBID", "JBID"), New System.Data.Common.DataColumnMapping("CPID", "CPID"), New System.Data.Common.DataColumnMapping("CPDate", "CPDate"), New System.Data.Common.DataColumnMapping("CPAmount", "CPAmount"), New System.Data.Common.DataColumnMapping("CPNotes", "CPNotes"), New System.Data.Common.DataColumnMapping("CPType", "CPType")})})
        Me.daClientPayments.UpdateCommand = Me.SqlUpdateCommand7
        '
        'SqlDeleteCommand7
        '
        Me.SqlDeleteCommand7.CommandText = "DELETE FROM ClientPayments WHERE (BRID = @Original_BRID) AND (CPID = @Original_CP" & _
        "ID) AND (JBID = @Original_JBID) AND (CPAmount = @Original_CPAmount OR @Original_" & _
        "CPAmount IS NULL AND CPAmount IS NULL) AND (CPDate = @Original_CPDate) AND (CPNo" & _
        "tes = @Original_CPNotes OR @Original_CPNotes IS NULL AND CPNotes IS NULL) AND (C" & _
        "PType = @Original_CPType OR @Original_CPType IS NULL AND CPType IS NULL)"
        Me.SqlDeleteCommand7.Connection = Me.SqlConnection
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CPID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CPID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CPAmount", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CPAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CPDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CPDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CPNotes", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CPNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CPType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CPType", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand5
        '
        Me.SqlInsertCommand5.CommandText = "INSERT INTO ClientPayments(BRID, JBID, CPDate, CPAmount, CPNotes, CPType) VALUES " & _
        "(@BRID, @JBID, @CPDate, @CPAmount, @CPNotes, @CPType); SELECT BRID, JBID, CPID, " & _
        "CPDate, CPAmount, CPNotes, CPType FROM ClientPayments WHERE (BRID = @BRID) AND (" & _
        "CPID = @@IDENTITY) AND (JBID = @JBID)"
        Me.SqlInsertCommand5.Connection = Me.SqlConnection
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CPDate", System.Data.SqlDbType.DateTime, 8, "CPDate"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CPAmount", System.Data.SqlDbType.Money, 8, "CPAmount"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CPNotes", System.Data.SqlDbType.VarChar, 100, "CPNotes"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CPType", System.Data.SqlDbType.VarChar, 2, "CPType"))
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT BRID, JBID, CPID, CPDate, CPAmount, CPNotes, CPType FROM ClientPayments WH" & _
        "ERE (BRID = @BRID) AND (JBID = @JBID)"
        Me.SqlSelectCommand5.Connection = Me.SqlConnection
        Me.SqlSelectCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlUpdateCommand7
        '
        Me.SqlUpdateCommand7.CommandText = "UPDATE ClientPayments SET BRID = @BRID, JBID = @JBID, CPDate = @CPDate, CPAmount " & _
        "= @CPAmount, CPNotes = @CPNotes, CPType = @CPType WHERE (BRID = @Original_BRID) " & _
        "AND (CPID = @Original_CPID) AND (JBID = @Original_JBID) AND (CPAmount = @Origina" & _
        "l_CPAmount OR @Original_CPAmount IS NULL AND CPAmount IS NULL) AND (CPDate = @Or" & _
        "iginal_CPDate) AND (CPNotes = @Original_CPNotes OR @Original_CPNotes IS NULL AND" & _
        " CPNotes IS NULL) AND (CPType = @Original_CPType OR @Original_CPType IS NULL AND" & _
        " CPType IS NULL); SELECT BRID, JBID, CPID, CPDate, CPAmount, CPNotes, CPType FRO" & _
        "M ClientPayments WHERE (BRID = @BRID) AND (CPID = @CPID) AND (JBID = @JBID)"
        Me.SqlUpdateCommand7.Connection = Me.SqlConnection
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CPDate", System.Data.SqlDbType.DateTime, 8, "CPDate"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CPAmount", System.Data.SqlDbType.Money, 8, "CPAmount"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CPNotes", System.Data.SqlDbType.VarChar, 100, "CPNotes"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CPType", System.Data.SqlDbType.VarChar, 2, "CPType"))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CPID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CPID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CPAmount", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CPAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CPDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CPDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CPNotes", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CPNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_CPType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CPType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CPID", System.Data.SqlDbType.BigInt, 8, "CPID"))
        '
        'daJobNonCoreSalesItems
        '
        Me.daJobNonCoreSalesItems.DeleteCommand = Me.SqlCommand1
        Me.daJobNonCoreSalesItems.InsertCommand = Me.SqlCommand2
        Me.daJobNonCoreSalesItems.SelectCommand = Me.SqlCommand3
        Me.daJobNonCoreSalesItems.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VJobsNonCoreSalesItems", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("NIID", "NIID")})})
        Me.daJobNonCoreSalesItems.UpdateCommand = Me.SqlCommand4
        '
        'SqlCommand1
        '
        Me.SqlCommand1.CommandText = "DELETE FROM JobsNonCoreSalesItems WHERE (BRID = @Original_BRID) AND (NIID = @Orig" & _
        "inal_NIID)"
        Me.SqlCommand1.Connection = Me.SqlConnection
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlCommand2
        '
        Me.SqlCommand2.CommandText = "INSERT INTO JobsNonCoreSalesItems(BRID) VALUES (@BRID); SELECT BRID, NIID FROM Jo" & _
        "bsNonCoreSalesItems WHERE (BRID = @BRID) AND (NIID = @@IDENTITY)"
        Me.SqlCommand2.Connection = Me.SqlConnection
        Me.SqlCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlCommand3
        '
        Me.SqlCommand3.CommandText = "SELECT BRID, NIID, JBID, NIName, NIDesc, NICost, NIPrice, EXName, NIType, NIOrder" & _
        "edBy, NIPaidBy, NIUserType, NIBrand, NISupplier FROM VJobsNonCoreSalesItems WHER" & _
        "E (BRID = @BRID) AND (JBID = @JBID)"
        Me.SqlCommand3.Connection = Me.SqlConnection
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlCommand4
        '
        Me.SqlCommand4.CommandText = "UPDATE JobsNonCoreSalesItems SET BRID = @BRID WHERE (BRID = @Original_BRID) AND (" & _
        "NIID = @Original_NIID); SELECT BRID, NIID FROM JobsNonCoreSalesItems WHERE (BRID" & _
        " = @BRID) AND (NIID = @NIID)"
        Me.SqlCommand4.Connection = Me.SqlConnection
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NIID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NIID", System.Data.SqlDbType.BigInt, 8, "NIID"))
        '
        'daJobs_StockedItems
        '
        Me.daJobs_StockedItems.DeleteCommand = Me.SqlDeleteCommand10
        Me.daJobs_StockedItems.InsertCommand = Me.SqlInsertCommand10
        Me.daJobs_StockedItems.SelectCommand = Me.SqlSelectCommand10
        Me.daJobs_StockedItems.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VJobs_StockedItems", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("JBID", "JBID"), New System.Data.Common.DataColumnMapping("SIID", "SIID"), New System.Data.Common.DataColumnMapping("JSInventoryAmount", "JSInventoryAmount")})})
        Me.daJobs_StockedItems.UpdateCommand = Me.SqlUpdateCommand10
        '
        'SqlDeleteCommand10
        '
        Me.SqlDeleteCommand10.CommandText = "DELETE FROM Jobs_StockedItems WHERE (BRID = @Original_BRID) AND (JBID = @Original" & _
        "_JBID) AND (SIID = @Original_SIID) AND (JSInventoryAmount = @Original_JSInventor" & _
        "yAmount OR @Original_JSInventoryAmount IS NULL AND JSInventoryAmount IS NULL)"
        Me.SqlDeleteCommand10.Connection = Me.SqlConnection
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "JSInventoryAmount", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand10
        '
        Me.SqlInsertCommand10.CommandText = "INSERT INTO Jobs_StockedItems (BRID, JBID, SIID, JSInventoryAmount) VALUES (@BRID" & _
        ", @JBID, @SIID, @JSInventoryAmount); SELECT BRID, JBID, SIID, JSInventoryAmount," & _
        " SIName, SIInventoryUOM, SIInventoryUOMShortName FROM VJobs_StockedItems WHERE (" & _
        "BRID = @BRID) AND (JBID = @JBID) AND (SIID = @SIID)"
        Me.SqlInsertCommand10.Connection = Me.SqlConnection
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIID", System.Data.SqlDbType.Int, 4, "SIID"))
        Me.SqlInsertCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "JSInventoryAmount", System.Data.DataRowVersion.Current, Nothing))
        '
        'SqlSelectCommand10
        '
        Me.SqlSelectCommand10.CommandText = "SELECT BRID, JBID, SIID, JSInventoryAmount, SIName, SIInventoryUOM, SIInventoryUO" & _
        "MShortName FROM VJobs_StockedItems WHERE (BRID = @BRID) AND (JBID = @JBID)"
        Me.SqlSelectCommand10.Connection = Me.SqlConnection
        Me.SqlSelectCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlUpdateCommand10
        '
        Me.SqlUpdateCommand10.CommandText = "UPDATE Jobs_StockedItems SET BRID = @BRID, JBID = @JBID, SIID = @SIID, JSInventor" & _
        "yAmount = @JSInventoryAmount WHERE (BRID = @Original_BRID) AND (JBID = @Original" & _
        "_JBID) AND (SIID = @Original_SIID) AND (JSInventoryAmount = @Original_JSInventor" & _
        "yAmount OR @Original_JSInventoryAmount IS NULL AND JSInventoryAmount IS NULL); S" & _
        "ELECT BRID, JBID, SIID, JSInventoryAmount, SIName, SIInventoryUOM, SIInventoryUO" & _
        "MShortName FROM VJobs_StockedItems WHERE (BRID = @BRID) AND (JBID = @JBID) AND (" & _
        "SIID = @SIID)"
        Me.SqlUpdateCommand10.Connection = Me.SqlConnection
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIID", System.Data.SqlDbType.Int, 4, "SIID"))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "JSInventoryAmount", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "JSInventoryAmount", System.Data.DataRowVersion.Original, Nothing))
        '
        'daJobs_Jobissues
        '
        Me.daJobs_Jobissues.DeleteCommand = Me.SqlDeleteCommand5
        Me.daJobs_Jobissues.InsertCommand = Me.SqlInsertCommand6
        Me.daJobs_Jobissues.SelectCommand = Me.SqlSelectCommand6
        Me.daJobs_Jobissues.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VJobs_JobIssues", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("JIID", "JIID"), New System.Data.Common.DataColumnMapping("JBID", "JBID"), New System.Data.Common.DataColumnMapping("IfOccured", "IfOccured"), New System.Data.Common.DataColumnMapping("IfOverSchedule", "IfOverSchedule")})})
        Me.daJobs_Jobissues.UpdateCommand = Me.SqlUpdateCommand5
        '
        'SqlDeleteCommand5
        '
        Me.SqlDeleteCommand5.CommandText = "DELETE FROM Jobs_JobIssues WHERE (BRID = @Original_BRID) AND (JBID = @Original_JB" & _
        "ID) AND (JIID = @Original_JIID) AND (IfOccured = @Original_IfOccured OR @Origina" & _
        "l_IfOccured IS NULL AND IfOccured IS NULL) AND (IfOverSchedule = @Original_IfOve" & _
        "rSchedule OR @Original_IfOverSchedule IS NULL AND IfOverSchedule IS NULL)"
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IfOccured", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IfOccured", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IfOverSchedule", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IfOverSchedule", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand6
        '
        Me.SqlInsertCommand6.CommandText = "INSERT INTO Jobs_JobIssues (BRID, JIID, JBID, IfOccured, IfOverSchedule) VALUES (" & _
        "@BRID, @JIID, @JBID, @IfOccured, @IfOverSchedule); SELECT BRID, JIID, JBID, IfOc" & _
        "cured, IfOverSchedule, JIDescription FROM VJobs_JobIssues WHERE (BRID = @BRID) A" & _
        "ND (JBID = @JBID) AND (JIID = @JIID)"
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JIID", System.Data.SqlDbType.Int, 4, "JIID"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IfOccured", System.Data.SqlDbType.Bit, 1, "IfOccured"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IfOverSchedule", System.Data.SqlDbType.Bit, 1, "IfOverSchedule"))
        '
        'SqlSelectCommand6
        '
        Me.SqlSelectCommand6.CommandText = "SELECT BRID, JIID, JBID, IfOccured, IfOverSchedule, JIDescription FROM VJobs_JobI" & _
        "ssues WHERE (BRID = @BRID) AND (JBID = @JBID)"
        Me.SqlSelectCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'SqlUpdateCommand5
        '
        Me.SqlUpdateCommand5.CommandText = "UPDATE Jobs_JobIssues SET BRID = @BRID, JIID = @JIID, JBID = @JBID, IfOccured = @" & _
        "IfOccured, IfOverSchedule = @IfOverSchedule WHERE (BRID = @Original_BRID) AND (J" & _
        "BID = @Original_JBID) AND (JIID = @Original_JIID) AND (IfOccured = @Original_IfO" & _
        "ccured OR @Original_IfOccured IS NULL AND IfOccured IS NULL) AND (IfOverSchedule" & _
        " = @Original_IfOverSchedule OR @Original_IfOverSchedule IS NULL AND IfOverSchedu" & _
        "le IS NULL); SELECT BRID, JIID, JBID, IfOccured, IfOverSchedule, JIDescription F" & _
        "ROM VJobs_JobIssues WHERE (BRID = @BRID) AND (JBID = @JBID) AND (JIID = @JIID)"
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JIID", System.Data.SqlDbType.Int, 4, "JIID"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IfOccured", System.Data.SqlDbType.Bit, 1, "IfOccured"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IfOverSchedule", System.Data.SqlDbType.Bit, 1, "IfOverSchedule"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IfOccured", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IfOccured", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IfOverSchedule", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IfOverSchedule", System.Data.DataRowVersion.Original, Nothing))
        '
        'HelpProvider1
        '
        Me.HelpProvider1.HelpNamespace = "C:\Documents and Settings\djpower\My Documents\My Projects\GTMS\Version1\Help\Hel" & _
        "pProject.chm"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(632, 504)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 3
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(712, 504)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        '
        'Button1
        '
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.Location = New System.Drawing.Point(8, 504)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(72, 23)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Help"
        '
        'daIDNotes
        '
        Me.daIDNotes.DeleteCommand = Me.SqlDeleteCommand6
        Me.daIDNotes.InsertCommand = Me.SqlCommand5
        Me.daIDNotes.SelectCommand = Me.SqlCommand6
        Me.daIDNotes.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "IDNotes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("INID", "INID"), New System.Data.Common.DataColumnMapping("ID", "ID"), New System.Data.Common.DataColumnMapping("INUser", "INUser"), New System.Data.Common.DataColumnMapping("INDate", "INDate"), New System.Data.Common.DataColumnMapping("INNotes", "INNotes")})})
        Me.daIDNotes.UpdateCommand = Me.SqlUpdateCommand6
        '
        'SqlDeleteCommand6
        '
        Me.SqlDeleteCommand6.CommandText = "DELETE FROM IDNotes WHERE (BRID = @Original_BRID) AND (INID = @Original_INID) AND" & _
        " (ID = @Original_ID) AND (INDate = @Original_INDate OR @Original_INDate IS NULL " & _
        "AND INDate IS NULL) AND (INNotes = @Original_INNotes OR @Original_INNotes IS NUL" & _
        "L AND INNotes IS NULL) AND (INUser = @Original_INUser OR @Original_INUser IS NUL" & _
        "L AND INUser IS NULL)"
        Me.SqlDeleteCommand6.Connection = Me.SqlConnection
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INNotes", System.Data.SqlDbType.VarChar, 1000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INUser", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INUser", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlCommand5
        '
        Me.SqlCommand5.CommandText = "INSERT INTO IDNotes(BRID, ID, INUser, INDate, INNotes) VALUES (@BRID, @ID, @INUse" & _
        "r, @INDate, @INNotes); SELECT BRID, INID, ID, INUser, INDate, INNotes FROM IDNot" & _
        "es WHERE (BRID = @BRID) AND (INID = @@IDENTITY)"
        Me.SqlCommand5.Connection = Me.SqlConnection
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INUser", System.Data.SqlDbType.VarChar, 50, "INUser"))
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INDate", System.Data.SqlDbType.DateTime, 8, "INDate"))
        Me.SqlCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INNotes", System.Data.SqlDbType.VarChar, 1000, "INNotes"))
        '
        'SqlCommand6
        '
        Me.SqlCommand6.CommandText = "SELECT BRID, INID, ID, INUser, INDate, INNotes FROM IDNotes WHERE (BRID = @BRID) " & _
        "AND (ID = @ID)"
        Me.SqlCommand6.Connection = Me.SqlConnection
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        '
        'SqlUpdateCommand6
        '
        Me.SqlUpdateCommand6.CommandText = "UPDATE IDNotes SET BRID = @BRID, ID = @ID, INUser = @INUser, INDate = @INDate, IN" & _
        "Notes = @INNotes WHERE (BRID = @Original_BRID) AND (INID = @Original_INID) AND (" & _
        "ID = @Original_ID) AND (INDate = @Original_INDate OR @Original_INDate IS NULL AN" & _
        "D INDate IS NULL) AND (INNotes = @Original_INNotes OR @Original_INNotes IS NULL " & _
        "AND INNotes IS NULL) AND (INUser = @Original_INUser OR @Original_INUser IS NULL " & _
        "AND INUser IS NULL); SELECT BRID, INID, ID, INUser, INDate, INNotes FROM IDNotes" & _
        " WHERE (BRID = @BRID) AND (INID = @INID)"
        Me.SqlUpdateCommand6.Connection = Me.SqlConnection
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INUser", System.Data.SqlDbType.VarChar, 50, "INUser"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INDate", System.Data.SqlDbType.DateTime, 8, "INDate"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INNotes", System.Data.SqlDbType.VarChar, 1000, "INNotes"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INNotes", System.Data.SqlDbType.VarChar, 1000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INUser", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INUser", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INID", System.Data.SqlDbType.BigInt, 8, "INID"))
        '
        'daExpenses
        '
        Me.daExpenses.DeleteCommand = Me.SqlCommand7
        Me.daExpenses.InsertCommand = Me.SqlCommand8
        Me.daExpenses.SelectCommand = Me.SqlCommand9
        Me.daExpenses.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Expenses", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("EXName", "EXName")})})
        Me.daExpenses.UpdateCommand = Me.SqlCommand10
        '
        'SqlCommand7
        '
        Me.SqlCommand7.CommandText = "DELETE FROM Expenses WHERE (BRID = @Original_BRID) AND (EXID = @Original_EXID) AN" & _
        "D (EXName = @Original_EXName OR @Original_EXName IS NULL AND EXName IS NULL)"
        Me.SqlCommand7.Connection = Me.SqlConnection
        Me.SqlCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand7.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXName", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlCommand8
        '
        Me.SqlCommand8.CommandText = "INSERT INTO Expenses (BRID, EXName) VALUES (@BRID, @EXName); SELECT BRID, EXID, E" & _
        "XName, EGType, EXAppearInCalendar, EXCalendarName, EGName FROM VExpenses WHERE (" & _
        "BRID = @BRID) AND (EXID = @@IDENTITY)"
        Me.SqlCommand8.Connection = Me.SqlConnection
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand8.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        '
        'SqlCommand9
        '
        Me.SqlCommand9.CommandText = "SELECT BRID, EXID, EXName, EGType, EXAppearInCalendar, EXCalendarName, EGName FRO" & _
        "M VExpenses WHERE (EXDiscontinued = 0) AND (BRID = @BRID)"
        Me.SqlCommand9.Connection = Me.SqlConnection
        Me.SqlCommand9.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlCommand10
        '
        Me.SqlCommand10.CommandText = "UPDATE Expenses SET BRID = @BRID, EXName = @EXName WHERE (BRID = @Original_BRID) " & _
        "AND (EXID = @Original_EXID) AND (EXName = @Original_EXName OR @Original_EXName I" & _
        "S NULL AND EXName IS NULL); SELECT BRID, EXID, EXName, EGType, EXAppearInCalenda" & _
        "r, EXCalendarName, EGName FROM VExpenses WHERE (BRID = @BRID) AND (EXID = @EXID)" & _
        ""
        Me.SqlCommand10.Connection = Me.SqlConnection
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlCommand10.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        '
        'frmJob
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(794, 536)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.ListBox)
        Me.DockPadding.All = 8
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmJob"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Job"
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMain.ResumeLayout(False)
        CType(Me.pnlMaterials, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMaterials.ResumeLayout(False)
        CType(Me.dgMaterials, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvJobs_Materials, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvMaterials, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumericMaterials, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlIntro, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlIntro.ResumeLayout(False)
        CType(Me.PictureEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlClientPayments, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlClientPayments.ResumeLayout(False)
        CType(Me.dgClientPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvClientPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvClientPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCurrencyClientPayments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgDiscounts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvDiscounts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvDiscounts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCurrencyDiscounts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgBadDebts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvBadDebts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvBadDebts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCurrencyBadDebts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlJobAddress, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlJobAddress.ResumeLayout(False)
        CType(Me.txtJBJobStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgJBJobAddressAsAbove.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBJobStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBJobSuburb.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBJobState.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBJobPostCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlClientInformation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlClientInformation.ResumeLayout(False)
        CType(Me.txtJBClientEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientFirstName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBReferenceName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientSuburb.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JBClientState.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientPostCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientPhoneNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBClientFaxNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBClientMobileNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlAppliances, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlAppliances.ResumeLayout(False)
        CType(Me.dgAppliances, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvAppliances, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvAppliances, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOrderedOrPaidBy, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlGranite, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlGranite.ResumeLayout(False)
        CType(Me.txtJBInventoryDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgStockedItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvStockedItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumericStockedItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgGranite, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvJobs_Granite, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvGranite, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumericGranite, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlSalesReps, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlSalesReps.ResumeLayout(False)
        CType(Me.dgSalesReps, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvJobs_SalesReps, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvSalesReps, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgPayAsSalesReps, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDollarSalesRep, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPercentageSalesReps, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlResponsibilities, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlResponsibilities.ResumeLayout(False)
        CType(Me.dgResponsibilities, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvResponsibilities, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPercentOfJob, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlOtherTrades, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlOtherTrades.ResumeLayout(False)
        CType(Me.dgOtherTrades, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvOtherTrades, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvOtherTrades, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlNotes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlNotes.ResumeLayout(False)
        CType(Me.dgNotes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvNotes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUser, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvExpenses, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtINDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemMemoEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlJobIssues, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlJobIssues.ResumeLayout(False)
        CType(Me.dgJobIssues, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlJobInformation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlJobInformation.ResumeLayout(False)
        CType(Me.dpJBScheduledStartDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dpJBScheduledFinishDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBJobInsurance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBJobType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dpJBActualFinishDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBCabinetryBAOTPrice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBGraniteBAOTPrice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtJBPrice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlDirectLabour, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlDirectLabour.ResumeLayout(False)
        CType(Me.dgDirectLabour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvJobs_DirectLabour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvDirectLabour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgPayAsDirectLabour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDollarDirectLabour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPercentageDirectLabour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlSummary, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlSummary.ResumeLayout(False)
        CType(Me.lblJBOtherReduction.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsReports, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblJBBalance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblJBClientPaid.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblJBPriceTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblJBJobInsurance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblJBPriceOtherTrades.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblJBPriceAppliances.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblJBPrice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnOK.Enabled = Not HasRole("branch_read_only")
        ListBox.SelectedIndex = 0
        PopulateJBCabinetryBAOTPrice()
    End Sub

    Private Sub LoadTreeViews()
        ' --- MATERIALS ---
        tvMaterial.Filter.SetOrAddField("BRID", BRID)
        tvMaterial.LoadFromStream(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream("WindowsApplication.xmlJobsMaterialTree.xml"))
        tvMaterial.ExpandAll()
        ' --- GRANITE ---
        tvGranite.Filter.SetOrAddField("BRID", BRID)
        tvGranite.LoadFromStream(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream("WindowsApplication.xmlJobsGraniteTree.xml"))
        tvGranite.ExpandAll()
        ' --- DIRECT LABOR ---
        tvDirectLabour.Filter.SetOrAddField("BRID", BRID)
        tvDirectLabour.LoadFromStream(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream("WindowsApplication.xmlJobsDirectLabourTree.xml"))
        tvDirectLabour.ExpandAll()
        ' --- SALES REPS ---
        tvSalesReps.Filter.SetOrAddField("BRID", BRID)
        tvSalesReps.LoadFromStream(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream("WindowsApplication.xmlJobsSalesReps.xml"))
        tvSalesReps.ExpandAll()
    End Sub

    Private Sub FillPreliminaryData()
        ' --- JOB TYPES ---
        daJobTypes.Fill(DsGTMS)
        ' --- EXPENSES ---
        daExpenses.SelectCommand.Parameters("@BRID").Value = BRID
        daExpenses.Fill(DsGTMS)
    End Sub

    Private Sub FillData()
        ' --- ID NOTES ---
        daIDNotes.SelectCommand.Parameters("@BRID").Value = BRID
        daIDNotes.SelectCommand.Parameters("@ID").Value = DataRow("ID")
        daIDNotes.Fill(DsGTMS)
        ' --- Customer PAYMENTS ---
        daClientPayments.SelectCommand.Parameters("@BRID").Value = BRID
        daClientPayments.SelectCommand.Parameters("@JBID").Value = JBID
        daClientPayments.Fill(DsGTMS)
        ' --- MATERIALS ---
        daJobs_Materials.SelectCommand.Parameters("@BRID").Value = BRID
        daJobs_Materials.SelectCommand.Parameters("@JBID").Value = JBID
        daJobs_Materials.Fill(DsGTMS)
        ' --- DIRECT LABOR ---
        daJobs_Expenses.SelectCommand.Parameters("@BRID").Value = BRID
        daJobs_Expenses.SelectCommand.Parameters("@JBID").Value = JBID
        daJobs_Expenses.Fill(DsGTMS)
        ' --- APPLIANCES AND OTHER TRADES ---
        daJobNonCoreSalesItems.SelectCommand.Parameters("@BRID").Value = BRID
        daJobNonCoreSalesItems.SelectCommand.Parameters("@JBID").Value = JBID
        daJobNonCoreSalesItems.Fill(DsGTMS)
        ' --- STOCKED ITEMS ---
        daJobs_StockedItems.SelectCommand.Parameters("@BRID").Value = BRID
        daJobs_StockedItems.SelectCommand.Parameters("@JBID").Value = JBID
        daJobs_StockedItems.Fill(DsGTMS)
        ' --- JOB ISSUES ---
        daJobs_Jobissues.SelectCommand.Parameters("@BRID").Value = BRID
        daJobs_Jobissues.SelectCommand.Parameters("@JBID").Value = JBID
        daJobs_Jobissues.Fill(DsGTMS)
    End Sub

    Private Sub EnableDisable()
        lblJobStreetAddress.Enabled = Not rgJBJobAddressAsAbove.EditValue
        lblJobSuburb.Enabled = Not rgJBJobAddressAsAbove.EditValue
        lblJobState.Enabled = Not rgJBJobAddressAsAbove.EditValue
        lblJobPostCode.Enabled = Not rgJBJobAddressAsAbove.EditValue
        txtJBJobStreetAddress01.Enabled = Not rgJBJobAddressAsAbove.EditValue
        txtJBJobStreetAddress02.Enabled = Not rgJBJobAddressAsAbove.EditValue
        txtJBJobSuburb.Enabled = Not rgJBJobAddressAsAbove.EditValue
        txtJBJobState.Enabled = Not rgJBJobAddressAsAbove.EditValue
        txtJBJobPostCode.Enabled = Not rgJBJobAddressAsAbove.EditValue
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        ' EndEdit() to end editing the dataset record so that we can update
        DataRow.EndEdit()

        SqlDataAdapter.Update(DsGTMS)
        daJobs_Materials.Update(DsGTMS)
        daJobs_Expenses.Update(DsGTMS)
        daJobs_StockedItems.Update(DsGTMS)
        daClientPayments.Update(DsGTMS)
        daJobs_Jobissues.Update(DsGTMS)
        daIDNotes.Update(DsGTMS)

        DataAccess.spExecLockRequest("sp_ReleaseJobLock", BRID, JBID, Transaction)
        Transaction.Commit()
        SqlConnection.Close()

        OK = True
        Me.Close()
    End Sub

    Private HasChanges As Boolean = False
    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            DataRow.EndEdit()
            If HasChanges Or DsGTMS.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If response = MsgBoxResult.Yes Then
                DataAccess.spExecLockRequest("sp_ReleaseJobLock", BRID, JBID, Transaction)
                Transaction.Rollback()
                SqlConnection.Close()
            Else
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private HelpTopic As String = Nothing
    Private Sub ListBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox.SelectedIndexChanged
        Dim pnl As DevExpress.XtraEditors.GroupControl
        For Each pnl In pnlMain.Controls
            pnl.Enabled = False
        Next
        pnl = Nothing
        Select Case ListBox.SelectedIndex
            Case 0
                pnl = pnlIntro
                HelpTopic = Nothing
            Case 1
                pnl = pnlClientInformation
                HelpTopic = "ClientInformation"
            Case 2
                pnl = pnlJobAddress
                HelpTopic = "JobAddress"
            Case 3
                pnl = pnlJobInformation
                HelpTopic = "JobInformation"
            Case 4
                pnl = pnlJobIssues
                HelpTopic = "JobIssues"
            Case 5
                pnl = pnlMaterials
                HelpTopic = "NonStockedMaterial"
            Case 6
                pnl = pnlGranite
                HelpTopic = "StockedMaterial"
            Case 7
                pnl = pnlDirectLabour
                HelpTopic = "DirectLabour"
            Case 8
                pnl = pnlSalesReps
                HelpTopic = "Salespeople"
            Case 9
                pnl = pnlResponsibilities
                gvResponsibilities.ExpandAllGroups()
                HelpTopic = "Responsibilities"
            Case 10
                pnl = pnlAppliances
                HelpTopic = "Appliances"
            Case 11
                pnl = pnlOtherTrades
                HelpTopic = "OtherTrades"
            Case 12
                pnl = pnlClientPayments
                HelpTopic = "ClientPayments"
            Case 13
                pnl = pnlNotes
                HelpTopic = "Notes"
            Case 14
                pnl = pnlSummary
                UpdateSummary()
                HelpTopic = Nothing
        End Select
        If Not pnl Is Nothing Then
            pnl.Enabled = True
            pnl.Dock = DockStyle.Fill
            Power.Library.Library.CenterControls(pnl)
            pnl.BringToFront()
        End If
    End Sub

#Region " Price "

    Private Sub txtJBPrice_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtJBPrice.Validated, txtJBGraniteBAOTPrice.Validated
        PopulateJBCabinetryBAOTPrice()
    End Sub

    Private Sub PopulateJBCabinetryBAOTPrice()
        If IsNumeric(txtJBPrice.Text) And IsNumeric(txtJBGraniteBAOTPrice.Text) Then
            txtJBCabinetryBAOTPrice.EditValue = CDbl(txtJBPrice.EditValue) - CDbl(txtJBGraniteBAOTPrice.EditValue)
        Else
            txtJBCabinetryBAOTPrice.EditValue = DBNull.Value
        End If
    End Sub

#End Region

#Region " Materials "

    Public ReadOnly Property SelectedMaterial() As DataRow
        Get
            If Not gvMaterials.GetSelectedRows Is Nothing Then
                Return gvMaterials.GetDataRow(gvMaterials.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub btnAddMaterial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddMaterial.Click

        Dim row As dsGTMS.VJobs_MaterialsRow = DsGTMS.VJobs_Materials.NewVJobs_MaterialsRow
        row.BRID = BRID
        row.JBID = JBID
        row.JMID = 0
        If CType(tvMaterial.SelectedNode, Power.Forms.TreeNode).Action = "ADD" Then
            row.MTID = CType(tvMaterial.SelectedNode, Power.Forms.TreeNode).PKey("MTID")
        ElseIf CType(tvMaterial.SelectedNode, Power.Forms.TreeNode).Action = "LIST" Then
            Message.ShowMessage("You cannot add this item. You may however select from the list of " & tvMaterial.SelectedNode.Text & ".", MsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            DsGTMS.VJobs_Materials.AddVJobs_MaterialsRow(row)
            daJobs_Materials.Update(DsGTMS)
            HasChanges = True
        Catch ex As System.Data.ConstraintException
            row.Delete()
            Message.ShowMessage(tvMaterial.SelectedNode.Text & " could not be added, as it already exists in the job.", MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub btnRemoveMaterial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveMaterial.Click
        If Not SelectedMaterial Is Nothing Then
            SelectedMaterial.Delete()
        End If
    End Sub

#End Region

#Region " Granite "

    Public ReadOnly Property SelectedGranite() As DataRow
        Get
            If Not gvGranite.GetSelectedRows Is Nothing Then
                Return gvGranite.GetDataRow(gvGranite.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub btnAddGranite_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddGranite.Click
        Dim row As dsGTMS.VJobs_MaterialsRow = DsGTMS.VJobs_Materials.NewVJobs_MaterialsRow
        row.BRID = BRID
        row.JBID = JBID
        row.JMID = 0
        If CType(tvGranite.SelectedNode, Power.Forms.TreeNode).Action = "ADD" Then
            row.MTID = CType(tvGranite.SelectedNode, Power.Forms.TreeNode).PKey("MTID")
        ElseIf CType(tvGranite.SelectedNode, Power.Forms.TreeNode).Action = "LIST" Then
            Message.ShowMessage("You cannot add this item. You may however select from the list of " & tvGranite.SelectedNode.Text & ".", MsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            DsGTMS.VJobs_Materials.AddVJobs_MaterialsRow(row)
            daJobs_Materials.Update(DsGTMS)
            daJobs_StockedItems.Update(DsGTMS) ' Update first to not loose changes
            daJobs_StockedItems.Fill(DsGTMS)
            HasChanges = True
        Catch ex As System.Data.ConstraintException
            row.Delete()
            Message.ShowMessage(tvGranite.SelectedNode.Text & " could not be added, as it already exists in the job.", MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub btnRemoveGranite_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveGranite.Click

        If Not SelectedGranite Is Nothing Then
            daJobs_StockedItems.Update(DsGTMS) ' Update first to not loose changes
            SelectedGranite.Delete()
            daJobs_Materials.Update(DsGTMS)
            DsGTMS.VJobs_StockedItems.Clear()
            daJobs_StockedItems.Fill(DsGTMS)
            HasChanges = True
        End If
    End Sub

#End Region

#Region " Direct Labor "

    Public ReadOnly Property SelectedDirectLabour() As DataRow
        Get
            If Not gvDirectLabour.GetSelectedRows Is Nothing Then
                Return gvDirectLabour.GetDataRow(gvDirectLabour.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub btnAddDirectLabour_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddDirectLabour.Click
        Dim row As dsGTMS.VJobs_ExpensesRow = DsGTMS.VJobs_Expenses.NewVJobs_ExpensesRow
        row.BRID = BRID
        row.JBID = JBID
        row.JEAsPercent = 0
        row.JEPercentageOfJob = 1
        If CType(tvDirectLabour.SelectedNode, Power.Forms.TreeNode).Action = "ADD" Then
            row.EXID = CType(tvDirectLabour.SelectedNode, Power.Forms.TreeNode).PKey("EXID")
        ElseIf CType(tvDirectLabour.SelectedNode, Power.Forms.TreeNode).Action = "LIST" Then
            Message.ShowMessage("You cannot add this item. You may however select from the list of " & tvDirectLabour.SelectedNode.Text & ".", MsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            DsGTMS.VJobs_Expenses.AddVJobs_ExpensesRow(row)
            daJobs_Expenses.Update(DsGTMS)
            HasChanges = True
            DistributeResponsibilitiesEvenly_Add(row)
        Catch ex As System.Data.ConstraintException
            row.Delete()
            Message.ShowMessage(tvDirectLabour.SelectedNode.Text & " could not be added, as it already exists in the job.", MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub btnRemoveDirectLabour_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveDirectLabour.Click
        If Not SelectedDirectLabour Is Nothing Then
            Dim row As DataRow = SelectedDirectLabour
            DistributeResponsibilitiesEvenly_Remove(row)
            row.Delete()
        End If
    End Sub

#End Region

#Region " Sales Reps "

    Public ReadOnly Property SelectedSalesRep() As DataRow
        Get
            If Not gvSalesReps.GetSelectedRows Is Nothing Then
                Return gvSalesReps.GetDataRow(gvSalesReps.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub btnAddSalesRep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddSalesRep.Click
        Dim row As dsGTMS.VJobs_ExpensesRow = DsGTMS.VJobs_Expenses.NewVJobs_ExpensesRow
        row.BRID = BRID
        row.JBID = JBID
        row.JEAsPercent = 1
        row.JEPercentageOfJob = 1
        If CType(tvSalesReps.SelectedNode, Power.Forms.TreeNode).Action = "ADD" Then
            row.EXID = CType(tvSalesReps.SelectedNode, Power.Forms.TreeNode).PKey("EXID")
        ElseIf CType(tvSalesReps.SelectedNode, Power.Forms.TreeNode).Action = "LIST" Then
            Message.ShowMessage("You cannot add this item. You may however select from the list of " & tvSalesReps.SelectedNode.Text & ".", MsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            DsGTMS.VJobs_Expenses.AddVJobs_ExpensesRow(row)
            daJobs_Expenses.Update(DsGTMS)
            HasChanges = True
            DistributeResponsibilitiesEvenly_Add(row)
        Catch ex As System.Data.ConstraintException
            row.Delete()
            Message.ShowMessage(tvSalesReps.SelectedNode.Text & " could not be added, as it already exists in the job.", MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub btnRemoveSalesRep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveSalesRep.Click
        If Not SelectedSalesRep Is Nothing Then
            Dim row As DataRow = SelectedSalesRep
            DistributeResponsibilitiesEvenly_Remove(row)
            row.Delete()
        End If
    End Sub

#End Region

#Region " Customer Payments "

    Private Sub gvClientPayments_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gvClientPayments.InitNewRow
        gvClientPayments.GetDataRow(e.RowHandle)("BRID") = BRID
        gvClientPayments.GetDataRow(e.RowHandle)("JBID") = JBID
        gvClientPayments.GetDataRow(e.RowHandle)("CPType") = "CP"
    End Sub

    Private Sub gvClientPayments_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gvClientPayments.KeyDown
        If e.KeyCode = Keys.Delete Then
            If gvClientPayments.SelectedRowsCount > 0 Then
                gvClientPayments.GetDataRow(gvClientPayments.GetSelectedRows(0)).Delete()
            End If
        End If
    End Sub
    Public ReadOnly Property SelectedClientPayment() As DataRow
        Get
            If Not gvClientPayments.GetSelectedRows Is Nothing Then
                Return gvClientPayments.GetDataRow(gvClientPayments.GetSelectedRows(0))
            End If
        End Get
    End Property
    Private Sub btnRemoveClientPayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveClientPayment.Click
        If Not SelectedClientPayment Is Nothing Then
            SelectedClientPayment.Delete()
        End If
    End Sub
    Public ReadOnly Property SelectedDiscount() As DataRow
        Get
            If Not gvDiscounts.GetSelectedRows Is Nothing Then
                Return gvDiscounts.GetDataRow(gvDiscounts.GetSelectedRows(0))
            End If
        End Get
    End Property
    Private Sub gvDiscounts_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gvDiscounts.InitNewRow
        gvDiscounts.GetDataRow(e.RowHandle)("BRID") = BRID
        gvDiscounts.GetDataRow(e.RowHandle)("JBID") = JBID
        gvDiscounts.GetDataRow(e.RowHandle)("CPType") = "DC"
    End Sub

    Private Sub gvDiscounts_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gvDiscounts.KeyDown
        If e.KeyCode = Keys.Delete Then
            If gvDiscounts.SelectedRowsCount > 0 Then
                gvDiscounts.GetDataRow(gvDiscounts.GetSelectedRows(0)).Delete()
            End If
        End If
    End Sub

    Private Sub btnRemoveDiscount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveDiscount.Click
        If Not SelectedDiscount Is Nothing Then
            SelectedDiscount.Delete()
        End If
    End Sub

    Private Sub gvBadDebts_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gvBadDebts.InitNewRow
        gvBadDebts.GetDataRow(e.RowHandle)("BRID") = BRID
        gvBadDebts.GetDataRow(e.RowHandle)("JBID") = JBID
        gvBadDebts.GetDataRow(e.RowHandle)("CPType") = "BD"
    End Sub

    Private Sub gvBadDebts_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gvBadDebts.KeyDown
        If e.KeyCode = Keys.Delete Then
            If gvBadDebts.SelectedRowsCount > 0 Then
                gvBadDebts.GetDataRow(gvBadDebts.GetSelectedRows(0)).Delete()
            End If
        End If
    End Sub
    Public ReadOnly Property SelectedBadDebt() As DataRow
        Get
            If Not gvBadDebts.GetSelectedRows Is Nothing Then
                Return gvBadDebts.GetDataRow(gvBadDebts.GetSelectedRows(0))
            End If
        End Get
    End Property
    Private Sub btnRemoveBadDebt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveBadDebt.Click
        If Not SelectedBadDebt Is Nothing Then
            SelectedBadDebt.Delete()
        End If
    End Sub

    Private Sub gvClientPayments_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gvClientPayments.InvalidRowException, gvDiscounts.InvalidRowException, gvBadDebts.InvalidRowException
        GridView_InvalidRowException(sender, e)
    End Sub

#End Region

#Region " Appliances "

    Public ReadOnly Property SelectedAppliance() As DataRow
        Get
            If Not gvAppliances.GetSelectedRows Is Nothing Then
                Return gvAppliances.GetDataRow(gvAppliances.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub dgAppliances_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgAppliances.DoubleClick
        btnEditAppliance_Click(sender, e)
    End Sub

    Private Sub btnAddAppliance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAppliance.Click
        Dim c As Cursor = Me.Cursor
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim gui As frmJobAppliance = frmJobAppliance.Add(BRID, JBID, Transaction)
        gui.ShowDialog(Me)
        daJobNonCoreSalesItems.Fill(DsGTMS)
        HasChanges = True
        Me.Cursor = c
    End Sub

    Private Sub btnEditAppliance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditAppliance.Click
        If Not SelectedAppliance Is Nothing Then
            Dim c As Cursor = Me.Cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Dim gui As frmJobAppliance = frmJobAppliance.Edit(SelectedAppliance.Item("BRID"), SelectedAppliance.Item("NIID"), Transaction)
            gui.ShowDialog(Me)
            daJobNonCoreSalesItems.Fill(DsGTMS)
            HasChanges = True
            Me.Cursor = c
        End If
    End Sub

    Private Sub btnRemoveAppliance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveAppliance.Click
        If Not SelectedAppliance Is Nothing Then
            SelectedAppliance.Delete()
            HasChanges = True
            daJobNonCoreSalesItems.Update(DsGTMS)
        End If
    End Sub

#End Region

#Region " Other Trades "

    Public ReadOnly Property SelectedOtherTrade() As DataRow
        Get
            If Not gvOtherTrades.GetSelectedRows Is Nothing Then
                Return gvOtherTrades.GetDataRow(gvOtherTrades.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub dgOtherTrades_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgOtherTrades.DoubleClick
        btnEditOtherTrade_Click(sender, e)
    End Sub

    Private Sub btnAddOtherTrade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddOtherTrade.Click
        Dim c As Cursor = Me.Cursor
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim gui As frmJobOtherTrade = frmJobOtherTrade.Add(BRID, JBID, Transaction)
        gui.ShowDialog(Me)
        daJobNonCoreSalesItems.Fill(DsGTMS)
        HasChanges = True
        Me.Cursor = c
    End Sub

    Private Sub btnEditOtherTrade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditOtherTrade.Click
        If Not SelectedOtherTrade Is Nothing Then
            Dim c As Cursor = Me.Cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Dim gui As frmJobOtherTrade = frmJobOtherTrade.Edit(SelectedOtherTrade.Item("BRID"), SelectedOtherTrade.Item("NIID"), Transaction)
            gui.ShowDialog(Me)
            daJobNonCoreSalesItems.Fill(DsGTMS)
            HasChanges = True
            Me.Cursor = c
        End If
    End Sub

    Private Sub btnRemoveOtherTrade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveOtherTrade.Click
        If Not SelectedOtherTrade Is Nothing Then
            SelectedOtherTrade.Delete()
            HasChanges = True
            daJobNonCoreSalesItems.Update(DsGTMS)
        End If
    End Sub

#End Region

#Region " Notes "

    Private Sub gvNotes_InitNewRow(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gvNotes.InitNewRow
        gvNotes.GetDataRow(e.RowHandle)("BRID") = BRID
        gvNotes.GetDataRow(e.RowHandle)("ID") = DataRow("ID")
    End Sub

    Private Sub gvNotes_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gvNotes.KeyDown
        If e.KeyCode = Keys.Delete Then
            If gvNotes.SelectedRowsCount > 0 Then
                gvNotes.GetDataRow(gvNotes.GetSelectedRows(0)).Delete()
            End If
        End If
    End Sub

    Private Sub gvNotes_InvalidRowException(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gvNotes.InvalidRowException
        GridView_InvalidRowException(sender, e)
    End Sub

#End Region

    Private Sub DistributeResponsibilitiesEvenly_Add(ByVal row As DataRow)
        Dim total, newtotal As Double
        total = 0
        newtotal = 0
        Dim count As Int16 = 0
        For Each r As DataRow In row.Table.Rows
            If Not r.RowState = DataRowState.Deleted Then
                If r("EGID") = row("EGID") And Not r Is row Then
                    total += r("JEPercentageOfJob")
                    count += 1
                End If
            End If
        Next
        If total >= 0.9999 And total <= 1 Then
            total = 1
        End If
        If count > 0 Then
            newtotal = (total - total / (count + 1)) ' This is the total the existing rows must NOW add up to
        Else
            total = 1
        End If
        For Each r As DataRow In row.Table.Rows
            If Not r.RowState = DataRowState.Deleted Then
                If r("EGID") = row("EGID") Then
                    If Not r Is row Then
                        r("JEPercentageOfJob") = r("JEPercentageOfJob") * newtotal / total
                    Else
                        r("JEPercentageOfJob") = total / (count + 1)
                    End If
                End If
            End If
        Next
    End Sub

    Private Sub DistributeResponsibilitiesEvenly_Remove(ByVal row As DataRow)
        Dim total, newtotal As Double
        total = 0
        newtotal = 0
        Dim count As Int16 = 0
        For Each r As DataRow In row.Table.Rows
            If Not r.RowState = DataRowState.Deleted Then
                If r("EGID") = row("EGID") And Not r Is row Then
                    total += r("JEPercentageOfJob")
                    count += 1
                End If
            End If
        Next
        newtotal = total + row("JEPercentageOfJob") ' This is the total the existing rows must NOW add up to
        If newtotal >= 0.9999 And newtotal <= 1 Then
            newtotal = 1
        End If
        For Each r As DataRow In row.Table.Rows
            If Not r.RowState = DataRowState.Deleted Then
                If r("EGID") = row("EGID") And Not r Is row Then
                    r("JEPercentageOfJob") = r("JEPercentageOfJob") * newtotal / total
                End If
            End If
        Next
    End Sub

    Private Sub rgJBJobAddressAsAbove_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgJBJobAddressAsAbove.SelectedIndexChanged
        EnableDisable()
    End Sub

    Private Sub gvDirectLabour_ShowingEditor(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles gvDirectLabour.ShowingEditor, gvSalesReps.ShowingEditor
        Dim view As DevExpress.XtraGrid.Views.Grid.GridView = sender
        If view.FocusedColumn.FieldName = "JECharge" And (view.GetFocusedRowCellValue("JEAsPercent") = 1 Or _
                    Not view.GetFocusedRowCellValue("PMAllowValueInJobScreen")) Or _
        view.FocusedColumn.FieldName = "JEPercent" And (view.GetFocusedRowCellValue("JEAsPercent") = 0 Or _
                    Not view.GetFocusedRowCellValue("PMAllowValueInJobScreen")) Or _
        view.FocusedColumn.FieldName = "JEAsPercent" And Not view.GetFocusedRowCellValue("PMAllowValueInJobScreen") Then
            e.Cancel = True
        End If
    End Sub

    Private Sub gvDirectLabour_RowCellStyle(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs) Handles gvDirectLabour.RowCellStyle, gvSalesReps.RowCellStyle
        Dim view As DevExpress.XtraGrid.Views.Grid.GridView = sender
        If e.Column.FieldName = "JECharge" And (view.GetRowCellValue(e.RowHandle, "JEAsPercent") = 1 Or _
                Not view.GetRowCellValue(e.RowHandle, "PMAllowValueInJobScreen")) Or _
        e.Column.FieldName = "JEPercent" And (view.GetRowCellValue(e.RowHandle, "JEAsPercent") = 0 Or _
                Not view.GetRowCellValue(e.RowHandle, "PMAllowValueInJobScreen")) Or _
        e.Column.FieldName = "JEAsPercent" And Not view.GetRowCellValue(e.RowHandle, "PMAllowValueInJobScreen") Then
            Dim appear As New DevExpress.Utils.AppearanceObject
            appear.BackColor = System.Drawing.SystemColors.Control
            appear.ForeColor = System.Drawing.SystemColors.GrayText
            e.CombineAppearance(appear)
        End If
    End Sub

    Private Sub Text_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) _
    Handles txtJBClientSurname.ParseEditValue, txtJBJobType.ParseEditValue
        Format.Text_ParseEditValue(sender, e)
    End Sub

    Private Sub Decimal_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) _
    Handles txtJBGraniteBAOTPrice.ParseEditValue, txtJBPrice.ParseEditValue, txtNumericMaterials.ParseEditValue, txtNumericGranite.ParseEditValue, _
    txtCurrencyClientPayments.ParseEditValue, txtPercentOfJob.ParseEditValue, _
    txtPercentOfJob.ParseEditValue, txtCurrencyBadDebts.ParseEditValue, txtCurrencyDiscounts.ParseEditValue, txtJBJobInsurance.ParseEditValue, _
    txtPercentageDirectLabour.ParseEditValue, txtPercentageSalesReps.ParseEditValue, txtDollarDirectLabour.ParseEditValue, _
    txtDollarSalesRep.ParseEditValue
        Format.Decimal_ParseEditValue(sender, e)
    End Sub

    Private Sub Decimal_WholeNumbers_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) _
    Handles txtNumericStockedItems.ParseEditValue
        Format.WholeNumber_ParseEditValue(sender, e)
    End Sub

    Private Sub txtPercentage_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) _
    Handles txtPercentageDirectLabour.ParseEditValue, txtPercentageSalesReps.ParseEditValue, txtPercentOfJob.ParseEditValue
        If TypeOf e.Value Is String Then
            If Trim(e.Value) = "" Then
                e.Value = DBNull.Value
            Else
                Try
                    e.Value = e.Value / 100
                Catch ex As Exception
                End Try
            End If
        End If
    End Sub

    Private Sub Date_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) _
    Handles txtINDate.Validating, dpJBScheduledStartDate.Validating, dpJBScheduledFinishDate.Validating, _
    dpJBActualFinishDate.Validating, txtJBInventoryDate.Validating, txtDate.Validating
        Library.DateEdit_Validating(sender, e)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If HelpTopic Is Nothing Then
            ShowHelpTopic(Me, "JobFinancialsTerms.html")
        Else
            ShowHelpTopic(Me, "JobFinancialsTerms.html#" & HelpTopic)
        End If
    End Sub

    Private Sub txtJBClientFirstName_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtJBClientFirstName.EditValueChanged, txtJBClientSurname.EditValueChanged, txtJBClientFirstName.Validated, txtJBClientSurname.Validated
        If Not DataRow Is Nothing Then
            Me.Text = "Job Financials " & DataRow("ID") & " - " & FormatName(DataRow("JBClientFirstName"), DataRow("JBClientSurname"))
        Else
            Me.Text = "Job Financials"
        End If
    End Sub

    Private Sub UpdateSummary()
        Me.Cursor = Windows.Forms.Cursors.WaitCursor

        DataRow.EndEdit()

        If DsGTMS.HasChanges Then
            HasChanges = True
        End If
        SqlDataAdapter.Update(DsGTMS)
        daJobs_Materials.Update(DsGTMS)
        daJobs_Expenses.Update(DsGTMS)
        daJobs_StockedItems.Update(DsGTMS)
        daClientPayments.Update(DsGTMS)
        daJobs_Jobissues.Update(DsGTMS)

        Dim cmd As New SqlClient.SqlCommand("exec dbo.sp_UpdateJobStatistics @BRID = @BRID, @JBID = @JBID", Connection)
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@JBID", JBID)
        cmd.Transaction = Transaction
        cmd.ExecuteNonQuery()
        Dim da As New SqlClient.SqlDataAdapter(cmd)
        cmd.CommandText = "SELECT * FROM dbo.[repJobReport](@BRID, @JBID)"
        da.Fill(DsReports.repJobReport)

        Me.Cursor = Windows.Forms.Cursors.Default
    End Sub

    Private Sub btnViewJobReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewJobReport.Click
        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        Dim gui As New frmReport(Nothing)
        Dim rep As New repJobReport
        rep.Load()
        Dim cmd As New SqlClient.SqlCommand("SELECT * FROM VJobs_Expenses WHERE BRID = @BRID AND JBID = @JBID", Connection)
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@JBID", JBID)
        cmd.Transaction = Transaction
        Dim da As New SqlClient.SqlDataAdapter(cmd)
        da.Fill(DsReports.VJobs_Expenses)
        rep.SetDataSource(DsReports)
        'rep.SetParameterValue("Filename", MyBusinessPlan.Filename)
        gui.Display(rep)
        Me.Cursor = Windows.Forms.Cursors.Default
        gui.Text = "Job Report"
        gui.Show()
    End Sub

End Class
