Public Class frmExpenseAdjustments2
    Inherits DevExpress.XtraEditors.XtraForm

    Private DataRow As DataRow
    Private EAType As String
    Private ShowHours As Boolean
    Private HelpTopic As String = Nothing

    Public Shared Sub Edit(ByVal BRID As Int32, ByRef EXID As Int64, ByVal EAType As String, _
            ByVal EXName As String, ByVal PaymentTypeDisplay As String, ByVal HelpTopic As String)
        Dim gui As New frmExpenseAdjustments2

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            If DataAccess.spExecLockRequest("sp_GetExpenseLock", BRID, EXID, .SqlConnection) Then

                .FillPreliminaryData(BRID, EXID, EAType)
                .CustomiseScreen(PaymentTypeDisplay, EXName, EAType)
                .HelpTopic = HelpTopic
                .EAType = EAType

                .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = BRID
                .SqlDataAdapter.SelectCommand.Parameters("@EXID").Value = EXID
                .SqlDataAdapter.Fill(.dataSet)
                .DataRow = .dataSet.Expenses(0)

                .FillData()

                gui.ShowDialog()

                DataAccess.spExecLockRequest("sp_ReleaseExpenseLock", BRID, EXID, .SqlConnection)
            Else
                Message.CurrentlyAccessed("item")
            End If

            .SqlConnection.Close()
        End With
    End Sub

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents dvAdjustments As System.Data.DataView
    Friend WithEvents daAdjustments As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colEADate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEAAmount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEADesc As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dgAdjustments As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvExpenseAdjustment As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tpExpenseProperties As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents btnRemoveAdjustment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtCurrencyGrid As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents txtDate As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents btnHelp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dataSet As WindowsApplication.dsExpenses
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents colEAHours As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtEAHours As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents Label1 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmExpenseAdjustments2))
        Me.dgAdjustments = New DevExpress.XtraGrid.GridControl
        Me.dataSet = New WindowsApplication.dsExpenses
        Me.gvExpenseAdjustment = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colEADate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtDate = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.colEAAmount = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtCurrencyGrid = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colEADesc = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colEAHours = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtEAHours = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.dvAdjustments = New System.Data.DataView
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.daAdjustments = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.tpExpenseProperties = New DevExpress.XtraTab.XtraTabControl
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnRemoveAdjustment = New DevExpress.XtraEditors.SimpleButton
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.btnHelp = New DevExpress.XtraEditors.SimpleButton
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        CType(Me.dgAdjustments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvExpenseAdjustment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCurrencyGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEAHours, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvAdjustments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tpExpenseProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpExpenseProperties.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgAdjustments
        '
        Me.dgAdjustments.DataSource = Me.dataSet.ExpenseAdjustments
        '
        'dgAdjustments.EmbeddedNavigator
        '
        Me.dgAdjustments.EmbeddedNavigator.Name = ""
        Me.dgAdjustments.Location = New System.Drawing.Point(8, 16)
        Me.dgAdjustments.MainView = Me.gvExpenseAdjustment
        Me.dgAdjustments.Name = "dgAdjustments"
        Me.dgAdjustments.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtCurrencyGrid, Me.txtDate, Me.txtEAHours})
        Me.dgAdjustments.Size = New System.Drawing.Size(472, 296)
        Me.dgAdjustments.TabIndex = 0
        Me.dgAdjustments.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvExpenseAdjustment, Me.GridView1})
        '
        'dataSet
        '
        Me.dataSet.DataSetName = "dsExpenses"
        Me.dataSet.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'gvExpenseAdjustment
        '
        Me.gvExpenseAdjustment.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvExpenseAdjustment.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvExpenseAdjustment.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvExpenseAdjustment.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvExpenseAdjustment.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvExpenseAdjustment.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvExpenseAdjustment.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvExpenseAdjustment.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvExpenseAdjustment.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvExpenseAdjustment.Appearance.VertLine.Options.UseBackColor = True
        Me.gvExpenseAdjustment.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colEADate, Me.colEAAmount, Me.colEADesc, Me.colEAHours})
        Me.gvExpenseAdjustment.GridControl = Me.dgAdjustments
        Me.gvExpenseAdjustment.Name = "gvExpenseAdjustment"
        Me.gvExpenseAdjustment.OptionsCustomization.AllowFilter = False
        Me.gvExpenseAdjustment.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvExpenseAdjustment.OptionsView.ShowGroupPanel = False
        Me.gvExpenseAdjustment.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colEADate, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colEADate
        '
        Me.colEADate.Caption = "Date"
        Me.colEADate.ColumnEdit = Me.txtDate
        Me.colEADate.FieldName = "EADate"
        Me.colEADate.Name = "colEADate"
        Me.colEADate.Visible = True
        Me.colEADate.VisibleIndex = 0
        Me.colEADate.Width = 93
        '
        'txtDate
        '
        Me.txtDate.AutoHeight = False
        Me.txtDate.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtDate.Name = "txtDate"
        '
        'colEAAmount
        '
        Me.colEAAmount.Caption = "Amount"
        Me.colEAAmount.ColumnEdit = Me.txtCurrencyGrid
        Me.colEAAmount.DisplayFormat.FormatString = "c"
        Me.colEAAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colEAAmount.FieldName = "EAAmount"
        Me.colEAAmount.Name = "colEAAmount"
        Me.colEAAmount.Visible = True
        Me.colEAAmount.VisibleIndex = 1
        Me.colEAAmount.Width = 82
        '
        'txtCurrencyGrid
        '
        Me.txtCurrencyGrid.AutoHeight = False
        Me.txtCurrencyGrid.DisplayFormat.FormatString = "c"
        Me.txtCurrencyGrid.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCurrencyGrid.EditFormat.FormatString = "c"
        Me.txtCurrencyGrid.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCurrencyGrid.Name = "txtCurrencyGrid"
        '
        'colEADesc
        '
        Me.colEADesc.Caption = "Description"
        Me.colEADesc.FieldName = "EADesc"
        Me.colEADesc.Name = "colEADesc"
        Me.colEADesc.Visible = True
        Me.colEADesc.VisibleIndex = 3
        Me.colEADesc.Width = 188
        '
        'colEAHours
        '
        Me.colEAHours.Caption = "Hours Worked"
        Me.colEAHours.ColumnEdit = Me.txtEAHours
        Me.colEAHours.FieldName = "EAHours"
        Me.colEAHours.Name = "colEAHours"
        Me.colEAHours.Visible = True
        Me.colEAHours.VisibleIndex = 2
        Me.colEAHours.Width = 88
        '
        'txtEAHours
        '
        Me.txtEAHours.AutoHeight = False
        Me.txtEAHours.DisplayFormat.FormatString = "#.00"
        Me.txtEAHours.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtEAHours.EditFormat.FormatString = "#.00"
        Me.txtEAHours.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtEAHours.Name = "txtEAHours"
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.dgAdjustments
        Me.GridView1.Name = "GridView1"
        '
        'dvAdjustments
        '
        Me.dvAdjustments.Table = Me.dataSet.ExpenseAdjustments
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;user id=sa;integrated security=SSPI;data sou" & _
        "rce=""SERVER\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'daAdjustments
        '
        Me.daAdjustments.DeleteCommand = Me.SqlDeleteCommand1
        Me.daAdjustments.InsertCommand = Me.SqlInsertCommand1
        Me.daAdjustments.SelectCommand = Me.SqlSelectCommand1
        Me.daAdjustments.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "ExpenseAdjustments", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("EAID", "EAID"), New System.Data.Common.DataColumnMapping("EADate", "EADate"), New System.Data.Common.DataColumnMapping("EAAmount", "EAAmount"), New System.Data.Common.DataColumnMapping("EADesc", "EADesc"), New System.Data.Common.DataColumnMapping("EAType", "EAType"), New System.Data.Common.DataColumnMapping("EAHours", "EAHours")})})
        Me.daAdjustments.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM ExpenseAdjustments WHERE (BRID = @Original_BRID) AND (EAID = @Origina" & _
        "l_EAID) AND (EAAmount = @Original_EAAmount) AND (EADate = @Original_EADate) AND " & _
        "(EADesc = @Original_EADesc OR @Original_EADesc IS NULL AND EADesc IS NULL) AND (" & _
        "EAHours = @Original_EAHours OR @Original_EAHours IS NULL AND EAHours IS NULL) AN" & _
        "D (EAType = @Original_EAType) AND (EXID = @Original_EXID)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EAID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EAID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EAAmount", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EAAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EADate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EADate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EADesc", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EADesc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EAHours", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(10, Byte), CType(2, Byte), "EAHours", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EAType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EAType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO ExpenseAdjustments(BRID, EXID, EADate, EAAmount, EADesc, EAType, EAHo" & _
        "urs) VALUES (@BRID, @EXID, @EADate, @EAAmount, @EADesc, @EAType, @EAHours); SELE" & _
        "CT BRID, EXID, EAID, EADate, EAAmount, EADesc, EAType, EAHours FROM ExpenseAdjus" & _
        "tments WHERE (BRID = @BRID) AND (EAID = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EADate", System.Data.SqlDbType.DateTime, 8, "EADate"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EAAmount", System.Data.SqlDbType.Money, 8, "EAAmount"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EADesc", System.Data.SqlDbType.VarChar, 100, "EADesc"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EAType", System.Data.SqlDbType.VarChar, 2, "EAType"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EAHours", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(10, Byte), CType(2, Byte), "EAHours", System.Data.DataRowVersion.Current, Nothing))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, EXID, EAID, EADate, EAAmount, EADesc, EAType, EAHours FROM ExpenseAd" & _
        "justments WHERE (BRID = @BRID) AND (EXID = @EXID) AND (EAType = @EAType)"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EAType", System.Data.SqlDbType.VarChar, 2, "EAType"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE ExpenseAdjustments SET BRID = @BRID, EXID = @EXID, EADate = @EADate, EAAmo" & _
        "unt = @EAAmount, EADesc = @EADesc, EAType = @EAType, EAHours = @EAHours WHERE (B" & _
        "RID = @Original_BRID) AND (EAID = @Original_EAID) AND (EAAmount = @Original_EAAm" & _
        "ount) AND (EADate = @Original_EADate) AND (EADesc = @Original_EADesc OR @Origina" & _
        "l_EADesc IS NULL AND EADesc IS NULL) AND (EAHours = @Original_EAHours OR @Origin" & _
        "al_EAHours IS NULL AND EAHours IS NULL) AND (EAType = @Original_EAType) AND (EXI" & _
        "D = @Original_EXID); SELECT BRID, EXID, EAID, EADate, EAAmount, EADesc, EAType, " & _
        "EAHours FROM ExpenseAdjustments WHERE (BRID = @BRID) AND (EAID = @EAID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EADate", System.Data.SqlDbType.DateTime, 8, "EADate"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EAAmount", System.Data.SqlDbType.Money, 8, "EAAmount"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EADesc", System.Data.SqlDbType.VarChar, 100, "EADesc"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EAType", System.Data.SqlDbType.VarChar, 2, "EAType"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EAHours", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(10, Byte), CType(2, Byte), "EAHours", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EAID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EAID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EAAmount", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EAAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EADate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EADate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EADesc", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EADesc", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EAHours", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(10, Byte), CType(2, Byte), "EAHours", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EAType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EAType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EAID", System.Data.SqlDbType.BigInt, 8, "EAID"))
        '
        'tpExpenseProperties
        '
        Me.tpExpenseProperties.Location = New System.Drawing.Point(8, 8)
        Me.tpExpenseProperties.Name = "tpExpenseProperties"
        Me.tpExpenseProperties.SelectedTabPage = Me.XtraTabPage1
        Me.tpExpenseProperties.Size = New System.Drawing.Size(496, 400)
        Me.tpExpenseProperties.TabIndex = 0
        Me.tpExpenseProperties.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1})
        Me.tpExpenseProperties.Text = "XtraTabControl1"
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.Label1)
        Me.XtraTabPage1.Controls.Add(Me.btnRemoveAdjustment)
        Me.XtraTabPage1.Controls.Add(Me.dgAdjustments)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(487, 370)
        Me.XtraTabPage1.Text = "Expense Properties"
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(16, 320)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(448, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "NOTE: All costs entered into the program include Indirect Tax, where applicable"
        '
        'btnRemoveAdjustment
        '
        Me.btnRemoveAdjustment.Image = CType(resources.GetObject("btnRemoveAdjustment.Image"), System.Drawing.Image)
        Me.btnRemoveAdjustment.Location = New System.Drawing.Point(8, 344)
        Me.btnRemoveAdjustment.Name = "btnRemoveAdjustment"
        Me.btnRemoveAdjustment.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveAdjustment.TabIndex = 2
        Me.btnRemoveAdjustment.Text = "Remove"
        '
        'btnOK
        '
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(352, 416)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(432, 416)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'btnHelp
        '
        Me.btnHelp.Image = CType(resources.GetObject("btnHelp.Image"), System.Drawing.Image)
        Me.btnHelp.Location = New System.Drawing.Point(8, 416)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(72, 23)
        Me.btnHelp.TabIndex = 1
        Me.btnHelp.Text = "Help"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand2
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand2
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand2
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Expenses", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("EXName", "EXName")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM Expenses WHERE (BRID = @Original_BRID) AND (EXID = @Original_EXID) AN" & _
        "D (EXName = @Original_EXName OR @Original_EXName IS NULL AND EXName IS NULL)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXName", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO Expenses(BRID, EXName) VALUES (@BRID, @EXName); SELECT BRID, EXID, EX" & _
        "Name FROM Expenses WHERE (BRID = @BRID) AND (EXID = @@IDENTITY)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT BRID, EXID, EXName FROM Expenses WHERE (BRID = @BRID) AND (EXID = @EXID)"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection
        Me.SqlSelectCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE Expenses SET BRID = @BRID, EXName = @EXName WHERE (BRID = @Original_BRID) " & _
        "AND (EXID = @Original_EXID) AND (EXName = @Original_EXName OR @Original_EXName I" & _
        "S NULL AND EXName IS NULL); SELECT BRID, EXID, EXName FROM Expenses WHERE (BRID " & _
        "= @BRID) AND (EXID = @EXID)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        '
        'frmExpenseAdjustments2
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(514, 448)
        Me.Controls.Add(Me.btnHelp)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.tpExpenseProperties)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExpenseAdjustments2"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Expense"
        CType(Me.dgAdjustments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvExpenseAdjustment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCurrencyGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEAHours, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvAdjustments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tpExpenseProperties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpExpenseProperties.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnOK.Enabled = Not HasRole("branch_read_only")
    End Sub

    Public ReadOnly Property SelectedExpenseAdjustment() As DataRow
        Get
            If Not gvExpenseAdjustment.GetSelectedRows Is Nothing Then
                Return gvExpenseAdjustment.GetDataRow(gvExpenseAdjustment.GetSelectedRows(0))
            End If
        End Get
    End Property
    ' This data must be loaded BEFORE the DataRow is loaded (eg lookups etc)
    Private Sub FillPreliminaryData(ByVal BRID As Integer, ByVal EXID As Integer, ByVal EAType As String)
        ' --- SHOW HOURS ---
        ShowHours = DataAccess.ShowHours(BRID, SqlConnection) And EAType = "PR" And DataAccess.EGType(BRID, EXID, SqlConnection) = "DL"
    End Sub

    ' This data must be loaded AFTER the DataRow is loaded (eg record related data)
    Private Sub FillData()
        ' Allowances
        daAdjustments.SelectCommand.Parameters("@BRID").Value = DataRow("BRID")
        daAdjustments.SelectCommand.Parameters("@EXID").Value = DataRow("EXID")
        daAdjustments.SelectCommand.Parameters("@EAType").Value = EAType
        Try
            daAdjustments.Fill(dataSet)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub UpdateData()
        daAdjustments.Update(dataSet)
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        ' EndEdit() to end editing the dataset record so that we can update
        DataRow.EndEdit()

        UpdateData()
        OK = True
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            DataRow.EndEdit()
            If dataSet.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If response = MsgBoxResult.No Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub CustomiseScreen(ByVal PaymentTypeDisplay As String, ByVal EXName As String, ByVal EAType As String)
        colEAHours.Visible = ShowHours
        Me.Text = PaymentTypeDisplay & " for " & EXName
        Me.tpExpenseProperties.Text = PaymentTypeDisplay
        Select Case EAType
            Case "PR"
                Me.Icon = New Icon(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream("WindowsApplication.ExpenseAdjustmentsPR.ico"))
            Case "BO"
                Me.Icon = New Icon(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream("WindowsApplication.ExpenseAdjustmentsBO.ico"))
        End Select
    End Sub

    Private Sub gvExpenseAllowance_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gvExpenseAdjustment.InitNewRow
        gvExpenseAdjustment.GetDataRow(e.RowHandle)("BRID") = DataRow("BRID")
        gvExpenseAdjustment.GetDataRow(e.RowHandle)("EXID") = DataRow("EXID")
        gvExpenseAdjustment.GetDataRow(e.RowHandle)("EAType") = EAType
    End Sub

    Private Sub gvExpenseAdjustment_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gvExpenseAdjustment.KeyDown
        If e.KeyCode = Keys.Delete Then
            If gvExpenseAdjustment.SelectedRowsCount > 0 Then
                gvExpenseAdjustment.GetDataRow(gvExpenseAdjustment.GetSelectedRows(0)).Delete()
            End If
        End If
    End Sub

    Private Sub btnRemoveAdjustment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveAdjustment.Click
        If Not SelectedExpenseAdjustment Is Nothing Then
            SelectedExpenseAdjustment.Delete()
        End If
    End Sub

    Private Sub gvClientPayments_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gvExpenseAdjustment.InvalidRowException
        GridView_InvalidRowException(sender, e)
    End Sub

    Private Sub txtCurrency_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) Handles txtCurrencyGrid.ParseEditValue, txtEAHours.ParseEditValue
        Decimal_ParseEditValue(sender, e)
    End Sub

    Private Sub Date_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtDate.Validating
        Library.DateEdit_Validating(sender, e)
    End Sub

    Private Sub btnHelp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHelp.Click
        If HelpTopic Is Nothing Then
            ShowHelp(Me)
        Else
            ShowHelpTopic(Me, HelpTopic)
        End If
    End Sub

End Class