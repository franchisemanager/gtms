Imports Power.Forms

Public Class lvJobs2
    Inherits DevExpress.XtraEditors.XtraUserControl
    Implements IListControl, IPrintableControl, ISelectedItemReports

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Connection As SqlClient.SqlConnection, ByVal BRID As Integer, ByVal ExplorerForm As frmExplorer)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.Connection = Connection
        Me.BRID = BRID
        hExplorerForm = ExplorerForm
        SetUpPermissions()
        SetUpFilters()
        DateFilterIsListening = True
        FillDataSet()
        EnableDisable()
    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DsGTMS As WindowsApplication.dsGTMS
    Friend WithEvents hSqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colJBClientName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colJBJobAddress As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colJBScheduledStartDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colJBPrice As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colJBStatus As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents ImageListToolBar As System.Windows.Forms.ImageList
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents btnNew As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnEdit As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnDelete As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPrintPreview As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subReportsOnSelectedItem As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnAppliancesOrderForm As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnOtherTradesOrderForm As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnJobReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnJobReportbyClient As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnJobIncompleteReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subFormsOnSelectedItem As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnStockedMaterialinJobForm As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnClientSatisfactionAndPaymentForm As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnClientSurveyForm As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bStandard As DevExpress.XtraBars.Bar
    Friend WithEvents btnQuickPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarAndDockingController1 As DevExpress.XtraBars.BarAndDockingController
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents PopupMenu1 As DevExpress.XtraBars.PopupMenu
    Friend WithEvents btnHelp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subReportsOnSelectedItem2 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents subFormsOnSelectedItem2 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnUncancel As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnCancel As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents bAdvancedFilter As DevExpress.XtraBars.Bar
    Friend WithEvents beiFromDate As DevExpress.XtraBars.BarEditItem
    Friend WithEvents beiToDate As DevExpress.XtraBars.BarEditItem
    Friend WithEvents btnMailMerge As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnResetFilters As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RepositoryItemPopupContainerEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit
    Friend WithEvents DataView As System.Data.DataView
    Friend WithEvents beiJBStatus As DevExpress.XtraBars.BarEditItem
    Friend WithEvents pccJBStatus As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents clbJBStatus As DevExpress.XtraEditors.CheckedListBoxControl
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents btnTaxInvoice As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents colJBGrossProfitPercent As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim StyleFormatCondition1 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(lvJobs2))
        Me.colJBStatus = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox
        Me.hSqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.DsGTMS = New WindowsApplication.dsGTMS
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.DataView = New System.Data.DataView
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colID = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colJBClientName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colJBJobAddress = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colJBScheduledStartDate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colJBPrice = New DevExpress.XtraGrid.Columns.GridColumn
        Me.ImageListToolBar = New System.Windows.Forms.ImageList(Me.components)
        Me.BarManager1 = New DevExpress.XtraBars.BarManager
        Me.bStandard = New DevExpress.XtraBars.Bar
        Me.btnEdit = New DevExpress.XtraBars.BarButtonItem
        Me.btnCancel = New DevExpress.XtraBars.BarButtonItem
        Me.subReportsOnSelectedItem = New DevExpress.XtraBars.BarSubItem
        Me.btnAppliancesOrderForm = New DevExpress.XtraBars.BarButtonItem
        Me.btnOtherTradesOrderForm = New DevExpress.XtraBars.BarButtonItem
        Me.btnTaxInvoice = New DevExpress.XtraBars.BarButtonItem
        Me.btnJobReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnJobReportbyClient = New DevExpress.XtraBars.BarButtonItem
        Me.btnJobIncompleteReport = New DevExpress.XtraBars.BarButtonItem
        Me.subFormsOnSelectedItem = New DevExpress.XtraBars.BarSubItem
        Me.btnStockedMaterialinJobForm = New DevExpress.XtraBars.BarButtonItem
        Me.btnClientSatisfactionAndPaymentForm = New DevExpress.XtraBars.BarButtonItem
        Me.btnClientSurveyForm = New DevExpress.XtraBars.BarButtonItem
        Me.btnQuickPrint = New DevExpress.XtraBars.BarButtonItem
        Me.btnPrintPreview = New DevExpress.XtraBars.BarButtonItem
        Me.btnMailMerge = New DevExpress.XtraBars.BarButtonItem
        Me.btnHelp = New DevExpress.XtraBars.BarButtonItem
        Me.bAdvancedFilter = New DevExpress.XtraBars.Bar
        Me.beiFromDate = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.beiToDate = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.beiJBStatus = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemPopupContainerEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit
        Me.pccJBStatus = New DevExpress.XtraEditors.PopupContainerControl
        Me.clbJBStatus = New DevExpress.XtraEditors.CheckedListBoxControl
        Me.btnResetFilters = New DevExpress.XtraBars.BarButtonItem
        Me.BarAndDockingController1 = New DevExpress.XtraBars.BarAndDockingController(Me.components)
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
        Me.btnNew = New DevExpress.XtraBars.BarButtonItem
        Me.btnUncancel = New DevExpress.XtraBars.BarButtonItem
        Me.btnDelete = New DevExpress.XtraBars.BarButtonItem
        Me.btnPrint = New DevExpress.XtraBars.BarButtonItem
        Me.subReportsOnSelectedItem2 = New DevExpress.XtraBars.BarSubItem
        Me.subFormsOnSelectedItem2 = New DevExpress.XtraBars.BarSubItem
        Me.PopupMenu1 = New DevExpress.XtraBars.PopupMenu
        Me.colJBGrossProfitPercent = New DevExpress.XtraGrid.Columns.GridColumn
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemPopupContainerEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pccJBStatus, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pccJBStatus.SuspendLayout()
        CType(Me.clbJBStatus, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'colJBStatus
        '
        Me.colJBStatus.Caption = "Status"
        Me.colJBStatus.ColumnEdit = Me.RepositoryItemComboBox1
        Me.colJBStatus.FieldName = "JBStatus"
        Me.colJBStatus.Name = "colJBStatus"
        Me.colJBStatus.Visible = True
        Me.colJBStatus.VisibleIndex = 6
        Me.colJBStatus.Width = 85
        '
        'RepositoryItemComboBox1
        '
        Me.RepositoryItemComboBox1.AutoHeight = False
        Me.RepositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox1.Items.AddRange(New Object() {"Incomplete", "Complete", "Cancelled"})
        Me.RepositoryItemComboBox1.Name = "RepositoryItemComboBox1"
        '
        'hSqlConnection
        '
        Me.hSqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;user id=sa;integrated security=SSPI;data sou" & _
        "rce=""SERVER\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VJobs", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("JBID", "JBID"), New System.Data.Common.DataColumnMapping("ID", "ID"), New System.Data.Common.DataColumnMapping("JBScheduledStartDate", "JBScheduledStartDate"), New System.Data.Common.DataColumnMapping("JBClientFirstName", "JBClientFirstName"), New System.Data.Common.DataColumnMapping("JBJobAddress", "JBJobAddress"), New System.Data.Common.DataColumnMapping("JBPriceQuoted", "JBPriceQuoted"), New System.Data.Common.DataColumnMapping("JBIsCancelled", "JBIsCancelled"), New System.Data.Common.DataColumnMapping("JBIsComplete", "JBIsComplete")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Jobs WHERE (BRID = @Original_BRID) AND (JBID = @Original_JBID)"
        Me.SqlDeleteCommand1.Connection = Me.hSqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        ''
        'Me.SqlInsertCommand1.CommandText = "INSERT INTO Jobs (BRID, ID, JBScheduledStartDate, JBClientName, JBJobAddress, JTID, JBP" &
        '"rice, JBIsCancelled, JBIsComplete) VALUES (@BRID, @ID, @JBScheduledStartDate, @J" &
        '"BClientName, @JBJobAddress, @JTID, @JBPrice, @JBIsCancelled, @JBIsComplete); SELECT BRI" &
        '"D, JBID, ID, JBScheduledStartDate, JBClientName, JBJobAddress, JTID, JBPrice, JBStatus" &
        '", JBIsCancelled, JBIsComplete, [JBGrossProfit%] FROM VJobs WHERE (BRID = @BRID) " &
        '"AND (JBID = @@IDENTITY)"


        Me.SqlInsertCommand1.CommandText = "INSERT INTO Jobs (BRID, ID, JBScheduledStartDate, JBClientFirstName,JBJobAddress, JTID, JBP" &
        "rice, JBIsCancelled, JBIsComplete) VALUES (@BRID, @ID,@JBScheduledStartDate, @J" &
        "BClientName, @JBJobAddress, @JBPriceQuoted, @JBIsCancelled, @JBIsComplete); SELECT BRI" &
        "D, JBID, ID, JBScheduledStartDate,JBClientFirstName, JBJobAddress,JBJobAddressMultiline, JTID, JBPriceQuoted, JBStatus" &
        ", JBIsCancelled, JBIsComplete, JBJobStreetAddress01, [JBGrossProfit%] FROM VJobs WHERE (BRID = @BRID) " &
        "AND (JBID = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.hSqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBScheduledStartDate", System.Data.SqlDbType.DateTime, 8, "JBScheduledStartDate"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientFirstName", System.Data.SqlDbType.VarChar, 102, "JBClientFirstName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobAddress", System.Data.SqlDbType.VarChar, 259, "JBJobAddress"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBPriceQuoted", System.Data.SqlDbType.Money, 8, "JBPriceQuoted"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBIsCancelled", System.Data.SqlDbType.Bit, 1, "JBIsCancelled"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBIsComplete", System.Data.SqlDbType.Bit, 1, "JBIsComplete"))
        '
        'SqlSelectCommand1
        '
        'Me.SqlSelectCommand1.CommandText = "SELECT BRID, JBID, ID, JBScheduledStartDate, JBClientName, JBJobAddress, JTID, JBPrice," &
        '" JBStatus, JBIsCancelled, JBIsComplete, [JBGrossProfit%] FROM VJobs WHERE (JBDat" &
        '"e >= @FROM_DATE) AND (JBDate < DATEADD(DAY, 1, @TO_DATE)) AND (BRID = @BRID) OR " &
        '"(JBDate < DATEADD(DAY, 1, @TO_DATE)) AND (BRID = @BRID) AND (@FROM_DATE IS NULL)" &
        '" OR (JBDate >= @FROM_DATE) AND (BRID = @BRID) AND (@TO_DATE IS NULL) OR (BRID = " &
        '"@BRID) AND (@FROM_DATE IS NULL) AND (@TO_DATE IS NULL)"

        'Me.SqlSelectCommand1.CommandText = "SELECT BRID, JBID, ID, JBScheduledStartDate, JBClientName, JBJobAddress, JTID, JBPrice," &
        '" JBStatus, JBIsCancelled, JBIsComplete, JBJobStreetAddress01 FROM VJobs WHERE (JBDat" &
        '"e >= @FROM_DATE) AND (JBDate < DATEADD(DAY, 1, @TO_DATE)) AND (BRID = @BRID) OR " &
        '"(JBDate < DATEADD(DAY, 1, @TO_DATE)) AND (BRID = @BRID) AND (@FROM_DATE IS NULL)" &
        '" OR (JBDate >= @FROM_DATE) AND (BRID = @BRID) AND (@TO_DATE IS NULL) OR (BRID = " &
        '"@BRID) AND (@FROM_DATE IS NULL) AND (@TO_DATE IS NULL)"



        Me.SqlSelectCommand1.CommandText = "SELECT BRID, JBID, ID, JBScheduledStartDate, JBClientName," &
            " COALESCE(JBClientFirstName + ', ', '') + COALESCE(JBClientSurname,'') as JBClientFirstName, " & " JBJobAddressMultiline, JBJobAddress, JBPriceQuoted, JTID, JBPrice," &
        " JBStatus, JBIsCancelled, JBIsComplete, JBJobStreetAddress01, [JBGrossProfit%] FROM VJobs WHERE (JBBookingDate >= @FROM_DATE) AND (JBBookingDate < DATEADD(DAY, 1, @TO_DATE)) AND (BRID = @BRID) OR " &
        "(JBBookingDate < DATEADD(DAY, 1, @TO_DATE)) AND (BRID = @BRID) AND (@FROM_DATE IS NULL)" &
        " OR (JBBookingDate >= @FROM_DATE) AND (BRID = @BRID) AND (@TO_DATE IS NULL) OR (BRID = " &
        "@BRID) AND (@FROM_DATE IS NULL) AND (@TO_DATE IS NULL)"
        Me.SqlSelectCommand1.Connection = Me.hSqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FROM_DATE", System.Data.SqlDbType.DateTime, 8, "JBDate"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TO_DATE", System.Data.SqlDbType.DateTime, 8, "JBDate"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlUpdateCommand1
        '
        'Me.SqlUpdateCommand1.CommandText = "UPDATE Jobs SET BRID = @BRID, ID = @ID, JBScheduledStartDate = @JBScheduledStartD" &
        '"ate, JBClientName = @JBClientName, JBJobAddress = @JBJobAddress, JTID=@JTID, JBPrice = @JBPr" &
        '"ice, JBIsCancelled = @JBIsCancelled, JBIsComplete = @JBIsComplete WHERE (BRID = " &
        '"@Original_BRID) AND (JBID = @Original_JBID); SELECT BRID, JBID, ID, JBScheduledS" &
        '"tartDate, JBClientName, JBJobAddress, JBPrice, JBStatus, JBIsCancelled, JBIsComp" &
        '"lete, [JBGrossProfit%] FROM VJobs WHERE (BRID = @BRID) AND (JBID = @JBID)"


        Me.SqlUpdateCommand1.CommandText = "UPDATE Jobs SET BRID = @BRID, ID = @ID, JBScheduledStartDate = @JBScheduledStartDate, JBClientFirstName = @JBClientFirstName,JBJobAddress=@JBJobAddress, JBPriceQuoted = @JBPriceQuoted, JBIsCancelled = @JBIsCancelled, JBIsComplete = @JBIsComplete WHERE (BRID = " &
        "@Original_BRID) AND (JBID = @Original_JBID); SELECT BRID, JBID, ID, JBScheduledS" &
        "tartDate, JBClientFirstName, JBJobAddress, " & " JBPrice,JBStatus, JBIsCancelled, JBIsComp" &
        "lete, JBJobStreetAddress01, [JBGrossProfit%] FROM VJobs WHERE (BRID = @BRID) AND (JBID = @JBID)"

        Me.SqlUpdateCommand1.Connection = Me.hSqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBScheduledStartDate", System.Data.SqlDbType.DateTime, 8, "JBScheduledStartDate"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBClientFirstName", System.Data.SqlDbType.VarChar, 102, "JBClientFirstName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBJobAddress", System.Data.SqlDbType.VarChar, 259, "JBJobAddress"))

        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBPriceQuoted", System.Data.SqlDbType.Money, 8, "JBPriceQuoted"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBIsCancelled", System.Data.SqlDbType.Bit, 1, "JBIsCancelled"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBIsComplete", System.Data.SqlDbType.Bit, 1, "JBIsComplete"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_JBID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "JBID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@JBID", System.Data.SqlDbType.BigInt, 8, "JBID"))
        '
        'DsGTMS
        '
        Me.DsGTMS.DataSetName = "dsGTMS"
        Me.DsGTMS.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.DataView
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'GridControl1.EmbeddedNavigator
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridControl1.Location = New System.Drawing.Point(0, 106)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemComboBox1})
        Me.GridControl1.Size = New System.Drawing.Size(632, 286)
        Me.GridControl1.Styles.AddReplace("CardBorder", New DevExpress.Utils.ViewStyleEx("CardBorder", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.InactiveBorder, System.Drawing.SystemColors.WindowFrame, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("BandPanelBackground", New DevExpress.Utils.ViewStyleEx("BandPanelBackground", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ControlDark, System.Drawing.Color.DarkSalmon, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("EmptySpace", New DevExpress.Utils.ViewStyleEx("EmptySpace", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("FieldValue", New DevExpress.Utils.ViewStyleEx("FieldValue", "CardView", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("BandPanel", New DevExpress.Utils.ViewStyleEx("BandPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("CardButton", New DevExpress.Utils.ViewStyleEx("CardButton", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("FocusedCardCaption", New DevExpress.Utils.ViewStyleEx("FocusedCardCaption", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ActiveCaption, System.Drawing.SystemColors.ActiveCaptionText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("CardCaption", New DevExpress.Utils.ViewStyleEx("CardCaption", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.InactiveCaption, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("HeaderPanelBackground", New DevExpress.Utils.ViewStyleEx("HeaderPanelBackground", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ControlDark, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("SeparatorLine", New DevExpress.Utils.ViewStyleEx("SeparatorLine", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ActiveBorder, System.Drawing.SystemColors.ActiveBorder, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("FieldCaption", New DevExpress.Utils.ViewStyleEx("FieldCaption", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Top, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'DataView
        '
        Me.DataView.Table = Me.DsGTMS.VJobs
        '
        'GridView1
        '
        Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
        Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
        Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.GridView1.Appearance.Row.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridView1.Appearance.Row.Options.UseFont = True
        Me.GridView1.Appearance.SelectedRow.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridView1.Appearance.SelectedRow.Options.UseFont = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID, Me.colJBClientName, Me.colJBJobAddress, Me.colJBScheduledStartDate, Me.colJBPrice, Me.colJBGrossProfitPercent, Me.colJBStatus})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        StyleFormatCondition1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Strikeout)
        StyleFormatCondition1.Appearance.ForeColor = System.Drawing.SystemColors.GrayText
        StyleFormatCondition1.Appearance.Options.UseFont = True
        StyleFormatCondition1.Appearance.Options.UseForeColor = True
        StyleFormatCondition1.ApplyToRow = True
        StyleFormatCondition1.Column = Me.colJBStatus
        StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal
        StyleFormatCondition1.Value1 = "Cancelled"
        Me.GridView1.FormatConditions.AddRange(New DevExpress.XtraGrid.StyleFormatCondition() {StyleFormatCondition1})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsNavigation.AutoFocusNewRow = True
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.OptionsView.ShowHorzLines = False
        Me.GridView1.OptionsView.ShowIndicator = False
        Me.GridView1.OptionsView.ShowVertLines = False
        Me.GridView1.RowHeight = 8
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colID, DevExpress.Data.ColumnSortOrder.Descending)})
        '
        'colID
        '
        Me.colID.Caption = "ID"
        Me.colID.FieldName = "ID"
        Me.colID.Name = "colID"
        Me.colID.Visible = True
        Me.colID.VisibleIndex = 0
        Me.colID.Width = 33
        '
        'colJBClientName
        '
        Me.colJBClientName.Caption = "Customer Name"
        Me.colJBClientName.FieldName = "JBClientFirstName"
        Me.colJBClientName.Name = "colJBClientName"
        Me.colJBClientName.Visible = True
        Me.colJBClientName.VisibleIndex = 1
        Me.colJBClientName.Width = 125
        '
        'colJBJobAddress
        '
        Me.colJBJobAddress.Caption = "Job Address"
        Me.colJBJobAddress.FieldName = "JBJobAddressMultiline"
        Me.colJBJobAddress.Name = "colJBJobAddress"
        Me.colJBJobAddress.Visible = True
        Me.colJBJobAddress.VisibleIndex = 2
        Me.colJBJobAddress.Width = 201
        '
        'colJBScheduledStartDate
        '
        Me.colJBScheduledStartDate.Caption = "Install Start Datejobs"
        Me.colJBScheduledStartDate.DisplayFormat.FormatString = "d"
        Me.colJBScheduledStartDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colJBScheduledStartDate.FieldName = "JBScheduledStartDate"
        Me.colJBScheduledStartDate.Name = "colJBScheduledStartDate"
        Me.colJBScheduledStartDate.Visible = True
        Me.colJBScheduledStartDate.VisibleIndex = 3
        Me.colJBScheduledStartDate.Width = 97
        '
        'colJBPrice
        '
        Me.colJBPrice.Caption = "Price"
        Me.colJBPrice.DisplayFormat.FormatString = "c"
        Me.colJBPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colJBPrice.FieldName = "JBPriceQuoted"
        Me.colJBPrice.Name = "colJBPrice"
        Me.colJBPrice.Visible = True
        Me.colJBPrice.VisibleIndex = 4
        Me.colJBPrice.Width = 89
        '
        'ImageListToolBar
        '
        Me.ImageListToolBar.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImageListToolBar.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageListToolBar.ImageStream = CType(resources.GetObject("ImageListToolBar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListToolBar.TransparentColor = System.Drawing.Color.Transparent
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.bStandard, Me.bAdvancedFilter})
        Me.BarManager1.Categories.AddRange(New DevExpress.XtraBars.BarManagerCategory() {New DevExpress.XtraBars.BarManagerCategory("Built-in Menus", New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd"), False), New DevExpress.XtraBars.BarManagerCategory("File", New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")), New DevExpress.XtraBars.BarManagerCategory("Reports", New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")), New DevExpress.XtraBars.BarManagerCategory("Forms", New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")), New DevExpress.XtraBars.BarManagerCategory("Help", New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")), New DevExpress.XtraBars.BarManagerCategory("Search", New System.Guid("a8792600-266d-4f26-9a37-6222d0f507f2"))})
        Me.BarManager1.Controller = Me.BarAndDockingController1
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Images = Me.ImageListToolBar
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnNew, Me.btnEdit, Me.btnCancel, Me.btnUncancel, Me.btnDelete, Me.subReportsOnSelectedItem, Me.btnAppliancesOrderForm, Me.btnOtherTradesOrderForm, Me.subFormsOnSelectedItem, Me.btnStockedMaterialinJobForm, Me.btnJobReport, Me.btnJobReportbyClient, Me.btnJobIncompleteReport, Me.btnHelp, Me.btnPrintPreview, Me.btnQuickPrint, Me.btnPrint, Me.btnClientSatisfactionAndPaymentForm, Me.btnClientSurveyForm, Me.subReportsOnSelectedItem2, Me.subFormsOnSelectedItem2, Me.beiFromDate, Me.beiToDate, Me.btnResetFilters, Me.btnMailMerge, Me.beiJBStatus, Me.btnTaxInvoice})
        Me.BarManager1.MainMenu = Me.bStandard
        Me.BarManager1.MaxItemId = 96
        Me.BarManager1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1, Me.RepositoryItemDateEdit2, Me.RepositoryItemPopupContainerEdit1})
        '
        'bStandard
        '
        Me.bStandard.BarName = "Standard Toolbar"
        Me.bStandard.DockCol = 0
        Me.bStandard.DockRow = 0
        Me.bStandard.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bStandard.FloatLocation = New System.Drawing.Point(44, 188)
        Me.bStandard.FloatSize = New System.Drawing.Size(659, 24)
        Me.bStandard.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnEdit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnCancel, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(Me.subReportsOnSelectedItem, True), New DevExpress.XtraBars.LinkPersistInfo(Me.subFormsOnSelectedItem, True), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnQuickPrint, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnPrintPreview, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnMailMerge, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnHelp, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.bStandard.OptionsBar.AllowQuickCustomization = False
        Me.bStandard.OptionsBar.DisableClose = True
        Me.bStandard.OptionsBar.DrawDragBorder = False
        Me.bStandard.OptionsBar.MultiLine = True
        Me.bStandard.OptionsBar.UseWholeRow = True
        Me.bStandard.Text = "Standard Toolbar"
        '
        'btnEdit
        '
        Me.btnEdit.Caption = "&Edit..."
        Me.btnEdit.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnEdit.Id = 20
        Me.btnEdit.ImageIndex = 1
        Me.btnEdit.ImageIndexDisabled = 1
        Me.btnEdit.Name = "btnEdit"
        '
        'btnCancel
        '
        Me.btnCancel.Caption = "&Cancel"
        Me.btnCancel.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnCancel.Id = 89
        Me.btnCancel.ImageIndex = 2
        Me.btnCancel.ImageIndexDisabled = 2
        Me.btnCancel.Name = "btnCancel"
        '
        'subReportsOnSelectedItem
        '
        Me.subReportsOnSelectedItem.Caption = "Reports on Selected Job"
        Me.subReportsOnSelectedItem.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.subReportsOnSelectedItem.Id = 78
        Me.subReportsOnSelectedItem.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnAppliancesOrderForm), New DevExpress.XtraBars.LinkPersistInfo(Me.btnOtherTradesOrderForm), New DevExpress.XtraBars.LinkPersistInfo(Me.btnTaxInvoice, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnJobReport, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnJobReportbyClient), New DevExpress.XtraBars.LinkPersistInfo(Me.btnJobIncompleteReport)})
        Me.subReportsOnSelectedItem.Name = "subReportsOnSelectedItem"
        '
        'btnAppliancesOrderForm
        '
        Me.btnAppliancesOrderForm.Caption = "&Appliances Order Form"
        Me.btnAppliancesOrderForm.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnAppliancesOrderForm.Id = 60
        Me.btnAppliancesOrderForm.Name = "btnAppliancesOrderForm"
        '
        'btnOtherTradesOrderForm
        '
        Me.btnOtherTradesOrderForm.Caption = "&Other Trades Order Form"
        Me.btnOtherTradesOrderForm.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnOtherTradesOrderForm.Id = 61
        Me.btnOtherTradesOrderForm.Name = "btnOtherTradesOrderForm"
        '
        'btnTaxInvoice
        '
        Me.btnTaxInvoice.Caption = "&Tax Invoice"
        Me.btnTaxInvoice.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnTaxInvoice.Id = 95
        Me.btnTaxInvoice.Name = "btnTaxInvoice"
        '
        'btnJobReport
        '
        Me.btnJobReport.Caption = "&Job Report"
        Me.btnJobReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnJobReport.Id = 57
        Me.btnJobReport.Name = "btnJobReport"
        '
        'btnJobReportbyClient
        '
        Me.btnJobReportbyClient.Caption = "Job Report by &Customer"
        Me.btnJobReportbyClient.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnJobReportbyClient.Id = 58
        Me.btnJobReportbyClient.Name = "btnJobReportbyClient"
        Me.btnJobReportbyClient.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInCustomizing
        '
        'btnJobIncompleteReport
        '
        Me.btnJobIncompleteReport.Caption = "Job &Incomplete Report"
        Me.btnJobIncompleteReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnJobIncompleteReport.Id = 59
        Me.btnJobIncompleteReport.Name = "btnJobIncompleteReport"
        '
        'subFormsOnSelectedItem
        '
        Me.subFormsOnSelectedItem.Caption = "Forms on Selected Job"
        Me.subFormsOnSelectedItem.CategoryGuid = New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")
        Me.subFormsOnSelectedItem.Id = 79
        Me.subFormsOnSelectedItem.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnStockedMaterialinJobForm), New DevExpress.XtraBars.LinkPersistInfo(Me.btnClientSatisfactionAndPaymentForm), New DevExpress.XtraBars.LinkPersistInfo(Me.btnClientSurveyForm)})
        Me.subFormsOnSelectedItem.Name = "subFormsOnSelectedItem"
        '
        'btnStockedMaterialinJobForm
        '
        Me.btnStockedMaterialinJobForm.Caption = "&Tracked Material in Job Form"
        Me.btnStockedMaterialinJobForm.CategoryGuid = New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")
        Me.btnStockedMaterialinJobForm.Id = 62
        Me.btnStockedMaterialinJobForm.Name = "btnStockedMaterialinJobForm"
        '
        'btnClientSatisfactionAndPaymentForm
        '
        Me.btnClientSatisfactionAndPaymentForm.Caption = "Customer Satisfaction and Payment Form"
        Me.btnClientSatisfactionAndPaymentForm.CategoryGuid = New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")
        Me.btnClientSatisfactionAndPaymentForm.Id = 80
        Me.btnClientSatisfactionAndPaymentForm.Name = "btnClientSatisfactionAndPaymentForm"
        '
        'btnClientSurveyForm
        '
        Me.btnClientSurveyForm.Caption = "Customer Survey Form"
        Me.btnClientSurveyForm.CategoryGuid = New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")
        Me.btnClientSurveyForm.Id = 81
        Me.btnClientSurveyForm.Name = "btnClientSurveyForm"
        '
        'btnQuickPrint
        '
        Me.btnQuickPrint.Caption = "Print"
        Me.btnQuickPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnQuickPrint.Id = 46
        Me.btnQuickPrint.ImageIndex = 11
        Me.btnQuickPrint.ImageIndexDisabled = 11
        Me.btnQuickPrint.Name = "btnQuickPrint"
        '
        'btnPrintPreview
        '
        Me.btnPrintPreview.Caption = "Print Pre&view"
        Me.btnPrintPreview.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrintPreview.Id = 44
        Me.btnPrintPreview.ImageIndex = 10
        Me.btnPrintPreview.ImageIndexDisabled = 10
        Me.btnPrintPreview.Name = "btnPrintPreview"
        '
        'btnMailMerge
        '
        Me.btnMailMerge.Caption = "&Mail Merge"
        Me.btnMailMerge.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnMailMerge.Id = 93
        Me.btnMailMerge.ImageIndex = 14
        Me.btnMailMerge.ImageIndexDisabled = 14
        Me.btnMailMerge.Name = "btnMailMerge"
        '
        'btnHelp
        '
        Me.btnHelp.Caption = "&Help"
        Me.btnHelp.CategoryGuid = New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")
        Me.btnHelp.Id = 68
        Me.btnHelp.ImageIndex = 4
        Me.btnHelp.ImageIndexDisabled = 4
        Me.btnHelp.Name = "btnHelp"
        '
        'bAdvancedFilter
        '
        Me.bAdvancedFilter.BarName = "Search Bar"
        Me.bAdvancedFilter.DockCol = 0
        Me.bAdvancedFilter.DockRow = 1
        Me.bAdvancedFilter.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bAdvancedFilter.FloatLocation = New System.Drawing.Point(8, 155)
        Me.bAdvancedFilter.FloatSize = New System.Drawing.Size(688, 24)
        Me.bAdvancedFilter.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.beiFromDate, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.beiToDate, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.beiJBStatus, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnResetFilters, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.bAdvancedFilter.OptionsBar.AllowQuickCustomization = False
        Me.bAdvancedFilter.OptionsBar.DisableClose = True
        Me.bAdvancedFilter.OptionsBar.MultiLine = True
        Me.bAdvancedFilter.Text = "Search Bar"
        '
        'beiFromDate
        '
        Me.beiFromDate.Caption = "Search for Install Start Dates from:"
        Me.beiFromDate.CategoryGuid = New System.Guid("a8792600-266d-4f26-9a37-6222d0f507f2")
        Me.beiFromDate.Edit = Me.RepositoryItemDateEdit1
        Me.beiFromDate.Id = 90
        Me.beiFromDate.Name = "beiFromDate"
        Me.beiFromDate.Width = 150
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'beiToDate
        '
        Me.beiToDate.Caption = "to:"
        Me.beiToDate.CategoryGuid = New System.Guid("a8792600-266d-4f26-9a37-6222d0f507f2")
        Me.beiToDate.Edit = Me.RepositoryItemDateEdit2
        Me.beiToDate.Id = 91
        Me.beiToDate.Name = "beiToDate"
        Me.beiToDate.Width = 150
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'beiJBStatus
        '
        Me.beiJBStatus.Caption = "Search by Status:"
        Me.beiJBStatus.CategoryGuid = New System.Guid("a8792600-266d-4f26-9a37-6222d0f507f2")
        Me.beiJBStatus.Edit = Me.RepositoryItemPopupContainerEdit1
        Me.beiJBStatus.Id = 94
        Me.beiJBStatus.Name = "beiJBStatus"
        Me.beiJBStatus.Width = 150
        '
        'RepositoryItemPopupContainerEdit1
        '
        Me.RepositoryItemPopupContainerEdit1.AutoHeight = False
        Me.RepositoryItemPopupContainerEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemPopupContainerEdit1.Name = "RepositoryItemPopupContainerEdit1"
        Me.RepositoryItemPopupContainerEdit1.PopupControl = Me.pccJBStatus
        Me.RepositoryItemPopupContainerEdit1.PopupSizeable = False
        Me.RepositoryItemPopupContainerEdit1.ShowPopupCloseButton = False
        '
        'pccJBStatus
        '
        Me.pccJBStatus.Controls.Add(Me.clbJBStatus)
        Me.pccJBStatus.Location = New System.Drawing.Point(168, 216)
        Me.pccJBStatus.Name = "pccJBStatus"
        Me.pccJBStatus.Size = New System.Drawing.Size(152, 56)
        Me.pccJBStatus.TabIndex = 4
        Me.pccJBStatus.Text = "PopupContainerControl1"
        '
        'clbJBStatus
        '
        Me.clbJBStatus.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.clbJBStatus.Dock = System.Windows.Forms.DockStyle.Fill
        Me.clbJBStatus.Items.AddRange(New DevExpress.XtraEditors.Controls.CheckedListBoxItem() {New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Data Incomplete", System.Windows.Forms.CheckState.Checked), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Complete", System.Windows.Forms.CheckState.Checked), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Cancelled", System.Windows.Forms.CheckState.Checked)})
        Me.clbJBStatus.Location = New System.Drawing.Point(0, 0)
        Me.clbJBStatus.Name = "clbJBStatus"
        Me.clbJBStatus.Size = New System.Drawing.Size(152, 56)
        Me.clbJBStatus.TabIndex = 0
        '
        'btnResetFilters
        '
        Me.btnResetFilters.Caption = "&Reset Filters"
        Me.btnResetFilters.CategoryGuid = New System.Guid("a8792600-266d-4f26-9a37-6222d0f507f2")
        Me.btnResetFilters.Id = 92
        Me.btnResetFilters.ImageIndex = 13
        Me.btnResetFilters.ImageIndexDisabled = 13
        Me.btnResetFilters.Name = "btnResetFilters"
        '
        'BarAndDockingController1
        '
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseForeColor = True
        Me.BarAndDockingController1.PaintStyleName = "OfficeXP"
        '
        'btnNew
        '
        Me.btnNew.Caption = "&New..."
        Me.btnNew.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnNew.Enabled = False
        Me.btnNew.Id = 19
        Me.btnNew.ImageIndex = 0
        Me.btnNew.ImageIndexDisabled = 0
        Me.btnNew.Name = "btnNew"
        '
        'btnUncancel
        '
        Me.btnUncancel.Caption = "&Uncancel"
        Me.btnUncancel.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnUncancel.Id = 86
        Me.btnUncancel.Name = "btnUncancel"
        '
        'btnDelete
        '
        Me.btnDelete.Caption = "&Delete"
        Me.btnDelete.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnDelete.Id = 23
        Me.btnDelete.ImageIndex = 12
        Me.btnDelete.ImageIndexDisabled = 12
        Me.btnDelete.Name = "btnDelete"
        '
        'btnPrint
        '
        Me.btnPrint.Caption = "&Print..."
        Me.btnPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrint.Id = 43
        Me.btnPrint.ImageIndex = 11
        Me.btnPrint.ImageIndexDisabled = 11
        Me.btnPrint.Name = "btnPrint"
        '
        'subReportsOnSelectedItem2
        '
        Me.subReportsOnSelectedItem2.Caption = "&Reports on Selected Job"
        Me.subReportsOnSelectedItem2.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subReportsOnSelectedItem2.Id = 87
        Me.subReportsOnSelectedItem2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnAppliancesOrderForm), New DevExpress.XtraBars.LinkPersistInfo(Me.btnOtherTradesOrderForm), New DevExpress.XtraBars.LinkPersistInfo(Me.btnTaxInvoice, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnJobReport, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnJobReportbyClient), New DevExpress.XtraBars.LinkPersistInfo(Me.btnJobIncompleteReport)})
        Me.subReportsOnSelectedItem2.Name = "subReportsOnSelectedItem2"
        '
        'subFormsOnSelectedItem2
        '
        Me.subFormsOnSelectedItem2.Caption = "&Forms on Selected Job"
        Me.subFormsOnSelectedItem2.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subFormsOnSelectedItem2.Id = 88
        Me.subFormsOnSelectedItem2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnStockedMaterialinJobForm), New DevExpress.XtraBars.LinkPersistInfo(Me.btnClientSatisfactionAndPaymentForm), New DevExpress.XtraBars.LinkPersistInfo(Me.btnClientSurveyForm)})
        Me.subFormsOnSelectedItem2.Name = "subFormsOnSelectedItem2"
        '
        'PopupMenu1
        '
        Me.PopupMenu1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.subReportsOnSelectedItem), New DevExpress.XtraBars.LinkPersistInfo(Me.subFormsOnSelectedItem), New DevExpress.XtraBars.LinkPersistInfo(Me.btnEdit, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnCancel), New DevExpress.XtraBars.LinkPersistInfo(Me.btnDelete), New DevExpress.XtraBars.LinkPersistInfo(Me.btnUncancel, True)})
        Me.PopupMenu1.Manager = Me.BarManager1
        Me.PopupMenu1.Name = "PopupMenu1"
        '
        'colJBGrossProfitPercent
        '
        Me.colJBGrossProfitPercent.Caption = "Gross %"
        Me.colJBGrossProfitPercent.DisplayFormat.FormatString = "p"
        Me.colJBGrossProfitPercent.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colJBGrossProfitPercent.FieldName = "JBGrossProfit%"
        Me.colJBGrossProfitPercent.Name = "colJBGrossProfitPercent"
        Me.colJBGrossProfitPercent.Visible = True
        Me.colJBGrossProfitPercent.VisibleIndex = 5
        '
        'lvJobs2
        '
        Me.Controls.Add(Me.pccJBStatus)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "lvJobs2"
        Me.Size = New System.Drawing.Size(632, 392)
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemPopupContainerEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pccJBStatus, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pccJBStatus.ResumeLayout(False)
        CType(Me.clbJBStatus, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return hSqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            hSqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private Sub EnableDisable()
        btnNew.Enabled = AllowNew
        btnEdit.Enabled = AllowEdit
        btnCancel.Enabled = AllowCancel
        btnUncancel.Enabled = AllowUncancel
        btnDelete.Enabled = AllowDelete
        If Not HasCommand("btnJobReport") Then
            Me.GridView1.Columns.Remove(Me.colJBGrossProfitPercent)
        End If
    End Sub

    Public Function FillDataSet() As Integer Implements IListControl.FillDataSet
        Dim cursor As System.Windows.Forms.Cursor = ExplorerForm.Cursor
        ExplorerForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
        ' Open connection
        Dim trans As SqlClient.SqlTransaction = Connection.BeginTransaction(IsolationLevel.ReadUncommitted)
        Power.Library.Library.ApplyTransactionToAllDataAdapters(trans, Me)
        'Me.GridControl1.DataSource.Clear()
        FillDataSet = SqlDataAdapter.Fill(DsGTMS)
        'Close connecton
        trans.Commit()
        ExplorerForm.Cursor = cursor
    End Function

#Region " Filters "

    Private Sub SetUpFilters()
        ' Set Up Default Advanced Filter
        beiFromDate.EditValue = DateAdd(DateInterval.Month, -6, Today.Date)
        beiToDate.EditValue = Nothing
        UpdateDateFilter()
        CheckAllStatuses()
        GridView1.ClearColumnsFilter()

        UpdateStatusCaption()
        UpdateAdvancedFilter()
    End Sub

    Private Sub DateFilter_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles beiFromDate.EditValueChanged, beiToDate.EditValueChanged
        UpdateDateFilter()
    End Sub

    Private DateFilterIsListening As Boolean = False ' This is so this doesn't trigger during load (this will happen in code)
    Private Sub UpdateDateFilter()
        Me.SqlDataAdapter.SelectCommand.Parameters("@FROM_DATE").Value = IsNull(beiFromDate.EditValue, DBNull.Value)
        Me.SqlDataAdapter.SelectCommand.Parameters("@TO_DATE").Value = IsNull(beiToDate.EditValue, DBNull.Value)
        If DateFilterIsListening Then
            Me.DsGTMS.Tables(Me.SqlDataAdapter.TableMappings(0).DataSetTable).Clear()
            FillDataSet()
        End If
    End Sub

    Private Sub btnResetFilters_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnResetFilters.ItemClick
        DateFilterIsListening = False
        SetUpFilters()
        Me.DsGTMS.Tables(Me.SqlDataAdapter.TableMappings(0).DataSetTable).Clear()
        FillDataSet()
        DateFilterIsListening = True
        GridView1.ClearColumnsFilter()
    End Sub

    Private Sub UpdateAdvancedFilter()
        DataView.RowFilter = StatusFilter()
    End Sub

    Private Sub RepositoryItemPopupContainerEdit1_QueryResultValue(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles RepositoryItemPopupContainerEdit1.QueryResultValue
        UpdateStatusCaption()
        UpdateAdvancedFilter()
    End Sub

    Private Sub UpdateStatusCaption()
        If AllStatusesTicked() Then
            beiJBStatus.EditValue = "(All)"
        ElseIf NoStatusesTicked() Then
            beiJBStatus.EditValue = "(None)"
        Else
            beiJBStatus.EditValue = StatusesTicked()
        End If
    End Sub

    Private Function StatusFilter() As String
        If AllStatusesTicked() Then Return ""
        Dim filter As String = ""
        For Each status As DevExpress.XtraEditors.Controls.CheckedListBoxItem In clbJBStatus.Items
            If status.CheckState = CheckState.Checked Then
                filter = AddCondition(filter, "[JBStatus] = '" & status.Value & "'", "OR")
            End If
        Next
        If filter.Length = 0 Then
            filter = "False" ' this means there were none selected
        Else
            filter = "(" & filter & ")" ' Add brackets because of the OR's
        End If
        Return filter
    End Function

    Private Function AllStatusesTicked() As Boolean
        For Each status As DevExpress.XtraEditors.Controls.CheckedListBoxItem In clbJBStatus.Items
            If Not (status.CheckState = CheckState.Checked) Then
                Return False
            End If
        Next
        Return True
    End Function

    Private Function NoStatusesTicked() As Boolean
        For Each status As DevExpress.XtraEditors.Controls.CheckedListBoxItem In clbJBStatus.Items
            If Not (status.CheckState = CheckState.Unchecked) Then
                Return False
            End If
        Next
        Return True
    End Function

    Private Function StatusesTicked() As String
        Dim str As String = ""
        For Each status As DevExpress.XtraEditors.Controls.CheckedListBoxItem In clbJBStatus.Items
            If status.CheckState = CheckState.Checked Then
                str = AddStatus(str, status.Value)
            End If
        Next
        Return str
    End Function

    Private Function AddStatus(ByVal existingStatuses As String, ByVal status As String) As String
        If existingStatuses.Length = 0 Then
            Return status
        Else
            Return existingStatuses & ", " & status
        End If
    End Function

    Private Sub CheckAllStatuses()
        For Each status As DevExpress.XtraEditors.Controls.CheckedListBoxItem In clbJBStatus.Items
            status.CheckState = CheckState.Checked
        Next
    End Sub

#End Region

    Private Sub GridView1_RowCountChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.RowCountChanged
        ExplorerForm.UpdateNumItems(NumItems)
        ExplorerForm.UpdateVisibleItems(VisibleItems)
    End Sub

    Public ReadOnly Property VisibleItems() As Integer Implements Power.Library.IListControl.VisibleItems
        Get
            Return GridView1.RowCount
        End Get
    End Property

    Public ReadOnly Property NumItems() As Integer Implements Power.Library.IListControl.NumItems
        Get
            Return DsGTMS.Tables(SqlDataAdapter.TableMappings(0).DataSetTable).Rows.Count
        End Get
    End Property

    Private hBRID As Integer
    Private Property BRID() As Integer
        Get
            Return hBRID
        End Get
        Set(ByVal Value As Integer)
            hBRID = Value
            SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = Value
        End Set
    End Property

    Public ReadOnly Property SelectedRow() As DataRow
        Get
            If Not GridView1.GetSelectedRows Is Nothing Then
                Return GridView1.GetDataRow(GridView1.GetSelectedRows(0))
            End If
        End Get
    End Property

    Public ReadOnly Property SelectedRowField(ByVal Field As String) As Object
        Get
            If Not SelectedRow Is Nothing Then
                Return SelectedRow.Item(Field)
            Else
                Return DBNull.Value
            End If
        End Get
    End Property

    Public Function SelectRow(ByVal BRID As Int32, ByVal JBID As Int64) As Boolean
        Dim i As Integer = 0
        Do Until False
            Try
                If GridView1.GetDataRow(GridView1.GetVisibleRowHandle(i))("BRID") = BRID And _
                        GridView1.GetDataRow(GridView1.GetVisibleRowHandle(i))("JBID") = JBID Then
                    GridView1.ClearSelection()
                    GridView1.SelectRow(GridView1.GetVisibleRowHandle(i))
                    Return True
                End If
                i += 1
            Catch ex As NullReferenceException
                Return False
            End Try
        Loop
        Return False
    End Function

    Private hExplorerForm As Form
    Public ReadOnly Property ExplorerForm() As frmExplorer
        Get
            Return hExplorerForm
        End Get
    End Property

#Region " New, Open and Delete "

    Private Sub GridControl1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridControl1.DoubleClick
        List_Edit()
    End Sub

    Private Sub btnNew_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnNew.ItemClick
        List_New()
    End Sub

    Public Sub List_New() Implements IListControl.List_New
        Exit Sub
    End Sub

    Private Sub btnEdit_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnEdit.ItemClick
        List_Edit()
    End Sub

    Public Sub List_Edit() Implements IListControl.List_Edit
        If Not SelectedRow Is Nothing Then
            Dim c As Cursor = ParentForm.Cursor
            ParentForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If JobExists(SelectedRowField("BRID"), SelectedRowField("JBID")) Then
                frmJob2.Edit(SelectedRowField("BRID"), SelectedRowField("JBID"))
                FillDataSet()
            Else
                Message.AlreadyDeleted("job", Message.ObjectAction.Edit)
                If TypeOf Me.GridControl1.DataSource Is DataView Then
                    CType(Me.GridControl1.DataSource, DataView).Table.Clear()
                ElseIf TypeOf Me.GridControl1.DataSource Is DataSet Then
                    CType(Me.GridControl1.DataSource, DataSet).Clear()
                End If
                FillDataSet()
            End If
            ParentForm.Cursor = c
        End If
    End Sub

    Private Sub btnCancel_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnCancel.ItemClick
        List_Cancel()
    End Sub

    Public Sub List_Cancel() Implements Power.Library.IListControl.List_Cancel
        If Not SelectedRow Is Nothing Then
            Dim BRID As Integer = SelectedRowField("BRID")
            Dim JBID As Int64 = SelectedRowField("JBID")
            If Message.AskDeleteObject("job", Message.ObjectAction.Cancel) = MsgBoxResult.Yes Then
                If JobExists(BRID, JBID) Then
                    If DataAccess.spExecLockRequest("sp_GetJobLock", BRID, JBID, Connection) Then
                        SelectedRow.Item("JBIsCancelled") = True
                        SqlDataAdapter.Update(DsGTMS)
                        DataAccess.spExecLockRequest("sp_ReleaseJobLock", BRID, JBID, Connection)
                    Else
                        Message.CurrentlyAccessed("job")
                    End If
                Else
                    Message.AlreadyDeleted("job", Message.ObjectAction.Cancel)
                    If TypeOf Me.GridControl1.DataSource Is DataView Then
                        CType(Me.GridControl1.DataSource, DataView).Table.Clear()
                    ElseIf TypeOf Me.GridControl1.DataSource Is DataSet Then
                        CType(Me.GridControl1.DataSource, DataSet).Clear()
                    End If
                    FillDataSet()
                End If
            End If
        End If
    End Sub

    Private Sub btnUncancel_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnUncancel.ItemClick
        List_Uncancel()
    End Sub

    Public Sub List_Uncancel() Implements Power.Library.IListControl.List_Uncancel
        If Not SelectedRow Is Nothing Then
            Dim BRID As Integer = SelectedRowField("BRID")
            Dim JBID As Int64 = SelectedRowField("JBID")
            If JobExists(BRID, JBID) Then
                If DataAccess.spExecLockRequest("sp_GetJobLock", BRID, JBID, Connection) Then
                    SelectedRow.Item("JBIsCancelled") = False
                    SqlDataAdapter.Update(DsGTMS)
                    DataAccess.spExecLockRequest("sp_ReleaseJobLock", BRID, JBID, Connection)
                Else
                    Message.CurrentlyAccessed("job")
                End If
            Else
                Message.AlreadyDeleted("job", Message.ObjectAction.Edit)
                If TypeOf Me.GridControl1.DataSource Is DataView Then
                    CType(Me.GridControl1.DataSource, DataView).Table.Clear()
                ElseIf TypeOf Me.GridControl1.DataSource Is DataSet Then
                    CType(Me.GridControl1.DataSource, DataSet).Clear()
                End If
                FillDataSet()
            End If
        End If
    End Sub

    Private Sub btnDelete_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnDelete.ItemClick
        List_Delete()
    End Sub

    Public Sub List_Delete() Implements IListControl.List_Delete
        If Not SelectedRow Is Nothing Then
            If Message.AskDeleteObject("job", Message.ObjectAction.Delete) = MsgBoxResult.Yes Then
                Dim BRID As Integer = SelectedRowField("BRID")
                Dim JBID As Int64 = SelectedRowField("JBID")
                If JobExists(SelectedRowField("BRID"), JBID) Then
                    If DataAccess.spExecLockRequest("sp_GetJobLock", BRID, JBID, Connection) Then
                        SelectedRow.Delete()
                        SqlDataAdapter.Update(DsGTMS)
                        DataAccess.spExecLockRequest("sp_ReleaseJobLock", BRID, JBID, Connection)
                    Else
                        Message.CurrentlyAccessed("job")
                    End If
                Else
                    If TypeOf Me.GridControl1.DataSource Is DataView Then
                        CType(Me.GridControl1.DataSource, DataView).Table.Clear()
                    ElseIf TypeOf Me.GridControl1.DataSource Is DataSet Then
                        CType(Me.GridControl1.DataSource, DataSet).Clear()
                    End If
                    FillDataSet()
                End If
            End If
        End If
    End Sub

    Private Function JobExists(ByVal BRID As Int32, ByVal JBID As Int64) As Boolean
        Dim cnn As SqlClient.SqlConnection = Connection
        Dim cmd As New SqlClient.SqlCommand("SELECT Count(ID) FROM Jobs WHERE BRID = @BRID AND JBID = @JBID", cnn)
        cmd.CommandType = CommandType.Text
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@JBID", JBID)
        Return cmd.ExecuteScalar > 0
    End Function

#End Region

#Region " Allow Methods "

    Public ReadOnly Property AllowNew() As Boolean Implements Power.Library.IListControl.AllowNew
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property AllowEdit() As Boolean Implements Power.Library.IListControl.AllowEdit
        Get
            Return True
        End Get
    End Property

    Public ReadOnly Property AllowCancel() As Boolean Implements Power.Library.IListControl.AllowCancel
        Get
            Return Not HasRole("branch_read_only")
        End Get
    End Property

    Public ReadOnly Property AllowUncancel() As Boolean Implements Power.Library.IListControl.AllowUncancel
        Get
            Return Not HasRole("branch_read_only")
        End Get
    End Property

    Public ReadOnly Property AllowDelete() As Boolean Implements Power.Library.IListControl.AllowDelete
        Get
            Return HasRole("full")
        End Get
    End Property

#End Region

#Region " Reports "

    Public ReadOnly Property SelectedItemForms() As DevExpress.XtraBars.BarItem Implements ISelectedItemReports.SelectedItemForms
        Get
            Return subFormsOnSelectedItem2
        End Get
    End Property

    Public ReadOnly Property SelectedItemReports() As DevExpress.XtraBars.BarItem Implements ISelectedItemReports.SelectedItemReports
        Get
            Return subReportsOnSelectedItem2
        End Get
    End Property

    Private Sub btnJobReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnJobReport.ItemClick
        If Not SelectedRow Is Nothing Then
            If SelectedRowField("JBIsComplete") Then
                Me.Cursor = Windows.Forms.Cursors.WaitCursor
                Dim cmd As SqlClient.SqlCommand
                Dim da As SqlClient.SqlDataAdapter
                Dim gui As New frmReport(Nothing)
                Dim rep As New repJobReport
                rep.Load()
                Dim ds As New dsReports

                sp_UpdateJobStatistics(SelectedRowField("BRID"), SelectedRowField("JBID"), Connection)

                cmd = New SqlClient.SqlCommand("SELECT * FROM dbo.[repJobReport](@BRID, @JBID)", Connection)
                cmd.Parameters.Add("@BRID", SelectedRowField("BRID"))
                cmd.Parameters.Add("@JBID", SelectedRowField("JBID"))
                da = New SqlClient.SqlDataAdapter(cmd)
                ReadUncommittedFill(da, ds.repJobReport)

                cmd = New SqlClient.SqlCommand("SELECT * FROM VJobs_Expenses WHERE BRID = @BRID AND JBID = @JBID", Connection)
                cmd.Parameters.Add("@BRID", SelectedRowField("BRID"))
                cmd.Parameters.Add("@JBID", SelectedRowField("JBID"))
                da = New SqlClient.SqlDataAdapter(cmd)
                ReadUncommittedFill(da, ds.VJobs_Expenses)

                rep.SetDataSource(ds)
                'rep.SetParameterValue("Filename", MyBusinessPlan.Filename)
                gui.Display(rep)
                Me.Cursor = Windows.Forms.Cursors.Default
                gui.Text = "Job Report"
                gui.Show()
            Else
                Message.ShowMessage("A job report cannot be produced on an incomplete job.", MessageBoxIcon.Exclamation)
            End If
        End If
    End Sub

    Private Sub btnJobIncompleteReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnJobIncompleteReport.ItemClick
        If Not SelectedRow Is Nothing Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repJobIncompleteReport
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repJobIncompleteReport]( @BRID, @JBID)", Connection)
            cmd.Parameters.Add("@BRID", SelectedRowField("BRID"))
            cmd.Parameters.Add("@JBID", SelectedRowField("JBID"))
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repJobIncompleteReport)
            rep.SetDataSource(ds)
            'rep.SetParameterValue("Filename", MyBusinessPlan.Filename)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Job Incomplete Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnJobReportbyClient_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnJobReportbyClient.ItemClick
        If Not SelectedRow Is Nothing Then
            Dim DateRangeGui As New frmDateRange(Connection)
            If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
                Me.Cursor = Windows.Forms.Cursors.WaitCursor
                Dim gui As New frmReport(Nothing)
                Dim rep As New repClientJobsReport
                rep.Load()
                Dim ds As New dsReports
                Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repClientJobsReport](@BRID, @FROM_DATE, @TO_DATE, @CLIENT_NAME)", Connection)
                cmd.Parameters.Add("@BRID", BRID)
                cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
                cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
                cmd.Parameters.Add("@CLIENT_NAME", SelectedRowField("JBClientFirstName"))
                Dim da As New SqlClient.SqlDataAdapter(cmd)
                ReadUncommittedFill(da, ds.repAllJobsReport)
                rep.SetDataSource(ds)
                rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
                rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
                rep.SetParameterValue("ClientName", SelectedRowField("JBClientFirstName"))
                gui.Display(rep)
                Me.Cursor = Windows.Forms.Cursors.Default
                gui.Text = "Job Report by Customer"
                gui.Show()
            End If
        End If
    End Sub

    Private Sub btnAppliancesOrderForm_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnAppliancesOrderForm.ItemClick
        If Not SelectedRow Is Nothing Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repOrderFormAppliances
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repOrderFormNonCoreSalesItems](@BRID, @JBID) WHERE NIType = 'AP'", Connection)
            cmd.Parameters.Add("@BRID", SelectedRowField("BRID"))
            cmd.Parameters.Add("@JBID", SelectedRowField("JBID"))
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repOrderFormNonCoreSalesItems)
            rep.SetDataSource(ds)
            'rep.SetParameterValue("Filename", MyBusinessPlan.Filename)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Appliances Order Form"
            gui.Show()
        End If
    End Sub

    Private Sub btnOtherTradesOrderForm_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnOtherTradesOrderForm.ItemClick
        If Not SelectedRow Is Nothing Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repOrderFormOtherTrades
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repOrderFormNonCoreSalesItems](@BRID, @JBID) WHERE NIType = 'OT'", Connection)
            cmd.Parameters.Add("@BRID", SelectedRowField("BRID"))
            cmd.Parameters.Add("@JBID", SelectedRowField("JBID"))
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            da.Fill(ds.repOrderFormNonCoreSalesItems)
            rep.SetDataSource(ds)
            'rep.SetParameterValue("Filename", MyBusinessPlan.Filename)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Other Trades Order Form"
            gui.Show()
        End If
    End Sub

    Private Sub btnStockedMaterialinJobForm_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnStockedMaterialinJobForm.ItemClick
        If Not SelectedRow Is Nothing Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repStockedMaterialInJobForm
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[Jobs] WHERE BRID = @BRID AND JBID = @JBID", Connection)
            cmd.Parameters.Add("@BRID", SelectedRowField("BRID"))
            cmd.Parameters.Add("@JBID", SelectedRowField("JBID"))
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.Jobs)
            cmd.CommandText = "SELECT * FROM dbo.[subJobs_Materials](@BRID, @JBID)"
            ReadUncommittedFill(da, ds.subJobs_Materials)
            cmd.CommandText = "SELECT * FROM dbo.[subJobs_StockedItems](@BRID, @JBID)"
            ReadUncommittedFill(da, ds.subJobs_StockedItems)
            rep.SetDataSource(ds)
            'rep.SetParameterValue("Filename", MyBusinessPlan.Filename)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Tracked Material in Job Form"
            gui.Show()
        End If
    End Sub

    Private Sub btnClientSatisfactionAndPaymentForm_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnClientSatisfactionAndPaymentForm.ItemClick
        If Not SelectedRow Is Nothing Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repStaticSatisfactionAndPayment
            rep.Load()
            Dim ds As New dsForms
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[Jobs] WHERE BRID = @BRID AND JBID = @JBID", Connection)
            cmd.Parameters.Add("@BRID", SelectedRowField("BRID"))
            cmd.Parameters.Add("@JBID", SelectedRowField("JBID"))
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.Jobs)
            cmd = New SqlClient.SqlCommand("SELECT * FROM dbo.[JobStatistics] WHERE BRID = @BRID AND JBID = @JBID", Connection)
            cmd.Parameters.Add("@BRID", SelectedRowField("BRID"))
            cmd.Parameters.Add("@JBID", SelectedRowField("JBID"))
            da = New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.JobStatistics)
            cmd = New SqlClient.SqlCommand("SELECT * FROM dbo.[VBranches] WHERE BRID = @BRID", Connection)
            cmd.Parameters.Add("@BRID", SelectedRowField("BRID"))
            da = New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.VBranches)
            rep.SetDataSource(ds)
            'rep.SetParameterValue("Filename", MyBusinessPlan.Filename)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Customer Satisfaction and Payment Form"
            gui.Show()
        End If
    End Sub

    Private Sub btnClientSurveyForm_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnClientSurveyForm.ItemClick
        If Not SelectedRow Is Nothing Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repStaticClientSurvey
            rep.Load()
            Dim ds As New dsForms
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[Jobs] WHERE BRID = @BRID AND JBID = @JBID", Connection)
            cmd.Parameters.Add("@BRID", SelectedRowField("BRID"))
            cmd.Parameters.Add("@JBID", SelectedRowField("JBID"))
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.Jobs)
            cmd = New SqlClient.SqlCommand("SELECT * FROM dbo.[VBranches] WHERE BRID = @BRID", Connection)
            cmd.Parameters.Add("@BRID", SelectedRowField("BRID"))
            da = New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.VBranches)
            rep.SetDataSource(ds)
            'rep.SetParameterValue("Filename", MyBusinessPlan.Filename)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Customer Survey Form"
            gui.Show()
        End If
    End Sub

    Private Sub btnTaxInvoice_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnTaxInvoice.ItemClick
        If Not SelectedRow Is Nothing Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim cmd As SqlClient.SqlCommand
            Dim da As SqlClient.SqlDataAdapter
            Dim gui As New frmReport(Nothing)
            Dim rep As New repTaxInvoice
            rep.Load()
            Dim ds As New dsReportsBranchJobsMailMerge

            sp_UpdateJobStatistics(SelectedRowField("BRID"), SelectedRowField("JBID"), Connection)

            cmd = New SqlClient.SqlCommand("SELECT * FROM dbo.[Job Mail Merge] WHERE BRID = @BRID AND JBID = @JBID", Connection)
            cmd.Parameters.Add("@BRID", SelectedRowField("BRID"))
            cmd.Parameters.Add("@JBID", SelectedRowField("JBID"))
            da = New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.Job_Mail_Merge)

            rep.SetDataSource(ds)
            'rep.SetParameterValue("Filename", MyBusinessPlan.Filename)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Tax Invoice"
            gui.Show()
        End If
    End Sub

#End Region

#Region " Printing "

    Private Sub btnPrint_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrint.ItemClick
        Print(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub Print(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.Print
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.PrintDlg()
    End Sub

    Private Sub btnPrintPreview_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrintPreview.ItemClick
        PrintPreview(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub PrintPreview(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.PrintPreview
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.ShowPreview()
    End Sub

    Private Sub btnQuickPrint_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnQuickPrint.ItemClick
        QuickPrint(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub QuickPrint(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.QuickPrint
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.Print(PrintingSystem.PageSettings.PrinterName)
    End Sub

#End Region

    Private Sub btnMailMerge_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnMailMerge.ItemClick
        If MailMergeFilterValid() Then
            Dim rep As New repGenericJobsMailout
            Dim cmd As SqlClient.SqlCommand
            Dim cmdString As String = "SELECT * FROM dbo.[Job Mail Merge] WHERE BRID = @BRID AND ([Scheduled Install Start Date] >= @FROM_DATE OR @FROM_DATE IS NULL) AND ([Scheduled Install Start Date] < DATEADD(DAY, 1, @TO_DATE) OR @TO_DATE IS NULL)"
            Dim filter As String = MailMergeFilter()
            If filter = "" Then
                cmd = New SqlClient.SqlCommand(cmdString, Connection)
            Else
                cmd = New SqlClient.SqlCommand(cmdString & " AND " & filter, Connection)
            End If
            cmd.Parameters.Add("@BRID", Me.BRID)
            cmd.Parameters.Add("@FROM_DATE", IsNull(beiFromDate.EditValue, DBNull.Value))
            cmd.Parameters.Add("@TO_DATE", IsNull(beiToDate.EditValue, DBNull.Value))
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            'da.MissingMappingAction = MissingMappingAction.Ignore
            ReadUncommittedFill(da, rep.dataSet.Job_Mail_Merge)
            If rep.dataSet.Job_Mail_Merge.Count > 100 Then
                Message.ShowMessage_MailoutTooLarge(100)
                Exit Sub
            End If
            Dim designerGui As New frmXtraReportDesigner(rep)
            designerGui.Show()
        End If
    End Sub

    Private Function MailMergeFilter() As String
        Return Replace(Replace(Replace(Replace(AddCondition(GridView1.RowFilter, StatusFilter),
            "[JBClientFirstName]", "[Customer Name]"),
            "[JBJobAddressMultiline]", "[Job Address (Full)]"),
            "[JBStatus]", "[Job Financials Status]"),
            "*", "%")
    End Function

    Private Function MailMergeFilterValid() As Boolean
        If InStr(GridView1.RowFilter, "[JBScheduledStartDate]") > 0 Then
            Message.ShowMessage_DateInSearchRow()
            Return False
        End If
        If InStr(GridView1.RowFilter, "[JBPrice]") > 0 Then
            Message.ShowMessage_NumberInSearchRow()
            Return False
        End If
        Return True
    End Function

    Private Sub GridView1_ShowGridMenu(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.GridMenuEventArgs) Handles GridView1.ShowGridMenu
        If SelectedRow Is Nothing Then Exit Sub
        Dim View As DevExpress.XtraGrid.Views.Grid.GridView = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
        Dim HitInfo As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo
        HitInfo = View.CalcHitInfo(e.Point)
        If HitInfo.InRow Then
            Dim p2 As New Point(e.Point.X + View.GridControl.Left, e.Point.Y + View.GridControl.Top)
            btnUncancel.Enabled = SelectedRow.Item("JBIsCancelled")
            PopupMenu1.ShowPopup(Me.PointToScreen(p2))
        End If
    End Sub

    Private Sub btnHelp_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHelp.ItemClick
        ShowHelpTopic(ExplorerForm, "JobFinancials.html")
    End Sub

    Public Sub SetUpPermissions()
        For Each cmd As CommandAccess In Commands
            If Not Me.BarManager1.Items(cmd.Command) Is Nothing Then  ' This will not fail if the cmd.Command is not found
                Me.BarManager1.Items(cmd.Command).Tag = cmd
                Me.BarManager1.Items(cmd.Command).Enabled = cmd.Allowed
            End If
        Next
    End Sub

End Class
