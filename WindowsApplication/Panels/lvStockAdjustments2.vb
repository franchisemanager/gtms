Imports Power.Forms

Public Class lvStockAdjustments2
    Inherits System.Windows.Forms.UserControl
    Implements IListControl, IPrintableControl

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Connection As SqlClient.SqlConnection, ByVal BRID As Integer, ByVal ExplorerForm As frmExplorer)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.Connection = Connection
        Me.BRID = BRID
        hExplorerForm = ExplorerForm
        SetUpFilters()
        DateFilterIsListening = True
        FillDataSet()
        EnableDisable()
    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DsGTMS As WindowsApplication.dsGTMS
    Friend WithEvents hSqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colSADate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSAInventoryAmount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSACost As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSATypeDisplay As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents ImageListToolBar As System.Windows.Forms.ImageList
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents bStandard As DevExpress.XtraBars.Bar
    Friend WithEvents btnNew As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnEdit As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subReportsOnSelectedItem As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnQuickPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPrintPreview As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnHelp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarAndDockingController1 As DevExpress.XtraBars.BarAndDockingController
    Friend WithEvents btnDelete As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subReportsOnSelectedItem2 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents PopupMenu1 As DevExpress.XtraBars.PopupMenu
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents bAdvancedFilter As DevExpress.XtraBars.Bar
    Friend WithEvents beiFromDate As DevExpress.XtraBars.BarEditItem
    Friend WithEvents beiToDate As DevExpress.XtraBars.BarEditItem
    Friend WithEvents btnResetFilters As DevExpress.XtraBars.BarButtonItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(lvStockAdjustments2))
        Me.hSqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.DsGTMS = New WindowsApplication.dsGTMS
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colSATypeDisplay = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSADate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSAInventoryAmount = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSACost = New DevExpress.XtraGrid.Columns.GridColumn
        Me.ImageListToolBar = New System.Windows.Forms.ImageList(Me.components)
        Me.BarManager1 = New DevExpress.XtraBars.BarManager
        Me.bStandard = New DevExpress.XtraBars.Bar
        Me.btnNew = New DevExpress.XtraBars.BarButtonItem
        Me.btnEdit = New DevExpress.XtraBars.BarButtonItem
        Me.btnDelete = New DevExpress.XtraBars.BarButtonItem
        Me.subReportsOnSelectedItem = New DevExpress.XtraBars.BarSubItem
        Me.btnQuickPrint = New DevExpress.XtraBars.BarButtonItem
        Me.btnPrintPreview = New DevExpress.XtraBars.BarButtonItem
        Me.btnHelp = New DevExpress.XtraBars.BarButtonItem
        Me.bAdvancedFilter = New DevExpress.XtraBars.Bar
        Me.beiFromDate = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.beiToDate = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.btnResetFilters = New DevExpress.XtraBars.BarButtonItem
        Me.BarAndDockingController1 = New DevExpress.XtraBars.BarAndDockingController(Me.components)
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
        Me.btnPrint = New DevExpress.XtraBars.BarButtonItem
        Me.subReportsOnSelectedItem2 = New DevExpress.XtraBars.BarSubItem
        Me.PopupMenu1 = New DevExpress.XtraBars.PopupMenu
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'hSqlConnection
        '
        Me.hSqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;user id=sa;integrated security=SSPI;data sou" & _
        "rce=""SERVER\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VStockAdjustments_Display", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("SAID", "SAID")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM StockAdjustments WHERE (BRID = @Original_BRID) AND (SAID = @Original_" & _
        "SAID)"
        Me.SqlDeleteCommand1.Connection = Me.hSqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SAID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SAID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO StockAdjustments(BRID) VALUES (@BRID); SELECT BRID, SAID FROM StockAd" & _
        "justments WHERE (BRID = @BRID) AND (SAID = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.hSqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, SAID, SADate, CountSIID, SACost, SATypeDisplay, SAInventoryAmount FR" & _
        "OM VStockAdjustments_Display WHERE (BRID = @BRID) AND (SADate >= @FROM_DATE) AND" & _
        " (SADate <= @TO_DATE) OR (BRID = @BRID) AND (SADate <= @TO_DATE) AND (@FROM_DATE" & _
        " IS NULL) OR (BRID = @BRID) AND (SADate >= @FROM_DATE) AND (@TO_DATE IS NULL) OR" & _
        " (BRID = @BRID) AND (@FROM_DATE IS NULL) AND (@TO_DATE IS NULL)"
        Me.SqlSelectCommand1.Connection = Me.hSqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FROM_DATE", System.Data.SqlDbType.DateTime, 8, "SADate"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TO_DATE", System.Data.SqlDbType.DateTime, 8, "SADate"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE StockAdjustments SET BRID = @BRID WHERE (BRID = @Original_BRID) AND (SAID " & _
        "= @Original_SAID); SELECT BRID, SAID FROM StockAdjustments WHERE (BRID = @BRID) " & _
        "AND (SAID = @SAID)"
        Me.SqlUpdateCommand1.Connection = Me.hSqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SAID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SAID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SAID", System.Data.SqlDbType.BigInt, 8, "SAID"))
        '
        'DsGTMS
        '
        Me.DsGTMS.DataSetName = "dsGTMS"
        Me.DsGTMS.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'GridControl1
        '
        Me.GridControl1.DataMember = "VStockAdjustments_Display"
        Me.GridControl1.DataSource = Me.DsGTMS
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'GridControl1.EmbeddedNavigator
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.Location = New System.Drawing.Point(0, 50)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(632, 342)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
        Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
        Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSATypeDisplay, Me.colSADate, Me.colSAInventoryAmount, Me.colSACost})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsNavigation.AutoFocusNewRow = True
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.OptionsView.ShowHorzLines = False
        Me.GridView1.OptionsView.ShowIndicator = False
        Me.GridView1.OptionsView.ShowVertLines = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colSADate, DevExpress.Data.ColumnSortOrder.Descending)})
        '
        'colSATypeDisplay
        '
        Me.colSATypeDisplay.Caption = "Adjustment"
        Me.colSATypeDisplay.FieldName = "SATypeDisplay"
        Me.colSATypeDisplay.Name = "colSATypeDisplay"
        Me.colSATypeDisplay.Visible = True
        Me.colSATypeDisplay.VisibleIndex = 0
        '
        'colSADate
        '
        Me.colSADate.Caption = "Date"
        Me.colSADate.FieldName = "SADate"
        Me.colSADate.Name = "colSADate"
        Me.colSADate.Visible = True
        Me.colSADate.VisibleIndex = 1
        '
        'colSAInventoryAmount
        '
        Me.colSAInventoryAmount.Caption = "Inventory"
        Me.colSAInventoryAmount.DisplayFormat.FormatString = "#0 Sheet(s)"
        Me.colSAInventoryAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colSAInventoryAmount.FieldName = "SAInventoryAmount"
        Me.colSAInventoryAmount.Name = "colSAInventoryAmount"
        Me.colSAInventoryAmount.OptionsColumn.ReadOnly = True
        Me.colSAInventoryAmount.Visible = True
        Me.colSAInventoryAmount.VisibleIndex = 2
        '
        'colSACost
        '
        Me.colSACost.Caption = "Cost"
        Me.colSACost.DisplayFormat.FormatString = "c"
        Me.colSACost.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colSACost.FieldName = "SACost"
        Me.colSACost.Name = "colSACost"
        Me.colSACost.Visible = True
        Me.colSACost.VisibleIndex = 3
        '
        'ImageListToolBar
        '
        Me.ImageListToolBar.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImageListToolBar.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageListToolBar.ImageStream = CType(resources.GetObject("ImageListToolBar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListToolBar.TransparentColor = System.Drawing.Color.Transparent
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.bStandard, Me.bAdvancedFilter})
        Me.BarManager1.Categories.AddRange(New DevExpress.XtraBars.BarManagerCategory() {New DevExpress.XtraBars.BarManagerCategory("Built-in Menus", New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd"), False), New DevExpress.XtraBars.BarManagerCategory("File", New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")), New DevExpress.XtraBars.BarManagerCategory("Reports", New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")), New DevExpress.XtraBars.BarManagerCategory("Forms", New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")), New DevExpress.XtraBars.BarManagerCategory("Help", New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")), New DevExpress.XtraBars.BarManagerCategory("Search", New System.Guid("d27cdec9-bd13-47ce-a40f-a58b83ae2195"))})
        Me.BarManager1.Controller = Me.BarAndDockingController1
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Images = Me.ImageListToolBar
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnNew, Me.btnEdit, Me.btnDelete, Me.btnHelp, Me.btnPrintPreview, Me.btnQuickPrint, Me.btnPrint, Me.subReportsOnSelectedItem, Me.subReportsOnSelectedItem2, Me.beiFromDate, Me.beiToDate, Me.btnResetFilters})
        Me.BarManager1.MainMenu = Me.bStandard
        Me.BarManager1.MaxItemId = 97
        Me.BarManager1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1, Me.RepositoryItemDateEdit2})
        '
        'bStandard
        '
        Me.bStandard.BarName = "Standard Toolbar"
        Me.bStandard.DockCol = 0
        Me.bStandard.DockRow = 0
        Me.bStandard.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bStandard.FloatLocation = New System.Drawing.Point(44, 188)
        Me.bStandard.FloatSize = New System.Drawing.Size(659, 24)
        Me.bStandard.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnNew, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnEdit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnDelete, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(Me.subReportsOnSelectedItem, True), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnQuickPrint, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnPrintPreview, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnHelp, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.bStandard.OptionsBar.AllowQuickCustomization = False
        Me.bStandard.OptionsBar.DisableClose = True
        Me.bStandard.OptionsBar.DrawDragBorder = False
        Me.bStandard.OptionsBar.MultiLine = True
        Me.bStandard.OptionsBar.UseWholeRow = True
        Me.bStandard.Text = "Standard Toolbar"
        '
        'btnNew
        '
        Me.btnNew.Caption = "&New..."
        Me.btnNew.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnNew.Id = 19
        Me.btnNew.ImageIndex = 0
        Me.btnNew.ImageIndexDisabled = 0
        Me.btnNew.Name = "btnNew"
        '
        'btnEdit
        '
        Me.btnEdit.Caption = "&Edit..."
        Me.btnEdit.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnEdit.Id = 20
        Me.btnEdit.ImageIndex = 1
        Me.btnEdit.ImageIndexDisabled = 1
        Me.btnEdit.Name = "btnEdit"
        '
        'btnDelete
        '
        Me.btnDelete.Caption = "&Delete"
        Me.btnDelete.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnDelete.Id = 23
        Me.btnDelete.ImageIndex = 13
        Me.btnDelete.ImageIndexDisabled = 13
        Me.btnDelete.Name = "btnDelete"
        '
        'subReportsOnSelectedItem
        '
        Me.subReportsOnSelectedItem.Caption = "Reports on Selected Item"
        Me.subReportsOnSelectedItem.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.subReportsOnSelectedItem.Id = 78
        Me.subReportsOnSelectedItem.Name = "subReportsOnSelectedItem"
        Me.subReportsOnSelectedItem.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInCustomizing
        '
        'btnQuickPrint
        '
        Me.btnQuickPrint.Caption = "Print"
        Me.btnQuickPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnQuickPrint.Id = 46
        Me.btnQuickPrint.ImageIndex = 11
        Me.btnQuickPrint.ImageIndexDisabled = 11
        Me.btnQuickPrint.Name = "btnQuickPrint"
        '
        'btnPrintPreview
        '
        Me.btnPrintPreview.Caption = "Print Pre&view"
        Me.btnPrintPreview.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrintPreview.Id = 44
        Me.btnPrintPreview.ImageIndex = 10
        Me.btnPrintPreview.ImageIndexDisabled = 10
        Me.btnPrintPreview.Name = "btnPrintPreview"
        '
        'btnHelp
        '
        Me.btnHelp.Caption = "&Help"
        Me.btnHelp.CategoryGuid = New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")
        Me.btnHelp.Id = 68
        Me.btnHelp.ImageIndex = 4
        Me.btnHelp.ImageIndexDisabled = 4
        Me.btnHelp.Name = "btnHelp"
        '
        'bAdvancedFilter
        '
        Me.bAdvancedFilter.BarName = "Search Bar"
        Me.bAdvancedFilter.DockCol = 0
        Me.bAdvancedFilter.DockRow = 1
        Me.bAdvancedFilter.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bAdvancedFilter.FloatLocation = New System.Drawing.Point(40, 189)
        Me.bAdvancedFilter.FloatSize = New System.Drawing.Size(570, 24)
        Me.bAdvancedFilter.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.beiFromDate, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.beiToDate, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnResetFilters, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.bAdvancedFilter.OptionsBar.AllowQuickCustomization = False
        Me.bAdvancedFilter.OptionsBar.DisableClose = True
        Me.bAdvancedFilter.OptionsBar.MultiLine = True
        Me.bAdvancedFilter.Text = "Search Bar"
        '
        'beiFromDate
        '
        Me.beiFromDate.Caption = "Search for Dates from:"
        Me.beiFromDate.CategoryGuid = New System.Guid("d27cdec9-bd13-47ce-a40f-a58b83ae2195")
        Me.beiFromDate.Edit = Me.RepositoryItemDateEdit1
        Me.beiFromDate.Id = 93
        Me.beiFromDate.Name = "beiFromDate"
        Me.beiFromDate.Width = 150
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'beiToDate
        '
        Me.beiToDate.Caption = "to:"
        Me.beiToDate.CategoryGuid = New System.Guid("d27cdec9-bd13-47ce-a40f-a58b83ae2195")
        Me.beiToDate.Edit = Me.RepositoryItemDateEdit2
        Me.beiToDate.Id = 94
        Me.beiToDate.Name = "beiToDate"
        Me.beiToDate.Width = 150
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'btnResetFilters
        '
        Me.btnResetFilters.Caption = "&Reset Filters"
        Me.btnResetFilters.CategoryGuid = New System.Guid("d27cdec9-bd13-47ce-a40f-a58b83ae2195")
        Me.btnResetFilters.Id = 95
        Me.btnResetFilters.ImageIndex = 14
        Me.btnResetFilters.ImageIndexDisabled = 14
        Me.btnResetFilters.Name = "btnResetFilters"
        '
        'BarAndDockingController1
        '
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseForeColor = True
        Me.BarAndDockingController1.PaintStyleName = "OfficeXP"
        '
        'btnPrint
        '
        Me.btnPrint.Caption = "&Print..."
        Me.btnPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrint.Id = 43
        Me.btnPrint.ImageIndex = 11
        Me.btnPrint.ImageIndexDisabled = 11
        Me.btnPrint.Name = "btnPrint"
        '
        'subReportsOnSelectedItem2
        '
        Me.subReportsOnSelectedItem2.Caption = "&Reports on Selected Item"
        Me.subReportsOnSelectedItem2.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subReportsOnSelectedItem2.Id = 87
        Me.subReportsOnSelectedItem2.Name = "subReportsOnSelectedItem2"
        '
        'PopupMenu1
        '
        Me.PopupMenu1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.subReportsOnSelectedItem, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnEdit, True)})
        Me.PopupMenu1.Manager = Me.BarManager1
        Me.PopupMenu1.Name = "PopupMenu1"
        '
        'lvStockAdjustments2
        '
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "lvStockAdjustments2"
        Me.Size = New System.Drawing.Size(632, 392)
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return hSqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            hSqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private Sub EnableDisable()
        btnNew.Enabled = AllowNew
        btnEdit.Enabled = AllowEdit
        btnDelete.Enabled = AllowDelete
    End Sub

    Public Function FillDataSet() As Integer Implements IListControl.FillDataSet
        Dim cursor As System.Windows.Forms.Cursor = ExplorerForm.Cursor
        ExplorerForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
        ' Open connection
        Dim trans As SqlClient.SqlTransaction = Connection.BeginTransaction(IsolationLevel.ReadUncommitted)
        Power.Library.Library.ApplyTransactionToAllDataAdapters(trans, Me)
        'Me.GridControl1.DataSource.Clear()
        FillDataSet = SqlDataAdapter.Fill(DsGTMS)
        'Close connecton
        trans.Commit()
        ExplorerForm.Cursor = cursor
    End Function

#Region " Filters "

    Private Sub SetUpFilters()
        ' Set Up Default Advanced Filter
        beiFromDate.EditValue = DateAdd(DateInterval.Month, -6, Today.Date)
        beiToDate.EditValue = Nothing
        UpdateDateFilter()
        GridView1.ClearColumnsFilter()
    End Sub

    Private Sub DateFilter_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beiFromDate.EditValueChanged, beiToDate.EditValueChanged
        UpdateDateFilter()
    End Sub

    Private DateFilterIsListening As Boolean = False ' This is so this doesn't trigger during load (this will happen in code)
    Private Sub UpdateDateFilter()
        Me.SqlDataAdapter.SelectCommand.Parameters("@FROM_DATE").Value = IsNull(beiFromDate.EditValue, DBNull.Value)
        Me.SqlDataAdapter.SelectCommand.Parameters("@TO_DATE").Value = IsNull(beiToDate.EditValue, DBNull.Value)
        If DateFilterIsListening Then
            Me.DsGTMS.Tables(Me.SqlDataAdapter.TableMappings(0).DataSetTable).Clear()
            FillDataSet()
        End If
    End Sub

    Private Sub btnResetFilters_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnResetFilters.ItemClick
        DateFilterIsListening = False
        SetUpFilters()
        Me.DsGTMS.Tables(Me.SqlDataAdapter.TableMappings(0).DataSetTable).Clear()
        FillDataSet()
        DateFilterIsListening = True
    End Sub

#End Region

    Private Sub GridView1_RowCountChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.RowCountChanged
        ExplorerForm.UpdateNumItems(NumItems)
        ExplorerForm.UpdateVisibleItems(VisibleItems)
    End Sub

    Public ReadOnly Property VisibleItems() As Integer Implements Power.Library.IListControl.VisibleItems
        Get
            Return GridView1.RowCount
        End Get
    End Property

    Public ReadOnly Property NumItems() As Integer Implements Power.Library.IListControl.NumItems
        Get
            Return DsGTMS.Tables(SqlDataAdapter.TableMappings(0).DataSetTable).Rows.Count
        End Get
    End Property

    Private hBRID As Integer
    Private Property BRID() As Integer
        Get
            Return hBRID
        End Get
        Set(ByVal Value As Integer)
            hBRID = Value
            SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = Value
        End Set
    End Property

    Public ReadOnly Property SelectedRow() As DataRow
        Get
            If Not GridView1.GetSelectedRows Is Nothing Then
                Return GridView1.GetDataRow(GridView1.GetSelectedRows(0))
            End If
        End Get
    End Property

    Public ReadOnly Property SelectedRowField(ByVal Field As String) As Object
        Get
            If Not SelectedRow Is Nothing Then
                Return SelectedRow.Item(Field)
            Else
                Return DBNull.Value
            End If
        End Get
    End Property

    Public Function SelectRow(ByVal BRID As Int32, ByVal SAID As Int64) As Boolean
        Dim i As Integer = 0
        Do Until False
            Try
                If GridView1.GetDataRow(GridView1.GetVisibleRowHandle(i))("BRID") = BRID And _
                        GridView1.GetDataRow(GridView1.GetVisibleRowHandle(i))("SAID") = SAID Then
                    GridView1.ClearSelection()
                    GridView1.SelectRow(GridView1.GetVisibleRowHandle(i))
                    Return True
                End If
                i += 1
            Catch ex As NullReferenceException
                Return False
            End Try
        Loop
        Return False
    End Function

#Region " New, Open and Delete "

    Private Sub DataListView_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GridControl1.DoubleClick
        List_Edit()
    End Sub

    Private Sub btnNew_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnNew.ItemClick
        List_New()
    End Sub

    Public Sub List_New() Implements IListControl.List_New
        Dim c As Cursor = ParentForm.Cursor
        ParentForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
        frmStockAdjustmentWizard2.Add(BRID)
        FillDataSet()
        ExplorerForm.RefreshDynamicCommands()
        ParentForm.Cursor = c
    End Sub

    Private Sub btnEdit_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnEdit.ItemClick
        List_Edit()
    End Sub

    Public Sub List_Edit() Implements IListControl.List_Edit
        If Not SelectedRow Is Nothing Then
            Dim c As Cursor = ParentForm.Cursor
            ParentForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If StockAdjustmentExists(SelectedRowField("BRID"), SelectedRowField("SAID")) Then
                frmStockAdjustment2.Edit(SelectedRowField("BRID"), SelectedRowField("SAID"))
                FillDataSet()
                ExplorerForm.RefreshDynamicCommands()
            Else
                Message.AlreadyDeleted("stock adjustment", Message.ObjectAction.Edit)
                If TypeOf Me.GridControl1.DataSource Is DataView Then
                    CType(Me.GridControl1.DataSource, DataView).Table.Clear()
                ElseIf TypeOf Me.GridControl1.DataSource Is DataSet Then
                    CType(Me.GridControl1.DataSource, DataSet).Clear()
                End If
                FillDataSet()
            End If
            ParentForm.Cursor = c
        End If
    End Sub

    Public Sub List_Cancel() Implements Power.Library.IListControl.List_Cancel

    End Sub

    Public Sub List_Uncancel() Implements Power.Library.IListControl.List_Uncancel

    End Sub

    Private Sub btnDelete_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnDelete.ItemClick
        List_Delete()
    End Sub

    Public Sub List_Delete() Implements IListControl.List_Delete
        If Not SelectedRow Is Nothing Then
            Dim SAID As Long = SelectedRowField("SAID")
            If Message.AskDeleteObject("stock adjustment", Message.ObjectAction.Delete) = MsgBoxResult.Yes Then
                If StockAdjustmentExists(BRID, SAID) Then
                    If DataAccess.spExecLockRequest("sp_GetStockAdjustmentLock", BRID, SAID, Connection) Then
                        SelectedRow.Delete()
                        SqlDataAdapter.Update(DsGTMS)
                        DataAccess.spExecLockRequest("sp_ReleaseStockAdjustmentLock", BRID, SAID, Connection)
                    Else
                        Message.CurrentlyAccessed("stock adjustment")
                    End If
                Else
                    If TypeOf Me.GridControl1.DataSource Is DataView Then
                        CType(Me.GridControl1.DataSource, DataView).Table.Clear()
                    ElseIf TypeOf Me.GridControl1.DataSource Is DataSet Then
                        CType(Me.GridControl1.DataSource, DataSet).Clear()
                    End If
                    FillDataSet()
                End If
            End If
        End If
    End Sub

    Private Function StockAdjustmentExists(ByVal BRID As Int32, ByVal SAID As Int64) As Boolean
        Dim cnn As SqlClient.SqlConnection = Connection
        Dim cmd As New SqlClient.SqlCommand("SELECT Count(SAID) FROM StockAdjustments WHERE BRID = @BRID AND SAID = @SAID", cnn)
        cmd.CommandType = CommandType.Text
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@SAID", SAID)
        Return cmd.ExecuteScalar > 0
    End Function

#End Region

    Private hExplorerForm As Form
    Public ReadOnly Property ExplorerForm() As frmExplorer
        Get
            Return hExplorerForm
        End Get
    End Property

#Region " Printing "

    Private Sub btnPrint_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrint.ItemClick
        Print(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub Print(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.Print
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.PrintDlg()
    End Sub

    Private Sub btnPrintPreview_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrintPreview.ItemClick
        PrintPreview(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub PrintPreview(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.PrintPreview
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(ExplorerForm.PrintingSystem)
        link.Component = GridControl1
        link.ShowPreview()
    End Sub

    Private Sub btnQuickPrint_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnQuickPrint.ItemClick
        QuickPrint(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub QuickPrint(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.QuickPrint
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.Print(PrintingSystem.PageSettings.PrinterName)
    End Sub

#End Region

    Public ReadOnly Property AllowDelete() As Boolean Implements Power.Library.IListControl.AllowDelete
        Get
            Return Not HasRole("branch_read_only")
        End Get
    End Property

    Public ReadOnly Property AllowEdit() As Boolean Implements Power.Library.IListControl.AllowEdit
        Get
            Return True
        End Get
    End Property

    Public ReadOnly Property AllowNew() As Boolean Implements Power.Library.IListControl.AllowNew
        Get
            Return Not HasRole("branch_read_only")
        End Get
    End Property

    Public ReadOnly Property AllowCancel() As Boolean Implements Power.Library.IListControl.AllowCancel
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property AllowUncancel() As Boolean Implements Power.Library.IListControl.AllowUncancel
        Get
            Return False
        End Get
    End Property

    Private Sub btnHelp_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHelp.ItemClick
        ShowHelpTopic(ExplorerForm, "StockTracking.html")
    End Sub
End Class
