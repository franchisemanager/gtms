Public Class frmLead
    Inherits DevExpress.XtraEditors.XtraForm

    Private DataRow As DataRow
    Public BRID As Int32
    Public LDID As Int64

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return SqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            SqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private hTransaction As SqlClient.SqlTransaction
    Private Property Transaction() As SqlClient.SqlTransaction
        Get
            Return hTransaction
        End Get
        Set(ByVal Value As SqlClient.SqlTransaction)
            hTransaction = Value
            Power.Library.Library.ApplyTransactionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Public Shared Function Add(ByVal BRID As Int32) As frmLead
        Dim gui As New frmLead

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()
            Dim tempTrans As SqlClient.SqlTransaction = .SqlConnection.BeginTransaction(IsolationLevel.ReadUncommitted)
            Dim NextID As Long = sp_GetNextID(BRID, tempTrans)
            tempTrans.Commit()

            .BRID = BRID

            .Transaction = .Connection.BeginTransaction(IsolationLevel.ReadUncommitted)
            .FillPreliminaryData()
            .LDID = spNew_Lead(.BRID, NextID, .Transaction)

            If DataAccess.spExecLockRequest("sp_GetLeadIDLock", .BRID, .LDID, .Transaction) Then

                .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = .BRID
                .SqlDataAdapter.SelectCommand.Parameters("@LDID").Value = .LDID
                .SqlDataAdapter.Fill(.DsGTMS)
                .DataRow = .DsGTMS.Leads(0)

                .FillData()

            Else
                Throw New ObjectLockedException
            End If
        End With

        Return gui
    End Function

    Public Shared Function Edit(ByVal BRID As Int32, ByRef LDID As Int64) As frmLead
        Dim gui As New frmLead

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            .BRID = BRID
            .LDID = LDID

            .Transaction = .SqlConnection.BeginTransaction(IsolationLevel.ReadUncommitted)
            .FillPreliminaryData()

            If DataAccess.spExecLockRequest("sp_GetLeadIDLock", .BRID, .LDID, .Transaction) Then

                .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = BRID
                .SqlDataAdapter.SelectCommand.Parameters("@LDID").Value = LDID
                .SqlDataAdapter.Fill(.DsGTMS)
                .DataRow = .DsGTMS.Leads(0)

                .FillData()

            Else
                Throw New ObjectLockedException
            End If
        End With

        Return gui
    End Function

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()
        ReadMRU(txtLDJobDescription, UserAppDataPath)

        'Add any initialization after the InitializeComponent() call
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DsGTMS As WindowsApplication.dsGTMS
    Friend WithEvents GroupLine1 As Power.Forms.GroupLine
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupLine2 As Power.Forms.GroupLine
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents GroupLine3 As Power.Forms.GroupLine
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupLine17 As Power.Forms.GroupLine
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents GroupLine18 As Power.Forms.GroupLine
    Friend WithEvents GroupLine19 As Power.Forms.GroupLine
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents GroupLine4 As Power.Forms.GroupLine
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents GroupLine5 As Power.Forms.GroupLine
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents GroupLine6 As Power.Forms.GroupLine
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents chkLDQuoteReceived As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ListBox As Power.Forms.ListBox
    Friend WithEvents pnlMain As DevExpress.XtraEditors.PanelControl
    Friend WithEvents dpLDDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents txtLDJobDescription As DevExpress.XtraEditors.MRUEdit
    Friend WithEvents txtEXIDRep As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents daLeadSources As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents txtLSName As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents txtLDJobStreetAddress01 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDJobStreetAddress02 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDJobSuburb As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDJobState As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDJobPostCode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents dvSalesReps As System.Data.DataView
    Friend WithEvents lblJobState As System.Windows.Forms.Label
    Friend WithEvents lblJobStreetAddress As System.Windows.Forms.Label
    Friend WithEvents lblJobSuburb As System.Windows.Forms.Label
    Friend WithEvents rgLDJobAddressAsAbove As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents GroupLine7 As Power.Forms.GroupLine
    Friend WithEvents txtLDClientSurname As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientFirstName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDReferenceName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientStreetAddress01 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientStreetAddress02 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientSuburb As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientState As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientPostCode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientPhoneNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientFaxNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtLDClientMobileNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents pnlEnquiryInformation As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlScheduling As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlJobAddress As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlEnquirySources As DevExpress.XtraEditors.GroupControl
    Friend WithEvents pnlClientInformation As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtLDUser As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents txtLDClientEmail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents daExpenses As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents daAppointments As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents btnAddAppointment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEditAppointment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveAppointment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents daLeadsSubSources As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents colSSName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSSUserSelected As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dgAppointments As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvAppointments As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colAPBegin As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAPEnd As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEXName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dvAppointments As System.Data.DataView
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents GroupLine8 As Power.Forms.GroupLine
    Friend WithEvents GroupLine9 As Power.Forms.GroupLine
    Friend WithEvents btnViewCalendar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents cmHistory As System.Windows.Forms.ContextMenu
    Friend WithEvents miClearHistory As System.Windows.Forms.MenuItem
    Friend WithEvents DsCalendar As WindowsApplication.dsCalendar
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daTasks As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents Storage As DevExpress.XtraScheduler.SchedulerStorage
    Friend WithEvents dvExpenses As System.Data.DataView
    Friend WithEvents dvResources As System.Data.DataView
    Friend WithEvents pnlNotes As DevExpress.XtraEditors.GroupControl
    Friend WithEvents dgNotes As DevExpress.XtraGrid.GridControl
    Friend WithEvents daIDNotes As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents gvNotes As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colINUser As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colINDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colINNotes As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlSelectCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents txtUser As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents RepositoryItemMemoEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit
    Friend WithEvents txtINDate As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents lblJobPostCode As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmLead))
        Me.DsGTMS = New WindowsApplication.dsGTMS
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.ListBox = New Power.Forms.ListBox
        Me.txtEXIDRep = New DevExpress.XtraEditors.LookUpEdit
        Me.dvSalesReps = New System.Data.DataView
        Me.txtLDJobDescription = New DevExpress.XtraEditors.MRUEdit
        Me.cmHistory = New System.Windows.Forms.ContextMenu
        Me.miClearHistory = New System.Windows.Forms.MenuItem
        Me.chkLDQuoteReceived = New DevExpress.XtraEditors.CheckEdit
        Me.dpLDDate = New DevExpress.XtraEditors.DateEdit
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupLine1 = New Power.Forms.GroupLine
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupLine2 = New Power.Forms.GroupLine
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.GroupLine3 = New Power.Forms.GroupLine
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.GroupLine6 = New Power.Forms.GroupLine
        Me.Label28 = New System.Windows.Forms.Label
        Me.Label29 = New System.Windows.Forms.Label
        Me.Label31 = New System.Windows.Forms.Label
        Me.GroupLine7 = New Power.Forms.GroupLine
        Me.pnlMain = New DevExpress.XtraEditors.PanelControl
        Me.pnlScheduling = New DevExpress.XtraEditors.GroupControl
        Me.Label16 = New System.Windows.Forms.Label
        Me.btnViewCalendar = New DevExpress.XtraEditors.SimpleButton
        Me.GroupLine9 = New Power.Forms.GroupLine
        Me.GroupLine8 = New Power.Forms.GroupLine
        Me.btnAddAppointment = New DevExpress.XtraEditors.SimpleButton
        Me.btnEditAppointment = New DevExpress.XtraEditors.SimpleButton
        Me.btnRemoveAppointment = New DevExpress.XtraEditors.SimpleButton
        Me.Label19 = New System.Windows.Forms.Label
        Me.dgAppointments = New DevExpress.XtraGrid.GridControl
        Me.dvAppointments = New System.Data.DataView
        Me.DsCalendar = New WindowsApplication.dsCalendar
        Me.gvAppointments = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colAPBegin = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colAPEnd = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colEXName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.pnlNotes = New DevExpress.XtraEditors.GroupControl
        Me.dgNotes = New DevExpress.XtraGrid.GridControl
        Me.gvNotes = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colINUser = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtUser = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.dvExpenses = New System.Data.DataView
        Me.colINDate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtINDate = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.colINNotes = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemMemoEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit
        Me.pnlJobAddress = New DevExpress.XtraEditors.GroupControl
        Me.lblJobPostCode = New System.Windows.Forms.Label
        Me.txtLDJobStreetAddress01 = New DevExpress.XtraEditors.TextEdit
        Me.rgLDJobAddressAsAbove = New DevExpress.XtraEditors.RadioGroup
        Me.Label18 = New System.Windows.Forms.Label
        Me.lblJobState = New System.Windows.Forms.Label
        Me.lblJobStreetAddress = New System.Windows.Forms.Label
        Me.lblJobSuburb = New System.Windows.Forms.Label
        Me.txtLDJobStreetAddress02 = New DevExpress.XtraEditors.TextEdit
        Me.txtLDJobSuburb = New DevExpress.XtraEditors.TextEdit
        Me.txtLDJobState = New DevExpress.XtraEditors.TextEdit
        Me.txtLDJobPostCode = New DevExpress.XtraEditors.TextEdit
        Me.pnlEnquirySources = New DevExpress.XtraEditors.GroupControl
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colSSName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSSUserSelected = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.GroupLine4 = New Power.Forms.GroupLine
        Me.Label24 = New System.Windows.Forms.Label
        Me.GroupLine5 = New Power.Forms.GroupLine
        Me.Label25 = New System.Windows.Forms.Label
        Me.txtLSName = New DevExpress.XtraEditors.LookUpEdit
        Me.pnlEnquiryInformation = New DevExpress.XtraEditors.GroupControl
        Me.txtLDUser = New DevExpress.XtraEditors.LookUpEdit
        Me.pnlClientInformation = New DevExpress.XtraEditors.GroupControl
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.txtLDClientEmail = New DevExpress.XtraEditors.TextEdit
        Me.txtLDClientSurname = New DevExpress.XtraEditors.TextEdit
        Me.GroupLine19 = New Power.Forms.GroupLine
        Me.GroupLine17 = New Power.Forms.GroupLine
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.GroupLine18 = New Power.Forms.GroupLine
        Me.Label30 = New System.Windows.Forms.Label
        Me.txtLDClientFirstName = New DevExpress.XtraEditors.TextEdit
        Me.txtLDReferenceName = New DevExpress.XtraEditors.TextEdit
        Me.txtLDClientStreetAddress01 = New DevExpress.XtraEditors.TextEdit
        Me.txtLDClientStreetAddress02 = New DevExpress.XtraEditors.TextEdit
        Me.txtLDClientSuburb = New DevExpress.XtraEditors.TextEdit
        Me.txtLDClientState = New DevExpress.XtraEditors.TextEdit
        Me.txtLDClientPostCode = New DevExpress.XtraEditors.TextEdit
        Me.txtLDClientPhoneNumber = New DevExpress.XtraEditors.TextEdit
        Me.txtLDClientFaxNumber = New DevExpress.XtraEditors.TextEdit
        Me.txtLDClientMobileNumber = New DevExpress.XtraEditors.TextEdit
        Me.daLeadSources = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.daExpenses = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton
        Me.daAppointments = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand
        Me.daLeadsSubSources = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand5 = New System.Data.SqlClient.SqlCommand
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider
        Me.daTasks = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand2 = New System.Data.SqlClient.SqlCommand
        Me.Storage = New DevExpress.XtraScheduler.SchedulerStorage(Me.components)
        Me.dvResources = New System.Data.DataView
        Me.daIDNotes = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand6 = New System.Data.SqlClient.SqlCommand
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEXIDRep.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvSalesReps, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDJobDescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkLDQuoteReceived.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dpLDDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMain.SuspendLayout()
        CType(Me.pnlScheduling, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlScheduling.SuspendLayout()
        CType(Me.dgAppointments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvAppointments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsCalendar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvAppointments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlNotes.SuspendLayout()
        CType(Me.dgNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvNotes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUser, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvExpenses, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtINDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemMemoEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlJobAddress, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlJobAddress.SuspendLayout()
        CType(Me.txtLDJobStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgLDJobAddressAsAbove.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDJobStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDJobSuburb.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDJobState.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDJobPostCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlEnquirySources, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlEnquirySources.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLSName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlEnquiryInformation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlEnquiryInformation.SuspendLayout()
        CType(Me.txtLDUser.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlClientInformation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlClientInformation.SuspendLayout()
        CType(Me.txtLDClientEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientSurname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientFirstName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDReferenceName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientSuburb.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientState.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientPostCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientPhoneNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientFaxNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDClientMobileNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Storage, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvResources, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DsGTMS
        '
        Me.DsGTMS.DataSetName = "dsGTMS"
        Me.DsGTMS.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(688, 496)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(608, 496)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 3
        Me.btnOK.Text = "OK"
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;integrated security=SSPI;data source=""SERVER" & _
        "\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Leads", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("LDID", "LDID"), New System.Data.Common.DataColumnMapping("ID", "ID"), New System.Data.Common.DataColumnMapping("LDDate", "LDDate"), New System.Data.Common.DataColumnMapping("LDClientFirstName", "LDClientFirstName"), New System.Data.Common.DataColumnMapping("LDClientSurname", "LDClientSurname"), New System.Data.Common.DataColumnMapping("LDClientName", "LDClientName"), New System.Data.Common.DataColumnMapping("LDClientStreetAddress01", "LDClientStreetAddress01"), New System.Data.Common.DataColumnMapping("LDClientStreetAddress02", "LDClientStreetAddress02"), New System.Data.Common.DataColumnMapping("LDClientSuburb", "LDClientSuburb"), New System.Data.Common.DataColumnMapping("LDClientState", "LDClientState"), New System.Data.Common.DataColumnMapping("LDClientPostCode", "LDClientPostCode"), New System.Data.Common.DataColumnMapping("LDClientAddress", "LDClientAddress"), New System.Data.Common.DataColumnMapping("LDClientPhoneNumber", "LDClientPhoneNumber"), New System.Data.Common.DataColumnMapping("LDClientFaxNumber", "LDClientFaxNumber"), New System.Data.Common.DataColumnMapping("LDClientMobileNumber", "LDClientMobileNumber"), New System.Data.Common.DataColumnMapping("LDJobAddressAsAbove", "LDJobAddressAsAbove"), New System.Data.Common.DataColumnMapping("LDJobStreetAddress01", "LDJobStreetAddress01"), New System.Data.Common.DataColumnMapping("LDJobStreetAddress02", "LDJobStreetAddress02"), New System.Data.Common.DataColumnMapping("LDJobSuburb", "LDJobSuburb"), New System.Data.Common.DataColumnMapping("LDJobState", "LDJobState"), New System.Data.Common.DataColumnMapping("LDJobPostCode", "LDJobPostCode"), New System.Data.Common.DataColumnMapping("LDJobAddress", "LDJobAddress"), New System.Data.Common.DataColumnMapping("LDReferenceName", "LDReferenceName"), New System.Data.Common.DataColumnMapping("LSID", "LSID"), New System.Data.Common.DataColumnMapping("LDHasAppointments", "LDHasAppointments"), New System.Data.Common.DataColumnMapping("LDUser", "LDUser"), New System.Data.Common.DataColumnMapping("LDJobDescription", "LDJobDescription"), New System.Data.Common.DataColumnMapping("EXIDRep", "EXIDRep"), New System.Data.Common.DataColumnMapping("LDQuoteReceived", "LDQuoteReceived"), New System.Data.Common.DataColumnMapping("LDClientEmail", "LDClientEmail")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Leads WHERE (BRID = @Original_BRID) AND (LDID = @Original_LDID) AND (" & _
        "EXIDRep = @Original_EXIDRep OR @Original_EXIDRep IS NULL AND EXIDRep IS NULL) AN" & _
        "D (ID = @Original_ID) AND (LDClientAddress = @Original_LDClientAddress OR @Origi" & _
        "nal_LDClientAddress IS NULL AND LDClientAddress IS NULL) AND (LDClientEmail = @O" & _
        "riginal_LDClientEmail OR @Original_LDClientEmail IS NULL AND LDClientEmail IS NU" & _
        "LL) AND (LDClientFaxNumber = @Original_LDClientFaxNumber OR @Original_LDClientFa" & _
        "xNumber IS NULL AND LDClientFaxNumber IS NULL) AND (LDClientFirstName = @Origina" & _
        "l_LDClientFirstName OR @Original_LDClientFirstName IS NULL AND LDClientFirstName" & _
        " IS NULL) AND (LDClientMobileNumber = @Original_LDClientMobileNumber OR @Origina" & _
        "l_LDClientMobileNumber IS NULL AND LDClientMobileNumber IS NULL) AND (LDClientNa" & _
        "me = @Original_LDClientName OR @Original_LDClientName IS NULL AND LDClientName I" & _
        "S NULL) AND (LDClientPhoneNumber = @Original_LDClientPhoneNumber OR @Original_LD" & _
        "ClientPhoneNumber IS NULL AND LDClientPhoneNumber IS NULL) AND (LDClientPostCode" & _
        " = @Original_LDClientPostCode OR @Original_LDClientPostCode IS NULL AND LDClient" & _
        "PostCode IS NULL) AND (LDClientState = @Original_LDClientState OR @Original_LDCl" & _
        "ientState IS NULL AND LDClientState IS NULL) AND (LDClientStreetAddress01 = @Ori" & _
        "ginal_LDClientStreetAddress01 OR @Original_LDClientStreetAddress01 IS NULL AND L" & _
        "DClientStreetAddress01 IS NULL) AND (LDClientStreetAddress02 = @Original_LDClien" & _
        "tStreetAddress02 OR @Original_LDClientStreetAddress02 IS NULL AND LDClientStreet" & _
        "Address02 IS NULL) AND (LDClientSuburb = @Original_LDClientSuburb OR @Original_L" & _
        "DClientSuburb IS NULL AND LDClientSuburb IS NULL) AND (LDClientSurname = @Origin" & _
        "al_LDClientSurname OR @Original_LDClientSurname IS NULL AND LDClientSurname IS N" & _
        "ULL) AND (LDDate = @Original_LDDate OR @Original_LDDate IS NULL AND LDDate IS NU" & _
        "LL) AND (LDHasAppointments = @Original_LDHasAppointments OR @Original_LDHasAppoi" & _
        "ntments IS NULL AND LDHasAppointments IS NULL) AND (LDJobAddress = @Original_LDJ" & _
        "obAddress OR @Original_LDJobAddress IS NULL AND LDJobAddress IS NULL) AND (LDJob" & _
        "AddressAsAbove = @Original_LDJobAddressAsAbove OR @Original_LDJobAddressAsAbove " & _
        "IS NULL AND LDJobAddressAsAbove IS NULL) AND (LDJobDescription = @Original_LDJob" & _
        "Description OR @Original_LDJobDescription IS NULL AND LDJobDescription IS NULL) " & _
        "AND (LDJobPostCode = @Original_LDJobPostCode OR @Original_LDJobPostCode IS NULL " & _
        "AND LDJobPostCode IS NULL) AND (LDJobState = @Original_LDJobState OR @Original_L" & _
        "DJobState IS NULL AND LDJobState IS NULL) AND (LDJobStreetAddress01 = @Original_" & _
        "LDJobStreetAddress01 OR @Original_LDJobStreetAddress01 IS NULL AND LDJobStreetAd" & _
        "dress01 IS NULL) AND (LDJobStreetAddress02 = @Original_LDJobStreetAddress02 OR @" & _
        "Original_LDJobStreetAddress02 IS NULL AND LDJobStreetAddress02 IS NULL) AND (LDJ" & _
        "obSuburb = @Original_LDJobSuburb OR @Original_LDJobSuburb IS NULL AND LDJobSubur" & _
        "b IS NULL) AND (LDQuoteReceived = @Original_LDQuoteReceived OR @Original_LDQuote" & _
        "Received IS NULL AND LDQuoteReceived IS NULL) AND (LDReferenceName = @Original_L" & _
        "DReferenceName OR @Original_LDReferenceName IS NULL AND LDReferenceName IS NULL)" & _
        " AND (LDUser = @Original_LDUser OR @Original_LDUser IS NULL AND LDUser IS NULL) " & _
        "AND (LSID = @Original_LSID OR @Original_LSID IS NULL AND LSID IS NULL)"
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDRep", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDRep", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientFirstName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientFirstName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientName", System.Data.SqlDbType.VarChar, 102, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientSurname", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDHasAppointments", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDHasAppointments", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobAddress", System.Data.SqlDbType.VarChar, 259, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobAddress", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobAddressAsAbove", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobDescription", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDQuoteReceived", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDQuoteReceived", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDReferenceName", System.Data.SqlDbType.VarChar, 200, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDReferenceName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDUser", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDUser", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LSID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LSID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Leads(BRID, ID, LDDate, LDClientFirstName, LDClientSurname, LDClientS" & _
        "treetAddress01, LDClientStreetAddress02, LDClientSuburb, LDClientState, LDClient" & _
        "PostCode, LDClientPhoneNumber, LDClientFaxNumber, LDClientMobileNumber, LDJobAdd" & _
        "ressAsAbove, LDJobStreetAddress01, LDJobStreetAddress02, LDJobSuburb, LDJobState" & _
        ", LDJobPostCode, LDReferenceName, LSID, LDHasAppointments, LDUser, LDJobDescript" & _
        "ion, EXIDRep, LDQuoteReceived, LDClientEmail) VALUES (@BRID, @ID, @LDDate, @LDCl" & _
        "ientFirstName, @LDClientSurname, @LDClientStreetAddress01, @LDClientStreetAddres" & _
        "s02, @LDClientSuburb, @LDClientState, @LDClientPostCode, @LDClientPhoneNumber, @" & _
        "LDClientFaxNumber, @LDClientMobileNumber, @LDJobAddressAsAbove, @LDJobStreetAddr" & _
        "ess01, @LDJobStreetAddress02, @LDJobSuburb, @LDJobState, @LDJobPostCode, @LDRefe" & _
        "renceName, @LSID, @LDHasAppointments, @LDUser, @LDJobDescription, @EXIDRep, @LDQ" & _
        "uoteReceived, @LDClientEmail); SELECT BRID, LDID, ID, LDDate, LDClientFirstName," & _
        " LDClientSurname, LDClientName, LDClientStreetAddress01, LDClientStreetAddress02" & _
        ", LDClientSuburb, LDClientState, LDClientPostCode, LDClientAddress, LDClientPhon" & _
        "eNumber, LDClientFaxNumber, LDClientMobileNumber, LDJobAddressAsAbove, LDJobStre" & _
        "etAddress01, LDJobStreetAddress02, LDJobSuburb, LDJobState, LDJobPostCode, LDJob" & _
        "Address, LDReferenceName, LSID, LDHasAppointments, LDUser, LDJobDescription, EXI" & _
        "DRep, LDQuoteReceived, LDClientEmail FROM Leads WHERE (BRID = @BRID) AND (LDID =" & _
        " @@IDENTITY)"
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDDate", System.Data.SqlDbType.DateTime, 8, "LDDate"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientFirstName", System.Data.SqlDbType.VarChar, 50, "LDClientFirstName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientSurname", System.Data.SqlDbType.VarChar, 50, "LDClientSurname"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, "LDClientStreetAddress01"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, "LDClientStreetAddress02"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientSuburb", System.Data.SqlDbType.VarChar, 50, "LDClientSuburb"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientState", System.Data.SqlDbType.VarChar, 3, "LDClientState"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientPostCode", System.Data.SqlDbType.VarChar, 20, "LDClientPostCode"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientPhoneNumber", System.Data.SqlDbType.VarChar, 20, "LDClientPhoneNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientFaxNumber", System.Data.SqlDbType.VarChar, 20, "LDClientFaxNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientMobileNumber", System.Data.SqlDbType.VarChar, 20, "LDClientMobileNumber"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, "LDJobAddressAsAbove"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, "LDJobStreetAddress01"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, "LDJobStreetAddress02"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobSuburb", System.Data.SqlDbType.VarChar, 50, "LDJobSuburb"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobState", System.Data.SqlDbType.VarChar, 3, "LDJobState"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobPostCode", System.Data.SqlDbType.VarChar, 20, "LDJobPostCode"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDReferenceName", System.Data.SqlDbType.VarChar, 200, "LDReferenceName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LSID", System.Data.SqlDbType.Int, 4, "LSID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDHasAppointments", System.Data.SqlDbType.Bit, 1, "LDHasAppointments"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDUser", System.Data.SqlDbType.VarChar, 50, "LDUser"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobDescription", System.Data.SqlDbType.VarChar, 50, "LDJobDescription"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDRep", System.Data.SqlDbType.Int, 4, "EXIDRep"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDQuoteReceived", System.Data.SqlDbType.Bit, 1, "LDQuoteReceived"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientEmail", System.Data.SqlDbType.VarChar, 50, "LDClientEmail"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, LDID, ID, LDDate, LDClientFirstName, LDClientSurname, LDClientName, " & _
        "LDClientStreetAddress01, LDClientStreetAddress02, LDClientSuburb, LDClientState," & _
        " LDClientPostCode, LDClientAddress, LDClientPhoneNumber, LDClientFaxNumber, LDCl" & _
        "ientMobileNumber, LDJobAddressAsAbove, LDJobStreetAddress01, LDJobStreetAddress0" & _
        "2, LDJobSuburb, LDJobState, LDJobPostCode, LDJobAddress, LDReferenceName, LSID, " & _
        "LDHasAppointments, LDUser, LDJobDescription, EXIDRep, LDQuoteReceived, LDClientE" & _
        "mail FROM Leads WHERE (BRID = @BRID) AND (LDID = @LDID)"
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDID", System.Data.SqlDbType.BigInt, 8, "LDID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Leads SET BRID = @BRID, ID = @ID, LDDate = @LDDate, LDClientFirstName = @L" & _
        "DClientFirstName, LDClientSurname = @LDClientSurname, LDClientStreetAddress01 = " & _
        "@LDClientStreetAddress01, LDClientStreetAddress02 = @LDClientStreetAddress02, LD" & _
        "ClientSuburb = @LDClientSuburb, LDClientState = @LDClientState, LDClientPostCode" & _
        " = @LDClientPostCode, LDClientPhoneNumber = @LDClientPhoneNumber, LDClientFaxNum" & _
        "ber = @LDClientFaxNumber, LDClientMobileNumber = @LDClientMobileNumber, LDJobAdd" & _
        "ressAsAbove = @LDJobAddressAsAbove, LDJobStreetAddress01 = @LDJobStreetAddress01" & _
        ", LDJobStreetAddress02 = @LDJobStreetAddress02, LDJobSuburb = @LDJobSuburb, LDJo" & _
        "bState = @LDJobState, LDJobPostCode = @LDJobPostCode, LDReferenceName = @LDRefer" & _
        "enceName, LSID = @LSID, LDHasAppointments = @LDHasAppointments, LDUser = @LDUser" & _
        ", LDJobDescription = @LDJobDescription, EXIDRep = @EXIDRep, LDQuoteReceived = @L" & _
        "DQuoteReceived, LDClientEmail = @LDClientEmail WHERE (BRID = @Original_BRID) AND" & _
        " (LDID = @Original_LDID) AND (EXIDRep = @Original_EXIDRep OR @Original_EXIDRep I" & _
        "S NULL AND EXIDRep IS NULL) AND (ID = @Original_ID) AND (LDClientEmail = @Origin" & _
        "al_LDClientEmail OR @Original_LDClientEmail IS NULL AND LDClientEmail IS NULL) A" & _
        "ND (LDClientFaxNumber = @Original_LDClientFaxNumber OR @Original_LDClientFaxNumb" & _
        "er IS NULL AND LDClientFaxNumber IS NULL) AND (LDClientFirstName = @Original_LDC" & _
        "lientFirstName OR @Original_LDClientFirstName IS NULL AND LDClientFirstName IS N" & _
        "ULL) AND (LDClientMobileNumber = @Original_LDClientMobileNumber OR @Original_LDC" & _
        "lientMobileNumber IS NULL AND LDClientMobileNumber IS NULL) AND (LDClientPhoneNu" & _
        "mber = @Original_LDClientPhoneNumber OR @Original_LDClientPhoneNumber IS NULL AN" & _
        "D LDClientPhoneNumber IS NULL) AND (LDClientPostCode = @Original_LDClientPostCod" & _
        "e OR @Original_LDClientPostCode IS NULL AND LDClientPostCode IS NULL) AND (LDCli" & _
        "entState = @Original_LDClientState OR @Original_LDClientState IS NULL AND LDClie" & _
        "ntState IS NULL) AND (LDClientStreetAddress01 = @Original_LDClientStreetAddress0" & _
        "1 OR @Original_LDClientStreetAddress01 IS NULL AND LDClientStreetAddress01 IS NU" & _
        "LL) AND (LDClientStreetAddress02 = @Original_LDClientStreetAddress02 OR @Origina" & _
        "l_LDClientStreetAddress02 IS NULL AND LDClientStreetAddress02 IS NULL) AND (LDCl" & _
        "ientSuburb = @Original_LDClientSuburb OR @Original_LDClientSuburb IS NULL AND LD" & _
        "ClientSuburb IS NULL) AND (LDClientSurname = @Original_LDClientSurname OR @Origi" & _
        "nal_LDClientSurname IS NULL AND LDClientSurname IS NULL) AND (LDDate = @Original" & _
        "_LDDate OR @Original_LDDate IS NULL AND LDDate IS NULL) AND (LDHasAppointments =" & _
        " @Original_LDHasAppointments OR @Original_LDHasAppointments IS NULL AND LDHasApp" & _
        "ointments IS NULL) AND (LDJobAddressAsAbove = @Original_LDJobAddressAsAbove OR @" & _
        "Original_LDJobAddressAsAbove IS NULL AND LDJobAddressAsAbove IS NULL) AND (LDJob" & _
        "Description = @Original_LDJobDescription OR @Original_LDJobDescription IS NULL A" & _
        "ND LDJobDescription IS NULL) AND (LDJobPostCode = @Original_LDJobPostCode OR @Or" & _
        "iginal_LDJobPostCode IS NULL AND LDJobPostCode IS NULL) AND (LDJobState = @Origi" & _
        "nal_LDJobState OR @Original_LDJobState IS NULL AND LDJobState IS NULL) AND (LDJo" & _
        "bStreetAddress01 = @Original_LDJobStreetAddress01 OR @Original_LDJobStreetAddres" & _
        "s01 IS NULL AND LDJobStreetAddress01 IS NULL) AND (LDJobStreetAddress02 = @Origi" & _
        "nal_LDJobStreetAddress02 OR @Original_LDJobStreetAddress02 IS NULL AND LDJobStre" & _
        "etAddress02 IS NULL) AND (LDJobSuburb = @Original_LDJobSuburb OR @Original_LDJob" & _
        "Suburb IS NULL AND LDJobSuburb IS NULL) AND (LDQuoteReceived = @Original_LDQuote" & _
        "Received OR @Original_LDQuoteReceived IS NULL AND LDQuoteReceived IS NULL) AND (" & _
        "LDReferenceName = @Original_LDReferenceName OR @Original_LDReferenceName IS NULL" & _
        " AND LDReferenceName IS NULL) AND (LDUser = @Original_LDUser OR @Original_LDUser" & _
        " IS NULL AND LDUser IS NULL) AND (LSID = @Original_LSID OR @Original_LSID IS NUL" & _
        "L AND LSID IS NULL); SELECT BRID, LDID, ID, LDDate, LDClientFirstName, LDClientS" & _
        "urname, LDClientName, LDClientStreetAddress01, LDClientStreetAddress02, LDClient" & _
        "Suburb, LDClientState, LDClientPostCode, LDClientAddress, LDClientPhoneNumber, L" & _
        "DClientFaxNumber, LDClientMobileNumber, LDJobAddressAsAbove, LDJobStreetAddress0" & _
        "1, LDJobStreetAddress02, LDJobSuburb, LDJobState, LDJobPostCode, LDJobAddress, L" & _
        "DReferenceName, LSID, LDHasAppointments, LDUser, LDJobDescription, EXIDRep, LDQu" & _
        "oteReceived, LDClientEmail FROM Leads WHERE (BRID = @BRID) AND (LDID = @LDID)"
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDDate", System.Data.SqlDbType.DateTime, 8, "LDDate"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientFirstName", System.Data.SqlDbType.VarChar, 50, "LDClientFirstName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientSurname", System.Data.SqlDbType.VarChar, 50, "LDClientSurname"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, "LDClientStreetAddress01"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, "LDClientStreetAddress02"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientSuburb", System.Data.SqlDbType.VarChar, 50, "LDClientSuburb"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientState", System.Data.SqlDbType.VarChar, 3, "LDClientState"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientPostCode", System.Data.SqlDbType.VarChar, 20, "LDClientPostCode"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientPhoneNumber", System.Data.SqlDbType.VarChar, 20, "LDClientPhoneNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientFaxNumber", System.Data.SqlDbType.VarChar, 20, "LDClientFaxNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientMobileNumber", System.Data.SqlDbType.VarChar, 20, "LDClientMobileNumber"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, "LDJobAddressAsAbove"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, "LDJobStreetAddress01"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, "LDJobStreetAddress02"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobSuburb", System.Data.SqlDbType.VarChar, 50, "LDJobSuburb"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobState", System.Data.SqlDbType.VarChar, 3, "LDJobState"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobPostCode", System.Data.SqlDbType.VarChar, 20, "LDJobPostCode"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDReferenceName", System.Data.SqlDbType.VarChar, 200, "LDReferenceName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LSID", System.Data.SqlDbType.Int, 4, "LSID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDHasAppointments", System.Data.SqlDbType.Bit, 1, "LDHasAppointments"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDUser", System.Data.SqlDbType.VarChar, 50, "LDUser"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDJobDescription", System.Data.SqlDbType.VarChar, 50, "LDJobDescription"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXIDRep", System.Data.SqlDbType.Int, 4, "EXIDRep"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDQuoteReceived", System.Data.SqlDbType.Bit, 1, "LDQuoteReceived"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDClientEmail", System.Data.SqlDbType.VarChar, 50, "LDClientEmail"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXIDRep", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXIDRep", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientEmail", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientEmail", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientFaxNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientFaxNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientFirstName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientFirstName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientMobileNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientMobileNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientPhoneNumber", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientPhoneNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDClientSurname", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDClientSurname", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDHasAppointments", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDHasAppointments", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobAddressAsAbove", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobAddressAsAbove", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobDescription", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobDescription", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobPostCode", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobPostCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobState", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobState", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobStreetAddress01", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobStreetAddress01", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobStreetAddress02", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobStreetAddress02", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDJobSuburb", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDJobSuburb", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDQuoteReceived", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDQuoteReceived", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDReferenceName", System.Data.SqlDbType.VarChar, 200, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDReferenceName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDUser", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDUser", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LSID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDID", System.Data.SqlDbType.BigInt, 8, "LDID"))
        '
        'ListBox
        '
        Me.ListBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ListBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ListBox.ItemHeight = 25
        Me.ListBox.Items.AddRange(New Object() {"Customer Information", "Job Address", "Enquiry Information", "Enquiry Sources", "Scheduling", "Notes"})
        Me.ListBox.Location = New System.Drawing.Point(8, 8)
        Me.ListBox.Name = "ListBox"
        Me.ListBox.Size = New System.Drawing.Size(136, 479)
        Me.ListBox.TabIndex = 0
        '
        'txtEXIDRep
        '
        Me.txtEXIDRep.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.EXIDRep"))
        Me.txtEXIDRep.Location = New System.Drawing.Point(144, 368)
        Me.txtEXIDRep.Name = "txtEXIDRep"
        '
        'txtEXIDRep.Properties
        '
        Me.txtEXIDRep.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtEXIDRep.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXName", "", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.txtEXIDRep.Properties.DataSource = Me.dvSalesReps
        Me.txtEXIDRep.Properties.DisplayMember = "EXName"
        Me.txtEXIDRep.Properties.NullText = ""
        Me.txtEXIDRep.Properties.ShowFooter = False
        Me.txtEXIDRep.Properties.ShowHeader = False
        Me.txtEXIDRep.Properties.ShowLines = False
        Me.txtEXIDRep.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.txtEXIDRep.Properties.ValueMember = "EXID"
        Me.txtEXIDRep.Size = New System.Drawing.Size(208, 20)
        Me.txtEXIDRep.TabIndex = 3
        '
        'dvSalesReps
        '
        Me.dvSalesReps.RowFilter = "EGType = 'RC'"
        Me.dvSalesReps.Sort = "EXName"
        Me.dvSalesReps.Table = Me.DsGTMS.Expenses
        '
        'txtLDJobDescription
        '
        Me.txtLDJobDescription.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.LDJobDescription"))
        Me.txtLDJobDescription.EditValue = ""
        Me.txtLDJobDescription.Location = New System.Drawing.Point(144, 256)
        Me.txtLDJobDescription.Name = "txtLDJobDescription"
        '
        'txtLDJobDescription.Properties
        '
        Me.txtLDJobDescription.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtLDJobDescription.Properties.ContextMenu = Me.cmHistory
        Me.txtLDJobDescription.Properties.MaxItemCount = 15
        Me.txtLDJobDescription.Size = New System.Drawing.Size(208, 20)
        Me.txtLDJobDescription.TabIndex = 2
        '
        'cmHistory
        '
        Me.cmHistory.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.miClearHistory})
        '
        'miClearHistory
        '
        Me.miClearHistory.Index = 0
        Me.miClearHistory.Text = "Clear History"
        '
        'chkLDQuoteReceived
        '
        Me.chkLDQuoteReceived.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.LDQuoteReceived"))
        Me.chkLDQuoteReceived.Location = New System.Drawing.Point(152, 456)
        Me.chkLDQuoteReceived.Name = "chkLDQuoteReceived"
        '
        'chkLDQuoteReceived.Properties
        '
        Me.chkLDQuoteReceived.Properties.Caption = "Quote received"
        Me.chkLDQuoteReceived.Size = New System.Drawing.Size(128, 18)
        Me.chkLDQuoteReceived.TabIndex = 4
        '
        'dpLDDate
        '
        Me.dpLDDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.LDDate"))
        Me.dpLDDate.EditValue = New Date(2005, 7, 21, 0, 0, 0, 0)
        Me.dpLDDate.Location = New System.Drawing.Point(152, 80)
        Me.dpLDDate.Name = "dpLDDate"
        '
        'dpLDDate.Properties
        '
        Me.dpLDDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False
        Me.dpLDDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpLDDate.Size = New System.Drawing.Size(152, 20)
        Me.dpLDDate.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(48, 56)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(304, 23)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Please enter the date the enquiry was recieved on."
        '
        'GroupLine1
        '
        Me.GroupLine1.Location = New System.Drawing.Point(16, 32)
        Me.GroupLine1.Name = "GroupLine1"
        Me.GroupLine1.Size = New System.Drawing.Size(336, 16)
        Me.GroupLine1.TabIndex = 0
        Me.GroupLine1.TextString = "Enquiry Date"
        Me.GroupLine1.TextWidth = 70
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(48, 80)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 23)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Enquiry date:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine2
        '
        Me.GroupLine2.Location = New System.Drawing.Point(16, 112)
        Me.GroupLine2.Name = "GroupLine2"
        Me.GroupLine2.Size = New System.Drawing.Size(336, 16)
        Me.GroupLine2.TabIndex = 0
        Me.GroupLine2.TextString = "Recorded by"
        Me.GroupLine2.TextWidth = 70
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(48, 136)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(304, 23)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Please enter the person who actually recorded the enquiry."
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(48, 168)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 23)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Recorded by:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine3
        '
        Me.GroupLine3.Location = New System.Drawing.Point(16, 208)
        Me.GroupLine3.Name = "GroupLine3"
        Me.GroupLine3.Size = New System.Drawing.Size(336, 16)
        Me.GroupLine3.TabIndex = 0
        Me.GroupLine3.TextString = "Job Description"
        Me.GroupLine3.TextWidth = 90
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(48, 232)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(296, 16)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "So the salesperson knows what they are going to quote."
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(48, 256)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(88, 23)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Job description:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine6
        '
        Me.GroupLine6.Location = New System.Drawing.Point(16, 296)
        Me.GroupLine6.Name = "GroupLine6"
        Me.GroupLine6.Size = New System.Drawing.Size(336, 16)
        Me.GroupLine6.TabIndex = 0
        Me.GroupLine6.TextString = "Salesperson"
        Me.GroupLine6.TextWidth = 90
        '
        'Label28
        '
        Me.Label28.Location = New System.Drawing.Point(48, 320)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(320, 40)
        Me.Label28.TabIndex = 1
        Me.Label28.Text = "Select the salesperson responsible for this appointment. This salesperson or any " & _
        "other salesperson who may be involved in this appointment can be scheduled in th" & _
        "e ""Scheduling"" section."
        '
        'Label29
        '
        Me.Label29.Location = New System.Drawing.Point(48, 424)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(304, 32)
        Me.Label29.TabIndex = 1
        Me.Label29.Text = "Tick if this salesperson has completed the quote and given it to administration."
        '
        'Label31
        '
        Me.Label31.Location = New System.Drawing.Point(48, 368)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(72, 23)
        Me.Label31.TabIndex = 1
        Me.Label31.Text = "Salesperson:"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine7
        '
        Me.GroupLine7.Location = New System.Drawing.Point(16, 400)
        Me.GroupLine7.Name = "GroupLine7"
        Me.GroupLine7.Size = New System.Drawing.Size(336, 16)
        Me.GroupLine7.TabIndex = 0
        Me.GroupLine7.TextString = "Quote Received"
        Me.GroupLine7.TextWidth = 90
        '
        'pnlMain
        '
        Me.pnlMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlMain.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.pnlMain.Controls.Add(Me.pnlJobAddress)
        Me.pnlMain.Controls.Add(Me.pnlScheduling)
        Me.pnlMain.Controls.Add(Me.pnlNotes)
        Me.pnlMain.Controls.Add(Me.pnlEnquirySources)
        Me.pnlMain.Controls.Add(Me.pnlEnquiryInformation)
        Me.pnlMain.Controls.Add(Me.pnlClientInformation)
        Me.pnlMain.Location = New System.Drawing.Point(152, 8)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(608, 480)
        Me.pnlMain.TabIndex = 1
        Me.pnlMain.Text = "PanelControl1"
        '
        'pnlScheduling
        '
        Me.pnlScheduling.Controls.Add(Me.Label16)
        Me.pnlScheduling.Controls.Add(Me.btnViewCalendar)
        Me.pnlScheduling.Controls.Add(Me.GroupLine9)
        Me.pnlScheduling.Controls.Add(Me.GroupLine8)
        Me.pnlScheduling.Controls.Add(Me.btnAddAppointment)
        Me.pnlScheduling.Controls.Add(Me.btnEditAppointment)
        Me.pnlScheduling.Controls.Add(Me.btnRemoveAppointment)
        Me.pnlScheduling.Controls.Add(Me.Label19)
        Me.pnlScheduling.Controls.Add(Me.dgAppointments)
        Me.pnlScheduling.Location = New System.Drawing.Point(32, 56)
        Me.pnlScheduling.Name = "pnlScheduling"
        Me.pnlScheduling.Size = New System.Drawing.Size(520, 400)
        Me.pnlScheduling.TabIndex = 19
        Me.pnlScheduling.Text = "Scheduling"
        '
        'Label16
        '
        Me.Label16.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label16.Location = New System.Drawing.Point(16, 80)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(480, 32)
        Me.Label16.TabIndex = 18
        Me.Label16.Text = "You can schedule appointments one of two ways.  You can interact directly with th" & _
        "e calendar by clicking 'View Calendar...' or you may enter into the list below b" & _
        "y clicking 'Add...'."
        '
        'btnViewCalendar
        '
        Me.btnViewCalendar.Location = New System.Drawing.Point(40, 144)
        Me.btnViewCalendar.Name = "btnViewCalendar"
        Me.btnViewCalendar.Size = New System.Drawing.Size(136, 23)
        Me.btnViewCalendar.TabIndex = 0
        Me.btnViewCalendar.Text = "View Calendar..."
        '
        'GroupLine9
        '
        Me.GroupLine9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupLine9.Location = New System.Drawing.Point(16, 184)
        Me.GroupLine9.Name = "GroupLine9"
        Me.GroupLine9.Size = New System.Drawing.Size(480, 16)
        Me.GroupLine9.TabIndex = 16
        Me.GroupLine9.TextString = "List View"
        Me.GroupLine9.TextWidth = 50
        '
        'GroupLine8
        '
        Me.GroupLine8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupLine8.Location = New System.Drawing.Point(16, 120)
        Me.GroupLine8.Name = "GroupLine8"
        Me.GroupLine8.Size = New System.Drawing.Size(480, 16)
        Me.GroupLine8.TabIndex = 15
        Me.GroupLine8.TextString = "Calendar View"
        Me.GroupLine8.TextWidth = 80
        '
        'btnAddAppointment
        '
        Me.btnAddAppointment.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddAppointment.Image = CType(resources.GetObject("btnAddAppointment.Image"), System.Drawing.Image)
        Me.btnAddAppointment.Location = New System.Drawing.Point(40, 368)
        Me.btnAddAppointment.Name = "btnAddAppointment"
        Me.btnAddAppointment.Size = New System.Drawing.Size(72, 23)
        Me.btnAddAppointment.TabIndex = 2
        Me.btnAddAppointment.Text = "Add..."
        '
        'btnEditAppointment
        '
        Me.btnEditAppointment.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEditAppointment.Image = CType(resources.GetObject("btnEditAppointment.Image"), System.Drawing.Image)
        Me.btnEditAppointment.Location = New System.Drawing.Point(120, 368)
        Me.btnEditAppointment.Name = "btnEditAppointment"
        Me.btnEditAppointment.Size = New System.Drawing.Size(72, 23)
        Me.btnEditAppointment.TabIndex = 3
        Me.btnEditAppointment.Text = "Edit..."
        '
        'btnRemoveAppointment
        '
        Me.btnRemoveAppointment.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveAppointment.Image = CType(resources.GetObject("btnRemoveAppointment.Image"), System.Drawing.Image)
        Me.btnRemoveAppointment.Location = New System.Drawing.Point(200, 368)
        Me.btnRemoveAppointment.Name = "btnRemoveAppointment"
        Me.btnRemoveAppointment.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveAppointment.TabIndex = 4
        Me.btnRemoveAppointment.Text = "Remove"
        '
        'Label19
        '
        Me.Label19.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label19.Location = New System.Drawing.Point(16, 32)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(480, 48)
        Me.Label19.TabIndex = 0
        Me.Label19.Text = "If necessary, this screen is used to schedule an appointment for the salesperson " & _
        "responsible for this quote request. You may schedule more than one salesperson t" & _
        "o this quote request. You may add appointments using the list or calendar view."
        '
        'dgAppointments
        '
        Me.dgAppointments.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgAppointments.DataSource = Me.dvAppointments
        '
        'dgAppointments.EmbeddedNavigator
        '
        Me.dgAppointments.EmbeddedNavigator.Name = ""
        Me.dgAppointments.Location = New System.Drawing.Point(40, 208)
        Me.dgAppointments.MainView = Me.gvAppointments
        Me.dgAppointments.Name = "dgAppointments"
        Me.dgAppointments.Size = New System.Drawing.Size(456, 152)
        Me.dgAppointments.TabIndex = 1
        Me.dgAppointments.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvAppointments})
        '
        'dvAppointments
        '
        Me.dvAppointments.Table = Me.DsCalendar.VAppointments
        '
        'DsCalendar
        '
        Me.DsCalendar.DataSetName = "dsCalendar"
        Me.DsCalendar.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'gvAppointments
        '
        Me.gvAppointments.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.gvAppointments.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvAppointments.Appearance.FocusedCell.Options.UseBackColor = True
        Me.gvAppointments.Appearance.FocusedCell.Options.UseForeColor = True
        Me.gvAppointments.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.gvAppointments.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gvAppointments.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvAppointments.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvAppointments.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colAPBegin, Me.colAPEnd, Me.colEXName})
        Me.gvAppointments.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.gvAppointments.GridControl = Me.dgAppointments
        Me.gvAppointments.Name = "gvAppointments"
        Me.gvAppointments.OptionsBehavior.Editable = False
        Me.gvAppointments.OptionsCustomization.AllowFilter = False
        Me.gvAppointments.OptionsNavigation.AutoFocusNewRow = True
        Me.gvAppointments.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.gvAppointments.OptionsView.ShowGroupPanel = False
        Me.gvAppointments.OptionsView.ShowHorzLines = False
        Me.gvAppointments.OptionsView.ShowIndicator = False
        Me.gvAppointments.OptionsView.ShowVertLines = False
        Me.gvAppointments.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colAPBegin, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colAPBegin
        '
        Me.colAPBegin.Caption = "Start Date and Time"
        Me.colAPBegin.DisplayFormat.FormatString = "g"
        Me.colAPBegin.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colAPBegin.FieldName = "APBegin"
        Me.colAPBegin.Name = "colAPBegin"
        Me.colAPBegin.Visible = True
        Me.colAPBegin.VisibleIndex = 0
        '
        'colAPEnd
        '
        Me.colAPEnd.Caption = "End Date and Time"
        Me.colAPEnd.DisplayFormat.FormatString = "g"
        Me.colAPEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colAPEnd.FieldName = "APEnd"
        Me.colAPEnd.Name = "colAPEnd"
        Me.colAPEnd.Visible = True
        Me.colAPEnd.VisibleIndex = 1
        '
        'colEXName
        '
        Me.colEXName.Caption = "Staff Member"
        Me.colEXName.FieldName = "EXName"
        Me.colEXName.Name = "colEXName"
        Me.colEXName.Visible = True
        Me.colEXName.VisibleIndex = 2
        '
        'pnlNotes
        '
        Me.pnlNotes.Controls.Add(Me.dgNotes)
        Me.pnlNotes.Location = New System.Drawing.Point(48, 24)
        Me.pnlNotes.Name = "pnlNotes"
        Me.pnlNotes.Size = New System.Drawing.Size(512, 424)
        Me.pnlNotes.TabIndex = 23
        Me.pnlNotes.Text = "Notes"
        '
        'dgNotes
        '
        Me.dgNotes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgNotes.DataSource = Me.DsGTMS.IDNotes
        '
        'dgNotes.EmbeddedNavigator
        '
        Me.dgNotes.EmbeddedNavigator.Name = ""
        Me.dgNotes.Location = New System.Drawing.Point(16, 32)
        Me.dgNotes.MainView = Me.gvNotes
        Me.dgNotes.Name = "dgNotes"
        Me.dgNotes.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtUser, Me.RepositoryItemMemoEdit1, Me.txtINDate})
        Me.dgNotes.Size = New System.Drawing.Size(480, 376)
        Me.dgNotes.TabIndex = 0
        Me.dgNotes.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvNotes})
        '
        'gvNotes
        '
        Me.gvNotes.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvNotes.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvNotes.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvNotes.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvNotes.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvNotes.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvNotes.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvNotes.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvNotes.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvNotes.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvNotes.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvNotes.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvNotes.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvNotes.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvNotes.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvNotes.Appearance.VertLine.Options.UseBackColor = True
        Me.gvNotes.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colINUser, Me.colINDate, Me.colINNotes})
        Me.gvNotes.GridControl = Me.dgNotes
        Me.gvNotes.Name = "gvNotes"
        Me.gvNotes.NewItemRowText = "Type here to add a new row"
        Me.gvNotes.OptionsCustomization.AllowFilter = False
        Me.gvNotes.OptionsCustomization.AllowRowSizing = True
        Me.gvNotes.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvNotes.OptionsView.RowAutoHeight = True
        Me.gvNotes.OptionsView.ShowGroupPanel = False
        Me.gvNotes.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colINDate, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colINUser
        '
        Me.colINUser.Caption = "User"
        Me.colINUser.ColumnEdit = Me.txtUser
        Me.colINUser.FieldName = "INUser"
        Me.colINUser.Name = "colINUser"
        Me.colINUser.Visible = True
        Me.colINUser.VisibleIndex = 1
        Me.colINUser.Width = 516
        '
        'txtUser
        '
        Me.txtUser.AutoHeight = False
        Me.txtUser.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXName")})
        Me.txtUser.DataSource = Me.dvExpenses
        Me.txtUser.DisplayMember = "EXName"
        Me.txtUser.Name = "txtUser"
        Me.txtUser.NullText = ""
        Me.txtUser.ShowFooter = False
        Me.txtUser.ShowHeader = False
        Me.txtUser.ShowLines = False
        Me.txtUser.ValueMember = "EXName"
        '
        'dvExpenses
        '
        Me.dvExpenses.RowFilter = "EGType <> 'OE'"
        Me.dvExpenses.Sort = "EXName"
        Me.dvExpenses.Table = Me.DsGTMS.Expenses
        '
        'colINDate
        '
        Me.colINDate.Caption = "Date"
        Me.colINDate.ColumnEdit = Me.txtINDate
        Me.colINDate.FieldName = "INDate"
        Me.colINDate.Name = "colINDate"
        Me.colINDate.Visible = True
        Me.colINDate.VisibleIndex = 0
        Me.colINDate.Width = 381
        '
        'txtINDate
        '
        Me.txtINDate.AutoHeight = False
        Me.txtINDate.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtINDate.Name = "txtINDate"
        '
        'colINNotes
        '
        Me.colINNotes.Caption = "Notes"
        Me.colINNotes.ColumnEdit = Me.RepositoryItemMemoEdit1
        Me.colINNotes.FieldName = "INNotes"
        Me.colINNotes.Name = "colINNotes"
        Me.colINNotes.Visible = True
        Me.colINNotes.VisibleIndex = 2
        Me.colINNotes.Width = 897
        '
        'RepositoryItemMemoEdit1
        '
        Me.RepositoryItemMemoEdit1.Name = "RepositoryItemMemoEdit1"
        '
        'pnlJobAddress
        '
        Me.pnlJobAddress.Controls.Add(Me.lblJobPostCode)
        Me.pnlJobAddress.Controls.Add(Me.txtLDJobStreetAddress01)
        Me.pnlJobAddress.Controls.Add(Me.rgLDJobAddressAsAbove)
        Me.pnlJobAddress.Controls.Add(Me.Label18)
        Me.pnlJobAddress.Controls.Add(Me.lblJobState)
        Me.pnlJobAddress.Controls.Add(Me.lblJobStreetAddress)
        Me.pnlJobAddress.Controls.Add(Me.lblJobSuburb)
        Me.pnlJobAddress.Controls.Add(Me.txtLDJobStreetAddress02)
        Me.pnlJobAddress.Controls.Add(Me.txtLDJobSuburb)
        Me.pnlJobAddress.Controls.Add(Me.txtLDJobState)
        Me.pnlJobAddress.Controls.Add(Me.txtLDJobPostCode)
        Me.pnlJobAddress.Location = New System.Drawing.Point(32, 48)
        Me.pnlJobAddress.Name = "pnlJobAddress"
        Me.pnlJobAddress.Size = New System.Drawing.Size(560, 336)
        Me.pnlJobAddress.TabIndex = 20
        Me.pnlJobAddress.Text = "Job Address"
        '
        'lblJobPostCode
        '
        Me.lblJobPostCode.Location = New System.Drawing.Point(304, 248)
        Me.lblJobPostCode.Name = "lblJobPostCode"
        Me.lblJobPostCode.Size = New System.Drawing.Size(96, 21)
        Me.lblJobPostCode.TabIndex = 32
        Me.lblJobPostCode.Text = "ZIP/postal code:"
        Me.lblJobPostCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLDJobStreetAddress01
        '
        Me.txtLDJobStreetAddress01.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.LDJobStreetAddress01"))
        Me.txtLDJobStreetAddress01.EditValue = ""
        Me.txtLDJobStreetAddress01.Location = New System.Drawing.Point(112, 152)
        Me.txtLDJobStreetAddress01.Name = "txtLDJobStreetAddress01"
        Me.txtLDJobStreetAddress01.Size = New System.Drawing.Size(376, 20)
        Me.txtLDJobStreetAddress01.TabIndex = 1
        '
        'rgLDJobAddressAsAbove
        '
        Me.rgLDJobAddressAsAbove.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.LDJobAddressAsAbove"))
        Me.rgLDJobAddressAsAbove.Location = New System.Drawing.Point(352, 56)
        Me.rgLDJobAddressAsAbove.Name = "rgLDJobAddressAsAbove"
        '
        'rgLDJobAddressAsAbove.Properties
        '
        Me.rgLDJobAddressAsAbove.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.rgLDJobAddressAsAbove.Properties.Appearance.Options.UseBackColor = True
        Me.rgLDJobAddressAsAbove.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.rgLDJobAddressAsAbove.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "Yes"), New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "No")})
        Me.rgLDJobAddressAsAbove.Size = New System.Drawing.Size(64, 56)
        Me.rgLDJobAddressAsAbove.TabIndex = 0
        '
        'Label18
        '
        Me.Label18.Location = New System.Drawing.Point(24, 72)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(328, 21)
        Me.Label18.TabIndex = 27
        Me.Label18.Text = "Is this job at the customer's billing address (same as previous)?"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobState
        '
        Me.lblJobState.Location = New System.Drawing.Point(24, 248)
        Me.lblJobState.Name = "lblJobState"
        Me.lblJobState.Size = New System.Drawing.Size(80, 21)
        Me.lblJobState.TabIndex = 24
        Me.lblJobState.Text = "State/region:"
        Me.lblJobState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobStreetAddress
        '
        Me.lblJobStreetAddress.Location = New System.Drawing.Point(24, 152)
        Me.lblJobStreetAddress.Name = "lblJobStreetAddress"
        Me.lblJobStreetAddress.Size = New System.Drawing.Size(88, 21)
        Me.lblJobStreetAddress.TabIndex = 26
        Me.lblJobStreetAddress.Text = "Street address:"
        Me.lblJobStreetAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobSuburb
        '
        Me.lblJobSuburb.Location = New System.Drawing.Point(24, 216)
        Me.lblJobSuburb.Name = "lblJobSuburb"
        Me.lblJobSuburb.Size = New System.Drawing.Size(80, 21)
        Me.lblJobSuburb.TabIndex = 23
        Me.lblJobSuburb.Text = "Suburb/town:"
        Me.lblJobSuburb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLDJobStreetAddress02
        '
        Me.txtLDJobStreetAddress02.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.LDJobStreetAddress02"))
        Me.txtLDJobStreetAddress02.EditValue = ""
        Me.txtLDJobStreetAddress02.Location = New System.Drawing.Point(112, 184)
        Me.txtLDJobStreetAddress02.Name = "txtLDJobStreetAddress02"
        Me.txtLDJobStreetAddress02.Size = New System.Drawing.Size(376, 20)
        Me.txtLDJobStreetAddress02.TabIndex = 2
        '
        'txtLDJobSuburb
        '
        Me.txtLDJobSuburb.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.LDJobSuburb"))
        Me.txtLDJobSuburb.EditValue = ""
        Me.txtLDJobSuburb.Location = New System.Drawing.Point(112, 216)
        Me.txtLDJobSuburb.Name = "txtLDJobSuburb"
        Me.txtLDJobSuburb.Size = New System.Drawing.Size(376, 20)
        Me.txtLDJobSuburb.TabIndex = 3
        '
        'txtLDJobState
        '
        Me.txtLDJobState.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.LDJobState"))
        Me.txtLDJobState.EditValue = ""
        Me.txtLDJobState.Location = New System.Drawing.Point(112, 248)
        Me.txtLDJobState.Name = "txtLDJobState"
        Me.txtLDJobState.Size = New System.Drawing.Size(88, 20)
        Me.txtLDJobState.TabIndex = 4
        '
        'txtLDJobPostCode
        '
        Me.txtLDJobPostCode.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.LDJobPostCode"))
        Me.txtLDJobPostCode.EditValue = ""
        Me.txtLDJobPostCode.Location = New System.Drawing.Point(400, 248)
        Me.txtLDJobPostCode.Name = "txtLDJobPostCode"
        Me.txtLDJobPostCode.Size = New System.Drawing.Size(88, 20)
        Me.txtLDJobPostCode.TabIndex = 5
        '
        'pnlEnquirySources
        '
        Me.pnlEnquirySources.Controls.Add(Me.GridControl1)
        Me.pnlEnquirySources.Controls.Add(Me.Label26)
        Me.pnlEnquirySources.Controls.Add(Me.Label23)
        Me.pnlEnquirySources.Controls.Add(Me.GroupLine4)
        Me.pnlEnquirySources.Controls.Add(Me.Label24)
        Me.pnlEnquirySources.Controls.Add(Me.GroupLine5)
        Me.pnlEnquirySources.Controls.Add(Me.Label25)
        Me.pnlEnquirySources.Controls.Add(Me.txtLSName)
        Me.pnlEnquirySources.Location = New System.Drawing.Point(32, 8)
        Me.pnlEnquirySources.Name = "pnlEnquirySources"
        Me.pnlEnquirySources.Size = New System.Drawing.Size(552, 464)
        Me.pnlEnquirySources.TabIndex = 21
        Me.pnlEnquirySources.Text = "Enquiry Sources"
        '
        'GridControl1
        '
        Me.GridControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GridControl1.DataSource = Me.DsGTMS.VLeads_SubSources
        '
        'GridControl1.EmbeddedNavigator
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.Location = New System.Drawing.Point(40, 256)
        Me.GridControl1.MainView = Me.GridView2
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(440, 200)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2, Me.GridView1})
        '
        'GridView2
        '
        Me.GridView2.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView2.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView2.Appearance.FocusedRow.Options.UseBackColor = True
        Me.GridView2.Appearance.FocusedRow.Options.UseForeColor = True
        Me.GridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView2.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView2.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView2.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.GridView2.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.GridView2.Appearance.HorzLine.Options.UseBackColor = True
        Me.GridView2.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView2.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView2.Appearance.SelectedRow.Options.UseBackColor = True
        Me.GridView2.Appearance.SelectedRow.Options.UseForeColor = True
        Me.GridView2.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.GridView2.Appearance.VertLine.Options.UseBackColor = True
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSSName, Me.colSSUserSelected})
        Me.GridView2.GridControl = Me.GridControl1
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsCustomization.AllowFilter = False
        Me.GridView2.OptionsView.ShowGroupPanel = False
        Me.GridView2.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colSSName, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colSSName
        '
        Me.colSSName.Caption = "Description"
        Me.colSSName.FieldName = "SSName"
        Me.colSSName.Name = "colSSName"
        Me.colSSName.OptionsColumn.AllowEdit = False
        Me.colSSName.OptionsColumn.AllowFocus = False
        Me.colSSName.Visible = True
        Me.colSSName.VisibleIndex = 0
        Me.colSSName.Width = 399
        '
        'colSSUserSelected
        '
        Me.colSSUserSelected.FieldName = "SSUserSelected"
        Me.colSSUserSelected.Name = "colSSUserSelected"
        Me.colSSUserSelected.Visible = True
        Me.colSSUserSelected.VisibleIndex = 1
        Me.colSSUserSelected.Width = 91
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        '
        'Label26
        '
        Me.Label26.Location = New System.Drawing.Point(40, 232)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(104, 23)
        Me.Label26.TabIndex = 1
        Me.Label26.Text = "Available sources:"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label23
        '
        Me.Label23.Location = New System.Drawing.Point(40, 56)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(440, 32)
        Me.Label23.TabIndex = 1
        Me.Label23.Text = "Primary enquiry sources are preset in the program. Because they are common to all" & _
        " franchisees, group information can be collated. "
        '
        'GroupLine4
        '
        Me.GroupLine4.Location = New System.Drawing.Point(16, 32)
        Me.GroupLine4.Name = "GroupLine4"
        Me.GroupLine4.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine4.TabIndex = 0
        Me.GroupLine4.TextString = "Primary Source"
        Me.GroupLine4.TextWidth = 90
        '
        'Label24
        '
        Me.Label24.Location = New System.Drawing.Point(40, 104)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(128, 23)
        Me.Label24.TabIndex = 1
        Me.Label24.Text = "Primary enquiry source:"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine5
        '
        Me.GroupLine5.Location = New System.Drawing.Point(16, 152)
        Me.GroupLine5.Name = "GroupLine5"
        Me.GroupLine5.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine5.TabIndex = 0
        Me.GroupLine5.TextString = "Secondary Source"
        Me.GroupLine5.TextWidth = 100
        '
        'Label25
        '
        Me.Label25.Location = New System.Drawing.Point(40, 176)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(440, 56)
        Me.Label25.TabIndex = 1
        Me.Label25.Text = "A secondary enquiry source allows you to add any additional information about the" & _
        " primary enquiry source, for market research purposes. For example you may wish " & _
        "to add the name of the shopping center, the name of the person on the display an" & _
        "d what sex they are. "
        '
        'txtLSName
        '
        Me.txtLSName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.LSID"))
        Me.txtLSName.Location = New System.Drawing.Point(184, 104)
        Me.txtLSName.Name = "txtLSName"
        '
        'txtLSName.Properties
        '
        Me.txtLSName.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtLSName.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("LSName")})
        Me.txtLSName.Properties.DataSource = Me.DsGTMS.LeadSources
        Me.txtLSName.Properties.DisplayMember = "LSName"
        Me.txtLSName.Properties.NullText = ""
        Me.txtLSName.Properties.ShowFooter = False
        Me.txtLSName.Properties.ShowHeader = False
        Me.txtLSName.Properties.ShowLines = False
        Me.txtLSName.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.txtLSName.Properties.ValueMember = "LSID"
        Me.txtLSName.Size = New System.Drawing.Size(200, 20)
        Me.txtLSName.TabIndex = 0
        '
        'pnlEnquiryInformation
        '
        Me.pnlEnquiryInformation.Controls.Add(Me.txtLDUser)
        Me.pnlEnquiryInformation.Controls.Add(Me.txtLDJobDescription)
        Me.pnlEnquiryInformation.Controls.Add(Me.chkLDQuoteReceived)
        Me.pnlEnquiryInformation.Controls.Add(Me.dpLDDate)
        Me.pnlEnquiryInformation.Controls.Add(Me.Label1)
        Me.pnlEnquiryInformation.Controls.Add(Me.GroupLine1)
        Me.pnlEnquiryInformation.Controls.Add(Me.Label2)
        Me.pnlEnquiryInformation.Controls.Add(Me.GroupLine2)
        Me.pnlEnquiryInformation.Controls.Add(Me.Label3)
        Me.pnlEnquiryInformation.Controls.Add(Me.Label6)
        Me.pnlEnquiryInformation.Controls.Add(Me.GroupLine6)
        Me.pnlEnquiryInformation.Controls.Add(Me.Label28)
        Me.pnlEnquiryInformation.Controls.Add(Me.Label29)
        Me.pnlEnquiryInformation.Controls.Add(Me.Label31)
        Me.pnlEnquiryInformation.Controls.Add(Me.GroupLine7)
        Me.pnlEnquiryInformation.Controls.Add(Me.Label4)
        Me.pnlEnquiryInformation.Controls.Add(Me.GroupLine3)
        Me.pnlEnquiryInformation.Controls.Add(Me.Label5)
        Me.pnlEnquiryInformation.Controls.Add(Me.txtEXIDRep)
        Me.pnlEnquiryInformation.Location = New System.Drawing.Point(64, 8)
        Me.pnlEnquiryInformation.Name = "pnlEnquiryInformation"
        Me.pnlEnquiryInformation.Size = New System.Drawing.Size(528, 480)
        Me.pnlEnquiryInformation.TabIndex = 18
        Me.pnlEnquiryInformation.Text = "Enquiry Information"
        '
        'txtLDUser
        '
        Me.txtLDUser.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.LDUser"))
        Me.txtLDUser.Location = New System.Drawing.Point(144, 168)
        Me.txtLDUser.Name = "txtLDUser"
        '
        'txtLDUser.Properties
        '
        Me.txtLDUser.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtLDUser.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXName", "", 45, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.txtLDUser.Properties.DataSource = Me.dvExpenses
        Me.txtLDUser.Properties.DisplayMember = "EXName"
        Me.txtLDUser.Properties.NullText = ""
        Me.txtLDUser.Properties.ShowFooter = False
        Me.txtLDUser.Properties.ShowHeader = False
        Me.txtLDUser.Properties.ShowLines = False
        Me.txtLDUser.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.txtLDUser.Properties.ValueMember = "EXName"
        Me.txtLDUser.Size = New System.Drawing.Size(208, 20)
        Me.txtLDUser.TabIndex = 1
        '
        'pnlClientInformation
        '
        Me.pnlClientInformation.Controls.Add(Me.Label10)
        Me.pnlClientInformation.Controls.Add(Me.Label17)
        Me.pnlClientInformation.Controls.Add(Me.txtLDClientEmail)
        Me.pnlClientInformation.Controls.Add(Me.txtLDClientSurname)
        Me.pnlClientInformation.Controls.Add(Me.GroupLine19)
        Me.pnlClientInformation.Controls.Add(Me.GroupLine17)
        Me.pnlClientInformation.Controls.Add(Me.Label7)
        Me.pnlClientInformation.Controls.Add(Me.Label8)
        Me.pnlClientInformation.Controls.Add(Me.Label9)
        Me.pnlClientInformation.Controls.Add(Me.Label11)
        Me.pnlClientInformation.Controls.Add(Me.Label12)
        Me.pnlClientInformation.Controls.Add(Me.Label13)
        Me.pnlClientInformation.Controls.Add(Me.Label14)
        Me.pnlClientInformation.Controls.Add(Me.Label15)
        Me.pnlClientInformation.Controls.Add(Me.GroupLine18)
        Me.pnlClientInformation.Controls.Add(Me.Label30)
        Me.pnlClientInformation.Controls.Add(Me.txtLDClientFirstName)
        Me.pnlClientInformation.Controls.Add(Me.txtLDReferenceName)
        Me.pnlClientInformation.Controls.Add(Me.txtLDClientStreetAddress01)
        Me.pnlClientInformation.Controls.Add(Me.txtLDClientStreetAddress02)
        Me.pnlClientInformation.Controls.Add(Me.txtLDClientSuburb)
        Me.pnlClientInformation.Controls.Add(Me.txtLDClientState)
        Me.pnlClientInformation.Controls.Add(Me.txtLDClientPostCode)
        Me.pnlClientInformation.Controls.Add(Me.txtLDClientPhoneNumber)
        Me.pnlClientInformation.Controls.Add(Me.txtLDClientFaxNumber)
        Me.pnlClientInformation.Controls.Add(Me.txtLDClientMobileNumber)
        Me.pnlClientInformation.Location = New System.Drawing.Point(40, 8)
        Me.pnlClientInformation.Name = "pnlClientInformation"
        Me.pnlClientInformation.Size = New System.Drawing.Size(552, 472)
        Me.pnlClientInformation.TabIndex = 22
        Me.pnlClientInformation.Text = "Customer Information"
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(296, 264)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(96, 21)
        Me.Label10.TabIndex = 32
        Me.Label10.Text = "ZIP/postal code:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label17
        '
        Me.Label17.Location = New System.Drawing.Point(40, 416)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(80, 21)
        Me.Label17.TabIndex = 30
        Me.Label17.Text = "Email:"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLDClientEmail
        '
        Me.txtLDClientEmail.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.LDClientEmail"))
        Me.txtLDClientEmail.EditValue = ""
        Me.txtLDClientEmail.Location = New System.Drawing.Point(184, 416)
        Me.txtLDClientEmail.Name = "txtLDClientEmail"
        Me.txtLDClientEmail.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientEmail.TabIndex = 11
        '
        'txtLDClientSurname
        '
        Me.txtLDClientSurname.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.LDClientSurname"))
        Me.txtLDClientSurname.EditValue = ""
        Me.txtLDClientSurname.Location = New System.Drawing.Point(184, 48)
        Me.txtLDClientSurname.Name = "txtLDClientSurname"
        Me.txtLDClientSurname.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientSurname.TabIndex = 0
        '
        'GroupLine19
        '
        Me.GroupLine19.Location = New System.Drawing.Point(16, 24)
        Me.GroupLine19.Name = "GroupLine19"
        Me.GroupLine19.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine19.TabIndex = 25
        Me.GroupLine19.TextString = "Name"
        Me.GroupLine19.TextWidth = 35
        '
        'GroupLine17
        '
        Me.GroupLine17.Location = New System.Drawing.Point(16, 144)
        Me.GroupLine17.Name = "GroupLine17"
        Me.GroupLine17.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine17.TabIndex = 27
        Me.GroupLine17.TextString = "Billing Address"
        Me.GroupLine17.TextWidth = 90
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(40, 80)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(72, 21)
        Me.Label7.TabIndex = 20
        Me.Label7.Text = "First name:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(40, 48)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(144, 21)
        Me.Label8.TabIndex = 19
        Me.Label8.Text = "Surname / business name:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(40, 264)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(136, 21)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "State/region:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(40, 168)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(80, 21)
        Me.Label11.TabIndex = 21
        Me.Label11.Text = "Street address:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(40, 232)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(80, 21)
        Me.Label12.TabIndex = 22
        Me.Label12.Text = "Suburb/town:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label13
        '
        Me.Label13.Location = New System.Drawing.Point(40, 320)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(80, 21)
        Me.Label13.TabIndex = 16
        Me.Label13.Text = "Phone:"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.Location = New System.Drawing.Point(40, 352)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(80, 21)
        Me.Label14.TabIndex = 17
        Me.Label14.Text = "Fax:"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label15
        '
        Me.Label15.Location = New System.Drawing.Point(40, 384)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(80, 21)
        Me.Label15.TabIndex = 14
        Me.Label15.Text = "Mobile:"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupLine18
        '
        Me.GroupLine18.Location = New System.Drawing.Point(16, 296)
        Me.GroupLine18.Name = "GroupLine18"
        Me.GroupLine18.Size = New System.Drawing.Size(464, 16)
        Me.GroupLine18.TabIndex = 26
        Me.GroupLine18.TextString = "Contact Details"
        Me.GroupLine18.TextWidth = 79
        '
        'Label30
        '
        Me.Label30.Location = New System.Drawing.Point(40, 112)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(96, 21)
        Me.Label30.TabIndex = 15
        Me.Label30.Text = "Reference name:"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLDClientFirstName
        '
        Me.txtLDClientFirstName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.LDClientFirstName"))
        Me.txtLDClientFirstName.EditValue = ""
        Me.txtLDClientFirstName.Location = New System.Drawing.Point(184, 80)
        Me.txtLDClientFirstName.Name = "txtLDClientFirstName"
        Me.txtLDClientFirstName.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientFirstName.TabIndex = 1
        '
        'txtLDReferenceName
        '
        Me.txtLDReferenceName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.LDReferenceName"))
        Me.txtLDReferenceName.EditValue = ""
        Me.txtLDReferenceName.Location = New System.Drawing.Point(184, 112)
        Me.txtLDReferenceName.Name = "txtLDReferenceName"
        Me.txtLDReferenceName.Size = New System.Drawing.Size(296, 20)
        Me.txtLDReferenceName.TabIndex = 2
        '
        'txtLDClientStreetAddress01
        '
        Me.txtLDClientStreetAddress01.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.LDClientStreetAddress01"))
        Me.txtLDClientStreetAddress01.EditValue = ""
        Me.txtLDClientStreetAddress01.Location = New System.Drawing.Point(184, 168)
        Me.txtLDClientStreetAddress01.Name = "txtLDClientStreetAddress01"
        Me.txtLDClientStreetAddress01.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientStreetAddress01.TabIndex = 3
        '
        'txtLDClientStreetAddress02
        '
        Me.txtLDClientStreetAddress02.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.LDClientStreetAddress02"))
        Me.txtLDClientStreetAddress02.EditValue = ""
        Me.txtLDClientStreetAddress02.Location = New System.Drawing.Point(184, 200)
        Me.txtLDClientStreetAddress02.Name = "txtLDClientStreetAddress02"
        Me.txtLDClientStreetAddress02.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientStreetAddress02.TabIndex = 4
        '
        'txtLDClientSuburb
        '
        Me.txtLDClientSuburb.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.LDClientSuburb"))
        Me.txtLDClientSuburb.EditValue = ""
        Me.txtLDClientSuburb.Location = New System.Drawing.Point(184, 232)
        Me.txtLDClientSuburb.Name = "txtLDClientSuburb"
        Me.txtLDClientSuburb.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientSuburb.TabIndex = 5
        '
        'txtLDClientState
        '
        Me.txtLDClientState.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.LDClientState"))
        Me.txtLDClientState.EditValue = ""
        Me.txtLDClientState.Location = New System.Drawing.Point(184, 264)
        Me.txtLDClientState.Name = "txtLDClientState"
        Me.txtLDClientState.Size = New System.Drawing.Size(88, 20)
        Me.txtLDClientState.TabIndex = 6
        '
        'txtLDClientPostCode
        '
        Me.txtLDClientPostCode.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.LDClientPostCode"))
        Me.txtLDClientPostCode.EditValue = ""
        Me.txtLDClientPostCode.Location = New System.Drawing.Point(392, 264)
        Me.txtLDClientPostCode.Name = "txtLDClientPostCode"
        Me.txtLDClientPostCode.Size = New System.Drawing.Size(88, 20)
        Me.txtLDClientPostCode.TabIndex = 7
        '
        'txtLDClientPhoneNumber
        '
        Me.txtLDClientPhoneNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.LDClientPhoneNumber"))
        Me.txtLDClientPhoneNumber.EditValue = ""
        Me.txtLDClientPhoneNumber.Location = New System.Drawing.Point(184, 320)
        Me.txtLDClientPhoneNumber.Name = "txtLDClientPhoneNumber"
        Me.txtLDClientPhoneNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientPhoneNumber.TabIndex = 8
        '
        'txtLDClientFaxNumber
        '
        Me.txtLDClientFaxNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.LDClientFaxNumber"))
        Me.txtLDClientFaxNumber.EditValue = ""
        Me.txtLDClientFaxNumber.Location = New System.Drawing.Point(184, 352)
        Me.txtLDClientFaxNumber.Name = "txtLDClientFaxNumber"
        Me.txtLDClientFaxNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientFaxNumber.TabIndex = 9
        '
        'txtLDClientMobileNumber
        '
        Me.txtLDClientMobileNumber.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Leads.LDClientMobileNumber"))
        Me.txtLDClientMobileNumber.EditValue = ""
        Me.txtLDClientMobileNumber.Location = New System.Drawing.Point(184, 384)
        Me.txtLDClientMobileNumber.Name = "txtLDClientMobileNumber"
        Me.txtLDClientMobileNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtLDClientMobileNumber.TabIndex = 10
        '
        'daLeadSources
        '
        Me.daLeadSources.DeleteCommand = Me.SqlDeleteCommand2
        Me.daLeadSources.InsertCommand = Me.SqlInsertCommand2
        Me.daLeadSources.SelectCommand = Me.SqlSelectCommand2
        Me.daLeadSources.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "LeadSources", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("LSID", "LSID"), New System.Data.Common.DataColumnMapping("LSName", "LSName")})})
        Me.daLeadSources.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM LeadSources WHERE (LSID = @Original_LSID) AND (LSName = @Original_LSN" & _
        "ame OR @Original_LSName IS NULL AND LSName IS NULL)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LSID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LSName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LSName", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO LeadSources(LSID, LSName) VALUES (@LSID, @LSName); SELECT LSID, LSNam" & _
        "e FROM LeadSources WHERE (LSID = @LSID)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LSID", System.Data.SqlDbType.Int, 4, "LSID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LSName", System.Data.SqlDbType.VarChar, 50, "LSName"))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT LSID, LSName FROM LeadSources"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE LeadSources SET LSID = @LSID, LSName = @LSName WHERE (LSID = @Original_LSI" & _
        "D) AND (LSName = @Original_LSName OR @Original_LSName IS NULL AND LSName IS NULL" & _
        "); SELECT LSID, LSName FROM LeadSources WHERE (LSID = @LSID)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LSID", System.Data.SqlDbType.Int, 4, "LSID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LSName", System.Data.SqlDbType.VarChar, 50, "LSName"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LSID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LSName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LSName", System.Data.DataRowVersion.Original, Nothing))
        '
        'daExpenses
        '
        Me.daExpenses.DeleteCommand = Me.SqlDeleteCommand3
        Me.daExpenses.InsertCommand = Me.SqlInsertCommand3
        Me.daExpenses.SelectCommand = Me.SqlSelectCommand3
        Me.daExpenses.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Expenses", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("EXName", "EXName")})})
        Me.daExpenses.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM Expenses WHERE (BRID = @Original_BRID) AND (EXID = @Original_EXID) AN" & _
        "D (EXName = @Original_EXName OR @Original_EXName IS NULL AND EXName IS NULL)"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXName", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO Expenses (BRID, EXName) VALUES (@BRID, @EXName); SELECT BRID, EXID, E" & _
        "XName, EGType, EXAppearInCalendar, EXCalendarName, EGName FROM VExpenses WHERE (" & _
        "BRID = @BRID) AND (EXID = @@IDENTITY)"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT BRID, EXID, EXName, EGType, EXAppearInCalendar, EXCalendarName, EGName FRO" & _
        "M VExpenses WHERE (EXDiscontinued = 0) AND (BRID = @BRID)"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection
        Me.SqlSelectCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE Expenses SET BRID = @BRID, EXName = @EXName WHERE (BRID = @Original_BRID) " & _
        "AND (EXID = @Original_EXID) AND (EXName = @Original_EXName OR @Original_EXName I" & _
        "S NULL AND EXName IS NULL); SELECT BRID, EXID, EXName, EGType, EXAppearInCalenda" & _
        "r, EXCalendarName, EGName FROM VExpenses WHERE (BRID = @BRID) AND (EXID = @EXID)" & _
        ""
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton1.Image = CType(resources.GetObject("SimpleButton1.Image"), System.Drawing.Image)
        Me.SimpleButton1.Location = New System.Drawing.Point(8, 496)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(72, 23)
        Me.SimpleButton1.TabIndex = 2
        Me.SimpleButton1.Text = "Help"
        '
        'daAppointments
        '
        Me.daAppointments.DeleteCommand = Me.SqlDeleteCommand4
        Me.daAppointments.InsertCommand = Me.SqlInsertCommand4
        Me.daAppointments.SelectCommand = Me.SqlSelectCommand4
        Me.daAppointments.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VAppointments", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("APID", "APID"), New System.Data.Common.DataColumnMapping("APBegin", "APBegin"), New System.Data.Common.DataColumnMapping("APEnd", "APEnd"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("APType", "APType"), New System.Data.Common.DataColumnMapping("APTypeID", "APTypeID"), New System.Data.Common.DataColumnMapping("APNotes", "APNotes"), New System.Data.Common.DataColumnMapping("APStatus", "APStatus"), New System.Data.Common.DataColumnMapping("APTask", "APTask")})})
        Me.daAppointments.UpdateCommand = Me.SqlUpdateCommand4
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = "DELETE FROM Appointments WHERE (APID = @Original_APID) AND (BRID = @Original_BRID" & _
        ") AND (APBegin = @Original_APBegin OR @Original_APBegin IS NULL AND APBegin IS N" & _
        "ULL) AND (APEnd = @Original_APEnd OR @Original_APEnd IS NULL AND APEnd IS NULL) " & _
        "AND (APNotes = @Original_APNotes OR @Original_APNotes IS NULL AND APNotes IS NUL" & _
        "L) AND (APStatus = @Original_APStatus OR @Original_APStatus IS NULL AND APStatus" & _
        " IS NULL) AND (APTask = @Original_APTask OR @Original_APTask IS NULL AND APTask " & _
        "IS NULL) AND (APType = @Original_APType OR @Original_APType IS NULL AND APType I" & _
        "S NULL) AND (APTypeID = @Original_APTypeID OR @Original_APTypeID IS NULL AND APT" & _
        "ypeID IS NULL) AND (EXID = @Original_EXID OR @Original_EXID IS NULL AND EXID IS " & _
        "NULL)"
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APBegin", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APBegin", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APEnd", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APEnd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APNotes", System.Data.SqlDbType.VarChar, 2000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APStatus", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APStatus", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APTask", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APTask", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APTypeID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APTypeID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = "INSERT INTO Appointments (BRID, APBegin, APEnd, EXID, APType, APTypeID, APNotes, " & _
        "APStatus, APTask) VALUES (@BRID, @APBegin, @APEnd, @EXID, @APType, @APTypeID, @A" & _
        "PNotes, @APStatus, @APTask); SELECT BRID, APID, APBegin, APEnd, EXID, APType, AP" & _
        "TypeID, APNotes, APStatus, APTask, APDescription, APAddressMultiline, ID, APClie" & _
        "ntName, EXName, APJobDescription, APClientPhoneNumber, APClientMobileNumber, APS" & _
        "uburb FROM VAppointments WHERE (APID = @@IDENTITY) AND (BRID = @BRID)"
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APBegin", System.Data.SqlDbType.DateTime, 8, "APBegin"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APEnd", System.Data.SqlDbType.DateTime, 8, "APEnd"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APType", System.Data.SqlDbType.VarChar, 2, "APType"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTypeID", System.Data.SqlDbType.BigInt, 8, "APTypeID"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APNotes", System.Data.SqlDbType.VarChar, 2000, "APNotes"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APStatus", System.Data.SqlDbType.Int, 4, "APStatus"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTask", System.Data.SqlDbType.Int, 4, "APTask"))
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT BRID, APID, APBegin, APEnd, EXID, APType, APTypeID, APNotes, APStatus, APT" & _
        "ask, APDescription, APAddressMultiline, ID, APClientName, EXName, APJobDescripti" & _
        "on, APClientPhoneNumber, APClientMobileNumber, APSuburb FROM VAppointments WHERE" & _
        " (BRID = @BRID) AND (APIsCancelled = 0)"
        Me.SqlSelectCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = "UPDATE Appointments SET BRID = @BRID, APBegin = @APBegin, APEnd = @APEnd, EXID = " & _
        "@EXID, APType = @APType, APTypeID = @APTypeID, APNotes = @APNotes, APStatus = @A" & _
        "PStatus, APTask = @APTask WHERE (APID = @Original_APID) AND (BRID = @Original_BR" & _
        "ID) AND (APBegin = @Original_APBegin OR @Original_APBegin IS NULL AND APBegin IS" & _
        " NULL) AND (APEnd = @Original_APEnd OR @Original_APEnd IS NULL AND APEnd IS NULL" & _
        ") AND (APNotes = @Original_APNotes OR @Original_APNotes IS NULL AND APNotes IS N" & _
        "ULL) AND (APStatus = @Original_APStatus OR @Original_APStatus IS NULL AND APStat" & _
        "us IS NULL) AND (APTask = @Original_APTask OR @Original_APTask IS NULL AND APTas" & _
        "k IS NULL) AND (APType = @Original_APType OR @Original_APType IS NULL AND APType" & _
        " IS NULL) AND (APTypeID = @Original_APTypeID OR @Original_APTypeID IS NULL AND A" & _
        "PTypeID IS NULL) AND (EXID = @Original_EXID OR @Original_EXID IS NULL AND EXID I" & _
        "S NULL); SELECT BRID, APID, APBegin, APEnd, EXID, APType, APTypeID, APNotes, APS" & _
        "tatus, APTask, APDescription, APAddressMultiline, ID, APClientName, EXName, APJo" & _
        "bDescription, APClientPhoneNumber, APClientMobileNumber, APSuburb FROM VAppointm" & _
        "ents WHERE (APID = @APID) AND (BRID = @BRID)"
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APBegin", System.Data.SqlDbType.DateTime, 8, "APBegin"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APEnd", System.Data.SqlDbType.DateTime, 8, "APEnd"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APType", System.Data.SqlDbType.VarChar, 2, "APType"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTypeID", System.Data.SqlDbType.BigInt, 8, "APTypeID"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APNotes", System.Data.SqlDbType.VarChar, 2000, "APNotes"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APStatus", System.Data.SqlDbType.Int, 4, "APStatus"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTask", System.Data.SqlDbType.Int, 4, "APTask"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APBegin", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APBegin", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APEnd", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APEnd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APNotes", System.Data.SqlDbType.VarChar, 2000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APStatus", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APStatus", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APTask", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APTask", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APTypeID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APTypeID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APID", System.Data.SqlDbType.BigInt, 8, "APID"))
        '
        'daLeadsSubSources
        '
        Me.daLeadsSubSources.DeleteCommand = Me.SqlDeleteCommand5
        Me.daLeadsSubSources.InsertCommand = Me.SqlInsertCommand5
        Me.daLeadsSubSources.SelectCommand = Me.SqlSelectCommand5
        Me.daLeadsSubSources.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "vLeads_SubSources", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("LDID", "LDID"), New System.Data.Common.DataColumnMapping("SSID", "SSID"), New System.Data.Common.DataColumnMapping("SSUserSelected", "SSUserSelected")})})
        Me.daLeadsSubSources.UpdateCommand = Me.SqlUpdateCommand5
        '
        'SqlDeleteCommand5
        '
        Me.SqlDeleteCommand5.CommandText = "DELETE FROM Leads_SubSources WHERE (BRID = @Original_BRID) AND (LDID = @Original_" & _
        "LDID) AND (SSID = @Original_SSID) AND (SSUserSelected = @Original_SSUserSelected" & _
        " OR @Original_SSUserSelected IS NULL AND SSUserSelected IS NULL)"
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SSID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SSUserSelected", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SSUserSelected", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand5
        '
        Me.SqlInsertCommand5.CommandText = "INSERT INTO Leads_SubSources(BRID, LDID, SSID, SSUserSelected) VALUES (@BRID, @LD" & _
        "ID, @SSID, @SSUserSelected); SELECT BRID, LDID, SSID, SSUserSelected FROM Leads_" & _
        "SubSources WHERE (BRID = @BRID) AND (LDID = @LDID) AND (SSID = @SSID)"
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDID", System.Data.SqlDbType.BigInt, 8, "LDID"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SSID", System.Data.SqlDbType.Int, 4, "SSID"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SSUserSelected", System.Data.SqlDbType.Bit, 1, "SSUserSelected"))
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT BRID, LDID, SSID, SSUserSelected, SSName FROM VLeads_SubSources WHERE (BRI" & _
        "D = @BRID) AND (LDID = @LDID)"
        Me.SqlSelectCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDID", System.Data.SqlDbType.BigInt, 8, "LDID"))
        '
        'SqlUpdateCommand5
        '
        Me.SqlUpdateCommand5.CommandText = "UPDATE Leads_SubSources SET BRID = @BRID, LDID = @LDID, SSID = @SSID, SSUserSelec" & _
        "ted = @SSUserSelected WHERE (BRID = @Original_BRID) AND (LDID = @Original_LDID) " & _
        "AND (SSID = @Original_SSID) AND (SSUserSelected = @Original_SSUserSelected OR @O" & _
        "riginal_SSUserSelected IS NULL AND SSUserSelected IS NULL); SELECT BRID, LDID, S" & _
        "SID, SSUserSelected FROM Leads_SubSources WHERE (BRID = @BRID) AND (LDID = @LDID" & _
        ") AND (SSID = @SSID)"
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDID", System.Data.SqlDbType.BigInt, 8, "LDID"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SSID", System.Data.SqlDbType.Int, 4, "SSID"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SSUserSelected", System.Data.SqlDbType.Bit, 1, "SSUserSelected"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SSID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SSUserSelected", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SSUserSelected", System.Data.DataRowVersion.Original, Nothing))
        '
        'HelpProvider1
        '
        Me.HelpProvider1.HelpNamespace = "C:\Documents and Settings\djpower\My Documents\My Projects\GTMS\Version1\Help\Hel" & _
        "pProject.chm"
        '
        'daTasks
        '
        Me.daTasks.InsertCommand = Me.SqlCommand1
        Me.daTasks.SelectCommand = Me.SqlCommand2
        Me.daTasks.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Tasks", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("TAName", "TAName"), New System.Data.Common.DataColumnMapping("TAControlsJobDate", "TAControlsJobDate"), New System.Data.Common.DataColumnMapping("TAMenuCaption", "TAMenuCaption"), New System.Data.Common.DataColumnMapping("TAColourR", "TAColourR"), New System.Data.Common.DataColumnMapping("TAColourG", "TAColourG"), New System.Data.Common.DataColumnMapping("TAColourB", "TAColourB"), New System.Data.Common.DataColumnMapping("TAID", "TAID")})})
        '
        'SqlCommand1
        '
        Me.SqlCommand1.CommandText = "INSERT INTO Tasks(TAName, TAControlsJobDate, TAMenuCaption, TAColourR, TAColourG," & _
        " TAColourB, TAID) VALUES (@TAName, @TAControlsJobDate, @TAMenuCaption, @TAColour" & _
        "R, @TAColourG, @TAColourB, @TAID); SELECT TAName, TAControlsJobDate, TAMenuCapti" & _
        "on, TAColourR, TAColourG, TAColourB, TAID FROM Tasks ORDER BY TAID"
        Me.SqlCommand1.Connection = Me.SqlConnection
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAName", System.Data.SqlDbType.VarChar, 50, "TAName"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAControlsJobDate", System.Data.SqlDbType.Bit, 1, "TAControlsJobDate"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAMenuCaption", System.Data.SqlDbType.VarChar, 50, "TAMenuCaption"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourR", System.Data.SqlDbType.SmallInt, 2, "TAColourR"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourG", System.Data.SqlDbType.SmallInt, 2, "TAColourG"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourB", System.Data.SqlDbType.SmallInt, 2, "TAColourB"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAID", System.Data.SqlDbType.Int, 4, "TAID"))
        '
        'SqlCommand2
        '
        Me.SqlCommand2.CommandText = "SELECT TAName, TAControlsJobDate, TAMenuCaption, TAColourR, TAColourG, TAColourB," & _
        " TAID FROM Tasks ORDER BY TAID"
        Me.SqlCommand2.Connection = Me.SqlConnection
        '
        'Storage
        '
        Me.Storage.Appointments.DataSource = Me.dvAppointments
        Me.Storage.Appointments.Mappings.AllDay = "APAllDay"
        Me.Storage.Appointments.Mappings.End = "APEnd"
        Me.Storage.Appointments.Mappings.Label = "APTask"
        Me.Storage.Appointments.Mappings.Location = "APSuburb"
        Me.Storage.Appointments.Mappings.ResourceId = "EXID"
        Me.Storage.Appointments.Mappings.Start = "APBegin"
        Me.Storage.Appointments.Mappings.Status = "APStatus"
        Me.Storage.Appointments.Mappings.Subject = "APDescription"
        Me.Storage.Appointments.Statuses.Add(New DevExpress.XtraScheduler.AppointmentStatus(DevExpress.XtraScheduler.AppointmentStatusType.Free, "Free", "&Free"))
        Me.Storage.Appointments.Statuses.Add(New DevExpress.XtraScheduler.AppointmentStatus(DevExpress.XtraScheduler.AppointmentStatusType.Tentative, System.Drawing.Color.FromArgb(CType(74, Byte), CType(135, Byte), CType(226, Byte)), "Tentative", "&Tentative"))
        Me.Storage.Appointments.Statuses.Add(New DevExpress.XtraScheduler.AppointmentStatus(DevExpress.XtraScheduler.AppointmentStatusType.Busy, System.Drawing.Color.FromArgb(CType(74, Byte), CType(135, Byte), CType(226, Byte)), "Busy", "&Busy"))
        Me.Storage.Appointments.Statuses.Add(New DevExpress.XtraScheduler.AppointmentStatus(DevExpress.XtraScheduler.AppointmentStatusType.OutOfOffice, System.Drawing.Color.FromArgb(CType(217, Byte), CType(83, Byte), CType(83, Byte)), "Off Work", "&Off Work"))
        Me.Storage.Resources.DataSource = Me.dvResources
        Me.Storage.Resources.Mappings.Caption = "EXCalendarName"
        Me.Storage.Resources.Mappings.Id = "EXID"
        '
        'dvResources
        '
        Me.dvResources.RowFilter = "EXAppearInCalendar = 1"
        Me.dvResources.Sort = "EGName, EXName"
        Me.dvResources.Table = Me.DsCalendar.VExpenses
        '
        'daIDNotes
        '
        Me.daIDNotes.DeleteCommand = Me.SqlDeleteCommand6
        Me.daIDNotes.InsertCommand = Me.SqlInsertCommand6
        Me.daIDNotes.SelectCommand = Me.SqlSelectCommand6
        Me.daIDNotes.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "IDNotes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("INID", "INID"), New System.Data.Common.DataColumnMapping("ID", "ID"), New System.Data.Common.DataColumnMapping("INUser", "INUser"), New System.Data.Common.DataColumnMapping("INDate", "INDate"), New System.Data.Common.DataColumnMapping("INNotes", "INNotes")})})
        Me.daIDNotes.UpdateCommand = Me.SqlUpdateCommand6
        '
        'SqlDeleteCommand6
        '
        Me.SqlDeleteCommand6.CommandText = "DELETE FROM IDNotes WHERE (BRID = @Original_BRID) AND (INID = @Original_INID) AND" & _
        " (ID = @Original_ID) AND (INDate = @Original_INDate OR @Original_INDate IS NULL " & _
        "AND INDate IS NULL) AND (INNotes = @Original_INNotes OR @Original_INNotes IS NUL" & _
        "L AND INNotes IS NULL) AND (INUser = @Original_INUser OR @Original_INUser IS NUL" & _
        "L AND INUser IS NULL)"
        Me.SqlDeleteCommand6.Connection = Me.SqlConnection
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INNotes", System.Data.SqlDbType.VarChar, 1000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INUser", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INUser", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand6
        '
        Me.SqlInsertCommand6.CommandText = "INSERT INTO IDNotes(BRID, ID, INUser, INDate, INNotes) VALUES (@BRID, @ID, @INUse" & _
        "r, @INDate, @INNotes); SELECT BRID, INID, ID, INUser, INDate, INNotes FROM IDNot" & _
        "es WHERE (BRID = @BRID) AND (INID = @@IDENTITY)"
        Me.SqlInsertCommand6.Connection = Me.SqlConnection
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INUser", System.Data.SqlDbType.VarChar, 50, "INUser"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INDate", System.Data.SqlDbType.DateTime, 8, "INDate"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INNotes", System.Data.SqlDbType.VarChar, 1000, "INNotes"))
        '
        'SqlSelectCommand6
        '
        Me.SqlSelectCommand6.CommandText = "SELECT BRID, INID, ID, INUser, INDate, INNotes FROM IDNotes WHERE (BRID = @BRID) " & _
        "AND (ID = @ID)"
        Me.SqlSelectCommand6.Connection = Me.SqlConnection
        Me.SqlSelectCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        '
        'SqlUpdateCommand6
        '
        Me.SqlUpdateCommand6.CommandText = "UPDATE IDNotes SET BRID = @BRID, ID = @ID, INUser = @INUser, INDate = @INDate, IN" & _
        "Notes = @INNotes WHERE (BRID = @Original_BRID) AND (INID = @Original_INID) AND (" & _
        "ID = @Original_ID) AND (INDate = @Original_INDate OR @Original_INDate IS NULL AN" & _
        "D INDate IS NULL) AND (INNotes = @Original_INNotes OR @Original_INNotes IS NULL " & _
        "AND INNotes IS NULL) AND (INUser = @Original_INUser OR @Original_INUser IS NULL " & _
        "AND INUser IS NULL); SELECT BRID, INID, ID, INUser, INDate, INNotes FROM IDNotes" & _
        " WHERE (BRID = @BRID) AND (INID = @INID)"
        Me.SqlUpdateCommand6.Connection = Me.SqlConnection
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INUser", System.Data.SqlDbType.VarChar, 50, "INUser"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INDate", System.Data.SqlDbType.DateTime, 8, "INDate"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INNotes", System.Data.SqlDbType.VarChar, 1000, "INNotes"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INNotes", System.Data.SqlDbType.VarChar, 1000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_INUser", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "INUser", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@INID", System.Data.SqlDbType.BigInt, 8, "INID"))
        '
        'frmLead
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(770, 528)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.ListBox)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.SimpleButton1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLead"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Quote Request"
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEXIDRep.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvSalesReps, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDJobDescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkLDQuoteReceived.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dpLDDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMain.ResumeLayout(False)
        CType(Me.pnlScheduling, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlScheduling.ResumeLayout(False)
        CType(Me.dgAppointments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvAppointments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsCalendar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvAppointments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlNotes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlNotes.ResumeLayout(False)
        CType(Me.dgNotes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvNotes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUser, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvExpenses, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtINDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemMemoEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlJobAddress, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlJobAddress.ResumeLayout(False)
        CType(Me.txtLDJobStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgLDJobAddressAsAbove.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDJobStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDJobSuburb.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDJobState.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDJobPostCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlEnquirySources, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlEnquirySources.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLSName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlEnquiryInformation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlEnquiryInformation.ResumeLayout(False)
        CType(Me.txtLDUser.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlClientInformation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlClientInformation.ResumeLayout(False)
        CType(Me.txtLDClientEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientSurname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientFirstName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDReferenceName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientStreetAddress01.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientStreetAddress02.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientSuburb.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientState.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientPostCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientPhoneNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientFaxNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDClientMobileNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Storage, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvResources, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnOK.Enabled = Not HasRole("branch_read_only")
        ListBox.SelectedIndex = 0
    End Sub

    ' This data must be loaded BEFORE the DataRow is loaded (eg lookups etc)
    Private Sub FillPreliminaryData()
        ' --- LEAD SOURCES ---
        daLeadSources.Fill(DsGTMS)
        ' --- SALES REPS ---
        daExpenses.SelectCommand.Parameters("@BRID").Value = BRID
        daExpenses.Fill(DsGTMS)
        daExpenses.Fill(DsCalendar.VExpenses)
        ' --- TASKS ---
        daTasks.Fill(DsCalendar)
    End Sub

    ' This data must be loaded AFTER the DataRow is loaded (eg record related data)
    Private Sub FillData()
        ' --- LEAD SUBSOURCES ---
        daLeadsSubSources.SelectCommand.Parameters("@BRID").Value = BRID
        daLeadsSubSources.SelectCommand.Parameters("@LDID").Value = LDID
        daLeadsSubSources.Fill(DsGTMS)
        ' --- ID NOTES ---
        daIDNotes.SelectCommand.Parameters("@BRID").Value = BRID
        daIDNotes.SelectCommand.Parameters("@ID").Value = DataRow("ID")
        daIDNotes.Fill(DsGTMS)
        ' --- APPOINTMENTS ---
        daAppointments.SelectCommand.Parameters("@BRID").Value = BRID
        daAppointments.Fill(DsCalendar.VAppointments)
        dvAppointments.RowFilter = "(BRID = " & BRID & ") AND (APType = 'LD') AND (APTypeID = " & LDID & ")"
        ' --- TASKS ---
        daTasks.Fill(DsCalendar)
        ' --- Calendar ---
        SetUpCalendar()
    End Sub

    Private Sub EnableDisable()
        On Error Resume Next
        lblJobStreetAddress.Enabled = Not rgLDJobAddressAsAbove.EditValue
        lblJobSuburb.Enabled = Not rgLDJobAddressAsAbove.EditValue
        lblJobState.Enabled = Not rgLDJobAddressAsAbove.EditValue
        lblJobPostCode.Enabled = Not rgLDJobAddressAsAbove.EditValue
        txtLDJobStreetAddress01.Enabled = Not rgLDJobAddressAsAbove.EditValue
        txtLDJobStreetAddress02.Enabled = Not rgLDJobAddressAsAbove.EditValue
        txtLDJobSuburb.Enabled = Not rgLDJobAddressAsAbove.EditValue
        txtLDJobState.Enabled = Not rgLDJobAddressAsAbove.EditValue
        txtLDJobPostCode.Enabled = Not rgLDJobAddressAsAbove.EditValue
        '  tcScheduling.Enabled = rgLDHasAppointments.EditValue
    End Sub

    Private Const DEFAULT_FILTER As String = "EXAppearInCalendar = 1"
    Private Const DEFAULT_SORT As String = "EGName, EXName"
    Private Sub SetUpCalendar()
        Storage.Appointments.Labels.Clear()
        For Each task As DataRow In DsCalendar.Tasks.Rows
            Storage.Appointments.Labels.Add(System.Drawing.Color.FromArgb(task("TAColourR"), task("TAColourG"), task("TAColourB")), _
                task("TAName"), task("TAMenuCaption"))
        Next
        ' --- SET UP FILTER ---
        dvResources.RowFilter = "(" & DEFAULT_FILTER & ") AND (EGType = 'RC')"
        dvResources.Sort = DEFAULT_SORT
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click

        ' EndEdit() to end editing the dataset record so that we can update
        DataRow.EndEdit()

        SqlDataAdapter.Update(DsGTMS)
        daLeadsSubSources.Update(DsGTMS)
        daIDNotes.Update(DsGTMS)
        WriteMRU(txtLDJobDescription, UserAppDataPath)

        ReleaseAppointmentLocks()
        DataAccess.spExecLockRequest("sp_ReleaseLeadIDLock", BRID, LDID, Transaction)
        Transaction.Commit()
        SqlConnection.Close()

        OK = True
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private HasChanges As Boolean = False
    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            DataRow.EndEdit()
            If HasChanges Or DsGTMS.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If response = MsgBoxResult.Yes Then
                ReleaseAppointmentLocks()
                DataAccess.spExecLockRequest("sp_ReleaseLeadIDLock", BRID, LDID, Transaction)
                Transaction.Rollback()
                SqlConnection.Close()
            Else
                e.Cancel = True
            End If
        End If
    End Sub

    Private HelpTopic As String = Nothing
    Private Sub ListBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox.SelectedIndexChanged
        Dim pnl As DevExpress.XtraEditors.GroupControl

        For Each pnl In pnlMain.Controls
            pnl.Enabled = False
        Next
        pnl = Nothing
        Select Case ListBox.SelectedIndex
            Case 0
                pnl = Me.pnlClientInformation
                HelpTopic = "ClientInformation"
            Case 1
                pnl = pnlJobAddress
                HelpTopic = "JobAddress"
            Case 2
                pnl = Me.pnlEnquiryInformation
                HelpTopic = "EnquiryInformation"
            Case 3
                pnl = Me.pnlEnquirySources
                HelpTopic = "EnquirySources"
            Case 4
                pnl = pnlScheduling
                HelpTopic = "Scheduling"

                DataRow.EndEdit()
                SqlDataAdapter.Update(DsGTMS)
                daAppointments.Fill(DsCalendar.VAppointments)
                If Not txtEXIDRep.EditValue Is DBNull.Value Then
                    'Calendar.ShowResource(txtEXIDRep.EditValue)
                    'Calendar.RefreshCalendar()
                Else
                    'Calendar.RefreshCalendar()
                End If
            Case 5
                pnl = Me.pnlNotes
                HelpTopic = "Notes"
        End Select
        If Not pnl Is Nothing Then
            pnl.Enabled = True
            pnl.Dock = DockStyle.Fill
            Power.Library.Library.CenterControls(pnl)
            pnl.BringToFront()
        End If
    End Sub

#Region " Appointments "

    Public ReadOnly Property SelectedAppointment() As DataRow
        Get
            If Not gvAppointments.GetSelectedRows Is Nothing Then
                Return gvAppointments.GetDataRow(gvAppointments.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub Calendar_NewAllDayEvent(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim c As Cursor = Me.Cursor
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim gui As frmAppointment = frmAppointment.Add(Storage, BRID, "LD", LDID, Transaction, 1)
        AddAppointmentLock(gui.APID)
        gui.APBegin = Today.Date
        gui.APEnd = DateAdd(DateInterval.Day, 1, Today.Date)
        gui.APAllDay = True
        gui.EXID = IsNull(DataRow("EXIDRep"), Nothing)
        gui.ShowDialog(Me)
        HasChanges = True
        daAppointments.Fill(DsCalendar.VAppointments)
        Me.Cursor = c
    End Sub

    Private Sub Calendar_NewAppointment(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim c As Cursor = Me.Cursor
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim gui As frmAppointment = frmAppointment.Add(Storage, BRID, "LD", LDID, Transaction, 1)
        AddAppointmentLock(gui.APID)
        gui.APBegin = Today.Date
        gui.APEnd = DateAdd(DateInterval.Hour, 1, Today.Date)
        gui.EXID = IsNull(DataRow("EXIDRep"), Nothing)
        gui.ShowDialog(Me)
        HasChanges = True
        daAppointments.Fill(DsCalendar.VAppointments)
        Me.Cursor = c
    End Sub

    Private Sub btnAddAppointment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAppointment.Click
        Calendar_NewAppointment(sender, e)
    End Sub

    Private Sub Calendar_EditAppointment(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not SelectedAppointment Is Nothing Then
            If Not (SelectedAppointment.Item("BRID") = Me.BRID And SelectedAppointment.Item("APType") = "LD" And SelectedAppointment.Item("APTypeID") = LDID) Then
                If Message.AppointmentNotCurrentObject("quote request", Message.ObjectAction.Edit) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If
            Dim c As Cursor = Me.Cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If AddAppointmentLock(SelectedAppointment.Item("APID")) Then
                Dim gui As frmAppointment = frmAppointment.Edit(Storage, SelectedAppointment.Item("BRID"), SelectedAppointment.Item("APID"), Transaction, 1)
                gui.ShowDialog(Me)
                HasChanges = True
                daAppointments.Fill(DsCalendar.VAppointments)
            Else
                Message.CurrentlyAccessed("appointment")
            End If
            Me.Cursor = c
        End If
    End Sub

    Private Sub dgAppointments_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgAppointments.DoubleClick
        Calendar_EditAppointment(sender, e)
    End Sub

    Private Sub btnEditAppointment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditAppointment.Click
        Calendar_EditAppointment(sender, e)
    End Sub

    Private Sub btnRemoveAppointment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveAppointment.Click
        If Not SelectedAppointment Is Nothing Then
            If Not (SelectedAppointment.Item("BRID") = Me.BRID And SelectedAppointment.Item("APType") = "LD" And SelectedAppointment.Item("APTypeID") = LDID) Then
                If Message.AppointmentNotCurrentObject("quote request", Message.ObjectAction.Delete) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If
            If AddAppointmentLock(SelectedAppointment.Item("APID")) Then
                SelectedAppointment.Delete()
                HasChanges = True
                daAppointments.Update(DsCalendar.VAppointments)
            Else
                Message.CurrentlyAccessed("appointment")
            End If
        End If
    End Sub

    Private Sub btnViewCalendar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewCalendar.Click
        Dim gui As New frmCalendar(BRID, "LD", LDID, Transaction)
        gui.EditedAppointmentList = EditedAppointmentList
        gui.Calendar.Calendar.SelectedInterval.Start = Today.Date
        gui.Calendar.Calendar.SelectedInterval.End = Today.Date
        If Not txtEXIDRep.EditValue Is DBNull.Value Then
            gui.Calendar.SelectResource(txtEXIDRep.EditValue)
        End If
        gui.ShowDialog(Me)
        HasChanges = True
        EditedAppointmentList = gui.EditedAppointmentList
        DsCalendar.VAppointments.Clear()
        daAppointments.Fill(DsCalendar.VAppointments)
    End Sub

    Private EditedAppointmentList As New ArrayList

    Private Function AddAppointmentLock(ByVal APID As Long) As Boolean
        If Not IsInEditList(APID) Then
            If DataAccess.spExecLockRequest("sp_GetAppointmentLock", BRID, APID, Transaction) Then
                EditedAppointmentList.Add(APID)
                Return True
            Else
                Return False
            End If
        Else
            Return True
        End If
    End Function

    Private Function IsInEditList(ByVal APID As Long) As Boolean
        For Each lng As Long In EditedAppointmentList
            If lng = APID Then
                Return True
            End If
        Next
        Return False
    End Function

    Private Sub ReleaseAppointmentLocks()
        For Each lng As Long In EditedAppointmentList
            DataAccess.spExecLockRequest("sp_ReleaseAppointmentLock", BRID, lng, Transaction)
        Next
    End Sub

#End Region

#Region " Notes "

    Private Sub gvNotes_InitNewRow(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gvNotes.InitNewRow
        gvNotes.GetDataRow(e.RowHandle)("BRID") = BRID
        gvNotes.GetDataRow(e.RowHandle)("ID") = DataRow("ID")
    End Sub

    Private Sub gvNotes_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gvNotes.KeyDown
        If e.KeyCode = Keys.Delete Then
            If gvNotes.SelectedRowsCount > 0 Then
                gvNotes.GetDataRow(gvNotes.GetSelectedRows(0)).Delete()
            End If
        End If
    End Sub

    Private Sub gvNotes_InvalidRowException(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gvNotes.InvalidRowException
        GridView_InvalidRowException(sender, e)
    End Sub

#End Region

    Private Sub Date_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) _
    Handles txtINDate.Validating, dpLDDate.Validating
        Library.DateEdit_Validating(sender, e)
    End Sub

    Private Sub rgLDJobAddressAsAbove_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgLDJobAddressAsAbove.SelectedIndexChanged
        EnableDisable()
    End Sub
    Private Sub RdLDHasAppointments_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        EnableDisable()
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        If HelpTopic Is Nothing Then
            ShowHelpTopic(Me, "QuoteRequestsTerms.html")
        Else
            ShowHelpTopic(Me, "QuoteRequestsTerms.html#" & HelpTopic)
        End If
    End Sub

    Private Sub txtLDClientFirstName_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLDClientFirstName.EditValueChanged, txtLDClientSurname.EditValueChanged, txtLDClientFirstName.Validated, txtLDClientSurname.Validated
        If Not DataRow Is Nothing Then
            Me.Text = "Quote Request " & DataRow("ID") & " - " & FormatName(DataRow("LDClientFirstName"), DataRow("LDClientSurname"))
        Else
            Me.Text = "Quote Request"
        End If
    End Sub

    Private Sub miClearHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miClearHistory.Click
        ClearHistory(sender, e)
    End Sub

End Class
