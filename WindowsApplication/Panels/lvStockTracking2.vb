Imports Power.Forms

Public Class lvStockTracking2
    Inherits System.Windows.Forms.UserControl
    Implements IListControl, IPrintableControl

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Connection As SqlClient.SqlConnection, ByVal BRID As Integer, ByVal ExplorerForm As frmExplorer)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.Connection = Connection
        Me.BRID = BRID
        hExplorerForm = ExplorerForm
        SetUpFilters()
        DateFilterIsListening = True
        FillDataSet()
        EnableDisable()
    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents hSqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents ImageListToolBar As System.Windows.Forms.ImageList
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents bStandard As DevExpress.XtraBars.Bar
    Friend WithEvents subReportsOnSelectedItem As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnQuickPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPrintPreview As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnHelp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarAndDockingController1 As DevExpress.XtraBars.BarAndDockingController
    Friend WithEvents btnPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subReportsOnSelectedItem2 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents PopupMenu1 As DevExpress.XtraBars.PopupMenu
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents bAdvancedFilter As DevExpress.XtraBars.Bar
    Friend WithEvents beiFromDate As DevExpress.XtraBars.BarEditItem
    Friend WithEvents beiToDate As DevExpress.XtraBars.BarEditItem
    Friend WithEvents btnResetFilters As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents dataSet As WindowsApplication.dsStockTracking
    Friend WithEvents colSIID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTInventoryAmount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTInventoryBefore As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTInventoryAfter As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents PersistentRepository1 As DevExpress.XtraEditors.Repository.PersistentRepository
    Friend WithEvents txtSIID As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents txtSTType As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents colSTStockIncoming As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTStockOutgoing As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTInventoryAdjusted As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTUsedInJobs As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daSTTypes As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daStockedItems As System.Data.SqlClient.SqlDataAdapter
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(lvStockTracking2))
        Me.hSqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.dataSet = New WindowsApplication.dsStockTracking
        Me.PersistentRepository1 = New DevExpress.XtraEditors.Repository.PersistentRepository(Me.components)
        Me.txtSIID = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.txtSTType = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colSIID = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSTDate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSTType = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSTInventoryAmount = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSTInventoryBefore = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSTInventoryAfter = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSTStockIncoming = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSTStockOutgoing = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSTInventoryAdjusted = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSTUsedInJobs = New DevExpress.XtraGrid.Columns.GridColumn
        Me.ImageListToolBar = New System.Windows.Forms.ImageList(Me.components)
        Me.BarManager1 = New DevExpress.XtraBars.BarManager
        Me.bStandard = New DevExpress.XtraBars.Bar
        Me.btnQuickPrint = New DevExpress.XtraBars.BarButtonItem
        Me.btnPrintPreview = New DevExpress.XtraBars.BarButtonItem
        Me.btnHelp = New DevExpress.XtraBars.BarButtonItem
        Me.bAdvancedFilter = New DevExpress.XtraBars.Bar
        Me.beiFromDate = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.beiToDate = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.btnResetFilters = New DevExpress.XtraBars.BarButtonItem
        Me.BarAndDockingController1 = New DevExpress.XtraBars.BarAndDockingController(Me.components)
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
        Me.btnPrint = New DevExpress.XtraBars.BarButtonItem
        Me.subReportsOnSelectedItem = New DevExpress.XtraBars.BarSubItem
        Me.subReportsOnSelectedItem2 = New DevExpress.XtraBars.BarSubItem
        Me.PopupMenu1 = New DevExpress.XtraBars.PopupMenu
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.daSTTypes = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.daStockedItems = New System.Data.SqlClient.SqlDataAdapter
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSIID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSTType, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'hSqlConnection
        '
        Me.hSqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;user id=sa;integrated security=SSPI;data sou" & _
        "rce=""SERVER\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "StockTracking", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("STID", "STID"), New System.Data.Common.DataColumnMapping("SIID", "SIID"), New System.Data.Common.DataColumnMapping("STDate", "STDate"), New System.Data.Common.DataColumnMapping("STDateSeq", "STDateSeq"), New System.Data.Common.DataColumnMapping("STType", "STType"), New System.Data.Common.DataColumnMapping("SAID", "SAID"), New System.Data.Common.DataColumnMapping("OSID", "OSID"), New System.Data.Common.DataColumnMapping("STInventoryAmount", "STInventoryAmount"), New System.Data.Common.DataColumnMapping("STInventoryBefore", "STInventoryBefore"), New System.Data.Common.DataColumnMapping("STInventoryAfter", "STInventoryAfter"), New System.Data.Common.DataColumnMapping("STStockIncoming", "STStockIncoming"), New System.Data.Common.DataColumnMapping("STStockOutgoing", "STStockOutgoing"), New System.Data.Common.DataColumnMapping("STInventoryAdjusted", "STInventoryAdjusted"), New System.Data.Common.DataColumnMapping("STUsedInJobs", "STUsedInJobs")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM StockTracking_values WHERE (BRID = @Original_BRID) AND (STID = @Origi" & _
        "nal_STID) AND (OSID = @Original_OSID OR @Original_OSID IS NULL AND OSID IS NULL)" & _
        " AND (SAID = @Original_SAID OR @Original_SAID IS NULL AND SAID IS NULL) AND (SII" & _
        "D = @Original_SIID) AND (STDate = @Original_STDate) AND (STDateSeq = @Original_S" & _
        "TDateSeq OR @Original_STDateSeq IS NULL AND STDateSeq IS NULL) AND (STInventoryA" & _
        "djusted = @Original_STInventoryAdjusted OR @Original_STInventoryAdjusted IS NULL" & _
        " AND STInventoryAdjusted IS NULL) AND (STInventoryAfter = @Original_STInventoryA" & _
        "fter OR @Original_STInventoryAfter IS NULL AND STInventoryAfter IS NULL) AND (ST" & _
        "InventoryAmount = @Original_STInventoryAmount) AND (STInventoryBefore = @Origina" & _
        "l_STInventoryBefore OR @Original_STInventoryBefore IS NULL AND STInventoryBefore" & _
        " IS NULL) AND (STStockIncoming = @Original_STStockIncoming OR @Original_STStockI" & _
        "ncoming IS NULL AND STStockIncoming IS NULL) AND (STStockOutgoing = @Original_ST" & _
        "StockOutgoing OR @Original_STStockOutgoing IS NULL AND STStockOutgoing IS NULL) " & _
        "AND (STType = @Original_STType) AND (STUsedInJobs = @Original_STUsedInJobs OR @O" & _
        "riginal_STUsedInJobs IS NULL AND STUsedInJobs IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OSID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SAID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SAID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STDateSeq", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STDateSeq", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STInventoryAdjusted", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STInventoryAdjusted", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STInventoryAfter", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STInventoryAfter", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STInventoryAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STInventoryBefore", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STInventoryBefore", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STStockIncoming", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STStockIncoming", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STStockOutgoing", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STStockOutgoing", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STUsedInJobs", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STUsedInJobs", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=DEV1;packet size=4096;integrated security=SSPI;data source=""SERVER" & _
        "\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO StockTracking_values(BRID, SIID, STDate, STDateSeq, STType, SAID, OSI" & _
        "D, STInventoryAmount, STInventoryBefore, STInventoryAfter, STStockIncoming, STSt" & _
        "ockOutgoing, STInventoryAdjusted, STUsedInJobs) VALUES (@BRID, @SIID, @STDate, @" & _
        "STDateSeq, @STType, @SAID, @OSID, @STInventoryAmount, @STInventoryBefore, @STInv" & _
        "entoryAfter, @STStockIncoming, @STStockOutgoing, @STInventoryAdjusted, @STUsedIn" & _
        "Jobs); SELECT BRID, STID, SIID, STDate, STDateSeq, STType, SAID, OSID, STInvento" & _
        "ryAmount, STInventoryBefore, STInventoryAfter, STStockIncoming, STStockOutgoing," & _
        " STInventoryAdjusted, STUsedInJobs FROM StockTracking_values WHERE (BRID = @BRID" & _
        ") AND (STID = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIID", System.Data.SqlDbType.Int, 4, "SIID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STDate", System.Data.SqlDbType.DateTime, 8, "STDate"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STDateSeq", System.Data.SqlDbType.Int, 4, "STDateSeq"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STType", System.Data.SqlDbType.VarChar, 2, "STType"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SAID", System.Data.SqlDbType.BigInt, 8, "SAID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OSID", System.Data.SqlDbType.BigInt, 8, "OSID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STInventoryAmount", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STInventoryBefore", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STInventoryBefore", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STInventoryAfter", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STInventoryAfter", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STStockIncoming", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STStockIncoming", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STStockOutgoing", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STStockOutgoing", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STInventoryAdjusted", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STInventoryAdjusted", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STUsedInJobs", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STUsedInJobs", System.Data.DataRowVersion.Current, Nothing))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, STID, SIID, STDate, STDateSeq, STType, SAID, OSID, STInventoryAmount" & _
        ", STInventoryBefore, STInventoryAfter, STStockIncoming, STStockOutgoing, STInven" & _
        "toryAdjusted, STUsedInJobs FROM StockTracking WHERE (STDate <= @TO_DATE) AND (BR" & _
        "ID = @BRID) AND (STDate >= @FROM_DATE) OR (BRID = @BRID) AND (STDate <= @TO_DATE" & _
        ") AND (@FROM_DATE IS NULL) OR (BRID = @BRID) AND (STDate >= @FROM_DATE) AND (@TO" & _
        "_DATE IS NULL) OR (BRID = @BRID) AND (@FROM_DATE IS NULL) AND (@TO_DATE IS NULL)" & _
        ""
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TO_DATE", System.Data.SqlDbType.DateTime, 8, "STDate"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FROM_DATE", System.Data.SqlDbType.DateTime, 8, "STDate"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE StockTracking_values SET BRID = @BRID, SIID = @SIID, STDate = @STDate, STD" & _
        "ateSeq = @STDateSeq, STType = @STType, SAID = @SAID, OSID = @OSID, STInventoryAm" & _
        "ount = @STInventoryAmount, STInventoryBefore = @STInventoryBefore, STInventoryAf" & _
        "ter = @STInventoryAfter, STStockIncoming = @STStockIncoming, STStockOutgoing = @" & _
        "STStockOutgoing, STInventoryAdjusted = @STInventoryAdjusted, STUsedInJobs = @STU" & _
        "sedInJobs WHERE (BRID = @Original_BRID) AND (STID = @Original_STID) AND (OSID = " & _
        "@Original_OSID OR @Original_OSID IS NULL AND OSID IS NULL) AND (SAID = @Original" & _
        "_SAID OR @Original_SAID IS NULL AND SAID IS NULL) AND (SIID = @Original_SIID) AN" & _
        "D (STDate = @Original_STDate) AND (STDateSeq = @Original_STDateSeq OR @Original_" & _
        "STDateSeq IS NULL AND STDateSeq IS NULL) AND (STInventoryAdjusted = @Original_ST" & _
        "InventoryAdjusted OR @Original_STInventoryAdjusted IS NULL AND STInventoryAdjust" & _
        "ed IS NULL) AND (STInventoryAfter = @Original_STInventoryAfter OR @Original_STIn" & _
        "ventoryAfter IS NULL AND STInventoryAfter IS NULL) AND (STInventoryAmount = @Ori" & _
        "ginal_STInventoryAmount) AND (STInventoryBefore = @Original_STInventoryBefore OR" & _
        " @Original_STInventoryBefore IS NULL AND STInventoryBefore IS NULL) AND (STStock" & _
        "Incoming = @Original_STStockIncoming OR @Original_STStockIncoming IS NULL AND ST" & _
        "StockIncoming IS NULL) AND (STStockOutgoing = @Original_STStockOutgoing OR @Orig" & _
        "inal_STStockOutgoing IS NULL AND STStockOutgoing IS NULL) AND (STType = @Origina" & _
        "l_STType) AND (STUsedInJobs = @Original_STUsedInJobs OR @Original_STUsedInJobs I" & _
        "S NULL AND STUsedInJobs IS NULL); SELECT BRID, STID, SIID, STDate, STDateSeq, ST" & _
        "Type, SAID, OSID, STInventoryAmount, STInventoryBefore, STInventoryAfter, STStoc" & _
        "kIncoming, STStockOutgoing, STInventoryAdjusted, STUsedInJobs FROM StockTracking" & _
        "_values WHERE (BRID = @BRID) AND (STID = @STID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIID", System.Data.SqlDbType.Int, 4, "SIID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STDate", System.Data.SqlDbType.DateTime, 8, "STDate"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STDateSeq", System.Data.SqlDbType.Int, 4, "STDateSeq"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STType", System.Data.SqlDbType.VarChar, 2, "STType"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SAID", System.Data.SqlDbType.BigInt, 8, "SAID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OSID", System.Data.SqlDbType.BigInt, 8, "OSID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STInventoryAmount", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STInventoryBefore", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STInventoryBefore", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STInventoryAfter", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STInventoryAfter", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STStockIncoming", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STStockIncoming", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STStockOutgoing", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STStockOutgoing", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STInventoryAdjusted", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STInventoryAdjusted", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STUsedInJobs", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STUsedInJobs", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_OSID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "OSID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SAID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SAID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STDateSeq", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STDateSeq", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STInventoryAdjusted", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STInventoryAdjusted", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STInventoryAfter", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STInventoryAfter", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STInventoryAmount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STInventoryBefore", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STInventoryBefore", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STStockIncoming", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STStockIncoming", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STStockOutgoing", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STStockOutgoing", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STUsedInJobs", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "STUsedInJobs", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STID", System.Data.SqlDbType.BigInt, 8, "STID"))
        '
        'GridControl1
        '
        Me.GridControl1.DataMember = "StockTracking"
        Me.GridControl1.DataSource = Me.dataSet
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'GridControl1.EmbeddedNavigator
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.ExternalRepository = Me.PersistentRepository1
        Me.GridControl1.Location = New System.Drawing.Point(0, 50)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(632, 342)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'dataSet
        '
        Me.dataSet.DataSetName = "dsStockTracking"
        Me.dataSet.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'PersistentRepository1
        '
        Me.PersistentRepository1.Items.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtSIID, Me.txtSTType})
        '
        'txtSIID
        '
        Me.txtSIID.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtSIID.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("SIName", "", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.txtSIID.DataSource = Me.dataSet.VStockedItems
        Me.txtSIID.DisplayMember = "SIName"
        Me.txtSIID.Name = "txtSIID"
        Me.txtSIID.NullText = ""
        Me.txtSIID.ShowFooter = False
        Me.txtSIID.ShowHeader = False
        Me.txtSIID.ValueMember = "SIID"
        '
        'txtSTType
        '
        Me.txtSTType.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtSTType.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("STTypeDisplay", "", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.txtSTType.DataSource = Me.dataSet.STTypes
        Me.txtSTType.DisplayMember = "STTypeDisplay"
        Me.txtSTType.Name = "txtSTType"
        Me.txtSTType.NullText = ""
        Me.txtSTType.ShowFooter = False
        Me.txtSTType.ShowHeader = False
        Me.txtSTType.ValueMember = "STType"
        '
        'GridView1
        '
        Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
        Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
        Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSIID, Me.colSTDate, Me.colSTType, Me.colSTInventoryAmount, Me.colSTInventoryBefore, Me.colSTInventoryAfter, Me.colSTStockIncoming, Me.colSTStockOutgoing, Me.colSTInventoryAdjusted, Me.colSTUsedInJobs})
        Me.GridView1.CustomizationFormBounds = New System.Drawing.Rectangle(1062, 729, 208, 170)
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsNavigation.AutoFocusNewRow = True
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.OptionsView.ShowHorzLines = False
        Me.GridView1.OptionsView.ShowIndicator = False
        Me.GridView1.OptionsView.ShowVertLines = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colSTDate, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colSIID
        '
        Me.colSIID.Caption = "Stocked Item"
        Me.colSIID.ColumnEdit = Me.txtSIID
        Me.colSIID.FieldName = "SIID"
        Me.colSIID.Name = "colSIID"
        Me.colSIID.Visible = True
        Me.colSIID.VisibleIndex = 0
        '
        'colSTDate
        '
        Me.colSTDate.Caption = "Date"
        Me.colSTDate.FieldName = "STDate"
        Me.colSTDate.Name = "colSTDate"
        Me.colSTDate.Visible = True
        Me.colSTDate.VisibleIndex = 1
        '
        'colSTType
        '
        Me.colSTType.Caption = "Adjustment Type"
        Me.colSTType.ColumnEdit = Me.txtSTType
        Me.colSTType.FieldName = "STType"
        Me.colSTType.Name = "colSTType"
        Me.colSTType.Visible = True
        Me.colSTType.VisibleIndex = 3
        '
        'colSTInventoryAmount
        '
        Me.colSTInventoryAmount.Caption = "Amount"
        Me.colSTInventoryAmount.FieldName = "STInventoryAmount"
        Me.colSTInventoryAmount.Name = "colSTInventoryAmount"
        Me.colSTInventoryAmount.Visible = True
        Me.colSTInventoryAmount.VisibleIndex = 2
        '
        'colSTInventoryBefore
        '
        Me.colSTInventoryBefore.Caption = "Inventory Before"
        Me.colSTInventoryBefore.FieldName = "STInventoryBefore"
        Me.colSTInventoryBefore.Name = "colSTInventoryBefore"
        Me.colSTInventoryBefore.Visible = True
        Me.colSTInventoryBefore.VisibleIndex = 4
        '
        'colSTInventoryAfter
        '
        Me.colSTInventoryAfter.Caption = "Inventory After"
        Me.colSTInventoryAfter.FieldName = "STInventoryAfter"
        Me.colSTInventoryAfter.Name = "colSTInventoryAfter"
        Me.colSTInventoryAfter.Visible = True
        Me.colSTInventoryAfter.VisibleIndex = 5
        '
        'colSTStockIncoming
        '
        Me.colSTStockIncoming.Caption = "Inventory Received"
        Me.colSTStockIncoming.FieldName = "STStockIncoming"
        Me.colSTStockIncoming.Name = "colSTStockIncoming"
        '
        'colSTStockOutgoing
        '
        Me.colSTStockOutgoing.Caption = "Inventory Outgoing"
        Me.colSTStockOutgoing.FieldName = "STStockOutgoing"
        Me.colSTStockOutgoing.Name = "colSTStockOutgoing"
        '
        'colSTInventoryAdjusted
        '
        Me.colSTInventoryAdjusted.Caption = "Adjusted (Physical Count)"
        Me.colSTInventoryAdjusted.FieldName = "STInventoryAdjusted"
        Me.colSTInventoryAdjusted.Name = "colSTInventoryAdjusted"
        '
        'colSTUsedInJobs
        '
        Me.colSTUsedInJobs.Caption = "Used In Jobs"
        Me.colSTUsedInJobs.FieldName = "STUsedInJobs"
        Me.colSTUsedInJobs.Name = "colSTUsedInJobs"
        '
        'ImageListToolBar
        '
        Me.ImageListToolBar.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImageListToolBar.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageListToolBar.ImageStream = CType(resources.GetObject("ImageListToolBar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListToolBar.TransparentColor = System.Drawing.Color.Transparent
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.bStandard, Me.bAdvancedFilter})
        Me.BarManager1.Categories.AddRange(New DevExpress.XtraBars.BarManagerCategory() {New DevExpress.XtraBars.BarManagerCategory("Built-in Menus", New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd"), False), New DevExpress.XtraBars.BarManagerCategory("File", New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")), New DevExpress.XtraBars.BarManagerCategory("Reports", New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")), New DevExpress.XtraBars.BarManagerCategory("Forms", New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")), New DevExpress.XtraBars.BarManagerCategory("Help", New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")), New DevExpress.XtraBars.BarManagerCategory("Search", New System.Guid("d27cdec9-bd13-47ce-a40f-a58b83ae2195"))})
        Me.BarManager1.Controller = Me.BarAndDockingController1
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Images = Me.ImageListToolBar
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnHelp, Me.btnPrintPreview, Me.btnQuickPrint, Me.btnPrint, Me.subReportsOnSelectedItem, Me.subReportsOnSelectedItem2, Me.beiFromDate, Me.beiToDate, Me.btnResetFilters})
        Me.BarManager1.MainMenu = Me.bStandard
        Me.BarManager1.MaxItemId = 97
        Me.BarManager1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1, Me.RepositoryItemDateEdit2})
        '
        'bStandard
        '
        Me.bStandard.BarName = "Standard Toolbar"
        Me.bStandard.DockCol = 0
        Me.bStandard.DockRow = 0
        Me.bStandard.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bStandard.FloatLocation = New System.Drawing.Point(44, 188)
        Me.bStandard.FloatSize = New System.Drawing.Size(659, 24)
        Me.bStandard.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnQuickPrint, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnPrintPreview, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnHelp, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.bStandard.OptionsBar.AllowQuickCustomization = False
        Me.bStandard.OptionsBar.DisableClose = True
        Me.bStandard.OptionsBar.DrawDragBorder = False
        Me.bStandard.OptionsBar.MultiLine = True
        Me.bStandard.OptionsBar.UseWholeRow = True
        Me.bStandard.Text = "Standard Toolbar"
        '
        'btnQuickPrint
        '
        Me.btnQuickPrint.Caption = "Print"
        Me.btnQuickPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnQuickPrint.Id = 46
        Me.btnQuickPrint.ImageIndex = 11
        Me.btnQuickPrint.ImageIndexDisabled = 11
        Me.btnQuickPrint.Name = "btnQuickPrint"
        '
        'btnPrintPreview
        '
        Me.btnPrintPreview.Caption = "Print Pre&view"
        Me.btnPrintPreview.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrintPreview.Id = 44
        Me.btnPrintPreview.ImageIndex = 10
        Me.btnPrintPreview.ImageIndexDisabled = 10
        Me.btnPrintPreview.Name = "btnPrintPreview"
        '
        'btnHelp
        '
        Me.btnHelp.Caption = "&Help"
        Me.btnHelp.CategoryGuid = New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")
        Me.btnHelp.Id = 68
        Me.btnHelp.ImageIndex = 4
        Me.btnHelp.ImageIndexDisabled = 4
        Me.btnHelp.Name = "btnHelp"
        '
        'bAdvancedFilter
        '
        Me.bAdvancedFilter.BarName = "Search Bar"
        Me.bAdvancedFilter.DockCol = 0
        Me.bAdvancedFilter.DockRow = 1
        Me.bAdvancedFilter.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bAdvancedFilter.FloatLocation = New System.Drawing.Point(40, 189)
        Me.bAdvancedFilter.FloatSize = New System.Drawing.Size(570, 24)
        Me.bAdvancedFilter.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.beiFromDate, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.beiToDate, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnResetFilters, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.bAdvancedFilter.OptionsBar.AllowQuickCustomization = False
        Me.bAdvancedFilter.OptionsBar.DisableClose = True
        Me.bAdvancedFilter.OptionsBar.MultiLine = True
        Me.bAdvancedFilter.Text = "Search Bar"
        '
        'beiFromDate
        '
        Me.beiFromDate.Caption = "Search for Dates from:"
        Me.beiFromDate.CategoryGuid = New System.Guid("d27cdec9-bd13-47ce-a40f-a58b83ae2195")
        Me.beiFromDate.Edit = Me.RepositoryItemDateEdit1
        Me.beiFromDate.Id = 93
        Me.beiFromDate.Name = "beiFromDate"
        Me.beiFromDate.Width = 150
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'beiToDate
        '
        Me.beiToDate.Caption = "to:"
        Me.beiToDate.CategoryGuid = New System.Guid("d27cdec9-bd13-47ce-a40f-a58b83ae2195")
        Me.beiToDate.Edit = Me.RepositoryItemDateEdit2
        Me.beiToDate.Id = 94
        Me.beiToDate.Name = "beiToDate"
        Me.beiToDate.Width = 150
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'btnResetFilters
        '
        Me.btnResetFilters.Caption = "&Reset Filters"
        Me.btnResetFilters.CategoryGuid = New System.Guid("d27cdec9-bd13-47ce-a40f-a58b83ae2195")
        Me.btnResetFilters.Id = 95
        Me.btnResetFilters.ImageIndex = 14
        Me.btnResetFilters.ImageIndexDisabled = 14
        Me.btnResetFilters.Name = "btnResetFilters"
        '
        'BarAndDockingController1
        '
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseForeColor = True
        Me.BarAndDockingController1.PaintStyleName = "OfficeXP"
        '
        'btnPrint
        '
        Me.btnPrint.Caption = "&Print..."
        Me.btnPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrint.Id = 43
        Me.btnPrint.ImageIndex = 11
        Me.btnPrint.ImageIndexDisabled = 11
        Me.btnPrint.Name = "btnPrint"
        '
        'subReportsOnSelectedItem
        '
        Me.subReportsOnSelectedItem.Caption = "Reports on Selected Item"
        Me.subReportsOnSelectedItem.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.subReportsOnSelectedItem.Id = 78
        Me.subReportsOnSelectedItem.Name = "subReportsOnSelectedItem"
        Me.subReportsOnSelectedItem.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInCustomizing
        '
        'subReportsOnSelectedItem2
        '
        Me.subReportsOnSelectedItem2.Caption = "&Reports on Selected Item"
        Me.subReportsOnSelectedItem2.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subReportsOnSelectedItem2.Id = 87
        Me.subReportsOnSelectedItem2.Name = "subReportsOnSelectedItem2"
        '
        'PopupMenu1
        '
        Me.PopupMenu1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.subReportsOnSelectedItem, True)})
        Me.PopupMenu1.Manager = Me.BarManager1
        Me.PopupMenu1.Name = "PopupMenu1"
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT STType, STTypeSequence, STTypeDisplay FROM STTypes"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO STTypes(STType, STTypeSequence, STTypeDisplay) VALUES (@STType, @STTy" & _
        "peSequence, @STTypeDisplay); SELECT STType, STTypeSequence, STTypeDisplay FROM S" & _
        "TTypes WHERE (STType = @STType)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection1
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STType", System.Data.SqlDbType.VarChar, 2, "STType"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STTypeSequence", System.Data.SqlDbType.SmallInt, 2, "STTypeSequence"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STTypeDisplay", System.Data.SqlDbType.VarChar, 50, "STTypeDisplay"))
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE STTypes SET STType = @STType, STTypeSequence = @STTypeSequence, STTypeDisp" & _
        "lay = @STTypeDisplay WHERE (STType = @Original_STType) AND (STTypeDisplay = @Ori" & _
        "ginal_STTypeDisplay OR @Original_STTypeDisplay IS NULL AND STTypeDisplay IS NULL" & _
        ") AND (STTypeSequence = @Original_STTypeSequence OR @Original_STTypeSequence IS " & _
        "NULL AND STTypeSequence IS NULL); SELECT STType, STTypeSequence, STTypeDisplay F" & _
        "ROM STTypes WHERE (STType = @STType)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STType", System.Data.SqlDbType.VarChar, 2, "STType"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STTypeSequence", System.Data.SqlDbType.SmallInt, 2, "STTypeSequence"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@STTypeDisplay", System.Data.SqlDbType.VarChar, 50, "STTypeDisplay"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STTypeDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STTypeDisplay", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STTypeSequence", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STTypeSequence", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM STTypes WHERE (STType = @Original_STType) AND (STTypeDisplay = @Origi" & _
        "nal_STTypeDisplay OR @Original_STTypeDisplay IS NULL AND STTypeDisplay IS NULL) " & _
        "AND (STTypeSequence = @Original_STTypeSequence OR @Original_STTypeSequence IS NU" & _
        "LL AND STTypeSequence IS NULL)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STTypeDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STTypeDisplay", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_STTypeSequence", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "STTypeSequence", System.Data.DataRowVersion.Original, Nothing))
        '
        'daSTTypes
        '
        Me.daSTTypes.DeleteCommand = Me.SqlDeleteCommand2
        Me.daSTTypes.InsertCommand = Me.SqlInsertCommand2
        Me.daSTTypes.SelectCommand = Me.SqlSelectCommand2
        Me.daSTTypes.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "STTypes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("STType", "STType"), New System.Data.Common.DataColumnMapping("STTypeSequence", "STTypeSequence"), New System.Data.Common.DataColumnMapping("STTypeDisplay", "STTypeDisplay")})})
        Me.daSTTypes.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT BRID, SIID, MTID, SIConversionToPrimary, MTPrimaryUOM, SIName, SIInventory" & _
        "UOM, SICurrentInventory, SICurrentValue, MTPrimaryUOMShortName, SICurrentPurchas" & _
        "eCost, SICurrentInventoryInPrimaryUOM, SIInventoryUOMShortName FROM VStockedItem" & _
        "s WHERE (BRID = @BRID)"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection1
        Me.SqlSelectCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO VStockedItems(BRID, MTID, SIConversionToPrimary, MTPrimaryUOM, SIName" & _
        ", SIInventoryUOM, SICurrentInventory, SICurrentValue, MTPrimaryUOMShortName, SIC" & _
        "urrentPurchaseCost, SICurrentInventoryInPrimaryUOM, SIInventoryUOMShortName) VAL" & _
        "UES (@BRID, @MTID, @SIConversionToPrimary, @MTPrimaryUOM, @SIName, @SIInventoryU" & _
        "OM, @SICurrentInventory, @SICurrentValue, @MTPrimaryUOMShortName, @SICurrentPurc" & _
        "haseCost, @SICurrentInventoryInPrimaryUOM, @SIInventoryUOMShortName); SELECT BRI" & _
        "D, SIID, MTID, SIConversionToPrimary, MTPrimaryUOM, SIName, SIInventoryUOM, SICu" & _
        "rrentInventory, SICurrentValue, MTPrimaryUOMShortName, SICurrentPurchaseCost, SI" & _
        "CurrentInventoryInPrimaryUOM, SIInventoryUOMShortName FROM VStockedItems"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection1
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIConversionToPrimary", System.Data.SqlDbType.Real, 4, "SIConversionToPrimary"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTPrimaryUOM", System.Data.SqlDbType.VarChar, 2, "MTPrimaryUOM"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIName", System.Data.SqlDbType.VarChar, 50, "SIName"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIInventoryUOM", System.Data.SqlDbType.VarChar, 2, "SIInventoryUOM"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SICurrentInventory", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "SICurrentInventory", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SICurrentValue", System.Data.SqlDbType.Money, 8, "SICurrentValue"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTPrimaryUOMShortName", System.Data.SqlDbType.VarChar, 50, "MTPrimaryUOMShortName"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SICurrentPurchaseCost", System.Data.SqlDbType.Money, 8, "SICurrentPurchaseCost"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SICurrentInventoryInPrimaryUOM", System.Data.SqlDbType.Real, 4, "SICurrentInventoryInPrimaryUOM"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIInventoryUOMShortName", System.Data.SqlDbType.VarChar, 50, "SIInventoryUOMShortName"))
        '
        'daStockedItems
        '
        Me.daStockedItems.InsertCommand = Me.SqlInsertCommand3
        Me.daStockedItems.SelectCommand = Me.SqlSelectCommand3
        Me.daStockedItems.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VStockedItems", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("SIID", "SIID"), New System.Data.Common.DataColumnMapping("MTID", "MTID"), New System.Data.Common.DataColumnMapping("SIConversionToPrimary", "SIConversionToPrimary"), New System.Data.Common.DataColumnMapping("MTPrimaryUOM", "MTPrimaryUOM"), New System.Data.Common.DataColumnMapping("SIName", "SIName"), New System.Data.Common.DataColumnMapping("SIInventoryUOM", "SIInventoryUOM"), New System.Data.Common.DataColumnMapping("SICurrentInventory", "SICurrentInventory"), New System.Data.Common.DataColumnMapping("SICurrentValue", "SICurrentValue"), New System.Data.Common.DataColumnMapping("MTPrimaryUOMShortName", "MTPrimaryUOMShortName"), New System.Data.Common.DataColumnMapping("SICurrentPurchaseCost", "SICurrentPurchaseCost"), New System.Data.Common.DataColumnMapping("SICurrentInventoryInPrimaryUOM", "SICurrentInventoryInPrimaryUOM"), New System.Data.Common.DataColumnMapping("SIInventoryUOMShortName", "SIInventoryUOMShortName")})})
        '
        'lvStockTracking2
        '
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "lvStockTracking2"
        Me.Size = New System.Drawing.Size(632, 392)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSIID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSTType, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return hSqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            hSqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private Sub EnableDisable()
    End Sub

    Public Function FillDataSet() As Integer Implements IListControl.FillDataSet
        Dim cursor As System.Windows.Forms.Cursor = ExplorerForm.Cursor
        ExplorerForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
        ' Open connection
        Dim trans As SqlClient.SqlTransaction = Connection.BeginTransaction(IsolationLevel.ReadUncommitted)
        Power.Library.Library.ApplyTransactionToAllDataAdapters(trans, Me)
        'Me.GridControl1.DataSource.Clear()
        daSTTypes.Fill(dataSet)
        daStockedItems.Fill(dataSet)
        dataSet.StockTracking.Clear()
        FillDataSet = SqlDataAdapter.Fill(dataSet)
        'Close connecton
        trans.Commit()
        ExplorerForm.Cursor = cursor
    End Function

#Region " Filters "

    Private Sub SetUpFilters()
        ' Set Up Default Advanced Filter
        beiFromDate.EditValue = DateAdd(DateInterval.Month, -6, Today.Date)
        beiToDate.EditValue = Nothing
        UpdateDateFilter()
        GridView1.ClearColumnsFilter()
    End Sub

    Private Sub DateFilter_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beiFromDate.EditValueChanged, beiToDate.EditValueChanged
        UpdateDateFilter()
    End Sub

    Private DateFilterIsListening As Boolean = False ' This is so this doesn't trigger during load (this will happen in code)
    Private Sub UpdateDateFilter()
        Me.SqlDataAdapter.SelectCommand.Parameters("@FROM_DATE").Value = IsNull(beiFromDate.EditValue, DBNull.Value)
        Me.SqlDataAdapter.SelectCommand.Parameters("@TO_DATE").Value = IsNull(beiToDate.EditValue, DBNull.Value)
        If DateFilterIsListening Then
            Me.dataSet.Tables(Me.SqlDataAdapter.TableMappings(0).DataSetTable).Clear()
            FillDataSet()
        End If
    End Sub

    Private Sub btnResetFilters_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnResetFilters.ItemClick
        DateFilterIsListening = False
        SetUpFilters()
        Me.dataSet.Tables(Me.SqlDataAdapter.TableMappings(0).DataSetTable).Clear()
        FillDataSet()
        DateFilterIsListening = True
    End Sub

#End Region

    Private Sub GridView1_RowCountChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.RowCountChanged
        ExplorerForm.UpdateNumItems(NumItems)
        ExplorerForm.UpdateVisibleItems(VisibleItems)
    End Sub

    Public ReadOnly Property VisibleItems() As Integer Implements Power.Library.IListControl.VisibleItems
        Get
            Return GridView1.RowCount
        End Get
    End Property

    Public ReadOnly Property NumItems() As Integer Implements Power.Library.IListControl.NumItems
        Get
            Return dataSet.Tables(SqlDataAdapter.TableMappings(0).DataSetTable).Rows.Count
        End Get
    End Property

    Private hBRID As Integer
    Private Property BRID() As Integer
        Get
            Return hBRID
        End Get
        Set(ByVal Value As Integer)
            hBRID = Value
            SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = Value
            daStockedItems.SelectCommand.Parameters("@BRID").Value = Value
        End Set
    End Property

    Public ReadOnly Property SelectedRow() As DataRow
        Get
            If Not GridView1.GetSelectedRows Is Nothing Then
                Return GridView1.GetDataRow(GridView1.GetSelectedRows(0))
            End If
        End Get
    End Property

    Public ReadOnly Property SelectedRowField(ByVal Field As String) As Object
        Get
            If Not SelectedRow Is Nothing Then
                Return SelectedRow.Item(Field)
            Else
                Return DBNull.Value
            End If
        End Get
    End Property

    Public Function SelectRow(ByVal BRID As Int32, ByVal SAID As Int64) As Boolean
        Dim i As Integer = 0
        Do Until False
            Try
                If GridView1.GetDataRow(GridView1.GetVisibleRowHandle(i))("BRID") = BRID And _
                        GridView1.GetDataRow(GridView1.GetVisibleRowHandle(i))("SAID") = SAID Then
                    GridView1.ClearSelection()
                    GridView1.SelectRow(GridView1.GetVisibleRowHandle(i))
                    Return True
                End If
                i += 1
            Catch ex As NullReferenceException
                Return False
            End Try
        Loop
        Return False
    End Function

#Region " New, Open and Delete "

    Private Sub DataListView_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GridControl1.DoubleClick
        List_Edit()
    End Sub

    Private Sub btnNew_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
        List_New()
    End Sub

    Public Sub List_New() Implements IListControl.List_New
    End Sub

    Private Sub btnEdit_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
        List_Edit()
    End Sub

    Public Sub List_Edit() Implements IListControl.List_Edit
    End Sub

    Public Sub List_Cancel() Implements Power.Library.IListControl.List_Cancel

    End Sub

    Public Sub List_Uncancel() Implements Power.Library.IListControl.List_Uncancel

    End Sub

    Private Sub btnDelete_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
        List_Delete()
    End Sub

    Public Sub List_Delete() Implements IListControl.List_Delete
    End Sub

    Private Function StockAdjustmentExists(ByVal BRID As Int32, ByVal SAID As Int64) As Boolean
        Dim cnn As SqlClient.SqlConnection = Connection
        Dim cmd As New SqlClient.SqlCommand("SELECT Count(SAID) FROM StockAdjustments WHERE BRID = @BRID AND SAID = @SAID", cnn)
        cmd.CommandType = CommandType.Text
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@SAID", SAID)
        Return cmd.ExecuteScalar > 0
    End Function

#End Region

    Private hExplorerForm As Form
    Public ReadOnly Property ExplorerForm() As frmExplorer
        Get
            Return hExplorerForm
        End Get
    End Property

#Region " Printing "

    Private Sub btnPrint_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrint.ItemClick
        Print(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub Print(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.Print
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.PrintDlg()
    End Sub

    Private Sub btnPrintPreview_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrintPreview.ItemClick
        PrintPreview(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub PrintPreview(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.PrintPreview
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(ExplorerForm.PrintingSystem)
        link.Component = GridControl1
        link.ShowPreview()
    End Sub

    Private Sub btnQuickPrint_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnQuickPrint.ItemClick
        QuickPrint(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub QuickPrint(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.QuickPrint
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.Print(PrintingSystem.PageSettings.PrinterName)
    End Sub

#End Region

    Public ReadOnly Property AllowDelete() As Boolean Implements Power.Library.IListControl.AllowDelete
        Get
            Return Not HasRole("branch_read_only")
        End Get
    End Property

    Public ReadOnly Property AllowEdit() As Boolean Implements Power.Library.IListControl.AllowEdit
        Get
            Return True
        End Get
    End Property

    Public ReadOnly Property AllowNew() As Boolean Implements Power.Library.IListControl.AllowNew
        Get
            Return Not HasRole("branch_read_only")
        End Get
    End Property

    Public ReadOnly Property AllowCancel() As Boolean Implements Power.Library.IListControl.AllowCancel
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property AllowUncancel() As Boolean Implements Power.Library.IListControl.AllowUncancel
        Get
            Return False
        End Get
    End Property

    Private Sub btnHelp_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHelp.ItemClick
        ShowHelpTopic(ExplorerForm, "InventoryTrackingHistory.html")
    End Sub
End Class
