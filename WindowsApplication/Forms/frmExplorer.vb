Imports Power.Forms
Imports Power.Library

Public Class frmExplorer
    Inherits DevExpress.XtraEditors.XtraForm

    Private HeadOfficeForm As frmExplorerHeadOffice
    Private IsInitializing As Boolean = True

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal BRID As Integer, Optional ByVal HeadOfficeForm As DevExpress.XtraEditors.XtraForm = Nothing)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        IsInitializing = False

        SqlConnection.ConnectionString = ConnectionString
        SqlConnection.Open()
        Me.BRID = BRID
        tvwMenu.Filter.Fields.Add("BRID", BRID)
        NodeCollection = New ArrayList
        NodeCollectionCursor = -1
        Me.Text = "Franchise Manager - " & sp_GetBRShortName(BRID, SqlConnection)

        Me.HeadOfficeForm = HeadOfficeForm
        If HeadOfficeForm Is Nothing Then
            Me.WindowState = FormWindowState.Normal
        End If
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents pnlMain As DevExpress.XtraEditors.PanelControl
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlOutside As DevExpress.XtraEditors.PanelControl
    Friend WithEvents pnlHeader As DevExpress.XtraEditors.PanelControl
    Friend WithEvents ImageListSmall As System.Windows.Forms.ImageList
    Friend WithEvents ImageListToolBar As System.Windows.Forms.ImageList
    Friend WithEvents ImageListLarge As System.Windows.Forms.ImageList
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents bMainMenu As DevExpress.XtraBars.Bar
    Friend WithEvents btnNew As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnEdit As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnDelete As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnExit As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Splitter1 As System.Windows.Forms.Splitter
    Friend WithEvents subReports As DevExpress.XtraBars.BarSubItem
    Friend WithEvents subOperations As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnOrderingIncompleteReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnUnfinishedJobsReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnDebtorsReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnJobsFinishedOnTimeReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnJobIssuesReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPredictedSalesReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subCalendars As DevExpress.XtraBars.BarSubItem
    Friend WithEvents subProfit As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnAllJobsReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnProfitReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subMaterials As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnStockReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnWasteReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnStockRequirementsReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subDirectLabourandSalespeople As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnDirectLabourandSalespeopleSpecifiedinJobs As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnSalespeopleQuoteStatistics As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnSalespeopleJobStatistics As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnMeasurersReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subMarketing As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnPrimaryEnquirySourcesReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnSecondaryEnquirySourcesReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPostCodesReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subOptions As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnBranchProperties As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnBranchUsers As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subHelp As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnAboutFranchiseManager As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subFile As DevExpress.XtraBars.BarSubItem
    Friend WithEvents bStatus As DevExpress.XtraBars.Bar
    Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents DsGTMS As WindowsApplication.dsGTMS
    Friend WithEvents daExpenses As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents pnlSpacer2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents pnlSpacer1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents BarAndDockingController1 As DevExpress.XtraBars.BarAndDockingController
    Friend WithEvents DockManager1 As DevExpress.XtraBars.Docking.DockManager
    Friend WithEvents dpNavigation As DevExpress.XtraBars.Docking.DockPanel
    Friend WithEvents DockPanel1_Container As DevExpress.XtraBars.Docking.ControlContainer
    Friend WithEvents tvwMenu As Power.Forms.TreeView
    Friend WithEvents BarToolbarsListItem As DevExpress.XtraBars.BarToolbarsListItem
    Friend WithEvents subSalespersonsCalendars As DevExpress.XtraBars.BarSubItem
    Friend WithEvents subJobBoardbyDirectLabour As DevExpress.XtraBars.BarSubItem
    Friend WithEvents subDirectLabourJobList As DevExpress.XtraBars.BarSubItem
    Friend WithEvents subSalespersonJobList As DevExpress.XtraBars.BarSubItem
    Friend WithEvents subSalespersonAmountOwingReport As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnAutoHideNavigationPanel As DevExpress.XtraBars.BarCheckItem
    Friend WithEvents btnPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPrintPreview As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnQuickPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPageSetup As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PrintingSystem As DevExpress.XtraPrinting.PrintingSystem
    Friend WithEvents ControlContainer1 As DevExpress.XtraBars.Docking.ControlContainer
    Friend WithEvents DockPanel1 As DevExpress.XtraBars.Docking.DockPanel
    Friend WithEvents lblSectionTitle As System.Windows.Forms.Label
    Friend WithEvents subStaticForms As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnJobIssuesForm As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnNonStockedMaterialInJobForm As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ImageListPanels As System.Windows.Forms.ImageList
    Friend WithEvents dpHelp As DevExpress.XtraBars.Docking.DockPanel
    Friend WithEvents DockPanel2_Container As DevExpress.XtraBars.Docking.ControlContainer
    Friend WithEvents pnlHelp As DevExpress.XtraEditors.PanelControl
    Friend WithEvents rtbHelp As Power.Forms.RichTextBox
    Friend WithEvents btnAutoHideHelpPanel As DevExpress.XtraBars.BarCheckItem
    Friend WithEvents hideContainerRight As DevExpress.XtraBars.Docking.AutoHideContainer
    Friend WithEvents subManualForms As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnManualQuoteRequestForm As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnShowHelp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnHelp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnQuoteRequestsCancelledReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents dvDirectLabourInCalendar As System.Data.DataView
    Friend WithEvents subQuoteRequestsCancelledList As DevExpress.XtraBars.BarSubItem
    Friend WithEvents dvSalesRepsInCalendar As System.Data.DataView
    Friend WithEvents subQuotesOutstandingReport As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnQuoteRequestsCancelledListNone As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnQuotesOutstandingReportNone As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnDirectSalesReportNone As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnFollowupReportNone As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnJobFolderCoverStamp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subExampleOrganisationalForms As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnQuoteRequestCancellationForm As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnManualJobBookingForm As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subTemplaterMeasurerForms As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnClientConfirmationForm As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnChangesToJobDetailsForm As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subSalespersonForms As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnCareOfGranite As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subInstallerForms As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnInstallerStatusReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subForms As DevExpress.XtraBars.BarSubItem
    Friend WithEvents bsiNumItems As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents bsiVisibleItems As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents subTrendProductInJobReport As DevExpress.XtraBars.BarSubItem
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daMaterials As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents dvGranite As System.Data.DataView
    Friend WithEvents dvDirectLabourInJobs As System.Data.DataView
    Friend WithEvents dvSalesRepsInJobs As System.Data.DataView
    Friend WithEvents subJobBoardByTask As DevExpress.XtraBars.BarSubItem
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daTasks As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents dvTasks As System.Data.DataView
    Friend WithEvents dvStaffMembers As System.Data.DataView
    Friend WithEvents subFollowUpReport As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnHoursCostAndVolumeReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnCancel As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subView As DevExpress.XtraBars.BarSubItem
    Friend WithEvents dvSalesRepCostingGroup As System.Data.DataView
    Friend WithEvents dvDirectLabourCostingGroup As System.Data.DataView
    Friend WithEvents dvSalesReps As System.Data.DataView
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents btnHOJobsFinishedOnTimeReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subHOReports As DevExpress.XtraBars.BarSubItem
    Friend WithEvents subHOOperations As DevExpress.XtraBars.BarSubItem
    Friend WithEvents subHOMarketing As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnHOPrimaryEnquirySourcesReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subHOMaterials As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnHOWasteReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subHODirectLabourandSalespeople As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnHoursCostsAndVolumeReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subHOProfit As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnHOAllJobsReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnHODebtorStatisticsReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnHOJobIssuesReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPrimaryEnquirySourcesPostCodesReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subRepJobs As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnHOQuoteStatisticsReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnAppliancesNotReceivedReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnJobTypesReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnSalespersonsCalendarsAll As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnJobBoardbyDirectLabourAll As DevExpress.XtraBars.BarButtonItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmExplorer))
        Me.ImageListToolBar = New System.Windows.Forms.ImageList(Me.components)
        Me.pnlMain = New DevExpress.XtraEditors.PanelControl
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.pnlHeader = New DevExpress.XtraEditors.PanelControl
        Me.lblSectionTitle = New System.Windows.Forms.Label
        Me.pnlSpacer2 = New DevExpress.XtraEditors.PanelControl
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.pnlSpacer1 = New DevExpress.XtraEditors.PanelControl
        Me.pnlOutside = New DevExpress.XtraEditors.PanelControl
        Me.Splitter1 = New System.Windows.Forms.Splitter
        Me.ImageListSmall = New System.Windows.Forms.ImageList(Me.components)
        Me.ImageListLarge = New System.Windows.Forms.ImageList(Me.components)
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider
        Me.BarManager1 = New DevExpress.XtraBars.BarManager
        Me.bMainMenu = New DevExpress.XtraBars.Bar
        Me.subFile = New DevExpress.XtraBars.BarSubItem
        Me.btnNew = New DevExpress.XtraBars.BarButtonItem
        Me.btnEdit = New DevExpress.XtraBars.BarButtonItem
        Me.btnCancel = New DevExpress.XtraBars.BarButtonItem
        Me.btnDelete = New DevExpress.XtraBars.BarButtonItem
        Me.btnPageSetup = New DevExpress.XtraBars.BarButtonItem
        Me.btnPrintPreview = New DevExpress.XtraBars.BarButtonItem
        Me.btnPrint = New DevExpress.XtraBars.BarButtonItem
        Me.btnExit = New DevExpress.XtraBars.BarButtonItem
        Me.subView = New DevExpress.XtraBars.BarSubItem
        Me.btnAutoHideNavigationPanel = New DevExpress.XtraBars.BarCheckItem
        Me.btnAutoHideHelpPanel = New DevExpress.XtraBars.BarCheckItem
        Me.subReports = New DevExpress.XtraBars.BarSubItem
        Me.subOperations = New DevExpress.XtraBars.BarSubItem
        Me.subQuotesOutstandingReport = New DevExpress.XtraBars.BarSubItem
        Me.btnQuotesOutstandingReportNone = New DevExpress.XtraBars.BarButtonItem
        Me.btnDirectSalesReportNone = New DevExpress.XtraBars.BarButtonItem
        Me.btnFollowupReportNone = New DevExpress.XtraBars.BarButtonItem

        Me.btnOrderingIncompleteReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnAppliancesNotReceivedReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnUnfinishedJobsReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnDebtorsReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnJobsFinishedOnTimeReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnJobIssuesReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnPredictedSalesReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnQuoteRequestsCancelledReport = New DevExpress.XtraBars.BarButtonItem
        Me.subQuoteRequestsCancelledList = New DevExpress.XtraBars.BarSubItem
        Me.btnQuoteRequestsCancelledListNone = New DevExpress.XtraBars.BarButtonItem
        Me.subFollowUpReport = New DevExpress.XtraBars.BarSubItem
        Me.subCalendars = New DevExpress.XtraBars.BarSubItem
        Me.subSalespersonsCalendars = New DevExpress.XtraBars.BarSubItem
        Me.btnSalespersonsCalendarsAll = New DevExpress.XtraBars.BarButtonItem
        Me.subJobBoardbyDirectLabour = New DevExpress.XtraBars.BarSubItem
        Me.btnJobBoardbyDirectLabourAll = New DevExpress.XtraBars.BarButtonItem
        Me.subJobBoardByTask = New DevExpress.XtraBars.BarSubItem
        Me.subMarketing = New DevExpress.XtraBars.BarSubItem
        Me.btnPrimaryEnquirySourcesReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnSecondaryEnquirySourcesReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnPostCodesReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnPrimaryEnquirySourcesPostCodesReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnJobTypesReport = New DevExpress.XtraBars.BarButtonItem
        Me.subMaterials = New DevExpress.XtraBars.BarSubItem
        Me.btnStockReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnWasteReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnStockRequirementsReport = New DevExpress.XtraBars.BarButtonItem
        Me.subTrendProductInJobReport = New DevExpress.XtraBars.BarSubItem
        Me.subDirectLabourandSalespeople = New DevExpress.XtraBars.BarSubItem
        Me.btnDirectLabourandSalespeopleSpecifiedinJobs = New DevExpress.XtraBars.BarButtonItem
        Me.subDirectLabourJobList = New DevExpress.XtraBars.BarSubItem
        Me.subSalespersonJobList = New DevExpress.XtraBars.BarSubItem
        Me.btnHoursCostAndVolumeReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnSalespeopleQuoteStatistics = New DevExpress.XtraBars.BarButtonItem
        Me.btnSalespeopleJobStatistics = New DevExpress.XtraBars.BarButtonItem
        Me.subRepJobs = New DevExpress.XtraBars.BarSubItem
        Me.subSalespersonAmountOwingReport = New DevExpress.XtraBars.BarSubItem
        Me.btnMeasurersReport = New DevExpress.XtraBars.BarButtonItem
        Me.subProfit = New DevExpress.XtraBars.BarSubItem
        Me.btnAllJobsReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnProfitReport = New DevExpress.XtraBars.BarButtonItem
        Me.subHOReports = New DevExpress.XtraBars.BarSubItem
        Me.subHOOperations = New DevExpress.XtraBars.BarSubItem
        Me.btnHOJobsFinishedOnTimeReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnHODebtorStatisticsReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnHOJobIssuesReport = New DevExpress.XtraBars.BarButtonItem
        Me.btnHOQuoteStatisticsReport = New DevExpress.XtraBars.BarButtonItem
        Me.subHOMarketing = New DevExpress.XtraBars.BarSubItem
        Me.btnHOPrimaryEnquirySourcesReport = New DevExpress.XtraBars.BarButtonItem
        Me.subHOMaterials = New DevExpress.XtraBars.BarSubItem
        Me.btnHOWasteReport = New DevExpress.XtraBars.BarButtonItem
        Me.subHODirectLabourandSalespeople = New DevExpress.XtraBars.BarSubItem
        Me.btnHoursCostsAndVolumeReport = New DevExpress.XtraBars.BarButtonItem
        Me.subHOProfit = New DevExpress.XtraBars.BarSubItem
        Me.btnHOAllJobsReport = New DevExpress.XtraBars.BarButtonItem
        Me.subForms = New DevExpress.XtraBars.BarSubItem
        Me.subStaticForms = New DevExpress.XtraBars.BarSubItem
        Me.subExampleOrganisationalForms = New DevExpress.XtraBars.BarSubItem
        Me.btnJobFolderCoverStamp = New DevExpress.XtraBars.BarButtonItem
        Me.btnInstallerStatusReport = New DevExpress.XtraBars.BarButtonItem
        Me.subSalespersonForms = New DevExpress.XtraBars.BarSubItem
        Me.btnQuoteRequestCancellationForm = New DevExpress.XtraBars.BarButtonItem
        Me.subTemplaterMeasurerForms = New DevExpress.XtraBars.BarSubItem
        Me.btnClientConfirmationForm = New DevExpress.XtraBars.BarButtonItem
        Me.btnChangesToJobDetailsForm = New DevExpress.XtraBars.BarButtonItem
        Me.btnJobIssuesForm = New DevExpress.XtraBars.BarButtonItem
        Me.btnNonStockedMaterialInJobForm = New DevExpress.XtraBars.BarButtonItem
        Me.btnCareOfGranite = New DevExpress.XtraBars.BarButtonItem
        Me.subManualForms = New DevExpress.XtraBars.BarSubItem
        Me.btnManualQuoteRequestForm = New DevExpress.XtraBars.BarButtonItem
        Me.btnManualJobBookingForm = New DevExpress.XtraBars.BarButtonItem
        Me.subOptions = New DevExpress.XtraBars.BarSubItem
        Me.btnBranchProperties = New DevExpress.XtraBars.BarButtonItem
        Me.btnBranchUsers = New DevExpress.XtraBars.BarButtonItem
        Me.subHelp = New DevExpress.XtraBars.BarSubItem
        Me.btnHelp = New DevExpress.XtraBars.BarButtonItem
        Me.btnAboutFranchiseManager = New DevExpress.XtraBars.BarButtonItem
        Me.bStatus = New DevExpress.XtraBars.Bar
        Me.bsiNumItems = New DevExpress.XtraBars.BarStaticItem
        Me.bsiVisibleItems = New DevExpress.XtraBars.BarStaticItem
        Me.BarAndDockingController1 = New DevExpress.XtraBars.BarAndDockingController(Me.components)
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
        Me.DockManager1 = New DevExpress.XtraBars.Docking.DockManager
        Me.hideContainerRight = New DevExpress.XtraBars.Docking.AutoHideContainer
        Me.dpHelp = New DevExpress.XtraBars.Docking.DockPanel
        Me.DockPanel2_Container = New DevExpress.XtraBars.Docking.ControlContainer
        Me.pnlHelp = New DevExpress.XtraEditors.PanelControl
        Me.rtbHelp = New Power.Forms.RichTextBox
        Me.ImageListPanels = New System.Windows.Forms.ImageList(Me.components)
        Me.dpNavigation = New DevExpress.XtraBars.Docking.DockPanel
        Me.DockPanel1_Container = New DevExpress.XtraBars.Docking.ControlContainer
        Me.tvwMenu = New Power.Forms.TreeView
        Me.btnShowHelp = New DevExpress.XtraBars.BarButtonItem
        Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem
        Me.BarToolbarsListItem = New DevExpress.XtraBars.BarToolbarsListItem
        Me.btnQuickPrint = New DevExpress.XtraBars.BarButtonItem
        Me.subInstallerForms = New DevExpress.XtraBars.BarSubItem
        Me.DsGTMS = New WindowsApplication.dsGTMS
        Me.daExpenses = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.dvSalesRepCostingGroup = New System.Data.DataView
        Me.dvDirectLabourInCalendar = New System.Data.DataView
        Me.PrintingSystem = New DevExpress.XtraPrinting.PrintingSystem(Me.components)
        Me.ControlContainer1 = New DevExpress.XtraBars.Docking.ControlContainer
        Me.DockPanel1 = New DevExpress.XtraBars.Docking.DockPanel
        Me.dvSalesRepsInCalendar = New System.Data.DataView
        Me.dvDirectLabourCostingGroup = New System.Data.DataView
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.daMaterials = New System.Data.SqlClient.SqlDataAdapter
        Me.dvGranite = New System.Data.DataView
        Me.dvSalesRepsInJobs = New System.Data.DataView
        Me.dvDirectLabourInJobs = New System.Data.DataView
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.daTasks = New System.Data.SqlClient.SqlDataAdapter
        Me.dvTasks = New System.Data.DataView
        Me.dvStaffMembers = New System.Data.DataView
        Me.dvSalesReps = New System.Data.DataView
        CType(Me.pnlMain, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlHeader.SuspendLayout()
        CType(Me.pnlSpacer2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlSpacer1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pnlOutside, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlOutside.SuspendLayout()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hideContainerRight.SuspendLayout()
        Me.dpHelp.SuspendLayout()
        Me.DockPanel2_Container.SuspendLayout()
        CType(Me.pnlHelp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlHelp.SuspendLayout()
        Me.dpNavigation.SuspendLayout()
        Me.DockPanel1_Container.SuspendLayout()
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvSalesRepCostingGroup, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvDirectLabourInCalendar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrintingSystem, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DockPanel1.SuspendLayout()
        CType(Me.dvSalesRepsInCalendar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvDirectLabourCostingGroup, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvGranite, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvSalesRepsInJobs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvDirectLabourInJobs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvTasks, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvStaffMembers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvSalesReps, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ImageListToolBar
        '
        Me.ImageListToolBar.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImageListToolBar.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageListToolBar.ImageStream = CType(resources.GetObject("ImageListToolBar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListToolBar.TransparentColor = System.Drawing.Color.Transparent
        '
        'pnlMain
        '
        Me.pnlMain.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.pnlMain.Appearance.Options.UseBackColor = True
        Me.pnlMain.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(1, 40)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(683, 475)
        Me.pnlMain.TabIndex = 4
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;user id=sa;integrated security=SSPI;data sou" & _
        "rce=""SERVER\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'pnlHeader
        '
        Me.pnlHeader.Appearance.BackColor = System.Drawing.SystemColors.ControlDark
        Me.pnlHeader.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.pnlHeader.Appearance.Options.UseBackColor = True
        Me.pnlHeader.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.pnlHeader.Controls.Add(Me.lblSectionTitle)
        Me.pnlHeader.Controls.Add(Me.pnlSpacer2)
        Me.pnlHeader.Controls.Add(Me.PictureBox1)
        Me.pnlHeader.Controls.Add(Me.pnlSpacer1)
        Me.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeader.DockPadding.All = 2
        Me.pnlHeader.Location = New System.Drawing.Point(1, 1)
        Me.pnlHeader.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat
        Me.pnlHeader.LookAndFeel.UseDefaultLookAndFeel = False
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Size = New System.Drawing.Size(683, 36)
        Me.pnlHeader.TabIndex = 5
        '
        'lblSectionTitle
        '
        Me.lblSectionTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblSectionTitle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblSectionTitle.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSectionTitle.ForeColor = System.Drawing.Color.White
        Me.lblSectionTitle.Location = New System.Drawing.Point(54, 2)
        Me.lblSectionTitle.Name = "lblSectionTitle"
        Me.lblSectionTitle.Size = New System.Drawing.Size(627, 32)
        Me.lblSectionTitle.TabIndex = 0
        Me.lblSectionTitle.Text = "Franchise Manager for Align Kitchens"
        Me.lblSectionTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlSpacer2
        '
        Me.pnlSpacer2.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.pnlSpacer2.Appearance.Options.UseBackColor = True
        Me.pnlSpacer2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.pnlSpacer2.Dock = System.Windows.Forms.DockStyle.Left
        Me.pnlSpacer2.Location = New System.Drawing.Point(42, 2)
        Me.pnlSpacer2.Name = "pnlSpacer2"
        Me.pnlSpacer2.Size = New System.Drawing.Size(12, 32)
        Me.pnlSpacer2.TabIndex = 1
        Me.pnlSpacer2.Text = "PanelControl3"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PictureBox1.Location = New System.Drawing.Point(10, 2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(32, 32)
        Me.PictureBox1.TabIndex = 3
        Me.PictureBox1.TabStop = False
        '
        'pnlSpacer1
        '
        Me.pnlSpacer1.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.pnlSpacer1.Appearance.Options.UseBackColor = True
        Me.pnlSpacer1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.pnlSpacer1.Dock = System.Windows.Forms.DockStyle.Left
        Me.pnlSpacer1.Location = New System.Drawing.Point(2, 2)
        Me.pnlSpacer1.Name = "pnlSpacer1"
        Me.pnlSpacer1.Size = New System.Drawing.Size(8, 32)
        Me.pnlSpacer1.TabIndex = 0
        Me.pnlSpacer1.Text = "PanelControl2"
        '
        'pnlOutside
        '
        Me.pnlOutside.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.pnlOutside.Controls.Add(Me.pnlMain)
        Me.pnlOutside.Controls.Add(Me.Splitter1)
        Me.pnlOutside.Controls.Add(Me.pnlHeader)
        Me.pnlOutside.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlOutside.DockPadding.All = 1
        Me.pnlOutside.Location = New System.Drawing.Point(257, 20)
        Me.pnlOutside.Name = "pnlOutside"
        Me.pnlOutside.Size = New System.Drawing.Size(685, 516)
        Me.pnlOutside.TabIndex = 6
        '
        'Splitter1
        '
        Me.Splitter1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Splitter1.Enabled = False
        Me.Splitter1.Location = New System.Drawing.Point(1, 37)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(683, 3)
        Me.Splitter1.TabIndex = 9
        Me.Splitter1.TabStop = False
        '
        'ImageListSmall
        '
        Me.ImageListSmall.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImageListSmall.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageListSmall.ImageStream = CType(resources.GetObject("ImageListSmall.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListSmall.TransparentColor = System.Drawing.Color.Transparent
        '
        'ImageListLarge
        '
        Me.ImageListLarge.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImageListLarge.ImageSize = New System.Drawing.Size(32, 32)
        Me.ImageListLarge.ImageStream = CType(resources.GetObject("ImageListLarge.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListLarge.TransparentColor = System.Drawing.Color.Transparent
        '
        'HelpProvider1
        '
        Me.HelpProvider1.HelpNamespace = "C:\Documents and Settings\cjrada\My Documents\HelpProject.chm"
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.bMainMenu, Me.bStatus})
        Me.BarManager1.Categories.AddRange(New DevExpress.XtraBars.BarManagerCategory() {New DevExpress.XtraBars.BarManagerCategory("Hidden Category", New System.Guid("0062ba9e-f3b2-483b-b446-94cf4d70d00b"), False), New DevExpress.XtraBars.BarManagerCategory("Built-in Menus", New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd"), False), New DevExpress.XtraBars.BarManagerCategory("File", New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")), New DevExpress.XtraBars.BarManagerCategory("Reports", New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")), New DevExpress.XtraBars.BarManagerCategory("Forms", New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")), New DevExpress.XtraBars.BarManagerCategory("Options", New System.Guid("b4347cfa-b71d-4f3d-abe2-213a7e6e3fc6")), New DevExpress.XtraBars.BarManagerCategory("Help", New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")), New DevExpress.XtraBars.BarManagerCategory("View", New System.Guid("80d922eb-0420-4701-a571-d1e304fb1b67"))})
        Me.BarManager1.Controller = Me.BarAndDockingController1
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.DockManager = Me.DockManager1
        Me.BarManager1.Form = Me
        Me.BarManager1.Images = Me.ImageListToolBar
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnNew, Me.btnEdit, Me.btnCancel, Me.subFile, Me.btnDelete, Me.subReports, Me.subOperations, Me.subQuotesOutstandingReport, Me.btnOrderingIncompleteReport, Me.btnAppliancesNotReceivedReport, Me.btnUnfinishedJobsReport, Me.btnDebtorsReport, Me.btnJobsFinishedOnTimeReport, Me.btnJobIssuesReport, Me.btnPredictedSalesReport, Me.btnQuoteRequestsCancelledReport, Me.subQuoteRequestsCancelledList, Me.subFollowUpReport, Me.subCalendars, Me.subProfit, Me.subSalespersonsCalendars, Me.btnSalespersonsCalendarsAll, Me.subJobBoardbyDirectLabour, Me.btnJobBoardbyDirectLabourAll, Me.subJobBoardByTask, Me.btnPrimaryEnquirySourcesReport, Me.btnSecondaryEnquirySourcesReport, Me.btnPostCodesReport, Me.btnJobTypesReport, Me.subMaterials, Me.btnStockReport, Me.btnWasteReport, Me.btnStockRequirementsReport, Me.subTrendProductInJobReport, Me.subDirectLabourandSalespeople, Me.btnDirectLabourandSalespeopleSpecifiedinJobs, Me.subDirectLabourJobList, Me.subSalespersonJobList, Me.btnHoursCostAndVolumeReport, Me.btnSalespeopleQuoteStatistics, Me.btnSalespeopleJobStatistics, Me.subSalespersonAmountOwingReport, Me.btnMeasurersReport, Me.btnAllJobsReport, Me.btnProfitReport, Me.subMarketing, Me.subOptions, Me.btnBranchProperties, Me.btnBranchUsers, Me.subHelp, Me.btnHelp, Me.btnShowHelp, Me.btnAboutFranchiseManager, Me.BarSubItem1, Me.subView, Me.BarToolbarsListItem, Me.btnAutoHideNavigationPanel, Me.btnPageSetup, Me.btnPrintPreview, Me.btnQuickPrint, Me.btnPrint, Me.btnExit, Me.subStaticForms, Me.btnJobFolderCoverStamp, Me.btnInstallerStatusReport, Me.btnQuoteRequestCancellationForm, Me.btnClientConfirmationForm, Me.btnChangesToJobDetailsForm, Me.btnJobIssuesForm, Me.btnNonStockedMaterialInJobForm, Me.btnCareOfGranite, Me.btnAutoHideHelpPanel, Me.subManualForms, Me.btnManualQuoteRequestForm, Me.btnQuotesOutstandingReportNone, Me.btnDirectSalesReportNone, Me.btnQuoteRequestsCancelledListNone, Me.btnFollowupReportNone, Me.subExampleOrganisationalForms, Me.btnManualJobBookingForm, Me.subTemplaterMeasurerForms, Me.subForms, Me.subSalespersonForms, Me.subInstallerForms, Me.bsiNumItems, Me.bsiVisibleItems, Me.subHOReports, Me.subHOOperations, Me.btnHOJobsFinishedOnTimeReport, Me.btnHODebtorStatisticsReport, Me.btnHOJobIssuesReport, Me.btnHOQuoteStatisticsReport, Me.subHOMarketing, Me.btnHOPrimaryEnquirySourcesReport, Me.subHOMaterials, Me.btnHOWasteReport, Me.subHODirectLabourandSalespeople, Me.btnHoursCostsAndVolumeReport, Me.subHOProfit, Me.btnHOAllJobsReport, Me.btnPrimaryEnquirySourcesPostCodesReport, Me.subRepJobs})
        Me.BarManager1.MainMenu = Me.bMainMenu
        Me.BarManager1.MaxItemId = 118
        Me.BarManager1.StatusBar = Me.bStatus
        '
        'bMainMenu
        '
        Me.bMainMenu.BarName = "Main Menu"
        Me.bMainMenu.DockCol = 0
        Me.bMainMenu.DockRow = 0
        Me.bMainMenu.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bMainMenu.FloatLocation = New System.Drawing.Point(39, 185)
        Me.bMainMenu.FloatSize = New System.Drawing.Size(333, 20)
        Me.bMainMenu.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.subFile), New DevExpress.XtraBars.LinkPersistInfo(Me.subView), New DevExpress.XtraBars.LinkPersistInfo(Me.subReports), New DevExpress.XtraBars.LinkPersistInfo(Me.subHOReports), New DevExpress.XtraBars.LinkPersistInfo(Me.subForms), New DevExpress.XtraBars.LinkPersistInfo(Me.subOptions), New DevExpress.XtraBars.LinkPersistInfo(Me.subHelp)})
        Me.bMainMenu.OptionsBar.AllowQuickCustomization = False
        Me.bMainMenu.OptionsBar.DisableClose = True
        Me.bMainMenu.OptionsBar.MultiLine = True
        Me.bMainMenu.OptionsBar.UseWholeRow = True
        Me.bMainMenu.Text = "Main Menu"
        '
        'subFile
        '
        Me.subFile.Caption = "&File"
        Me.subFile.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subFile.Id = 21
        Me.subFile.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnNew), New DevExpress.XtraBars.LinkPersistInfo(Me.btnEdit), New DevExpress.XtraBars.LinkPersistInfo(Me.btnCancel), New DevExpress.XtraBars.LinkPersistInfo(Me.btnDelete), New DevExpress.XtraBars.LinkPersistInfo(Me.btnPageSetup, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnPrintPreview), New DevExpress.XtraBars.LinkPersistInfo(Me.btnPrint), New DevExpress.XtraBars.LinkPersistInfo(Me.btnExit, True)})
        Me.subFile.Name = "subFile"
        '
        'btnNew
        '
        Me.btnNew.Caption = "&New..."
        Me.btnNew.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnNew.Id = 19
        Me.btnNew.ImageIndex = 0
        Me.btnNew.ImageIndexDisabled = 0
        Me.btnNew.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N))
        Me.btnNew.Name = "btnNew"
        '
        'btnEdit
        '
        Me.btnEdit.Caption = "&Edit..."
        Me.btnEdit.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnEdit.Id = 20
        Me.btnEdit.ImageIndex = 1
        Me.btnEdit.ImageIndexDisabled = 1
        Me.btnEdit.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.E))
        Me.btnEdit.Name = "btnEdit"
        '
        'btnCancel
        '
        Me.btnCancel.Caption = "&Cancel"
        Me.btnCancel.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnCancel.Id = 91
        Me.btnCancel.ImageIndex = 2
        Me.btnCancel.ImageIndexDisabled = 2
        Me.btnCancel.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.D))
        Me.btnCancel.Name = "btnCancel"
        '
        'btnDelete
        '
        Me.btnDelete.Caption = "&Delete"
        Me.btnDelete.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnDelete.Id = 23
        Me.btnDelete.ImageIndex = 12
        Me.btnDelete.ImageIndexDisabled = 12
        Me.btnDelete.Name = "btnDelete"
        '
        'btnPageSetup
        '
        Me.btnPageSetup.Caption = "Page Set&up"
        Me.btnPageSetup.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPageSetup.Id = 47
        Me.btnPageSetup.Name = "btnPageSetup"
        '
        'btnPrintPreview
        '
        Me.btnPrintPreview.Caption = "Print Pre&view"
        Me.btnPrintPreview.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrintPreview.Id = 6
        Me.btnPrintPreview.ImageIndex = 10
        Me.btnPrintPreview.ImageIndexDisabled = 10
        Me.btnPrintPreview.Name = "btnPrintPreview"
        '
        'btnPrint
        '
        Me.btnPrint.Caption = "&Print..."
        Me.btnPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrint.Id = 5
        Me.btnPrint.ImageIndex = 11
        Me.btnPrint.ImageIndexDisabled = 11
        Me.btnPrint.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P))
        Me.btnPrint.Name = "btnPrint"
        '
        'btnExit
        '
        Me.btnExit.Caption = "E&xit"
        Me.btnExit.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnExit.Id = 25
        Me.btnExit.Name = "btnExit"
        '
        'subView
        '
        Me.subView.Caption = "&View"
        Me.subView.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subView.Id = 28
        Me.subView.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnAutoHideNavigationPanel, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnAutoHideHelpPanel)})
        Me.subView.Name = "subView"
        '
        'btnAutoHideNavigationPanel
        '
        Me.btnAutoHideNavigationPanel.Caption = "Hide &Navigation Panel"
        Me.btnAutoHideNavigationPanel.CategoryGuid = New System.Guid("80d922eb-0420-4701-a571-d1e304fb1b67")
        Me.btnAutoHideNavigationPanel.Id = 4
        Me.btnAutoHideNavigationPanel.ItemShortcut = New DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F6)
        Me.btnAutoHideNavigationPanel.Name = "btnAutoHideNavigationPanel"
        '
        'btnAutoHideHelpPanel
        '
        Me.btnAutoHideHelpPanel.Caption = "Hide &Help Panel"
        Me.btnAutoHideHelpPanel.CategoryGuid = New System.Guid("80d922eb-0420-4701-a571-d1e304fb1b67")
        Me.btnAutoHideHelpPanel.Checked = True
        Me.btnAutoHideHelpPanel.Id = 10
        Me.btnAutoHideHelpPanel.ItemShortcut = New DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F7)
        Me.btnAutoHideHelpPanel.Name = "btnAutoHideHelpPanel"
        '
        'subReports
        '
        Me.subReports.Caption = "&Reports"
        Me.subReports.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subReports.Id = 26
        Me.subReports.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.subOperations), New DevExpress.XtraBars.LinkPersistInfo(Me.subCalendars), New DevExpress.XtraBars.LinkPersistInfo(Me.subMarketing, True), New DevExpress.XtraBars.LinkPersistInfo(Me.subMaterials), New DevExpress.XtraBars.LinkPersistInfo(Me.subDirectLabourandSalespeople), New DevExpress.XtraBars.LinkPersistInfo(Me.subProfit)})
        Me.subReports.Name = "subReports"
        '
        'subOperations
        '
        Me.subOperations.Caption = "&Operations"
        Me.subOperations.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subOperations.Id = 27
        Me.subOperations.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.subQuotesOutstandingReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnOrderingIncompleteReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnAppliancesNotReceivedReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnUnfinishedJobsReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnDebtorsReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnJobsFinishedOnTimeReport, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnJobIssuesReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnPredictedSalesReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnQuoteRequestsCancelledReport, True), New DevExpress.XtraBars.LinkPersistInfo(Me.subQuoteRequestsCancelledList), New DevExpress.XtraBars.LinkPersistInfo(Me.subFollowUpReport, True)})
        Me.subOperations.Name = "subOperations"
        '
        'subQuotesOutstandingReport
        '
        Me.subQuotesOutstandingReport.Caption = "Quotes Outstanding Report"
        Me.subQuotesOutstandingReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.subQuotesOutstandingReport.Id = 11
        Me.subQuotesOutstandingReport.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnQuotesOutstandingReportNone)})
        Me.subQuotesOutstandingReport.Name = "subQuotesOutstandingReport"
        '
        'btnQuotesOutstandingReportNone
        '
        Me.btnQuotesOutstandingReportNone.Caption = "--- Salesperson not specified ---"
        Me.btnQuotesOutstandingReportNone.CategoryGuid = New System.Guid("0062ba9e-f3b2-483b-b446-94cf4d70d00b")
        Me.btnQuotesOutstandingReportNone.Id = 12
        Me.btnQuotesOutstandingReportNone.Name = "btnQuotesOutstandingReportNone"


        '
        'btnDirectSalesReportNone
        '
        Me.btnDirectSalesReportNone.Caption = "--- Direct labor not sepcified ---"
        Me.btnDirectSalesReportNone.CategoryGuid = New System.Guid("0062ba9e-f3b2-483b-b446-94cf4d70d00b")
        Me.btnDirectSalesReportNone.Id = 12
        Me.btnDirectSalesReportNone.Name = "btnDirectSalesReportNone"




        '
        'btnFollowupReportNone
        '
        Me.btnFollowupReportNone.Caption = "--- Follow up not sepcified ---"
        Me.btnFollowupReportNone.CategoryGuid = New System.Guid("0062ba9e-f3b2-483b-b446-94cf4d70d00b")
        Me.btnFollowupReportNone.Id = 12
        Me.btnFollowupReportNone.Name = "btnFollowupReportNone"



        '
        'btnOrderingIncompleteReport
        '
        Me.btnOrderingIncompleteReport.Caption = "Ordering Incomplete Report"
        Me.btnOrderingIncompleteReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnOrderingIncompleteReport.Id = 29
        Me.btnOrderingIncompleteReport.Name = "btnOrderingIncompleteReport"
        '
        'btnAppliancesNotReceivedReport
        '
        Me.btnAppliancesNotReceivedReport.Caption = "Appliances Not Received Report"
        Me.btnAppliancesNotReceivedReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnAppliancesNotReceivedReport.Id = 114
        Me.btnAppliancesNotReceivedReport.Name = "btnAppliancesNotReceivedReport"
        '
        'btnUnfinishedJobsReport
        '
        Me.btnUnfinishedJobsReport.Caption = "Unfinished Jobs Report"
        Me.btnUnfinishedJobsReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnUnfinishedJobsReport.Id = 30
        Me.btnUnfinishedJobsReport.Name = "btnUnfinishedJobsReport"
        '
        'btnDebtorsReport
        '
        Me.btnDebtorsReport.Caption = "Accounts Receivable Report"
        Me.btnDebtorsReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnDebtorsReport.Id = 22
        Me.btnDebtorsReport.Name = "btnDebtorsReport"
        '
        'btnJobsFinishedOnTimeReport
        '
        Me.btnJobsFinishedOnTimeReport.Caption = "Jobs Finished On Time Report"
        Me.btnJobsFinishedOnTimeReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnJobsFinishedOnTimeReport.Id = 32
        Me.btnJobsFinishedOnTimeReport.Name = "btnJobsFinishedOnTimeReport"
        '
        'btnJobIssuesReport
        '
        Me.btnJobIssuesReport.Caption = "Job Issues Report"
        Me.btnJobIssuesReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnJobIssuesReport.Id = 33
        Me.btnJobIssuesReport.Name = "btnJobIssuesReport"
        '
        'btnPredictedSalesReport
        '
        Me.btnPredictedSalesReport.Caption = "Predicted Sales Report"
        Me.btnPredictedSalesReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnPredictedSalesReport.Id = 34
        Me.btnPredictedSalesReport.Name = "btnPredictedSalesReport"
        '
        'btnQuoteRequestsCancelledReport
        '
        Me.btnQuoteRequestsCancelledReport.Caption = "Quote Requests Cancelled Report"
        Me.btnQuoteRequestsCancelledReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnQuoteRequestsCancelledReport.Id = 60
        Me.btnQuoteRequestsCancelledReport.Name = "btnQuoteRequestsCancelledReport"
        '
        'subQuoteRequestsCancelledList
        '
        Me.subQuoteRequestsCancelledList.Caption = "Quote Requests Cancelled List"
        Me.subQuoteRequestsCancelledList.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.subQuoteRequestsCancelledList.Id = 64
        Me.subQuoteRequestsCancelledList.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnQuoteRequestsCancelledListNone)})
        Me.subQuoteRequestsCancelledList.Name = "subQuoteRequestsCancelledList"
        '
        'btnQuoteRequestsCancelledListNone
        '
        Me.btnQuoteRequestsCancelledListNone.Caption = "--- Salesperson not specified ---"
        Me.btnQuoteRequestsCancelledListNone.CategoryGuid = New System.Guid("0062ba9e-f3b2-483b-b446-94cf4d70d00b")
        Me.btnQuoteRequestsCancelledListNone.Id = 13
        Me.btnQuoteRequestsCancelledListNone.Name = "btnQuoteRequestsCancelledListNone"
        '
        'subFollowUpReport
        '
        Me.subFollowUpReport.Caption = "Follow Up Report"
        Me.subFollowUpReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.subFollowUpReport.Id = 89
        Me.subFollowUpReport.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnFollowupReportNone)})
        Me.subFollowUpReport.Name = "subFollowUpReport"
        '
        'subCalendars
        '
        Me.subCalendars.Caption = "Cale&ndars"
        Me.subCalendars.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subCalendars.Id = 35
        Me.subCalendars.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.subSalespersonsCalendars), New DevExpress.XtraBars.LinkPersistInfo(Me.subJobBoardbyDirectLabour), New DevExpress.XtraBars.LinkPersistInfo(Me.subJobBoardByTask)})
        Me.subCalendars.Name = "subCalendars"
        '
        'subSalespersonsCalendars
        '
        Me.subSalespersonsCalendars.Caption = "Salesperson Calendars"
        Me.subSalespersonsCalendars.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.subSalespersonsCalendars.Id = 1
        Me.subSalespersonsCalendars.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnSalespersonsCalendarsAll)})
        Me.subSalespersonsCalendars.Name = "subSalespersonsCalendars"
        '
        'btnSalespersonsCalendarsAll
        '
        Me.btnSalespersonsCalendarsAll.Caption = "--- All ---"
        Me.btnSalespersonsCalendarsAll.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnSalespersonsCalendarsAll.Id = 116
        Me.btnSalespersonsCalendarsAll.Name = "btnSalespersonsCalendarsAll"
        '
        'subJobBoardbyDirectLabour
        '
        Me.subJobBoardbyDirectLabour.Caption = "Job Board by Direct Labor"
        Me.subJobBoardbyDirectLabour.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.subJobBoardbyDirectLabour.Id = 2
        Me.subJobBoardbyDirectLabour.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnJobBoardbyDirectLabourAll)})
        Me.subJobBoardbyDirectLabour.Name = "subJobBoardbyDirectLabour"
        '
        'btnJobBoardbyDirectLabourAll
        '
        Me.btnJobBoardbyDirectLabourAll.Caption = "--- All ---"
        Me.btnJobBoardbyDirectLabourAll.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnJobBoardbyDirectLabourAll.Id = 117
        Me.btnJobBoardbyDirectLabourAll.Name = "btnJobBoardbyDirectLabourAll"
        '
        'subJobBoardByTask
        '
        Me.subJobBoardByTask.Caption = "Job Board by Task"
        Me.subJobBoardByTask.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.subJobBoardByTask.Id = 87
        Me.subJobBoardByTask.Name = "subJobBoardByTask"
        '
        'subMarketing
        '
        Me.subMarketing.Caption = "Ma&rketing"
        Me.subMarketing.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subMarketing.Id = 9
        Me.subMarketing.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnPrimaryEnquirySourcesReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnSecondaryEnquirySourcesReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnPostCodesReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnPrimaryEnquirySourcesPostCodesReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnJobTypesReport)})
        Me.subMarketing.Name = "subMarketing"
        '
        'btnPrimaryEnquirySourcesReport
        '
        Me.btnPrimaryEnquirySourcesReport.Caption = "Primary Enquiry Sources Report"
        Me.btnPrimaryEnquirySourcesReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnPrimaryEnquirySourcesReport.Id = 54
        Me.btnPrimaryEnquirySourcesReport.Name = "btnPrimaryEnquirySourcesReport"
        '
        'btnSecondaryEnquirySourcesReport
        '
        Me.btnSecondaryEnquirySourcesReport.Caption = "Secondary Enquiry Sources Report"
        Me.btnSecondaryEnquirySourcesReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnSecondaryEnquirySourcesReport.Id = 55
        Me.btnSecondaryEnquirySourcesReport.Name = "btnSecondaryEnquirySourcesReport"
        '
        'btnPostCodesReport
        '
        Me.btnPostCodesReport.Caption = "ZIP/Postal Codes Report"
        Me.btnPostCodesReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnPostCodesReport.Id = 56
        Me.btnPostCodesReport.Name = "btnPostCodesReport"
        '
        'btnPrimaryEnquirySourcesPostCodesReport
        '
        Me.btnPrimaryEnquirySourcesPostCodesReport.Caption = "Primary Enquiry Sources - ZIP/Postal Codes Report"
        Me.btnPrimaryEnquirySourcesPostCodesReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnPrimaryEnquirySourcesPostCodesReport.Id = 110
        Me.btnPrimaryEnquirySourcesPostCodesReport.Name = "btnPrimaryEnquirySourcesPostCodesReport"
        '
        'btnJobTypesReport
        '
        Me.btnJobTypesReport.Caption = "Job Types Report"
        Me.btnJobTypesReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnJobTypesReport.Id = 115
        Me.btnJobTypesReport.Name = "btnJobTypesReport"
        '
        'subMaterials
        '
        Me.subMaterials.Caption = "&Materials"
        Me.subMaterials.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subMaterials.Id = 41
        Me.subMaterials.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnStockReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnWasteReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnStockRequirementsReport), New DevExpress.XtraBars.LinkPersistInfo(Me.subTrendProductInJobReport)})
        Me.subMaterials.Name = "subMaterials"
        '
        'btnStockReport
        '
        Me.btnStockReport.Caption = "Inventory Report"
        Me.btnStockReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnStockReport.Id = 42
        Me.btnStockReport.Name = "btnStockReport"
        '
        'btnWasteReport
        '
        Me.btnWasteReport.Caption = "Waste Report"
        Me.btnWasteReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnWasteReport.Id = 43
        Me.btnWasteReport.Name = "btnWasteReport"
        '
        'btnStockRequirementsReport
        '
        Me.btnStockRequirementsReport.Caption = "Tracked Material Requirements Report"
        Me.btnStockRequirementsReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnStockRequirementsReport.Id = 44
        Me.btnStockRequirementsReport.Name = "btnStockRequirementsReport"
        '
        'subTrendProductInJobReport
        '
        Me.subTrendProductInJobReport.Caption = "Trend Product in Jobs Report"
        Me.subTrendProductInJobReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.subTrendProductInJobReport.Id = 86
        Me.subTrendProductInJobReport.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnDirectSalesReportNone)})
        Me.subTrendProductInJobReport.Name = "subTrendProductInJobReport"
        '
        'subDirectLabourandSalespeople
        '
        Me.subDirectLabourandSalespeople.Caption = "&Direct Labor and Salespeople"
        Me.subDirectLabourandSalespeople.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subDirectLabourandSalespeople.Id = 45
        Me.subDirectLabourandSalespeople.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnDirectLabourandSalespeopleSpecifiedinJobs), New DevExpress.XtraBars.LinkPersistInfo(Me.subDirectLabourJobList), New DevExpress.XtraBars.LinkPersistInfo(Me.subSalespersonJobList), New DevExpress.XtraBars.LinkPersistInfo(Me.btnHoursCostAndVolumeReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnSalespeopleQuoteStatistics, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnSalespeopleJobStatistics), New DevExpress.XtraBars.LinkPersistInfo(Me.subRepJobs), New DevExpress.XtraBars.LinkPersistInfo(Me.subSalespersonAmountOwingReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnMeasurersReport, True)})
        Me.subDirectLabourandSalespeople.Name = "subDirectLabourandSalespeople"

        '
        'btnDirectLabourandSalespeopleSpecifiedinJobs
        '
        Me.btnDirectLabourandSalespeopleSpecifiedinJobs.Caption = "Direct Labor and Salespeople Job Costings"
        Me.btnDirectLabourandSalespeopleSpecifiedinJobs.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnDirectLabourandSalespeopleSpecifiedinJobs.Id = 46
        Me.btnDirectLabourandSalespeopleSpecifiedinJobs.Name = "btnDirectLabourandSalespeopleSpecifiedinJobs"
        '
        'subDirectLabourJobList
        '
        Me.subDirectLabourJobList.Caption = "Direct Labor Job Costings List"
        Me.subDirectLabourJobList.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.subDirectLabourJobList.Id = 3
        Me.subDirectLabourJobList.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnDirectSalesReportNone)})
        Me.subDirectLabourJobList.Name = "subDirectLabourJobList"
        '
        'subSalespersonJobList
        '
        Me.subSalespersonJobList.Caption = "Salesperson Job Costings List"
        Me.subSalespersonJobList.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.subSalespersonJobList.Id = 36
        Me.subSalespersonJobList.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnQuotesOutstandingReportNone)})
        Me.subSalespersonJobList.Name = "subSalespersonJobList"
        '
        'btnHoursCostAndVolumeReport
        '
        Me.btnHoursCostAndVolumeReport.Caption = "Hours, Cost and Volume Report"
        Me.btnHoursCostAndVolumeReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnHoursCostAndVolumeReport.Id = 90
        Me.btnHoursCostAndVolumeReport.Name = "btnHoursCostAndVolumeReport"
        '
        'btnSalespeopleQuoteStatistics
        '
        Me.btnSalespeopleQuoteStatistics.Caption = "Salespeople Quote Statistics"
        Me.btnSalespeopleQuoteStatistics.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnSalespeopleQuoteStatistics.Id = 49
        Me.btnSalespeopleQuoteStatistics.Name = "btnSalespeopleQuoteStatistics"
        '
        'btnSalespeopleJobStatistics
        '
        Me.btnSalespeopleJobStatistics.Caption = "Salespeople Job Statistics"
        Me.btnSalespeopleJobStatistics.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnSalespeopleJobStatistics.Id = 50
        Me.btnSalespeopleJobStatistics.Name = "btnSalespeopleJobStatistics"
        '
        'subRepJobs
        '
        Me.subRepJobs.Caption = "Salesperson Job List"
        Me.subRepJobs.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.subRepJobs.Id = 112
        Me.subRepJobs.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnQuotesOutstandingReportNone)})
        Me.subRepJobs.Name = "subRepJobs"
        '
        'subSalespersonAmountOwingReport
        '
        Me.subSalespersonAmountOwingReport.Caption = "Salesperson Amount Owing Report"
        Me.subSalespersonAmountOwingReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.subSalespersonAmountOwingReport.Id = 37
        Me.subSalespersonAmountOwingReport.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnQuotesOutstandingReportNone)})
        Me.subSalespersonAmountOwingReport.Name = "subSalespersonAmountOwingReport"
        '
        'btnMeasurersReport
        '
        Me.btnMeasurersReport.Caption = "Templaters / Measurers Report"
        Me.btnMeasurersReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnMeasurersReport.Id = 8
        Me.btnMeasurersReport.Name = "btnMeasurersReport"
        '
        'subProfit
        '
        Me.subProfit.Caption = "&Profit"
        Me.subProfit.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subProfit.Id = 38
        Me.subProfit.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnAllJobsReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnProfitReport)})
        Me.subProfit.Name = "subProfit"
        '
        'btnAllJobsReport
        '
        Me.btnAllJobsReport.Caption = "All Jobs Report"
        Me.btnAllJobsReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnAllJobsReport.Id = 39
        Me.btnAllJobsReport.Name = "btnAllJobsReport"
        '
        'btnProfitReport
        '
        Me.btnProfitReport.Caption = "Profit Report"
        Me.btnProfitReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnProfitReport.Id = 40
        Me.btnProfitReport.Name = "btnProfitReport"
        '
        'subHOReports
        '
        Me.subHOReports.Caption = "&Group Reports"
        Me.subHOReports.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subHOReports.Id = 94
        Me.subHOReports.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.subHOOperations), New DevExpress.XtraBars.LinkPersistInfo(Me.subHOMarketing, True), New DevExpress.XtraBars.LinkPersistInfo(Me.subHOMaterials), New DevExpress.XtraBars.LinkPersistInfo(Me.subHODirectLabourandSalespeople), New DevExpress.XtraBars.LinkPersistInfo(Me.subHOProfit)})
        Me.subHOReports.Name = "subHOReports"
        '
        'subHOOperations
        '
        Me.subHOOperations.Caption = "&Operations"
        Me.subHOOperations.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subHOOperations.Id = 95
        Me.subHOOperations.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnHOJobsFinishedOnTimeReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnHODebtorStatisticsReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnHOJobIssuesReport), New DevExpress.XtraBars.LinkPersistInfo(Me.btnHOQuoteStatisticsReport, True)})
        Me.subHOOperations.Name = "subHOOperations"
        '
        'btnHOJobsFinishedOnTimeReport
        '
        Me.btnHOJobsFinishedOnTimeReport.Caption = "Jobs Finished On Time Report"
        Me.btnHOJobsFinishedOnTimeReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnHOJobsFinishedOnTimeReport.Id = 93
        Me.btnHOJobsFinishedOnTimeReport.Name = "btnHOJobsFinishedOnTimeReport"
        '
        'btnHODebtorStatisticsReport
        '
        Me.btnHODebtorStatisticsReport.Caption = "Accounts Receivable Statistics Report"
        Me.btnHODebtorStatisticsReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnHODebtorStatisticsReport.Id = 107
        Me.btnHODebtorStatisticsReport.Name = "btnHODebtorStatisticsReport"
        '
        'btnHOJobIssuesReport
        '
        Me.btnHOJobIssuesReport.Caption = "Job Issues Report"
        Me.btnHOJobIssuesReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnHOJobIssuesReport.Id = 108
        Me.btnHOJobIssuesReport.Name = "btnHOJobIssuesReport"
        '
        'btnHOQuoteStatisticsReport
        '
        Me.btnHOQuoteStatisticsReport.Caption = "Quote Statistics Report"
        Me.btnHOQuoteStatisticsReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnHOQuoteStatisticsReport.Id = 113
        Me.btnHOQuoteStatisticsReport.Name = "btnHOQuoteStatisticsReport"
        '
        'subHOMarketing
        '
        Me.subHOMarketing.Caption = "Ma&rketing"
        Me.subHOMarketing.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subHOMarketing.Id = 96
        Me.subHOMarketing.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnHOPrimaryEnquirySourcesReport)})
        Me.subHOMarketing.Name = "subHOMarketing"
        '
        'btnHOPrimaryEnquirySourcesReport
        '
        Me.btnHOPrimaryEnquirySourcesReport.Caption = "Primary Enquiry Sources Report"
        Me.btnHOPrimaryEnquirySourcesReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnHOPrimaryEnquirySourcesReport.Id = 97
        Me.btnHOPrimaryEnquirySourcesReport.Name = "btnHOPrimaryEnquirySourcesReport"
        '
        'subHOMaterials
        '
        Me.subHOMaterials.Caption = "&Materials"
        Me.subHOMaterials.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subHOMaterials.Id = 98
        Me.subHOMaterials.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnHOWasteReport)})
        Me.subHOMaterials.Name = "subHOMaterials"
        '
        'btnHOWasteReport
        '
        Me.btnHOWasteReport.Caption = "Waste Report"
        Me.btnHOWasteReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnHOWasteReport.Id = 100
        Me.btnHOWasteReport.Name = "btnHOWasteReport"
        '
        'subHODirectLabourandSalespeople
        '
        Me.subHODirectLabourandSalespeople.Caption = "&Direct Labor and Salespeople"
        Me.subHODirectLabourandSalespeople.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subHODirectLabourandSalespeople.Id = 102
        Me.subHODirectLabourandSalespeople.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnHoursCostsAndVolumeReport)})
        Me.subHODirectLabourandSalespeople.Name = "subHODirectLabourandSalespeople"
        '
        'btnHoursCostsAndVolumeReport
        '
        Me.btnHoursCostsAndVolumeReport.Caption = "Hours, Cost and Volume Report"
        Me.btnHoursCostsAndVolumeReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnHoursCostsAndVolumeReport.Id = 103
        Me.btnHoursCostsAndVolumeReport.Name = "btnHoursCostsAndVolumeReport"
        '
        'subHOProfit
        '
        Me.subHOProfit.Caption = "&Profit"
        Me.subHOProfit.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subHOProfit.Id = 104
        Me.subHOProfit.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnHOAllJobsReport)})
        Me.subHOProfit.Name = "subHOProfit"
        '
        'btnHOAllJobsReport
        '
        Me.btnHOAllJobsReport.Caption = "All Jobs Report"
        Me.btnHOAllJobsReport.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.btnHOAllJobsReport.Id = 105
        Me.btnHOAllJobsReport.Name = "btnHOAllJobsReport"
        '
        'subForms
        '
        Me.subForms.Caption = "For&ms"
        Me.subForms.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subForms.Id = 75
        Me.subForms.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.subStaticForms), New DevExpress.XtraBars.LinkPersistInfo(Me.subManualForms)})
        Me.subForms.Name = "subForms"
        '
        'subStaticForms
        '
        Me.subStaticForms.Caption = "Organisational Forms"
        Me.subStaticForms.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subStaticForms.Id = 51
        Me.subStaticForms.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.subExampleOrganisationalForms), New DevExpress.XtraBars.LinkPersistInfo(Me.subSalespersonForms), New DevExpress.XtraBars.LinkPersistInfo(Me.subTemplaterMeasurerForms), New DevExpress.XtraBars.LinkPersistInfo(Me.btnJobIssuesForm), New DevExpress.XtraBars.LinkPersistInfo(Me.btnNonStockedMaterialInJobForm), New DevExpress.XtraBars.LinkPersistInfo(Me.btnCareOfGranite)})
        Me.subStaticForms.Name = "subStaticForms"
        '
        'subExampleOrganisationalForms
        '
        Me.subExampleOrganisationalForms.Caption = "Example Organisational Forms"
        Me.subExampleOrganisationalForms.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subExampleOrganisationalForms.Id = 69
        Me.subExampleOrganisationalForms.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnJobFolderCoverStamp), New DevExpress.XtraBars.LinkPersistInfo(Me.btnInstallerStatusReport)})
        Me.subExampleOrganisationalForms.Name = "subExampleOrganisationalForms"
        '
        'btnJobFolderCoverStamp
        '
        Me.btnJobFolderCoverStamp.Caption = "Example Job Folder Cover Stamp"
        Me.btnJobFolderCoverStamp.CategoryGuid = New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")
        Me.btnJobFolderCoverStamp.Id = 14
        Me.btnJobFolderCoverStamp.Name = "btnJobFolderCoverStamp"
        '
        'btnInstallerStatusReport
        '
        Me.btnInstallerStatusReport.Caption = "Example Installer Status Report"
        Me.btnInstallerStatusReport.CategoryGuid = New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")
        Me.btnInstallerStatusReport.Id = 83
        Me.btnInstallerStatusReport.Name = "btnInstallerStatusReport"
        '
        'subSalespersonForms
        '
        Me.subSalespersonForms.Caption = "Salesperson Forms"
        Me.subSalespersonForms.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subSalespersonForms.Id = 76
        Me.subSalespersonForms.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnQuoteRequestCancellationForm)})
        Me.subSalespersonForms.Name = "subSalespersonForms"
        '
        'btnQuoteRequestCancellationForm
        '
        Me.btnQuoteRequestCancellationForm.Caption = "Quote Request Cancellation Form"
        Me.btnQuoteRequestCancellationForm.CategoryGuid = New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")
        Me.btnQuoteRequestCancellationForm.Id = 70
        Me.btnQuoteRequestCancellationForm.Name = "btnQuoteRequestCancellationForm"
        '
        'subTemplaterMeasurerForms
        '
        Me.subTemplaterMeasurerForms.Caption = "Templater / Measurer Forms"
        Me.subTemplaterMeasurerForms.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subTemplaterMeasurerForms.Id = 72
        Me.subTemplaterMeasurerForms.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnClientConfirmationForm), New DevExpress.XtraBars.LinkPersistInfo(Me.btnChangesToJobDetailsForm)})
        Me.subTemplaterMeasurerForms.Name = "subTemplaterMeasurerForms"
        '
        'btnClientConfirmationForm
        '
        Me.btnClientConfirmationForm.Caption = "Customer Confirmation Form"
        Me.btnClientConfirmationForm.CategoryGuid = New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")
        Me.btnClientConfirmationForm.Id = 73
        Me.btnClientConfirmationForm.Name = "btnClientConfirmationForm"
        '
        'btnChangesToJobDetailsForm
        '
        Me.btnChangesToJobDetailsForm.Caption = "Changes To Job Details Form"
        Me.btnChangesToJobDetailsForm.CategoryGuid = New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")
        Me.btnChangesToJobDetailsForm.Id = 74
        Me.btnChangesToJobDetailsForm.Name = "btnChangesToJobDetailsForm"
        '
        'btnJobIssuesForm
        '
        Me.btnJobIssuesForm.Caption = "Job Issues Form"
        Me.btnJobIssuesForm.CategoryGuid = New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")
        Me.btnJobIssuesForm.Id = 52
        Me.btnJobIssuesForm.Name = "btnJobIssuesForm"
        '
        'btnNonStockedMaterialInJobForm
        '
        Me.btnNonStockedMaterialInJobForm.Caption = "Non-Tracked Material in Job Form"
        Me.btnNonStockedMaterialInJobForm.CategoryGuid = New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")
        Me.btnNonStockedMaterialInJobForm.Id = 53
        Me.btnNonStockedMaterialInJobForm.Name = "btnNonStockedMaterialInJobForm"
        '
        'btnCareOfGranite
        '
        Me.btnCareOfGranite.Caption = "Care of Benchtop Letter"
        Me.btnCareOfGranite.CategoryGuid = New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")
        Me.btnCareOfGranite.Id = 77
        Me.btnCareOfGranite.Name = "btnCareOfGranite"
        '
        'subManualForms
        '
        Me.subManualForms.Caption = "Manual Forms"
        Me.subManualForms.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subManualForms.Id = 57
        Me.subManualForms.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnManualQuoteRequestForm), New DevExpress.XtraBars.LinkPersistInfo(Me.btnManualJobBookingForm)})
        Me.subManualForms.Name = "subManualForms"
        '
        'btnManualQuoteRequestForm
        '
        Me.btnManualQuoteRequestForm.Caption = "Manual Quote Request Form"
        Me.btnManualQuoteRequestForm.CategoryGuid = New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")
        Me.btnManualQuoteRequestForm.Id = 58
        Me.btnManualQuoteRequestForm.Name = "btnManualQuoteRequestForm"
        '
        'btnManualJobBookingForm
        '
        Me.btnManualJobBookingForm.Caption = "Manual Job Booking Form"
        Me.btnManualJobBookingForm.CategoryGuid = New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")
        Me.btnManualJobBookingForm.Id = 71
        Me.btnManualJobBookingForm.Name = "btnManualJobBookingForm"
        '
        'subOptions
        '
        Me.subOptions.Caption = "&Options"
        Me.subOptions.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subOptions.Id = 63
        Me.subOptions.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnBranchProperties, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnBranchUsers)})
        Me.subOptions.Name = "subOptions"
        '
        'btnBranchProperties
        '
        Me.btnBranchProperties.Caption = "&Branch Properties..."
        Me.btnBranchProperties.CategoryGuid = New System.Guid("b4347cfa-b71d-4f3d-abe2-213a7e6e3fc6")
        Me.btnBranchProperties.Id = 65
        Me.btnBranchProperties.ImageIndex = 6
        Me.btnBranchProperties.ImageIndexDisabled = 6
        Me.btnBranchProperties.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.B))
        Me.btnBranchProperties.Name = "btnBranchProperties"
        '
        'btnBranchUsers
        '
        Me.btnBranchUsers.Caption = "Branch &Users..."
        Me.btnBranchUsers.CategoryGuid = New System.Guid("b4347cfa-b71d-4f3d-abe2-213a7e6e3fc6")
        Me.btnBranchUsers.Id = 66
        Me.btnBranchUsers.ImageIndex = 5
        Me.btnBranchUsers.ImageIndexDisabled = 5
        Me.btnBranchUsers.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.U))
        Me.btnBranchUsers.Name = "btnBranchUsers"
        '
        'subHelp
        '
        Me.subHelp.Caption = "&Help"
        Me.subHelp.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subHelp.Id = 67
        Me.subHelp.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnHelp), New DevExpress.XtraBars.LinkPersistInfo(Me.btnAboutFranchiseManager, True)})
        Me.subHelp.Name = "subHelp"
        '
        'btnHelp
        '
        Me.btnHelp.Caption = "Franchise Manager &Help..."
        Me.btnHelp.CategoryGuid = New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")
        Me.btnHelp.Id = 59
        Me.btnHelp.ImageIndex = 4
        Me.btnHelp.ImageIndexDisabled = 4
        Me.btnHelp.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F1))
        Me.btnHelp.Name = "btnHelp"
        '
        'btnAboutFranchiseManager
        '
        Me.btnAboutFranchiseManager.Caption = "&About Franchise Manager..."
        Me.btnAboutFranchiseManager.CategoryGuid = New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")
        Me.btnAboutFranchiseManager.Id = 15
        Me.btnAboutFranchiseManager.Name = "btnAboutFranchiseManager"
        '
        'bStatus
        '
        Me.bStatus.BarName = "Status Bar"
        Me.bStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
        Me.bStatus.DockCol = 0
        Me.bStatus.DockRow = 0
        Me.bStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.bStatus.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.bsiNumItems), New DevExpress.XtraBars.LinkPersistInfo(Me.bsiVisibleItems)})
        Me.bStatus.OptionsBar.AllowQuickCustomization = False
        Me.bStatus.OptionsBar.DisableClose = True
        Me.bStatus.OptionsBar.DisableCustomization = True
        Me.bStatus.OptionsBar.DrawDragBorder = False
        Me.bStatus.OptionsBar.DrawSizeGrip = True
        Me.bStatus.OptionsBar.UseWholeRow = True
        Me.bStatus.Text = "Status Bar"
        '
        'bsiNumItems
        '
        Me.bsiNumItems.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None
        Me.bsiNumItems.Border = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.bsiNumItems.Caption = "0 Items"
        Me.bsiNumItems.CategoryGuid = New System.Guid("0062ba9e-f3b2-483b-b446-94cf4d70d00b")
        Me.bsiNumItems.Id = 84
        Me.bsiNumItems.Name = "bsiNumItems"
        Me.bsiNumItems.TextAlignment = System.Drawing.StringAlignment.Near
        Me.bsiNumItems.Width = 100
        '
        'bsiVisibleItems
        '
        Me.bsiVisibleItems.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring
        Me.bsiVisibleItems.Border = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.bsiVisibleItems.Caption = "0 Visible Items"
        Me.bsiVisibleItems.CategoryGuid = New System.Guid("0062ba9e-f3b2-483b-b446-94cf4d70d00b")
        Me.bsiVisibleItems.Id = 85
        Me.bsiVisibleItems.Name = "bsiVisibleItems"
        Me.bsiVisibleItems.TextAlignment = System.Drawing.StringAlignment.Near
        Me.bsiVisibleItems.Width = 32
        '
        'BarAndDockingController1
        '
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseForeColor = True
        Me.BarAndDockingController1.PaintStyleName = "OfficeXP"
        '
        'DockManager1
        '
        Me.DockManager1.AutoHideContainers.AddRange(New DevExpress.XtraBars.Docking.AutoHideContainer() {Me.hideContainerRight})
        Me.DockManager1.Controller = Me.BarAndDockingController1
        Me.DockManager1.Form = Me
        Me.DockManager1.Images = Me.ImageListPanels
        Me.DockManager1.RootPanels.AddRange(New DevExpress.XtraBars.Docking.DockPanel() {Me.dpNavigation})
        Me.DockManager1.TopZIndexControls.AddRange(New String() {"DevExpress.XtraBars.BarDockControl", "System.Windows.Forms.StatusBar"})
        '
        'hideContainerRight
        '
        Me.hideContainerRight.Controls.Add(Me.dpHelp)
        Me.hideContainerRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.hideContainerRight.Location = New System.Drawing.Point(942, 20)
        Me.hideContainerRight.Name = "hideContainerRight"
        Me.hideContainerRight.Size = New System.Drawing.Size(22, 516)
        '
        'dpHelp
        '
        Me.dpHelp.Controls.Add(Me.DockPanel2_Container)
        Me.dpHelp.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right
        Me.dpHelp.ID = New System.Guid("43b6dea4-d4b1-4be5-9813-36f727e3ff9f")
        Me.dpHelp.ImageIndex = 0
        Me.dpHelp.Location = New System.Drawing.Point(0, 0)
        Me.dpHelp.Name = "dpHelp"
        Me.dpHelp.Options.ShowCloseButton = False
        Me.dpHelp.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Right
        Me.dpHelp.SavedIndex = 1
        Me.dpHelp.Size = New System.Drawing.Size(265, 493)
        Me.dpHelp.Text = "Help"
        Me.dpHelp.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide
        '
        'DockPanel2_Container
        '
        Me.DockPanel2_Container.Controls.Add(Me.pnlHelp)
        Me.DockPanel2_Container.Location = New System.Drawing.Point(4, 21)
        Me.DockPanel2_Container.Name = "DockPanel2_Container"
        Me.DockPanel2_Container.Size = New System.Drawing.Size(257, 468)
        Me.DockPanel2_Container.TabIndex = 0
        '
        'pnlHelp
        '
        Me.pnlHelp.Appearance.BackColor = System.Drawing.SystemColors.Window
        Me.pnlHelp.Appearance.Options.UseBackColor = True
        Me.pnlHelp.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.pnlHelp.Controls.Add(Me.rtbHelp)
        Me.pnlHelp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlHelp.DockPadding.Bottom = 4
        Me.pnlHelp.DockPadding.Left = 4
        Me.pnlHelp.DockPadding.Right = 4
        Me.pnlHelp.Location = New System.Drawing.Point(0, 0)
        Me.pnlHelp.Name = "pnlHelp"
        Me.pnlHelp.Size = New System.Drawing.Size(257, 468)
        Me.pnlHelp.TabIndex = 0
        Me.pnlHelp.Text = "PanelControl1"
        '
        'rtbHelp
        '
        Me.rtbHelp.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.rtbHelp.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.rtbHelp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rtbHelp.Location = New System.Drawing.Point(6, 2)
        Me.rtbHelp.Name = "rtbHelp"
        Me.rtbHelp.ReadOnly = True
        Me.rtbHelp.Size = New System.Drawing.Size(245, 460)
        Me.rtbHelp.TabIndex = 0
        Me.rtbHelp.Text = "RichTextBox1"
        '
        'ImageListPanels
        '
        Me.ImageListPanels.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageListPanels.ImageStream = CType(resources.GetObject("ImageListPanels.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListPanels.TransparentColor = System.Drawing.Color.Transparent
        '
        'dpNavigation
        '
        Me.dpNavigation.Controls.Add(Me.DockPanel1_Container)
        Me.dpNavigation.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left
        Me.dpNavigation.FloatSize = New System.Drawing.Size(284, 567)
        Me.dpNavigation.ID = New System.Guid("6dfd1974-63df-40f4-8659-d5cc04a2f00c")
        Me.dpNavigation.Location = New System.Drawing.Point(0, 20)
        Me.dpNavigation.Name = "dpNavigation"
        Me.dpNavigation.Options.ShowCloseButton = False
        Me.dpNavigation.Size = New System.Drawing.Size(257, 516)
        Me.dpNavigation.Text = "Navigation Panel"
        '
        'DockPanel1_Container
        '
        Me.DockPanel1_Container.Controls.Add(Me.tvwMenu)
        Me.DockPanel1_Container.Location = New System.Drawing.Point(4, 21)
        Me.DockPanel1_Container.Name = "DockPanel1_Container"
        Me.DockPanel1_Container.Size = New System.Drawing.Size(249, 491)
        Me.DockPanel1_Container.TabIndex = 0
        '
        'tvwMenu
        '
        Me.tvwMenu.BackColor = System.Drawing.Color.White
        Me.tvwMenu.Connection = Me.SqlConnection
        Me.tvwMenu.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvwMenu.HideSelection = False
        Me.tvwMenu.ImageList = Me.ImageListSmall
        Me.tvwMenu.Location = New System.Drawing.Point(0, 0)
        Me.tvwMenu.Name = "tvwMenu"
        Me.tvwMenu.Size = New System.Drawing.Size(249, 491)
        Me.tvwMenu.TabIndex = 9
        '
        'btnShowHelp
        '
        Me.btnShowHelp.Caption = "&Help"
        Me.btnShowHelp.CategoryGuid = New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")
        Me.btnShowHelp.Id = 68
        Me.btnShowHelp.ImageIndex = 4
        Me.btnShowHelp.ImageIndexDisabled = 4
        Me.btnShowHelp.Name = "btnShowHelp"
        '
        'BarSubItem1
        '
        Me.BarSubItem1.Caption = "Calendars"
        Me.BarSubItem1.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.BarSubItem1.Id = 24
        Me.BarSubItem1.Name = "BarSubItem1"
        '
        'BarToolbarsListItem
        '
        Me.BarToolbarsListItem.Caption = "Toolbar List"
        Me.BarToolbarsListItem.CategoryGuid = New System.Guid("80d922eb-0420-4701-a571-d1e304fb1b67")
        Me.BarToolbarsListItem.Id = 31
        Me.BarToolbarsListItem.Name = "BarToolbarsListItem"
        '
        'btnQuickPrint
        '
        Me.btnQuickPrint.Caption = "Print"
        Me.btnQuickPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnQuickPrint.Id = 7
        Me.btnQuickPrint.ImageIndex = 11
        Me.btnQuickPrint.ImageIndexDisabled = 11
        Me.btnQuickPrint.Name = "btnQuickPrint"
        '
        'subInstallerForms
        '
        Me.subInstallerForms.Caption = "Installer Forms"
        Me.subInstallerForms.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subInstallerForms.Id = 82
        Me.subInstallerForms.Name = "subInstallerForms"
        '
        'DsGTMS
        '
        Me.DsGTMS.DataSetName = "dsGTMS"
        Me.DsGTMS.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'daExpenses
        '
        Me.daExpenses.InsertCommand = Me.SqlInsertCommand1
        Me.daExpenses.SelectCommand = Me.SqlSelectCommand1
        Me.daExpenses.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VExpenses", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("EXName", "EXName"), New System.Data.Common.DataColumnMapping("EXType", "EXType"), New System.Data.Common.DataColumnMapping("EGID", "EGID"), New System.Data.Common.DataColumnMapping("EXDiscontinued", "EXDiscontinued"), New System.Data.Common.DataColumnMapping("EXPaymentMethod", "EXPaymentMethod"), New System.Data.Common.DataColumnMapping("EGType", "EGType"), New System.Data.Common.DataColumnMapping("EXAssignToPortion", "EXAssignToPortion"), New System.Data.Common.DataColumnMapping("PMShowInJobScreen", "PMShowInJobScreen"), New System.Data.Common.DataColumnMapping("EXAppearInCalendarRC", "EXAppearInCalendarRC"), New System.Data.Common.DataColumnMapping("EXAppearInCalendarDL", "EXAppearInCalendarDL"), New System.Data.Common.DataColumnMapping("EXAppearInSalesperson", "EXAppearInSalesperson")})})
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO VExpenses(BRID, EXName, EXType, EGID, EXDiscontinued, EXPaymentMethod" & _
        ", EGType, EXAssignToPortion, PMShowInJobScreen, EXAppearInCalendarRC, EXAppearIn" & _
        "CalendarDL, EXAppearInSalesperson) VALUES (@BRID, @EXName, @EXType, @EGID, @EXDi" & _
        "scontinued, @EXPaymentMethod, @EGType, @EXAssignToPortion, @PMShowInJobScreen, @" & _
        "EXAppearInCalendarRC, @EXAppearInCalendarDL, @EXAppearInSalesperson); SELECT BRI" & _
        "D, EXID, EXName, EXType, EGID, EXDiscontinued, EXPaymentMethod, EGType, EXAssign" & _
        "ToPortion, PMShowInJobScreen, EXAppearInCalendarRC, EXAppearInCalendarDL, EXAppe" & _
        "arInSalesperson FROM VExpenses"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXType", System.Data.SqlDbType.VarChar, 2, "EXType"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGID", System.Data.SqlDbType.Int, 4, "EGID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXDiscontinued", System.Data.SqlDbType.Bit, 1, "EXDiscontinued"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXPaymentMethod", System.Data.SqlDbType.VarChar, 2, "EXPaymentMethod"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EGType", System.Data.SqlDbType.VarChar, 2, "EGType"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAssignToPortion", System.Data.SqlDbType.VarChar, 2, "EXAssignToPortion"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PMShowInJobScreen", System.Data.SqlDbType.Bit, 1, "PMShowInJobScreen"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAppearInCalendarRC", System.Data.SqlDbType.Bit, 1, "EXAppearInCalendarRC"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAppearInCalendarDL", System.Data.SqlDbType.Bit, 1, "EXAppearInCalendarDL"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXAppearInSalesperson", System.Data.SqlDbType.Bit, 1, "EXAppearInSalesperson"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, EXID, EXName, EXType, EGID, EXDiscontinued, EXPaymentMethod, EGType," & _
        " EXAssignToPortion, PMShowInJobScreen, EXAppearInCalendarRC, EXAppearInCalendarD" & _
        "L, EXAppearInSalesperson FROM VExpenses WHERE (BRID = @BRID) AND (EXDiscontinued" & _
        " = 0)"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'dvSalesRepCostingGroup
        '
        Me.dvSalesRepCostingGroup.RowFilter = "EGType = 'RC'"
        Me.dvSalesRepCostingGroup.Sort = "EXName"
        Me.dvSalesRepCostingGroup.Table = Me.DsGTMS.VExpenses
        '
        'dvDirectLabourInCalendar
        '
        Me.dvDirectLabourInCalendar.RowFilter = "EXAppearInCalendarDL = 1"
        Me.dvDirectLabourInCalendar.Sort = "EXName"
        Me.dvDirectLabourInCalendar.Table = Me.DsGTMS.VExpenses
        '
        'ControlContainer1
        '
        Me.ControlContainer1.Location = New System.Drawing.Point(4, 21)
        Me.ControlContainer1.Name = "ControlContainer1"
        Me.ControlContainer1.Size = New System.Drawing.Size(192, 175)
        Me.ControlContainer1.TabIndex = 0
        '
        'DockPanel1
        '
        Me.DockPanel1.Controls.Add(Me.ControlContainer1)
        Me.DockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float
        Me.DockPanel1.FloatLocation = New System.Drawing.Point(339, 234)
        Me.DockPanel1.ID = New System.Guid("ea3cbac2-ea55-419e-82e7-9a829aa4f7d7")
        Me.DockPanel1.Location = New System.Drawing.Point(0, 0)
        Me.DockPanel1.Name = "DockPanel1"
        Me.DockPanel1.Text = "DockPanel1"
        '
        'dvSalesRepsInCalendar
        '
        Me.dvSalesRepsInCalendar.RowFilter = "EXAppearInCalendarRC = 1"
        Me.dvSalesRepsInCalendar.Sort = "EXName"
        Me.dvSalesRepsInCalendar.Table = Me.DsGTMS.VExpenses
        '
        'dvDirectLabourCostingGroup
        '
        Me.dvDirectLabourCostingGroup.RowFilter = "EGType = 'DL'"
        Me.dvDirectLabourCostingGroup.Sort = "EXName"
        Me.dvDirectLabourCostingGroup.Table = Me.DsGTMS.VExpenses
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT BRID, MTID, MTName, MTStocked FROM VMaterials WHERE (MTDiscontinued = 0) A" & _
        "ND (BRID = @BRID)"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection
        Me.SqlSelectCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO VMaterials(BRID, MTName, MTInventoryUOM, MTPrimaryUOM, MTDiscontinued" & _
        ", MTStocked, MGID, MTCurrentPurchaseCost, MTCurrentWasteFactorAllowance, MGContr" & _
        "olledAtHeadOffice, MTCurrentInventory, MTCurrentWasteFactor, MGAssignToPortion, " & _
        "MMTID, MTCurrentWastePercentage) VALUES (@BRID, @MTName, @MTInventoryUOM, @MTPri" & _
        "maryUOM, @MTDiscontinued, @MTStocked, @MGID, @MTCurrentPurchaseCost, @MTCurrentW" & _
        "asteFactorAllowance, @MGControlledAtHeadOffice, @MTCurrentInventory, @MTCurrentW" & _
        "asteFactor, @MGAssignToPortion, @MMTID, @MTCurrentWastePercentage); SELECT BRID," & _
        " MTID, MTName, MTInventoryUOM, MTPrimaryUOM, MTDiscontinued, MTStocked, MGID, MT" & _
        "CurrentPurchaseCost, MTCurrentWasteFactorAllowance, MGControlledAtHeadOffice, MT" & _
        "CurrentInventory, MTCurrentWasteFactor, MGAssignToPortion, MMTID, MTCurrentWaste" & _
        "Percentage FROM VMaterials"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTName", System.Data.SqlDbType.VarChar, 50, "MTName"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTInventoryUOM", System.Data.SqlDbType.VarChar, 2, "MTInventoryUOM"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTPrimaryUOM", System.Data.SqlDbType.VarChar, 2, "MTPrimaryUOM"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTDiscontinued", System.Data.SqlDbType.Bit, 1, "MTDiscontinued"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTStocked", System.Data.SqlDbType.Bit, 1, "MTStocked"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTCurrentPurchaseCost", System.Data.SqlDbType.Money, 8, "MTCurrentPurchaseCost"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTCurrentWasteFactorAllowance", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "MTCurrentWasteFactorAllowance", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGControlledAtHeadOffice", System.Data.SqlDbType.Bit, 1, "MGControlledAtHeadOffice"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTCurrentInventory", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "MTCurrentInventory", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTCurrentWasteFactor", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "MTCurrentWasteFactor", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGAssignToPortion", System.Data.SqlDbType.VarChar, 2, "MGAssignToPortion"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTID", System.Data.SqlDbType.Int, 4, "MMTID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTCurrentWastePercentage", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(4, Byte), "MTCurrentWastePercentage", System.Data.DataRowVersion.Current, Nothing))
        '
        'daMaterials
        '
        Me.daMaterials.InsertCommand = Me.SqlInsertCommand2
        Me.daMaterials.SelectCommand = Me.SqlSelectCommand2
        Me.daMaterials.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VMaterials", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("MTID", "MTID"), New System.Data.Common.DataColumnMapping("MTName", "MTName"), New System.Data.Common.DataColumnMapping("MTInventoryUOM", "MTInventoryUOM"), New System.Data.Common.DataColumnMapping("MTPrimaryUOM", "MTPrimaryUOM"), New System.Data.Common.DataColumnMapping("MTDiscontinued", "MTDiscontinued"), New System.Data.Common.DataColumnMapping("MTStocked", "MTStocked"), New System.Data.Common.DataColumnMapping("MGID", "MGID"), New System.Data.Common.DataColumnMapping("MTCurrentPurchaseCost", "MTCurrentPurchaseCost"), New System.Data.Common.DataColumnMapping("MTCurrentWasteFactorAllowance", "MTCurrentWasteFactorAllowance"), New System.Data.Common.DataColumnMapping("MGControlledAtHeadOffice", "MGControlledAtHeadOffice"), New System.Data.Common.DataColumnMapping("MTCurrentInventory", "MTCurrentInventory"), New System.Data.Common.DataColumnMapping("MTCurrentWasteFactor", "MTCurrentWasteFactor"), New System.Data.Common.DataColumnMapping("MGAssignToPortion", "MGAssignToPortion"), New System.Data.Common.DataColumnMapping("MMTID", "MMTID"), New System.Data.Common.DataColumnMapping("MTCurrentWastePercentage", "MTCurrentWastePercentage")})})
        '
        'dvGranite
        '
        Me.dvGranite.RowFilter = "MTStocked = 1"
        Me.dvGranite.Sort = "MTName"
        Me.dvGranite.Table = Me.DsGTMS.VMaterials
        '
        'dvSalesRepsInJobs
        '
        Me.dvSalesRepsInJobs.RowFilter = "EGType = 'RC' AND PMShowInJobScreen = 1"
        Me.dvSalesRepsInJobs.Sort = "EXName"
        Me.dvSalesRepsInJobs.Table = Me.DsGTMS.VExpenses
        '
        'dvDirectLabourInJobs
        '
        Me.dvDirectLabourInJobs.RowFilter = "EGType = 'DL' AND PMShowInJobScreen = 1"
        Me.dvDirectLabourInJobs.Sort = "EXName"
        Me.dvDirectLabourInJobs.Table = Me.DsGTMS.VExpenses
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT TAName, TAControlsJobDate, TAMenuCaption, TAID, TAColourR, TAColourG, TACo" & _
        "lourB FROM Tasks"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO Tasks(TAName, TAControlsJobDate, TAMenuCaption, TAID, TAColourR, TACo" & _
        "lourG, TAColourB) VALUES (@TAName, @TAControlsJobDate, @TAMenuCaption, @TAID, @T" & _
        "AColourR, @TAColourG, @TAColourB); SELECT TAName, TAControlsJobDate, TAMenuCapti" & _
        "on, TAID, TAColourR, TAColourG, TAColourB FROM Tasks WHERE (TAID = @TAID)"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAName", System.Data.SqlDbType.VarChar, 50, "TAName"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAControlsJobDate", System.Data.SqlDbType.Bit, 1, "TAControlsJobDate"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAMenuCaption", System.Data.SqlDbType.VarChar, 50, "TAMenuCaption"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAID", System.Data.SqlDbType.Int, 4, "TAID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourR", System.Data.SqlDbType.SmallInt, 2, "TAColourR"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourG", System.Data.SqlDbType.SmallInt, 2, "TAColourG"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourB", System.Data.SqlDbType.SmallInt, 2, "TAColourB"))
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE Tasks SET TAName = @TAName, TAControlsJobDate = @TAControlsJobDate, TAMenu" & _
        "Caption = @TAMenuCaption, TAID = @TAID, TAColourR = @TAColourR, TAColourG = @TAC" & _
        "olourG, TAColourB = @TAColourB WHERE (TAID = @Original_TAID) AND (TAColourB = @O" & _
        "riginal_TAColourB OR @Original_TAColourB IS NULL AND TAColourB IS NULL) AND (TAC" & _
        "olourG = @Original_TAColourG OR @Original_TAColourG IS NULL AND TAColourG IS NUL" & _
        "L) AND (TAColourR = @Original_TAColourR OR @Original_TAColourR IS NULL AND TACol" & _
        "ourR IS NULL) AND (TAControlsJobDate = @Original_TAControlsJobDate) AND (TAMenuC" & _
        "aption = @Original_TAMenuCaption OR @Original_TAMenuCaption IS NULL AND TAMenuCa" & _
        "ption IS NULL) AND (TAName = @Original_TAName OR @Original_TAName IS NULL AND TA" & _
        "Name IS NULL); SELECT TAName, TAControlsJobDate, TAMenuCaption, TAID, TAColourR," & _
        " TAColourG, TAColourB FROM Tasks WHERE (TAID = @TAID)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAName", System.Data.SqlDbType.VarChar, 50, "TAName"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAControlsJobDate", System.Data.SqlDbType.Bit, 1, "TAControlsJobDate"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAMenuCaption", System.Data.SqlDbType.VarChar, 50, "TAMenuCaption"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAID", System.Data.SqlDbType.Int, 4, "TAID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourR", System.Data.SqlDbType.SmallInt, 2, "TAColourR"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourG", System.Data.SqlDbType.SmallInt, 2, "TAColourG"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourB", System.Data.SqlDbType.SmallInt, 2, "TAColourB"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAColourB", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAColourB", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAColourG", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAColourG", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAColourR", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAColourR", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAControlsJobDate", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAControlsJobDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAMenuCaption", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAMenuCaption", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAName", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM Tasks WHERE (TAID = @Original_TAID) AND (TAColourB = @Original_TAColo" & _
        "urB OR @Original_TAColourB IS NULL AND TAColourB IS NULL) AND (TAColourG = @Orig" & _
        "inal_TAColourG OR @Original_TAColourG IS NULL AND TAColourG IS NULL) AND (TAColo" & _
        "urR = @Original_TAColourR OR @Original_TAColourR IS NULL AND TAColourR IS NULL) " & _
        "AND (TAControlsJobDate = @Original_TAControlsJobDate) AND (TAMenuCaption = @Orig" & _
        "inal_TAMenuCaption OR @Original_TAMenuCaption IS NULL AND TAMenuCaption IS NULL)" & _
        " AND (TAName = @Original_TAName OR @Original_TAName IS NULL AND TAName IS NULL)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAColourB", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAColourB", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAColourG", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAColourG", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAColourR", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAColourR", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAControlsJobDate", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAControlsJobDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAMenuCaption", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAMenuCaption", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TAName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TAName", System.Data.DataRowVersion.Original, Nothing))
        '
        'daTasks
        '
        Me.daTasks.DeleteCommand = Me.SqlDeleteCommand2
        Me.daTasks.InsertCommand = Me.SqlInsertCommand3
        Me.daTasks.SelectCommand = Me.SqlSelectCommand3
        Me.daTasks.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Tasks", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("TAName", "TAName"), New System.Data.Common.DataColumnMapping("TAControlsJobDate", "TAControlsJobDate"), New System.Data.Common.DataColumnMapping("TAMenuCaption", "TAMenuCaption"), New System.Data.Common.DataColumnMapping("TAID", "TAID"), New System.Data.Common.DataColumnMapping("TAColourR", "TAColourR"), New System.Data.Common.DataColumnMapping("TAColourG", "TAColourG"), New System.Data.Common.DataColumnMapping("TAColourB", "TAColourB")})})
        Me.daTasks.UpdateCommand = Me.SqlUpdateCommand2
        '
        'dvTasks
        '
        Me.dvTasks.Table = Me.DsGTMS.Tasks
        '
        'dvStaffMembers
        '
        Me.dvStaffMembers.RowFilter = "EGType = 'RC' OR EGType = 'DL' OR EGType = 'OW'"
        Me.dvStaffMembers.Sort = "EXName"
        Me.dvStaffMembers.Table = Me.DsGTMS.VExpenses
        '
        'dvSalesReps
        '
        Me.dvSalesReps.RowFilter = "EXAppearInSalesperson = 1"
        Me.dvSalesReps.Sort = "EXName"
        Me.dvSalesReps.Table = Me.DsGTMS.VExpenses
        '
        'frmExplorer
        '
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(964, 557)
        Me.Controls.Add(Me.pnlOutside)
        Me.Controls.Add(Me.dpNavigation)
        Me.Controls.Add(Me.hideContainerRight)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmExplorer"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Franchise Manager - Branch"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.pnlMain, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlHeader.ResumeLayout(False)
        CType(Me.pnlSpacer2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlSpacer1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pnlOutside, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlOutside.ResumeLayout(False)
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hideContainerRight.ResumeLayout(False)
        Me.dpHelp.ResumeLayout(False)
        Me.DockPanel2_Container.ResumeLayout(False)
        CType(Me.pnlHelp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlHelp.ResumeLayout(False)
        Me.dpNavigation.ResumeLayout(False)
        Me.DockPanel1_Container.ResumeLayout(False)
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvSalesRepCostingGroup, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvDirectLabourInCalendar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrintingSystem, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DockPanel1.ResumeLayout(False)
        CType(Me.dvSalesRepsInCalendar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvDirectLabourCostingGroup, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvGranite, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvSalesRepsInJobs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvDirectLabourInJobs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvTasks, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvStaffMembers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvSalesReps, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private hBRID As Integer
    Public Property BRID() As Integer
        Get
            Return hBRID
        End Get
        Set(ByVal Value As Integer)
            hBRID = Value
            Try
                tvwMenu.Filter.Field("BRID") = Value
            Catch ex As IndexOutOfRangeException
                ' Ignor
            End Try
        End Set
    End Property

    Private Sub frmExplorer_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim w As Integer = tvwMenu.Width
        tvwMenu.Width = 0
        tvwMenu.Width = w
    End Sub

    Private Sub frmExplorer_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        UnloadDynamicCommandsFromMenu()
        UnloadSelectedItemReports()
        Power.Library.SaveBarManagerToXML(BarManager1, UserAppDataPathWithVerson)
        'Power.Library.SaveDockManagerToXML(DockManager1, UserAppDataPathWithVerson)
        ' Close connection and exit
        If SqlConnection.State = ConnectionState.Open Then
            SqlConnection.Close()
        End If
        If HeadOfficeForm Is Nothing Then
            Application.Exit()
        Else
            HeadOfficeForm.RemoveBranchForm(Me)
        End If
    End Sub

#Region " Permissions "

    Public Function SetUpScreen() As Boolean
        For Each cmd As CommandAccess In Commands
            If Not Me.BarManager1.Items(cmd.Command) Is Nothing Then  ' This will not fail if the cmd.Command is not found
                Me.BarManager1.Items(cmd.Command).Tag = cmd
                Me.BarManager1.Items(cmd.Command).Enabled = cmd.Allowed
            End If
        Next
        LoadTreeView()
        'Power.Library.RestoreDockManagerFromXML(DockManager1, UserAppDataPathWithVerson)
        'Power.Library.RestoreBarManagerFromXML(BarManager1, UserAppDataPathWithVerson)
        RefreshDynamicCommands()
    End Function

    Private Sub LoadTreeView()
        ' Load the treeview based on this user's permissions
        tvwMenu.LoadFromStream(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream("WindowsApplication.xmlBranchTree.xml"))
        If Not HasRole("administration") Then
            HideNode("ADMINISTRATION", tvwMenu.Nodes)
        End If
        If Not HasRole("accounting") Then
            HideNode("ACCOUNTING", tvwMenu.Nodes)
        End If
        If Not HasRole("stock_control") Then
            HideNode("STOCK_CONTROL", tvwMenu.Nodes)
        End If
        If Not HasRole("management") Then
            HideNode("MANAGEMENT", tvwMenu.Nodes)
        End If
    End Sub

    Private Sub HideNode(ByVal Action As String, ByVal tnc As TreeNodeCollection)
        For Each node As Power.Forms.TreeNode In tnc
            If node.Action = Action Then
                node.Remove()
            Else
                HideNode(Action, node.Nodes)
            End If
        Next
    End Sub

#End Region

#Region " Navigation "

#Region " Menu Select "

    Private MainControl As Control
    Private NodeCollection As ArrayList
    Private NodeCollectionCursor As Int32
    Private Sub tvwMenu_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvwMenu.AfterSelect
        Dim OldMainControl As Control

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        ' If it has sub-nodes, expand
        tvwMenu.SelectedNode.Expand()
        ' Set up the title bar
        If Not tvwMenu.SelectedNode.ImageIndex = -1 Then
            Me.PictureBox1.Image = ImageListLarge.Images(tvwMenu.SelectedNode.ImageIndex)
        Else
            Me.PictureBox1.Image = ImageListLarge.Images(tvwMenu.SelectedImageIndex)
        End If
        lblSectionTitle.Text = tvwMenu.SelectedNode.Text

        Dim newScreenAction As String = CType(tvwMenu.SelectedNode, Power.Forms.TreeNode).Action

        If Not tvwMenu.SelectedNode.Tag Is Nothing Then
            If TypeOf tvwMenu.SelectedNode.Tag Is Control Then
                If TypeOf tvwMenu.SelectedNode.Tag Is IListControl Then
                    CType(tvwMenu.SelectedNode.Tag, IListControl).FillDataSet()
                End If
                CType(tvwMenu.SelectedNode.Tag, Control).Enabled = True
                CType(tvwMenu.SelectedNode.Tag, Control).BringToFront()
                MainControl.Enabled = False
                MainControl = CType(tvwMenu.SelectedNode.Tag, Control)
            Else
                Throw New InvalidCastException("Invalid tag object: " & tvwMenu.SelectedNode.Tag.GetType.ToString & _
                    ". Expected: System.Windows.Forms.Control.")
            End If
        Else
            ' Add new control
            Dim NewControl As Control
            If tvwMenu.SelectedNode.GetType Is (New Power.Forms.TreeNode).GetType Then
                Select Case newScreenAction
                    Case "WELCOME"
                        Dim pnl As New pnlRichText(Me, "WindowsApplication.BranchIntro.rtf", newScreenAction)
                        NewControl = pnl
                    Case "ADMINISTRATION"
                        NewControl = New pnlRichText(Me, "WindowsApplication.Administration.rtf", newScreenAction)
                    Case "CONTACTS"
                        Dim lv As New lvContacts2(Me.SqlConnection, BRID, Me)
                        NewControl = lv
                    Case "LEADS"
                        Dim lv As New lvLeads2(Me.SqlConnection, BRID, Me)
                        NewControl = lv
                    Case "CALENDAR_RC"
                        Dim lv As New lvCalendar2(Me.SqlConnection, BRID, Me, "EXAppearInCalendarRC")
                        NewControl = lv
                    Case "BOOKINGS"
                        Dim lv As New lvBookings2(Me.SqlConnection, BRID, Me)
                        NewControl = lv
                    Case "CALENDAR_DL"
                        Dim lv As New lvCalendar2(Me.SqlConnection, BRID, Me, "EXAppearInCalendarDL")
                        lv.Calendar.Calendar.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Week
                        NewControl = lv
                    Case "ORDERS_ADMIN"
                        Dim lv As New lvOrders2(Me.SqlConnection, BRID, Me, True)
                        NewControl = lv
                    Case "ORDERS_ACCOUNTING"
                        Dim lv As New lvOrders2(Me.SqlConnection, BRID, Me)
                        NewControl = lv
                    Case "ACCOUNTING"
                        NewControl = New pnlRichText(Me, "WindowsApplication.Accounting.rtf", newScreenAction)
                    Case "JOBS"
                        Dim lv As New lvJobs2(Me.SqlConnection, BRID, Me)
                        NewControl = lv
                    Case "EXPENSE_ALLOWANCES"
                        Dim lv As New lvExpenseAllowances2(Me.SqlConnection, BRID, Me)
                        NewControl = lv
                    Case "EXPENSE_ADJUSTMENTS_PR"
                        Dim lv As New lvExpenseReceipts2(Me.SqlConnection, BRID, Me)
                        NewControl = lv
                    Case "EXPENSE_ADJUSTMENTS_BO"
                        Dim lv As New lvExpenseBonuses2(Me.SqlConnection, BRID, Me)
                        NewControl = lv
                    Case "STOCK_ADJUSTMENTS"
                        Dim lv As New lvStockAdjustments2(Me.SqlConnection, BRID, Me)
                        NewControl = lv
                    Case "STOCK_TRACKING"
                        Dim lv As New lvStockTracking2(Me.SqlConnection, BRID, Me)
                        NewControl = lv
                    Case "STOCK_CONTROL"
                        NewControl = New pnlRichText(Me, "WindowsApplication.InvenotryTracking.rtf", newScreenAction)
                    Case "MANAGEMENT"
                        NewControl = New pnlRichText(Me, "WindowsApplication.BusinessSetup.rtf", newScreenAction)
                    Case "MATERIALS"
                        Dim lv As New lvMaterials2(Me.SqlConnection, BRID, CType(tvwMenu.SelectedNode, Power.Forms.TreeNode).Filter("MGID"), Me, True)
                        NewControl = lv
                    Case "MATERIALS_HEADOFFICE"
                        Dim lv As New lvMaterials2(Me.SqlConnection, BRID, CType(tvwMenu.SelectedNode, Power.Forms.TreeNode).Filter("MGID"), Me, False)
                        NewControl = lv
                    Case "EXPENSES"
                        Dim lv As New lvExpenses2(Me.SqlConnection, BRID, CType(tvwMenu.SelectedNode, Power.Forms.TreeNode).Filter("EGID"), Me)
                        NewControl = lv
                    Case "POSTCODES"
                        Dim lv As New lvPostCodes(Me.SqlConnection, BRID, Me)
                        NewControl = lv
                    Case "SECONDARYLEADSOURCES"
                        Dim lv As New lvSecondaryEnquirySources(Me.SqlConnection, BRID, Me)
                        NewControl = lv
                    Case "BRANCH_SETTINGS"
                        Dim pnl As New pnlRichText(Me, "WindowsApplication.BranchSettings.rtf", newScreenAction)
                        NewControl = pnl
                    Case Else
                        Dim lv As New lvNodeList(tvwMenu.SelectedNode, ImageListLarge, ImageListSmall, Me)
                        lv.ListView.View = View.LargeIcon
                        NewControl = lv
                End Select
            Else
                Dim lv As New lvNodeList(tvwMenu.SelectedNode, ImageListLarge, ImageListSmall, Me)
                lv.ListView.View = View.LargeIcon
                NewControl = lv

                btnNew.Enabled = False
                btnEdit.Enabled = False
                btnDelete.Enabled = False
            End If
            If Not NewControl Is Nothing Then
                NewControl.Dock = DockStyle.Fill
                NewControl.Visible = False
                pnlMain.Controls.Add(NewControl)
                NewControl.SendToBack()
                NewControl.Visible = True
                NewControl.BringToFront()
                If Not MainControl Is Nothing Then
                    MainControl.Enabled = False
                End If
                tvwMenu.SelectedNode.Tag = NewControl
                MainControl = NewControl
            End If
        End If
        If Not BackForwarding Then
            Do Until NodeCollection.Count <= NodeCollectionCursor + 1
                NodeCollection.RemoveAt(NodeCollectionCursor + 1)
            Loop
            NodeCollection.Insert(NodeCollectionCursor + 1, tvwMenu.SelectedNode)
            NodeCollectionCursor = NodeCollectionCursor + 1
            'btnForward.Enabled = False
            If NodeCollectionCursor > 0 Then
                'btnBack.Enabled = True
            End If
        End If
        'btnUp.Enabled = Not tvwMenu.SelectedNode.Parent Is Nothing
        EnableDisable(MainControl)
        SetUpHelp(CType(tvwMenu.SelectedNode, Power.Forms.TreeNode).HelpScreen)
        LoadSelectedItemReports(MainControl)

        ' Setting up the status bar
        If TypeOf MainControl Is IListControl Then
            UpdateNumItems(CType(MainControl, IListControl).NumItems)
            UpdateVisibleItems(CType(MainControl, IListControl).VisibleItems)
        Else
            UpdateNumItems(0)
            UpdateVisibleItems(0)
        End If


        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private SelectedItemReportsBarItem As DevExpress.XtraBars.BarItem = Nothing
    Private SelectedItemFormsBarItem As DevExpress.XtraBars.BarItem = Nothing
    Private Sub LoadSelectedItemReports(ByVal MainControl As Control)
        UnloadSelectedItemReports()
        If TypeOf MainControl Is ISelectedItemReports Then
            SelectedItemReportsBarItem = CType(MainControl, ISelectedItemReports).SelectedItemReports
            If Not SelectedItemReportsBarItem Is Nothing Then
                LoadBarItemIntoSub(SelectedItemReportsBarItem, subReports)
            End If
            SelectedItemFormsBarItem = CType(MainControl, ISelectedItemReports).SelectedItemForms
            If Not SelectedItemFormsBarItem Is Nothing Then
                LoadBarItemIntoSub(SelectedItemFormsBarItem, subForms)
            End If
        End If
    End Sub

    Private Sub UnloadSelectedItemReports()
        If Not SelectedItemReportsBarItem Is Nothing Then
            UnloadBarItem(SelectedItemReportsBarItem)
            SelectedItemReportsBarItem = Nothing
        End If
        If Not SelectedItemFormsBarItem Is Nothing Then
            UnloadBarItem(SelectedItemFormsBarItem)
            SelectedItemFormsBarItem = Nothing
        End If
    End Sub

    Public Sub UpdateNumItems(ByVal items As Integer)
        If items = 1 Then
            bsiNumItems.Caption = "1 Item"
        Else
            bsiNumItems.Caption = items & " Items"
        End If
    End Sub

    Public Sub UpdateVisibleItems(ByVal items As Integer)
        If items = 1 Then
            bsiVisibleItems.Caption = "1 Visible Item"
        Else
            bsiVisibleItems.Caption = items & " Visible Items"
        End If
    End Sub

#End Region

#Region " EnableDisable "

    Private Sub CheckEnabled(ByVal btn As DevExpress.XtraBars.BarItem, ByVal enabled As Boolean)
        If btn.Tag Is Nothing Then
            btn.Enabled = enabled
        Else
            If TypeOf btn.Tag Is CommandAccess Then
                btn.Enabled = CType(btn.Tag, CommandAccess).Allowed And enabled
            Else
                Throw New Exception("The tag against " & btn.Name & " should be of type CommandAccess")
            End If
        End If
    End Sub

    Private Sub EnableDisable(ByVal MainControl As Control)
        If TypeOf MainControl Is IListControl Then
            Dim ctrl As IListControl = CType(MainControl, IListControl)
            CheckEnabled(btnNew, ctrl.AllowNew)
            CheckEnabled(btnEdit, ctrl.AllowEdit)
            CheckEnabled(btnDelete, ctrl.AllowDelete)
            CheckEnabled(btnCancel, ctrl.AllowCancel)
        Else
            CheckEnabled(btnNew, False)
            CheckEnabled(btnEdit, False)
            CheckEnabled(btnDelete, False)
            CheckEnabled(btnCancel, False)
        End If
        CheckEnabled(btnPrint, TypeOf MainControl Is IPrintableControl)
        CheckEnabled(btnQuickPrint, TypeOf MainControl Is IPrintableControl)
        CheckEnabled(btnPrintPreview, TypeOf MainControl Is IPrintableControl)
        CheckEnabled(btnPageSetup, TypeOf MainControl Is IPrintableControl)
    End Sub

#End Region

#Region " Side Panel Help "

    Private Sub SetUpHelp(ByVal richTextStreamPath As String)
        If richTextStreamPath Is Nothing Then
            LoadRichText("WindowsApplication.NoHelpAvailable.rtf")
        Else
            LoadRichText(richTextStreamPath)
        End If
    End Sub

    Private Sub LoadRichText(ByVal richTextStreamPath As String)
        rtbHelp.Font = New Font(Me.Font, FontStyle.Regular)

        rtbHelp.Visible = False ' Hide while loading
        Me.rtbHelp.LoadFile(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream(richTextStreamPath), RichTextBoxStreamType.RichText)

        Dim cursor As Integer = 1
        Do Until InStr(cursor, rtbHelp.Text, "<a") = 0
            ConvertToLink(rtbHelp, InStr(cursor, rtbHelp.Text, "<a"))
            cursor += 1
        Loop
        rtbHelp.Visible = True ' Show
    End Sub

    Public Shared Sub ConvertToLink(ByVal rtb As Power.Forms.RichTextBox, ByVal startPosition As Integer)
        Dim address, text As String

        Dim startOpenTag As Integer = startPosition
        Dim endOpenTag As Integer = InStr(startPosition, rtb.Text, ">")
        If endOpenTag = 0 Then Exit Sub ' Could not find ending '>'

        Dim nextOpenTag As Integer = InStr(startOpenTag + 1, rtb.Text, "<")
        If 0 <> nextOpenTag And nextOpenTag < endOpenTag Then Exit Sub ' Found extra '<' within tag

        Dim startAddress As Integer = InStr(startOpenTag + 1, rtb.Text, "{")
        Dim endAddress As Integer = InStr(startAddress + 1, rtb.Text, "}")
        If startAddress = 0 Or startAddress > endOpenTag Or endAddress = 0 Or endAddress > endOpenTag Then Exit Sub ' Could not find two '"'

        address = Mid(rtb.Text, startAddress + 1, endAddress - startAddress - 1)

        Dim startCloseTag As Integer = InStr(endOpenTag, rtb.Text, "</a>")
        If startCloseTag = 0 Then Exit Sub ' could not find closing tag

        text = Mid(rtb.Text, endOpenTag + 1, startCloseTag - endOpenTag - 1)

        rtb.Select(startOpenTag - 1, (startCloseTag + 3) - startOpenTag + 1) ' Don't know why we need the '- 1' in both cases, but whatever works :)
        rtb.InsertLink(text, address)
    End Sub

    Private Sub RichTextBox1_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkClickedEventArgs) Handles rtbHelp.LinkClicked
        DoHelpAction(CType(tvwMenu.SelectedNode, Power.Forms.TreeNode).Action, Mid(e.LinkText, InStr(e.LinkText, "#") + 1))
    End Sub

#Region " Help Actions "

    Private Sub DoHelpAction(ByVal screen As String, ByVal helpAction As String)
        helpAction = Trim(helpAction)
        If InStr(helpAction, "/") > 0 Then
            ShowHelpTopic(Me, Mid(helpAction, InStr(helpAction, "/") + 1))
        Else
            Select Case screen
                Case "ADMINISTRATION"
                    Select Case helpAction
                        ' List help actions
                    End Select
                Case "LEADS"
                    Select Case helpAction
                        ' List help actions
                    End Select
                Case "BOOKINGS"
                    Select Case helpAction
                        ' List help actions
                    End Select
            End Select
        End If
    End Sub

#End Region

#End Region

#Region " Back and Forward "

    Private BackForwarding As Boolean = False ' If this is true, then we are calling the below back forward methods
    Private Sub GoBack()
        NodeCollectionCursor = NodeCollectionCursor - 1
        BackForwarding = True
        tvwMenu.SelectedNode = CType(NodeCollection(NodeCollectionCursor), TreeNode)
        BackForwarding = False
        If NodeCollectionCursor <= 0 Then
            'btnBack.Enabled = False
        End If
        'btnForward.Enabled = True
    End Sub

    Private Sub GoForward()
        NodeCollectionCursor = NodeCollectionCursor + 1
        BackForwarding = True
        tvwMenu.SelectedNode = CType(NodeCollection(NodeCollectionCursor), TreeNode)
        BackForwarding = False
        If NodeCollectionCursor >= NodeCollection.Count - 1 Then
            'btnForward.Enabled = False
        End If
        'btnBack.Enabled = True
    End Sub

    Private Sub GoUp()
        tvwMenu.SelectedNode = tvwMenu.SelectedNode.Parent
    End Sub

    Private Sub btnBack2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        GoBack()
    End Sub

    Private Sub btnForward2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        GoForward()
    End Sub

#End Region

#End Region

#Region " Commands "

#Region " File "

    Private Sub btnNew_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnNew.ItemClick
        CType(MainControl, IListControl).List_New()
    End Sub

    Private Sub btnEdit_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnEdit.ItemClick
        CType(MainControl, IListControl).List_Edit()
    End Sub

    Private Sub btnDelete_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnDelete.ItemClick
        CType(MainControl, IListControl).List_Delete()
    End Sub

    Private Sub btnCancel_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnCancel.ItemClick
        CType(MainControl, IListControl).List_Cancel()
    End Sub

    Private Sub btnPageSetup_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPageSetup.ItemClick
        PrintingSystem.PageSetup()
    End Sub

    Private Sub btnQuickPrint_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnQuickPrint.ItemClick
        CType(MainControl, IPrintableControl).QuickPrint(PrintingSystem)
    End Sub

    Private Sub btnPrint_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrint.ItemClick
        CType(MainControl, IPrintableControl).Print(PrintingSystem)
    End Sub

    Private Sub btnPrintPreview_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrintPreview.ItemClick
        CType(MainControl, IPrintableControl).PrintPreview(PrintingSystem)
    End Sub

    Private Sub btnExit_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnExit.ItemClick
        Me.Close()
    End Sub

#End Region

#Region " View "

    Private Sub btnAutoHideNavigationPanel_CheckedChanged(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnAutoHideNavigationPanel.CheckedChanged
        If Not IsInitializing Then
            If btnAutoHideNavigationPanel.Checked Then
                Me.dpNavigation.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide
            Else
                Me.dpNavigation.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible
            End If
        End If
    End Sub

    Private Sub dpNavigation_VisibilityChanged(ByVal sender As Object, ByVal e As DevExpress.XtraBars.Docking.VisibilityChangedEventArgs) Handles dpNavigation.VisibilityChanged
        If Not IsInitializing Then
            btnAutoHideNavigationPanel.Checked = (dpNavigation.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide)
        End If
    End Sub

    Private Sub btnAutoHideHelpPanel_CheckedChanged(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnAutoHideHelpPanel.CheckedChanged
        If Not IsInitializing Then
            If btnAutoHideHelpPanel.Checked Then
                Me.dpHelp.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide
            Else
                Me.dpHelp.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible
            End If
        End If
    End Sub

    Private Sub dpHelp_VisibilityChanged(ByVal sender As Object, ByVal e As DevExpress.XtraBars.Docking.VisibilityChangedEventArgs) Handles dpHelp.VisibilityChanged
        If Not IsInitializing Then
            btnAutoHideHelpPanel.Checked = (dpHelp.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide)
        End If
    End Sub

#End Region

#Region " Reports "

#Region " Operations "

    Private Sub btnQuotesOutstandingReportNone_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnQuotesOutstandingReportNone.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("QuotesOutstandingReport.html")
            Dim rep As New repQuotesOutstanding
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repQuotesOutstanding](@BRID, @EXID, @FROM_DATE, @TO_DATE, @AsOf)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@EXID", DBNull.Value)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            cmd.Parameters.Add("@AsOf", DateTime.Now)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repQuotesOutstanding)
            cmd = New SqlClient.SqlCommand("SELECT * FROM dbo.[repQuoteRequestsNotScheduled](@BRID, @EXID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@EXID", DBNull.Value)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            da = New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repQuoteRequestsNotScheduled)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            rep.SetParameterValue("EXName", e.Item.Caption)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Quotes Outstanding Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnQuotesOutstandingReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("QuotesOutstandingReport.html")
            Dim rep As New repQuotesOutstanding
            rep.Load()
            Dim ds As New dsReports
            Dim EXID As Integer = CType(e.Item.Tag, DataRowView)("EXID")
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repQuotesOutstanding](@BRID, @EXID, @FROM_DATE, @TO_DATE, @AsOf)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@EXID", EXID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            cmd.Parameters.Add("@AsOf", DateTime.Now)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repQuotesOutstanding)
            cmd = New SqlClient.SqlCommand("SELECT * FROM dbo.[repQuoteRequestsNotScheduled](@BRID, @EXID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@EXID", EXID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            da = New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repQuoteRequestsNotScheduled)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            rep.SetParameterValue("EXName", e.Item.Caption)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Quotes Outstanding Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnUnfinishedJobsReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnUnfinishedJobsReport.ItemClick
        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        Dim gui As New frmReport("UnfinishedJobsReport.html")
        Dim rep As New repUnfinishedJobs
        rep.Load()
        Dim ds As New dsReports
        Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repUnfinishedJobs](@BRID, @AS_OF)", SqlConnection)
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@AS_OF", Today.Date)
        Dim da As New SqlClient.SqlDataAdapter(cmd)
        ReadUncommittedFill(da, ds.repUnfinishedJobs)
        rep.SetDataSource(ds)
        rep.SetParameterValue("AsOfDate", CDate(cmd.Parameters("@AS_OF").Value))
        gui.Display(rep)
        Me.Cursor = Windows.Forms.Cursors.Default
        gui.Text = "Unfinished Jobs Report"
        gui.Show()
    End Sub

    Private Sub btnDebtorsReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnDebtorsReport.ItemClick
        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        Dim gui As New frmReport("DebtorsReport.html")
        Dim rep As New repDebtorsReport
        rep.Load()
        Dim ds As New dsReports
        Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repDebtorsReport](@BRID, @GETDATE)", SqlConnection)
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@GETDATE", Today.Date)
        Dim da As New SqlClient.SqlDataAdapter(cmd)
        ReadUncommittedFill(da, ds.repDebtorsReport)
        cmd.CommandText = "SELECT * FROM dbo.[repDebtorStatisticsReport](@BRID, @GETDATE)"
        ReadUncommittedFill(da, ds.repDebtorStatisticsReport)
        cmd.CommandText = "SELECT * FROM dbo.[repUnfinishedJobs](@BRID, @GETDATE)"
        ReadUncommittedFill(da, ds.repUnfinishedJobs)
        rep.SetDataSource(ds)
        rep.SetParameterValue("AsOfDate", CDate(cmd.Parameters("@GETDATE").Value))
        gui.Display(rep)
        Me.Cursor = Windows.Forms.Cursors.Default
        gui.Text = "Accounts Receivable Report"
        gui.Show()
    End Sub

    Private Sub btnHODebtorStatisticsReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHODebtorStatisticsReport.ItemClick
        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        Dim gui As New frmReport(Nothing)
        Dim rep As New repHODebtorStatisticsReport
        rep.Load()
        Dim ds As New dsReports
        Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repDebtorStatisticsReport](@BRID, @GETDATE)", SqlConnection)
        cmd.Parameters.Add("@BRID", DBNull.Value)
        cmd.Parameters.Add("@GETDATE", Today.Date)
        Dim da As New SqlClient.SqlDataAdapter(cmd)
        ReadUncommittedFill(da, ds.repDebtorStatisticsReport)
        cmd.CommandText = "SELECT * FROM Branches WHERE BRIncludeInReporting = 1"
        ReadUncommittedFill(da, ds.Branches)
        rep.SetDataSource(ds)
        rep.SetParameterValue("AsOfDate", CDate(cmd.Parameters("@GETDATE").Value))
        gui.Display(rep, True)
        Me.Cursor = Windows.Forms.Cursors.Default
        gui.Text = "Accounts Receivable Statistics Report - Group Report"
        gui.Show()
    End Sub

    Private Sub btnOrderingIncompleteReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnOrderingIncompleteReport.ItemClick
        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        Dim gui As New frmReport("OrderingIncompleteReport.html")
        Dim rep As New repOrdersIncomplete
        rep.Load()
        Dim ds As New dsReports
        Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repOrdersIncomplete](@BRID)", SqlConnection)
        cmd.Parameters.Add("@BRID", BRID)
        Dim da As New SqlClient.SqlDataAdapter(cmd)
        ReadUncommittedFill(da, ds.repOrdersIncomplete)
        rep.SetDataSource(ds)
        rep.SetParameterValue("AsOfDate", Today.Date)
        gui.Display(rep)
        Me.Cursor = Windows.Forms.Cursors.Default
        gui.Text = "Ordering Incomplete Report"
        gui.Show()
    End Sub

    Private Sub btnAppliancesNotReceivedReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnAppliancesNotReceivedReport.ItemClick
        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        Dim gui As New frmReport(Nothing)
        Dim rep As New repAppliancesNotReceived
        rep.Load()
        Dim ds As New dsReports
        Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repAppliancesNotReceived](@BRID)", SqlConnection)
        cmd.Parameters.Add("@BRID", BRID)
        Dim da As New SqlClient.SqlDataAdapter(cmd)
        ReadUncommittedFill(da, ds.repAppliancesNotReceived)
        rep.SetDataSource(ds)
        gui.Display(rep)
        Me.Cursor = Windows.Forms.Cursors.Default
        gui.Text = "Appliances Not Received Report"
        gui.Show()
    End Sub

    Private Sub btnJobIssuesReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnJobIssuesReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("JobIssuesReport.html")
            Dim rep As New repJobIssues
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repJobIssues](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repJobIssues)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Job Issues Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnHOJobIssuesReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHOJobIssuesReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("JobIssuesReport.html")
            Dim rep As New repHOJobIssues
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repJobIssues](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", DBNull.Value)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repJobIssues)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep, True)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Job Issues Report - Group Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnPredictedSalesReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPredictedSalesReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        DateRangeGui.dpFrom.EditValue = Today.Date
        DateRangeGui.dpTo.EditValue = Nothing
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("PredictedSalesReport.html")
            Dim rep As New repPredictedSales
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repPredictedSales](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repPredictedSales)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Predicted Sales Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnJobsFinishedOnTimeReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnJobsFinishedOnTimeReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("JobsFinishedOnTimeReport.html")
            Dim rep As New repFinishedOnTime
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repFinishedOnTime](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repFinishedOnTime)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Jobs Finished On Time Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnHOJobsFinishedOnTimeReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHOJobsFinishedOnTimeReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("JobsFinishedOnTimeReport.html")
            Dim rep As New repHOFinishedOnTime
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repFinishedOnTime](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", DBNull.Value)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repFinishedOnTime)
            cmd.CommandText = "SELECT * FROM Branches WHERE BRIncludeInReporting = 1"
            ReadUncommittedFill(da, ds.Branches)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep, True)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Jobs Finished On Time Report - Group Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnFollowUpReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
        Dim DateGui As New frmDate
        If DateGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("FollowUpReport.html")
            Dim rep As New repFollowUp
            rep.Load()
            Dim ds As New dsReports
            Dim EXID As Integer = CType(e.Item.Tag, DataRowView)("EXID")
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repFollowUp](@BRID, @AS_OF) WHERE EXID = @EXID", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@AS_OF", DateGui.DateValue)
            cmd.Parameters.Add("@EXID", EXID)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repFollowUp)
            rep.SetDataSource(ds)
            rep.SetParameterValue("AsOfDate", CDate(cmd.Parameters("@AS_OF").Value))
            rep.SetParameterValue("EXName", CType(e.Item.Tag, DataRowView)("EXName"))
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Follow Up Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnHOQuoteStatisticsReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHOQuoteStatisticsReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repHOAppointmentStatistics
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repAppointmentStatistics](@BRID, @FROM_DATE, @TO_DATE, @AsOf)", SqlConnection)
            cmd.Parameters.Add("@BRID", DBNull.Value)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            cmd.Parameters.Add("@AsOf", DateTime.Now)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repAppointmentStatistics)
            cmd.CommandText = "SELECT * FROM Branches WHERE BRIncludeInReporting = 1"
            ReadUncommittedFill(da, ds.Branches)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep, True)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Quote Statistics Report"
            gui.Show()
        End If
    End Sub

#End Region

#Region " Calendars "

    Private Sub btnSalespersonsCalendarsAll_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnSalespersonsCalendarsAll.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        DateRangeGui.dpFrom.EditValue = Today.Date
        DateRangeGui.dpTo.EditValue = Nothing
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("SalespersonsCalendars.html")
            Dim rep As New repCalendarAllRC
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repCalendar](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repCalendar)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            rep.SetParameterValue("BRID", BRID)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Salesperson Calendar"
            gui.Show()
            Application.DoEvents()
        End If
    End Sub

    Private Sub btnSalespersonsCalendars_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        DateRangeGui.dpFrom.EditValue = Today.Date
        DateRangeGui.dpTo.EditValue = Nothing
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("SalespersonsCalendars.html")
            Dim rep As New repCalendarRC
            rep.Load()
            Dim ds As New dsReports
            Dim EXID As Integer = CType(e.Item.Tag, DataRowView)("EXID")
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repCalendar](@BRID, @FROM_DATE, @TO_DATE) WHERE EXID = @EXID", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            cmd.Parameters.Add("@EXID", EXID)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repCalendar)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            rep.SetParameterValue("BRID", BRID)
            rep.SetParameterValue("EXID", EXID)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Salesperson Calendar"
            gui.Show()
            Application.DoEvents()
        End If
    End Sub

    Private Sub btnJobBoardbyDirectLabour_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        DateRangeGui.dpFrom.EditValue = Today.Date
        DateRangeGui.dpTo.EditValue = Nothing
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("JobBoardByDirectLaborReport.html")
            Dim rep As New repCalendarDL
            rep.Load()
            Dim ds As New dsReports
            Dim EXID As Integer = CType(e.Item.Tag, DataRowView)("EXID")
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repCalendar](@BRID, @FROM_DATE, @TO_DATE) WHERE EXID = @EXID", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            cmd.Parameters.Add("@EXID", EXID)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repCalendar)
            cmd = New SqlClient.SqlCommand("SELECT * FROM dbo.[subCalendarGranite](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            da = New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.subCalendarGranite)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            rep.SetParameterValue("BRID", BRID)
            rep.SetParameterValue("EXID", EXID)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Job Board by Direct Labor"
            gui.Show()
            Application.DoEvents()
        End If
    End Sub

    Private Sub btnJobBoardbyDirectLabourAll_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnJobBoardbyDirectLabourAll.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        DateRangeGui.dpFrom.EditValue = Today.Date
        DateRangeGui.dpTo.EditValue = Nothing
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("JobBoardByDirectLaborReport.html")
            Dim rep As New repCalendarAllDL
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repCalendar](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repCalendar)
            cmd = New SqlClient.SqlCommand("SELECT * FROM dbo.[subCalendarGranite](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            da = New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.subCalendarGranite)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            rep.SetParameterValue("BRID", BRID)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Job Board by Direct Labor"
            gui.Show()
            Application.DoEvents()
        End If
    End Sub

    Private Sub btnJobBoardbyTask_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        DateRangeGui.dpFrom.EditValue = Today.Date
        DateRangeGui.dpTo.EditValue = Nothing
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repCalendarTask
            rep.Load()
            Dim ds As New dsReports
            Dim TAID As Integer = CType(e.Item.Tag, DataRowView)("TAID")
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repCalendar](@BRID, @FROM_DATE, @TO_DATE) WHERE TAID = @TAID AND APType = 'JB'", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            cmd.Parameters.Add("@TAID", TAID)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repCalendar)
            cmd = New SqlClient.SqlCommand("SELECT * FROM dbo.[subCalendarGranite](@BRID, @FROM_DATE, @TO_DATE) WHERE TAID = @TAID", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@TAID", TAID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            da = New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.subCalendarGranite)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            rep.SetParameterValue("BRID", BRID)
            rep.SetParameterValue("TAID", TAID)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Job Board by Task"
            gui.Show()
            Application.DoEvents()
        End If
    End Sub

#End Region

#Region " Profit "

    Private Sub btnAllJobsReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnAllJobsReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("AllJobsReport.html")
            Dim rep As New repAllJobsReport
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repAllJobsReport](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repAllJobsReport)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "All Jobs Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnProfitReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnProfitReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("ProfitReport.html")
            Dim rep As New repProfitReport
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repProfitReport](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repProfitReport)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Profit Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnHOAllJobsReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHOAllJobsReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("AllJobsReport.html")
            Dim rep As New repHOAllJobsReport
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repAllJobsReport](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", DBNull.Value)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repAllJobsReport)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep, True)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "All Jobs Report - Group Report"
            gui.Show()
        End If
    End Sub

#End Region

#Region " Materials "

    Private Sub btnWasteReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnWasteReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("WasteReport.html")
            Dim rep As New repWasteReport
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repWasteReport2](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            cmd.CommandTimeout = 50
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repWasteReport)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Waste Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnHOWasteReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHOWasteReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("WasteReport.html")
            Dim rep As New repHOWasteReport
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repWasteReport2](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", DBNull.Value)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            cmd.CommandTimeout = 50
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repWasteReport)
            cmd.CommandText = "SELECT * FROM dbo.[repWasteReportByBranch2](@BRID, @FROM_DATE, @TO_DATE)"
            ReadUncommittedFill(da, ds.repWasteReportByBranch)
            cmd.CommandText = "SELECT * FROM Branches WHERE BRIncludeInReporting = 1"
            ReadUncommittedFill(da, ds.Branches)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep, True)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Waste Report - Group Report"
            gui.Show()
        End If

    End Sub

    Private Sub btnStockReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnStockReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("StockReport.html")
            Dim rep As New repStockReport
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repStockReport2](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repStockReport)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Inventory Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnStockRequirementsReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnStockRequirementsReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        DateRangeGui.dpFrom.EditValue = Today.Date
        DateRangeGui.dpTo.EditValue = Nothing
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("StockRequirementsReport.html")
            Dim rep As New repProductRequirements
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repProductRequirements2](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repProductRequirements)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Tracked Material Requirements Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnGraniteinJobsReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("TrendProductsInJobsReport.html")
            Dim rep As New repGraniteInJobs
            rep.Load()
            Dim ds As New dsReports
            Dim MTID As Integer = CType(e.Item.Tag, DataRowView)("MTID")
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repGraniteInJobs](@BRID, @MTID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@MTID", MTID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repGraniteInJobs)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            rep.SetParameterValue("BRID", BRID)
            rep.SetParameterValue("MTID", MTID)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Trend Product in Jobs Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnJobTypesReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnJobTypesReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("")
            Dim rep As New repJobTypes
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repJobTypes](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repJobTypes)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Job Types Report"
            gui.Show()
        End If
    End Sub

#End Region

#Region " Direct Labor and Salespeople "

    Private Sub btnDirectLabourandSalespeopleSpecifiedinJobs_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnDirectLabourandSalespeopleSpecifiedinJobs.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("DirectLaborandSalespeopleSpecifiedinJobsReport.html")
            Dim rep As New repDirectLabour
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repDirectLabour](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repDirectLabour)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Direct Labor and Salespeople Job Costings Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnDirectLabourJobList_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("DirectLaborJobListReport.html")
            Dim rep As New repDirectLabourJobs
            rep.Load()
            Dim ds As New dsReports
            Dim EXID As Integer = CType(e.Item.Tag, DataRowView)("EXID")
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repDirectLabourJobs](@BRID, @FROM_DATE, @TO_DATE) WHERE EGType = 'DL' AND EXID = " & EXID, SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repDirectLabourJobs)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            rep.SetParameterValue("BRID", BRID)
            rep.SetParameterValue("EXID", EXID)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Direct Labor Job Costings List"
            gui.Show()
        End If
    End Sub




    Private Sub btnFollowupReportNone_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnFollowupReportNone.ItemClick
        Dim DateGui As New frmDate
        If DateGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("FollowUpReport.html")
            Dim rep As New repFollowUp
            rep.Load()
            Dim ds As New dsReports
            Dim EXID As Integer = CType(e.Item.Tag, DataRowView)("EXID")
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repFollowUp](@BRID, @AS_OF) WHERE EXID = @EXID", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@AS_OF", DateGui.DateValue)
            cmd.Parameters.Add("@EXID", EXID)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repFollowUp)
            rep.SetDataSource(ds)
            rep.SetParameterValue("AsOfDate", CDate(cmd.Parameters("@AS_OF").Value))
            rep.SetParameterValue("EXName", CType(e.Item.Tag, DataRowView)("EXName"))
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Follow Up Report"
            gui.Show()
        End If

    End Sub


    Private Sub btnDirectSalesReportNone_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnDirectSalesReportNone.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("DirectLaborJobListReport.html")
            Dim rep As New repDirectLabourJobs
            rep.Load()
            Dim ds As New dsReports
            Dim EXID As Integer = 0
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repDirectLabourJobs](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repDirectLabourJobs)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            rep.SetParameterValue("BRID", BRID)
            rep.SetParameterValue("EXID", EXID)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Direct Labor Job Costings List"
            gui.Show()
        End If
    End Sub

    Private Sub btnSalespersonJobList_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("SalespersonJobListReport.html")
            Dim rep As New repDirectLabourJobs
            rep.Load()
            Dim ds As New dsReports
            Dim EXID As Integer = CType(e.Item.Tag, DataRowView)("EXID")
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repDirectLabourJobs](@BRID, @FROM_DATE, @TO_DATE) WHERE EGType = 'RC' AND EXID = " & EXID, SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repDirectLabourJobs)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            rep.SetParameterValue("BRID", BRID)
            rep.SetParameterValue("EXID", EXID)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Salesperson Job Costings List"
            gui.Show()
        End If
    End Sub

    Private Sub btnRepJobs_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("")
            Dim rep As New repRepJobs
            rep.Load()
            Dim ds As New dsReports
            Dim EXID As Integer = CType(e.Item.Tag, DataRowView)("EXID")
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repRepJobs](@BRID, @FROM_DATE, @TO_DATE) WHERE EXID = " & EXID, SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repRepJobs)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            rep.SetParameterValue("BRID", BRID)
            rep.SetParameterValue("EXID", EXID)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Salesperson Job List"
            gui.Show()
        End If
    End Sub

    Private Sub btnHoursCostAndVolumeReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHoursCostAndVolumeReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repHoursCostAndVolumeReport_Branch
            rep.Load()
            Dim ds As New dsReportsHeadOfficeDirectLabour
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[Hours Cost and Volume Report](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.Hours_Cost_and_Volume_Report)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Hours, Cost and Volume Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnHoursCostsAndVolumeReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHoursCostsAndVolumeReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport(Nothing)
            Dim rep As New repHoursCostAndVolumeReport
            rep.Load()
            Dim ds As New dsReportsHeadOfficeDirectLabour
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[Hours Cost and Volume Report](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", DBNull.Value)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.Hours_Cost_and_Volume_Report)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep, True)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Hours, Cost and Volume Report - Group Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnSalespeopleJobStatistics_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnSalespeopleJobStatistics.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("SalespeopleJobStatisticsReport.html")
            Dim rep As New repRepsJobStatistics
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repRepsJobStatistics](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repRepsJobStatistics)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Salespeople Job Statistics Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnSalespeopleQuoteStatistics_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnSalespeopleQuoteStatistics.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("SalespeopleQuoteStatisticsReport.html")
            Dim rep As New repRepsAppointmentStatistics
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repRepsAppointmentStatistics](@BRID, @FROM_DATE, @TO_DATE, @AsOf)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            cmd.Parameters.Add("@AsOf", DateTime.Now)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repRepsAppointmentStatistics)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Salespeople Quote Statistics Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnQuoteRequestsCancelledReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnQuoteRequestsCancelledReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("QuoteRequestsCancelledReport.html")
            Dim rep As New repQuoteRequestsCancelledReport
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repQuoteRequestsCancelledReport](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repQuoteRequestsCancelledReport)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Quote Requests Cancelled Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnQuoteRequestsCancelledListNone_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnQuoteRequestsCancelledListNone.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("QuoteRequestsCancelledList.html")
            Dim rep As New repQuoteRequestsCancelledList
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repQuoteRequestsCancelledList](@BRID, @EXID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@EXID", DBNull.Value)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repQuoteRequestsCancelledList)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Quote Requests Cancelled List"
            gui.Show()
        End If
    End Sub

    Private Sub btnQuoteRequestsCancelledList_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("QuoteRequestsCancelledList.html")
            Dim rep As New repQuoteRequestsCancelledList
            rep.Load()
            Dim ds As New dsReports
            Dim EXID As Integer = CType(e.Item.Tag, DataRowView)("EXID")
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repQuoteRequestsCancelledList](@BRID, @EXID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@EXID", EXID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repQuoteRequestsCancelledList)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Quote Requests Cancelled List"
            gui.Show()
        End If
    End Sub

    Private Sub btnSalespersonAmountOwingReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
        Dim DateGui As New frmDate
        If DateGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("SalespersonAmountOwingReport.html")
            Dim rep As New repRepEarnings
            rep.Load()
            Dim ds As New dsReports
            Dim EXID As Integer = CType(e.Item.Tag, DataRowView)("EXID")
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repRepEarnings](@BRID, @AS_OF) WHERE EXID = " & EXID, SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@AS_OF", DateGui.DateValue)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repRepEarnings)
            rep.SetDataSource(ds)
            rep.SetParameterValue("AsOfDate", CDate(cmd.Parameters("@AS_OF").Value))
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Sales Person Amount Owing Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnMeasurersReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnMeasurersReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("MeasurersReport.html")
            Dim rep As New repMeasurersReport
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repMeasurersReport](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repMeasurersReport)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Templaters / Measurers Report"
            gui.Show()
        End If
    End Sub

#End Region

#Region " Marketing "

    Private Sub btnPostCodesReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPostCodesReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("PostCodesReport.html")
            Dim rep As New repPostCodes
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repPostCodes](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repPostCodes)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "ZIP/Postal Codes Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnPrimaryEnquirySourcesReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrimaryEnquirySourcesReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("PrimaryEnquirySourcesReport.html")
            Dim rep As New repLeadSources
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repLeadSources](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repLeadSources)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Primary Enquiry Sources Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnHOPrimaryEnquirySourcesReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHOPrimaryEnquirySourcesReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("PrimaryEnquirySourcesReport.html")
            Dim rep As New repHOLeadSources
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repLeadSources](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", DBNull.Value)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repLeadSources)
            cmd.CommandText = "SELECT * FROM Branches WHERE BRIncludeInReporting = 1"
            ReadUncommittedFill(da, ds.Branches)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep, True)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Primary Enquiry Sources Report - Group Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnSecondaryEnquirySourcesReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnSecondaryEnquirySourcesReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("SeconadaryEnquirySourcesReport.html")
            Dim rep As New repLeadSubSources
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repLeadSubSources](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repLeadSubSources)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Secondary Enquiry Sources Report"
            gui.Show()
        End If
    End Sub

    Private Sub btnPrimaryEnquirySourcesPostCodesReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrimaryEnquirySourcesPostCodesReport.ItemClick
        Dim DateRangeGui As New frmDateRange(SqlConnection)
        If DateRangeGui.ShowDialog(Me) = DialogResult.OK Then
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Dim gui As New frmReport("PostCodesAndPrimaryEnquirySourcesReport.html")
            Dim rep As New repLeadSourcesPostCodes
            rep.Load()
            Dim ds As New dsReports
            Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repLeadSourcesPostCodes](@BRID, @FROM_DATE, @TO_DATE)", SqlConnection)
            cmd.Parameters.Add("@BRID", BRID)
            cmd.Parameters.Add("@FROM_DATE", DateRangeGui.FromDate)
            cmd.Parameters.Add("@TO_DATE", DateRangeGui.ToDate)
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            ReadUncommittedFill(da, ds.repLeadSourcesPostCodes)
            rep.SetDataSource(ds)
            rep.SetParameterValue("FromDate", DateRangeGui.FromDate)
            rep.SetParameterValue("ToDate", DateRangeGui.ToDate)
            gui.Display(rep)
            Me.Cursor = Windows.Forms.Cursors.Default
            gui.Text = "Primary Enquiry Sources - ZIP/Postal Codes Report"
            gui.Show()
        End If
    End Sub

#End Region

#End Region

#Region " Forms "

#Region " Static Forms "

    Private Sub btnJobFolderCoverStamp_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnJobFolderCoverStamp.ItemClick
        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        Dim gui As New frmReport(Nothing)
        Dim rep As New repStaticJobFolderCoverStamp
        rep.Load()
        gui.Display(rep)
        Me.Cursor = Windows.Forms.Cursors.Default
        gui.Text = "Example Job Folder Cover Stamp"
        gui.Show()
    End Sub

    Private Sub btnJobIssuesForm_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnJobIssuesForm.ItemClick
        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        Dim gui As New frmReport("JobIssuesForm.html")
        Dim rep As New repStaticJobIssues
        rep.Load()
        Dim ds As New dsHOReports
        Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[JobIssues]", SqlConnection)
        Dim da As New SqlClient.SqlDataAdapter(cmd)
        ReadUncommittedFill(da, ds.JobIssues)
        rep.SetDataSource(ds)
        gui.Display(rep)
        Me.Cursor = Windows.Forms.Cursors.Default
        gui.Text = "Job Issues Form"
        gui.Show()
    End Sub

    Private Sub btnNonStockedMaterialInJobForm_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnNonStockedMaterialInJobForm.ItemClick
        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        Dim gui As New frmReport(Nothing)
        Dim rep As New repStaticNonStockedMaterial
        rep.Load()
        Dim ds As New dsReports
        Dim cmd As New SqlClient.SqlCommand("SELECT * FROM dbo.[repStaticNonStockedMaterial](@BRID)", SqlConnection)
        cmd.Parameters.Add("@BRID", BRID)
        Dim da As New SqlClient.SqlDataAdapter(cmd)
        ReadUncommittedFill(da, ds.repStaticNonStockedMaterial)
        rep.SetDataSource(ds)
        gui.Display(rep)
        Me.Cursor = Windows.Forms.Cursors.Default
        gui.Text = "Non-Tracked Material in Job Form"
        gui.Show()
    End Sub

    Private Sub btnQuoteRequestCancellationForm_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnQuoteRequestCancellationForm.ItemClick
        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        Dim gui As New frmReport(Nothing)
        Dim rep As New repStaticQuoteRequestCancellationForm
        rep.Load()
        gui.Display(rep)
        Me.Cursor = Windows.Forms.Cursors.Default
        gui.Text = "Quote Request Cancellation Form"
        gui.Show()
    End Sub

    Private Sub btnClientConfirmationForm_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnClientConfirmationForm.ItemClick
        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        Dim gui As New frmReport(Nothing)
        Dim rep As New repStaticClientConfirmationForm
        rep.Load()
        gui.Display(rep)
        Me.Cursor = Windows.Forms.Cursors.Default
        gui.Text = "Customer Confirmation Form"
        gui.Show()
    End Sub

    Private Sub btnChangesToJobDetailsForm_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnChangesToJobDetailsForm.ItemClick
        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        Dim gui As New frmReport(Nothing)
        Dim rep As New repStaticChangesToJobDetailsForm
        rep.Load()
        gui.Display(rep)
        Me.Cursor = Windows.Forms.Cursors.Default
        gui.Text = "Changes To Job Details Form"
        gui.Show()
    End Sub

    Private Sub btnCareOfGranite_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnCareOfGranite.ItemClick
        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        Dim gui As New frmReport(Nothing)
        Dim rep As New repStaticCareOfGranite
        rep.Load()
        gui.Display(rep)
        Me.Cursor = Windows.Forms.Cursors.Default
        gui.Text = "Care of Benchtop Letter"
        gui.Show()
    End Sub

    Private Sub btnInstallerStatusReport_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnInstallerStatusReport.ItemClick
        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        Dim gui As New frmReport(Nothing)
        Dim rep As New repStaticInstallerStatusReport
        rep.Load()
        gui.Display(rep)
        Me.Cursor = Windows.Forms.Cursors.Default
        gui.Text = "Installer Status Report"
        gui.Show()
    End Sub

#End Region

#Region " Manual Forms "

    Private Sub btnManualQuoteRequestForm_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnManualQuoteRequestForm.ItemClick
        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        Dim gui As New frmReport(Nothing)
        Dim rep As New repStaticQuoteRequestForm
        rep.Load()
        gui.Display(rep)
        Me.Cursor = Windows.Forms.Cursors.Default
        gui.Text = "Manual Quote Request Form"
        gui.Show()
    End Sub

    Private Sub btnManualJobBookingForm_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnManualJobBookingForm.ItemClick
        Me.Cursor = Windows.Forms.Cursors.WaitCursor
        Dim gui As New frmReport(Nothing)
        Dim rep As New repStaticJobBookingForm
        rep.Load()
        gui.Display(rep)
        Me.Cursor = Windows.Forms.Cursors.Default
        gui.Text = "Manual Job Booking Form"
        gui.Show()
    End Sub

#End Region

#End Region

#Region " Options "

    Private Sub btnBranchProperties_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnBranchProperties.ItemClick
        LaunchBranchProperties()
    End Sub

    Public Sub LaunchBranchProperties()
        If TypeOf btnBranchProperties.Tag Is CommandAccess Then
            If Not CType(btnBranchProperties.Tag, CommandAccess).Allowed Then
                ShowMessage_AccessDenied()
                Exit Sub
            End If
        End If
        Dim c As Cursor = Me.Cursor
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        frmBranch2.Edit(BRID, Roles)
        Me.Cursor = c
    End Sub

    Private Sub btnBranchUsers_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnBranchUsers.ItemClick
        LaunchBranchUsers()
    End Sub

    Public Sub LaunchBranchUsers()
        If TypeOf btnBranchUsers.Tag Is CommandAccess Then
            If Not CType(btnBranchUsers.Tag, CommandAccess).Allowed Then
                ShowMessage_AccessDenied()
                Exit Sub
            End If
        End If
        Dim gui As New frmUsers(SqlConnection, BRID)
        gui.ShowDialog(Me)
    End Sub

#End Region

#Region " Help "

    Private Sub btnShowHelp_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnShowHelp.ItemClick
        Me.dpHelp.Show()
    End Sub

    Private Sub btnHelp_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHelp.ItemClick
        ShowHelp(Me)
    End Sub

    Private Sub btnAboutFranchiseManager_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnAboutFranchiseManager.ItemClick
        Dim gui As New frmAbout
        gui.ShowDialog(Me)
    End Sub

#End Region

#Region " Web "

    Private Sub btnBack_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
        GoBack()
    End Sub

    Private Sub btnForward_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
        GoForward()
    End Sub

    Private Sub btnUp_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
        GoUp()
    End Sub

#End Region

#End Region

#Region " Bar Functions "

    Private dynamicCommands As ArrayList = New ArrayList

    Public Sub RefreshDynamicCommands()
        ' --- EXPENSES ---
        DsGTMS.Tables(daExpenses.TableMappings(0).DataSetTable).Clear()
        daExpenses.SelectCommand.Parameters("@BRID").Value = BRID
        daExpenses.Fill(DsGTMS)
        ' --- MATERIALS ---
        DsGTMS.Tables(daMaterials.TableMappings(0).DataSetTable).Clear()
        daMaterials.SelectCommand.Parameters("@BRID").Value = BRID
        daMaterials.Fill(DsGTMS)
        ' --- TASKS ---
        DsGTMS.Tables(daTasks.TableMappings(0).DataSetTable).Clear()
        daTasks.Fill(DsGTMS)

        UnloadDynamicCommandsFromMenu()
        LoadDynamicCommandsIntoMenu()
    End Sub

    Private Sub LoadDynamicCommandsIntoMenu()
        ' Everybody in the Salesperson Costing Group
        For Each salesrep As DataRowView In Me.dvSalesRepCostingGroup
            dynamicCommands.Add(LoadNewDynamicCommandIntoMenu(salesrep, "EXName", subSalespersonAmountOwingReport, AddressOf btnSalespersonAmountOwingReport_ItemClick))
        Next

        ' Everybody in the Salesperson cost group who is also specified in jobs
        For Each salesrep As DataRowView In Me.dvSalesRepsInJobs
            dynamicCommands.Add(LoadNewDynamicCommandIntoMenu(salesrep, "EXName", subSalespersonJobList, AddressOf btnSalespersonJobList_ItemClick))
        Next

        ' Everybody who is specified as a Salesperson (role)
        For Each salesrep As DataRowView In Me.dvSalesReps
            dynamicCommands.Add(LoadNewDynamicCommandIntoMenu(salesrep, "EXName", subQuotesOutstandingReport, AddressOf btnQuotesOutstandingReport_ItemClick))
            dynamicCommands.Add(LoadNewDynamicCommandIntoMenu(salesrep, "EXName", subQuoteRequestsCancelledList, AddressOf btnQuoteRequestsCancelledList_ItemClick))
            dynamicCommands.Add(LoadNewDynamicCommandIntoMenu(salesrep, "EXName", subRepJobs, AddressOf btnRepJobs_ItemClick))
        Next

        ' Everybody who appears in the salesperson calendar
        For Each salesrep As DataRowView In Me.dvSalesRepsInCalendar
            dynamicCommands.Add(LoadNewDynamicCommandIntoMenu(salesrep, "EXName", subSalespersonsCalendars, AddressOf btnSalespersonsCalendars_ItemClick))
        Next

        ' Everybody in the Direct Labour costing group and specified in jobs
        For Each directlabour As DataRowView In Me.dvDirectLabourInJobs
            dynamicCommands.Add(LoadNewDynamicCommandIntoMenu(directlabour, "EXName", subDirectLabourJobList, AddressOf btnDirectLabourJobList_ItemClick))
        Next

        ' Everybody who appears in the direct labour calendar
        For Each directlabour As DataRowView In Me.dvDirectLabourInCalendar
            dynamicCommands.Add(LoadNewDynamicCommandIntoMenu(directlabour, "EXName", subJobBoardbyDirectLabour, AddressOf btnJobBoardbyDirectLabour_ItemClick))
        Next

        ' Staff Members
        For Each person As DataRowView In Me.dvStaffMembers
            dynamicCommands.Add(LoadNewDynamicCommandIntoMenu(person, "EXName", subFollowUpReport, AddressOf btnFollowUpReport_ItemClick))
        Next

        For Each material As DataRowView In Me.dvGranite
            dynamicCommands.Add(LoadNewDynamicCommandIntoMenu(material, "MTName", subTrendProductInJobReport, AddressOf btnGraniteinJobsReport_ItemClick))
        Next

        For Each task As DataRowView In Me.dvTasks
            dynamicCommands.Add(LoadNewDynamicCommandIntoMenu(task, "TAName", subJobBoardByTask, AddressOf btnJobBoardbyTask_ItemClick))
        Next

    End Sub

    Private Sub UnloadDynamicCommandsFromMenu()
        Do Until dynamicCommands.Count = 0
            Dim barItem As DevExpress.XtraBars.BarItem
            barItem = dynamicCommands(0)
            UnloadBarItem(barItem)
            dynamicCommands.Remove(barItem)
        Loop
    End Sub

    Private Function LoadNewDynamicCommandIntoMenu(ByVal dataRowView As DataRowView, ByVal captionField As String, _
            ByVal menu As DevExpress.XtraBars.BarSubItem, ByVal handler As DevExpress.XtraBars.ItemClickEventHandler) As DevExpress.XtraBars.BarItem
        Dim caption As String = IsNull(dataRowView(captionField), "")

        Dim btn As New DevExpress.XtraBars.BarButtonItem(menu.Manager, caption)
        menu.Manager.Items.Add(btn)
        btn.Tag = dataRowView
        menu.ItemLinks.Add(btn)
        AddHandler btn.ItemClick, handler

        Return btn
    End Function

    Private Sub LoadBarItemIntoSub(ByVal barItem As DevExpress.XtraBars.BarItem, ByVal subItem As DevExpress.XtraBars.BarSubItem, Optional ByVal beginGroup As Boolean = True)
        subItem.Manager.Items.Add(barItem)
        subItem.ItemLinks.Add(barItem).BeginGroup = beginGroup
    End Sub

    Private Sub UnloadBarItem(ByVal barItem As DevExpress.XtraBars.BarItem)
        Dim foundLink As Boolean = False
        Do
            foundLink = False
            For barCount As Integer = 0 To barItem.Manager.Bars.Count - 1
                Dim bar As DevExpress.XtraBars.Bar = barItem.Manager.Bars(barCount)
                For linkCount As Integer = 0 To bar.ItemLinks.Count - 1
                    Dim link As DevExpress.XtraBars.BarItemLink = bar.ItemLinks(linkCount)
                    If TypeOf link.Item Is DevExpress.XtraBars.BarSubItem Then
                        RemoveLinksFromSubItem(barItem, link.Item)
                    End If
                    If link.Item Is barItem Then
                        bar.ItemLinks.Remove(link)
                        foundLink = True
                        Exit For
                    End If
                Next
            Next
        Loop Until Not foundLink
        barItem.Manager.Items.Remove(barItem)
    End Sub

    Private Sub RemoveLinksFromSubItem(ByVal barItem As DevExpress.XtraBars.BarItem, ByVal subItem As DevExpress.XtraBars.BarSubItem)
        For linkCount As Integer = 0 To subItem.ItemLinks.Count - 1
            Dim link As DevExpress.XtraBars.BarItemLink = subItem.ItemLinks(linkCount)
            If TypeOf link.Item Is DevExpress.XtraBars.BarSubItem Then
                RemoveLinksFromSubItem(barItem, link.Item)
            End If
            If link.Item Is barItem Then
                subItem.ItemLinks.Remove(link)
                Exit For
            End If
        Next
    End Sub

#End Region

End Class
