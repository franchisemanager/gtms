Imports Power.Forms

Public Class lvMaterials
    Inherits System.Windows.Forms.UserControl
    Implements IListControl, IPrintableControl

    Private AllowAddRemove As Boolean

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Connection As SqlClient.SqlConnection, ByVal BRID As Integer, ByVal MGID As Integer, ByVal ExplorerForm As frmExplorer, Optional ByVal AllowAddRemove As Boolean = True)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.Connection = Connection
        Me.BRID = BRID
        Me.MGID = MGID
        hExplorerForm = ExplorerForm
        Me.AllowAddRemove = AllowAddRemove
        FillDataSet()
    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents hSqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents GridControl As DevExpress.XtraGrid.GridControl
    Friend WithEvents colMTName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMTDiscontinued As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMTCurrentPurchaseCost As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gvMaterials As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gvStockedItems As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents DsMaterialsGrid As WindowsApplication.dsMaterialsGrid
    Friend WithEvents colSIName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents daStockedItems As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents colSICurrentInventory As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMTStocked As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSICurrentValue As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSICurrentPurchaseCost As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSICurrentInventoryInPrimaryUOM As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents colMTCurrentWastePercentage As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode
        Me.gvStockedItems = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colSIName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSICurrentPurchaseCost = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSICurrentInventory = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSICurrentValue = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSICurrentInventoryInPrimaryUOM = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridControl = New DevExpress.XtraGrid.GridControl
        Me.DsMaterialsGrid = New WindowsApplication.dsMaterialsGrid
        Me.gvMaterials = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colMTName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colMTDiscontinued = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colMTStocked = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colMTCurrentPurchaseCost = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colMTCurrentWastePercentage = New DevExpress.XtraGrid.Columns.GridColumn
        Me.hSqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.daStockedItems = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        CType(Me.gvStockedItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsMaterialsGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvMaterials, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gvStockedItems
        '
        Me.gvStockedItems.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.gvStockedItems.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvStockedItems.Appearance.FocusedCell.Options.UseBackColor = True
        Me.gvStockedItems.Appearance.FocusedCell.Options.UseForeColor = True
        Me.gvStockedItems.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.gvStockedItems.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gvStockedItems.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvStockedItems.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvStockedItems.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSIName, Me.colSICurrentPurchaseCost, Me.colSICurrentInventory, Me.colSICurrentValue, Me.colSICurrentInventoryInPrimaryUOM})
        Me.gvStockedItems.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.gvStockedItems.GridControl = Me.GridControl
        Me.gvStockedItems.Name = "gvStockedItems"
        Me.gvStockedItems.OptionsBehavior.Editable = False
        Me.gvStockedItems.OptionsCustomization.AllowFilter = False
        Me.gvStockedItems.OptionsNavigation.AutoFocusNewRow = True
        Me.gvStockedItems.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.gvStockedItems.OptionsView.ShowFooter = True
        Me.gvStockedItems.OptionsView.ShowGroupPanel = False
        Me.gvStockedItems.OptionsView.ShowHorzLines = False
        Me.gvStockedItems.OptionsView.ShowIndicator = False
        Me.gvStockedItems.OptionsView.ShowVertLines = False
        Me.gvStockedItems.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colSIName, DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.gvStockedItems.ViewCaption = "Stocked Items"
        '
        'colSIName
        '
        Me.colSIName.Caption = "Stocked Item"
        Me.colSIName.FieldName = "SIName"
        Me.colSIName.Name = "colSIName"
        Me.colSIName.Visible = True
        Me.colSIName.VisibleIndex = 0
        Me.colSIName.Width = 359
        '
        'colSICurrentPurchaseCost
        '
        Me.colSICurrentPurchaseCost.Caption = "Cost (per Inventory Unit of Measure)"
        Me.colSICurrentPurchaseCost.DisplayFormat.FormatString = "c"
        Me.colSICurrentPurchaseCost.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colSICurrentPurchaseCost.FieldName = "SICurrentPurchaseCost"
        Me.colSICurrentPurchaseCost.Name = "colSICurrentPurchaseCost"
        Me.colSICurrentPurchaseCost.Visible = True
        Me.colSICurrentPurchaseCost.VisibleIndex = 1
        Me.colSICurrentPurchaseCost.Width = 115
        '
        'colSICurrentInventory
        '
        Me.colSICurrentInventory.Caption = "Inventory (Inventory Unit of Measure)"
        Me.colSICurrentInventory.DisplayFormat.FormatString = "#0 Sheet(s)"
        Me.colSICurrentInventory.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colSICurrentInventory.FieldName = "SICurrentInventory"
        Me.colSICurrentInventory.Name = "colSICurrentInventory"
        Me.colSICurrentInventory.SummaryItem.DisplayFormat = "Total: {0} Sheet(s)"
        Me.colSICurrentInventory.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.colSICurrentInventory.Visible = True
        Me.colSICurrentInventory.VisibleIndex = 2
        Me.colSICurrentInventory.Width = 120
        '
        'colSICurrentValue
        '
        Me.colSICurrentValue.Caption = "Current Value"
        Me.colSICurrentValue.DisplayFormat.FormatString = "c"
        Me.colSICurrentValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colSICurrentValue.FieldName = "SICurrentValue"
        Me.colSICurrentValue.Name = "colSICurrentValue"
        Me.colSICurrentValue.SummaryItem.DisplayFormat = "Total: {0:c}"
        Me.colSICurrentValue.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.colSICurrentValue.Visible = True
        Me.colSICurrentValue.VisibleIndex = 4
        Me.colSICurrentValue.Width = 139
        '
        'colSICurrentInventoryInPrimaryUOM
        '
        Me.colSICurrentInventoryInPrimaryUOM.Caption = "Inventory (Primary Unit of Measure)"
        Me.colSICurrentInventoryInPrimaryUOM.DisplayFormat.FormatString = "#0.000"
        Me.colSICurrentInventoryInPrimaryUOM.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colSICurrentInventoryInPrimaryUOM.FieldName = "SICurrentInventoryInPrimaryUOM"
        Me.colSICurrentInventoryInPrimaryUOM.Name = "colSICurrentInventoryInPrimaryUOM"
        Me.colSICurrentInventoryInPrimaryUOM.SummaryItem.DisplayFormat = "Total: {0}"
        Me.colSICurrentInventoryInPrimaryUOM.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        Me.colSICurrentInventoryInPrimaryUOM.Visible = True
        Me.colSICurrentInventoryInPrimaryUOM.VisibleIndex = 3
        Me.colSICurrentInventoryInPrimaryUOM.Width = 118
        '
        'GridControl
        '
        Me.GridControl.DataMember = "VMaterials_Display"
        Me.GridControl.DataSource = Me.DsMaterialsGrid
        Me.GridControl.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'GridControl.EmbeddedNavigator
        '
        Me.GridControl.EmbeddedNavigator.Name = ""
        GridLevelNode1.LevelTemplate = Me.gvStockedItems
        GridLevelNode1.RelationName = "VMaterials_Display_VStockedItems"
        Me.GridControl.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.GridControl.Location = New System.Drawing.Point(0, 0)
        Me.GridControl.MainView = Me.gvMaterials
        Me.GridControl.Name = "GridControl"
        Me.GridControl.Size = New System.Drawing.Size(632, 392)
        Me.GridControl.TabIndex = 0
        Me.GridControl.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvMaterials, Me.gvStockedItems})
        '
        'DsMaterialsGrid
        '
        Me.DsMaterialsGrid.DataSetName = "dsMaterialsGrid"
        Me.DsMaterialsGrid.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'gvMaterials
        '
        Me.gvMaterials.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.gvMaterials.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvMaterials.Appearance.FocusedCell.Options.UseBackColor = True
        Me.gvMaterials.Appearance.FocusedCell.Options.UseForeColor = True
        Me.gvMaterials.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.gvMaterials.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gvMaterials.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvMaterials.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvMaterials.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colMTName, Me.colMTDiscontinued, Me.colMTStocked, Me.colMTCurrentPurchaseCost, Me.colMTCurrentWastePercentage})
        Me.gvMaterials.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        Me.gvMaterials.GridControl = Me.GridControl
        Me.gvMaterials.Name = "gvMaterials"
        Me.gvMaterials.OptionsBehavior.Editable = False
        Me.gvMaterials.OptionsCustomization.AllowFilter = False
        Me.gvMaterials.OptionsNavigation.AutoFocusNewRow = True
        Me.gvMaterials.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.gvMaterials.OptionsView.ShowGroupPanel = False
        Me.gvMaterials.OptionsView.ShowHorzLines = False
        Me.gvMaterials.OptionsView.ShowIndicator = False
        Me.gvMaterials.OptionsView.ShowVertLines = False
        Me.gvMaterials.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colMTName, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colMTName
        '
        Me.colMTName.Caption = "Name"
        Me.colMTName.FieldName = "MTName"
        Me.colMTName.Name = "colMTName"
        Me.colMTName.Visible = True
        Me.colMTName.VisibleIndex = 0
        Me.colMTName.Width = 233
        '
        'colMTDiscontinued
        '
        Me.colMTDiscontinued.Caption = "Status"
        Me.colMTDiscontinued.FieldName = "MTDiscontinued_Display"
        Me.colMTDiscontinued.Name = "colMTDiscontinued"
        Me.colMTDiscontinued.OptionsColumn.ReadOnly = True
        Me.colMTDiscontinued.Visible = True
        Me.colMTDiscontinued.VisibleIndex = 1
        Me.colMTDiscontinued.Width = 135
        '
        'colMTStocked
        '
        Me.colMTStocked.Caption = "Stocked"
        Me.colMTStocked.FieldName = "MTStocked"
        Me.colMTStocked.Name = "colMTStocked"
        Me.colMTStocked.Width = 107
        '
        'colMTCurrentPurchaseCost
        '
        Me.colMTCurrentPurchaseCost.Caption = "Cost (per Unit of Measure)"
        Me.colMTCurrentPurchaseCost.DisplayFormat.FormatString = "c"
        Me.colMTCurrentPurchaseCost.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colMTCurrentPurchaseCost.FieldName = "MTCurrentPurchaseCost"
        Me.colMTCurrentPurchaseCost.Name = "colMTCurrentPurchaseCost"
        Me.colMTCurrentPurchaseCost.OptionsColumn.ReadOnly = True
        Me.colMTCurrentPurchaseCost.Visible = True
        Me.colMTCurrentPurchaseCost.VisibleIndex = 2
        Me.colMTCurrentPurchaseCost.Width = 132
        '
        'colMTCurrentWastePercentage
        '
        Me.colMTCurrentWastePercentage.Caption = "Waste % (actual)"
        Me.colMTCurrentWastePercentage.DisplayFormat.FormatString = "p"
        Me.colMTCurrentWastePercentage.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colMTCurrentWastePercentage.FieldName = "MTCurrentWastePercentage"
        Me.colMTCurrentWastePercentage.Name = "colMTCurrentWastePercentage"
        Me.colMTCurrentWastePercentage.Visible = True
        Me.colMTCurrentWastePercentage.VisibleIndex = 3
        Me.colMTCurrentWastePercentage.Width = 130
        '
        'hSqlConnection
        '
        Me.hSqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;integrated security=SSPI;data source=""SERVER" & _
        "\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VMaterials_Display", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("MTID", "MTID"), New System.Data.Common.DataColumnMapping("MGID", "MGID"), New System.Data.Common.DataColumnMapping("MTDiscontinued", "MTDiscontinued")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Materials WHERE (BRID = @Original_BRID) AND (MTID = @Original_MTID) A" & _
        "ND (MGID = @Original_MGID OR @Original_MGID IS NULL AND MGID IS NULL) AND (MTDis" & _
        "continued = @Original_MTDiscontinued OR @Original_MTDiscontinued IS NULL AND MTD" & _
        "iscontinued IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.hSqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTDiscontinued", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTDiscontinued", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Materials (BRID, MGID, MTDiscontinued) VALUES (@BRID, @MGID, @MTDisco" & _
        "ntinued); SELECT BRID, MTID, MGID, MTDiscontinued, MTName, MTDiscontinued_Displa" & _
        "y, MTStocked_Display, MTCurrentPurchaseCost, MTCurrentWastePercentage WHERE (BRI" & _
        "D = @BRID) AND (MTID = @@IDENTITY) ORDER BY MTID"
        Me.SqlInsertCommand1.Connection = Me.hSqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTDiscontinued", System.Data.SqlDbType.Bit, 1, "MTDiscontinued"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, MTID, MGID, MTDiscontinued, MTName, MTDiscontinued_Display, MTStocke" & _
        "d_Display, MTCurrentPurchaseCost, MTCurrentWastePercentage FROM VMaterials_Displ" & _
        "ay WHERE (MGID = @MGID) AND (BRID = @BRID) ORDER BY MTID"
        Me.SqlSelectCommand1.Connection = Me.hSqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Materials SET BRID = @BRID, MGID = @MGID, MTDiscontinued = @MTDiscontinued" & _
        " WHERE (BRID = @Original_BRID) AND (MTID = @Original_MTID) AND (MGID = @Original" & _
        "_MGID OR @Original_MGID IS NULL AND MGID IS NULL) AND (MTDiscontinued = @Origina" & _
        "l_MTDiscontinued OR @Original_MTDiscontinued IS NULL AND MTDiscontinued IS NULL)" & _
        "; SELECT BRID, MTID, MGID, MTDiscontinued, MTName, MTDiscontinued_Display, MTSto" & _
        "cked_Display, MTCurrentPurchaseCost, MTCurrentWastePercentage FROM VMaterials_Di" & _
        "splay WHERE (BRID = @BRID) AND (MTID = @MTID) ORDER BY MTID"
        Me.SqlUpdateCommand1.Connection = Me.hSqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTDiscontinued", System.Data.SqlDbType.Bit, 1, "MTDiscontinued"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTDiscontinued", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTDiscontinued", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        '
        'daStockedItems
        '
        Me.daStockedItems.DeleteCommand = Me.SqlDeleteCommand2
        Me.daStockedItems.InsertCommand = Me.SqlInsertCommand2
        Me.daStockedItems.SelectCommand = Me.SqlSelectCommand2
        Me.daStockedItems.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VStockedItems", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("SIID", "SIID"), New System.Data.Common.DataColumnMapping("MTID", "MTID")})})
        Me.daStockedItems.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM StockedItems WHERE (BRID = @Original_BRID) AND (SIID = @Original_SIID" & _
        ") AND (MTID = @Original_MTID)"
        Me.SqlDeleteCommand2.Connection = Me.hSqlConnection
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO StockedItems (BRID, MTID) VALUES (@BRID, @MTID); SELECT BRID, SIID, M" & _
        "TID, SIConversionToPrimary, MTPrimaryUOM, SIName, SIInventoryUOM, SICurrentInven" & _
        "tory, SICurrentValue, SICurrentPurchaseCost, SICurrentInventoryInPrimaryUOM FROM" & _
        " VStockedItems WHERE (BRID = @BRID) AND (SIID = @@IDENTITY)"
        Me.SqlInsertCommand2.Connection = Me.hSqlConnection
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT BRID, SIID, MTID, SIConversionToPrimary, MTPrimaryUOM, SIName, SIInventory" & _
        "UOM, SICurrentInventory, SICurrentValue, SICurrentPurchaseCost, SICurrentInvento" & _
        "ryInPrimaryUOM FROM VStockedItems WHERE (BRID = @BRID) AND ((SELECT MGID FROM Ma" & _
        "terials M WHERE M.BRID = VStockedItems.BRID AND M.MTID = VStockedItems.MTID) = @" & _
        "MGID)"
        Me.SqlSelectCommand2.Connection = Me.hSqlConnection
        Me.SqlSelectCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.Variant))
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE StockedItems SET BRID = @BRID, MTID = @MTID WHERE (BRID = @Original_BRID) " & _
        "AND (SIID = @Original_SIID) AND (MTID = @Original_MTID); SELECT BRID, SIID, MTID" & _
        ", SIConversionToPrimary, MTPrimaryUOM, SIName, SIInventoryUOM, SICurrentInventor" & _
        "y, SICurrentValue, SICurrentPurchaseCost, SICurrentInventoryInPrimaryUOM FROM VS" & _
        "tockedItems WHERE (BRID = @BRID) AND (SIID = @SIID)"
        Me.SqlUpdateCommand2.Connection = Me.hSqlConnection
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIID", System.Data.SqlDbType.Int, 4, "SIID"))
        '
        'lvMaterials
        '
        Me.Controls.Add(Me.GridControl)
        Me.Name = "lvMaterials"
        Me.Size = New System.Drawing.Size(632, 392)
        CType(Me.gvStockedItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsMaterialsGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvMaterials, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return hSqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            hSqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Public Function FillDataSet() As Integer Implements IListControl.FillDataSet
        ' Open connection
        Dim trans As SqlClient.SqlTransaction = Connection.BeginTransaction(IsolationLevel.ReadUncommitted)
        Power.Library.Library.ApplyTransactionToAllDataAdapters(trans, Me)
        'Me.GridControl1.DataSource.Clear()
        FillDataSet = SqlDataAdapter.Fill(DsMaterialsGrid)
        daStockedItems.Fill(DsMaterialsGrid)
        'Close connecton
        trans.Commit()
    End Function

    Private Sub GridView1_RowCountChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvMaterials.RowCountChanged
        ExplorerForm.UpdateNumItems(NumItems)
        ExplorerForm.UpdateVisibleItems(VisibleItems)
    End Sub

    Public ReadOnly Property VisibleItems() As Integer Implements Power.Library.IListControl.VisibleItems
        Get
            Return gvMaterials.RowCount
        End Get
    End Property

    Public ReadOnly Property NumItems() As Integer Implements Power.Library.IListControl.NumItems
        Get
            Return DsMaterialsGrid.Tables(SqlDataAdapter.TableMappings(0).DataSetTable).Rows.Count
        End Get
    End Property

    Private hBRID As Integer
    Private Property BRID() As Integer
        Get
            Return hBRID
        End Get
        Set(ByVal Value As Integer)
            hBRID = Value
            SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = Value
            daStockedItems.SelectCommand.Parameters("@BRID").Value = Value
        End Set
    End Property

    Private hMGID As Integer
    Private Property MGID() As Integer
        Get
            Return hMGID
        End Get
        Set(ByVal Value As Integer)
            hMGID = Value
            SqlDataAdapter.SelectCommand.Parameters("@MGID").Value = Value
            daStockedItems.SelectCommand.Parameters("@MGID").Value = Value
        End Set
    End Property

    Public ReadOnly Property SelectedRow() As DataRow
        Get
            If Not gvMaterials.GetSelectedRows Is Nothing Then
                Return gvMaterials.GetDataRow(gvMaterials.GetSelectedRows(0))
            End If
        End Get
    End Property

    Public ReadOnly Property SelectedRowField(ByVal Field As String) As Object
        Get
            If Not SelectedRow Is Nothing Then
                Return SelectedRow.Item(Field)
            Else
                Return DBNull.Value
            End If
        End Get
    End Property

    Public Function SelectRow(ByVal BRID As Int32, ByVal MTID As Int32) As Boolean
        Dim i As Integer = 0
        Do Until False
            Try
                If gvMaterials.GetDataRow(gvMaterials.GetVisibleRowHandle(i))("BRID") = BRID And _
                        gvMaterials.GetDataRow(gvMaterials.GetVisibleRowHandle(i))("MTID") = MTID Then
                    gvMaterials.ClearSelection()
                    gvMaterials.SelectRow(gvMaterials.GetVisibleRowHandle(i))
                    Return True
                End If
                i += 1
            Catch ex As NullReferenceException
                Return False
            End Try
        Loop
        Return False
    End Function

#Region " New, Open and Delete "

    Private Sub GridControl1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridControl.DoubleClick
        List_Edit()
    End Sub

    Public Sub List_New() Implements IListControl.List_New
        Dim c As Cursor = ParentForm.Cursor
        ParentForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            Dim gui As frmMaterialWizard = frmMaterialWizard.Add(BRID, MGID)
            gui.ShowDialog(Me)
            FillDataSet()
            SelectRow(BRID, gui.MTID)
            ExplorerForm.RefreshDynamicCommands()
        Catch ex As ObjectLockedException
            Message.CurrentlyAccessed("material")
        End Try
        ParentForm.Cursor = c
    End Sub

    Public Sub List_Edit() Implements IListControl.List_Edit
        If Not SelectedRow Is Nothing Then
            Dim c As Cursor = ParentForm.Cursor
            ParentForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If MaterialExists(SelectedRowField("BRID"), SelectedRowField("MTID")) Then
                Try
                    Dim gui As frmMaterial = frmMaterial.Edit(SelectedRowField("BRID"), SelectedRowField("MTID"))
                    gui.ShowDialog(Me)
                    FillDataSet()
                    ExplorerForm.RefreshDynamicCommands()
                Catch ex As ObjectLockedException
                    Message.CurrentlyAccessed("material")
                End Try
            Else
                Message.AlreadyDeleted("material", Message.ObjectAction.Edit)
                Me.GridControl.DataSource.Clear()
                FillDataSet()
            End If
            ParentForm.Cursor = c
        End If
    End Sub

    Public Sub List_Delete() Implements IListControl.List_Delete
        If Not SelectedRow Is Nothing Then
            Dim MTID As Long = SelectedRowField("MTID")
            'If gvMaterials.IsFocusedView Then
            If Message.AskDeleteObject("material", Message.ObjectAction.Discontinue) = MsgBoxResult.Yes Then
                If MaterialExists(SelectedRowField("BRID"), SelectedRowField("MTID")) Then
                    If DataAccess.spExecLockRequest("sp_GetMaterialLock", BRID, MTID, Connection) Then
                        SelectedRow.Item("MTDiscontinued") = True
                        SqlDataAdapter.Update(DsMaterialsGrid)
                        DataAccess.spExecLockRequest("sp_ReleaseMaterialLock", BRID, MTID, Connection)
                        ExplorerForm.RefreshDynamicCommands()
                    Else
                        Message.CurrentlyAccessed("material")
                    End If
                Else
                    Message.AlreadyDeleted("material", Message.ObjectAction.Discontinue)
                    Me.GridControl.DataSource.Clear()
                    FillDataSet()
                End If
            End If
        End If
    End Sub

    Private Function MaterialExists(ByVal BRID As Int32, ByVal MTID As Int64) As Boolean
        Dim cnn As SqlClient.SqlConnection = Connection
        Dim cmd As New SqlClient.SqlCommand("SELECT Count(MTID) FROM Materials WHERE BRID = @BRID AND MTID = @MTID", cnn)
        cmd.CommandType = CommandType.Text
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@MTID", MTID)
        Return cmd.ExecuteScalar > 0
    End Function

#End Region

    Private hExplorerForm As Form
    Public ReadOnly Property ExplorerForm() As frmExplorer
        Get
            Return hExplorerForm
        End Get
    End Property

    Public Sub Print(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.Print
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl
        link.PrintDlg()
    End Sub

    Public Sub PrintPreview(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.PrintPreview
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl
        link.ShowPreview()
    End Sub

    Public Sub QuickPrint(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.QuickPrint
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl
        link.Print(PrintingSystem.PageSettings.PrinterName)
    End Sub

    Public ReadOnly Property AllowDelete() As Boolean Implements Power.Library.IListControl.AllowDelete
        Get
            Return False
        End Get
    End Property

    Public ReadOnly Property AllowEdit() As Boolean Implements Power.Library.IListControl.AllowEdit
        Get
            Return True
        End Get
    End Property

    Public ReadOnly Property AllowNew() As Boolean Implements Power.Library.IListControl.AllowNew
        Get
            Return AllowAddRemove And Not HasRole("branch_read_only")
        End Get
    End Property

    Public ReadOnly Property AllowUndelete() As Boolean Implements Power.Library.IListControl.AllowUndelete
        Get
            Return False
        End Get
    End Property

    Public Sub List_Undelete() Implements Power.Library.IListControl.List_Undelete

    End Sub

    Public ReadOnly Property DeleteCaption() As String Implements Power.Library.IListControl.DeleteCaption
        Get
            Return "&Discontinue"
        End Get
    End Property

    Public ReadOnly Property UndeleteCaption() As String Implements Power.Library.IListControl.UndeleteCaption
        Get
            Return "&Continue"
        End Get
    End Property
End Class
