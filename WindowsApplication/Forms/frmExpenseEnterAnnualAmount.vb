Public Class frmExpenseEnterAnnualAmount
    Inherits DevExpress.XtraEditors.XtraForm

    Private ExpenseAllowanceDataRow As DataRow
    Public BRID As Int32
    Public EXID As Int32

    Private hTransaction As SqlClient.SqlTransaction
    Private Property Transaction() As SqlClient.SqlTransaction
        Get
            Return hTransaction
        End Get
        Set(ByVal Value As SqlClient.SqlTransaction)
            hTransaction = Value
            Power.Library.Library.ApplyTransactionToAllDataAdapters(Value, Me)
        End Set
    End Property

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal BRID As Integer, ByVal EXID As Integer, ByVal Transaction As System.Data.SqlClient.SqlTransaction)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.BRID = BRID
        Me.EXID = EXID
        Me.Transaction = Transaction
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DsWizards As WindowsApplication.dsWizards
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txtALAllowance As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txtALFromDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmExpenseEnterAnnualAmount))
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton
        Me.DsWizards = New WindowsApplication.dsWizards
        Me.Label28 = New System.Windows.Forms.Label
        Me.txtALAllowance = New DevExpress.XtraEditors.TextEdit
        Me.Label27 = New System.Windows.Forms.Label
        Me.txtALFromDate = New DevExpress.XtraEditors.DateEdit
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        CType(Me.DsWizards, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtALAllowance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtALFromDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(296, 248)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 8
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(376, 248)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 9
        Me.btnCancel.Text = "Cancel"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton1.Image = CType(resources.GetObject("SimpleButton1.Image"), System.Drawing.Image)
        Me.SimpleButton1.Location = New System.Drawing.Point(8, 248)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(72, 23)
        Me.SimpleButton1.TabIndex = 7
        Me.SimpleButton1.Text = "Help"
        '
        'DsWizards
        '
        Me.DsWizards.DataSetName = "dsWizards"
        Me.DsWizards.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'Label28
        '
        Me.Label28.Location = New System.Drawing.Point(80, 208)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(96, 21)
        Me.Label28.TabIndex = 5
        Me.Label28.Text = "Annual amount:"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtALAllowance
        '
        Me.txtALAllowance.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsWizards, "ExpenseAllowances.ALAllowance"))
        Me.txtALAllowance.Location = New System.Drawing.Point(176, 208)
        Me.txtALAllowance.Name = "txtALAllowance"
        '
        'txtALAllowance.Properties
        '
        Me.txtALAllowance.Properties.Appearance.Options.UseTextOptions = True
        Me.txtALAllowance.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtALAllowance.Properties.DisplayFormat.FormatString = "c"
        Me.txtALAllowance.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtALAllowance.Properties.EditFormat.FormatString = "c"
        Me.txtALAllowance.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtALAllowance.Size = New System.Drawing.Size(176, 20)
        Me.txtALAllowance.TabIndex = 6
        '
        'Label27
        '
        Me.Label27.Location = New System.Drawing.Point(80, 176)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(96, 21)
        Me.Label27.TabIndex = 3
        Me.Label27.Text = "Start date:"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtALFromDate
        '
        Me.txtALFromDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsWizards, "ExpenseAllowances.ALFromDate"))
        Me.txtALFromDate.EditValue = Nothing
        Me.txtALFromDate.Location = New System.Drawing.Point(176, 176)
        Me.txtALFromDate.Name = "txtALFromDate"
        '
        'txtALFromDate.Properties
        '
        Me.txtALFromDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtALFromDate.Size = New System.Drawing.Size(176, 20)
        Me.txtALFromDate.TabIndex = 4
        '
        'Label26
        '
        Me.Label26.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label26.Location = New System.Drawing.Point(8, 120)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(448, 32)
        Me.Label26.TabIndex = 2
        Me.Label26.Text = "If the annual amount changes, this can be altered in the Amount Specified Annuall" & _
        "y list in the accounting section of the program."
        '
        'Label25
        '
        Me.Label25.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label25.Location = New System.Drawing.Point(8, 48)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(448, 64)
        Me.Label25.TabIndex = 1
        Me.Label25.Text = "Note: The annual amount must be the amount the expense WOULD BE PAID if the expen" & _
        "se spanned the entire finiancial year.  If the expense was $40 000 over a full f" & _
        "inancial year, but the start date is half way through the financial year, the pr" & _
        "ogram will only assign $20 000 for this finacial year."
        '
        'Label24
        '
        Me.Label24.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label24.Location = New System.Drawing.Point(8, 8)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(448, 32)
        Me.Label24.TabIndex = 0
        Me.Label24.Text = "You must set up an annual amount for this expense.  Select the date the expense b" & _
        "egan and a total annual amount."
        '
        'frmExpenseEnterAnnualAmount
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(458, 280)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.txtALAllowance)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.txtALFromDate)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.SimpleButton1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExpenseEnterAnnualAmount"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Enter Annual Amount"
        CType(Me.DsWizards, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtALAllowance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtALFromDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmExpenseEndAnnualAmount_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ExpenseAllowanceDataRow = DsWizards.ExpenseAllowances.NewExpenseAllowancesRow
        ExpenseAllowanceDataRow("BRID") = BRID
        ExpenseAllowanceDataRow("EXID") = EXID
        DsWizards.ExpenseAllowances.AddExpenseAllowancesRow(ExpenseAllowanceDataRow)
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If ValidateForm() Then
            sp_InsertExpenseAllowance(BRID, EXID, ExpenseAllowanceDataRow("ALFromDate"), ExpenseAllowanceDataRow("ALAllowance"), ExpenseAllowanceDataRow("ALHours"), Transaction)

            OK = True
            Me.Close()
        End If
    End Sub

    Private Function ValidateForm() As Boolean
        If ExpenseAllowanceDataRow("ALFromDate") Is DBNull.Value Then
            DevExpress.XtraEditors.XtraMessageBox.Show("You must enter a start date.", "Franchise Manager")
            Return False
        End If
        If ExpenseAllowanceDataRow("ALAllowance") Is DBNull.Value Then
            DevExpress.XtraEditors.XtraMessageBox.Show("You must enter an annual amount.", "Franchise Manager")
            Return False
        End If
        Return True
    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        End If
    End Sub

End Class

