Public Class frmMemo
    Inherits DevExpress.XtraEditors.XtraForm

    Public Shared Function Edit(ByVal text As String, ByVal title As String) As String
        Dim gui As New frmMemo

        With gui
            .Text = title
            .txtMemo.EditValue = text

            If gui.ShowDialog = .DialogResult.OK Then
                Return .txtMemo.EditValue
            Else
                Return text
            End If
        End With
    End Function

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents btnHelp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmHistory As System.Windows.Forms.ContextMenu
    Friend WithEvents miClearHistory As System.Windows.Forms.MenuItem
    Friend WithEvents dataSet As WindowsApplication.dsNotes
    Friend WithEvents dvExpenses As System.Data.DataView
    Friend WithEvents txtMemo As DevExpress.XtraEditors.MemoEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMemo))
        Me.dataSet = New WindowsApplication.dsNotes
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.cmHistory = New System.Windows.Forms.ContextMenu
        Me.miClearHistory = New System.Windows.Forms.MenuItem
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.btnHelp = New DevExpress.XtraEditors.SimpleButton
        Me.dvExpenses = New System.Data.DataView
        Me.txtMemo = New DevExpress.XtraEditors.MemoEdit
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvExpenses, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMemo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dataSet
        '
        Me.dataSet.DataSetName = "dsNotes"
        Me.dataSet.EnforceConstraints = False
        Me.dataSet.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=WORKSTATION3;packet size=4096;integrated security=SSPI;data source" & _
        "=""server\dev"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;integrated security=SSPI;data source=SERVER;" & _
        "persist security info=False;initial catalog=GTMS_DEV"
        '
        'cmHistory
        '
        Me.cmHistory.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.miClearHistory})
        '
        'miClearHistory
        '
        Me.miClearHistory.Index = 0
        Me.miClearHistory.Text = "Clear History"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(448, 368)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(368, 368)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "OK"
        '
        'btnHelp
        '
        Me.btnHelp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnHelp.Image = CType(resources.GetObject("btnHelp.Image"), System.Drawing.Image)
        Me.btnHelp.Location = New System.Drawing.Point(8, 368)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(72, 24)
        Me.btnHelp.TabIndex = 1
        Me.btnHelp.Text = "Help"
        '
        'dvExpenses
        '
        Me.dvExpenses.RowFilter = "EGType <> 'OE'"
        Me.dvExpenses.Sort = "EXName"
        Me.dvExpenses.Table = Me.dataSet.VExpenses
        '
        'txtMemo
        '
        Me.txtMemo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMemo.Location = New System.Drawing.Point(8, 8)
        Me.txtMemo.Name = "txtMemo"
        Me.txtMemo.Size = New System.Drawing.Size(512, 352)
        Me.txtMemo.TabIndex = 0
        '
        'frmMemo
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(528, 398)
        Me.Controls.Add(Me.txtMemo)
        Me.Controls.Add(Me.btnHelp)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMemo"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Memo Edit"
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvExpenses, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMemo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

End Class
