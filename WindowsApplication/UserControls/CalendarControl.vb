Imports System.ComponentModel

Public Class CalendarControl
    Inherits System.Windows.Forms.UserControl
    Implements IPrintableControl

    Private Const DEFAULT_SORT As String = "EGName, EXName"

    Public Const DEFAULT_WORK_START_HOUR As Integer = 8
    Public Const DEFAULT_WORK_END_HOUR As Integer = 17

    ' These variables are used to store the primary key if and only if this calendar is within
    ' a lead or booking
    Private UseHightlight As Boolean = False
    Private BRID As Integer
    Private APType As String
    Private APTypeID As Long

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Calendar.Views.DayView.WorkTime.Start = New TimeSpan(DEFAULT_WORK_START_HOUR, 0, 0)
        Calendar.Views.DayView.WorkTime.End = New TimeSpan(DEFAULT_WORK_END_HOUR, 0, 0)
    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ToolTipController As DevExpress.Utils.ToolTipController
    Friend WithEvents Storage As DevExpress.XtraScheduler.SchedulerStorage
    Friend WithEvents DsCalendar As WindowsApplication.dsCalendar
    Friend WithEvents dvExpenses As System.Data.DataView
    Friend WithEvents dvAppointments As System.Data.DataView
    Friend WithEvents Calendar As DevExpress.XtraScheduler.SchedulerControl
    Friend WithEvents ResourcesCheckedListBoxControl1 As DevExpress.XtraScheduler.UI.ResourcesCheckedListBoxControl
    Friend WithEvents DateNavigator1 As DevExpress.XtraScheduler.DateNavigator
    Friend WithEvents ResourcesComboBoxControl1 As DevExpress.XtraScheduler.UI.ResourcesComboBoxControl
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim TimeRuler1 As DevExpress.XtraScheduler.TimeRuler = New DevExpress.XtraScheduler.TimeRuler
        Dim TimeRuler2 As DevExpress.XtraScheduler.TimeRuler = New DevExpress.XtraScheduler.TimeRuler
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl
        Me.Calendar = New DevExpress.XtraScheduler.SchedulerControl
        Me.Storage = New DevExpress.XtraScheduler.SchedulerStorage(Me.components)
        Me.dvAppointments = New System.Data.DataView
        Me.DsCalendar = New WindowsApplication.dsCalendar
        Me.dvExpenses = New System.Data.DataView
        Me.ToolTipController = New DevExpress.Utils.ToolTipController(Me.components)
        Me.ResourcesCheckedListBoxControl1 = New DevExpress.XtraScheduler.UI.ResourcesCheckedListBoxControl
        Me.ResourcesComboBoxControl1 = New DevExpress.XtraScheduler.UI.ResourcesComboBoxControl
        Me.Label1 = New System.Windows.Forms.Label
        Me.DateNavigator1 = New DevExpress.XtraScheduler.DateNavigator
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.Calendar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Storage, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvAppointments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsCalendar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvExpenses, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ResourcesCheckedListBoxControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ResourcesComboBoxControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.Calendar)
        Me.SplitContainerControl1.Panel1.Text = "SplitContainerControl1_Panel1"
        Me.SplitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.ResourcesCheckedListBoxControl1)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.ResourcesComboBoxControl1)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.Label1)
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.DateNavigator1)
        Me.SplitContainerControl1.Panel2.Text = "SplitContainerControl1_Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(704, 424)
        Me.SplitContainerControl1.SplitterPosition = 207
        Me.SplitContainerControl1.TabIndex = 13
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'Calendar
        '
        Me.Calendar.Controls.Add(Me.Calendar.ResourceNavigator)
        Me.Calendar.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Calendar.GroupType = DevExpress.XtraScheduler.SchedulerGroupType.Date
        Me.Calendar.Location = New System.Drawing.Point(0, 0)
        Me.Calendar.Name = "Calendar"
        Me.Calendar.OptionsCustomization.AllowAppointmentConflicts = DevExpress.XtraScheduler.AppointmentConflictsMode.Forbidden
        Me.Calendar.OptionsCustomization.AllowAppointmentCopy = DevExpress.XtraScheduler.UsedAppointmentType.None
        Me.Calendar.OptionsCustomization.AllowAppointmentMultiSelect = False
        Me.Calendar.OptionsCustomization.AllowInplaceEditor = DevExpress.XtraScheduler.UsedAppointmentType.None
        Me.Calendar.OptionsView.ToolTipVisibility = DevExpress.XtraScheduler.ToolTipVisibility.Always
        '
        'Calendar.ResourceNavigator
        '
        Me.Calendar.ResourceNavigator.Name = ""
        Me.Calendar.Size = New System.Drawing.Size(491, 424)
        Me.Calendar.Start = New Date(2006, 3, 2, 0, 0, 0, 0)
        Me.Calendar.Storage = Me.Storage
        Me.Calendar.TabIndex = 0
        Me.Calendar.Text = "SchedulerControl1"
        Me.Calendar.ToolTipController = Me.ToolTipController
        Me.Calendar.Views.DayView.TimeRulers.AddRange(New DevExpress.XtraScheduler.TimeRuler() {TimeRuler1})
        Me.Calendar.Views.DayView.VisibleTime.Duration = System.TimeSpan.Parse("1.00:00:00")
        Me.Calendar.Views.DayView.WorkTime.Duration = System.TimeSpan.Parse("09:00:00")
        Me.Calendar.Views.DayView.WorkTime.End = System.TimeSpan.Parse("17:00:00")
        Me.Calendar.Views.DayView.WorkTime.Start = System.TimeSpan.Parse("08:00:00")
        Me.Calendar.Views.WorkWeekView.TimeRulers.AddRange(New DevExpress.XtraScheduler.TimeRuler() {TimeRuler2})
        Me.Calendar.Views.WorkWeekView.VisibleTime.Duration = System.TimeSpan.Parse("1.00:00:00")
        Me.Calendar.Views.WorkWeekView.WorkTime.Duration = System.TimeSpan.Parse("09:00:00")
        Me.Calendar.Views.WorkWeekView.WorkTime.End = System.TimeSpan.Parse("17:00:00")
        Me.Calendar.Views.WorkWeekView.WorkTime.Start = System.TimeSpan.Parse("08:00:00")
        '
        'Storage
        '
        Me.Storage.Appointments.DataSource = Me.dvAppointments
        Me.Storage.Appointments.Mappings.AllDay = "APAllDay"
        Me.Storage.Appointments.Mappings.End = "APEnd"
        Me.Storage.Appointments.Mappings.Label = "APTask"
        Me.Storage.Appointments.Mappings.Location = "APSuburb"
        Me.Storage.Appointments.Mappings.ResourceId = "EXID"
        Me.Storage.Appointments.Mappings.Start = "APBegin"
        Me.Storage.Appointments.Mappings.Status = "APStatus"
        Me.Storage.Appointments.Mappings.Subject = "APDescription"
        Me.Storage.Appointments.Statuses.Add(New DevExpress.XtraScheduler.AppointmentStatus(DevExpress.XtraScheduler.AppointmentStatusType.Free, "Free", "&Free"))
        Me.Storage.Appointments.Statuses.Add(New DevExpress.XtraScheduler.AppointmentStatus(DevExpress.XtraScheduler.AppointmentStatusType.Tentative, System.Drawing.Color.FromArgb(CType(74, Byte), CType(135, Byte), CType(226, Byte)), "Tentative", "&Tentative"))
        Me.Storage.Appointments.Statuses.Add(New DevExpress.XtraScheduler.AppointmentStatus(DevExpress.XtraScheduler.AppointmentStatusType.Busy, System.Drawing.Color.FromArgb(CType(74, Byte), CType(135, Byte), CType(226, Byte)), "Busy", "&Busy"))
        Me.Storage.Appointments.Statuses.Add(New DevExpress.XtraScheduler.AppointmentStatus(DevExpress.XtraScheduler.AppointmentStatusType.OutOfOffice, System.Drawing.Color.FromArgb(CType(217, Byte), CType(83, Byte), CType(83, Byte)), "Off Work", "&Off Work"))
        Me.Storage.Resources.DataSource = Me.dvExpenses
        Me.Storage.Resources.Mappings.Caption = "EXCalendarName"
        Me.Storage.Resources.Mappings.Id = "EXID"
        '
        'dvAppointments
        '
        Me.dvAppointments.Table = Me.DsCalendar.VAppointments
        '
        'DsCalendar
        '
        Me.DsCalendar.DataSetName = "dsCalendar"
        Me.DsCalendar.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'dvExpenses
        '
        Me.dvExpenses.Sort = "EGName, EXName"
        Me.dvExpenses.Table = Me.DsCalendar.VExpenses
        '
        'ToolTipController
        '
        Me.ToolTipController.AutoPopDelay = 20000
        Me.ToolTipController.Rounded = True
        Me.ToolTipController.ShowBeak = True
        '
        'ResourcesCheckedListBoxControl1
        '
        Me.ResourcesCheckedListBoxControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ResourcesCheckedListBoxControl1.Location = New System.Drawing.Point(0, 185)
        Me.ResourcesCheckedListBoxControl1.Name = "ResourcesCheckedListBoxControl1"
        Me.ResourcesCheckedListBoxControl1.SchedulerControl = Me.Calendar
        Me.ResourcesCheckedListBoxControl1.Size = New System.Drawing.Size(207, 239)
        Me.ResourcesCheckedListBoxControl1.TabIndex = 12
        '
        'ResourcesComboBoxControl1
        '
        Me.ResourcesComboBoxControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.ResourcesComboBoxControl1.Location = New System.Drawing.Point(0, 165)
        Me.ResourcesComboBoxControl1.Name = "ResourcesComboBoxControl1"
        '
        'ResourcesComboBoxControl1.Properties
        '
        Me.ResourcesComboBoxControl1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ResourcesComboBoxControl1.SchedulerControl = Me.Calendar
        Me.ResourcesComboBoxControl1.ShowNoneResourcesItem = False
        Me.ResourcesComboBoxControl1.Size = New System.Drawing.Size(207, 20)
        Me.ResourcesComboBoxControl1.TabIndex = 15
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.Window
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label1.Location = New System.Drawing.Point(0, 144)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(207, 21)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Selectss your staff member(s):"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'DateNavigator1
        '
        Me.DateNavigator1.Dock = System.Windows.Forms.DockStyle.Top
        Me.DateNavigator1.Location = New System.Drawing.Point(0, 0)
        Me.DateNavigator1.Name = "DateNavigator1"
        Me.DateNavigator1.SchedulerControl = Me.Calendar
        Me.DateNavigator1.ShowTodayButton = False
        Me.DateNavigator1.Size = New System.Drawing.Size(207, 144)
        Me.DateNavigator1.TabIndex = 14
        '
        'CalendarControl
        '
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Name = "CalendarControl"
        Me.Size = New System.Drawing.Size(704, 424)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.Calendar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Storage, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvAppointments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsCalendar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvExpenses, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ResourcesCheckedListBoxControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ResourcesComboBoxControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public Property DataSet() As DsCalendar
        Get
            Return DsCalendar
        End Get
        Set(ByVal Value As DsCalendar)
            DsCalendar = Value
            SetViewDataSet(dvExpenses, Value)
            SetViewDataSet(dvAppointments, Value)
            RefreshTasks(Value.Tasks)
        End Set
    End Property

    Private Sub SetViewDataSet(ByVal view As DataView, ByVal dataSet As dataSet)
        Dim RowFilter As String = view.RowFilter
        Dim Sort As String = view.Sort
        view.Table = dataSet.Tables(view.Table.TableName)
        view.RowFilter = RowFilter
        view.Sort = Sort
    End Sub

    Public Sub SetHighlight(ByVal BRID As Integer, ByVal APType As String, ByVal APTypeID As Long) ' This will highlight (bold etc) the current job or lead
        UseHightlight = True
        Me.BRID = BRID
        Me.APType = APType
        Me.APTypeID = APTypeID
        UpdateHighlight()
        Calendar.Refresh()
    End Sub

    Public Sub ClearHighligh()
        UseHightlight = False
        UpdateHighlight()
    End Sub

    Public Sub UpdateHighlight()
        For Each app As DevExpress.XtraScheduler.Appointment In Storage.Appointments.Items
            If app.GetRow(Storage)("BRID") = BRID And app.GetRow(Storage)("APType") = APType And app.GetRow(Storage)("APTypeID") = APTypeID Then
                app.Appearance.Font = New System.Drawing.Font(app.Appearance.Font, FontStyle.Bold)
            Else
                app.Appearance.Font = New System.Drawing.Font(app.Appearance.Font, FontStyle.Regular)
            End If
        Next
    End Sub

    Public Sub ApplyEGTypeFilter(ByVal CalendarColumn As String)
        dvExpenses.RowFilter = CalendarColumn & " = 1"
        dvExpenses.Sort = DEFAULT_SORT
    End Sub

    Public Sub ApplyDefaultFilter()
        dvExpenses.RowFilter = ""
        dvExpenses.Sort = DEFAULT_SORT
    End Sub

    Public Sub SelectResource(ByVal EXID As Integer)
        For i As Integer = 0 To ResourcesCheckedListBoxControl1.Items.Count - 1
            If CType(ResourcesCheckedListBoxControl1.Items(i).Value, DevExpress.XtraScheduler.UI.ResourceCheckedListBoxItem).Resource.Id = EXID Then
                ResourcesCheckedListBoxControl1.Items(i).CheckState = CheckState.Checked
            Else
                ResourcesCheckedListBoxControl1.Items(i).CheckState = CheckState.Unchecked
            End If
        Next
    End Sub

    Public Sub RefreshTasks(ByVal DataSource As DsCalendar.TasksDataTable)
        Storage.Appointments.Labels.Clear()
        For Each task As DataRow In DataSource.Rows
            Storage.Appointments.Labels.Add(System.Drawing.Color.FromArgb(task("TAColourR"), task("TAColourG"), task("TAColourB")), _
                task("TAName"), task("TAMenuCaption"))
        Next
    End Sub

    Private Sub Calendar_EditAppointmentFormShowing(ByVal sender As System.Object, ByVal e As DevExpress.XtraScheduler.AppointmentFormEventArgs) _
    Handles Calendar.EditAppointmentFormShowing
        If SelectedAppointment Is Nothing Then
            If e.Appointment.AllDay Then
                RaiseEvent NewAllDayEvent(sender, New System.EventArgs)
            Else
                RaiseEvent NewAppointment(sender, New System.EventArgs)
            End If
        Else
            RaiseEvent EditAppointment(sender, New System.EventArgs)
        End If
        e.Handled = True
    End Sub

    Public Event NewAppointment(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Event NewAllDayEvent(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Event EditAppointment(ByVal sender As Object, ByVal e As System.EventArgs)
    Private Sub Calendar_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles Calendar.DoubleClick
        Exit Sub
        If SelectedAppointment Is Nothing Then
            'RaiseEvent NewAppointment(sender, e)
        Else
            'RaiseEvent EditAppointment(sender, e)
        End If
    End Sub

    Private Sub Calendar_InitAppointmentDisplayText(ByVal sender As System.Object, ByVal e As DevExpress.XtraScheduler.AppointmentDisplayTextEventArgs) _
    Handles Calendar.InitAppointmentDisplayText
        Dim apt As DevExpress.XtraScheduler.Appointment = e.Appointment
        Dim datarow As DataRowView = apt.GetRow(Me.Storage)
        If Not datarow Is Nothing Then
            Select Case datarow("APType")
                Case "LD"
                    e.Text = IsNull(datarow("ID"), "") & ": " & IsNull(datarow("APJobDescription"), "") & _
                        " (" & IsNull(datarow("APSuburb"), "") & ")"
                    e.Description = IsNull(datarow("APClientName"), "") & vbNewLine & _
                        "Address: " & IsNull(datarow("APAddressMultiline"), "") & vbNewLine & _
                        "Home Ph: " & IsNull(datarow("APClientHomePhoneNumber"), "") & vbNewLine & _
                        "Work Ph: " & IsNull(datarow("APClientWorkPhoneNumber"), "") & vbNewLine & _
                        "Mob: " & IsNull(datarow("APClientMobileNumber"), "") & vbNewLine & _
                        "Notes: " & IsNull(datarow("APNotes"), "")
                Case "JB"
                    e.Text = IsNull(datarow("ID"), "") & ": " & IsNull(datarow("APClientName"), "") & _
                        " (" & IsNull(datarow("APSuburb"), "") & ")"
                    e.Description = "Job desc: " & IsNull(datarow("APJobDescription"), "") & vbNewLine & _
                        "Address: " & IsNull(datarow("APAddressMultiline"), "") & vbNewLine & _
                        "Home Ph: " & IsNull(datarow("APClientHomePhoneNumber"), "") & vbNewLine & _
                        "Work Ph: " & IsNull(datarow("APClientWorkPhoneNumber"), "") & vbNewLine & _
                        "Mob: " & IsNull(datarow("APClientMobileNumber"), "") & vbNewLine & _
                        "Notes: " & IsNull(datarow("APNotes"), "")
                Case "NO"
                    e.Text = IsNull(datarow("APNotes"), "")
            End Select
        End If
    End Sub

    Private Sub ToolTipController_BeforeShow(ByVal sender As Object, ByVal e As DevExpress.Utils.ToolTipControllerShowEventArgs) Handles ToolTipController.BeforeShow
        Dim controller As DevExpress.Utils.ToolTipController = CType(sender, DevExpress.Utils.ToolTipController)
        If Not TypeOf controller.ActiveObject Is DevExpress.XtraScheduler.Appointment Then
            Return
        End If
        Dim apt As DevExpress.XtraScheduler.Appointment = CType(controller.ActiveObject, DevExpress.XtraScheduler.Appointment)
        Dim datarow As DataRowView = apt.GetRow(Storage)
        If Not datarow Is Nothing Then
            Select Case datarow("APType")
                Case "LD"
                    e.ToolTip = IsNull(datarow("ID"), "") & ": " & IsNull(datarow("APClientName"), "") & vbNewLine & _
                        "Job desc: " & IsNull(datarow("APJobDescription"), "") & vbNewLine & _
                        "Address: " & IsNull(datarow("APAddressMultiline"), "") & vbNewLine & _
                        "Home Ph: " & IsNull(datarow("APClientHomePhoneNumber"), "") & vbNewLine & _
                        "Work Ph: " & IsNull(datarow("APClientWorkPhoneNumber"), "") & vbNewLine & _
                        "Mob: " & IsNull(datarow("APClientMobileNumber"), "") & vbNewLine & _
                        "Notes: " & IsNull(datarow("APNotes"), "") ' apt.Start.ToString("g") & "..." & apt.End.ToString("g") & vbNewLine & _
                Case "JB"
                    Dim PriceString As String
                    If datarow("JBPriceQuoted") Is DBNull.Value Then
                        PriceString = "No estimated job price entered."
                    Else
                        PriceString = CType(datarow("JBPriceQuoted"), Double).ToString("c")
                    End If
                    e.ToolTip = IsNull(datarow("ID"), "") & ": " & IsNull(datarow("APClientName"), "") & vbNewLine & _
                        "Job desc: " & IsNull(datarow("APJobDescription"), "") & vbNewLine & _
                        "Address: " & IsNull(datarow("APAddressMultiline"), "") & vbNewLine & _
                        "Home Ph: " & IsNull(datarow("APClientHomePhoneNumber"), "") & vbNewLine & _
                        "Work Ph: " & IsNull(datarow("APClientWorkPhoneNumber"), "") & vbNewLine & _
                        "Mob: " & IsNull(datarow("APClientMobileNumber"), "") & vbNewLine & _
                        "Notes: " & IsNull(datarow("APNotes"), "") & vbNewLine & _
                        "Estimated job price: " & PriceString ' apt.Start.ToString("g") & "..." & apt.End.ToString("g") & vbNewLine & _
                Case "NO"
                    e.ToolTip = IsNull(datarow("APNotes"), "") ' apt.Start.ToString("g") & "..." & apt.End.ToString("g") & vbNewLine & _
            End Select
        End If
    End Sub

    Public ReadOnly Property SelectedAppointment() As DataRow
        Get
            If Calendar.SelectedAppointments.Count > 0 Then
                Return CType(Calendar.SelectedAppointments(0).GetRow(Calendar.Storage), DataRowView).Row
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property SelectedResource() As DataRowView
        Get
            Return Calendar.SelectedResource.GetRow(Calendar.Storage)
        End Get
    End Property

    Public Event AppointmentChanging(ByVal sender As Object, ByVal e As DevExpress.XtraScheduler.PersistentObjectEventArgs)
    Private Sub Storage_AppointmentChanging(ByVal sender As Object, ByVal e As DevExpress.XtraScheduler.PersistentObjectCancelEventArgs) Handles Storage.AppointmentChanging
        RaiseEvent AppointmentChanging(sender, e)
    End Sub

    Public Event AppointmentsChanged(ByVal sender As Object, ByVal e As DevExpress.XtraScheduler.PersistentObjectsEventArgs)
    Private Sub Storage_AppointmentsChanged(ByVal sender As Object, ByVal e As DevExpress.XtraScheduler.PersistentObjectsEventArgs) Handles Storage.AppointmentsChanged
        RaiseEvent AppointmentsChanged(sender, e)
    End Sub

    Public Event AppointmentsDeleted(ByVal sender As Object, ByVal e As DevExpress.XtraScheduler.PersistentObjectsEventArgs)
    Private Sub Storage_AppointmentsDeleted(ByVal sender As Object, ByVal e As DevExpress.XtraScheduler.PersistentObjectsEventArgs) Handles Storage.AppointmentsDeleted
        RaiseEvent AppointmentsDeleted(sender, e)
    End Sub

    Public Event CalendarDragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs)
    Private Sub Calendar_DragDrop(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Calendar.DragDrop
        RaiseEvent CalendarDragDrop(sender, e)
    End Sub

    Public Sub Print(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.Print
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = Calendar
        link.PrintDlg()
    End Sub

    Public Sub PrintPreview(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.PrintPreview
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = Calendar
        link.ShowPreview()
    End Sub

    Public Sub QuickPrint(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.QuickPrint
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = Calendar
        link.Print(PrintingSystem.PageSettings.PrinterName)
    End Sub

    Private Sub CalendarControl_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.ResourcesCheckedListBoxControl1.SchedulerControl = Nothing
        Me.ResourcesCheckedListBoxControl1.SchedulerControl = Calendar
        Me.ResourcesComboBoxControl1.SchedulerControl = Nothing
        Me.ResourcesComboBoxControl1.SchedulerControl = Calendar
        ResourcesComboBoxControl1.SelectedIndex = 1
        ResourcesComboBoxControl1.SelectedIndex = 0

    End Sub

End Class
