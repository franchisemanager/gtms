Imports DevExpress.XtraReports.UI

Public Class repGenericContactsMailout
    Inherits DevExpress.XtraReports.UI.XtraReport

#Region " Component Designer generated code "

    Public Sub New(Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Designer support
        Container.Add(me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Private WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.
    'Do not modify it using the code editor.
    Friend WithEvents pbLogo As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents dataSet As WindowsApplication.dsReportsBranchContactsMailMerge
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrRichTextBox2 As DevExpress.XtraReports.UI.XRRichTextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(repGenericContactsMailout))
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.XrRichTextBox2 = New DevExpress.XtraReports.UI.XRRichTextBox
        Me.dataSet = New WindowsApplication.dsReportsBranchContactsMailMerge
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.pbLogo = New DevExpress.XtraReports.UI.XRPictureBox
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichTextBox2, Me.XrLabel8, Me.XrLabel7, Me.XrPageInfo1})
        Me.Detail.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.Detail.Height = 458
        Me.Detail.Name = "Detail"
        Me.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
        Me.Detail.ParentStyleUsing.UseFont = False
        '
        'XrRichTextBox2
        '
        Me.XrRichTextBox2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrRichTextBox2.Location = New System.Drawing.Point(0, 150)
        Me.XrRichTextBox2.Name = "XrRichTextBox2"
        Me.XrRichTextBox2.ParentStyleUsing.UseBackColor = False
        Me.XrRichTextBox2.ParentStyleUsing.UseFont = False
        Me.XrRichTextBox2.RtfText = CType(resources.GetObject("XrRichTextBox2.RtfText"), DevExpress.XtraReports.UI.SerializableString)
        Me.XrRichTextBox2.Size = New System.Drawing.Size(567, 300)
        '
        'dataSet
        '
        Me.dataSet.DataSetName = "dsReportsBranchContactsMailMerge"
        Me.dataSet.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'XrLabel8
        '
        Me.XrLabel8.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.dataSet, "Contacts Mail Merge.Contact Address (Multiline)", "")})
        Me.XrLabel8.Location = New System.Drawing.Point(0, 67)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Size = New System.Drawing.Size(342, 50)
        Me.XrLabel8.Text = "XrLabel8"
        '
        'XrLabel7
        '
        Me.XrLabel7.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.dataSet, "Contacts Mail Merge.Contact Name", "")})
        Me.XrLabel7.Location = New System.Drawing.Point(0, 50)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Size = New System.Drawing.Size(342, 17)
        Me.XrLabel7.Text = "XrLabel7"
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Location = New System.Drawing.Point(0, 17)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.XrPageInfo1.Size = New System.Drawing.Size(342, 33)
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel6, Me.XrLabel9, Me.XrLabel5, Me.XrLabel3, Me.XrLabel4, Me.XrLabel14, Me.XrLabel2, Me.XrLabel1, Me.pbLogo})
        Me.PageHeader.Height = 83
        Me.PageHeader.Name = "PageHeader"
        '
        'XrLabel6
        '
        Me.XrLabel6.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.dataSet, "Contacts Mail Merge.Branch Email", "")})
        Me.XrLabel6.ForeColor = System.Drawing.Color.DimGray
        Me.XrLabel6.Location = New System.Drawing.Point(50, 58)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.ParentStyleUsing.UseForeColor = False
        Me.XrLabel6.Size = New System.Drawing.Size(308, 17)
        Me.XrLabel6.Text = "XrLabel6"
        '
        'XrLabel9
        '
        Me.XrLabel9.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.XrLabel9.ForeColor = System.Drawing.Color.DimGray
        Me.XrLabel9.Location = New System.Drawing.Point(0, 58)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.ParentStyleUsing.UseFont = False
        Me.XrLabel9.ParentStyleUsing.UseForeColor = False
        Me.XrLabel9.Size = New System.Drawing.Size(50, 16)
        Me.XrLabel9.Text = "Email:"
        '
        'XrLabel5
        '
        Me.XrLabel5.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.dataSet, "Contacts Mail Merge.Branch Fax", "")})
        Me.XrLabel5.ForeColor = System.Drawing.Color.DimGray
        Me.XrLabel5.Location = New System.Drawing.Point(183, 42)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.ParentStyleUsing.UseForeColor = False
        Me.XrLabel5.Size = New System.Drawing.Size(175, 16)
        Me.XrLabel5.Text = "XrLabel5"
        '
        'XrLabel3
        '
        Me.XrLabel3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.dataSet, "Contacts Mail Merge.Branch Phone", "")})
        Me.XrLabel3.ForeColor = System.Drawing.Color.DimGray
        Me.XrLabel3.Location = New System.Drawing.Point(50, 42)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.ParentStyleUsing.UseForeColor = False
        Me.XrLabel3.Size = New System.Drawing.Size(100, 16)
        Me.XrLabel3.Text = "XrLabel3"
        '
        'XrLabel4
        '
        Me.XrLabel4.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.XrLabel4.ForeColor = System.Drawing.Color.DimGray
        Me.XrLabel4.Location = New System.Drawing.Point(150, 42)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.ParentStyleUsing.UseFont = False
        Me.XrLabel4.ParentStyleUsing.UseForeColor = False
        Me.XrLabel4.Size = New System.Drawing.Size(33, 16)
        Me.XrLabel4.Text = "Fax:"
        '
        'XrLabel14
        '
        Me.XrLabel14.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.XrLabel14.ForeColor = System.Drawing.Color.DimGray
        Me.XrLabel14.Location = New System.Drawing.Point(0, 42)
        Me.XrLabel14.Name = "XrLabel14"
        Me.XrLabel14.ParentStyleUsing.UseFont = False
        Me.XrLabel14.ParentStyleUsing.UseForeColor = False
        Me.XrLabel14.Size = New System.Drawing.Size(50, 16)
        Me.XrLabel14.Text = "Phone:"
        '
        'XrLabel2
        '
        Me.XrLabel2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.dataSet, "Contacts Mail Merge.Branch Address", "")})
        Me.XrLabel2.ForeColor = System.Drawing.Color.DimGray
        Me.XrLabel2.Location = New System.Drawing.Point(0, 25)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.ParentStyleUsing.UseForeColor = False
        Me.XrLabel2.Size = New System.Drawing.Size(358, 17)
        Me.XrLabel2.Text = "XrLabel2"
        '
        'XrLabel1
        '
        Me.XrLabel1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Me.dataSet, "Contacts Mail Merge.Branch Business Name", "")})
        Me.XrLabel1.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel1.Location = New System.Drawing.Point(0, 0)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.ParentStyleUsing.UseFont = False
        Me.XrLabel1.Size = New System.Drawing.Size(358, 25)
        Me.XrLabel1.Text = "XrLabel1"
        '
        'pbLogo
        '
        Me.pbLogo.Image = CType(resources.GetObject("pbLogo.Image"), System.Drawing.Image)
        Me.pbLogo.Location = New System.Drawing.Point(358, 0)
        Me.pbLogo.Name = "pbLogo"
        Me.pbLogo.Size = New System.Drawing.Size(208, 67)

        '
        'repGenericContactsMailout
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader})
        Me.DataMember = "Contacts Mail Merge"
        Me.DataSource = Me.dataSet
        Me.Margins = New System.Drawing.Printing.Margins(130, 130, 50, 100)
        Me.PageHeight = 1169
        Me.PageWidth = 827
        Me.PaperKind = System.Drawing.Printing.PaperKind.A4
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

End Class

