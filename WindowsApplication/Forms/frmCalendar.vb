Public Class frmCalendar
    Inherits DevExpress.XtraEditors.XtraForm

    Private BRID As Int32
    Private APType As String
    Private APTypeID As Long

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return SqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            SqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private hTransaction As SqlClient.SqlTransaction
    Private Property Transaction() As SqlClient.SqlTransaction
        Get
            Return hTransaction
        End Get
        Set(ByVal Value As SqlClient.SqlTransaction)
            hTransaction = Value
            Power.Library.Library.ApplyTransactionToAllDataAdapters(Value, Me)
        End Set
    End Property

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal BRID As Int32, Optional ByVal APType As String = Nothing, Optional ByVal APTypeID As Long = Nothing, Optional ByVal Transaction As SqlClient.SqlTransaction = Nothing)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.BRID = BRID
        Me.APType = APType
        Me.APTypeID = APTypeID
        Me.Transaction = Transaction
        Calendar.Calendar.Start = Today.Date
        Select Case APType
            Case "LD"
                Calendar.ApplyEGTypeFilter("EXAppearInCalendarRC")
            Case "JB"
                Calendar.ApplyEGTypeFilter("EXAppearInCalendarDL")
        End Select
        FillData()
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnAddAppointment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEditAppointment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveAppointment As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents daExpenses As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daAppointments As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents daTasks As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Calendar As WindowsApplication.CalendarControl
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCalendar))
        Me.btnAddAppointment = New DevExpress.XtraEditors.SimpleButton
        Me.btnEditAppointment = New DevExpress.XtraEditors.SimpleButton
        Me.btnRemoveAppointment = New DevExpress.XtraEditors.SimpleButton
        Me.btnClose = New DevExpress.XtraEditors.SimpleButton
        Me.Calendar = New WindowsApplication.CalendarControl
        Me.daExpenses = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.daAppointments = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.daTasks = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SuspendLayout()
        '
        'btnAddAppointment
        '
        Me.btnAddAppointment.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddAppointment.Image = CType(resources.GetObject("btnAddAppointment.Image"), System.Drawing.Image)
        Me.btnAddAppointment.Location = New System.Drawing.Point(8, 640)
        Me.btnAddAppointment.Name = "btnAddAppointment"
        Me.btnAddAppointment.Size = New System.Drawing.Size(72, 23)
        Me.btnAddAppointment.TabIndex = 1
        Me.btnAddAppointment.Text = "Add..."
        '
        'btnEditAppointment
        '
        Me.btnEditAppointment.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEditAppointment.Image = CType(resources.GetObject("btnEditAppointment.Image"), System.Drawing.Image)
        Me.btnEditAppointment.Location = New System.Drawing.Point(88, 640)
        Me.btnEditAppointment.Name = "btnEditAppointment"
        Me.btnEditAppointment.Size = New System.Drawing.Size(72, 23)
        Me.btnEditAppointment.TabIndex = 2
        Me.btnEditAppointment.Text = "Edit..."
        '
        'btnRemoveAppointment
        '
        Me.btnRemoveAppointment.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveAppointment.Image = CType(resources.GetObject("btnRemoveAppointment.Image"), System.Drawing.Image)
        Me.btnRemoveAppointment.Location = New System.Drawing.Point(168, 640)
        Me.btnRemoveAppointment.Name = "btnRemoveAppointment"
        Me.btnRemoveAppointment.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveAppointment.TabIndex = 3
        Me.btnRemoveAppointment.Text = "Remove"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(896, 640)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(72, 23)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "Close"
        '
        'Calendar
        '
        Me.Calendar.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Calendar.Location = New System.Drawing.Point(8, 8)
        Me.Calendar.Name = "Calendar"
        Me.Calendar.Size = New System.Drawing.Size(960, 624)
        Me.Calendar.TabIndex = 0
        '
        'daExpenses
        '
        Me.daExpenses.DeleteCommand = Me.SqlDeleteCommand3
        Me.daExpenses.InsertCommand = Me.SqlInsertCommand3
        Me.daExpenses.SelectCommand = Me.SqlSelectCommand3
        Me.daExpenses.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VExpenses", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("EXName", "EXName")})})
        Me.daExpenses.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM Expenses WHERE (BRID = @Original_BRID) AND (EXID = @Original_EXID) AN" & _
        "D (EXName = @Original_EXName OR @Original_EXName IS NULL AND EXName IS NULL)"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXName", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;integrated security=SSPI;data source=""SERVER" & _
        "\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO Expenses (BRID, EXName) VALUES (@BRID, @EXName); SELECT BRID, EXID, E" & _
        "XName, EGType, EXAppearInCalendar, EXCalendarName, EGName FROM VExpenses WHERE (" & _
        "BRID = @BRID) AND (EXID = @@IDENTITY)"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT BRID, EXID, EXName, EGType, EXAppearInCalendar, EXCalendarName, EGName FRO" & _
        "M VExpenses WHERE (EXDiscontinued = 0) AND (BRID = @BRID)"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection
        Me.SqlSelectCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE Expenses SET BRID = @BRID, EXName = @EXName WHERE (BRID = @Original_BRID) " & _
        "AND (EXID = @Original_EXID) AND (EXName = @Original_EXName OR @Original_EXName I" & _
        "S NULL AND EXName IS NULL); SELECT BRID, EXID, EXName, EGType, EXAppearInCalenda" & _
        "r, EXCalendarName, EGName FROM VExpenses WHERE (BRID = @BRID) AND (EXID = @EXID)" & _
        ""
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXName", System.Data.SqlDbType.VarChar, 50, "EXName"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        '
        'daAppointments
        '
        Me.daAppointments.DeleteCommand = Me.SqlDeleteCommand2
        Me.daAppointments.InsertCommand = Me.SqlInsertCommand2
        Me.daAppointments.SelectCommand = Me.SqlSelectCommand2
        Me.daAppointments.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VAppointments", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("APID", "APID"), New System.Data.Common.DataColumnMapping("APBegin", "APBegin"), New System.Data.Common.DataColumnMapping("APEnd", "APEnd"), New System.Data.Common.DataColumnMapping("EXID", "EXID"), New System.Data.Common.DataColumnMapping("APType", "APType"), New System.Data.Common.DataColumnMapping("APTypeID", "APTypeID"), New System.Data.Common.DataColumnMapping("APNotes", "APNotes"), New System.Data.Common.DataColumnMapping("APStatus", "APStatus"), New System.Data.Common.DataColumnMapping("APTask", "APTask"), New System.Data.Common.DataColumnMapping("APAllDay", "APAllDay")})})
        Me.daAppointments.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM Appointments WHERE (APID = @Original_APID) AND (BRID = @Original_BRID" & _
        ") AND (APAllDay = @Original_APAllDay OR @Original_APAllDay IS NULL AND APAllDay " & _
        "IS NULL) AND (APBegin = @Original_APBegin OR @Original_APBegin IS NULL AND APBeg" & _
        "in IS NULL) AND (APEnd = @Original_APEnd OR @Original_APEnd IS NULL AND APEnd IS" & _
        " NULL) AND (APNotes = @Original_APNotes OR @Original_APNotes IS NULL AND APNotes" & _
        " IS NULL) AND (APStatus = @Original_APStatus OR @Original_APStatus IS NULL AND A" & _
        "PStatus IS NULL) AND (APTask = @Original_APTask OR @Original_APTask IS NULL AND " & _
        "APTask IS NULL) AND (APType = @Original_APType OR @Original_APType IS NULL AND A" & _
        "PType IS NULL) AND (APTypeID = @Original_APTypeID OR @Original_APTypeID IS NULL " & _
        "AND APTypeID IS NULL) AND (EXID = @Original_EXID OR @Original_EXID IS NULL AND E" & _
        "XID IS NULL)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APAllDay", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APAllDay", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APBegin", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APBegin", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APEnd", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APEnd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APNotes", System.Data.SqlDbType.VarChar, 2000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APStatus", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APStatus", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APTask", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APTask", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APTypeID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APTypeID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO Appointments (BRID, APBegin, APEnd, EXID, APType, APTypeID, APNotes, " & _
        "APStatus, APTask, APAllDay) VALUES (@BRID, @APBegin, @APEnd, @EXID, @APType, @AP" & _
        "TypeID, @APNotes, @APStatus, @APTask, @APAllDay); SELECT BRID, APID, APBegin, AP" & _
        "End, EXID, APType, APTypeID, APNotes, APStatus, APTask, APDescription, APAddress" & _
        "Multiline, ID, APClientName, EXName, APJobDescription, APClientPhoneNumber, APCl" & _
        "ientMobileNumber, APSuburb, APAllDay, JBPriceQuoted FROM VAppointments WHERE (AP" & _
        "ID = @@IDENTITY) AND (BRID = @BRID)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APBegin", System.Data.SqlDbType.DateTime, 8, "APBegin"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APEnd", System.Data.SqlDbType.DateTime, 8, "APEnd"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APType", System.Data.SqlDbType.VarChar, 2, "APType"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTypeID", System.Data.SqlDbType.BigInt, 8, "APTypeID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APNotes", System.Data.SqlDbType.VarChar, 2000, "APNotes"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APStatus", System.Data.SqlDbType.Int, 4, "APStatus"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTask", System.Data.SqlDbType.Int, 4, "APTask"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APAllDay", System.Data.SqlDbType.Bit, 1, "APAllDay"))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT BRID, APID, APBegin, APEnd, EXID, APType, APTypeID, APNotes, APStatus, APT" & _
        "ask, APDescription, APAddressMultiline, ID, APClientName, EXName, APJobDescripti" & _
        "on, APClientPhoneNumber, APClientMobileNumber, APSuburb, APAllDay, JBPriceQuoted" & _
        " FROM VAppointments WHERE (BRID = @BRID) AND (APIsCancelled = 0)"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection
        Me.SqlSelectCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE Appointments SET BRID = @BRID, APBegin = @APBegin, APEnd = @APEnd, EXID = " & _
        "@EXID, APType = @APType, APTypeID = @APTypeID, APNotes = @APNotes, APStatus = @A" & _
        "PStatus, APTask = @APTask, APAllDay = @APAllDay WHERE (APID = @Original_APID) AN" & _
        "D (BRID = @Original_BRID) AND (APAllDay = @Original_APAllDay OR @Original_APAllD" & _
        "ay IS NULL AND APAllDay IS NULL) AND (APBegin = @Original_APBegin OR @Original_A" & _
        "PBegin IS NULL AND APBegin IS NULL) AND (APEnd = @Original_APEnd OR @Original_AP" & _
        "End IS NULL AND APEnd IS NULL) AND (APNotes = @Original_APNotes OR @Original_APN" & _
        "otes IS NULL AND APNotes IS NULL) AND (APStatus = @Original_APStatus OR @Origina" & _
        "l_APStatus IS NULL AND APStatus IS NULL) AND (APTask = @Original_APTask OR @Orig" & _
        "inal_APTask IS NULL AND APTask IS NULL) AND (APType = @Original_APType OR @Origi" & _
        "nal_APType IS NULL AND APType IS NULL) AND (APTypeID = @Original_APTypeID OR @Or" & _
        "iginal_APTypeID IS NULL AND APTypeID IS NULL) AND (EXID = @Original_EXID OR @Ori" & _
        "ginal_EXID IS NULL AND EXID IS NULL); SELECT BRID, APID, APBegin, APEnd, EXID, A" & _
        "PType, APTypeID, APNotes, APStatus, APTask, APDescription, APAddressMultiline, I" & _
        "D, APClientName, EXName, APJobDescription, APClientPhoneNumber, APClientMobileNu" & _
        "mber, APSuburb, APAllDay, JBPriceQuoted FROM VAppointments WHERE (APID = @APID) " & _
        "AND (BRID = @BRID)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APBegin", System.Data.SqlDbType.DateTime, 8, "APBegin"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APEnd", System.Data.SqlDbType.DateTime, 8, "APEnd"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EXID", System.Data.SqlDbType.Int, 4, "EXID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APType", System.Data.SqlDbType.VarChar, 2, "APType"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTypeID", System.Data.SqlDbType.BigInt, 8, "APTypeID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APNotes", System.Data.SqlDbType.VarChar, 2000, "APNotes"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APStatus", System.Data.SqlDbType.Int, 4, "APStatus"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APTask", System.Data.SqlDbType.Int, 4, "APTask"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APAllDay", System.Data.SqlDbType.Bit, 1, "APAllDay"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APAllDay", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APAllDay", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APBegin", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APBegin", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APEnd", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APEnd", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APNotes", System.Data.SqlDbType.VarChar, 2000, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APNotes", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APStatus", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APStatus", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APTask", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APTask", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_APTypeID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "APTypeID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_EXID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "EXID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APID", System.Data.SqlDbType.BigInt, 8, "APID"))
        '
        'daTasks
        '
        Me.daTasks.InsertCommand = Me.SqlInsertCommand1
        Me.daTasks.SelectCommand = Me.SqlSelectCommand1
        Me.daTasks.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Tasks", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("TAName", "TAName"), New System.Data.Common.DataColumnMapping("TAControlsJobDate", "TAControlsJobDate"), New System.Data.Common.DataColumnMapping("TAMenuCaption", "TAMenuCaption"), New System.Data.Common.DataColumnMapping("TAColourR", "TAColourR"), New System.Data.Common.DataColumnMapping("TAColourG", "TAColourG"), New System.Data.Common.DataColumnMapping("TAColourB", "TAColourB"), New System.Data.Common.DataColumnMapping("TAID", "TAID")})})
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Tasks(TAName, TAControlsJobDate, TAMenuCaption, TAColourR, TAColourG," & _
        " TAColourB, TAID) VALUES (@TAName, @TAControlsJobDate, @TAMenuCaption, @TAColour" & _
        "R, @TAColourG, @TAColourB, @TAID); SELECT TAName, TAControlsJobDate, TAMenuCapti" & _
        "on, TAColourR, TAColourG, TAColourB, TAID FROM Tasks ORDER BY TAID"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAName", System.Data.SqlDbType.VarChar, 50, "TAName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAControlsJobDate", System.Data.SqlDbType.Bit, 1, "TAControlsJobDate"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAMenuCaption", System.Data.SqlDbType.VarChar, 50, "TAMenuCaption"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourR", System.Data.SqlDbType.SmallInt, 2, "TAColourR"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourG", System.Data.SqlDbType.SmallInt, 2, "TAColourG"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAColourB", System.Data.SqlDbType.SmallInt, 2, "TAColourB"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TAID", System.Data.SqlDbType.Int, 4, "TAID"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT TAName, TAControlsJobDate, TAMenuCaption, TAColourR, TAColourG, TAColourB," & _
        " TAID FROM Tasks ORDER BY TAID"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        '
        'frmCalendar
        '
        Me.AcceptButton = Me.btnClose
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(976, 670)
        Me.Controls.Add(Me.Calendar)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnAddAppointment)
        Me.Controls.Add(Me.btnEditAppointment)
        Me.Controls.Add(Me.btnRemoveAppointment)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmCalendar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Calendar"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnAddAppointment.Enabled = Not HasRole("branch_read_only")
        btnRemoveAppointment.Enabled = Not HasRole("branch_read_only")
    End Sub

    ' This data must be loaded AFTER the DataRow is loaded (eg record related data)
    Private Sub FillData()
        ' --- SALES REPS ---
        daExpenses.SelectCommand.Parameters("@BRID").Value = BRID
        daExpenses.Fill(Calendar.DsCalendar.VExpenses)
        ' --- TASKS ---
        daTasks.Fill(Calendar.DsCalendar)
        Calendar.RefreshTasks(Calendar.DsCalendar.Tasks)
        ' --- APPOINTMENTS ---
        daAppointments.SelectCommand.Parameters("@BRID").Value = BRID
        Calendar.DsCalendar.VAppointments.Clear()
        daAppointments.Fill(Calendar.DsCalendar.VAppointments)
        Calendar.SetHighlight(BRID, APType, APTypeID)
    End Sub

    Private Sub RefreshAppointments()
        ' --- APPOINTMENTS ---
        daAppointments.SelectCommand.Parameters("@BRID").Value = BRID
        Calendar.DsCalendar.VAppointments.Clear()
        daAppointments.Fill(Calendar.DsCalendar.VAppointments)
        Calendar.SetHighlight(BRID, APType, APTypeID)
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#Region " Appointments "

    Private Sub Calendar_NewAllDayEvent(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar.NewAllDayEvent
        If HasRole("branch_read_only") Then Exit Sub
        Dim c As Cursor = Me.Cursor
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim gui As frmAppointment = frmAppointment.Add(Calendar.Storage, BRID, APType, APTypeID, Transaction, 1)
        AddAppointmentLock(gui.APID)
        If Not Calendar.Calendar.SelectedInterval.Start < Calendar.Calendar.Start Then
            gui.APBegin = Calendar.Calendar.SelectedInterval.Start
            If Calendar.Calendar.SelectedInterval.Start = Calendar.Calendar.SelectedInterval.End Then
                gui.APEnd = DateAdd(DateInterval.Day, 1, Calendar.Calendar.SelectedInterval.Start)
            Else
                gui.APEnd = Calendar.Calendar.SelectedInterval.End
            End If
        End If
        gui.APAllDay = True
        If Not Calendar.SelectedResource Is Nothing Then
            gui.EXID = Calendar.SelectedResource("EXID")
        End If
        If APType = "JB" Then
            gui.APTask = 1
        End If
        gui.ShowDialog(Me)
        RefreshAppointments()
        Me.Cursor = c
    End Sub

    Private Sub Calendar_NewAppointment(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar.NewAppointment
        If HasRole("branch_read_only") Then Exit Sub
        Dim c As Cursor = Me.Cursor
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim gui As frmAppointment = frmAppointment.Add(Calendar.Storage, BRID, APType, APTypeID, Transaction, 1)
        AddAppointmentLock(gui.APID)
        If Not Calendar.Calendar.SelectedInterval.Start < Calendar.Calendar.Start Then
            gui.APBegin = Calendar.Calendar.SelectedInterval.Start
            If Calendar.Calendar.SelectedInterval.Start = Calendar.Calendar.SelectedInterval.End Then
                gui.APEnd = DateAdd(DateInterval.Hour, 1, Calendar.Calendar.SelectedInterval.Start)
            Else
                gui.APEnd = Calendar.Calendar.SelectedInterval.End
            End If
        End If
        If Not Calendar.SelectedResource Is Nothing Then
            gui.EXID = Calendar.SelectedResource("EXID")
        End If
        If APType = "JB" Then
            gui.APTask = 1
        End If
        gui.ShowDialog(Me)
        RefreshAppointments()
        Me.Cursor = c
    End Sub

    Private Sub btnAddAppointment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAppointment.Click
        If Calendar.Calendar.SelectedInterval.Start.Hour = 0 And Calendar.Calendar.SelectedInterval.Start.Minute = 0 And _
                Calendar.Calendar.SelectedInterval.End.Hour = 0 And Calendar.Calendar.SelectedInterval.End.Minute = 0 And _
                Calendar.Calendar.SelectedInterval.Start <> Calendar.Calendar.SelectedInterval.End Then
            Calendar_NewAllDayEvent(sender, e)
        Else
            Calendar_NewAppointment(sender, e)
        End If
    End Sub

    Private Sub Calendar_EditAppointment(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar.EditAppointment
        If Not Calendar.SelectedAppointment Is Nothing Then
            If Not (Calendar.SelectedAppointment.Item("BRID") = Me.BRID And Calendar.SelectedAppointment.Item("APType") = APType And Calendar.SelectedAppointment.Item("APTypeID") = APTypeID) Then
                If Message.AppointmentNotCurrentObject("job", Message.ObjectAction.Edit) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If
            Dim c As Cursor = Me.Cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If AddAppointmentLock(Calendar.SelectedAppointment.Item("APID")) Then
                Dim gui As frmAppointment = frmAppointment.Edit(Calendar.Storage, Calendar.SelectedAppointment.Item("BRID"), Calendar.SelectedAppointment.Item("APID"), Transaction, 1)
                gui.ShowDialog(Me)
                RefreshAppointments()
            Else
                Message.CurrentlyAccessed("appointment")
            End If
            Me.Cursor = c
        End If
    End Sub

    Private Sub btnEditAppointment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditAppointment.Click
        Calendar_EditAppointment(sender, e)
    End Sub

    Private Sub btnRemoveAppointment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveAppointment.Click
        If Not Calendar.SelectedAppointment Is Nothing Then
            If Not (Calendar.SelectedAppointment.Item("BRID") = Me.BRID And Calendar.SelectedAppointment.Item("APType") = APType And Calendar.SelectedAppointment.Item("APTypeID") = APTypeID) Then
                If Message.AppointmentNotCurrentObject("job", Message.ObjectAction.Delete) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If
            If AddAppointmentLock(Calendar.SelectedAppointment.Item("APID")) Then
                Calendar.SelectedAppointment.Delete()
                daAppointments.Update(Calendar.DsCalendar.VAppointments)
                RefreshAppointments()
            Else
                Message.CurrentlyAccessed("appointment")
            End If
        End If
    End Sub

    Private Sub Calendar_AppointmentsChanged(ByVal sender As Object, ByVal e As DevExpress.XtraScheduler.PersistentObjectsEventArgs) Handles Calendar.AppointmentsChanged
        If HasRole("branch_read_only") Then Exit Sub
        For Each obj As Object In e.Objects
            'If Not (obj.GetRow(Calendar.Storage)("BRID") = BRID And obj.GetRow(Calendar.Storage)("APType") = APType And obj.GetRow(Calendar.Storage)("APTypeID") = APTypeID) Then
            '   If Message.AppointmentNotCurrentObject ("appointment") = MsgBoxResult.No Then
            '       Calendar.DsCalendar.RejectChanges()
            '    End If
            'End If
            If Not AddAppointmentLock(obj.GetRow(Calendar.Storage)("APID")) Then
                Message.CurrentlyAccessed("appointment")
                Calendar.DsCalendar.RejectChanges()
            End If
        Next
        daAppointments.Update(Calendar.DsCalendar.VAppointments)
        RefreshAppointments()
    End Sub

    Private Sub Calendar_AppointmentsDeleted(ByVal sender As Object, ByVal e As DevExpress.XtraScheduler.PersistentObjectsEventArgs) Handles Calendar.AppointmentsDeleted
        If HasRole("branch_read_only") Then Exit Sub
        Dim DeletedRows As New ArrayList
        For Each obj As System.Data.DataRow In Calendar.DsCalendar.VAppointments.Rows
            If obj.RowState = DataRowState.Deleted Then
                DeletedRows.Add(obj)
            End If
        Next
        For Each obj As System.Data.DataRow In DeletedRows
            obj.RejectChanges()
            Dim APID As Long = obj("APID")
            'If Not (obj("BRID") = BRID And obj("APType") = "NO" And obj("APTypeID") = 0) Then
            '   If Message.AppointmentNotCurrentObject("quote request") = MsgBoxResult.No Then
            '        'obj.RejectChanges()
            '    End If
            'End If
            If AddAppointmentLock(APID) Then
                obj.Delete()
            Else
                Message.CurrentlyAccessed("appointment")
                'Calendar.DsCalendar.RejectChanges()
            End If
        Next
        daAppointments.Update(Calendar.DsCalendar.VAppointments)
        RefreshAppointments()
    End Sub

    Private hEditedAppointmentList As New ArrayList
    Public Property EditedAppointmentList() As ArrayList
        Get
            Return hEditedAppointmentList
        End Get
        Set(ByVal Value As ArrayList)
            hEditedAppointmentList = Value
        End Set
    End Property

    Private Function AddAppointmentLock(ByVal APID As Long) As Boolean
        If Not IsInEditList(APID) Then
            If DataAccess.spExecLockRequest("sp_GetAppointmentLock", BRID, APID, Transaction) Then
                EditedAppointmentList.Add(APID)
                Return True
            Else
                Return False
            End If
        Else
            Return True
        End If
    End Function

    Private Function IsInEditList(ByVal APID As Long) As Boolean
        For Each lng As Long In EditedAppointmentList
            If lng = APID Then
                Return True
            End If
        Next
        Return False
    End Function

    Private Sub ReleaseAppointmentLocks()
        For Each lng As Long In EditedAppointmentList
            DataAccess.spExecLockRequest("sp_ReleaseAppointmentLock", BRID, lng, Transaction)
        Next
    End Sub

#End Region

End Class
