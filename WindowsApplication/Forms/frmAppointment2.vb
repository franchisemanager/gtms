Public Class frmAppointment2
    Inherits DevExpress.XtraEditors.XtraForm

    Private DataRow As DataRow
    Private MinHours As Integer
    Private SqlConnection As SqlClient.SqlConnection

    Public Shared Function Add(ByVal SqlConnection As SqlClient.SqlConnection, ByVal Storage As DevExpress.XtraScheduler.SchedulerStorage, _
        ByVal dataTable As DataTable, ByVal BRID As Integer, ByVal APType As String, ByVal APTypeID As Int64, _
        Optional ByVal AllDay As Boolean = False, Optional ByVal MinHours As Integer = 0, _
        Optional ByVal EXID As Object = Nothing, Optional ByVal APBegin As Object = Nothing, _
        Optional ByVal APEnd As Object = Nothing, Optional ByVal parentRowForm As Form = Nothing)

        Dim gui As New frmAppointment2(Storage)

        With gui
            .SqlConnection = SqlConnection

            .DataRow = dataTable.NewRow()
            .DataRow("BRID") = BRID
            .DataRow("APType") = APType
            .DataRow("APTypeID") = APTypeID
            .DataRow("APBegin") = IsNull(APBegin, Today.Date)
            .DataRow("APAllDay") = AllDay
            If AllDay Then
                .DataRow("APEnd") = IsNull(APEnd, DateAdd(DateInterval.Day, 1, Today.Date))
            Else
                .DataRow("APEnd") = IsNull(APEnd, DateAdd(DateInterval.Hour, 1, Today.Date))
            End If
            Select Case APType
                Case "JB"
                    .DataRow("APTask") = 1
                    If TypeOf parentRowForm Is frmBooking2 Then
                        CType(parentRowForm, frmBooking2).PopulateAppointmentWithJobData(.DataRow)
                    End If
                Case "LD"
                    .DataRow("APTask") = 0
                    If TypeOf parentRowForm Is frmLead2 Then
                        CType(parentRowForm, frmLead2).PopulateAppointmentWithLeadData(.DataRow)
                    End If
                Case Else
                    .DataRow("APTask") = 0
            End Select
            .DataRow("TAName") = Storage.Appointments.Labels(.DataRow("APTask"))
            .DataRow("EXID") = IsNull(EXID, DBNull.Value)
            If Not .DataRow("EXID") Is DBNull.Value Then
                .DataRow("EXName") = CType(CType(.txtEXID.Storage.Resources.DataSource, DataView).Table, dsCalendar.VExpensesDataTable).FindByBRIDEXID(.DataRow("BRID"), .DataRow("EXID")).EXName
            End If

            .DataRow = .dataSet.Tables(dataTable.TableName).Rows.Add(.DataRow.ItemArray)
            .dataSet.AcceptChanges()

            .MinHours = MinHours

            .CustomizeScreen()

            If gui.ShowDialog() = .DialogResult.OK Then
                dataTable.Rows.Add(.DataRow.ItemArray)
            End If
        End With
    End Function

    Public Shared Function Edit(ByVal SqlConnection As SqlClient.SqlConnection, ByVal Storage As DevExpress.XtraScheduler.SchedulerStorage, ByVal row As DataRow, Optional ByVal MinHours As Integer = 0)
        Dim gui As New frmAppointment2(Storage)

        With gui
            .SqlConnection = SqlConnection

            .DataRow = .dataSet.Tables(row.Table.TableName).Rows.Add(row.ItemArray)
            .dataSet.AcceptChanges()

            .CustomizeScreen()

            If gui.ShowDialog = .DialogResult.OK Then
                UpdateOriginalRow(.DataRow, row)
            End If
        End With
    End Function

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Storage As DevExpress.XtraScheduler.SchedulerStorage)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        txtEXID.Storage = Storage
        txtAPTask.Storage = Storage
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtAPNotes As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dpAPBegin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dpAPEnd As DevExpress.XtraEditors.DateEdit
    Friend WithEvents txtAPDescription As DevExpress.XtraEditors.TextEdit
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents btnHelp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblStaffMember As System.Windows.Forms.Label
    Friend WithEvents lblStaffMemberDesc As System.Windows.Forms.Label
    Friend WithEvents lblNotes As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkAPAllDay As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtEXID As DevExpress.XtraScheduler.UI.AppointmentResourceEdit
    Friend WithEvents txtAPTask As DevExpress.XtraScheduler.UI.AppointmentLabelEdit
    Friend WithEvents dataSet As WindowsApplication.dsCalendar
    Friend WithEvents teAPBegin As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents teAPEnd As DevExpress.XtraEditors.TimeEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmAppointment2))
        Me.lblStaffMemberDesc = New System.Windows.Forms.Label
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage
        Me.txtAPTask = New DevExpress.XtraScheduler.UI.AppointmentLabelEdit
        Me.dataSet = New WindowsApplication.dsCalendar
        Me.txtEXID = New DevExpress.XtraScheduler.UI.AppointmentResourceEdit
        Me.Label1 = New System.Windows.Forms.Label
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit
        Me.txtAPDescription = New DevExpress.XtraEditors.TextEdit
        Me.lblStaffMember = New System.Windows.Forms.Label
        Me.txtAPNotes = New DevExpress.XtraEditors.MemoEdit
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl
        Me.teAPEnd = New DevExpress.XtraEditors.TimeEdit
        Me.teAPBegin = New DevExpress.XtraEditors.TimeEdit
        Me.chkAPAllDay = New DevExpress.XtraEditors.CheckEdit
        Me.dpAPBegin = New DevExpress.XtraEditors.DateEdit
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.dpAPEnd = New DevExpress.XtraEditors.DateEdit
        Me.lblNotes = New System.Windows.Forms.Label
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.btnHelp = New DevExpress.XtraEditors.SimpleButton
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.txtAPTask.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEXID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAPDescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAPNotes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.teAPEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teAPBegin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAPAllDay.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dpAPBegin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dpAPEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblStaffMemberDesc
        '
        Me.lblStaffMemberDesc.Location = New System.Drawing.Point(16, 96)
        Me.lblStaffMemberDesc.Name = "lblStaffMemberDesc"
        Me.lblStaffMemberDesc.Size = New System.Drawing.Size(240, 16)
        Me.lblStaffMemberDesc.TabIndex = 2
        Me.lblStaffMemberDesc.Text = "Please select a staff member."
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.XtraTabControl1.Location = New System.Drawing.Point(8, 8)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(392, 480)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1})
        Me.XtraTabControl1.Text = "XtraTabControl1"
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.txtAPTask)
        Me.XtraTabPage1.Controls.Add(Me.txtEXID)
        Me.XtraTabPage1.Controls.Add(Me.Label1)
        Me.XtraTabPage1.Controls.Add(Me.MemoEdit1)
        Me.XtraTabPage1.Controls.Add(Me.txtAPDescription)
        Me.XtraTabPage1.Controls.Add(Me.lblStaffMember)
        Me.XtraTabPage1.Controls.Add(Me.txtAPNotes)
        Me.XtraTabPage1.Controls.Add(Me.GroupControl1)
        Me.XtraTabPage1.Controls.Add(Me.lblStaffMemberDesc)
        Me.XtraTabPage1.Controls.Add(Me.lblNotes)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(386, 454)
        Me.XtraTabPage1.Text = "Appointment Information"
        '
        'txtAPTask
        '
        Me.txtAPTask.DataBindings.Add(New System.Windows.Forms.Binding("SelectedIndex", Me.dataSet, "VAppointments.APTask"))
        Me.txtAPTask.Location = New System.Drawing.Point(144, 296)
        Me.txtAPTask.Name = "txtAPTask"
        '
        'txtAPTask.Properties
        '
        Me.txtAPTask.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtAPTask.Size = New System.Drawing.Size(224, 20)
        Me.txtAPTask.TabIndex = 7
        '
        'dataSet
        '
        Me.dataSet.DataSetName = "dsCalendar"
        Me.dataSet.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'txtEXID
        '
        Me.txtEXID.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "VAppointments.EXID"))
        Me.txtEXID.Location = New System.Drawing.Point(104, 120)
        Me.txtEXID.Name = "txtEXID"
        '
        'txtEXID.Properties
        '
        Me.txtEXID.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtEXID.Size = New System.Drawing.Size(264, 20)
        Me.txtEXID.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(16, 296)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(128, 23)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Task:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'MemoEdit1
        '
        Me.MemoEdit1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "VAppointments.APAddressMultiline"))
        Me.MemoEdit1.EditValue = "Address"
        Me.MemoEdit1.Location = New System.Drawing.Point(16, 40)
        Me.MemoEdit1.Name = "MemoEdit1"
        '
        'MemoEdit1.Properties
        '
        Me.MemoEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.MemoEdit1.Properties.Appearance.Options.UseBackColor = True
        Me.MemoEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.MemoEdit1.Size = New System.Drawing.Size(360, 48)
        Me.MemoEdit1.TabIndex = 1
        '
        'txtAPDescription
        '
        Me.txtAPDescription.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAPDescription.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "VAppointments.APDescription"))
        Me.txtAPDescription.EditValue = "Quote Request"
        Me.txtAPDescription.Location = New System.Drawing.Point(16, 16)
        Me.txtAPDescription.Name = "txtAPDescription"
        '
        'txtAPDescription.Properties
        '
        Me.txtAPDescription.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.txtAPDescription.Properties.Appearance.Options.UseBackColor = True
        Me.txtAPDescription.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.txtAPDescription.Properties.ReadOnly = True
        Me.txtAPDescription.Size = New System.Drawing.Size(358, 18)
        Me.txtAPDescription.TabIndex = 0
        '
        'lblStaffMember
        '
        Me.lblStaffMember.Location = New System.Drawing.Point(16, 120)
        Me.lblStaffMember.Name = "lblStaffMember"
        Me.lblStaffMember.Size = New System.Drawing.Size(80, 23)
        Me.lblStaffMember.TabIndex = 3
        Me.lblStaffMember.Text = "Staff member:"
        Me.lblStaffMember.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAPNotes
        '
        Me.txtAPNotes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAPNotes.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "VAppointments.APNotes"))
        Me.txtAPNotes.EditValue = ""
        Me.txtAPNotes.Location = New System.Drawing.Point(16, 344)
        Me.txtAPNotes.Name = "txtAPNotes"
        '
        'txtAPNotes.Properties
        '
        Me.txtAPNotes.Properties.MaxLength = 2000
        Me.txtAPNotes.Size = New System.Drawing.Size(355, 96)
        Me.txtAPNotes.TabIndex = 9
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.GroupControl1.Appearance.Options.UseBackColor = True
        Me.GroupControl1.Controls.Add(Me.teAPEnd)
        Me.GroupControl1.Controls.Add(Me.teAPBegin)
        Me.GroupControl1.Controls.Add(Me.chkAPAllDay)
        Me.GroupControl1.Controls.Add(Me.dpAPBegin)
        Me.GroupControl1.Controls.Add(Me.Label2)
        Me.GroupControl1.Controls.Add(Me.Label4)
        Me.GroupControl1.Controls.Add(Me.dpAPEnd)
        Me.GroupControl1.Location = New System.Drawing.Point(16, 152)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(360, 128)
        Me.GroupControl1.TabIndex = 5
        Me.GroupControl1.Text = "Time Slot Scheduling Details"
        '
        'teAPEnd
        '
        Me.teAPEnd.EditValue = New Date(2006, 7, 10, 0, 0, 0, 0)
        Me.teAPEnd.Location = New System.Drawing.Point(248, 96)
        Me.teAPEnd.Name = "teAPEnd"
        '
        'teAPEnd.Properties
        '
        Me.teAPEnd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.teAPEnd.TabIndex = 6
        '
        'teAPBegin
        '
        Me.teAPBegin.EditValue = New Date(2006, 7, 10, 0, 0, 0, 0)
        Me.teAPBegin.Location = New System.Drawing.Point(248, 64)
        Me.teAPBegin.Name = "teAPBegin"
        '
        'teAPBegin.Properties
        '
        Me.teAPBegin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.teAPBegin.TabIndex = 3
        '
        'chkAPAllDay
        '
        Me.chkAPAllDay.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "VAppointments.APAllDay"))
        Me.chkAPAllDay.Location = New System.Drawing.Point(16, 32)
        Me.chkAPAllDay.Name = "chkAPAllDay"
        '
        'chkAPAllDay.Properties
        '
        Me.chkAPAllDay.Properties.Caption = "All day"
        Me.chkAPAllDay.Size = New System.Drawing.Size(88, 18)
        Me.chkAPAllDay.TabIndex = 0
        '
        'dpAPBegin
        '
        Me.dpAPBegin.EditValue = New Date(2005, 7, 21, 0, 0, 0, 0)
        Me.dpAPBegin.Location = New System.Drawing.Point(128, 64)
        Me.dpAPBegin.Name = "dpAPBegin"
        '
        'dpAPBegin.Properties
        '
        Me.dpAPBegin.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False
        Me.dpAPBegin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpAPBegin.Size = New System.Drawing.Size(112, 20)
        Me.dpAPBegin.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(16, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 23)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Start Date:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(16, 96)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 23)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "End Date:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dpAPEnd
        '
        Me.dpAPEnd.EditValue = New Date(2005, 7, 21, 0, 0, 0, 0)
        Me.dpAPEnd.Location = New System.Drawing.Point(128, 96)
        Me.dpAPEnd.Name = "dpAPEnd"
        '
        'dpAPEnd.Properties
        '
        Me.dpAPEnd.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False
        Me.dpAPEnd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpAPEnd.Size = New System.Drawing.Size(112, 20)
        Me.dpAPEnd.TabIndex = 5
        '
        'lblNotes
        '
        Me.lblNotes.Location = New System.Drawing.Point(16, 328)
        Me.lblNotes.Name = "lblNotes"
        Me.lblNotes.Size = New System.Drawing.Size(96, 16)
        Me.lblNotes.TabIndex = 8
        Me.lblNotes.Text = "Notes:"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(250, 496)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(330, 496)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'btnHelp
        '
        Me.btnHelp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnHelp.Image = CType(resources.GetObject("btnHelp.Image"), System.Drawing.Image)
        Me.btnHelp.Location = New System.Drawing.Point(8, 496)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(72, 23)
        Me.btnHelp.TabIndex = 1
        Me.btnHelp.Text = "Help"
        '
        'frmAppointment2
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(410, 528)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnHelp)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAppointment2"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Appointment"
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.txtAPTask.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEXID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAPDescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAPNotes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.teAPEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teAPBegin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAPAllDay.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dpAPBegin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dpAPEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnOK.Enabled = Not HasRole("branch_read_only")
        chkAPAllDay_CheckedChanged(sender, e)
        LoadDateTimes()
    End Sub

    Private Sub LoadDateTimes()
        If DataRow("APAllDay") Then
            dpAPBegin.DateTime = CType(DataRow("APBegin"), DateTime).Date
            dpAPEnd.DateTime = CType(DataRow("APEnd"), DateTime).Date.AddHours(-1)
        Else
            dpAPBegin.DateTime = CType(DataRow("APBegin"), DateTime).Date
            dpAPEnd.DateTime = CType(DataRow("APEnd"), DateTime).Date
            teAPBegin.Time = CType(DataRow("APBegin"), DateTime)
            teAPEnd.Time = CType(DataRow("APEnd"), DateTime)
        End If
    End Sub

    Private Sub SaveDateTimes()
        If DataRow("APAllDay") Then
            DataRow("APBegin") = dpAPBegin.DateTime.Date
            DataRow("APEnd") = dpAPEnd.DateTime.Date.AddDays(1)
        Else
            DataRow("APBegin") = dpAPBegin.DateTime.Date.AddHours(teAPBegin.Time.Hour).AddMinutes(teAPBegin.Time.Minute)
            DataRow("APEnd") = dpAPEnd.DateTime.Date.AddHours(teAPEnd.Time.Hour).AddMinutes(teAPEnd.Time.Minute)
        End If
    End Sub

    Private Sub FillData()
        CustomizeScreen()
    End Sub

    Private Sub EnableDisable()
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        DataRow.EndEdit()
        SaveDateTimes()
        DataRow.EndEdit()
        If ValidateForm() Then
            OK = True
            Me.Close()
        End If
    End Sub

    Private Function ValidateForm() As Boolean
        If txtEXID.EditValue Is DBNull.Value Then
            Select Case DataRow("APType")
                Case "LD"
                    DevExpress.XtraEditors.XtraMessageBox.Show("You must select a salesperson for this appointment.", "Franchise Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Case "JB"
                    DevExpress.XtraEditors.XtraMessageBox.Show("You must select direct labor for this appointment.", "Franchise Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Case Else
                    DevExpress.XtraEditors.XtraMessageBox.Show("You must select person for this appointment.", "Franchise Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Select
            txtEXID.Focus()
            Return False
        End If
        If DataRow("APBegin") Is DBNull.Value Then
            DevExpress.XtraEditors.XtraMessageBox.Show("You must select a start date and time for this appointment.", "Franchise Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            dpAPBegin.Focus()
            Return False
        ElseIf Not IsValidDate(CDate(DataRow("APBegin"))) Then
            DevExpress.XtraEditors.XtraMessageBox.Show("You must select a start date between the years 1753 and 9999.", "Franchise Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            dpAPBegin.Focus()
            Return False
        End If
        If DataRow("APEnd") Is DBNull.Value Then
            DevExpress.XtraEditors.XtraMessageBox.Show("You must select a end date and time for this appointment.", "Franchise Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            dpAPEnd.Focus()
            Return False
        ElseIf Not IsValidDate(CDate(DataRow("APEnd"))) Then
            DevExpress.XtraEditors.XtraMessageBox.Show("You must select a end date between the years 1753 and 9999.", "Franchise Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            dpAPBegin.Focus()
            Return False
        End If
        If DateDiff(DateInterval.Hour, DataRow("APBegin"), DataRow("APEnd")) < MinHours Then
            DevExpress.XtraEditors.XtraMessageBox.Show("You cannot enter an appointment less than " & MinHours & " hour(s).", "Franchise Manager", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End If
        If False And ExecuteScalar("SELECT dbo.IsAppointmentClashing(@BRID, @APID, @APBegin, @APEnd, @EXID)", CommandType.Text, New SqlClient.SqlParameter() { _
                New SqlClient.SqlParameter("@BRID", DataRow("BRID")), _
                New SqlClient.SqlParameter("@APID", DataRow("APID")), _
                New SqlClient.SqlParameter("@APBegin", DataRow("APBegin")), _
                New SqlClient.SqlParameter("@APEnd", DataRow("APEnd")), _
                New SqlClient.SqlParameter("@EXID", DataRow("EXID"))}, SqlConnection) Then
            Message.ShowMessage("This appointment clashes with another appointment.  Please reschedule this appointment.", MessageBoxIcon.Exclamation)
            Return False
        End If
        Return True
    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            DataRow.EndEdit()
            If dataSet.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If Not response = MsgBoxResult.Yes Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub CustomizeScreen()
        Select Case DataRow("APType")
            Case "LD"
                Me.lblStaffMember.Text = "Salesperson:"
                Me.lblStaffMemberDesc.Text = "Please select a salesperson."
            Case "JB"
                Me.lblStaffMember.Text = "Direct Labor:"
                Me.lblStaffMemberDesc.Text = "Please select a direct labor person."
            Case "NO"
                Me.lblStaffMember.Text = "Staff Member:"
                Me.lblStaffMemberDesc.Text = "Please select a staff member."
        End Select
    End Sub

    Public Property APAllDay() As Boolean
        Get
            Return DataRow("APAllDay")
        End Get
        Set(ByVal Value As Boolean)
            DataRow("APAllDay") = Value
        End Set
    End Property

    Public Property APBegin() As Date
        Get
            Return DataRow("APBegin")
        End Get
        Set(ByVal Value As Date)
            DataRow("APBegin") = Value
        End Set
    End Property

    Public Property APEnd() As Date
        Get
            Return DataRow("APEnd")
        End Get
        Set(ByVal Value As Date)
            DataRow("APEnd") = Value
        End Set
    End Property

    Public Property EXID() As Int32
        Get
            Return DataRow("EXID")
        End Get
        Set(ByVal Value As Int32)
            DataRow("EXID") = Value
        End Set
    End Property

    Public Property APTask() As Int32
        Get
            Return DataRow("APTask")
        End Get
        Set(ByVal Value As Int32)
            DataRow("APTask") = Value
        End Set
    End Property

    Private hAPBegin As DateTime
    Private hAPEnd As DateTime
    Private hHasPopulatedHiddenTimes As Boolean = False
    Private Sub chkAPAllDay_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAPAllDay.CheckedChanged
        If chkAPAllDay.Checked Then
            teAPBegin.Visible = False
            teAPEnd.Visible = False
        Else
            teAPBegin.Visible = True
            teAPEnd.Visible = True
            teAPBegin.Time = New Date(1, 1, 1, CalendarControl.DEFAULT_WORK_START_HOUR, 0, 0)
            teAPEnd.Time = New Date(1, 1, 1, CalendarControl.DEFAULT_WORK_END_HOUR, 0, 0)
        End If
    End Sub

    Private Sub btnHelp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHelp.Click
    End Sub

    Private Sub txtAPTask_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAPTask.Validated
        If Not DataRow("APTask") Is DBNull.Value Then
            DataRow("TAName") = Me.txtEXID.Storage.Appointments.Labels(DataRow("APTask"))
        End If
    End Sub

    Private Sub txtEXID_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEXID.Validated
        If Not DataRow("EXID") Is DBNull.Value Then
            Dim row As WindowsApplication.dsCalendar.VExpensesRow = CType(CType(Me.txtEXID.Storage.Resources.DataSource, DataView).Table, dsCalendar.VExpensesDataTable).FindByBRIDEXID(DataRow("BRID"), DataRow("EXID"))
            If Not row Is Nothing Then
                DataRow("EXName") = row.EXName
            Else
                DataRow("EXName") = DBNull.Value
            End If
        End If
    End Sub
End Class

