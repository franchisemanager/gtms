Imports Power.Library
Imports Power.Forms

Public Class lvLeads2
    Inherits System.Windows.Forms.UserControl
    Implements IListControl, IPrintableControl

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal Connection As SqlClient.SqlConnection, ByVal BRID As Integer, ByVal ExplorerForm As frmExplorer)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.Connection = Connection
        Me.BRID = BRID
        hExplorerForm = ExplorerForm
        SetUpPermissions()
        SetUpFilters()
        DateFilterIsListening = True
        FillDataSet()
        EnableDisable()
    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DsGTMS As WindowsApplication.dsGTMS
    Friend WithEvents hSqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLDClientName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLDDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLDUser As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLDJobAddress As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents colLDStatus As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents bStandard As DevExpress.XtraBars.Bar
    Friend WithEvents btnNew As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnEdit As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnDelete As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnMailMerge As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subReportsOnSelectedItem As DevExpress.XtraBars.BarSubItem
    Friend WithEvents subFormsOnSelectedItem As DevExpress.XtraBars.BarSubItem
    Friend WithEvents btnHelp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarAndDockingController1 As DevExpress.XtraBars.BarAndDockingController
    Friend WithEvents ImageListToolBar As System.Windows.Forms.ImageList
    Friend WithEvents btnCopyLeadToJob As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPrintPreview As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnQuickPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnPrint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents subReportsOnSelectedItem2 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents subFormsOnSelectedItem2 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents PopupMenu1 As DevExpress.XtraBars.PopupMenu
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents btnCancel As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnUncancel As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bAdvancedFilter As DevExpress.XtraBars.Bar
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents beiFromDate As DevExpress.XtraBars.BarEditItem
    Friend WithEvents beiToDate As DevExpress.XtraBars.BarEditItem
    Friend WithEvents colLDAppointmentBeginDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents RepositoryItemDateEdit4 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents DataView As System.Data.DataView
    Friend WithEvents beiAppointmentFromDate As DevExpress.XtraBars.BarEditItem
    Friend WithEvents beiAppointmentToDate As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemPopupContainerEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit
    Friend WithEvents beiLDStatus As DevExpress.XtraBars.BarEditItem
    Friend WithEvents clbLDStatus As DevExpress.XtraEditors.CheckedListBoxControl
    Friend WithEvents pccLDStatus As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents btnResetFilters As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents colEXNameRep As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim StyleFormatCondition1 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(lvLeads2))
        Me.colLDStatus = New DevExpress.XtraGrid.Columns.GridColumn
        Me.hSqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.DsGTMS = New WindowsApplication.dsGTMS
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl
        Me.DataView = New System.Data.DataView
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colID = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colLDClientName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colLDJobAddress = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colLDDate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colLDUser = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colLDAppointmentBeginDate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colEXNameRep = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox
        Me.BarManager1 = New DevExpress.XtraBars.BarManager
        Me.bStandard = New DevExpress.XtraBars.Bar
        Me.btnNew = New DevExpress.XtraBars.BarButtonItem
        Me.btnEdit = New DevExpress.XtraBars.BarButtonItem
        Me.btnCancel = New DevExpress.XtraBars.BarButtonItem
        Me.btnCopyLeadToJob = New DevExpress.XtraBars.BarButtonItem
        Me.subReportsOnSelectedItem = New DevExpress.XtraBars.BarSubItem
        Me.subFormsOnSelectedItem = New DevExpress.XtraBars.BarSubItem
        Me.btnQuickPrint = New DevExpress.XtraBars.BarButtonItem
        Me.btnPrintPreview = New DevExpress.XtraBars.BarButtonItem
        Me.btnMailMerge = New DevExpress.XtraBars.BarButtonItem
        Me.btnHelp = New DevExpress.XtraBars.BarButtonItem
        Me.bAdvancedFilter = New DevExpress.XtraBars.Bar
        Me.beiFromDate = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.beiToDate = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.beiAppointmentFromDate = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemDateEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.beiAppointmentToDate = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemDateEdit4 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.beiLDStatus = New DevExpress.XtraBars.BarEditItem
        Me.RepositoryItemPopupContainerEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit
        Me.pccLDStatus = New DevExpress.XtraEditors.PopupContainerControl
        Me.clbLDStatus = New DevExpress.XtraEditors.CheckedListBoxControl
        Me.btnResetFilters = New DevExpress.XtraBars.BarButtonItem
        Me.BarAndDockingController1 = New DevExpress.XtraBars.BarAndDockingController(Me.components)
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
        Me.ImageListToolBar = New System.Windows.Forms.ImageList(Me.components)
        Me.btnUncancel = New DevExpress.XtraBars.BarButtonItem
        Me.btnDelete = New DevExpress.XtraBars.BarButtonItem
        Me.btnPrint = New DevExpress.XtraBars.BarButtonItem
        Me.subReportsOnSelectedItem2 = New DevExpress.XtraBars.BarSubItem
        Me.subFormsOnSelectedItem2 = New DevExpress.XtraBars.BarSubItem
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.PopupMenu1 = New DevExpress.XtraBars.PopupMenu
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemPopupContainerEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pccLDStatus, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pccLDStatus.SuspendLayout()
        CType(Me.clbLDStatus, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'colLDStatus
        '
        Me.colLDStatus.Caption = "Status"
        Me.colLDStatus.FieldName = "LDStatus"
        Me.colLDStatus.Name = "colLDStatus"
        Me.colLDStatus.Visible = True
        Me.colLDStatus.VisibleIndex = 7
        Me.colLDStatus.Width = 160
        '
        'hSqlConnection
        '
        Me.hSqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;user id=sa;integrated security=SSPI;data sou" & _
        "rce=""SERVER\DEV"";persist security info=False;initial catalog=GTMS_DEV"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VLeads", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("LDID", "LDID"), New System.Data.Common.DataColumnMapping("ID", "ID"), New System.Data.Common.DataColumnMapping("LDDate", "LDDate"), New System.Data.Common.DataColumnMapping("LDClientName", "LDClientName"), New System.Data.Common.DataColumnMapping("LDJobAddress", "LDJobAddress"), New System.Data.Common.DataColumnMapping("LDUser", "LDUser"), New System.Data.Common.DataColumnMapping("LDIsCancelled", "LDIsCancelled"), New System.Data.Common.DataColumnMapping("LDQuoteRequested", "LDQuoteRequested")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Leads WHERE (BRID = @Original_BRID) AND (LDID = @Original_LDID)"
        Me.SqlDeleteCommand1.Connection = Me.hSqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDID", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Leads (BRID, ID, LDDate, LDUser, LDIsCancelled, LDQuoteRequested) VAL" & _
        "UES (@BRID, @ID, @LDDate, @LDUser, @LDIsCancelled, @LDQuoteRequested); SELECT BR" & _
        "ID, LDID, ID, LDDate, LDClientName, LDJobAddress, LDUser, LDStatus, LDIsCancelle" & _
        "d, LDQuoteRequested, LDAppointmentBeginDate, EXNameRep FROM VLeads WHERE (BRID =" & _
        " @BRID) AND (LDID = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.hSqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDDate", System.Data.SqlDbType.DateTime, 8, "LDDate"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDUser", System.Data.SqlDbType.VarChar, 50, "LDUser"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDIsCancelled", System.Data.SqlDbType.Bit, 1, "LDIsCancelled"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDQuoteRequested", System.Data.SqlDbType.Bit, 1, "LDQuoteRequested"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, LDID, ID, LDDate, LDClientName, LDJobAddress, LDUser, LDStatus, LDIs" & _
        "Cancelled, LDQuoteRequested, LDAppointmentBeginDate, EXNameRep FROM VLeads WHERE" & _
        " (LDDate < DATEADD(DAY, 1, @TO_DATE) OR @TO_DATE IS NULL) AND (BRID = @BRID) AND" & _
        " (LDDate >= @FROM_DATE OR @FROM_DATE IS NULL) AND (LDAppointmentBeginDate >= @AP" & _
        "POINTMENT_FROM_DATE OR @APPOINTMENT_FROM_DATE IS NULL) AND (LDAppointmentBeginDa" & _
        "te < DATEADD(DAY, 1, @APPOINTMENT_TO_DATE) OR @APPOINTMENT_TO_DATE IS NULL)"
        Me.SqlSelectCommand1.Connection = Me.hSqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TO_DATE", System.Data.SqlDbType.DateTime, 8, "LDDate"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FROM_DATE", System.Data.SqlDbType.DateTime, 8, "LDDate"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APPOINTMENT_FROM_DATE", System.Data.SqlDbType.DateTime, 8, "LDAppointmentBeginDate"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@APPOINTMENT_TO_DATE", System.Data.SqlDbType.DateTime, 8, "LDAppointmentBeginDate"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Leads SET BRID = @BRID, ID = @ID, LDDate = @LDDate, LDUser = @LDUser, LDIs" & _
        "Cancelled = @LDIsCancelled, LDQuoteRequested = @LDQuoteRequested WHERE (BRID = @" & _
        "Original_BRID) AND (LDID = @Original_LDID); SELECT BRID, LDID, ID, LDDate, LDCli" & _
        "entName, LDJobAddress, LDUser, LDStatus, LDIsCancelled, LDQuoteRequested, LDAppo" & _
        "intmentBeginDate, EXNameRep FROM VLeads WHERE (BRID = @BRID) AND (LDID = @LDID)"
        Me.SqlUpdateCommand1.Connection = Me.hSqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.BigInt, 8, "ID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDDate", System.Data.SqlDbType.DateTime, 8, "LDDate"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDUser", System.Data.SqlDbType.VarChar, 50, "LDUser"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDIsCancelled", System.Data.SqlDbType.Bit, 1, "LDIsCancelled"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDQuoteRequested", System.Data.SqlDbType.Bit, 1, "LDQuoteRequested"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LDID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LDID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LDID", System.Data.SqlDbType.BigInt, 8, "LDID"))
        '
        'DsGTMS
        '
        Me.DsGTMS.DataSetName = "dsGTMS"
        Me.DsGTMS.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.DataView
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        '
        'GridControl1.EmbeddedNavigator
        '
        Me.GridControl1.EmbeddedNavigator.Name = ""
        Me.GridControl1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridControl1.Location = New System.Drawing.Point(0, 133)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemComboBox1})
        Me.GridControl1.Size = New System.Drawing.Size(632, 259)
        Me.GridControl1.Styles.AddReplace("CardBorder", New DevExpress.Utils.ViewStyleEx("CardBorder", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.InactiveBorder, System.Drawing.SystemColors.WindowFrame, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("BandPanelBackground", New DevExpress.Utils.ViewStyleEx("BandPanelBackground", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ControlDark, System.Drawing.Color.DarkSalmon, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("EmptySpace", New DevExpress.Utils.ViewStyleEx("EmptySpace", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("FieldValue", New DevExpress.Utils.ViewStyleEx("FieldValue", "CardView", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("BandPanel", New DevExpress.Utils.ViewStyleEx("BandPanel", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("CardButton", New DevExpress.Utils.ViewStyleEx("CardButton", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("FocusedCardCaption", New DevExpress.Utils.ViewStyleEx("FocusedCardCaption", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ActiveCaption, System.Drawing.SystemColors.ActiveCaptionText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("CardCaption", New DevExpress.Utils.ViewStyleEx("CardCaption", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.InactiveCaption, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("HeaderPanelBackground", New DevExpress.Utils.ViewStyleEx("HeaderPanelBackground", "Grid", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ControlDark, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("SeparatorLine", New DevExpress.Utils.ViewStyleEx("SeparatorLine", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ActiveBorder, System.Drawing.SystemColors.ActiveBorder, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.Styles.AddReplace("FieldCaption", New DevExpress.Utils.ViewStyleEx("FieldCaption", "CardView", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Top, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'DataView
        '
        Me.DataView.Table = Me.DsGTMS.VLeads
        '
        'GridView1
        '
        Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.Window
        Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
        Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
        Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Control
        Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID, Me.colLDClientName, Me.colLDJobAddress, Me.colLDDate, Me.colLDUser, Me.colLDStatus, Me.colLDAppointmentBeginDate, Me.colEXNameRep})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None
        StyleFormatCondition1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Strikeout, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        StyleFormatCondition1.Appearance.ForeColor = System.Drawing.SystemColors.GrayText
        StyleFormatCondition1.Appearance.Options.UseFont = True
        StyleFormatCondition1.Appearance.Options.UseForeColor = True
        StyleFormatCondition1.ApplyToRow = True
        StyleFormatCondition1.Column = Me.colLDStatus
        StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal
        StyleFormatCondition1.Value1 = "Cancelled"
        Me.GridView1.FormatConditions.AddRange(New DevExpress.XtraGrid.StyleFormatCondition() {StyleFormatCondition1})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsNavigation.AutoFocusNewRow = True
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowGroupedColumns = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.OptionsView.ShowHorzLines = False
        Me.GridView1.OptionsView.ShowIndicator = False
        Me.GridView1.OptionsView.ShowVertLines = False
        Me.GridView1.RowHeight = 8
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colID, DevExpress.Data.ColumnSortOrder.Descending)})
        '
        'colID
        '
        Me.colID.Caption = "ID"
        Me.colID.FieldName = "ID"
        Me.colID.Name = "colID"
        Me.colID.Visible = True
        Me.colID.VisibleIndex = 0
        Me.colID.Width = 42
        '
        'colLDClientName
        '
        Me.colLDClientName.Caption = "Customer Name"
        Me.colLDClientName.FieldName = "LDClientName"
        Me.colLDClientName.Name = "colLDClientName"
        Me.colLDClientName.Visible = True
        Me.colLDClientName.VisibleIndex = 1
        Me.colLDClientName.Width = 179
        '
        'colLDJobAddress
        '
        Me.colLDJobAddress.Caption = "Job Address"
        Me.colLDJobAddress.FieldName = "LDJobAddress"
        Me.colLDJobAddress.Name = "colLDJobAddress"
        Me.colLDJobAddress.Visible = True
        Me.colLDJobAddress.VisibleIndex = 2
        Me.colLDJobAddress.Width = 184
        '
        'colLDDate
        '
        Me.colLDDate.Caption = "Enquiry Date"
        Me.colLDDate.FieldName = "LDDate"
        Me.colLDDate.Name = "colLDDate"
        Me.colLDDate.Visible = True
        Me.colLDDate.VisibleIndex = 5
        Me.colLDDate.Width = 134
        '
        'colLDUser
        '
        Me.colLDUser.Caption = "Recorded By"
        Me.colLDUser.FieldName = "LDUser"
        Me.colLDUser.Name = "colLDUser"
        Me.colLDUser.Visible = True
        Me.colLDUser.VisibleIndex = 3
        Me.colLDUser.Width = 152
        '
        'colLDAppointmentBeginDate
        '
        Me.colLDAppointmentBeginDate.Caption = "Date of First Appointment"
        Me.colLDAppointmentBeginDate.FieldName = "LDAppointmentBeginDate"
        Me.colLDAppointmentBeginDate.Name = "colLDAppointmentBeginDate"
        Me.colLDAppointmentBeginDate.Visible = True
        Me.colLDAppointmentBeginDate.VisibleIndex = 6
        Me.colLDAppointmentBeginDate.Width = 128
        '
        'colEXNameRep
        '
        Me.colEXNameRep.Caption = "Salesperson"
        Me.colEXNameRep.FieldName = "EXNameRep"
        Me.colEXNameRep.Name = "colEXNameRep"
        Me.colEXNameRep.Visible = True
        Me.colEXNameRep.VisibleIndex = 4
        Me.colEXNameRep.Width = 126
        '
        'RepositoryItemComboBox1
        '
        Me.RepositoryItemComboBox1.AutoHeight = False
        Me.RepositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox1.Name = "RepositoryItemComboBox1"
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.bStandard, Me.bAdvancedFilter})
        Me.BarManager1.Categories.AddRange(New DevExpress.XtraBars.BarManagerCategory() {New DevExpress.XtraBars.BarManagerCategory("Built-in Menus", New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd"), False), New DevExpress.XtraBars.BarManagerCategory("File", New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")), New DevExpress.XtraBars.BarManagerCategory("Reports", New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")), New DevExpress.XtraBars.BarManagerCategory("Forms", New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")), New DevExpress.XtraBars.BarManagerCategory("Help", New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")), New DevExpress.XtraBars.BarManagerCategory("Search", New System.Guid("2524bb1c-b1f8-4399-8539-f6369f28ea6d"))})
        Me.BarManager1.Controller = Me.BarAndDockingController1
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Images = Me.ImageListToolBar
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnNew, Me.btnEdit, Me.btnCancel, Me.btnUncancel, Me.btnDelete, Me.btnCopyLeadToJob, Me.btnHelp, Me.btnPrintPreview, Me.btnQuickPrint, Me.btnPrint, Me.subReportsOnSelectedItem, Me.subFormsOnSelectedItem, Me.subReportsOnSelectedItem2, Me.subFormsOnSelectedItem2, Me.btnMailMerge, Me.beiFromDate, Me.beiToDate, Me.beiAppointmentFromDate, Me.beiAppointmentToDate, Me.beiLDStatus, Me.btnResetFilters})
        Me.BarManager1.MainMenu = Me.bStandard
        Me.BarManager1.MaxItemId = 105
        Me.BarManager1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1, Me.RepositoryItemDateEdit2, Me.RepositoryItemDateEdit3, Me.RepositoryItemDateEdit4, Me.RepositoryItemTextEdit1, Me.RepositoryItemPopupContainerEdit1})
        '
        'bStandard
        '
        Me.bStandard.BarName = "Standard Toolbar"
        Me.bStandard.DockCol = 0
        Me.bStandard.DockRow = 0
        Me.bStandard.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bStandard.FloatLocation = New System.Drawing.Point(44, 188)
        Me.bStandard.FloatSize = New System.Drawing.Size(659, 24)
        Me.bStandard.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnNew, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnEdit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnCancel, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnCopyLeadToJob, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(Me.subReportsOnSelectedItem, True), New DevExpress.XtraBars.LinkPersistInfo(Me.subFormsOnSelectedItem, True), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnQuickPrint, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnPrintPreview, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnMailMerge, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnHelp, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.bStandard.OptionsBar.AllowQuickCustomization = False
        Me.bStandard.OptionsBar.DisableClose = True
        Me.bStandard.OptionsBar.DrawDragBorder = False
        Me.bStandard.OptionsBar.MultiLine = True
        Me.bStandard.OptionsBar.UseWholeRow = True
        Me.bStandard.Text = "Standard Toolbar"
        '
        'btnNew
        '
        Me.btnNew.Caption = "&New..."
        Me.btnNew.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnNew.Id = 19
        Me.btnNew.ImageIndex = 0
        Me.btnNew.ImageIndexDisabled = 0
        Me.btnNew.Name = "btnNew"
        '
        'btnEdit
        '
        Me.btnEdit.Caption = "&Edit..."
        Me.btnEdit.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnEdit.Id = 20
        Me.btnEdit.ImageIndex = 1
        Me.btnEdit.ImageIndexDisabled = 1
        Me.btnEdit.Name = "btnEdit"
        '
        'btnCancel
        '
        Me.btnCancel.Caption = "&Cancel"
        Me.btnCancel.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnCancel.Id = 92
        Me.btnCancel.ImageIndex = 2
        Me.btnCancel.ImageIndexDisabled = 2
        Me.btnCancel.Name = "btnCancel"
        '
        'btnCopyLeadToJob
        '
        Me.btnCopyLeadToJob.Caption = "Book As &Job..."
        Me.btnCopyLeadToJob.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnCopyLeadToJob.Id = 24
        Me.btnCopyLeadToJob.ImageIndex = 3
        Me.btnCopyLeadToJob.ImageIndexDisabled = 3
        Me.btnCopyLeadToJob.Name = "btnCopyLeadToJob"
        '
        'subReportsOnSelectedItem
        '
        Me.subReportsOnSelectedItem.Caption = "Reports on Selected Lead"
        Me.subReportsOnSelectedItem.CategoryGuid = New System.Guid("1b1e2a95-b0e2-4de4-9bf9-40c52107091e")
        Me.subReportsOnSelectedItem.Id = 78
        Me.subReportsOnSelectedItem.Name = "subReportsOnSelectedItem"
        Me.subReportsOnSelectedItem.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInCustomizing
        '
        'subFormsOnSelectedItem
        '
        Me.subFormsOnSelectedItem.Caption = "Forms on Selected Lead"
        Me.subFormsOnSelectedItem.CategoryGuid = New System.Guid("bead2c95-03f3-428b-ab24-c591429e57a8")
        Me.subFormsOnSelectedItem.Id = 79
        Me.subFormsOnSelectedItem.Name = "subFormsOnSelectedItem"
        Me.subFormsOnSelectedItem.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInCustomizing
        '
        'btnQuickPrint
        '
        Me.btnQuickPrint.Caption = "Print"
        Me.btnQuickPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnQuickPrint.Id = 46
        Me.btnQuickPrint.ImageIndex = 11
        Me.btnQuickPrint.ImageIndexDisabled = 11
        Me.btnQuickPrint.Name = "btnQuickPrint"
        '
        'btnPrintPreview
        '
        Me.btnPrintPreview.Caption = "Print Pre&view"
        Me.btnPrintPreview.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrintPreview.Id = 44
        Me.btnPrintPreview.ImageIndex = 10
        Me.btnPrintPreview.ImageIndexDisabled = 10
        Me.btnPrintPreview.Name = "btnPrintPreview"
        '
        'btnMailMerge
        '
        Me.btnMailMerge.Caption = "&Mail Merge..."
        Me.btnMailMerge.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnMailMerge.Id = 91
        Me.btnMailMerge.ImageIndex = 12
        Me.btnMailMerge.ImageIndexDisabled = 12
        Me.btnMailMerge.Name = "btnMailMerge"
        '
        'btnHelp
        '
        Me.btnHelp.Caption = "&Help"
        Me.btnHelp.CategoryGuid = New System.Guid("cb85db31-d6c5-4c3e-b302-c26afaaf5785")
        Me.btnHelp.Id = 68
        Me.btnHelp.ImageIndex = 4
        Me.btnHelp.ImageIndexDisabled = 4
        Me.btnHelp.Name = "btnHelp"
        '
        'bAdvancedFilter
        '
        Me.bAdvancedFilter.BarName = "Search Bar"
        Me.bAdvancedFilter.DockCol = 0
        Me.bAdvancedFilter.DockRow = 1
        Me.bAdvancedFilter.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bAdvancedFilter.FloatLocation = New System.Drawing.Point(40, 390)
        Me.bAdvancedFilter.FloatSize = New System.Drawing.Size(767, 51)
        Me.bAdvancedFilter.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(CType((DevExpress.XtraBars.BarLinkUserDefines.PaintStyle Or DevExpress.XtraBars.BarLinkUserDefines.Width), DevExpress.XtraBars.BarLinkUserDefines), Me.beiFromDate, "", False, True, True, 150, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.beiToDate, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.beiAppointmentFromDate, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.beiAppointmentToDate, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.beiLDStatus, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.btnResetFilters, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.bAdvancedFilter.Offset = 72
        Me.bAdvancedFilter.OptionsBar.AllowQuickCustomization = False
        Me.bAdvancedFilter.OptionsBar.DisableClose = True
        Me.bAdvancedFilter.OptionsBar.MultiLine = True
        Me.bAdvancedFilter.Text = "Search Bar"
        '
        'beiFromDate
        '
        Me.beiFromDate.Caption = "Search for Enquiry Dates from:"
        Me.beiFromDate.CategoryGuid = New System.Guid("2524bb1c-b1f8-4399-8539-f6369f28ea6d")
        Me.beiFromDate.Edit = Me.RepositoryItemDateEdit1
        Me.beiFromDate.Id = 93
        Me.beiFromDate.Name = "beiFromDate"
        Me.beiFromDate.Width = 150
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'beiToDate
        '
        Me.beiToDate.Caption = "to:"
        Me.beiToDate.CategoryGuid = New System.Guid("2524bb1c-b1f8-4399-8539-f6369f28ea6d")
        Me.beiToDate.Edit = Me.RepositoryItemDateEdit2
        Me.beiToDate.Id = 94
        Me.beiToDate.Name = "beiToDate"
        Me.beiToDate.Width = 150
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'beiAppointmentFromDate
        '
        Me.beiAppointmentFromDate.Caption = "Search for Date of First Appointment from:"
        Me.beiAppointmentFromDate.CategoryGuid = New System.Guid("2524bb1c-b1f8-4399-8539-f6369f28ea6d")
        Me.beiAppointmentFromDate.Edit = Me.RepositoryItemDateEdit3
        Me.beiAppointmentFromDate.Id = 96
        Me.beiAppointmentFromDate.Name = "beiAppointmentFromDate"
        Me.beiAppointmentFromDate.Width = 150
        '
        'RepositoryItemDateEdit3
        '
        Me.RepositoryItemDateEdit3.AutoHeight = False
        Me.RepositoryItemDateEdit3.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit3.Name = "RepositoryItemDateEdit3"
        '
        'beiAppointmentToDate
        '
        Me.beiAppointmentToDate.Caption = "to:"
        Me.beiAppointmentToDate.CategoryGuid = New System.Guid("2524bb1c-b1f8-4399-8539-f6369f28ea6d")
        Me.beiAppointmentToDate.Edit = Me.RepositoryItemDateEdit4
        Me.beiAppointmentToDate.Id = 97
        Me.beiAppointmentToDate.Name = "beiAppointmentToDate"
        Me.beiAppointmentToDate.Width = 150
        '
        'RepositoryItemDateEdit4
        '
        Me.RepositoryItemDateEdit4.AutoHeight = False
        Me.RepositoryItemDateEdit4.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit4.Name = "RepositoryItemDateEdit4"
        '
        'beiLDStatus
        '
        Me.beiLDStatus.Caption = "Search by Status:"
        Me.beiLDStatus.CategoryGuid = New System.Guid("2524bb1c-b1f8-4399-8539-f6369f28ea6d")
        Me.beiLDStatus.Edit = Me.RepositoryItemPopupContainerEdit1
        Me.beiLDStatus.Id = 104
        Me.beiLDStatus.Name = "beiLDStatus"
        Me.beiLDStatus.Width = 150
        '
        'RepositoryItemPopupContainerEdit1
        '
        Me.RepositoryItemPopupContainerEdit1.AutoHeight = False
        Me.RepositoryItemPopupContainerEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemPopupContainerEdit1.Name = "RepositoryItemPopupContainerEdit1"
        Me.RepositoryItemPopupContainerEdit1.PopupControl = Me.pccLDStatus
        Me.RepositoryItemPopupContainerEdit1.PopupSizeable = False
        Me.RepositoryItemPopupContainerEdit1.ShowPopupCloseButton = False
        '
        'pccLDStatus
        '
        Me.pccLDStatus.Controls.Add(Me.clbLDStatus)
        Me.pccLDStatus.Location = New System.Drawing.Point(192, 248)
        Me.pccLDStatus.Name = "pccLDStatus"
        Me.pccLDStatus.Size = New System.Drawing.Size(152, 128)
        Me.pccLDStatus.TabIndex = 4
        Me.pccLDStatus.Text = "PopupContainerControl1"
        '
        'clbLDStatus
        '
        Me.clbLDStatus.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.clbLDStatus.Dock = System.Windows.Forms.DockStyle.Fill
        Me.clbLDStatus.Items.AddRange(New DevExpress.XtraEditors.Controls.CheckedListBoxItem() {New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Lead", System.Windows.Forms.CheckState.Checked), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Quote Requested", System.Windows.Forms.CheckState.Checked), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Appointment Upcoming", System.Windows.Forms.CheckState.Checked), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Quote Outstanding", System.Windows.Forms.CheckState.Checked), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Quote Received", System.Windows.Forms.CheckState.Checked), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Converted To Job", System.Windows.Forms.CheckState.Checked), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("Cancelled", System.Windows.Forms.CheckState.Checked)})
        Me.clbLDStatus.Location = New System.Drawing.Point(0, 0)
        Me.clbLDStatus.Name = "clbLDStatus"
        Me.clbLDStatus.Size = New System.Drawing.Size(152, 128)
        Me.clbLDStatus.TabIndex = 0
        '
        'btnResetFilters
        '
        Me.btnResetFilters.Caption = "&Reset Filters"
        Me.btnResetFilters.CategoryGuid = New System.Guid("2524bb1c-b1f8-4399-8539-f6369f28ea6d")
        Me.btnResetFilters.Id = 95
        Me.btnResetFilters.ImageIndex = 14
        Me.btnResetFilters.ImageIndexDisabled = 14
        Me.btnResetFilters.Name = "btnResetFilters"
        '
        'BarAndDockingController1
        '
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HideContainer.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButton.Options.UseForeColor = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.ForeColor = System.Drawing.Color.Blue
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseFont = True
        Me.BarAndDockingController1.AppearancesDocking.HidePanelButtonActive.Options.UseForeColor = True
        Me.BarAndDockingController1.PaintStyleName = "OfficeXP"
        '
        'ImageListToolBar
        '
        Me.ImageListToolBar.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImageListToolBar.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageListToolBar.ImageStream = CType(resources.GetObject("ImageListToolBar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListToolBar.TransparentColor = System.Drawing.Color.Transparent
        '
        'btnUncancel
        '
        Me.btnUncancel.Caption = "&Uncancel"
        Me.btnUncancel.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnUncancel.Id = 86
        Me.btnUncancel.Name = "btnUncancel"
        '
        'btnDelete
        '
        Me.btnDelete.Caption = "&Delete"
        Me.btnDelete.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnDelete.Id = 23
        Me.btnDelete.ImageIndex = 13
        Me.btnDelete.ImageIndexDisabled = 13
        Me.btnDelete.Name = "btnDelete"
        '
        'btnPrint
        '
        Me.btnPrint.Caption = "&Print..."
        Me.btnPrint.CategoryGuid = New System.Guid("2e2466c8-44b6-49bb-b8ed-0016eecb02e0")
        Me.btnPrint.Id = 43
        Me.btnPrint.ImageIndex = 11
        Me.btnPrint.ImageIndexDisabled = 11
        Me.btnPrint.Name = "btnPrint"
        '
        'subReportsOnSelectedItem2
        '
        Me.subReportsOnSelectedItem2.Caption = "&Reports on Selected Lead"
        Me.subReportsOnSelectedItem2.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subReportsOnSelectedItem2.Id = 87
        Me.subReportsOnSelectedItem2.Name = "subReportsOnSelectedItem2"
        '
        'subFormsOnSelectedItem2
        '
        Me.subFormsOnSelectedItem2.Caption = "&Forms on Selected Lead"
        Me.subFormsOnSelectedItem2.CategoryGuid = New System.Guid("d00af398-58e0-44c6-86cc-bf6efec186fd")
        Me.subFormsOnSelectedItem2.Id = 88
        Me.subFormsOnSelectedItem2.Name = "subFormsOnSelectedItem2"
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'PopupMenu1
        '
        Me.PopupMenu1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.subReportsOnSelectedItem, True), New DevExpress.XtraBars.LinkPersistInfo(Me.subFormsOnSelectedItem), New DevExpress.XtraBars.LinkPersistInfo(Me.btnEdit, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btnCancel), New DevExpress.XtraBars.LinkPersistInfo(Me.btnDelete), New DevExpress.XtraBars.LinkPersistInfo(Me.btnUncancel, True)})
        Me.PopupMenu1.Manager = Me.BarManager1
        Me.PopupMenu1.Name = "PopupMenu1"
        '
        'lvLeads2
        '
        Me.Controls.Add(Me.pccLDStatus)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "lvLeads2"
        Me.Size = New System.Drawing.Size(632, 392)
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemPopupContainerEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pccLDStatus, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pccLDStatus.ResumeLayout(False)
        CType(Me.clbLDStatus, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return hSqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            hSqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private Sub EnableDisable()
        btnNew.Enabled = AllowNew
        btnEdit.Enabled = AllowEdit
        btnCancel.Enabled = AllowCancel
        btnUncancel.Enabled = AllowUncancel
        btnDelete.Enabled = AllowDelete
    End Sub

    Public Function FillDataSet() As Integer Implements IListControl.FillDataSet
        Dim cursor As System.Windows.Forms.Cursor = ExplorerForm.Cursor
        ExplorerForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
        ' Open connection
        Dim trans As SqlClient.SqlTransaction = Connection.BeginTransaction(IsolationLevel.ReadUncommitted)
        Power.Library.Library.ApplyTransactionToAllDataAdapters(trans, Me)
        'Me.GridControl1.DataSource.Clear()
        FillDataSet = SqlDataAdapter.Fill(DsGTMS)
        'Close connecton
        trans.Commit()
        ExplorerForm.Cursor = cursor
    End Function

#Region " Filters "

    Private Sub SetUpFilters()
        ' Set Up Default Advanced Filter
        beiFromDate.EditValue = DateAdd(DateInterval.Month, -6, Today.Date)
        beiToDate.EditValue = Nothing
        beiAppointmentFromDate.EditValue = Nothing
        beiAppointmentToDate.EditValue = Nothing
        UpdateDateFilter()
        CheckAllStatuses()
        GridView1.ClearColumnsFilter()

        UpdateStatusCaption()
        UpdateAdvancedFilter()
    End Sub

    Private Sub DateFilter_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles beiFromDate.EditValueChanged, beiToDate.EditValueChanged, _
    beiAppointmentFromDate.EditValueChanged, beiAppointmentToDate.EditValueChanged
        UpdateDateFilter()
    End Sub

    Private DateFilterIsListening As Boolean = False ' This is so this doesn't trigger during load (this will happen in code)
    Private Sub UpdateDateFilter()
        Me.SqlDataAdapter.SelectCommand.Parameters("@FROM_DATE").Value = IsNull(beiFromDate.EditValue, DBNull.Value)
        Me.SqlDataAdapter.SelectCommand.Parameters("@TO_DATE").Value = IsNull(beiToDate.EditValue, DBNull.Value)
        Me.SqlDataAdapter.SelectCommand.Parameters("@APPOINTMENT_FROM_DATE").Value = IsNull(beiAppointmentFromDate.EditValue, DBNull.Value)
        Me.SqlDataAdapter.SelectCommand.Parameters("@APPOINTMENT_TO_DATE").Value = IsNull(beiAppointmentToDate.EditValue, DBNull.Value)
        If DateFilterIsListening Then
            Me.DsGTMS.Tables(Me.SqlDataAdapter.TableMappings(0).DataSetTable).Clear()
            FillDataSet()
        End If
    End Sub

    Private Sub btnResetFilters_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnResetFilters.ItemClick
        DateFilterIsListening = False
        SetUpFilters()
        Me.DsGTMS.Tables(Me.SqlDataAdapter.TableMappings(0).DataSetTable).Clear()
        FillDataSet()
        DateFilterIsListening = True
    End Sub

    Private Sub UpdateAdvancedFilter()
        DataView.RowFilter = StatusFilter()
    End Sub

    Private Sub RepositoryItemPopupContainerEdit1_QueryResultValue(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles RepositoryItemPopupContainerEdit1.QueryResultValue
        UpdateStatusCaption()
        UpdateAdvancedFilter()
    End Sub

    Private Sub UpdateStatusCaption()
        If AllStatusesTicked() Then
            beiLDStatus.EditValue = "(All)"
        ElseIf NoStatusesTicked() Then
            beiLDStatus.EditValue = "(None)"
        Else
            beiLDStatus.EditValue = StatusesTicked()
        End If
    End Sub

    Private Function StatusFilter() As String
        If AllStatusesTicked() Then Return ""
        Dim filter As String = ""
        For Each status As DevExpress.XtraEditors.Controls.CheckedListBoxItem In clbLDStatus.Items
            If status.CheckState = CheckState.Checked Then
                filter = AddCondition(filter, "[LDStatus] = '" & status.Value & "'", "OR")
            End If
        Next
        If filter.Length = 0 Then
            filter = "False" ' this means there were none selected
        Else
            filter = "(" & filter & ")" ' Add brackets because of the OR's
        End If
        Return filter
    End Function

    Private Function AllStatusesTicked() As Boolean
        For Each status As DevExpress.XtraEditors.Controls.CheckedListBoxItem In clbLDStatus.Items
            If Not (status.CheckState = CheckState.Checked) Then
                Return False
            End If
        Next
        Return True
    End Function

    Private Function NoStatusesTicked() As Boolean
        For Each status As DevExpress.XtraEditors.Controls.CheckedListBoxItem In clbLDStatus.Items
            If Not (status.CheckState = CheckState.Unchecked) Then
                Return False
            End If
        Next
        Return True
    End Function

    Private Function StatusesTicked() As String
        Dim str As String = ""
        For Each status As DevExpress.XtraEditors.Controls.CheckedListBoxItem In clbLDStatus.Items
            If status.CheckState = CheckState.Checked Then
                str = AddStatus(str, status.Value)
            End If
        Next
        Return str
    End Function

    Private Function AddStatus(ByVal existingStatuses As String, ByVal status As String) As String
        If existingStatuses.Length = 0 Then
            Return status
        Else
            Return existingStatuses & ", " & status
        End If
    End Function

    Private Sub CheckAllStatuses()
        For Each status As DevExpress.XtraEditors.Controls.CheckedListBoxItem In clbLDStatus.Items
            status.CheckState = CheckState.Checked
        Next
    End Sub

#End Region

    Private Sub GridView1_RowCountChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.RowCountChanged
        ExplorerForm.UpdateNumItems(NumItems)
        ExplorerForm.UpdateVisibleItems(VisibleItems)
    End Sub

    Public ReadOnly Property VisibleItems() As Integer Implements Power.Library.IListControl.VisibleItems
        Get
            Return GridView1.RowCount
        End Get
    End Property

    Public ReadOnly Property NumItems() As Integer Implements Power.Library.IListControl.NumItems
        Get
            Return DsGTMS.Tables(SqlDataAdapter.TableMappings(0).DataSetTable).Rows.Count
        End Get
    End Property

    Private hBRID As Integer
    Private Property BRID() As Integer
        Get
            Return hBRID
        End Get
        Set(ByVal Value As Integer)
            hBRID = Value
            SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = Value
        End Set
    End Property

    Public ReadOnly Property SelectedRow() As DataRow
        Get
            If Not GridView1.GetSelectedRows Is Nothing Then
                Return GridView1.GetDataRow(GridView1.GetSelectedRows(0))
            End If
        End Get
    End Property

    Public ReadOnly Property SelectedRowField(ByVal Field As String) As Object
        Get
            If Not SelectedRow Is Nothing Then
                Return SelectedRow.Item(Field)
            Else
                Return DBNull.Value
            End If
        End Get
    End Property

    Public Function SelectRow(ByVal BRID As Int32, ByVal LDID As Int64) As Boolean
        Dim i As Integer = 0
        Do Until False
            Try
                If GridView1.GetDataRow(GridView1.GetVisibleRowHandle(i))("BRID") = BRID And _
                        GridView1.GetDataRow(GridView1.GetVisibleRowHandle(i))("LDID") = LDID Then
                    GridView1.ClearSelection()
                    GridView1.SelectRow(GridView1.GetVisibleRowHandle(i))
                    Return True
                End If
                i += 1
            Catch ex As NullReferenceException
                Return False
            End Try
        Loop
        Return False
    End Function

#Region " New, Open and Delete "

    Private Sub GridControl1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridControl1.DoubleClick
        List_Edit()
    End Sub

    Private Sub btnNew_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnNew.ItemClick
        List_New()
    End Sub

    Public Sub List_New() Implements IListControl.List_New
        Dim c As Cursor = ParentForm.Cursor
        ParentForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
        frmLead2.Add(BRID)
        FillDataSet()
        ParentForm.Cursor = c
    End Sub

    Private Sub btnEdit_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnEdit.ItemClick
        List_Edit()
    End Sub

    Public Sub List_Edit() Implements IListControl.List_Edit
        If Not SelectedRow Is Nothing Then
            Dim c As Cursor = ParentForm.Cursor
            ParentForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If AppointmentExists(SelectedRowField("BRID"), SelectedRowField("LDID")) Then
                frmLead2.Edit(SelectedRowField("BRID"), SelectedRowField("LDID"))
                FillDataSet()
            Else
                Message.AlreadyDeleted("lead", Message.ObjectAction.Edit)
                If TypeOf Me.GridControl1.DataSource Is DataView Then
                    CType(Me.GridControl1.DataSource, DataView).Table.Clear()
                ElseIf TypeOf Me.GridControl1.DataSource Is DataSet Then
                    CType(Me.GridControl1.DataSource, DataSet).Clear()
                End If
                FillDataSet()
            End If
            ParentForm.Cursor = c
        End If
    End Sub

    Private Sub btnCancel_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnCancel.ItemClick
        List_Cancel()
    End Sub

    Public Sub List_Cancel() Implements Power.Library.IListControl.List_Cancel
        If Not SelectedRow Is Nothing Then
            Dim BRID As Integer = SelectedRowField("BRID")
            Dim LDID As Int64 = SelectedRowField("LDID")
            If Message.AskDeleteObject("lead", Message.ObjectAction.Cancel) = MsgBoxResult.Yes Then
                If AppointmentExists(BRID, LDID) Then
                    If DataAccess.spExecLockRequest("sp_GetLeadLock", BRID, LDID, Connection) Then
                        SelectedRow.Item("LDIsCancelled") = True
                        SqlDataAdapter.Update(DsGTMS)
                        DataAccess.spExecLockRequest("sp_ReleaseLeadLock", BRID, LDID, Connection)
                    Else
                        Message.CurrentlyAccessed("lead")
                    End If
                Else
                    Message.AlreadyDeleted("lead", Message.ObjectAction.Cancel)
                    If TypeOf Me.GridControl1.DataSource Is DataView Then
                        CType(Me.GridControl1.DataSource, DataView).Table.Clear()
                    ElseIf TypeOf Me.GridControl1.DataSource Is DataSet Then
                        CType(Me.GridControl1.DataSource, DataSet).Clear()
                    End If
                    FillDataSet()
                End If
            End If
        End If
    End Sub

    Private Sub btnUncancel_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnUncancel.ItemClick
        List_Uncancel()
    End Sub

    Public Sub List_Uncancel() Implements Power.Library.IListControl.List_Uncancel
        If Not SelectedRow Is Nothing Then
            Dim BRID As Integer = SelectedRowField("BRID")
            Dim LDID As Int64 = SelectedRowField("LDID")
            If AppointmentExists(BRID, LDID) Then
                If DataAccess.spExecLockRequest("sp_GetLeadLock", BRID, LDID, Connection) Then
                    SelectedRow.Item("LDIsCancelled") = False
                    SqlDataAdapter.Update(DsGTMS)
                    DataAccess.spExecLockRequest("sp_ReleaseLeadLock", BRID, LDID, Connection)
                Else
                    Message.CurrentlyAccessed("lead")
                End If
            Else
                Message.AlreadyDeleted("lead", Message.ObjectAction.Edit)
                If TypeOf Me.GridControl1.DataSource Is DataView Then
                    CType(Me.GridControl1.DataSource, DataView).Table.Clear()
                ElseIf TypeOf Me.GridControl1.DataSource Is DataSet Then
                    CType(Me.GridControl1.DataSource, DataSet).Clear()
                End If
                FillDataSet()
            End If
        End If
    End Sub

    Private Sub btnDelete_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnDelete.ItemClick
        List_Delete()
    End Sub

    Public Sub List_Delete() Implements IListControl.List_Delete
        If Not SelectedRow Is Nothing Then
            If Message.AskDeleteObject("lead", Message.ObjectAction.Delete) = MsgBoxResult.Yes Then
                Dim BRID As Integer = SelectedRowField("BRID")
                Dim LDID As Int64 = SelectedRowField("LDID")
                If AppointmentExists(BRID, LDID) Then
                    If DataAccess.spExecLockRequest("sp_GetLeadLock", BRID, LDID, Connection) Then
                        SelectedRow.Delete()
                        SqlDataAdapter.Update(DsGTMS)
                        DataAccess.spExecLockRequest("sp_ReleaseLeadLock", BRID, LDID, Connection)
                    Else
                        Message.CurrentlyAccessed("lead")
                    End If
                Else
                    If TypeOf Me.GridControl1.DataSource Is DataView Then
                        CType(Me.GridControl1.DataSource, DataView).Table.Clear()
                    ElseIf TypeOf Me.GridControl1.DataSource Is DataSet Then
                        CType(Me.GridControl1.DataSource, DataSet).Clear()
                    End If
                    FillDataSet()
                End If
            End If
        End If
    End Sub

    Private Function AppointmentExists(ByVal BRID As Int32, ByVal LDID As Int64) As Boolean
        Dim cnn As SqlClient.SqlConnection = Connection
        Dim cmd As New SqlClient.SqlCommand("SELECT Count(ID) FROM Leads WHERE BRID = @BRID AND LDID = @LDID", cnn)
        cmd.CommandType = CommandType.Text
        cmd.Parameters.Add("@BRID", BRID)
        cmd.Parameters.Add("@LDID", LDID)
        Return cmd.ExecuteScalar > 0
    End Function

#End Region

    Private Sub btnCopyLeadToJob_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnCopyLeadToJob.ItemClick
        CopyToJob()
    End Sub

    Public Function CopyToJob() As Boolean
        If Not SelectedRow Is Nothing Then
            If CBool(SelectedRow.Item("LDQuoteRequested")) Then
                Dim c As Cursor = ParentForm.Cursor
                ParentForm.Cursor = System.Windows.Forms.Cursors.WaitCursor
                If AppointmentExists(SelectedRowField("BRID"), SelectedRowField("LDID")) Then
                    CopyToJob = frmBooking2.CopyFromLead(SelectedRowField("BRID"), SelectedRowField("LDID")) = DialogResult.OK
                    FillDataSet()
                Else
                    Message.AlreadyDeleted("quote request", Message.ObjectAction.Edit)
                    Me.GridControl1.DataSource.Clear()
                    FillDataSet()
                End If
                ParentForm.Cursor = c
            Else
                Message.ShowMessage("You cannot book in a lead as a job.  This lead must become a quote request before it becomes a job.", MessageBoxIcon.Exclamation)
            End If
        End If
    End Function

    Private hExplorerForm As Form
    Public ReadOnly Property ExplorerForm() As frmExplorer
        Get
            Return hExplorerForm
        End Get
    End Property

#Region " Printing "

    Private Sub btnPrint_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrint.ItemClick
        Print(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub Print(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.Print
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.PrintDlg()
    End Sub

    Private Sub btnPrintPreview_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnPrintPreview.ItemClick
        PrintPreview(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub PrintPreview(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.PrintPreview
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.ShowPreview()
    End Sub

    Private Sub btnQuickPrint_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnQuickPrint.ItemClick
        QuickPrint(ExplorerForm.PrintingSystem)
    End Sub

    Public Sub QuickPrint(ByVal PrintingSystem As DevExpress.XtraPrinting.PrintingSystem) Implements Power.Library.IPrintableControl.QuickPrint
        Dim link As New DevExpress.XtraPrinting.PrintableComponentLink(PrintingSystem)
        link.Component = GridControl1
        link.Print(PrintingSystem.PageSettings.PrinterName)
    End Sub

#End Region

    Private Sub btnMailMerge_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnMailMerge.ItemClick
        If MailMergeFilterValid() Then
            Dim rep As New repGenericLeadsMailout
            Dim cmd As SqlClient.SqlCommand
            Dim cmdString As String = "SELECT * FROM dbo.[Lead Mail Merge] WHERE BRID = @BRID " & _
                "AND ([Enquiry Date] >= @FROM_DATE OR @FROM_DATE IS NULL) AND ([Enquiry Date] < DATEADD(DAY, 1, @TO_DATE) OR @TO_DATE IS NULL) " & _
                "AND ([Date of First Appointment] >= @APPOINTMENT_FROM_DATE OR @APPOINTMENT_FROM_DATE IS NULL) AND ([Date of First Appointment] < DATEADD(DAY, 1, @APPOINTMENT_TO_DATE) OR @APPOINTMENT_TO_DATE IS NULL)"
            Dim filter As String = MailMergeFilter()
            If filter = "" Then
                cmd = New SqlClient.SqlCommand(cmdString, Connection)
            Else
                cmd = New SqlClient.SqlCommand(cmdString & " AND " & filter, Connection)
            End If
            cmd.Parameters.Add("@BRID", Me.BRID)
            cmd.Parameters.Add("@FROM_DATE", IsNull(beiFromDate.EditValue, DBNull.Value))
            cmd.Parameters.Add("@TO_DATE", IsNull(beiToDate.EditValue, DBNull.Value))
            cmd.Parameters.Add("@APPOINTMENT_FROM_DATE", IsNull(beiAppointmentFromDate.EditValue, DBNull.Value))
            cmd.Parameters.Add("@APPOINTMENT_TO_DATE", IsNull(beiAppointmentToDate.EditValue, DBNull.Value))
            Dim da As New SqlClient.SqlDataAdapter(cmd)
            'da.MissingMappingAction = MissingMappingAction.Ignore
            ReadUncommittedFill(da, rep.dataSet.Lead_Mail_Merge)
            If rep.dataSet.Lead_Mail_Merge.Count > 100 Then
                Message.ShowMessage_MailoutTooLarge(100)
                Exit Sub
            End If
            Dim designerGui As New frmXtraReportDesigner(rep)
            designerGui.Show()
        End If
    End Sub

    Private Function MailMergeFilter() As String
        Return Replace(Replace(Replace(Replace(Replace(AddCondition(GridView1.RowFilter, StatusFilter), _
            "[LDClientName]", "[Customer Name]"), _
            "[LDJobAddress]", "[Job Address (Full)]"), _
            "[LDUser]", "[Contacted By]"), _
            "[LDStatus]", "[Leads Status]"), _
            "*", "%")
    End Function

    Private Function MailMergeFilterValid() As Boolean
        If InStr(GridView1.RowFilter, "[LDDate]") > 0 Or InStr(GridView1.RowFilter, "[LDAppointmentBeginDate]") > 0 Then
            Message.ShowMessage_DateInSearchRow()
            Return False
        End If
        Return True
    End Function

    Public ReadOnly Property AllowDelete() As Boolean Implements Power.Library.IListControl.AllowDelete
        Get
            Return HasRole("full")
        End Get
    End Property

    Public ReadOnly Property AllowEdit() As Boolean Implements Power.Library.IListControl.AllowEdit
        Get
            Return True
        End Get
    End Property

    Public ReadOnly Property AllowNew() As Boolean Implements Power.Library.IListControl.AllowNew
        Get
            Return Not HasRole("branch_read_only")
        End Get
    End Property

    Public ReadOnly Property AllowCancel() As Boolean Implements Power.Library.IListControl.AllowCancel
        Get
            Return Not HasRole("branch_read_only")
        End Get
    End Property

    Public ReadOnly Property AllowUncancel() As Boolean Implements Power.Library.IListControl.AllowUncancel
        Get
            Return Not HasRole("branch_read_only")
        End Get
    End Property

    Private Sub GridView1_ShowGridMenu(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.GridMenuEventArgs) Handles GridView1.ShowGridMenu
        If SelectedRow Is Nothing Then Exit Sub
        Dim View As DevExpress.XtraGrid.Views.Grid.GridView = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
        Dim HitInfo As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo
        HitInfo = View.CalcHitInfo(e.Point)
        If HitInfo.InRow Then
            Dim p2 As New Point(e.Point.X + View.GridControl.Left, e.Point.Y + View.GridControl.Top)
            btnUncancel.Enabled = SelectedRow.Item("LDIsCancelled")
            PopupMenu1.ShowPopup(Me.PointToScreen(p2))
        End If
    End Sub

    Public Sub SetUpPermissions()
        For Each cmd As CommandAccess In Commands
            If Not Me.BarManager1.Items(cmd.Command) Is Nothing Then  ' This will not fail if the cmd.Command is not found
                Me.BarManager1.Items(cmd.Command).Tag = cmd
                Me.BarManager1.Items(cmd.Command).Enabled = cmd.Allowed
            End If
        Next
    End Sub

    Private Sub btnHelp_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnHelp.ItemClick
        ShowHelpTopic(ExplorerForm, "QuoteRequests.html")
    End Sub
End Class
