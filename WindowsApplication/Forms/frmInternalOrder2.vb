Public Class frmInternalOrder2
    Inherits DevExpress.XtraEditors.XtraForm

    Private Const TableName As String = "VOrders"
    Private OriginalDataSet As DataSet
    Dim jobBRIDInventory As Int64

    'Private DataRow As DataRow
    Private Property DataRow() As DataRow
        Set(ByVal Value As DataRow)
            Dim i = 0
            BindingContext(dvOrders).Position = 0
            Do Until IsSameRowByPrimaryKey(DataRow, Value)
                If BindingContext(dvOrders).Position = BindingContext(dvOrders).Count - 1 Then
                    Throw New Exception("The DataRow could not be found in the DataTable " & TableName & ".")
                End If
                BindingContext(dvOrders).Position = BindingContext(dvOrders).Position + 1
            Loop
        End Set
        Get
            Return CType(BindingContext(dvOrders).Current(), DataRowView).Row
        End Get
    End Property

    Public Shared Sub Add(ByVal dataTable As DataTable, ByVal BRID As Integer, ByVal JBID As Int64, ByVal ID As Int64)
        Dim gui As New frmInternalOrder2

        With gui
            .SqlConnection.ConnectionString = ConnectionString

            .OriginalDataSet = dataTable.DataSet
            CopyData(.OriginalDataSet, .dataSet, False)
            .dataSet.AcceptChanges()

            .CreateNewRow(BRID, JBID, ID)

            If gui.ShowDialog() = .DialogResult.OK Then
                UpdateOriginalDataSet(.dataSet, .OriginalDataSet)
            End If
        End With
    End Sub

    Public Shared Sub Edit(ByVal row As DataRow)
        Dim gui As New frmInternalOrder2

        With gui
            .SqlConnection.ConnectionString = ConnectionString

            .OriginalDataSet = row.Table.DataSet
            CopyData(.OriginalDataSet, .dataSet, False)
            .dataSet.AcceptChanges()

            .DataRow = row

            If gui.ShowDialog = .DialogResult.OK Then
                UpdateOriginalDataSet(.dataSet, .OriginalDataSet)
            End If
        End With
    End Sub

    Private Sub CreateNewRow(ByVal BRID As Integer, ByVal JBID As Long, ByVal ID As Int64)
        SqlConnection.Open()
        Dim OrderNumber As Long = sp_GetNextOrderNumber(BRID, SqlConnection)
        SqlConnection.Close()

        Dim newRow As DataRow = dataSet.Tables(TableName).NewRow()
        newRow("BRID") = BRID
        newRow("JBID") = JBID
        newRow("ID") = ID
        newRow("OROrderNumber") = OrderNumber
        newRow("ORDate") = Today.Date
        newRow("ORType") = "IO"
        newRow("ORShipToJobAddress") = False
        newRow("ORReceived") = False
        DataRow = dataSet.Tables(TableName).Rows.Add(newRow.ItemArray)

        jobBRIDInventory = BRID

    End Sub

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()



        'Dim cmd As New SqlClient.SqlCommand("SELECT SIID, SIName FROM dbo.VBranches WHERE BRID = @BRID", SqlConnection)
        'cmd.Parameters.Add("@BRID", BRID)
        'Dim da As New SqlClient.SqlDataAdapter(cmd)
        'ReadUncommittedFill(da, Me.dataSet.VStockedItems)




        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents dataSet As WindowsApplication.dsOrders
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtNIName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtNIDeliveryDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtLDUser As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents dgOrder_Materials As DevExpress.XtraGrid.GridControl
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents RepositoryItemLookUpEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents gvOrders_Materials As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colMTID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOMBrand As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOMModelNumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOMPrimaryAmount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMTPrimaryUOMShortName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOMDescription As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gvOrders_Materials_SubItems As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents BandedGridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BandedGridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BandedGridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BandedGridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents editButtonPopup As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents txtAmount As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemButtonEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents gvOrders_StockedItems As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemLookUpEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents colSIID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOSInventoryAmount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSIInventoryUOMShortName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOSInventoryDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dvStockedItems As System.Data.DataView
    Friend WithEvents txtAmount2 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents dvStaff As System.Data.DataView
    Friend WithEvents btnRemoveStockedItem As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveMaterial As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dgOrder_StockedItems As DevExpress.XtraGrid.GridControl
    Friend WithEvents dvOrders As System.Data.DataView
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TextEdit4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmInternalOrder2))
        Me.gvOrders_Materials_SubItems = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.BandedGridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.BandedGridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.BandedGridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.BandedGridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.editButtonPopup = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
        Me.dgOrder_Materials = New DevExpress.XtraGrid.GridControl
        Me.dvOrders = New System.Data.DataView
        Me.dataSet = New WindowsApplication.dsOrders
        Me.gvOrders_Materials = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colMTID = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemLookUpEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.colOMBrand = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colOMModelNumber = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colOMPrimaryAmount = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtAmount = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colMTPrimaryUOMShortName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colOMDescription = New DevExpress.XtraGrid.Columns.GridColumn
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage
        Me.Label9 = New System.Windows.Forms.Label
        Me.TextEdit4 = New DevExpress.XtraEditors.TextEdit
        Me.btnRemoveStockedItem = New DevExpress.XtraEditors.SimpleButton
        Me.btnRemoveMaterial = New DevExpress.XtraEditors.SimpleButton
        Me.Label6 = New System.Windows.Forms.Label
        Me.dgOrder_StockedItems = New DevExpress.XtraGrid.GridControl
        Me.gvOrders_StockedItems = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colSIID = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemLookUpEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.dvStockedItems = New System.Data.DataView
        Me.colOSInventoryAmount = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtAmount2 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colSIInventoryUOMShortName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colOSInventoryDate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.RepositoryItemButtonEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtLDUser = New DevExpress.XtraEditors.LookUpEdit
        Me.dvStaff = New System.Data.DataView
        Me.Label4 = New System.Windows.Forms.Label
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.dtNIDeliveryDate = New DevExpress.XtraEditors.DateEdit
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtNIName = New DevExpress.XtraEditors.TextEdit
        Me.Label7 = New System.Windows.Forms.Label
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        CType(Me.gvOrders_Materials_SubItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.editButtonPopup, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgOrder_Materials, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvOrders_Materials, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemLookUpEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgOrder_StockedItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvOrders_StockedItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemLookUpEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvStockedItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAmount2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemButtonEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtLDUser.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvStaff, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtNIDeliveryDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNIName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gvOrders_Materials_SubItems
        '
        Me.gvOrders_Materials_SubItems.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvOrders_Materials_SubItems.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvOrders_Materials_SubItems.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvOrders_Materials_SubItems.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvOrders_Materials_SubItems.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvOrders_Materials_SubItems.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvOrders_Materials_SubItems.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvOrders_Materials_SubItems.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvOrders_Materials_SubItems.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvOrders_Materials_SubItems.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvOrders_Materials_SubItems.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvOrders_Materials_SubItems.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvOrders_Materials_SubItems.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvOrders_Materials_SubItems.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvOrders_Materials_SubItems.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvOrders_Materials_SubItems.Appearance.VertLine.Options.UseBackColor = True
        Me.gvOrders_Materials_SubItems.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.BandedGridColumn1, Me.BandedGridColumn2, Me.BandedGridColumn3, Me.BandedGridColumn4})
        Me.gvOrders_Materials_SubItems.GridControl = Me.dgOrder_Materials
        Me.gvOrders_Materials_SubItems.Name = "gvOrders_Materials_SubItems"
        Me.gvOrders_Materials_SubItems.OptionsCustomization.AllowFilter = False
        Me.gvOrders_Materials_SubItems.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvOrders_Materials_SubItems.OptionsView.ShowGroupPanel = False
        Me.gvOrders_Materials_SubItems.ViewCaption = "Sub Items"
        '
        'BandedGridColumn1
        '
        Me.BandedGridColumn1.Caption = "Brand"
        Me.BandedGridColumn1.FieldName = "OMSBrand"
        Me.BandedGridColumn1.Name = "BandedGridColumn1"
        Me.BandedGridColumn1.Visible = True
        Me.BandedGridColumn1.VisibleIndex = 1
        Me.BandedGridColumn1.Width = 126
        '
        'BandedGridColumn2
        '
        Me.BandedGridColumn2.Caption = "Model #"
        Me.BandedGridColumn2.FieldName = "OMSModelNumber"
        Me.BandedGridColumn2.Name = "BandedGridColumn2"
        Me.BandedGridColumn2.Visible = True
        Me.BandedGridColumn2.VisibleIndex = 2
        Me.BandedGridColumn2.Width = 124
        '
        'BandedGridColumn3
        '
        Me.BandedGridColumn3.Caption = "Qty"
        Me.BandedGridColumn3.FieldName = "OMSQty"
        Me.BandedGridColumn3.Name = "BandedGridColumn3"
        Me.BandedGridColumn3.Visible = True
        Me.BandedGridColumn3.VisibleIndex = 3
        Me.BandedGridColumn3.Width = 88
        '
        'BandedGridColumn4
        '
        Me.BandedGridColumn4.Caption = "Description"
        Me.BandedGridColumn4.ColumnEdit = Me.editButtonPopup
        Me.BandedGridColumn4.FieldName = "OMSDescription"
        Me.BandedGridColumn4.Name = "BandedGridColumn4"
        Me.BandedGridColumn4.Visible = True
        Me.BandedGridColumn4.VisibleIndex = 0
        Me.BandedGridColumn4.Width = 353
        '
        'editButtonPopup
        '
        Me.editButtonPopup.AutoHeight = False
        Me.editButtonPopup.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.editButtonPopup.Name = "editButtonPopup"
        '
        'dgOrder_Materials
        '
        'Me.dgOrder_Materials.DataBindings.Add(New System.Windows.Forms.Binding("DataSource", Me.dvOrders, "VOrdersVOrders_Materials"))
        Me.dgOrder_Materials.DataSource = Me.dataSet.VOrders_Materials
        '
        'dgOrder_Materials.EmbeddedNavigator
        '
        Me.dgOrder_Materials.EmbeddedNavigator.Name = ""
        GridLevelNode1.LevelTemplate = Me.gvOrders_Materials_SubItems
        GridLevelNode1.RelationName = "VOrders_MaterialsOrders_Materials_SubItems"
        Me.dgOrder_Materials.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.dgOrder_Materials.Location = New System.Drawing.Point(8, 160)
        Me.dgOrder_Materials.MainView = Me.gvOrders_Materials
        Me.dgOrder_Materials.Name = "dgOrder_Materials"
        Me.dgOrder_Materials.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemLookUpEdit1, Me.editButtonPopup, Me.txtAmount})
        Me.dgOrder_Materials.ShowOnlyPredefinedDetails = True
        Me.dgOrder_Materials.Size = New System.Drawing.Size(712, 120)
        Me.dgOrder_Materials.TabIndex = 13
        Me.dgOrder_Materials.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvOrders_Materials, Me.gvOrders_Materials_SubItems})
        '
        'dvOrders
        '
        Me.dvOrders.RowFilter = "ORType = 'IO'"
        Me.dvOrders.Table = Me.dataSet.VOrders
        '
        'dataSet
        '
        Me.dataSet.DataSetName = "dsOrders"
        Me.dataSet.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'gvOrders_Materials
        '
        Me.gvOrders_Materials.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvOrders_Materials.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvOrders_Materials.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvOrders_Materials.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvOrders_Materials.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvOrders_Materials.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvOrders_Materials.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvOrders_Materials.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvOrders_Materials.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvOrders_Materials.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvOrders_Materials.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvOrders_Materials.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvOrders_Materials.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvOrders_Materials.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvOrders_Materials.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvOrders_Materials.Appearance.VertLine.Options.UseBackColor = True
        Me.gvOrders_Materials.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colMTID, Me.colOMBrand, Me.colOMModelNumber, Me.colOMPrimaryAmount, Me.colMTPrimaryUOMShortName, Me.colOMDescription})
        Me.gvOrders_Materials.GridControl = Me.dgOrder_Materials
        Me.gvOrders_Materials.Name = "gvOrders_Materials"
        Me.gvOrders_Materials.OptionsCustomization.AllowFilter = False
        Me.gvOrders_Materials.OptionsDetail.AllowExpandEmptyDetails = True
        Me.gvOrders_Materials.OptionsDetail.EnableDetailToolTip = True
        Me.gvOrders_Materials.OptionsDetail.ShowDetailTabs = False
        Me.gvOrders_Materials.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvOrders_Materials.OptionsView.ShowGroupPanel = False
        '
        'colMTID
        '
        Me.colMTID.Caption = "Item"
        Me.colMTID.ColumnEdit = Me.RepositoryItemLookUpEdit1
        Me.colMTID.FieldName = "MTID"
        Me.colMTID.Name = "colMTID"
        Me.colMTID.Visible = True
        Me.colMTID.VisibleIndex = 0
        Me.colMTID.Width = 164
        '
        'RepositoryItemLookUpEdit1
        '
        Me.RepositoryItemLookUpEdit1.AutoHeight = False
        Me.RepositoryItemLookUpEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemLookUpEdit1.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("MTName")})
        Me.RepositoryItemLookUpEdit1.DataSource = Me.dataSet.VMaterials
        Me.RepositoryItemLookUpEdit1.DisplayMember = "MTName"
        Me.RepositoryItemLookUpEdit1.Name = "RepositoryItemLookUpEdit1"
        Me.RepositoryItemLookUpEdit1.NullText = "<Incomplete>"
        Me.RepositoryItemLookUpEdit1.ShowFooter = False
        Me.RepositoryItemLookUpEdit1.ShowHeader = False
        Me.RepositoryItemLookUpEdit1.ValueMember = "MTID"
        '
        'colOMBrand
        '
        Me.colOMBrand.Caption = "Brand"
        Me.colOMBrand.FieldName = "OMBrand"
        Me.colOMBrand.Name = "colOMBrand"
        Me.colOMBrand.Visible = True
        Me.colOMBrand.VisibleIndex = 2
        Me.colOMBrand.Width = 87
        '
        'colOMModelNumber
        '
        Me.colOMModelNumber.Caption = "Model #"
        Me.colOMModelNumber.FieldName = "OMModelNumber"
        Me.colOMModelNumber.Name = "colOMModelNumber"
        Me.colOMModelNumber.Visible = True
        Me.colOMModelNumber.VisibleIndex = 3
        Me.colOMModelNumber.Width = 89
        '
        'colOMPrimaryAmount
        '
        Me.colOMPrimaryAmount.Caption = "Amount"
        Me.colOMPrimaryAmount.ColumnEdit = Me.txtAmount
        Me.colOMPrimaryAmount.FieldName = "OMPrimaryAmount"
        Me.colOMPrimaryAmount.Name = "colOMPrimaryAmount"
        Me.colOMPrimaryAmount.Visible = True
        Me.colOMPrimaryAmount.VisibleIndex = 4
        Me.colOMPrimaryAmount.Width = 92
        '
        'txtAmount
        '
        Me.txtAmount.AutoHeight = False
        Me.txtAmount.DisplayFormat.FormatString = "#.000"
        Me.txtAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtAmount.EditFormat.FormatString = "#.000"
        Me.txtAmount.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.NullText = "<Incomplete>"
        '
        'colMTPrimaryUOMShortName
        '
        Me.colMTPrimaryUOMShortName.FieldName = "MTPrimaryUOMShortName"
        Me.colMTPrimaryUOMShortName.Name = "colMTPrimaryUOMShortName"
        Me.colMTPrimaryUOMShortName.OptionsColumn.AllowEdit = False
        Me.colMTPrimaryUOMShortName.OptionsColumn.AllowFocus = False
        Me.colMTPrimaryUOMShortName.Visible = True
        Me.colMTPrimaryUOMShortName.VisibleIndex = 5
        Me.colMTPrimaryUOMShortName.Width = 41
        '
        'colOMDescription
        '
        Me.colOMDescription.Caption = "Description"
        Me.colOMDescription.ColumnEdit = Me.editButtonPopup
        Me.colOMDescription.FieldName = "OMDescription"
        Me.colOMDescription.Name = "colOMDescription"
        Me.colOMDescription.Visible = True
        Me.colOMDescription.VisibleIndex = 1
        Me.colOMDescription.Width = 218
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.XtraTabControl1.Location = New System.Drawing.Point(8, 8)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(744, 496)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1})
        Me.XtraTabControl1.Text = "XtraTabControl1"
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.Label9)
        Me.XtraTabPage1.Controls.Add(Me.TextEdit4)
        Me.XtraTabPage1.Controls.Add(Me.btnRemoveStockedItem)
        Me.XtraTabPage1.Controls.Add(Me.btnRemoveMaterial)
        Me.XtraTabPage1.Controls.Add(Me.Label6)
        Me.XtraTabPage1.Controls.Add(Me.dgOrder_StockedItems)
        Me.XtraTabPage1.Controls.Add(Me.Label5)
        Me.XtraTabPage1.Controls.Add(Me.dgOrder_Materials)
        Me.XtraTabPage1.Controls.Add(Me.txtLDUser)
        Me.XtraTabPage1.Controls.Add(Me.Label4)
        Me.XtraTabPage1.Controls.Add(Me.TextEdit2)
        Me.XtraTabPage1.Controls.Add(Me.Label3)
        Me.XtraTabPage1.Controls.Add(Me.Label2)
        Me.XtraTabPage1.Controls.Add(Me.dtNIDeliveryDate)
        Me.XtraTabPage1.Controls.Add(Me.TextEdit1)
        Me.XtraTabPage1.Controls.Add(Me.Label1)
        Me.XtraTabPage1.Controls.Add(Me.txtNIName)
        Me.XtraTabPage1.Controls.Add(Me.Label7)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(735, 466)
        Me.XtraTabPage1.Text = "Order Details"
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(8, 112)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(72, 21)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "Notes:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextEdit4
        '
        Me.TextEdit4.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dvOrders, "ORNotes"))
        Me.TextEdit4.EditValue = ""
        Me.TextEdit4.Location = New System.Drawing.Point(80, 112)
        Me.TextEdit4.Name = "TextEdit4"
        Me.TextEdit4.Size = New System.Drawing.Size(640, 20)
        Me.TextEdit4.TabIndex = 11
        '
        'btnRemoveStockedItem
        '
        Me.btnRemoveStockedItem.Image = CType(resources.GetObject("btnRemoveStockedItem.Image"), System.Drawing.Image)
        Me.btnRemoveStockedItem.Location = New System.Drawing.Point(8, 440)
        Me.btnRemoveStockedItem.Name = "btnRemoveStockedItem"
        Me.btnRemoveStockedItem.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveStockedItem.TabIndex = 17
        Me.btnRemoveStockedItem.Text = "Remove"
        '
        'btnRemoveMaterial
        '
        Me.btnRemoveMaterial.Image = CType(resources.GetObject("btnRemoveMaterial.Image"), System.Drawing.Image)
        Me.btnRemoveMaterial.Location = New System.Drawing.Point(8, 288)
        Me.btnRemoveMaterial.Name = "btnRemoveMaterial"
        Me.btnRemoveMaterial.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveMaterial.TabIndex = 14
        Me.btnRemoveMaterial.Text = "Remove"
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(8, 320)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(192, 21)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Inventory Tracking Information:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'dgOrder_StockedItems
        '
        'Me.dgOrder_StockedItems.DataBindings.Add(New System.Windows.Forms.Binding("DataSource", Me.dvOrders, "VOrdersVOrders_StockedItems"))
        Me.dgOrder_StockedItems.DataSource = Me.dataSet.VOrders_StockedItems



        '
        'dgOrder_StockedItems.EmbeddedNavigator
        '
        Me.dgOrder_StockedItems.EmbeddedNavigator.Name = ""
        Me.dgOrder_StockedItems.Location = New System.Drawing.Point(8, 344)
        Me.dgOrder_StockedItems.MainView = Me.gvOrders_StockedItems
        Me.dgOrder_StockedItems.Name = "dgOrder_StockedItems"
        Me.dgOrder_StockedItems.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemLookUpEdit2, Me.RepositoryItemButtonEdit1, Me.txtAmount2, Me.RepositoryItemDateEdit1})
        Me.dgOrder_StockedItems.ShowOnlyPredefinedDetails = True
        Me.dgOrder_StockedItems.Size = New System.Drawing.Size(712, 88)
        Me.dgOrder_StockedItems.TabIndex = 16
        Me.dgOrder_StockedItems.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvOrders_StockedItems, Me.GridView1})
        '
        'gvOrders_StockedItems
        '
        Me.gvOrders_StockedItems.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvOrders_StockedItems.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvOrders_StockedItems.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvOrders_StockedItems.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvOrders_StockedItems.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvOrders_StockedItems.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvOrders_StockedItems.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvOrders_StockedItems.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvOrders_StockedItems.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvOrders_StockedItems.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvOrders_StockedItems.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvOrders_StockedItems.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvOrders_StockedItems.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvOrders_StockedItems.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvOrders_StockedItems.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvOrders_StockedItems.Appearance.VertLine.Options.UseBackColor = True
        Me.gvOrders_StockedItems.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSIID, Me.colOSInventoryAmount, Me.colSIInventoryUOMShortName, Me.colOSInventoryDate})
        Me.gvOrders_StockedItems.GridControl = Me.dgOrder_StockedItems
        Me.gvOrders_StockedItems.Name = "gvOrders_StockedItems"
        Me.gvOrders_StockedItems.OptionsCustomization.AllowFilter = False
        Me.gvOrders_StockedItems.OptionsDetail.AllowExpandEmptyDetails = True
        Me.gvOrders_StockedItems.OptionsDetail.EnableDetailToolTip = True
        Me.gvOrders_StockedItems.OptionsDetail.ShowDetailTabs = False
        Me.gvOrders_StockedItems.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvOrders_StockedItems.OptionsView.ShowGroupPanel = False
        '
        'colSIID
        '
        Me.colSIID.Caption = "Inventory Item"
        Me.colSIID.ColumnEdit = Me.RepositoryItemLookUpEdit2
        Me.colSIID.FieldName = "SIID"
        Me.colSIID.Name = "colSIID"
        Me.colSIID.Visible = True
        Me.colSIID.VisibleIndex = 0
        Me.colSIID.Width = 264
        '
        'RepositoryItemLookUpEdit2
        '
        Me.RepositoryItemLookUpEdit2.AutoHeight = False
        Me.RepositoryItemLookUpEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemLookUpEdit2.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("SIName")})
        Me.RepositoryItemLookUpEdit2.DataSource = Me.dvStockedItems
        Me.RepositoryItemLookUpEdit2.DisplayMember = "SIName"
        Me.RepositoryItemLookUpEdit2.Name = "RepositoryItemLookUpEdit2"
        Me.RepositoryItemLookUpEdit2.NullText = "<Incomplete>"
        Me.RepositoryItemLookUpEdit2.ShowFooter = False
        Me.RepositoryItemLookUpEdit2.ShowHeader = False
        Me.RepositoryItemLookUpEdit2.ValueMember = "SIID"
        '
        'dvStockedItems
        '
        Me.dvStockedItems.Table = Me.dataSet.VStockedItems

        '
        'colOSInventoryAmount
        '
        Me.colOSInventoryAmount.Caption = "Amount"
        Me.colOSInventoryAmount.ColumnEdit = Me.txtAmount2
        Me.colOSInventoryAmount.FieldName = "OSInventoryAmount"
        Me.colOSInventoryAmount.Name = "colOSInventoryAmount"
        Me.colOSInventoryAmount.Visible = True
        Me.colOSInventoryAmount.VisibleIndex = 2
        Me.colOSInventoryAmount.Width = 172
        '
        'txtAmount2
        '
        Me.txtAmount2.AutoHeight = False
        Me.txtAmount2.DisplayFormat.FormatString = "#.000"
        Me.txtAmount2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtAmount2.EditFormat.FormatString = "#.000"
        Me.txtAmount2.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtAmount2.Name = "txtAmount2"
        Me.txtAmount2.NullText = "<Incomplete>"
        '
        'colSIInventoryUOMShortName
        '
        Me.colSIInventoryUOMShortName.FieldName = "SIInventoryUOMShortName"
        Me.colSIInventoryUOMShortName.Name = "colSIInventoryUOMShortName"
        Me.colSIInventoryUOMShortName.OptionsColumn.AllowEdit = False
        Me.colSIInventoryUOMShortName.OptionsColumn.AllowFocus = False
        Me.colSIInventoryUOMShortName.Visible = True
        Me.colSIInventoryUOMShortName.VisibleIndex = 3
        Me.colSIInventoryUOMShortName.Width = 72
        '
        'colOSInventoryDate
        '
        Me.colOSInventoryDate.Caption = "Inventory Date"
        Me.colOSInventoryDate.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colOSInventoryDate.FieldName = "OSInventoryDate"
        Me.colOSInventoryDate.Name = "colOSInventoryDate"
        Me.colOSInventoryDate.Visible = True
        Me.colOSInventoryDate.VisibleIndex = 1
        Me.colOSInventoryDate.Width = 183
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        Me.RepositoryItemDateEdit1.NullText = "<Incomplete>"
        '
        'RepositoryItemButtonEdit1
        '
        Me.RepositoryItemButtonEdit1.AutoHeight = False
        Me.RepositoryItemButtonEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.RepositoryItemButtonEdit1.Name = "RepositoryItemButtonEdit1"
        '
        'GridView1
        '
        Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
        Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
        Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
        Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
        Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
        Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4})
        Me.GridView1.GridControl = Me.dgOrder_StockedItems
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.ViewCaption = "Sub Items"
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Brand"
        Me.GridColumn1.FieldName = "OMSBrand"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 1
        Me.GridColumn1.Width = 126
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Model #"
        Me.GridColumn2.FieldName = "OMSModelNumber"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 2
        Me.GridColumn2.Width = 124
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Qty"
        Me.GridColumn3.FieldName = "OMSQty"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 3
        Me.GridColumn3.Width = 88
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Description"
        Me.GridColumn4.ColumnEdit = Me.RepositoryItemButtonEdit1
        Me.GridColumn4.FieldName = "OMSDescription"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 0
        Me.GridColumn4.Width = 353
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(8, 136)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(104, 21)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Items in Order:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'txtLDUser
        '
        Me.txtLDUser.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dvOrders, "EXIDOrderedBy"))
        Me.txtLDUser.Location = New System.Drawing.Point(80, 48)
        Me.txtLDUser.Name = "txtLDUser"
        '
        'txtLDUser.Properties
        '
        Me.txtLDUser.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtLDUser.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EXName", "", 45, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.txtLDUser.Properties.DataSource = Me.dvStaff
        Me.txtLDUser.Properties.DisplayMember = "EXName"
        Me.txtLDUser.Properties.NullText = ""
        Me.txtLDUser.Properties.ShowFooter = False
        Me.txtLDUser.Properties.ShowHeader = False
        Me.txtLDUser.Properties.ShowLines = False
        Me.txtLDUser.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.txtLDUser.Properties.ValueMember = "EXID"
        Me.txtLDUser.Size = New System.Drawing.Size(176, 20)
        Me.txtLDUser.TabIndex = 5
        '
        'dvStaff
        '
        Me.dvStaff.RowFilter = "EGType <> 'OE'"
        Me.dvStaff.Table = Me.dataSet.VExpenses
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(8, 48)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 21)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Raised by:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextEdit2
        '
        Me.TextEdit2.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dvOrders, "ORDescription"))
        Me.TextEdit2.EditValue = ""
        Me.TextEdit2.Location = New System.Drawing.Point(80, 80)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Size = New System.Drawing.Size(640, 20)
        Me.TextEdit2.TabIndex = 9
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(8, 80)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 21)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Description:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(288, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 21)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Date raised:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtNIDeliveryDate
        '
        Me.dtNIDeliveryDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dvOrders, "ORDate"))
        Me.dtNIDeliveryDate.EditValue = New Date(2005, 7, 20, 0, 0, 0, 0)
        Me.dtNIDeliveryDate.Location = New System.Drawing.Point(368, 48)
        Me.dtNIDeliveryDate.Name = "dtNIDeliveryDate"
        '
        'dtNIDeliveryDate.Properties
        '
        Me.dtNIDeliveryDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False
        Me.dtNIDeliveryDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtNIDeliveryDate.Size = New System.Drawing.Size(168, 20)
        Me.dtNIDeliveryDate.TabIndex = 7
        '
        'TextEdit1
        '
        Me.TextEdit1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dvOrders, "ID"))
        Me.TextEdit1.EditValue = ""
        Me.TextEdit1.Enabled = False
        Me.TextEdit1.Location = New System.Drawing.Point(216, 16)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Size = New System.Drawing.Size(72, 20)
        Me.TextEdit1.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(160, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 21)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Job ID:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNIName
        '
        Me.txtNIName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dvOrders, "OROrderNumber"))
        Me.txtNIName.EditValue = ""
        Me.txtNIName.Enabled = False
        Me.txtNIName.Location = New System.Drawing.Point(80, 16)
        Me.txtNIName.Name = "txtNIName"
        Me.txtNIName.Size = New System.Drawing.Size(72, 20)
        Me.txtNIName.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(8, 16)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(72, 21)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Order #:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton1.Image = CType(resources.GetObject("SimpleButton1.Image"), System.Drawing.Image)
        Me.SimpleButton1.Location = New System.Drawing.Point(8, 512)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(72, 23)
        Me.SimpleButton1.TabIndex = 1
        Me.SimpleButton1.Text = "Help"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(680, 512)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(600, 512)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "OK"
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;user id=sa;data source=""SERVER\DEV"";persist " &
        "security info=False;initial catalog=GTMS_DEV"
        '
        'frmInternalOrder2
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(762, 544)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmInternalOrder2"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Internal Order"
        CType(Me.gvOrders_Materials_SubItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.editButtonPopup, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgOrder_Materials, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvOrders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvOrders_Materials, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemLookUpEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgOrder_StockedItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvOrders_StockedItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemLookUpEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvStockedItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAmount2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemButtonEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtLDUser.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvStaff, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtNIDeliveryDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNIName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        UpdateStockedItemList()
    End Sub

    Private Sub EnableDisable()
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        DataRow.EndEdit()
        CalculateRowFields()
        OK = True
        Me.Close()
    End Sub

    Private Sub CalculateRowFields()
        DataRow.EndEdit()
        DataRow.BeginEdit()
        DataRow("ORIsComplete") = True
        Dim ORInvoicedCostTotal As Double = 0
        Dim FoundORInvoicedCost As Boolean = False
        Dim FoundNullORInvoicedCost As Boolean = False
        For Each row As DataRow In DataRow.GetChildRows("VOrdersVOrders_Materials")
            If row("OMInvoicedCost") Is DBNull.Value And row("OMUseInvoicedCost") = True Then
                FoundNullORInvoicedCost = True
                DataRow("ORIsComplete") = False
            ElseIf Not row("OMInvoicedCost") Is DBNull.Value And row("OMUseInvoicedCost") = True Then
                FoundORInvoicedCost = True
                ORInvoicedCostTotal = ORInvoicedCostTotal + row("OMInvoicedCost")
            End If
            If row("OMPrimaryAmount") Is DBNull.Value And row("OMUseInvoicedCost") = False Then
                DataRow("ORIsComplete") = False
            End If
            If row("MTID") Is DBNull.Value Then
                DataRow("ORIsComplete") = False
            End If
        Next

        For Each row As DataRow In DataRow.GetChildRows("VOrdersVOrders_StockedItems")
            If row("SIID") Is DBNull.Value Then
                DataRow("ORIsComplete") = False
            End If
            If row("OSInventoryAmount") Is DBNull.Value Then
                DataRow("ORIsComplete") = False
            End If
            If row("OSInventoryDate") Is DBNull.Value Then
                DataRow("ORIsComplete") = False
            End If
        Next

        If FoundORInvoicedCost And Not FoundNullORInvoicedCost Then
            DataRow("ORInvoicedCostTotal") = ORInvoicedCostTotal
        Else
            DataRow("ORInvoicedCostTotal") = DBNull.Value
        End If

        If Not DataRow("ORIsComplete") Then
            DataRow("ORStatus") = "<Incomplete>"
        Else
            DataRow("ORStatus") = ""
        End If

        DataRow.EndEdit()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            DataRow.EndEdit()
            If dataSet.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If Not response = MsgBoxResult.Yes Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub editButtonPopup_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) _
    Handles editButtonPopup.ButtonClick
        Dim focusedView As DevExpress.XtraGrid.Views.Grid.GridView = dgOrder_Materials.FocusedView
        If focusedView.GetDataRow(focusedView.GetSelectedRows(0)) Is Nothing Then
            focusedView.AddNewRow()
        End If
        focusedView.SetFocusedValue(frmMemo.Edit(IsNull(focusedView.GetFocusedValue, ""), focusedView.FocusedColumn.Caption))
    End Sub

    Private Sub GridView_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) _
    Handles gvOrders_Materials.KeyDown, gvOrders_Materials_SubItems.KeyDown, gvOrders_StockedItems.KeyDown
        If e.KeyCode = Keys.Delete Then
            If CType(sender, DevExpress.XtraGrid.Views.Grid.GridView).SelectedRowsCount > 0 Then
                CType(sender, DevExpress.XtraGrid.Views.Grid.GridView).GetDataRow(CType(sender, DevExpress.XtraGrid.Views.Grid.GridView).GetSelectedRows(0)).Delete()
            End If
        End If
    End Sub

    Private Sub Decimal_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) Handles txtAmount.ParseEditValue, txtAmount2.ParseEditValue
        Format.Decimal_ParseEditValue(sender, e)
    End Sub

#Region " Orders_Materials "

    Private Sub gvOrders_Materials_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gvOrders_Materials.InitNewRow
        gvOrders_Materials.GetDataRow(e.RowHandle)("OMUseInvoicedCost") = False
    End Sub

    Private Sub gvOrders_Materials_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gvOrders_Materials.ValidateRow
        UpdateMTPrimaryUOM(e.RowHandle)
    End Sub

    Private Sub gvOrders_Materials_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gvOrders_Materials.RowUpdated
        Select Case CType(e.Row, DataRowView).Row.RowState
            Case DataRowState.Added
                UpdateStockedItemList(CType(e.Row, DataRowView).Row)
            Case DataRowState.Modified
                UpdateStockedItemList(CType(e.Row, DataRowView).Row)
            Case Else
                UpdateStockedItemList()
        End Select
    End Sub

    Private Sub UpdateMTPrimaryUOM(ByVal rowHandle As Integer)
        If Not (gvOrders_Materials.GetDataRow(rowHandle)("BRID") Is DBNull.Value Or gvOrders_Materials.GetDataRow(rowHandle)("MTID") Is DBNull.Value) Then
            gvOrders_Materials.GetDataRow(rowHandle)("MTPrimaryUOMShortName") = dataSet.VMaterials.FindByBRIDMTID(gvOrders_Materials.GetDataRow(rowHandle)("BRID"), gvOrders_Materials.GetDataRow(rowHandle)("MTID")).MTPrimaryUOMShortName()
        Else
            gvOrders_Materials.GetDataRow(rowHandle)("MTPrimaryUOMShortName") = ""
            gvOrders_Materials.GetDataRow(rowHandle)("BRID") = jobBRIDInventory

        End If
    End Sub

    Private Sub btnRemoveMaterial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveMaterial.Click
        If Not CType(dgOrder_Materials.FocusedView, DevExpress.XtraGrid.Views.Grid.GridView).GetSelectedRows Is Nothing Then
            If Not CType(dgOrder_Materials.FocusedView, DevExpress.XtraGrid.Views.Grid.GridView).GetDataRow(CType(dgOrder_Materials.FocusedView, DevExpress.XtraGrid.Views.Grid.GridView).GetSelectedRows(0)) Is Nothing Then
                CType(dgOrder_Materials.FocusedView, DevExpress.XtraGrid.Views.Grid.GridView).GetDataRow(CType(dgOrder_Materials.FocusedView, DevExpress.XtraGrid.Views.Grid.GridView).GetSelectedRows(0)).Delete()
                'UpdateStockedItemList()
            End If
        End If
    End Sub

#End Region

#Region " Orders_StockedItems "

    Private Sub UpdateStockedItemList(Optional ByVal addedRow As DataRow = Nothing)

        dvStockedItems.RowFilter = dvStockedItems_Fitler()
        ' Delete any stocked items that no longer belong in this order
        For Each stockedItem As DataRow In CType(DataRow, dsOrders.VOrdersRow).GetVOrders_StockedItemsRows
            If Not stockedItem("SIID") Is DBNull.Value Then
                If Not dataSet.VStockedItems.FindByBRIDSIID(stockedItem("BRID"), stockedItem("SIID")) Is Nothing Then
                    If Not OrderHasMaterial(stockedItem("BRID"), dataSet.VStockedItems.FindByBRIDSIID(stockedItem("BRID"), stockedItem("SIID")).MTID) Then
                        stockedItem.Delete()
                    End If
                End If
            End If
        Next
        ' Add any new stocked items that now DO belong in this order
        If Not addedRow Is Nothing Then
            For Each stockedItem As DataRow In dataSet.VStockedItems
                If stockedItem("BRID") = addedRow("BRID") And stockedItem("MTID") = addedRow("MTID") Then
                    If Not OrderHasStockedItem(stockedItem("BRID"), stockedItem("SIID")) Then
                        Dim row As DataRow = dataSet.VOrders_StockedItems.NewRow
                        row("BRID") = DataRow("BRID")
                        row("ORID") = DataRow("ORID")
                        row("SIID") = stockedItem("SIID")
                        row("JBID") = row("JBID")
                        row("SIName") = stockedItem("SIName")
                        row("SIInventoryUOM") = stockedItem("SIInventoryUOM")
                        row("SIInventoryUOMShortName") = stockedItem("SIInventoryUOMShortName")
                        dataSet.VOrders_StockedItems.Rows.Add(row)
                    End If
                End If
            Next
        End If
    End Sub

    Private Function OrderHasMaterial(ByVal BRID As Int32, ByVal MTID As Int32) As Boolean
        For Each material As DataRow In CType(DataRow, dsOrders.VOrdersRow).GetVOrders_MaterialsRows
            If Not material("MTID") Is DBNull.Value Then
                If material("BRID") = BRID And material("MTID") = MTID Then
                    Return True
                End If
            End If
        Next
        Return False
    End Function

    Private Function OrderHasStockedItem(ByVal BRID As Int32, ByVal SIID As Int32) As Boolean
        For Each stockedItem As DataRow In CType(DataRow, dsOrders.VOrdersRow).GetVOrders_StockedItemsRows
            If stockedItem("BRID") = BRID And stockedItem("SIID") = SIID Then
                Return True
            End If
        Next
        Return False
    End Function

    Private Function dvStockedItems_Fitler() As String
        Dim rowFilter As String = ""
        For Each row As DataRow In CType(DataRow, dsOrders.VOrdersRow).GetVOrders_MaterialsRows
            If Not row("MTID") Is DBNull.Value Then
                If rowFilter = "" Then
                    rowFilter = "(MTID = " & row("MTID") & ")"
                Else
                    rowFilter = "(" & rowFilter & ") OR (MTID = " & row("MTID") & ")"
                End If
            End If
        Next
        If rowFilter = "" Then
            Return "False"
        Else
            Return rowFilter
        End If
    End Function

    Private Sub gvOrders_StockedItems_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gvOrders_StockedItems.ValidateRow
        UpdateSIInventoryUOM(e.RowHandle)
    End Sub

    Private Sub UpdateSIInventoryUOM(ByVal rowHandle As Integer)
        If Not (gvOrders_StockedItems.GetDataRow(rowHandle)("BRID") Is DBNull.Value Or gvOrders_StockedItems.GetDataRow(rowHandle)("SIID") Is DBNull.Value) Then
            gvOrders_StockedItems.GetDataRow(rowHandle)("SIInventoryUOMShortName") = dataSet.VStockedItems.FindByBRIDSIID(gvOrders_StockedItems.GetDataRow(rowHandle)("BRID"), gvOrders_StockedItems.GetDataRow(rowHandle)("SIID")).SIInventoryUOMShortName
        Else
            gvOrders_StockedItems.GetDataRow(rowHandle)("SIInventoryUOMShortName") = ""
            gvOrders_StockedItems.GetDataRow(rowHandle)("BRID") = jobBRIDInventory
        End If
    End Sub

    Private Sub btnRemoveStockedItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveStockedItem.Click
        If Not CType(dgOrder_StockedItems.FocusedView, DevExpress.XtraGrid.Views.Grid.GridView).GetSelectedRows Is Nothing Then
            If Not CType(dgOrder_StockedItems.FocusedView, DevExpress.XtraGrid.Views.Grid.GridView).GetDataRow(CType(dgOrder_StockedItems.FocusedView, DevExpress.XtraGrid.Views.Grid.GridView).GetSelectedRows(0)) Is Nothing Then
                CType(dgOrder_StockedItems.FocusedView, DevExpress.XtraGrid.Views.Grid.GridView).GetDataRow(CType(dgOrder_StockedItems.FocusedView, DevExpress.XtraGrid.Views.Grid.GridView).GetSelectedRows(0)).Delete()
            End If
        End If
    End Sub

#End Region

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        ShowHelpTopic(Me, "JobBookingsTerms.html#InternalOrders")
    End Sub
End Class

