Imports System.Data.SqlClient

Public Class frmMaterial
    Inherits DevExpress.XtraEditors.XtraForm

    Private DataRow As DataRow
    Public BRID As Int32
    Public MTID As Int32
    Public MGID As Int32

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return SqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            SqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private hTransaction As SqlClient.SqlTransaction
    Private Property Transaction() As SqlClient.SqlTransaction
        Get
            Return hTransaction
        End Get
        Set(ByVal Value As SqlClient.SqlTransaction)
            hTransaction = Value
            Power.Library.Library.ApplyTransactionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Public Shared Function Add(ByVal BRID As Int32, ByVal MGID As Int32) As frmMaterial
        Dim gui As New frmMaterial

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            .BRID = BRID
            .MGID = MGID

            .Transaction = .SqlConnection.BeginTransaction(IsolationLevel.ReadUncommitted)
            .FillPreliminaryData()
            .MTID = spNew_Material(.BRID, MGID, .Transaction)
            .FillMGIDDependantData()

            If DataAccess.spExecLockRequest("sp_GetMaterialLock", .BRID, .MTID, .Transaction) Then

                .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = .BRID
                .SqlDataAdapter.SelectCommand.Parameters("@MTID").Value = .MTID
                .SqlDataAdapter.Fill(.DsGTMS)
                .DataRow = .DsGTMS.Materials(0)

                .FillData()

            Else
                Throw New ObjectLockedException
            End If
        End With

        Return gui
    End Function

    Public Shared Function Edit(ByVal BRID As Int32, ByRef MTID As Int64) As frmMaterial
        Dim gui As New frmMaterial

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            .BRID = BRID

            .Transaction = .SqlConnection.BeginTransaction(IsolationLevel.ReadUncommitted)
            .FillPreliminaryData()
            .MTID = MTID
            .MGID = DataAccess.MGID(BRID, MTID, .Transaction)
            .FillMGIDDependantData()

            If DataAccess.spExecLockRequest("sp_GetMaterialLock", .BRID, .MTID, .Transaction) Then

                .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = .BRID
                .SqlDataAdapter.SelectCommand.Parameters("@MTID").Value = .MTID
                .SqlDataAdapter.Fill(.DsGTMS)
                .DataRow = .DsGTMS.Materials(0)

                .FillData()

            Else
                Throw New ObjectLockedException
            End If
        End With

        Return gui
    End Function

#Region " Windows Form Designer generated code "

    ' FOR NEW
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents DsGTMS As WindowsApplication.dsGTMS
    Friend WithEvents daMaterialsCosts As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents dvMaterialsCosts As System.Data.DataView
    Friend WithEvents daMaterialGroups_Branch As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents dgStockedItems As DevExpress.XtraGrid.GridControl
    Friend WithEvents daStockedItems As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents gvStockedItems As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtSIConversion As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RadioGroup2 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtMTName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents colMCFromDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnRemoveMaterialsCost As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveSheetSize As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colSIConversionToPrimary As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSIName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SqlSelectCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand6 As System.Data.SqlClient.SqlCommand
    Friend WithEvents colMTPrimaryUOMShortName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dgCosts As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvCosts As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colMCPurchaseCost As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tpProperties As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tpCost As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tpStockedItems As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tcTabs As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents txtMTPrimaryUOM As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daUOM As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DsPrimaryUOM As WindowsApplication.dsUOM
    Friend WithEvents DsInventoryUOM As WindowsApplication.dsUOM
    Friend WithEvents txtMTInventoryUOM As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents colMTPrimaryUOM As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtCost As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents lblMTInventoryUOM As System.Windows.Forms.Label
    Friend WithEvents rgMTIsCoreMaterial As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents txtMCFromDate As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents btnHelp As DevExpress.XtraEditors.SimpleButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim configurationAppSettings As System.Configuration.AppSettingsReader = New System.Configuration.AppSettingsReader
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMaterial))
        Me.RadioGroup2 = New DevExpress.XtraEditors.RadioGroup
        Me.DsGTMS = New WindowsApplication.dsGTMS
        Me.Label3 = New System.Windows.Forms.Label
        Me.rgMTIsCoreMaterial = New DevExpress.XtraEditors.RadioGroup
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.dvMaterialsCosts = New System.Data.DataView
        Me.dgStockedItems = New DevExpress.XtraGrid.GridControl
        Me.gvStockedItems = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colSIConversionToPrimary = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtSIConversion = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colMTPrimaryUOMShortName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSIName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.daMaterialsCosts = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.daMaterialGroups_Branch = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand
        Me.daStockedItems = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand6 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand6 = New System.Data.SqlClient.SqlCommand
        Me.tcTabs = New DevExpress.XtraTab.XtraTabControl
        Me.tpProperties = New DevExpress.XtraTab.XtraTabPage
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtMTInventoryUOM = New DevExpress.XtraEditors.LookUpEdit
        Me.DsInventoryUOM = New WindowsApplication.dsUOM
        Me.lblMTInventoryUOM = New System.Windows.Forms.Label
        Me.txtMTPrimaryUOM = New DevExpress.XtraEditors.LookUpEdit
        Me.DsPrimaryUOM = New WindowsApplication.dsUOM
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtMTName = New DevExpress.XtraEditors.TextEdit
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl
        Me.tpCost = New DevExpress.XtraTab.XtraTabPage
        Me.dgCosts = New DevExpress.XtraGrid.GridControl
        Me.gvCosts = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colMCFromDate = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtMCFromDate = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.colMCPurchaseCost = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtCost = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colMTPrimaryUOM = New DevExpress.XtraGrid.Columns.GridColumn
        Me.btnRemoveMaterialsCost = New DevExpress.XtraEditors.SimpleButton
        Me.tpStockedItems = New DevExpress.XtraTab.XtraTabPage
        Me.btnRemoveSheetSize = New DevExpress.XtraEditors.SimpleButton
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.btnHelp = New DevExpress.XtraEditors.SimpleButton
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.daUOM = New System.Data.SqlClient.SqlDataAdapter
        CType(Me.RadioGroup2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgMTIsCoreMaterial.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dvMaterialsCosts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgStockedItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvStockedItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSIConversion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tcTabs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tcTabs.SuspendLayout()
        Me.tpProperties.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.txtMTInventoryUOM.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsInventoryUOM, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMTPrimaryUOM.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsPrimaryUOM, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMTName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        Me.tpCost.SuspendLayout()
        CType(Me.dgCosts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvCosts, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMCFromDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCost, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpStockedItems.SuspendLayout()
        Me.SuspendLayout()
        '
        'RadioGroup2
        '
        Me.RadioGroup2.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Materials.MTDiscontinued"))
        Me.RadioGroup2.Location = New System.Drawing.Point(24, 64)
        Me.RadioGroup2.Name = "RadioGroup2"
        '
        'RadioGroup2.Properties
        '
        Me.RadioGroup2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.RadioGroup2.Properties.Appearance.Options.UseBackColor = True
        Me.RadioGroup2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.RadioGroup2.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "Make this material available to use (current)"), New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "Discontinue this material (non current)")})
        Me.RadioGroup2.Size = New System.Drawing.Size(248, 40)
        Me.RadioGroup2.TabIndex = 1
        '
        'DsGTMS
        '
        Me.DsGTMS.DataSetName = "dsGTMS"
        Me.DsGTMS.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.Location = New System.Drawing.Point(8, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(448, 48)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Setting a material as discontinued allows you to remove this material from the li" & _
        "st of available material, without destroying existing data, including jobs that " & _
        "have used this material."
        '
        'rgMTIsCoreMaterial
        '
        Me.rgMTIsCoreMaterial.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Materials.MTIsCoreMaterial"))
        Me.rgMTIsCoreMaterial.Location = New System.Drawing.Point(24, 112)
        Me.rgMTIsCoreMaterial.Name = "rgMTIsCoreMaterial"
        '
        'rgMTIsCoreMaterial.Properties
        '
        Me.rgMTIsCoreMaterial.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.rgMTIsCoreMaterial.Properties.Appearance.Options.UseBackColor = True
        Me.rgMTIsCoreMaterial.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.rgMTIsCoreMaterial.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(True, "Estimated"), New DevExpress.XtraEditors.Controls.RadioGroupItem(False, "Invoiced Directly")})
        Me.rgMTIsCoreMaterial.Size = New System.Drawing.Size(160, 40)
        Me.rgMTIsCoreMaterial.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(440, 80)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Some materials are kept on hand to be used in Jobs. These materials will not be i" & _
        "nvoiced by the supplier directly for a particular job. Sometimes material costs " & _
        "will need to be estimated. Will this material be invoiced directly by the suppli" & _
        "er for particular jobs or estimated from materials on hand? Choosing Estimated m" & _
        "eans that by default these items will appear in the Job Financials material cost" & _
        "s to remind you to make an entry. If both options could apply, choose Estimated." & _
        ""
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(8, 8)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 21)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Name:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dvMaterialsCosts
        '
        Me.dvMaterialsCosts.Table = Me.DsGTMS.VMaterialsCosts
        '
        'dgStockedItems
        '
        Me.dgStockedItems.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgStockedItems.DataSource = Me.DsGTMS.VStockedItems
        '
        'dgStockedItems.EmbeddedNavigator
        '
        Me.dgStockedItems.EmbeddedNavigator.Name = ""
        Me.dgStockedItems.Location = New System.Drawing.Point(8, 16)
        Me.dgStockedItems.MainView = Me.gvStockedItems
        Me.dgStockedItems.Name = "dgStockedItems"
        Me.dgStockedItems.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtSIConversion})
        Me.dgStockedItems.Size = New System.Drawing.Size(445, 387)
        Me.dgStockedItems.Styles.AddReplace("CardBorder", New DevExpress.Utils.ViewStyleEx("CardBorder", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.InactiveBorder, System.Drawing.SystemColors.WindowFrame, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("BandPanelBackground", New DevExpress.Utils.ViewStyleEx("BandPanelBackground", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ControlDark, System.Drawing.Color.DarkSalmon, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("EmptySpace", New DevExpress.Utils.ViewStyleEx("EmptySpace", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("FieldValue", New DevExpress.Utils.ViewStyleEx("FieldValue", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("BandPanel", New DevExpress.Utils.ViewStyleEx("BandPanel", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("CardButton", New DevExpress.Utils.ViewStyleEx("CardButton", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("FocusedCardCaption", New DevExpress.Utils.ViewStyleEx("FocusedCardCaption", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ActiveCaption, System.Drawing.SystemColors.ActiveCaptionText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("CardCaption", New DevExpress.Utils.ViewStyleEx("CardCaption", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.InactiveCaption, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("HeaderPanelBackground", New DevExpress.Utils.ViewStyleEx("HeaderPanelBackground", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ControlDark, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("SeparatorLine", New DevExpress.Utils.ViewStyleEx("SeparatorLine", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ActiveBorder, System.Drawing.SystemColors.ActiveBorder, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("FieldCaption", New DevExpress.Utils.ViewStyleEx("FieldCaption", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Top, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.TabIndex = 0
        Me.dgStockedItems.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvStockedItems})
        '
        'gvStockedItems
        '
        Me.gvStockedItems.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvStockedItems.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvStockedItems.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvStockedItems.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvStockedItems.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvStockedItems.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvStockedItems.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvStockedItems.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvStockedItems.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvStockedItems.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvStockedItems.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvStockedItems.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvStockedItems.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvStockedItems.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvStockedItems.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvStockedItems.Appearance.VertLine.Options.UseBackColor = True
        Me.gvStockedItems.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSIConversionToPrimary, Me.colMTPrimaryUOMShortName, Me.colSIName})
        Me.gvStockedItems.GridControl = Me.dgStockedItems
        Me.gvStockedItems.Name = "gvStockedItems"
        Me.gvStockedItems.OptionsCustomization.AllowFilter = False
        Me.gvStockedItems.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvStockedItems.OptionsView.ShowGroupPanel = False
        Me.gvStockedItems.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colSIConversionToPrimary, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colSIConversionToPrimary
        '
        Me.colSIConversionToPrimary.Caption = "Sheet Size"
        Me.colSIConversionToPrimary.ColumnEdit = Me.txtSIConversion
        Me.colSIConversionToPrimary.FieldName = "SIConversionToPrimary"
        Me.colSIConversionToPrimary.Name = "colSIConversionToPrimary"
        Me.colSIConversionToPrimary.Visible = True
        Me.colSIConversionToPrimary.VisibleIndex = 0
        '
        'txtSIConversion
        '
        Me.txtSIConversion.AutoHeight = False
        Me.txtSIConversion.Name = "txtSIConversion"
        '
        'colMTPrimaryUOMShortName
        '
        Me.colMTPrimaryUOMShortName.FieldName = "MTPrimaryUOMShortName"
        Me.colMTPrimaryUOMShortName.Name = "colMTPrimaryUOMShortName"
        Me.colMTPrimaryUOMShortName.OptionsColumn.AllowEdit = False
        Me.colMTPrimaryUOMShortName.OptionsColumn.AllowFocus = False
        Me.colMTPrimaryUOMShortName.OptionsColumn.ReadOnly = True
        Me.colMTPrimaryUOMShortName.Visible = True
        Me.colMTPrimaryUOMShortName.VisibleIndex = 1
        Me.colMTPrimaryUOMShortName.Width = 42
        '
        'colSIName
        '
        Me.colSIName.Caption = "Display Name"
        Me.colSIName.FieldName = "SIName"
        Me.colSIName.Name = "colSIName"
        Me.colSIName.OptionsColumn.AllowEdit = False
        Me.colSIName.OptionsColumn.AllowFocus = False
        Me.colSIName.OptionsColumn.ReadOnly = True
        Me.colSIName.Visible = True
        Me.colSIName.VisibleIndex = 2
        Me.colSIName.Width = 237
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = CType(configurationAppSettings.GetValue("SqlConnection.ConnectionString", GetType(System.String)), String)
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Materials", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("MTID", "MTID"), New System.Data.Common.DataColumnMapping("MTName", "MTName"), New System.Data.Common.DataColumnMapping("MTInventoryUOM", "MTInventoryUOM"), New System.Data.Common.DataColumnMapping("MTPrimaryUOM", "MTPrimaryUOM"), New System.Data.Common.DataColumnMapping("MTDiscontinued", "MTDiscontinued"), New System.Data.Common.DataColumnMapping("MTStocked", "MTStocked"), New System.Data.Common.DataColumnMapping("MGID", "MGID"), New System.Data.Common.DataColumnMapping("MTIsCoreMaterial", "MTIsCoreMaterial")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Materials WHERE (BRID = @Original_BRID) AND (MTID = @Original_MTID) A" & _
        "ND (MGID = @Original_MGID OR @Original_MGID IS NULL AND MGID IS NULL) AND (MTDis" & _
        "continued = @Original_MTDiscontinued OR @Original_MTDiscontinued IS NULL AND MTD" & _
        "iscontinued IS NULL) AND (MTInventoryUOM = @Original_MTInventoryUOM OR @Original" & _
        "_MTInventoryUOM IS NULL AND MTInventoryUOM IS NULL) AND (MTIsCoreMaterial = @Ori" & _
        "ginal_MTIsCoreMaterial OR @Original_MTIsCoreMaterial IS NULL AND MTIsCoreMateria" & _
        "l IS NULL) AND (MTName = @Original_MTName OR @Original_MTName IS NULL AND MTName" & _
        " IS NULL) AND (MTPrimaryUOM = @Original_MTPrimaryUOM OR @Original_MTPrimaryUOM I" & _
        "S NULL AND MTPrimaryUOM IS NULL) AND (MTStocked = @Original_MTStocked OR @Origin" & _
        "al_MTStocked IS NULL AND MTStocked IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTDiscontinued", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTDiscontinued", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTInventoryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTInventoryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTIsCoreMaterial", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTIsCoreMaterial", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTPrimaryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTPrimaryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTStocked", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTStocked", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Materials(BRID, MTName, MTInventoryUOM, MTPrimaryUOM, MTDiscontinued," & _
        " MTStocked, MGID, MTIsCoreMaterial) VALUES (@BRID, @MTName, @MTInventoryUOM, @MT" & _
        "PrimaryUOM, @MTDiscontinued, @MTStocked, @MGID, @MTIsCoreMaterial); SELECT BRID," & _
        " MTID, MTName, MTInventoryUOM, MTPrimaryUOM, MTDiscontinued, MTStocked, MGID, MT" & _
        "IsCoreMaterial FROM Materials WHERE (BRID = @BRID) AND (MTID = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTName", System.Data.SqlDbType.VarChar, 50, "MTName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTInventoryUOM", System.Data.SqlDbType.VarChar, 2, "MTInventoryUOM"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTPrimaryUOM", System.Data.SqlDbType.VarChar, 2, "MTPrimaryUOM"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTDiscontinued", System.Data.SqlDbType.Bit, 1, "MTDiscontinued"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTStocked", System.Data.SqlDbType.Bit, 1, "MTStocked"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTIsCoreMaterial", System.Data.SqlDbType.Bit, 1, "MTIsCoreMaterial"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, MTID, MTName, MTInventoryUOM, MTPrimaryUOM, MTDiscontinued, MTStocke" & _
        "d, MGID, MTIsCoreMaterial FROM Materials WHERE (BRID = @BRID) AND (MTID = @MTID)" & _
        ""
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Materials SET BRID = @BRID, MTName = @MTName, MTInventoryUOM = @MTInventor" & _
        "yUOM, MTPrimaryUOM = @MTPrimaryUOM, MTDiscontinued = @MTDiscontinued, MTStocked " & _
        "= @MTStocked, MGID = @MGID, MTIsCoreMaterial = @MTIsCoreMaterial WHERE (BRID = @" & _
        "Original_BRID) AND (MTID = @Original_MTID) AND (MGID = @Original_MGID OR @Origin" & _
        "al_MGID IS NULL AND MGID IS NULL) AND (MTDiscontinued = @Original_MTDiscontinued" & _
        " OR @Original_MTDiscontinued IS NULL AND MTDiscontinued IS NULL) AND (MTInventor" & _
        "yUOM = @Original_MTInventoryUOM OR @Original_MTInventoryUOM IS NULL AND MTInvent" & _
        "oryUOM IS NULL) AND (MTIsCoreMaterial = @Original_MTIsCoreMaterial OR @Original_" & _
        "MTIsCoreMaterial IS NULL AND MTIsCoreMaterial IS NULL) AND (MTName = @Original_M" & _
        "TName OR @Original_MTName IS NULL AND MTName IS NULL) AND (MTPrimaryUOM = @Origi" & _
        "nal_MTPrimaryUOM OR @Original_MTPrimaryUOM IS NULL AND MTPrimaryUOM IS NULL) AND" & _
        " (MTStocked = @Original_MTStocked OR @Original_MTStocked IS NULL AND MTStocked I" & _
        "S NULL); SELECT BRID, MTID, MTName, MTInventoryUOM, MTPrimaryUOM, MTDiscontinued" & _
        ", MTStocked, MGID, MTIsCoreMaterial FROM Materials WHERE (BRID = @BRID) AND (MTI" & _
        "D = @MTID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTName", System.Data.SqlDbType.VarChar, 50, "MTName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTInventoryUOM", System.Data.SqlDbType.VarChar, 2, "MTInventoryUOM"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTPrimaryUOM", System.Data.SqlDbType.VarChar, 2, "MTPrimaryUOM"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTDiscontinued", System.Data.SqlDbType.Bit, 1, "MTDiscontinued"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTStocked", System.Data.SqlDbType.Bit, 1, "MTStocked"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTIsCoreMaterial", System.Data.SqlDbType.Bit, 1, "MTIsCoreMaterial"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTDiscontinued", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTDiscontinued", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTInventoryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTInventoryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTIsCoreMaterial", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTIsCoreMaterial", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTPrimaryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTPrimaryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTStocked", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTStocked", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        '
        'daMaterialsCosts
        '
        Me.daMaterialsCosts.DeleteCommand = Me.SqlDeleteCommand3
        Me.daMaterialsCosts.InsertCommand = Me.SqlInsertCommand3
        Me.daMaterialsCosts.SelectCommand = Me.SqlSelectCommand3
        Me.daMaterialsCosts.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VMaterialsCosts", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("MTID", "MTID"), New System.Data.Common.DataColumnMapping("MCFromDate", "MCFromDate"), New System.Data.Common.DataColumnMapping("MCPurchaseCost", "MCPurchaseCost")})})
        Me.daMaterialsCosts.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM MaterialsCosts WHERE (BRID = @Original_BRID) AND (MCFromDate = @Origi" & _
        "nal_MCFromDate) AND (MTID = @Original_MTID) AND (MCPurchaseCost = @Original_MCPu" & _
        "rchaseCost OR @Original_MCPurchaseCost IS NULL AND MCPurchaseCost IS NULL)"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MCFromDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MCFromDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MCPurchaseCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MCPurchaseCost", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO MaterialsCosts (BRID, MTID, MCFromDate, MCPurchaseCost) VALUES (@BRID" & _
        ", @MTID, @MCFromDate, @MCPurchaseCost); SELECT * FROM VMaterialsCosts WHERE (BRI" & _
        "D = @BRID) AND (MCFromDate = @MCFromDate) AND (MTID = @MTID)"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MCFromDate", System.Data.SqlDbType.DateTime, 8, "MCFromDate"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MCPurchaseCost", System.Data.SqlDbType.Money, 8, "MCPurchaseCost"))
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT * FROM VMaterialsCosts WHERE (BRID = @BRID) AND (MTID = @MTID)"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection
        Me.SqlSelectCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Current, "0"))
        Me.SqlSelectCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTID", System.Data.DataRowVersion.Current, "0"))
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE MaterialsCosts SET BRID = @BRID, MTID = @MTID, MCFromDate = @MCFromDate, M" & _
        "CPurchaseCost = @MCPurchaseCost WHERE (BRID = @Original_BRID) AND (MCFromDate = " & _
        "@Original_MCFromDate) AND (MTID = @Original_MTID) AND (MCPurchaseCost = @Origina" & _
        "l_MCPurchaseCost OR @Original_MCPurchaseCost IS NULL AND MCPurchaseCost IS NULL)" & _
        "; SELECT * FROM VMaterialsCosts WHERE (BRID = @BRID) AND (MCFromDate = @MCFromDa" & _
        "te) AND (MTID = @MTID)"
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MCFromDate", System.Data.SqlDbType.DateTime, 8, "MCFromDate"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MCPurchaseCost", System.Data.SqlDbType.Money, 8, "MCPurchaseCost"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MCFromDate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MCFromDate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MCPurchaseCost", System.Data.SqlDbType.Money, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MCPurchaseCost", System.Data.DataRowVersion.Original, Nothing))
        '
        'daMaterialGroups_Branch
        '
        Me.daMaterialGroups_Branch.DeleteCommand = Me.SqlDeleteCommand4
        Me.daMaterialGroups_Branch.InsertCommand = Me.SqlInsertCommand4
        Me.daMaterialGroups_Branch.SelectCommand = Me.SqlSelectCommand4
        Me.daMaterialGroups_Branch.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VMaterialGroups_Branch", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("MGID", "MGID"), New System.Data.Common.DataColumnMapping("MGBInventoryUOM", "MGBInventoryUOM"), New System.Data.Common.DataColumnMapping("MGBPrimaryUOM", "MGBPrimaryUOM"), New System.Data.Common.DataColumnMapping("MGBStocked", "MGBStocked"), New System.Data.Common.DataColumnMapping("MGBDiscontinued", "MGBDiscontinued")})})
        Me.daMaterialGroups_Branch.UpdateCommand = Me.SqlUpdateCommand4
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = "DELETE FROM MaterialGroups_Branch WHERE (BRID = @Original_BRID) AND (MGID = @Orig" & _
        "inal_MGID) AND (MGBDiscontinued = @Original_MGBDiscontinued OR @Original_MGBDisc" & _
        "ontinued IS NULL AND MGBDiscontinued IS NULL) AND (MGBInventoryUOM = @Original_M" & _
        "GBInventoryUOM OR @Original_MGBInventoryUOM IS NULL AND MGBInventoryUOM IS NULL)" & _
        " AND (MGBPrimaryUOM = @Original_MGBPrimaryUOM OR @Original_MGBPrimaryUOM IS NULL" & _
        " AND MGBPrimaryUOM IS NULL) AND (MGBStocked = @Original_MGBStocked OR @Original_" & _
        "MGBStocked IS NULL AND MGBStocked IS NULL)"
        Me.SqlDeleteCommand4.Connection = Me.SqlConnection
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGBDiscontinued", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGBDiscontinued", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGBInventoryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGBInventoryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGBPrimaryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGBPrimaryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGBStocked", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGBStocked", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = "INSERT INTO MaterialGroups_Branch (BRID, MGID, MGBInventoryUOM, MGBPrimaryUOM, MG" & _
        "BStocked, MGBDiscontinued) VALUES (@BRID, @MGID, @MGBInventoryUOM, @MGBPrimaryUO" & _
        "M, @MGBStocked, @MGBDiscontinued); SELECT BRID, MGID, MGBInventoryUOM, MGBPrimar" & _
        "yUOM, MGBStocked, MGName, MGControlledAtHeadOffice FROM VMaterialGroups_Branch W" & _
        "HERE (BRID = @BRID) AND (MGID = @MGID)"
        Me.SqlInsertCommand4.Connection = Me.SqlConnection
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGBInventoryUOM", System.Data.SqlDbType.VarChar, 2, "MGBInventoryUOM"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGBPrimaryUOM", System.Data.SqlDbType.VarChar, 2, "MGBPrimaryUOM"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGBStocked", System.Data.SqlDbType.Bit, 1, "MGBStocked"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGBDiscontinued", System.Data.SqlDbType.Bit, 1, "MGBDiscontinued"))
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT BRID, MGID, MGBInventoryUOM, MGBPrimaryUOM, MGBStocked, MGName, MGControll" & _
        "edAtHeadOffice FROM VMaterialGroups_Branch WHERE (MGID = @MGID) AND (BRID = @BRI" & _
        "D)"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection
        Me.SqlSelectCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        Me.SqlSelectCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = "UPDATE MaterialGroups_Branch SET BRID = @BRID, MGID = @MGID, MGBInventoryUOM = @M" & _
        "GBInventoryUOM, MGBPrimaryUOM = @MGBPrimaryUOM, MGBStocked = @MGBStocked, MGBDis" & _
        "continued = @MGBDiscontinued WHERE (BRID = @Original_BRID) AND (MGID = @Original" & _
        "_MGID) AND (MGBDiscontinued = @Original_MGBDiscontinued OR @Original_MGBDisconti" & _
        "nued IS NULL AND MGBDiscontinued IS NULL) AND (MGBInventoryUOM = @Original_MGBIn" & _
        "ventoryUOM OR @Original_MGBInventoryUOM IS NULL AND MGBInventoryUOM IS NULL) AND" & _
        " (MGBPrimaryUOM = @Original_MGBPrimaryUOM OR @Original_MGBPrimaryUOM IS NULL AND" & _
        " MGBPrimaryUOM IS NULL) AND (MGBStocked = @Original_MGBStocked OR @Original_MGBS" & _
        "tocked IS NULL AND MGBStocked IS NULL); SELECT BRID, MGID, MGBInventoryUOM, MGBP" & _
        "rimaryUOM, MGBStocked, MGName, MGControlledAtHeadOffice FROM VMaterialGroups_Bra" & _
        "nch WHERE (BRID = @BRID) AND (MGID = @MGID)"
        Me.SqlUpdateCommand4.Connection = Me.SqlConnection
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGBInventoryUOM", System.Data.SqlDbType.VarChar, 2, "MGBInventoryUOM"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGBPrimaryUOM", System.Data.SqlDbType.VarChar, 2, "MGBPrimaryUOM"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGBStocked", System.Data.SqlDbType.Bit, 1, "MGBStocked"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGBDiscontinued", System.Data.SqlDbType.Variant))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGBDiscontinued", System.Data.SqlDbType.Variant, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGBInventoryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGBInventoryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGBPrimaryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGBPrimaryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGBStocked", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGBStocked", System.Data.DataRowVersion.Original, Nothing))
        '
        'daStockedItems
        '
        Me.daStockedItems.DeleteCommand = Me.SqlDeleteCommand6
        Me.daStockedItems.InsertCommand = Me.SqlInsertCommand6
        Me.daStockedItems.SelectCommand = Me.SqlSelectCommand6
        Me.daStockedItems.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VStockedItems", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("SIID", "SIID"), New System.Data.Common.DataColumnMapping("MTID", "MTID"), New System.Data.Common.DataColumnMapping("SIConversionToPrimary", "SIConversionToPrimary")})})
        Me.daStockedItems.UpdateCommand = Me.SqlUpdateCommand6
        '
        'SqlDeleteCommand6
        '
        Me.SqlDeleteCommand6.CommandText = "DELETE FROM StockedItems WHERE (BRID = @Original_BRID) AND (SIID = @Original_SIID" & _
        ") AND (MTID = @Original_MTID) AND (SIConversionToPrimary = @Original_SIConversio" & _
        "nToPrimary OR @Original_SIConversionToPrimary IS NULL AND SIConversionToPrimary " & _
        "IS NULL)"
        Me.SqlDeleteCommand6.Connection = Me.SqlConnection
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SIConversionToPrimary", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SIConversionToPrimary", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand6
        '
        Me.SqlInsertCommand6.CommandText = "INSERT INTO StockedItems (BRID, MTID, SIConversionToPrimary) VALUES (@BRID, @MTID" & _
        ", @SIConversionToPrimary); SELECT BRID, SIID, MTID, SIConversionToPrimary, SINam" & _
        "e, MTPrimaryUOMShortName FROM VStockedItems WHERE (BRID = @BRID) AND (SIID = @@I" & _
        "DENTITY)"
        Me.SqlInsertCommand6.Connection = Me.SqlConnection
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        Me.SqlInsertCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIConversionToPrimary", System.Data.SqlDbType.Real, 4, "SIConversionToPrimary"))
        '
        'SqlSelectCommand6
        '
        Me.SqlSelectCommand6.CommandText = "SELECT BRID, SIID, MTID, SIConversionToPrimary, SIName, MTPrimaryUOMShortName FRO" & _
        "M VStockedItems WHERE (BRID = @BRID) AND (MTID = @MTID)"
        Me.SqlSelectCommand6.Connection = Me.SqlConnection
        Me.SqlSelectCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        '
        'SqlUpdateCommand6
        '
        Me.SqlUpdateCommand6.CommandText = "UPDATE StockedItems SET BRID = @BRID, MTID = @MTID, SIConversionToPrimary = @SICo" & _
        "nversionToPrimary WHERE (BRID = @Original_BRID) AND (SIID = @Original_SIID) AND " & _
        "(MTID = @Original_MTID) AND (SIConversionToPrimary = @Original_SIConversionToPri" & _
        "mary OR @Original_SIConversionToPrimary IS NULL AND SIConversionToPrimary IS NUL" & _
        "L); SELECT BRID, SIID, MTID, SIConversionToPrimary, SIName, MTPrimaryUOMShortNam" & _
        "e FROM VStockedItems WHERE (BRID = @BRID) AND (SIID = @SIID)"
        Me.SqlUpdateCommand6.Connection = Me.SqlConnection
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MTID", System.Data.SqlDbType.Int, 4, "MTID"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIConversionToPrimary", System.Data.SqlDbType.Real, 4, "SIConversionToPrimary"))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SIConversionToPrimary", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SIConversionToPrimary", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand6.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIID", System.Data.SqlDbType.Int, 4, "SIID"))
        '
        'tcTabs
        '
        Me.tcTabs.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tcTabs.Location = New System.Drawing.Point(8, 8)
        Me.tcTabs.Name = "tcTabs"
        Me.tcTabs.SelectedTabPage = Me.tpProperties
        Me.tcTabs.Size = New System.Drawing.Size(472, 471)
        Me.tcTabs.TabIndex = 0
        Me.tcTabs.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tpProperties, Me.tpCost, Me.tpStockedItems})
        Me.tcTabs.Text = "XtraTabControl1"
        '
        'tpProperties
        '
        Me.tpProperties.Controls.Add(Me.GroupControl3)
        Me.tpProperties.Controls.Add(Me.txtMTName)
        Me.tpProperties.Controls.Add(Me.GroupControl2)
        Me.tpProperties.Controls.Add(Me.GroupControl1)
        Me.tpProperties.Controls.Add(Me.Label4)
        Me.tpProperties.Name = "tpProperties"
        Me.tpProperties.Size = New System.Drawing.Size(463, 441)
        Me.tpProperties.Text = "Material Properties"
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.Label6)
        Me.GroupControl3.Controls.Add(Me.txtMTInventoryUOM)
        Me.GroupControl3.Controls.Add(Me.lblMTInventoryUOM)
        Me.GroupControl3.Controls.Add(Me.txtMTPrimaryUOM)
        Me.GroupControl3.Controls.Add(Me.Label1)
        Me.GroupControl3.Location = New System.Drawing.Point(8, 312)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(448, 120)
        Me.GroupControl3.TabIndex = 4
        Me.GroupControl3.Text = "Units of Measure"
        '
        'Label6
        '
        Me.Label6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.Location = New System.Drawing.Point(8, 16)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(432, 32)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "The primary unit of measure is used to cost the material into a job. The inventor" & _
        "y unit of measure is used to keep track of the materials stock."
        '
        'txtMTInventoryUOM
        '
        Me.txtMTInventoryUOM.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Materials.MTInventoryUOM"))
        Me.txtMTInventoryUOM.Location = New System.Drawing.Point(160, 88)
        Me.txtMTInventoryUOM.Name = "txtMTInventoryUOM"
        '
        'txtMTInventoryUOM.Properties
        '
        Me.txtMTInventoryUOM.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtMTInventoryUOM.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("UOMLongName")})
        Me.txtMTInventoryUOM.Properties.DataSource = Me.DsInventoryUOM.UOM
        Me.txtMTInventoryUOM.Properties.DisplayMember = "UOMLongName"
        Me.txtMTInventoryUOM.Properties.NullText = "<Group default - >"
        Me.txtMTInventoryUOM.Properties.ShowFooter = False
        Me.txtMTInventoryUOM.Properties.ShowHeader = False
        Me.txtMTInventoryUOM.Properties.ValueMember = "UOMID"
        Me.txtMTInventoryUOM.Size = New System.Drawing.Size(208, 20)
        Me.txtMTInventoryUOM.TabIndex = 4
        '
        'DsInventoryUOM
        '
        Me.DsInventoryUOM.DataSetName = "dsUOM"
        Me.DsInventoryUOM.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'lblMTInventoryUOM
        '
        Me.lblMTInventoryUOM.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMTInventoryUOM.Location = New System.Drawing.Point(8, 88)
        Me.lblMTInventoryUOM.Name = "lblMTInventoryUOM"
        Me.lblMTInventoryUOM.Size = New System.Drawing.Size(144, 21)
        Me.lblMTInventoryUOM.TabIndex = 3
        Me.lblMTInventoryUOM.Text = "Inventory unit of measure:"
        Me.lblMTInventoryUOM.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMTPrimaryUOM
        '
        Me.txtMTPrimaryUOM.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Materials.MTPrimaryUOM"))
        Me.txtMTPrimaryUOM.Location = New System.Drawing.Point(160, 56)
        Me.txtMTPrimaryUOM.Name = "txtMTPrimaryUOM"
        '
        'txtMTPrimaryUOM.Properties
        '
        Me.txtMTPrimaryUOM.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True
        Me.txtMTPrimaryUOM.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtMTPrimaryUOM.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("UOMLongName")})
        Me.txtMTPrimaryUOM.Properties.DataSource = Me.DsPrimaryUOM.UOM
        Me.txtMTPrimaryUOM.Properties.DisplayMember = "UOMLongName"
        Me.txtMTPrimaryUOM.Properties.NullText = "<Group default - >"
        Me.txtMTPrimaryUOM.Properties.ShowFooter = False
        Me.txtMTPrimaryUOM.Properties.ShowHeader = False
        Me.txtMTPrimaryUOM.Properties.ValueMember = "UOMID"
        Me.txtMTPrimaryUOM.Size = New System.Drawing.Size(208, 20)
        Me.txtMTPrimaryUOM.TabIndex = 2
        '
        'DsPrimaryUOM
        '
        Me.DsPrimaryUOM.DataSetName = "dsUOM"
        Me.DsPrimaryUOM.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 56)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(136, 21)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Primary unit of measure:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMTName
        '
        Me.txtMTName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "Materials.MTName"))
        Me.txtMTName.EditValue = ""
        Me.txtMTName.Location = New System.Drawing.Point(64, 8)
        Me.txtMTName.Name = "txtMTName"
        Me.txtMTName.Size = New System.Drawing.Size(336, 20)
        Me.txtMTName.TabIndex = 1
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.Label3)
        Me.GroupControl2.Controls.Add(Me.RadioGroup2)
        Me.GroupControl2.Location = New System.Drawing.Point(8, 200)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(448, 104)
        Me.GroupControl2.TabIndex = 3
        Me.GroupControl2.Text = "Status"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.Label2)
        Me.GroupControl1.Controls.Add(Me.rgMTIsCoreMaterial)
        Me.GroupControl1.Location = New System.Drawing.Point(8, 32)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(448, 160)
        Me.GroupControl1.TabIndex = 2
        Me.GroupControl1.Text = "Estimated or Invoiced Directly"
        '
        'tpCost
        '
        Me.tpCost.Controls.Add(Me.dgCosts)
        Me.tpCost.Controls.Add(Me.btnRemoveMaterialsCost)
        Me.tpCost.Name = "tpCost"
        Me.tpCost.Size = New System.Drawing.Size(463, 441)
        Me.tpCost.Text = "Cost"
        '
        'dgCosts
        '
        Me.dgCosts.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgCosts.DataSource = Me.dvMaterialsCosts
        '
        'dgCosts.EmbeddedNavigator
        '
        Me.dgCosts.EmbeddedNavigator.Name = ""
        Me.dgCosts.Location = New System.Drawing.Point(8, 16)
        Me.dgCosts.MainView = Me.gvCosts
        Me.dgCosts.Name = "dgCosts"
        Me.dgCosts.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtCost, Me.txtMCFromDate})
        Me.dgCosts.Size = New System.Drawing.Size(445, 387)
        Me.dgCosts.TabIndex = 0
        Me.dgCosts.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvCosts})
        '
        'gvCosts
        '
        Me.gvCosts.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvCosts.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvCosts.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvCosts.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvCosts.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvCosts.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvCosts.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvCosts.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvCosts.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvCosts.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvCosts.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvCosts.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvCosts.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvCosts.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvCosts.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvCosts.Appearance.VertLine.Options.UseBackColor = True
        Me.gvCosts.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colMCFromDate, Me.colMCPurchaseCost, Me.colMTPrimaryUOM})
        Me.gvCosts.GridControl = Me.dgCosts
        Me.gvCosts.Name = "gvCosts"
        Me.gvCosts.OptionsCustomization.AllowFilter = False
        Me.gvCosts.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvCosts.OptionsView.ShowGroupPanel = False
        Me.gvCosts.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colMCFromDate, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colMCFromDate
        '
        Me.colMCFromDate.Caption = "From Date"
        Me.colMCFromDate.ColumnEdit = Me.txtMCFromDate
        Me.colMCFromDate.FieldName = "MCFromDate"
        Me.colMCFromDate.Name = "colMCFromDate"
        Me.colMCFromDate.Visible = True
        Me.colMCFromDate.VisibleIndex = 0
        Me.colMCFromDate.Width = 177
        '
        'txtMCFromDate
        '
        Me.txtMCFromDate.AutoHeight = False
        Me.txtMCFromDate.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtMCFromDate.Name = "txtMCFromDate"
        '
        'colMCPurchaseCost
        '
        Me.colMCPurchaseCost.Caption = "Cost"
        Me.colMCPurchaseCost.ColumnEdit = Me.txtCost
        Me.colMCPurchaseCost.FieldName = "MCPurchaseCost"
        Me.colMCPurchaseCost.Name = "colMCPurchaseCost"
        Me.colMCPurchaseCost.Visible = True
        Me.colMCPurchaseCost.VisibleIndex = 1
        Me.colMCPurchaseCost.Width = 193
        '
        'txtCost
        '
        Me.txtCost.AutoHeight = False
        Me.txtCost.DisplayFormat.FormatString = "c"
        Me.txtCost.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCost.EditFormat.FormatString = "c"
        Me.txtCost.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtCost.Name = "txtCost"
        '
        'colMTPrimaryUOM
        '
        Me.colMTPrimaryUOM.Caption = "Per"
        Me.colMTPrimaryUOM.FieldName = "MTPrimaryUOMShortName"
        Me.colMTPrimaryUOM.Name = "colMTPrimaryUOM"
        Me.colMTPrimaryUOM.OptionsColumn.AllowEdit = False
        Me.colMTPrimaryUOM.OptionsColumn.AllowFocus = False
        Me.colMTPrimaryUOM.Visible = True
        Me.colMTPrimaryUOM.VisibleIndex = 2
        '
        'btnRemoveMaterialsCost
        '
        Me.btnRemoveMaterialsCost.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveMaterialsCost.Image = CType(resources.GetObject("btnRemoveMaterialsCost.Image"), System.Drawing.Image)
        Me.btnRemoveMaterialsCost.Location = New System.Drawing.Point(8, 411)
        Me.btnRemoveMaterialsCost.Name = "btnRemoveMaterialsCost"
        Me.btnRemoveMaterialsCost.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveMaterialsCost.TabIndex = 1
        Me.btnRemoveMaterialsCost.Text = "Remove"
        '
        'tpStockedItems
        '
        Me.tpStockedItems.Controls.Add(Me.btnRemoveSheetSize)
        Me.tpStockedItems.Controls.Add(Me.dgStockedItems)
        Me.tpStockedItems.Name = "tpStockedItems"
        Me.tpStockedItems.Size = New System.Drawing.Size(463, 441)
        Me.tpStockedItems.Text = "Sheet Sizes"
        '
        'btnRemoveSheetSize
        '
        Me.btnRemoveSheetSize.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveSheetSize.Image = CType(resources.GetObject("btnRemoveSheetSize.Image"), System.Drawing.Image)
        Me.btnRemoveSheetSize.Location = New System.Drawing.Point(8, 411)
        Me.btnRemoveSheetSize.Name = "btnRemoveSheetSize"
        Me.btnRemoveSheetSize.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveSheetSize.TabIndex = 1
        Me.btnRemoveSheetSize.Text = "Remove"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(320, 487)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(400, 487)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'btnHelp
        '
        Me.btnHelp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnHelp.Image = CType(resources.GetObject("btnHelp.Image"), System.Drawing.Image)
        Me.btnHelp.Location = New System.Drawing.Point(8, 487)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(72, 23)
        Me.btnHelp.TabIndex = 1
        Me.btnHelp.Text = "Help"
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT UOMID, UOMLongName, UOMShortName FROM UOM"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO UOM(UOMID, UOMLongName, UOMShortName) VALUES (@UOMID, @UOMLongName, @" & _
        "UOMShortName); SELECT UOMID, UOMLongName, UOMShortName FROM UOM WHERE (UOMID = @" & _
        "UOMID)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UOMID", System.Data.SqlDbType.VarChar, 2, "UOMID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UOMLongName", System.Data.SqlDbType.VarChar, 50, "UOMLongName"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UOMShortName", System.Data.SqlDbType.VarChar, 50, "UOMShortName"))
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE UOM SET UOMID = @UOMID, UOMLongName = @UOMLongName, UOMShortName = @UOMSho" & _
        "rtName WHERE (UOMID = @Original_UOMID) AND (UOMLongName = @Original_UOMLongName " & _
        "OR @Original_UOMLongName IS NULL AND UOMLongName IS NULL) AND (UOMShortName = @O" & _
        "riginal_UOMShortName OR @Original_UOMShortName IS NULL AND UOMShortName IS NULL)" & _
        "; SELECT UOMID, UOMLongName, UOMShortName FROM UOM WHERE (UOMID = @UOMID)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UOMID", System.Data.SqlDbType.VarChar, 2, "UOMID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UOMLongName", System.Data.SqlDbType.VarChar, 50, "UOMLongName"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UOMShortName", System.Data.SqlDbType.VarChar, 50, "UOMShortName"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UOMID", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UOMID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UOMLongName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UOMLongName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UOMShortName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UOMShortName", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM UOM WHERE (UOMID = @Original_UOMID) AND (UOMLongName = @Original_UOML" & _
        "ongName OR @Original_UOMLongName IS NULL AND UOMLongName IS NULL) AND (UOMShortN" & _
        "ame = @Original_UOMShortName OR @Original_UOMShortName IS NULL AND UOMShortName " & _
        "IS NULL)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UOMID", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UOMID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UOMLongName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UOMLongName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UOMShortName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UOMShortName", System.Data.DataRowVersion.Original, Nothing))
        '
        'daUOM
        '
        Me.daUOM.DeleteCommand = Me.SqlDeleteCommand2
        Me.daUOM.InsertCommand = Me.SqlInsertCommand2
        Me.daUOM.SelectCommand = Me.SqlSelectCommand2
        Me.daUOM.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "UOM", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("UOMID", "UOMID"), New System.Data.Common.DataColumnMapping("UOMLongName", "UOMLongName"), New System.Data.Common.DataColumnMapping("UOMShortName", "UOMShortName")})})
        Me.daUOM.UpdateCommand = Me.SqlUpdateCommand2
        '
        'frmMaterial
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(482, 519)
        Me.Controls.Add(Me.btnHelp)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.tcTabs)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMaterial"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Material"
        CType(Me.RadioGroup2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgMTIsCoreMaterial.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dvMaterialsCosts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgStockedItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvStockedItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSIConversion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tcTabs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tcTabs.ResumeLayout(False)
        Me.tpProperties.ResumeLayout(False)
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.txtMTInventoryUOM.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsInventoryUOM, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMTPrimaryUOM.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsPrimaryUOM, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMTName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.tpCost.ResumeLayout(False)
        CType(Me.dgCosts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvCosts, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMCFromDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCost, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpStockedItems.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnOK.Enabled = Not HasRole("branch_read_only")
    End Sub

    ' This data must be loaded BEFORE the DataRow is loaded (eg lookups etc)
    Private Sub FillPreliminaryData()
    End Sub

    Private Sub FillMGIDDependantData()
        ' Material Group
        daMaterialGroups_Branch.SelectCommand.Parameters("@BRID").Value = BRID
        daMaterialGroups_Branch.SelectCommand.Parameters("@MGID").Value = MGID
        daMaterialGroups_Branch.Fill(DsGTMS)

        ' UOM
        daUOM.Fill(DsPrimaryUOM)
        daUOM.Fill(DsInventoryUOM)
        Dim PrimaryNullText As String = "<Group default - " & DsPrimaryUOM.UOM.FindByUOMID(DsGTMS.VMaterialGroups_Branch(0).MGBPrimaryUOM).UOMLongName & ">"
        Dim InventoryNullText As String = "<Group default - " & DsInventoryUOM.UOM.FindByUOMID(DsGTMS.VMaterialGroups_Branch(0).MGBInventoryUOM).UOMLongName & ">"
        DsPrimaryUOM.UOM.AddUOMRow("", PrimaryNullText, DsPrimaryUOM.UOM.FindByUOMID(DsGTMS.VMaterialGroups_Branch(0).MGBPrimaryUOM).UOMShortName)
        DsInventoryUOM.UOM.AddUOMRow("", InventoryNullText, DsInventoryUOM.UOM.FindByUOMID(DsGTMS.VMaterialGroups_Branch(0).MGBInventoryUOM).UOMShortName)
        txtMTPrimaryUOM.Properties.NullText = PrimaryNullText
        txtMTInventoryUOM.Properties.NullText = InventoryNullText
    End Sub

    ' This data must be loaded AFTER the DataRow is loaded (eg record related data)
    Private Sub FillData()
        ' --- MATERIAL COSTS ---
        daMaterialsCosts.SelectCommand.Parameters("@BRID").Value = BRID
        daMaterialsCosts.SelectCommand.Parameters("@MTID").Value = MTID
        daMaterialsCosts.Fill(DsGTMS)

        ' --- STOCKED ITEMS ---
        daStockedItems.SelectCommand.Parameters("@BRID").Value = BRID
        daStockedItems.SelectCommand.Parameters("@MTID").Value = MTID
        daStockedItems.Fill(DsGTMS)

        CustomizeScreen()
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        ' EndEdit() to end editing the dataset record so that we can update
        DataRow.EndEdit()
        If ValidateForm() Then
            SqlDataAdapter.Update(DsGTMS)
            daMaterialsCosts.Update(DsGTMS)
            daStockedItems.Update(DsGTMS)

            DataAccess.spExecLockRequest("sp_ReleaseMaterialLock", BRID, MTID, Transaction)
            Transaction.Commit()
            SqlConnection.Close()

            OK = True
            Me.Close()
        End If
    End Sub
    Private Function ValidateForm() As Boolean
        If DataRow("MTName") Is DBNull.Value Then
            Message.ShowMessage("You must enter a Name.", MessageBoxIcon.Exclamation)
            Return False
        End If
        If (Not AllowUserToEnterCosts()) And dvMaterialsCosts.Count > 0 Then
            If DevExpress.XtraEditors.XtraMessageBox.Show("Costs cannot be entered against a material which has the primary unit of measure is set to currency a currency.  " & _
                    "You must either change the primary unit of measure or delete the costs against this material." & _
                    vbNewLine & vbNewLine & "Would you like the program to automatically delete the costs against this material?  " & _
                    "Click 'No' to go back and change the primary unit of measure.", "Franchise Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) = MsgBoxResult.Yes Then
                Do Until dvMaterialsCosts.Count = 0
                    dvMaterialsCosts.Delete(0)
                Loop
            Else
                Return False
            End If
        End If
        Return True
    End Function
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private HasChanges As Boolean = False
    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            DataRow.EndEdit()
            If HasChanges Or DsGTMS.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If response = MsgBoxResult.Yes Then
                DataAccess.spExecLockRequest("sp_ReleaseMaterialLock", BRID, MTID, Transaction)
                Transaction.Rollback()
                SqlConnection.Close()
            Else
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub CustomizeScreen()
        Dim IsStocked As Boolean
        If DataRow("MTStocked") Is DBNull.Value Then
            IsStocked = DsGTMS.VMaterialGroups_Branch(0).MGBStocked
        Else
            IsStocked = DataRow("MTStocked")
        End If
        If DsGTMS.VMaterialGroups_Branch(0).MGControlledAtHeadOffice Then
            txtMTName.Enabled = False
            txtMTPrimaryUOM.Enabled = False
            txtMTInventoryUOM.Enabled = False
            dgStockedItems.Enabled = False
            dgStockedItems.BackColor = System.Drawing.SystemColors.Control
            btnRemoveSheetSize.Enabled = False
        End If
        If Not IsStocked Then
            txtMTInventoryUOM.Visible = False
            lblMTInventoryUOM.Visible = False
            tcTabs.TabPages.Remove(tpStockedItems)
        End If
        Dim MGName As String = ""
        If Not DsGTMS.VMaterialGroups_Branch(0)("MGName") Is DBNull.Value Then
            MGName = DsGTMS.VMaterialGroups_Branch(0)("MGName")
        End If
        Me.Text = MGName
    End Sub

#Region " Material Cost "

    Private Sub gvCosts_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gvCosts.InitNewRow
        gvCosts.GetDataRow(e.RowHandle)("BRID") = BRID
        gvCosts.GetDataRow(e.RowHandle)("MTID") = MTID
    End Sub

    Private Sub gvCosts_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gvCosts.RowUpdated
        ' Update costs and refresh
        gvCosts.UpdateCurrentRow()
        daMaterialsCosts.Update(DsGTMS)
        HasChanges = True
    End Sub

    Private Sub gvCosts_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gvCosts.KeyDown
        If e.KeyCode = Keys.Delete Then
            If gvCosts.SelectedRowsCount > 0 Then
                gvCosts.GetDataRow(gvCosts.GetSelectedRows(0)).Delete()
            End If
        End If
    End Sub

    Public ReadOnly Property SelectedMaterialCost() As DataRow
        Get
            If Not gvCosts.GetSelectedRows Is Nothing Then
                Return gvCosts.GetDataRow(gvCosts.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub btnRemoveMaterialsCost_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveMaterialsCost.Click
        If Not SelectedMaterialCost Is Nothing Then
            SelectedMaterialCost.Delete()
        End If
    End Sub

#End Region

#Region " Sheet Sizes "

    Private Sub gvStockedItems_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gvStockedItems.InitNewRow
        gvStockedItems.GetDataRow(e.RowHandle)("BRID") = BRID
        gvStockedItems.GetDataRow(e.RowHandle)("MTID") = MTID
    End Sub

    Private Sub gvStockedItems_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gvStockedItems.RowUpdated
        ' Update sheet sizes and refresh
        gvStockedItems.UpdateCurrentRow()
        daStockedItems.Update(DsGTMS)
        HasChanges = True
    End Sub

    Private Sub gvStockedItems_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles gvStockedItems.CellValueChanged
        If gvStockedItems.GetDataRow(e.RowHandle)("SIConversionToPrimary") Is DBNull.Value Then
            If ExecuteScalar("SELECT dbo.SIIsUsed(@BRID, @SIID)", CommandType.Text, New SqlParameter() {New SqlParameter("@BRID", BRID), New SqlParameter("@SIID", SelectedSheetSize("SIID"))}, Transaction) Then
                Message.ShowMessage("You cannot delete this stocked item because it has either been used in a job or is currently being held in stock.", MessageBoxIcon.Exclamation)
                gvStockedItems.GetDataRow(e.RowHandle).CancelEdit()
            Else
                gvStockedItems.GetDataRow(e.RowHandle).Delete()
            End If
        End If
    End Sub

    Private Sub gvStockedItems_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gvStockedItems.KeyDown
        If e.KeyCode = Keys.Delete Then
            If gvStockedItems.SelectedRowsCount > 0 Then
                If ExecuteScalar("SELECT dbo.SIIsUsed(@BRID, @SIID)", CommandType.Text, New SqlParameter() {New SqlParameter("@BRID", BRID), New SqlParameter("@SIID", SelectedSheetSize("SIID"))}, Transaction) Then
                    Message.ShowMessage("You cannot delete this stocked item because it has either been used in a job or is currently being held in stock.", MessageBoxIcon.Exclamation)
                Else
                    gvStockedItems.GetDataRow(gvStockedItems.GetSelectedRows(0)).Delete()
                End If
            End If
        End If
    End Sub

    Public ReadOnly Property SelectedSheetSize() As DataRow
        Get
            If Not gvStockedItems.GetSelectedRows Is Nothing Then
                Return gvStockedItems.GetDataRow(gvStockedItems.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub btnRemoveSheetSize_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveSheetSize.Click
        If Not SelectedSheetSize Is Nothing Then
            If ExecuteScalar("SELECT dbo.SIIsUsed(@BRID, @SIID)", CommandType.Text, New SqlParameter() {New SqlParameter("@BRID", BRID), New SqlParameter("@SIID", SelectedSheetSize("SIID"))}, Transaction) Then
                Message.ShowMessage("You cannot delete this stocked item because it has either been used in a job or is currently being held in stock.", MessageBoxIcon.Exclamation)
            Else
                SelectedSheetSize.Delete()
            End If
        End If
    End Sub

#End Region

    Private Sub Decimal_ParseEditValue(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) _
    Handles txtCost.ParseEditValue, txtSIConversion.ParseEditValue
        Format.Decimal_ParseEditValue(sender, e)
    End Sub

    Private Sub Text_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) _
    Handles txtMTName.ParseEditValue
        Format.Text_ParseEditValue(sender, e)
    End Sub

    Private Sub Date_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtMCFromDate.Validating
        Library.DateEdit_Validating(sender, e)
    End Sub

    Private Sub txtUOM_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
        Handles txtMTPrimaryUOM.EditValueChanged, txtMTInventoryUOM.EditValueChanged
        If TypeOf CType(sender, DevExpress.XtraEditors.LookUpEdit).EditValue Is String Then
            If CType(sender, DevExpress.XtraEditors.LookUpEdit).EditValue = "" Then
                CType(sender, DevExpress.XtraEditors.LookUpEdit).EditValue = DBNull.Value
            End If
        End If
        If AllowUserToEnterCosts() Then
            dgCosts.Enabled = True
            btnRemoveMaterialsCost.Enabled = True
        Else
            If dvMaterialsCosts.Count > 0 Then
                If DevExpress.XtraEditors.XtraMessageBox.Show("Costs cannot be entered against a material which has the primary unit of measure is set to currency a currency.  " & _
                        "You must either change the primary unit of measure or delete the costs against this material." & _
                        vbNewLine & vbNewLine & "Would you like the program to automatically delete the costs against this material?  " & _
                        "Click 'No' to go back and change the primary unit of measure.", "Franchise Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) = MsgBoxResult.Yes Then
                    Do Until dvMaterialsCosts.Count = 0
                        dvMaterialsCosts.Delete(0)
                        daMaterialsCosts.Update(DsGTMS)
                        HasChanges = True
                    Loop
                Else
                    ' Rollback change and exit the sub
                    txtMTPrimaryUOM.EditValue = DataRow("MTPrimaryUOM")
                    Exit Sub
                End If
            End If
            dgCosts.Enabled = False
            btnRemoveMaterialsCost.Enabled = False
        End If
    End Sub

    Private Function AllowUserToEnterCosts() As Boolean
        If txtMTPrimaryUOM.EditValue Is DBNull.Value Then
            If Trim(DsGTMS.VMaterialGroups_Branch(0).MGBPrimaryUOM) = "$" Then
                AllowUserToEnterCosts = False
            Else
                AllowUserToEnterCosts = True
            End If
        Else
            If Trim(txtMTPrimaryUOM.EditValue) = "$" Then
                AllowUserToEnterCosts = False
            Else
                AllowUserToEnterCosts = True
            End If
        End If
    End Function

    Private Sub txtUOM_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) _
    Handles txtMTInventoryUOM.KeyDown, txtMTPrimaryUOM.KeyDown
        If e.KeyCode = Keys.Delete Then
            CType(sender, DevExpress.XtraEditors.LookUpEdit).EditValue = Nothing
        End If
    End Sub

    Private Sub txtUOM_Validated(ByVal sender As Object, ByVal e As System.EventArgs) _
    Handles txtMTInventoryUOM.Validated, txtMTPrimaryUOM.Validated
        ' Update main screen info
        DataRow.EndEdit()
        SqlDataAdapter.Update(DsGTMS)
        HasChanges = True
        daMaterialsCosts.Fill(DsGTMS)
        daStockedItems.Fill(DsGTMS)
    End Sub

    Private Sub txtMTName_Validated(ByVal sender As Object, ByVal e As System.EventArgs) _
    Handles txtMTName.Validated
        ' Update main screen info
        DataRow.EndEdit()
        SqlDataAdapter.Update(DsGTMS)
        HasChanges = True
        daStockedItems.Fill(DsGTMS)
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHelp.Click
        ShowHelpTopic(Me, "MaterialsTerms.html")
    End Sub

    Private Sub GroupControl1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles GroupControl1.Paint

    End Sub
End Class