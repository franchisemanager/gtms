Public Class frmStockAdjustment
    Inherits DevExpress.XtraEditors.XtraForm

    Private DataRow As DataRow
    Public BRID As Int32
    Public SAID As Int64

    Private Property Connection() As SqlClient.SqlConnection
        Get
            Return SqlConnection
        End Get
        Set(ByVal Value As SqlClient.SqlConnection)
            SqlConnection = Value
            Power.Library.Library.ApplyConnectionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Private hTransaction As SqlClient.SqlTransaction
    Private Property Transaction() As SqlClient.SqlTransaction
        Get
            Return hTransaction
        End Get
        Set(ByVal Value As SqlClient.SqlTransaction)
            hTransaction = Value
            Power.Library.Library.ApplyTransactionToAllDataAdapters(Value, Me)
        End Set
    End Property

    Public Shared Function Add(ByVal BRID As Int32) As frmStockAdjustment
        Dim gui As New frmStockAdjustment

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            .BRID = BRID
            .LoadTreeViews()

            .Transaction = .SqlConnection.BeginTransaction(IsolationLevel.ReadUncommitted)
            .FillPreliminaryData()
            .SAID = spNew_StockAdjustment(.BRID, .Transaction)

            If DataAccess.spExecLockRequest("sp_GetStockAdjustmentLock", .BRID, .SAID, .Transaction) Then

                .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = .BRID
                .SqlDataAdapter.SelectCommand.Parameters("@SAID").Value = .SAID
                .SqlDataAdapter.Fill(.DsGTMS)
                .DataRow = .DsGTMS.StockAdjustments(0)

                .FillData()

            Else
                Throw New ObjectLockedException
            End If
        End With

        Return gui
    End Function

    Public Shared Function Edit(ByVal BRID As Int32, ByRef SAID As Int64) As frmStockAdjustment
        Dim gui As New frmStockAdjustment

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            .BRID = BRID
            .SAID = SAID
            .LoadTreeViews()

            .Transaction = .SqlConnection.BeginTransaction(IsolationLevel.ReadUncommitted)
            .FillPreliminaryData()

            If DataAccess.spExecLockRequest("sp_GetStockAdjustmentLock", .BRID, .SAID, .Transaction) Then

                .SqlDataAdapter.SelectCommand.Parameters("@BRID").Value = BRID
                .SqlDataAdapter.SelectCommand.Parameters("@SAID").Value = SAID
                .SqlDataAdapter.Fill(.DsGTMS)
                .DataRow = .DsGTMS.StockAdjustments(0)

                .FillData()

            Else
                Throw New ObjectLockedException
            End If
        End With

        Return gui
    End Function

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents DsGTMS As WindowsApplication.dsGTMS
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daSATypes As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents txtSADate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dgMaterials_StockAdjustments As DevExpress.XtraGrid.GridControl
    Friend WithEvents txtSAType As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents gvMaterials_StockAdjustments As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btnAddMaterial As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveMaterial As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnHelp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents daStockedItems_StockAdjustments As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand5 As System.Data.SqlClient.SqlCommand
    Friend WithEvents tvStockedItems As Power.Forms.TreeView
    Friend WithEvents colSSInventoryAmount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSIInventoryUOMShortName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSIName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtSSInventoryAmount As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmStockAdjustment))
        Dim configurationAppSettings As System.Configuration.AppSettingsReader = New System.Configuration.AppSettingsReader
        Me.btnRemoveMaterial = New DevExpress.XtraEditors.SimpleButton
        Me.btnAddMaterial = New DevExpress.XtraEditors.SimpleButton
        Me.dgMaterials_StockAdjustments = New DevExpress.XtraGrid.GridControl
        Me.DsGTMS = New WindowsApplication.dsGTMS
        Me.gvMaterials_StockAdjustments = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colSSInventoryAmount = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtSSInventoryAmount = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colSIInventoryUOMShortName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSIName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtSAType = New DevExpress.XtraEditors.LookUpEdit
        Me.txtSADate = New DevExpress.XtraEditors.DateEdit
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand
        Me.daSATypes = New System.Data.SqlClient.SqlDataAdapter
        Me.btnHelp = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage
        Me.tvStockedItems = New Power.Forms.TreeView
        Me.daStockedItems_StockAdjustments = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand5 = New System.Data.SqlClient.SqlCommand
        CType(Me.dgMaterials_StockAdjustments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvMaterials_StockAdjustments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSSInventoryAmount, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSAType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSADate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnRemoveMaterial
        '
        Me.btnRemoveMaterial.Image = CType(resources.GetObject("btnRemoveMaterial.Image"), System.Drawing.Image)
        Me.btnRemoveMaterial.Location = New System.Drawing.Point(264, 224)
        Me.btnRemoveMaterial.Name = "btnRemoveMaterial"
        Me.btnRemoveMaterial.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveMaterial.TabIndex = 7
        Me.btnRemoveMaterial.Text = "Remove"
        '
        'btnAddMaterial
        '
        Me.btnAddMaterial.Image = CType(resources.GetObject("btnAddMaterial.Image"), System.Drawing.Image)
        Me.btnAddMaterial.ImageAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.btnAddMaterial.Location = New System.Drawing.Point(264, 192)
        Me.btnAddMaterial.Name = "btnAddMaterial"
        Me.btnAddMaterial.Size = New System.Drawing.Size(72, 23)
        Me.btnAddMaterial.TabIndex = 6
        Me.btnAddMaterial.Text = "Add"
        '
        'dgMaterials_StockAdjustments
        '
        Me.dgMaterials_StockAdjustments.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgMaterials_StockAdjustments.DataSource = Me.DsGTMS.VStockAdjustments_StockedItems
        '
        'dgMaterials_StockAdjustments.EmbeddedNavigator
        '
        Me.dgMaterials_StockAdjustments.EmbeddedNavigator.Name = ""
        Me.dgMaterials_StockAdjustments.Location = New System.Drawing.Point(352, 80)
        Me.dgMaterials_StockAdjustments.MainView = Me.gvMaterials_StockAdjustments
        Me.dgMaterials_StockAdjustments.Name = "dgMaterials_StockAdjustments"
        Me.dgMaterials_StockAdjustments.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtSSInventoryAmount})
        Me.dgMaterials_StockAdjustments.Size = New System.Drawing.Size(352, 360)
        Me.dgMaterials_StockAdjustments.TabIndex = 9
        Me.dgMaterials_StockAdjustments.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvMaterials_StockAdjustments})
        '
        'DsGTMS
        '
        Me.DsGTMS.DataSetName = "dsGTMS"
        Me.DsGTMS.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'gvMaterials_StockAdjustments
        '
        Me.gvMaterials_StockAdjustments.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvMaterials_StockAdjustments.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvMaterials_StockAdjustments.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvMaterials_StockAdjustments.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvMaterials_StockAdjustments.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvMaterials_StockAdjustments.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvMaterials_StockAdjustments.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvMaterials_StockAdjustments.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvMaterials_StockAdjustments.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvMaterials_StockAdjustments.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvMaterials_StockAdjustments.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvMaterials_StockAdjustments.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvMaterials_StockAdjustments.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvMaterials_StockAdjustments.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvMaterials_StockAdjustments.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvMaterials_StockAdjustments.Appearance.VertLine.Options.UseBackColor = True
        Me.gvMaterials_StockAdjustments.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSSInventoryAmount, Me.colSIInventoryUOMShortName, Me.colSIName})
        Me.gvMaterials_StockAdjustments.GridControl = Me.dgMaterials_StockAdjustments
        Me.gvMaterials_StockAdjustments.Name = "gvMaterials_StockAdjustments"
        Me.gvMaterials_StockAdjustments.OptionsCustomization.AllowFilter = False
        Me.gvMaterials_StockAdjustments.OptionsView.ShowGroupPanel = False
        Me.gvMaterials_StockAdjustments.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colSIName, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colSSInventoryAmount
        '
        Me.colSSInventoryAmount.Caption = "Amount"
        Me.colSSInventoryAmount.ColumnEdit = Me.txtSSInventoryAmount
        Me.colSSInventoryAmount.FieldName = "SSInventoryAmount"
        Me.colSSInventoryAmount.Name = "colSSInventoryAmount"
        Me.colSSInventoryAmount.Visible = True
        Me.colSSInventoryAmount.VisibleIndex = 1
        Me.colSSInventoryAmount.Width = 64
        '
        'txtSSInventoryAmount
        '
        Me.txtSSInventoryAmount.AutoHeight = False
        Me.txtSSInventoryAmount.DisplayFormat.FormatString = "#0"
        Me.txtSSInventoryAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtSSInventoryAmount.EditFormat.FormatString = "#0"
        Me.txtSSInventoryAmount.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtSSInventoryAmount.Name = "txtSSInventoryAmount"
        '
        'colSIInventoryUOMShortName
        '
        Me.colSIInventoryUOMShortName.FieldName = "SIInventoryUOMShortName"
        Me.colSIInventoryUOMShortName.Name = "colSIInventoryUOMShortName"
        Me.colSIInventoryUOMShortName.OptionsColumn.AllowEdit = False
        Me.colSIInventoryUOMShortName.OptionsColumn.AllowFocus = False
        Me.colSIInventoryUOMShortName.Visible = True
        Me.colSIInventoryUOMShortName.VisibleIndex = 2
        Me.colSIInventoryUOMShortName.Width = 69
        '
        'colSIName
        '
        Me.colSIName.Caption = "Stocked item"
        Me.colSIName.FieldName = "SIName"
        Me.colSIName.Name = "colSIName"
        Me.colSIName.OptionsColumn.AllowEdit = False
        Me.colSIName.OptionsColumn.AllowFocus = False
        Me.colSIName.Visible = True
        Me.colSIName.VisibleIndex = 0
        Me.colSIName.Width = 165
        '
        'txtSAType
        '
        Me.txtSAType.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "StockAdjustments.SAType"))
        Me.txtSAType.Location = New System.Drawing.Point(424, 16)
        Me.txtSAType.Name = "txtSAType"
        '
        'txtSAType.Properties
        '
        Me.txtSAType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtSAType.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("SATypeDisplay")})
        Me.txtSAType.Properties.DataSource = Me.DsGTMS.SATypes
        Me.txtSAType.Properties.DisplayMember = "SATypeDisplay"
        Me.txtSAType.Properties.NullText = ""
        Me.txtSAType.Properties.ShowFooter = False
        Me.txtSAType.Properties.ShowHeader = False
        Me.txtSAType.Properties.ValueMember = "SAType"
        Me.txtSAType.Size = New System.Drawing.Size(184, 20)
        Me.txtSAType.TabIndex = 3
        '
        'txtSADate
        '
        Me.txtSADate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsGTMS, "StockAdjustments.SADate"))
        Me.txtSADate.EditValue = New Date(2005, 8, 2, 0, 0, 0, 0)
        Me.txtSADate.Location = New System.Drawing.Point(88, 16)
        Me.txtSADate.Name = "txtSADate"
        '
        'txtSADate.Properties
        '
        Me.txtSADate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtSADate.Size = New System.Drawing.Size(144, 20)
        Me.txtSADate.TabIndex = 1
        '
        'Label22
        '
        Me.Label22.Location = New System.Drawing.Point(8, 56)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(232, 21)
        Me.Label22.TabIndex = 4
        Me.Label22.Text = "Available stocked items:"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label23
        '
        Me.Label23.Location = New System.Drawing.Point(352, 56)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(240, 21)
        Me.Label23.TabIndex = 8
        Me.Label23.Text = "Stocked items in adjustment:"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(16, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 21)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Date:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(288, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(128, 21)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Stock Adjustment Type:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = CType(configurationAppSettings.GetValue("SqlConnection.ConnectionString", GetType(System.String)), String)
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "StockAdjustments", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("SAID", "SAID"), New System.Data.Common.DataColumnMapping("SADate", "SADate"), New System.Data.Common.DataColumnMapping("SAType", "SAType")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM StockAdjustments WHERE (BRID = @Original_BRID) AND (SAID = @Original_" & _
        "SAID) AND (SADate = @Original_SADate OR @Original_SADate IS NULL AND SADate IS N" & _
        "ULL) AND (SAType = @Original_SAType OR @Original_SAType IS NULL AND SAType IS NU" & _
        "LL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SAID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SAID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SADate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SADate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SAType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SAType", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO StockAdjustments(BRID, SADate, SAType) VALUES (@BRID, @SADate, @SATyp" & _
        "e); SELECT BRID, SAID, SADate, SAType FROM StockAdjustments WHERE (BRID = @BRID)" & _
        " AND (SAID = @@IDENTITY)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SADate", System.Data.SqlDbType.DateTime, 8, "SADate"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SAType", System.Data.SqlDbType.VarChar, 2, "SAType"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT BRID, SAID, SADate, SAType, rowguid FROM StockAdjustments WHERE (BRID = @B" & _
        "RID) AND (SAID = @SAID)"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SAID", System.Data.SqlDbType.BigInt, 8, "SAID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE StockAdjustments SET BRID = @BRID, SADate = @SADate, SAType = @SAType WHER" & _
        "E (BRID = @Original_BRID) AND (SAID = @Original_SAID) AND (SADate = @Original_SA" & _
        "Date OR @Original_SADate IS NULL AND SADate IS NULL) AND (SAType = @Original_SAT" & _
        "ype OR @Original_SAType IS NULL AND SAType IS NULL); SELECT BRID, SAID, SADate, " & _
        "SAType FROM StockAdjustments WHERE (BRID = @BRID) AND (SAID = @SAID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SADate", System.Data.SqlDbType.DateTime, 8, "SADate"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SAType", System.Data.SqlDbType.VarChar, 2, "SAType"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SAID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SAID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SADate", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SADate", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SAType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SAType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SAID", System.Data.SqlDbType.BigInt, 8, "SAID"))
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT SAType, SATypeDisplay FROM SATypes"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = "INSERT INTO SATypes(SAType, SATypeDisplay) VALUES (@SAType, @SATypeDisplay); SELE" & _
        "CT SAType, SATypeDisplay FROM SATypes WHERE (SAType = @SAType)"
        Me.SqlInsertCommand4.Connection = Me.SqlConnection
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SAType", System.Data.SqlDbType.VarChar, 2, "SAType"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SATypeDisplay", System.Data.SqlDbType.VarChar, 50, "SATypeDisplay"))
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = "UPDATE SATypes SET SAType = @SAType, SATypeDisplay = @SATypeDisplay WHERE (SAType" & _
        " = @Original_SAType) AND (SATypeDisplay = @Original_SATypeDisplay OR @Original_S" & _
        "ATypeDisplay IS NULL AND SATypeDisplay IS NULL); SELECT SAType, SATypeDisplay FR" & _
        "OM SATypes WHERE (SAType = @SAType)"
        Me.SqlUpdateCommand4.Connection = Me.SqlConnection
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SAType", System.Data.SqlDbType.VarChar, 2, "SAType"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SATypeDisplay", System.Data.SqlDbType.VarChar, 50, "SATypeDisplay"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SAType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SAType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SATypeDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SATypeDisplay", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = "DELETE FROM SATypes WHERE (SAType = @Original_SAType) AND (SATypeDisplay = @Origi" & _
        "nal_SATypeDisplay OR @Original_SATypeDisplay IS NULL AND SATypeDisplay IS NULL)"
        Me.SqlDeleteCommand4.Connection = Me.SqlConnection
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SAType", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SAType", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SATypeDisplay", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SATypeDisplay", System.Data.DataRowVersion.Original, Nothing))
        '
        'daSATypes
        '
        Me.daSATypes.DeleteCommand = Me.SqlDeleteCommand4
        Me.daSATypes.InsertCommand = Me.SqlInsertCommand4
        Me.daSATypes.SelectCommand = Me.SqlSelectCommand4
        Me.daSATypes.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "SATypes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("SAType", "SAType"), New System.Data.Common.DataColumnMapping("SATypeDisplay", "SATypeDisplay")})})
        Me.daSATypes.UpdateCommand = Me.SqlUpdateCommand4
        '
        'btnHelp
        '
        Me.btnHelp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnHelp.Image = CType(resources.GetObject("btnHelp.Image"), System.Drawing.Image)
        Me.btnHelp.Location = New System.Drawing.Point(8, 496)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(72, 24)
        Me.btnHelp.TabIndex = 1
        Me.btnHelp.Text = "Help"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(656, 496)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(576, 496)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "OK"
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.XtraTabControl1.Location = New System.Drawing.Point(8, 8)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(720, 480)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1})
        Me.XtraTabControl1.Text = "XtraTabControl1"
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.tvStockedItems)
        Me.XtraTabPage1.Controls.Add(Me.txtSAType)
        Me.XtraTabPage1.Controls.Add(Me.txtSADate)
        Me.XtraTabPage1.Controls.Add(Me.dgMaterials_StockAdjustments)
        Me.XtraTabPage1.Controls.Add(Me.Label1)
        Me.XtraTabPage1.Controls.Add(Me.Label2)
        Me.XtraTabPage1.Controls.Add(Me.btnRemoveMaterial)
        Me.XtraTabPage1.Controls.Add(Me.btnAddMaterial)
        Me.XtraTabPage1.Controls.Add(Me.Label22)
        Me.XtraTabPage1.Controls.Add(Me.Label23)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(714, 454)
        Me.XtraTabPage1.Text = "Stock Adjustment Properties"
        '
        'tvStockedItems
        '
        Me.tvStockedItems.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.tvStockedItems.Connection = Me.SqlConnection
        Me.tvStockedItems.HideSelection = False
        Me.tvStockedItems.ImageIndex = -1
        Me.tvStockedItems.Location = New System.Drawing.Point(8, 80)
        Me.tvStockedItems.Name = "tvStockedItems"
        Me.tvStockedItems.SelectedImageIndex = -1
        Me.tvStockedItems.Size = New System.Drawing.Size(240, 360)
        Me.tvStockedItems.TabIndex = 5
        '
        'daStockedItems_StockAdjustments
        '
        Me.daStockedItems_StockAdjustments.DeleteCommand = Me.SqlDeleteCommand5
        Me.daStockedItems_StockAdjustments.InsertCommand = Me.SqlInsertCommand5
        Me.daStockedItems_StockAdjustments.SelectCommand = Me.SqlSelectCommand5
        Me.daStockedItems_StockAdjustments.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VStockAdjustments_StockedItems", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("BRID", "BRID"), New System.Data.Common.DataColumnMapping("SAID", "SAID"), New System.Data.Common.DataColumnMapping("SIID", "SIID"), New System.Data.Common.DataColumnMapping("SSInventoryAmount", "SSInventoryAmount")})})
        Me.daStockedItems_StockAdjustments.UpdateCommand = Me.SqlUpdateCommand5
        '
        'SqlDeleteCommand5
        '
        Me.SqlDeleteCommand5.CommandText = "DELETE FROM StockAdjustments_StockedItems WHERE (BRID = @Original_BRID) AND (SAID" & _
        " = @Original_SAID) AND (SIID = @Original_SIID) AND (SSInventoryAmount = @Origina" & _
        "l_SSInventoryAmount OR @Original_SSInventoryAmount IS NULL AND SSInventoryAmount" & _
        " IS NULL)"
        Me.SqlDeleteCommand5.Connection = Me.SqlConnection
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SAID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SAID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "SSInventoryAmount", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand5
        '
        Me.SqlInsertCommand5.CommandText = "INSERT INTO StockAdjustments_StockedItems (BRID, SAID, SIID, SSInventoryAmount) V" & _
        "ALUES (@BRID, @SAID, @SIID, @SSInventoryAmount); SELECT BRID, SAID, SIID, SSInve" & _
        "ntoryAmount, SIInventoryUOMShortName, SIName FROM VStockAdjustments_StockedItems" & _
        " WHERE (BRID = @BRID) AND (SAID = @SAID) AND (SIID = @SIID)"
        Me.SqlInsertCommand5.Connection = Me.SqlConnection
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SAID", System.Data.SqlDbType.BigInt, 8, "SAID"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIID", System.Data.SqlDbType.Int, 4, "SIID"))
        Me.SqlInsertCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "SSInventoryAmount", System.Data.DataRowVersion.Current, Nothing))
        '
        'SqlSelectCommand5
        '
        Me.SqlSelectCommand5.CommandText = "SELECT BRID, SAID, SIID, SSInventoryAmount, SIInventoryUOMShortName, SIName FROM " & _
        "VStockAdjustments_StockedItems WHERE (BRID = @BRID) AND (SAID = @SAID)"
        Me.SqlSelectCommand5.Connection = Me.SqlConnection
        Me.SqlSelectCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlSelectCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SAID", System.Data.SqlDbType.BigInt, 8, "SAID"))
        '
        'SqlUpdateCommand5
        '
        Me.SqlUpdateCommand5.CommandText = "UPDATE StockAdjustments_StockedItems SET BRID = @BRID, SAID = @SAID, SIID = @SIID" & _
        ", SSInventoryAmount = @SSInventoryAmount WHERE (BRID = @Original_BRID) AND (SAID" & _
        " = @Original_SAID) AND (SIID = @Original_SIID) AND (SSInventoryAmount = @Origina" & _
        "l_SSInventoryAmount OR @Original_SSInventoryAmount IS NULL AND SSInventoryAmount" & _
        " IS NULL); SELECT BRID, SAID, SIID, SSInventoryAmount, SIInventoryUOMShortName, " & _
        "SIName FROM VStockAdjustments_StockedItems WHERE (BRID = @BRID) AND (SAID = @SAI" & _
        "D) AND (SIID = @SIID)"
        Me.SqlUpdateCommand5.Connection = Me.SqlConnection
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BRID", System.Data.SqlDbType.Int, 4, "BRID"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SAID", System.Data.SqlDbType.BigInt, 8, "SAID"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SIID", System.Data.SqlDbType.Int, 4, "SIID"))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "SSInventoryAmount", System.Data.DataRowVersion.Current, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_BRID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "BRID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SAID", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SAID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand5.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SSInventoryAmount", System.Data.SqlDbType.Decimal, 9, System.Data.ParameterDirection.Input, False, CType(18, Byte), CType(3, Byte), "SSInventoryAmount", System.Data.DataRowVersion.Original, Nothing))
        '
        'frmStockAdjustment
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(738, 528)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnHelp)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmStockAdjustment"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Stock Adjustment"
        CType(Me.dgMaterials_StockAdjustments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsGTMS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvMaterials_StockAdjustments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSSInventoryAmount, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSAType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSADate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnOK.Enabled = Not HasRole("branch_read_only")
    End Sub

    Private Sub LoadTreeViews()
        ' --- STOCKED ITEMS ---
        tvStockedItems.Filter.SetOrAddField("BRID", BRID)
        tvStockedItems.LoadFromStream(System.Reflection.Assembly.LoadFrom(Application.ExecutablePath).GetManifestResourceStream("WindowsApplication.xmlStockedItemsTree.xml"))
        tvStockedItems.ExpandAll()
    End Sub

    Private Sub FillPreliminaryData()
        ' ---  UOMs ---
        daSATypes.Fill(DsGTMS)
    End Sub
    Private Sub FillData()
        ' --- MATERIALS ---
        daStockedItems_StockAdjustments.SelectCommand.Parameters("@BRID").Value = BRID
        daStockedItems_StockAdjustments.SelectCommand.Parameters("@SAID").Value = SAID
        daStockedItems_StockAdjustments.Fill(DsGTMS)
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        ' EndEdit() to end editing the dataset record so that we can update
        DataRow.EndEdit()
        If ValidateForm() Then
            SqlDataAdapter.Update(DsGTMS)
            Me.daStockedItems_StockAdjustments.Update(DsGTMS)

            DataAccess.spExecLockRequest("sp_ReleaseStockAdjustmentLock", BRID, SAID, Transaction)
            Transaction.Commit()
            SqlConnection.Close()

            OK = True
            Me.Close()
        End If
    End Sub
    Private Function ValidateForm() As Boolean
        If DataRow("SADate") Is DBNull.Value Then
            Message.ShowMessage("You must enter a Date.", MessageBoxIcon.Exclamation)
            Return False
        End If
        If DataRow("SAType") Is DBNull.Value Then
            Message.ShowMessage("You must enter a Stock Adjustment Type.", MessageBoxIcon.Exclamation)
            Return False
        End If
        Return True
    End Function
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private HasChanges As Boolean = False
    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            DataRow.EndEdit()
            If HasChanges Or DsGTMS.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If response = MsgBoxResult.Yes Then
                DataAccess.spExecLockRequest("sp_ReleaseStockAdjustmentLock", BRID, SAID, Transaction)
                Transaction.Rollback()
                SqlConnection.Close()
            Else
                e.Cancel = True
            End If
        End If
    End Sub

#Region " Materials "

    Public ReadOnly Property SelectedMaterial() As DataRow
        Get
            If Not gvMaterials_StockAdjustments.GetSelectedRows Is Nothing Then
                Return gvMaterials_StockAdjustments.GetDataRow(gvMaterials_StockAdjustments.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub btnAddMaterial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddMaterial.Click
        Dim row As dsGTMS.VStockAdjustments_StockedItemsRow = DsGTMS.VStockAdjustments_StockedItems.NewVStockAdjustments_StockedItemsRow
        row.BRID = BRID
        row.SAID = SAID
        If CType(tvStockedItems.SelectedNode, Power.Forms.TreeNode).Action = "ADD" Then
            row.SIID = CType(tvStockedItems.SelectedNode, Power.Forms.TreeNode).PKey("SIID")
        ElseIf CType(tvStockedItems.SelectedNode, Power.Forms.TreeNode).Action = "LIST" Then
            Message.ShowMessage("You cannot add this item. You may however select from the list of " & tvStockedItems.SelectedNode.Text & ".", MsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            DsGTMS.VStockAdjustments_StockedItems.AddVStockAdjustments_StockedItemsRow(row)
            daStockedItems_StockAdjustments.Update(DsGTMS)
            HasChanges = True
        Catch ex As System.Data.ConstraintException
            row.Delete()
            Message.ShowMessage(tvStockedItems.SelectedNode.Text & " could not be added, as it already exists in the stock adjustment.", MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub btnRemoveMaterial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveMaterial.Click
        If Not SelectedMaterial Is Nothing Then
            SelectedMaterial.Delete()
        End If
    End Sub

#End Region

    Private Sub Decimal_WholeNumbers_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) _
    Handles txtSSInventoryAmount.ParseEditValue
        Format.WholeNumber_ParseEditValue(sender, e)
    End Sub

    Private Sub Date_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtSADate.Validating
        Library.DateEdit_Validating(sender, e)
    End Sub

    Private Sub btnHelp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHelp.Click
        ShowHelpTopic(Me, "StockTrackingTerms.html")
    End Sub

End Class
