Imports System.Collections.Generic
Imports System.Data.SqlClient

Public Class frmMasterMaterial2
    Inherits DevExpress.XtraEditors.XtraForm

    Private DataRow As DataRow
    Private MaterialGroup_BranchRow As DataRow

    Public Shared Sub Add(ByVal MGID As Int32)
        Dim gui As New frmMasterMaterial2

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            .FillPreliminaryData()
            .FillMGIDDependantData(MGID)

            .DataRow = .dataSet.Master_Materials.NewRow()
            .DataRow("MGID") = MGID
            .dataSet.Master_Materials.Rows.Add(.DataRow)

            gui.ShowDialog()

            .SqlConnection.Close()
        End With
    End Sub

    Public Shared Sub Edit(ByRef MMTID As Int64)
        Dim gui As New frmMasterMaterial2

        With gui
            .SqlConnection.ConnectionString = ConnectionString
            .SqlConnection.Open()

            If DataAccess.spExecLockRequest("sp_GetMasterMaterialLock", MMTID, .SqlConnection) Then

                .FillPreliminaryData()
                .FillMGIDDependantData(MGID(MMTID, .SqlConnection))

                .SqlDataAdapter.SelectCommand.Parameters("@MMTID").Value = MMTID
                .SqlDataAdapter.Fill(.dataSet)
                .DataRow = .dataSet.Master_Materials(0)

                .FillData()

                gui.ShowDialog()

                DataAccess.spExecLockRequest("sp_ReleaseMasterMaterialLock", MMTID, .SqlConnection)
            Else
                Message.CurrentlyAccessed("material")
            End If

            .SqlConnection.Close()
        End With
    End Sub

#Region " Windows Form Designer generated code "

    ' FOR NEW
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SqlConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents SqlDataAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dgStockedItems As DevExpress.XtraGrid.GridControl
    Friend WithEvents daStockedItems As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents gvStockedItems As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtSIConversion As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents txtMTName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRemoveSheetSize As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colSIConversionToPrimary As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSIName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMTPrimaryUOMShortName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tpProperties As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tpStockedItems As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tcTabs As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents txtMTPrimaryUOM As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents daUOM As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents txtMTInventoryUOM As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblMTInventoryUOM As System.Windows.Forms.Label
    Friend WithEvents daMaterialGroups As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
    Friend WithEvents dataSet As WindowsApplication.dsMasterMaterials
    Friend WithEvents DsPrimaryUOM As WindowsApplication.dsUOM
    Friend WithEvents DsInventoryUOM As WindowsApplication.dsUOM
    Friend WithEvents chkMMTAutoGenerateSIName As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMasterMaterial2))
        Me.dataSet = New WindowsApplication.dsMasterMaterials
        Me.Label4 = New System.Windows.Forms.Label
        Me.dgStockedItems = New DevExpress.XtraGrid.GridControl
        Me.gvStockedItems = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colSIConversionToPrimary = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtSIConversion = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.colMTPrimaryUOMShortName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colSIName = New DevExpress.XtraGrid.Columns.GridColumn
        Me.SqlConnection = New System.Data.SqlClient.SqlConnection
        Me.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.daMaterialGroups = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand
        Me.daStockedItems = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand
        Me.tcTabs = New DevExpress.XtraTab.XtraTabControl
        Me.tpProperties = New DevExpress.XtraTab.XtraTabPage
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtMTInventoryUOM = New DevExpress.XtraEditors.LookUpEdit
        Me.DsInventoryUOM = New WindowsApplication.dsUOM
        Me.lblMTInventoryUOM = New System.Windows.Forms.Label
        Me.txtMTPrimaryUOM = New DevExpress.XtraEditors.LookUpEdit
        Me.DsPrimaryUOM = New WindowsApplication.dsUOM
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtMTName = New DevExpress.XtraEditors.TextEdit
        Me.tpStockedItems = New DevExpress.XtraTab.XtraTabPage
        Me.chkMMTAutoGenerateSIName = New DevExpress.XtraEditors.CheckEdit
        Me.btnRemoveSheetSize = New DevExpress.XtraEditors.SimpleButton
        Me.btnOK = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.daUOM = New System.Data.SqlClient.SqlDataAdapter
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgStockedItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvStockedItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSIConversion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tcTabs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tcTabs.SuspendLayout()
        Me.tpProperties.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.txtMTInventoryUOM.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsInventoryUOM, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMTPrimaryUOM.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsPrimaryUOM, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMTName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpStockedItems.SuspendLayout()
        CType(Me.chkMMTAutoGenerateSIName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dataSet
        '
        Me.dataSet.DataSetName = "dsMasterMaterials"
        Me.dataSet.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(8, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 21)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Name:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dgStockedItems
        '
        Me.dgStockedItems.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgStockedItems.DataSource = Me.dataSet.VMaster_StockedItems
        '
        'dgStockedItems.EmbeddedNavigator
        '
        Me.dgStockedItems.EmbeddedNavigator.Name = ""
        Me.dgStockedItems.Location = New System.Drawing.Point(8, 16)
        Me.dgStockedItems.MainView = Me.gvStockedItems
        Me.dgStockedItems.Name = "dgStockedItems"
        Me.dgStockedItems.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtSIConversion})
        Me.dgStockedItems.Size = New System.Drawing.Size(395, 188)
        Me.dgStockedItems.Styles.AddReplace("CardBorder", New DevExpress.Utils.ViewStyleEx("CardBorder", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.InactiveBorder, System.Drawing.SystemColors.WindowFrame, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("BandPanelBackground", New DevExpress.Utils.ViewStyleEx("BandPanelBackground", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ControlDark, System.Drawing.Color.DarkSalmon, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("EmptySpace", New DevExpress.Utils.ViewStyleEx("EmptySpace", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("FieldValue", New DevExpress.Utils.ViewStyleEx("FieldValue", "", System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("BandPanel", New DevExpress.Utils.ViewStyleEx("BandPanel", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("CardButton", New DevExpress.Utils.ViewStyleEx("CardButton", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("FocusedCardCaption", New DevExpress.Utils.ViewStyleEx("FocusedCardCaption", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ActiveCaption, System.Drawing.SystemColors.ActiveCaptionText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("CardCaption", New DevExpress.Utils.ViewStyleEx("CardCaption", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.InactiveCaption, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("HeaderPanelBackground", New DevExpress.Utils.ViewStyleEx("HeaderPanelBackground", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ControlDark, System.Drawing.SystemColors.ControlText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("SeparatorLine", New DevExpress.Utils.ViewStyleEx("SeparatorLine", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ActiveBorder, System.Drawing.SystemColors.ActiveBorder, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.Styles.AddReplace("FieldCaption", New DevExpress.Utils.ViewStyleEx("FieldCaption", "", "", True, False, False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Utils.VertAlignment.Top, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.WindowText, System.Drawing.Color.Empty, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dgStockedItems.TabIndex = 0
        Me.dgStockedItems.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvStockedItems})
        '
        'gvStockedItems
        '
        Me.gvStockedItems.Appearance.FocusedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvStockedItems.Appearance.FocusedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvStockedItems.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gvStockedItems.Appearance.FocusedRow.Options.UseForeColor = True
        Me.gvStockedItems.Appearance.HideSelectionRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvStockedItems.Appearance.HideSelectionRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvStockedItems.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.gvStockedItems.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.gvStockedItems.Appearance.HorzLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvStockedItems.Appearance.HorzLine.Options.UseBackColor = True
        Me.gvStockedItems.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.Window
        Me.gvStockedItems.Appearance.SelectedRow.ForeColor = System.Drawing.SystemColors.WindowText
        Me.gvStockedItems.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gvStockedItems.Appearance.SelectedRow.Options.UseForeColor = True
        Me.gvStockedItems.Appearance.VertLine.BackColor = System.Drawing.SystemColors.Control
        Me.gvStockedItems.Appearance.VertLine.Options.UseBackColor = True
        Me.gvStockedItems.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSIConversionToPrimary, Me.colMTPrimaryUOMShortName, Me.colSIName})
        Me.gvStockedItems.GridControl = Me.dgStockedItems
        Me.gvStockedItems.Name = "gvStockedItems"
        Me.gvStockedItems.OptionsCustomization.AllowFilter = False
        Me.gvStockedItems.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
        Me.gvStockedItems.OptionsView.ShowGroupPanel = False
        Me.gvStockedItems.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colSIConversionToPrimary, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colSIConversionToPrimary
        '
        Me.colSIConversionToPrimary.Caption = "Sheet Size"
        Me.colSIConversionToPrimary.ColumnEdit = Me.txtSIConversion
        Me.colSIConversionToPrimary.FieldName = "MSIConversionToPrimary"
        Me.colSIConversionToPrimary.Name = "colSIConversionToPrimary"
        Me.colSIConversionToPrimary.Visible = True
        Me.colSIConversionToPrimary.VisibleIndex = 0
        '
        'txtSIConversion
        '
        Me.txtSIConversion.AutoHeight = False
        Me.txtSIConversion.Name = "txtSIConversion"
        '
        'colMTPrimaryUOMShortName
        '
        Me.colMTPrimaryUOMShortName.FieldName = "MMTPrimaryUOMShortName"
        Me.colMTPrimaryUOMShortName.Name = "colMTPrimaryUOMShortName"
        Me.colMTPrimaryUOMShortName.OptionsColumn.AllowEdit = False
        Me.colMTPrimaryUOMShortName.OptionsColumn.AllowFocus = False
        Me.colMTPrimaryUOMShortName.OptionsColumn.ReadOnly = True
        Me.colMTPrimaryUOMShortName.Visible = True
        Me.colMTPrimaryUOMShortName.VisibleIndex = 1
        Me.colMTPrimaryUOMShortName.Width = 42
        '
        'colSIName
        '
        Me.colSIName.Caption = "Display Name"
        Me.colSIName.FieldName = "MSIName"
        Me.colSIName.Name = "colSIName"
        Me.colSIName.OptionsColumn.AllowEdit = False
        Me.colSIName.OptionsColumn.AllowFocus = False
        Me.colSIName.OptionsColumn.ReadOnly = True
        Me.colSIName.Visible = True
        Me.colSIName.VisibleIndex = 2
        Me.colSIName.Width = 237
        '
        'SqlConnection
        '
        Me.SqlConnection.ConnectionString = "workstation id=DEV1;packet size=4096;user id=sa;data source=""SERVER\DEV"";persist " & _
        "security info=False;initial catalog=GTMS_DEV"
        '
        'SqlDataAdapter
        '
        Me.SqlDataAdapter.DeleteCommand = Me.SqlDeleteCommand1
        Me.SqlDataAdapter.InsertCommand = Me.SqlInsertCommand1
        Me.SqlDataAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.SqlDataAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Master_Materials", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("MMTID", "MMTID"), New System.Data.Common.DataColumnMapping("MMTName", "MMTName"), New System.Data.Common.DataColumnMapping("MMTInventoryUOM", "MMTInventoryUOM"), New System.Data.Common.DataColumnMapping("MMTPrimaryUOM", "MMTPrimaryUOM"), New System.Data.Common.DataColumnMapping("MMTStocked", "MMTStocked"), New System.Data.Common.DataColumnMapping("MGID", "MGID"), New System.Data.Common.DataColumnMapping("MMTAutoGenerateSIName", "MMTAutoGenerateSIName")})})
        Me.SqlDataAdapter.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Master_Materials WHERE (MMTID = @Original_MMTID) AND (MGID = @Origina" & _
        "l_MGID OR @Original_MGID IS NULL AND MGID IS NULL) AND (MMTAutoGenerateSIName = " & _
        "@Original_MMTAutoGenerateSIName OR @Original_MMTAutoGenerateSIName IS NULL AND M" & _
        "MTAutoGenerateSIName IS NULL) AND (MMTInventoryUOM = @Original_MMTInventoryUOM O" & _
        "R @Original_MMTInventoryUOM IS NULL AND MMTInventoryUOM IS NULL) AND (MMTName = " & _
        "@Original_MMTName OR @Original_MMTName IS NULL AND MMTName IS NULL) AND (MMTPrim" & _
        "aryUOM = @Original_MMTPrimaryUOM OR @Original_MMTPrimaryUOM IS NULL AND MMTPrima" & _
        "ryUOM IS NULL) AND (MMTStocked = @Original_MMTStocked OR @Original_MMTStocked IS" & _
        " NULL AND MMTStocked IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTAutoGenerateSIName", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTAutoGenerateSIName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTInventoryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTInventoryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTPrimaryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTPrimaryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTStocked", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTStocked", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Master_Materials (MMTName, MMTInventoryUOM, MMTPrimaryUOM, MMTStocked" & _
        ", MGID, MMTAutoGenerateSIName) VALUES (@MMTName, @MMTInventoryUOM, @MMTPrimaryUO" & _
        "M, @MMTStocked, @MGID, @MMTAutoGenerateSIName); SELECT MMTID, MMTName, MMTInvent" & _
        "oryUOM, MMTPrimaryUOM, MMTStocked, MGID, MMTAutoGenerateSIName FROM Master_Mater" & _
        "ials WHERE (MMTID = SCOPE_IDENTITY())"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTName", System.Data.SqlDbType.VarChar, 50, "MMTName"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTInventoryUOM", System.Data.SqlDbType.VarChar, 2, "MMTInventoryUOM"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTPrimaryUOM", System.Data.SqlDbType.VarChar, 2, "MMTPrimaryUOM"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTStocked", System.Data.SqlDbType.Bit, 1, "MMTStocked"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTAutoGenerateSIName", System.Data.SqlDbType.Bit, 1, "MMTAutoGenerateSIName"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT MMTID, MMTName, MMTInventoryUOM, MMTPrimaryUOM, MMTStocked, MGID, MMTAutoG" & _
        "enerateSIName FROM Master_Materials WHERE (MMTID = @MMTID)"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTID", System.Data.SqlDbType.Int, 4, "MMTID"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Master_Materials SET MMTName = @MMTName, MMTInventoryUOM = @MMTInventoryUO" & _
        "M, MMTPrimaryUOM = @MMTPrimaryUOM, MMTStocked = @MMTStocked, MGID = @MGID, MMTAu" & _
        "toGenerateSIName = @MMTAutoGenerateSIName WHERE (MMTID = @Original_MMTID) AND (M" & _
        "GID = @Original_MGID OR @Original_MGID IS NULL AND MGID IS NULL) AND (MMTAutoGen" & _
        "erateSIName = @Original_MMTAutoGenerateSIName OR @Original_MMTAutoGenerateSIName" & _
        " IS NULL AND MMTAutoGenerateSIName IS NULL) AND (MMTInventoryUOM = @Original_MMT" & _
        "InventoryUOM OR @Original_MMTInventoryUOM IS NULL AND MMTInventoryUOM IS NULL) A" & _
        "ND (MMTName = @Original_MMTName OR @Original_MMTName IS NULL AND MMTName IS NULL" & _
        ") AND (MMTPrimaryUOM = @Original_MMTPrimaryUOM OR @Original_MMTPrimaryUOM IS NUL" & _
        "L AND MMTPrimaryUOM IS NULL) AND (MMTStocked = @Original_MMTStocked OR @Original" & _
        "_MMTStocked IS NULL AND MMTStocked IS NULL); SELECT MMTID, MMTName, MMTInventory" & _
        "UOM, MMTPrimaryUOM, MMTStocked, MGID, MMTAutoGenerateSIName FROM Master_Material" & _
        "s WHERE (MMTID = @MMTID)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTName", System.Data.SqlDbType.VarChar, 50, "MMTName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTInventoryUOM", System.Data.SqlDbType.VarChar, 2, "MMTInventoryUOM"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTPrimaryUOM", System.Data.SqlDbType.VarChar, 2, "MMTPrimaryUOM"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTStocked", System.Data.SqlDbType.Bit, 1, "MMTStocked"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTAutoGenerateSIName", System.Data.SqlDbType.Bit, 1, "MMTAutoGenerateSIName"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTAutoGenerateSIName", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTAutoGenerateSIName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTInventoryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTInventoryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTPrimaryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTPrimaryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTStocked", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTStocked", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTID", System.Data.SqlDbType.Int, 4, "MMTID"))
        '
        'daMaterialGroups
        '
        Me.daMaterialGroups.DeleteCommand = Me.SqlDeleteCommand4
        Me.daMaterialGroups.InsertCommand = Me.SqlInsertCommand4
        Me.daMaterialGroups.SelectCommand = Me.SqlSelectCommand4
        Me.daMaterialGroups.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "MaterialGroups", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("MGID", "MGID"), New System.Data.Common.DataColumnMapping("MGInventoryUOM", "MGInventoryUOM"), New System.Data.Common.DataColumnMapping("MGPrimaryUOM", "MGPrimaryUOM"), New System.Data.Common.DataColumnMapping("MGStocked", "MGStocked"), New System.Data.Common.DataColumnMapping("MGName", "MGName"), New System.Data.Common.DataColumnMapping("MGControlledAtHeadOffice", "MGControlledAtHeadOffice"), New System.Data.Common.DataColumnMapping("MGAssignToPortion", "MGAssignToPortion")})})
        Me.daMaterialGroups.UpdateCommand = Me.SqlUpdateCommand4
        '
        'SqlDeleteCommand4
        '
        Me.SqlDeleteCommand4.CommandText = "DELETE FROM MaterialGroups WHERE (MGID = @Original_MGID) AND (MGAssignToPortion =" & _
        " @Original_MGAssignToPortion OR @Original_MGAssignToPortion IS NULL AND MGAssign" & _
        "ToPortion IS NULL) AND (MGControlledAtHeadOffice = @Original_MGControlledAtHeadO" & _
        "ffice OR @Original_MGControlledAtHeadOffice IS NULL AND MGControlledAtHeadOffice" & _
        " IS NULL) AND (MGInventoryUOM = @Original_MGInventoryUOM OR @Original_MGInventor" & _
        "yUOM IS NULL AND MGInventoryUOM IS NULL) AND (MGName = @Original_MGName OR @Orig" & _
        "inal_MGName IS NULL AND MGName IS NULL) AND (MGPrimaryUOM = @Original_MGPrimaryU" & _
        "OM OR @Original_MGPrimaryUOM IS NULL AND MGPrimaryUOM IS NULL) AND (MGStocked = " & _
        "@Original_MGStocked OR @Original_MGStocked IS NULL AND MGStocked IS NULL)"
        Me.SqlDeleteCommand4.Connection = Me.SqlConnection
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGAssignToPortion", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGAssignToPortion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGControlledAtHeadOffice", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGControlledAtHeadOffice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGInventoryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGInventoryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGPrimaryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGPrimaryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGStocked", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGStocked", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand4
        '
        Me.SqlInsertCommand4.CommandText = "INSERT INTO MaterialGroups(MGInventoryUOM, MGPrimaryUOM, MGStocked, MGName, MGCon" & _
        "trolledAtHeadOffice, MGAssignToPortion) VALUES (@MGInventoryUOM, @MGPrimaryUOM, " & _
        "@MGStocked, @MGName, @MGControlledAtHeadOffice, @MGAssignToPortion); SELECT MGID" & _
        ", MGInventoryUOM, MGPrimaryUOM, MGStocked, MGName, MGControlledAtHeadOffice, MGA" & _
        "ssignToPortion FROM MaterialGroups WHERE (MGID = @@IDENTITY)"
        Me.SqlInsertCommand4.Connection = Me.SqlConnection
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGInventoryUOM", System.Data.SqlDbType.VarChar, 2, "MGInventoryUOM"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGPrimaryUOM", System.Data.SqlDbType.VarChar, 2, "MGPrimaryUOM"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGStocked", System.Data.SqlDbType.Bit, 1, "MGStocked"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGName", System.Data.SqlDbType.VarChar, 50, "MGName"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGControlledAtHeadOffice", System.Data.SqlDbType.Bit, 1, "MGControlledAtHeadOffice"))
        Me.SqlInsertCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGAssignToPortion", System.Data.SqlDbType.VarChar, 2, "MGAssignToPortion"))
        '
        'SqlSelectCommand4
        '
        Me.SqlSelectCommand4.CommandText = "SELECT MGID, MGInventoryUOM, MGPrimaryUOM, MGStocked, MGName, MGControlledAtHeadO" & _
        "ffice, MGAssignToPortion FROM MaterialGroups WHERE (MGID = @MGID)"
        Me.SqlSelectCommand4.Connection = Me.SqlConnection
        Me.SqlSelectCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        '
        'SqlUpdateCommand4
        '
        Me.SqlUpdateCommand4.CommandText = "UPDATE MaterialGroups SET MGInventoryUOM = @MGInventoryUOM, MGPrimaryUOM = @MGPri" & _
        "maryUOM, MGStocked = @MGStocked, MGName = @MGName, MGControlledAtHeadOffice = @M" & _
        "GControlledAtHeadOffice, MGAssignToPortion = @MGAssignToPortion WHERE (MGID = @O" & _
        "riginal_MGID) AND (MGAssignToPortion = @Original_MGAssignToPortion OR @Original_" & _
        "MGAssignToPortion IS NULL AND MGAssignToPortion IS NULL) AND (MGControlledAtHead" & _
        "Office = @Original_MGControlledAtHeadOffice OR @Original_MGControlledAtHeadOffic" & _
        "e IS NULL AND MGControlledAtHeadOffice IS NULL) AND (MGInventoryUOM = @Original_" & _
        "MGInventoryUOM OR @Original_MGInventoryUOM IS NULL AND MGInventoryUOM IS NULL) A" & _
        "ND (MGName = @Original_MGName OR @Original_MGName IS NULL AND MGName IS NULL) AN" & _
        "D (MGPrimaryUOM = @Original_MGPrimaryUOM OR @Original_MGPrimaryUOM IS NULL AND M" & _
        "GPrimaryUOM IS NULL) AND (MGStocked = @Original_MGStocked OR @Original_MGStocked" & _
        " IS NULL AND MGStocked IS NULL); SELECT MGID, MGInventoryUOM, MGPrimaryUOM, MGSt" & _
        "ocked, MGName, MGControlledAtHeadOffice, MGAssignToPortion FROM MaterialGroups W" & _
        "HERE (MGID = @MGID)"
        Me.SqlUpdateCommand4.Connection = Me.SqlConnection
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGInventoryUOM", System.Data.SqlDbType.VarChar, 2, "MGInventoryUOM"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGPrimaryUOM", System.Data.SqlDbType.VarChar, 2, "MGPrimaryUOM"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGStocked", System.Data.SqlDbType.Bit, 1, "MGStocked"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGName", System.Data.SqlDbType.VarChar, 50, "MGName"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGControlledAtHeadOffice", System.Data.SqlDbType.Bit, 1, "MGControlledAtHeadOffice"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGAssignToPortion", System.Data.SqlDbType.VarChar, 2, "MGAssignToPortion"))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGID", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGAssignToPortion", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGAssignToPortion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGControlledAtHeadOffice", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGControlledAtHeadOffice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGInventoryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGInventoryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGPrimaryUOM", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGPrimaryUOM", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MGStocked", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MGStocked", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand4.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MGID", System.Data.SqlDbType.SmallInt, 2, "MGID"))
        '
        'daStockedItems
        '
        Me.daStockedItems.DeleteCommand = Me.SqlDeleteCommand3
        Me.daStockedItems.InsertCommand = Me.SqlInsertCommand3
        Me.daStockedItems.SelectCommand = Me.SqlSelectCommand3
        Me.daStockedItems.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "VMaster_StockedItems", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("MSIID", "MSIID"), New System.Data.Common.DataColumnMapping("MMTID", "MMTID"), New System.Data.Common.DataColumnMapping("MSIConversionToPrimary", "MSIConversionToPrimary"), New System.Data.Common.DataColumnMapping("MSIName", "MSIName")})})
        Me.daStockedItems.UpdateCommand = Me.SqlUpdateCommand3
        '
        'SqlDeleteCommand3
        '
        Me.SqlDeleteCommand3.CommandText = "DELETE FROM Master_StockedItems WHERE (MSIID = @Original_MSIID) AND (MMTID = @Ori" & _
        "ginal_MMTID) AND (MSIConversionToPrimary = @Original_MSIConversionToPrimary OR @" & _
        "Original_MSIConversionToPrimary IS NULL AND MSIConversionToPrimary IS NULL) AND " & _
        "(MSIName = @Original_MSIName OR @Original_MSIName IS NULL AND MSIName IS NULL)"
        Me.SqlDeleteCommand3.Connection = Me.SqlConnection
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MSIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MSIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MSIConversionToPrimary", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MSIConversionToPrimary", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MSIName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MSIName", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlInsertCommand3
        '
        Me.SqlInsertCommand3.CommandText = "INSERT INTO Master_StockedItems(MMTID, MSIConversionToPrimary, MSIName) VALUES (@" & _
        "MMTID, @MSIConversionToPrimary, @MSIName); SELECT MSIID, MMTID, MSIConversionToP" & _
        "rimary, MSIName FROM Master_StockedItems WHERE (MSIID = @@IDENTITY)"
        Me.SqlInsertCommand3.Connection = Me.SqlConnection
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTID", System.Data.SqlDbType.Int, 4, "MMTID"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MSIConversionToPrimary", System.Data.SqlDbType.Real, 4, "MSIConversionToPrimary"))
        Me.SqlInsertCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MSIName", System.Data.SqlDbType.VarChar, 50, "MSIName"))
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT MSIID, MMTID, MSIConversionToPrimary, MSIName, MSIInventoryUOM, MMTPrimary" & _
        "UOM, MMTPrimaryUOMShortName FROM VMaster_StockedItems WHERE (MMTID = @MMTID)"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection
        Me.SqlSelectCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTID", System.Data.SqlDbType.Int, 4, "MMTID"))
        '
        'SqlUpdateCommand3
        '
        Me.SqlUpdateCommand3.CommandText = "UPDATE Master_StockedItems SET MMTID = @MMTID, MSIConversionToPrimary = @MSIConve" & _
        "rsionToPrimary, MSIName = @MSIName WHERE (MSIID = @Original_MSIID) AND (MMTID = " & _
        "@Original_MMTID) AND (MSIConversionToPrimary = @Original_MSIConversionToPrimary " & _
        "OR @Original_MSIConversionToPrimary IS NULL AND MSIConversionToPrimary IS NULL) " & _
        "AND (MSIName = @Original_MSIName OR @Original_MSIName IS NULL AND MSIName IS NUL" & _
        "L); SELECT MSIID, MMTID, MSIConversionToPrimary, MSIName FROM Master_StockedItem" & _
        "s WHERE (MSIID = @MSIID)"
        Me.SqlUpdateCommand3.Connection = Me.SqlConnection
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MMTID", System.Data.SqlDbType.Int, 4, "MMTID"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MSIConversionToPrimary", System.Data.SqlDbType.Real, 4, "MSIConversionToPrimary"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MSIName", System.Data.SqlDbType.VarChar, 50, "MSIName"))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MSIID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MSIID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MMTID", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MMTID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MSIConversionToPrimary", System.Data.SqlDbType.Real, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MSIConversionToPrimary", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MSIName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MSIName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand3.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MSIID", System.Data.SqlDbType.Int, 4, "MSIID"))
        '
        'tcTabs
        '
        Me.tcTabs.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tcTabs.Location = New System.Drawing.Point(8, 8)
        Me.tcTabs.Name = "tcTabs"
        Me.tcTabs.SelectedTabPage = Me.tpProperties
        Me.tcTabs.Size = New System.Drawing.Size(416, 264)
        Me.tcTabs.TabIndex = 0
        Me.tcTabs.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tpProperties, Me.tpStockedItems})
        Me.tcTabs.Text = "XtraTabControl1"
        '
        'tpProperties
        '
        Me.tpProperties.Controls.Add(Me.GroupControl3)
        Me.tpProperties.Controls.Add(Me.txtMTName)
        Me.tpProperties.Controls.Add(Me.Label4)
        Me.tpProperties.Name = "tpProperties"
        Me.tpProperties.Size = New System.Drawing.Size(410, 238)
        Me.tpProperties.Text = "Material Properties"
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.Label6)
        Me.GroupControl3.Controls.Add(Me.txtMTInventoryUOM)
        Me.GroupControl3.Controls.Add(Me.lblMTInventoryUOM)
        Me.GroupControl3.Controls.Add(Me.txtMTPrimaryUOM)
        Me.GroupControl3.Controls.Add(Me.Label1)
        Me.GroupControl3.Location = New System.Drawing.Point(8, 56)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(392, 128)
        Me.GroupControl3.TabIndex = 2
        Me.GroupControl3.Text = "Units of Measure"
        '
        'Label6
        '
        Me.Label6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.Location = New System.Drawing.Point(8, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(376, 32)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "The primary unit of measure is used to cost the material into a job. The inventor" & _
        "y unit of measure is used to keep track of the materials inventory."
        '
        'txtMTInventoryUOM
        '
        Me.txtMTInventoryUOM.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Master_Materials.MMTInventoryUOM"))
        Me.txtMTInventoryUOM.Location = New System.Drawing.Point(160, 96)
        Me.txtMTInventoryUOM.Name = "txtMTInventoryUOM"
        '
        'txtMTInventoryUOM.Properties
        '
        Me.txtMTInventoryUOM.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtMTInventoryUOM.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("UOMLongName")})
        Me.txtMTInventoryUOM.Properties.DataSource = Me.DsInventoryUOM.UOM
        Me.txtMTInventoryUOM.Properties.DisplayMember = "UOMLongName"
        Me.txtMTInventoryUOM.Properties.NullText = "<Group default - >"
        Me.txtMTInventoryUOM.Properties.ShowFooter = False
        Me.txtMTInventoryUOM.Properties.ShowHeader = False
        Me.txtMTInventoryUOM.Properties.ValueMember = "UOMID"
        Me.txtMTInventoryUOM.Size = New System.Drawing.Size(208, 20)
        Me.txtMTInventoryUOM.TabIndex = 4
        '
        'DsInventoryUOM
        '
        Me.DsInventoryUOM.DataSetName = "dsUOM"
        Me.DsInventoryUOM.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'lblMTInventoryUOM
        '
        Me.lblMTInventoryUOM.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMTInventoryUOM.Location = New System.Drawing.Point(8, 96)
        Me.lblMTInventoryUOM.Name = "lblMTInventoryUOM"
        Me.lblMTInventoryUOM.Size = New System.Drawing.Size(144, 21)
        Me.lblMTInventoryUOM.TabIndex = 3
        Me.lblMTInventoryUOM.Text = "Inventory unit of measure:"
        Me.lblMTInventoryUOM.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMTPrimaryUOM
        '
        Me.txtMTPrimaryUOM.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Master_Materials.MMTPrimaryUOM"))
        Me.txtMTPrimaryUOM.Location = New System.Drawing.Point(160, 64)
        Me.txtMTPrimaryUOM.Name = "txtMTPrimaryUOM"
        '
        'txtMTPrimaryUOM.Properties
        '
        Me.txtMTPrimaryUOM.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True
        Me.txtMTPrimaryUOM.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtMTPrimaryUOM.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("UOMLongName")})
        Me.txtMTPrimaryUOM.Properties.DataSource = Me.DsPrimaryUOM.UOM
        Me.txtMTPrimaryUOM.Properties.DisplayMember = "UOMLongName"
        Me.txtMTPrimaryUOM.Properties.NullText = "<Group default - >"
        Me.txtMTPrimaryUOM.Properties.ShowFooter = False
        Me.txtMTPrimaryUOM.Properties.ShowHeader = False
        Me.txtMTPrimaryUOM.Properties.ValueMember = "UOMID"
        Me.txtMTPrimaryUOM.Size = New System.Drawing.Size(208, 20)
        Me.txtMTPrimaryUOM.TabIndex = 2
        '
        'DsPrimaryUOM
        '
        Me.DsPrimaryUOM.DataSetName = "dsUOM"
        Me.DsPrimaryUOM.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 64)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(136, 21)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Primary unit of measure:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMTName
        '
        Me.txtMTName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Master_Materials.MMTName"))
        Me.txtMTName.EditValue = ""
        Me.txtMTName.Location = New System.Drawing.Point(64, 16)
        Me.txtMTName.Name = "txtMTName"
        Me.txtMTName.Size = New System.Drawing.Size(336, 20)
        Me.txtMTName.TabIndex = 1
        '
        'tpStockedItems
        '
        Me.tpStockedItems.Controls.Add(Me.chkMMTAutoGenerateSIName)
        Me.tpStockedItems.Controls.Add(Me.btnRemoveSheetSize)
        Me.tpStockedItems.Controls.Add(Me.dgStockedItems)
        Me.tpStockedItems.Name = "tpStockedItems"
        Me.tpStockedItems.Size = New System.Drawing.Size(410, 238)
        Me.tpStockedItems.Text = "Sheet Sizes"
        '
        'chkMMTAutoGenerateSIName
        '
        Me.chkMMTAutoGenerateSIName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.dataSet, "Master_Materials.MMTAutoGenerateSIName"))
        Me.chkMMTAutoGenerateSIName.Location = New System.Drawing.Point(192, 208)
        Me.chkMMTAutoGenerateSIName.Name = "chkMMTAutoGenerateSIName"
        '
        'chkMMTAutoGenerateSIName.Properties
        '
        Me.chkMMTAutoGenerateSIName.Properties.Caption = "Automatically generate display names"
        Me.chkMMTAutoGenerateSIName.Size = New System.Drawing.Size(208, 18)
        Me.chkMMTAutoGenerateSIName.TabIndex = 2
        '
        'btnRemoveSheetSize
        '
        Me.btnRemoveSheetSize.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveSheetSize.Image = CType(resources.GetObject("btnRemoveSheetSize.Image"), System.Drawing.Image)
        Me.btnRemoveSheetSize.Location = New System.Drawing.Point(8, 212)
        Me.btnRemoveSheetSize.Name = "btnRemoveSheetSize"
        Me.btnRemoveSheetSize.Size = New System.Drawing.Size(72, 23)
        Me.btnRemoveSheetSize.TabIndex = 1
        Me.btnRemoveSheetSize.Text = "Remove"
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnOK.Location = New System.Drawing.Point(272, 280)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(72, 23)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "OK"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(352, 280)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton1.Image = CType(resources.GetObject("SimpleButton1.Image"), System.Drawing.Image)
        Me.SimpleButton1.Location = New System.Drawing.Point(8, 280)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(72, 23)
        Me.SimpleButton1.TabIndex = 1
        Me.SimpleButton1.Text = "Help"
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT UOMID, UOMLongName, UOMShortName FROM UOM"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = "INSERT INTO UOM(UOMID, UOMLongName, UOMShortName) VALUES (@UOMID, @UOMLongName, @" & _
        "UOMShortName); SELECT UOMID, UOMLongName, UOMShortName FROM UOM WHERE (UOMID = @" & _
        "UOMID)"
        Me.SqlInsertCommand2.Connection = Me.SqlConnection
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UOMID", System.Data.SqlDbType.VarChar, 2, "UOMID"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UOMLongName", System.Data.SqlDbType.VarChar, 50, "UOMLongName"))
        Me.SqlInsertCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UOMShortName", System.Data.SqlDbType.VarChar, 50, "UOMShortName"))
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE UOM SET UOMID = @UOMID, UOMLongName = @UOMLongName, UOMShortName = @UOMSho" & _
        "rtName WHERE (UOMID = @Original_UOMID) AND (UOMLongName = @Original_UOMLongName " & _
        "OR @Original_UOMLongName IS NULL AND UOMLongName IS NULL) AND (UOMShortName = @O" & _
        "riginal_UOMShortName OR @Original_UOMShortName IS NULL AND UOMShortName IS NULL)" & _
        "; SELECT UOMID, UOMLongName, UOMShortName FROM UOM WHERE (UOMID = @UOMID)"
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UOMID", System.Data.SqlDbType.VarChar, 2, "UOMID"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UOMLongName", System.Data.SqlDbType.VarChar, 50, "UOMLongName"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UOMShortName", System.Data.SqlDbType.VarChar, 50, "UOMShortName"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UOMID", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UOMID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UOMLongName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UOMLongName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UOMShortName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UOMShortName", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM UOM WHERE (UOMID = @Original_UOMID) AND (UOMLongName = @Original_UOML" & _
        "ongName OR @Original_UOMLongName IS NULL AND UOMLongName IS NULL) AND (UOMShortN" & _
        "ame = @Original_UOMShortName OR @Original_UOMShortName IS NULL AND UOMShortName " & _
        "IS NULL)"
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UOMID", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UOMID", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UOMLongName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UOMLongName", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UOMShortName", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UOMShortName", System.Data.DataRowVersion.Original, Nothing))
        '
        'daUOM
        '
        Me.daUOM.DeleteCommand = Me.SqlDeleteCommand2
        Me.daUOM.InsertCommand = Me.SqlInsertCommand2
        Me.daUOM.SelectCommand = Me.SqlSelectCommand2
        Me.daUOM.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "UOM", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("UOMID", "UOMID"), New System.Data.Common.DataColumnMapping("UOMLongName", "UOMLongName"), New System.Data.Common.DataColumnMapping("UOMShortName", "UOMShortName")})})
        Me.daUOM.UpdateCommand = Me.SqlUpdateCommand2
        '
        'frmMasterMaterial2
        '
        Me.AcceptButton = Me.btnOK
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(434, 312)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.tcTabs)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMasterMaterial2"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Material"
        CType(Me.dataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgStockedItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvStockedItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSIConversion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tcTabs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tcTabs.ResumeLayout(False)
        Me.tpProperties.ResumeLayout(False)
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.txtMTInventoryUOM.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsInventoryUOM, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMTPrimaryUOM.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsPrimaryUOM, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMTName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpStockedItems.ResumeLayout(False)
        CType(Me.chkMMTAutoGenerateSIName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


    ' This data must be loaded BEFORE the DataRow is loaded (eg lookups etc)
    Private Sub FillPreliminaryData()
    End Sub

    Private Sub FillMGIDDependantData(ByVal MGID As Integer)
        ' Material Group
        daMaterialGroups.SelectCommand.Parameters("@MGID").Value = MGID
        daMaterialGroups.Fill(dataSet)
        MaterialGroup_BranchRow = dataSet.MaterialGroups(0)

        ' UOM
        daUOM.Fill(DsPrimaryUOM)
        daUOM.Fill(DsInventoryUOM)
        Dim PrimaryNullText As String = "<Group default - " & DsPrimaryUOM.UOM.FindByUOMID(MaterialGroup_BranchRow("MGPrimaryUOM")).UOMLongName & ">"
        Dim InventoryNullText As String = "<Group default - " & DsInventoryUOM.UOM.FindByUOMID(MaterialGroup_BranchRow("MGInventoryUOM")).UOMLongName & ">"
        DsPrimaryUOM.UOM.AddUOMRow("", PrimaryNullText, DsPrimaryUOM.UOM.FindByUOMID(MaterialGroup_BranchRow("MGPrimaryUOM")).UOMShortName)
        DsInventoryUOM.UOM.AddUOMRow("", InventoryNullText, DsInventoryUOM.UOM.FindByUOMID(MaterialGroup_BranchRow("MGInventoryUOM")).UOMShortName)
        txtMTPrimaryUOM.Properties.NullText = PrimaryNullText
        txtMTInventoryUOM.Properties.NullText = InventoryNullText
    End Sub

    ' This data must be loaded AFTER the DataRow is loaded (eg record related data)
    Private Sub FillData()
        ' --- STOCKED ITEMS ---
        daStockedItems.SelectCommand.Parameters("@MMTID").Value = DataRow("MMTID")
        daStockedItems.Fill(dataSet)

        CustomizeScreen()
    End Sub

    Private Sub UpdateData()
        SqlDataAdapter.Update(dataSet)
        daStockedItems.Update(dataSet)
    End Sub

    Dim OK As Boolean = False
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        ' EndEdit() to end editing the dataset record so that we can update
        DataRow.EndEdit()
        If ValidateForm() Then
            UpdateData()
            OK = True
            Me.Close()
        End If
    End Sub

    Private Function ValidateForm() As Boolean
        If DataRow("MMTName") Is DBNull.Value Then
            Message.ShowMessage("You must enter a Name.", MessageBoxIcon.Exclamation)
            Return False
        End If



        'For i As Integer = dataSet.VMaster_StockedItems.Rows.Count - 1 To 0 Step -1
        '    Dim row As DataRow = dataSet.VMaster_StockedItems.Rows(i)
        '    If row.Item(0) Is Nothing Then
        '        dataSet.VMaster_StockedItems.Rows.Remove(row)
        '    ElseIf row.Item(0).ToString = "" Then
        '        dataSet.VMaster_StockedItems.Rows.Remove(row)
        '    End If
        'Next


        'For i As Integer = dataSet.VMaster_StockedItems.Rows.Count - 1 To 0 Step -1
        '    If (IsDBNull(dataSet.VMaster_StockedItems.Rows(i).Item("MSIName")) Or dataSet.VMaster_StockedItems.Rows(i).Item("MSIName") = "") Then
        '        dataSet.VMaster_StockedItems.Rows(i).Delete()

        '    End If
        'Next


        For Each stockedItem As DataRow In dataSet.VMaster_StockedItems

            If stockedItem("MSIName") Is DBNull.Value Then
                Message.ShowMessage("At least one of your Display Names against the sheet sizes are blank." &
                vbNewLine & vbNewLine & "You must either enter a Display Name for every sheet size or tick 'Automatically generate display names'.", MessageBoxIcon.Exclamation)
                Return False
            End If
            For Each otherStockedItem As DataRow In dataSet.VMaster_StockedItems
                If Not stockedItem Is otherStockedItem And stockedItem("MSIConversionToPrimary") = otherStockedItem("MSIConversionToPrimary") Then
                    Message.ShowMessage("Your sheet sizes are not unique.  You cannot have more than one of any given sheet size.", MessageBoxIcon.Exclamation)
                    Return False
                End If
            Next
        Next
        Return True
    End Function
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim response As MsgBoxResult

        If Me.DialogResult = DialogResult.OK Then
            If Not OK Then e.Cancel = True
        ElseIf Me.DialogResult = DialogResult.Cancel Then
            btnCancel.Focus()
            DataRow.EndEdit()
            If dataSet.HasChanges Then
                response = Message.AskCancelChanges
            Else
                response = MsgBoxResult.Yes
            End If
            If response = MsgBoxResult.No Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub CustomizeScreen()
        Dim IsStocked As Boolean
        If DataRow("MMTStocked") Is DBNull.Value Then
            IsStocked = MaterialGroup_BranchRow("MGStocked")
        Else
            IsStocked = DataRow("MMTStocked")
        End If
        If Not IsStocked Then
            txtMTInventoryUOM.Visible = False
            lblMTInventoryUOM.Visible = False
            tcTabs.TabPages.Remove(tpStockedItems)
        End If
        Dim MGName As String = ""
        If Not MaterialGroup_BranchRow("MGName") Is DBNull.Value Then
            MGName = MaterialGroup_BranchRow("MGName")
        End If
        Me.Text = MGName
        colSIName.OptionsColumn.AllowEdit = Not CBool(chkMMTAutoGenerateSIName.EditValue)
        colSIName.OptionsColumn.AllowFocus = Not CBool(chkMMTAutoGenerateSIName.EditValue)
        colSIName.OptionsColumn.ReadOnly = CBool(chkMMTAutoGenerateSIName.EditValue)
    End Sub

#Region " Sheet Sizes "

    Private Sub gvStockedItems_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gvStockedItems.InitNewRow
        gvStockedItems.GetDataRow(e.RowHandle)("MMTID") = DataRow("MMTID")
        gvStockedItems.GetDataRow(e.RowHandle)("MMTPrimaryUOMShortName") = DsPrimaryUOM.UOM.FindByUOMID(IsNull(DataRow("MMTPrimaryUOM"), MaterialGroup_BranchRow("MGPrimaryUOM"))).UOMShortName
    End Sub

    Private Sub gvStockedItems_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles gvStockedItems.CellValueChanged
        If e.Column Is colSIConversionToPrimary Then
            If gvStockedItems.GetDataRow(e.RowHandle)("MSIConversionToPrimary") Is DBNull.Value Then
                If MSIIsUsed(SelectedSheetSize("MSIID"), SqlConnection) And SelectedSheetSize.RowState <> DataRowState.Added Then
                    Message.ShowMessage("You cannot delete this stocked item because it has either been used in a job or is currently being held in stock at a branch.", MessageBoxIcon.Exclamation)
                    gvStockedItems.GetDataRow(e.RowHandle).CancelEdit()
                Else
                    gvStockedItems.GetDataRow(e.RowHandle).Delete()
                End If
            Else
                If CBool(chkMMTAutoGenerateSIName.EditValue) Then
                    gvStockedItems.GetDataRow(e.RowHandle)("MSIName") = DataRow("MMTName") & " - " & _
                                        gvStockedItems.GetDataRow(e.RowHandle)("MSIConversionToPrimary") & " " & _
                                        DsPrimaryUOM.UOM.FindByUOMID(IsNull(DataRow("MMTPrimaryUOM"), MaterialGroup_BranchRow("MGPrimaryUOM"))).UOMShortName
                End If
            End If
        End If
    End Sub

    Private Sub gvStockedItems_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gvStockedItems.KeyDown
        If e.KeyCode = Keys.Delete Then
            If gvStockedItems.SelectedRowsCount > 0 Then
                If MSIIsUsed(SelectedSheetSize("MSIID"), SqlConnection) And SelectedSheetSize.RowState <> DataRowState.Added Then
                    Message.ShowMessage("You cannot delete this stocked item because it has either been used in a job or is currently being held in stock at a branch.", MessageBoxIcon.Exclamation)
                Else
                    gvStockedItems.GetDataRow(gvStockedItems.GetSelectedRows(0)).Delete()
                End If
            End If
        End If
    End Sub

    Public ReadOnly Property SelectedSheetSize() As DataRow
        Get
            If Not gvStockedItems.GetSelectedRows Is Nothing Then
                Return gvStockedItems.GetDataRow(gvStockedItems.GetSelectedRows(0))
            End If
        End Get
    End Property

    Private Sub btnRemoveSheetSize_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveSheetSize.Click
        If Not SelectedSheetSize Is Nothing Then
            If MSIIsUsed(SelectedSheetSize("MSIID"), SqlConnection) And SelectedSheetSize.RowState <> DataRowState.Added Then
                Message.ShowMessage("You cannot delete this stocked item because it has either been used in a job or is currently being held in stock at a branch.", MessageBoxIcon.Exclamation)
            Else
                SelectedSheetSize.Delete()
                dataSet.VMaster_StockedItems.AcceptChanges()
            End If
        End If
    End Sub

#End Region

    Private Sub Decimal_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) _
    Handles txtSIConversion.ParseEditValue
        Format.Decimal_ParseEditValue(sender, e)
    End Sub

    Private Sub Text_ParseEditValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs) _
    Handles txtMTName.ParseEditValue
        Format.Text_ParseEditValue(sender, e)
    End Sub

    Private Sub txtUOM_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles txtMTPrimaryUOM.EditValueChanged, txtMTInventoryUOM.EditValueChanged
        If TypeOf CType(sender, DevExpress.XtraEditors.LookUpEdit).EditValue Is String Then
            If CType(sender, DevExpress.XtraEditors.LookUpEdit).EditValue = "" Then
                CType(sender, DevExpress.XtraEditors.LookUpEdit).EditValue = DBNull.Value
            End If
        End If
    End Sub

    Private Sub txtUOM_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) _
    Handles txtMTInventoryUOM.KeyDown, txtMTPrimaryUOM.KeyDown
        If e.KeyCode = Keys.Delete Then
            CType(sender, DevExpress.XtraEditors.LookUpEdit).EditValue = Nothing
        End If
    End Sub

    Private Sub txtUOM_Validated(ByVal sender As Object, ByVal e As System.EventArgs) _
    Handles txtMTInventoryUOM.Validated, txtMTPrimaryUOM.Validated
        For i As Integer = 0 To dataSet.VMaster_StockedItems.Count - 1
            dataSet.VMaster_StockedItems(i)("MMTPrimaryUOMShortName") = DsPrimaryUOM.UOM.FindByUOMID(IsNull(DataRow("MMTPrimaryUOM"), MaterialGroup_BranchRow("MGPrimaryUOM"))).UOMShortName
            If CBool(chkMMTAutoGenerateSIName.EditValue) Then
                dataSet.VMaster_StockedItems(i)("MSIName") = DataRow("MMTName") & " - " & _
                        dataSet.VMaster_StockedItems(i)("MSIConversionToPrimary") & " " & _
                        DsPrimaryUOM.UOM.FindByUOMID(IsNull(DataRow("MMTPrimaryUOM"), MaterialGroup_BranchRow("MGPrimaryUOM"))).UOMShortName
            End If
        Next
    End Sub

    Private Sub txtMTName_Validated(ByVal sender As Object, ByVal e As System.EventArgs) _
    Handles txtMTName.Validated
        If CBool(chkMMTAutoGenerateSIName.EditValue) Then
            For i As Integer = 0 To dataSet.VMaster_StockedItems.Count - 1
                dataSet.VMaster_StockedItems(i)("MSIName") = DataRow("MMTName") & " - " & _
                        dataSet.VMaster_StockedItems(i)("MSIConversionToPrimary") & " " & _
                        DsPrimaryUOM.UOM.FindByUOMID(IsNull(DataRow("MMTPrimaryUOM"), MaterialGroup_BranchRow("MGPrimaryUOM"))).UOMShortName
            Next
        End If
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        ShowHelpTopic(Me, "MaterialsTerms.html")
    End Sub

    Private Sub chkmMTAutoGenerateSIName_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMMTAutoGenerateSIName.CheckedChanged
        colSIName.OptionsColumn.AllowEdit = Not CBool(chkMMTAutoGenerateSIName.EditValue)
        colSIName.OptionsColumn.AllowFocus = Not CBool(chkMMTAutoGenerateSIName.EditValue)
        colSIName.OptionsColumn.ReadOnly = CBool(chkMMTAutoGenerateSIName.EditValue)
        If CBool(chkMMTAutoGenerateSIName.EditValue) Then
            For i As Integer = 0 To dataSet.VMaster_StockedItems.Count - 1
                dataSet.VMaster_StockedItems(i)("MSIName") = DataRow("MMTName") & " - " & _
                        dataSet.VMaster_StockedItems(i)("MSIConversionToPrimary") & " " & _
                        DsPrimaryUOM.UOM.FindByUOMID(IsNull(DataRow("MMTPrimaryUOM"), MaterialGroup_BranchRow("MGPrimaryUOM"))).UOMShortName
            Next
        End If
    End Sub

End Class